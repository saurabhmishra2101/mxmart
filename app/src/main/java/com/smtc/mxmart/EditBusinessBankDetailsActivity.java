package com.smtc.mxmart;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;

public class EditBusinessBankDetailsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    Typeface source_sans_pro_normal;
    String panbusinessedttxtstr,cinbusinessedttxtstr,vattinedttxtstr,ecccodeedttxtstr,exciserangeedttxtstr,excisedivisionedttxtstr,etBillingAddBusinesstxtstr,citybusinessedttxtstr,pinbusinessedttxtstr,etStateBusinesstxtstr,excisecommisioneratebusinessedttxtstr,establishmentdatebusinessedttxtstr;
    String etBankNameBankDettxtstr,etBranchBankDettxtstr,etAccBankDettxtstr,etIfscBankDettxtstr,etCompanyWebBankDettxtstr,etCompanyLogoBankDettxtstr;
    String grpnamebankedttxtstr,grpcodebankedttxtstr,etLocationBankDettxtstr,userCodestr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    UtilsDialog util = new UtilsDialog();
    ProgressDialog pdia;
    ArrayList<String> cityal = new ArrayList<String>();
    ArrayList<String> stateal = new ArrayList<String>();
    AutoCompleteTextView citybusinessedttxt,etStateBusinesstxt;
    int mYear,mMonth,mDay;
    String estdatestr;
    private SimpleDateFormat dateFormatter;
    static EditText establishmentdatebusinessedttxt;
    DatePickerFragment newFragment;
    ImageView backiv;
    SessionManager sessionManager;
    private static final String LOG_TAG = "ExampleApp";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyCPffetd-dKlj51pNBPrDphhcnipwwS2Zo";
    TextView main_toolbar_title;
    String mobile_num,fcm_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_business_bank_details1);

        SharedPreferences prefs = EditBusinessBankDetailsActivity.this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobile_num = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id",null);

     /*   Toolbar toolbar = (Toolbar) findViewById(R.id.editbusinesstoolbar);
        setSupportActionBar(toolbar);
*/
        main_toolbar_title = (TextView)findViewById(R.id.main_toolbar_title);
        main_toolbar_title.setText("Edit Business & Others");

        sessionManager = new SessionManager(getApplicationContext());

        backiv = (ImageView)findViewById(R.id.backicon);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(EditBusinessBankDetailsActivity.this, HomeActivity.class);

                SingletonActivity.backfromeditpersonaldetails = false;
                SingletonActivity.fromviewlivetrade = false;
                SingletonActivity.fromaddnewoffer = false;
                SingletonActivity.isNotificationClicked = false;
                // SingletonActivity.index = index;
                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                SingletonActivity.FromAddNewOfferTabOne=false;
                SingletonActivity.FromAddNewOfferTabZero=false;
                SingletonActivity.isNewEnquiryClicked = false;
                SingletonActivity.frombackofbuyerdispatch = false;
                SingletonActivity.frombackofsellerdispatch = false;
                SingletonActivity.frombackofbuyerpayment = false;
                SingletonActivity.frombackofsellerpayment = false;
                SingletonActivity.fromselltodaysoffer = false;
                SingletonActivity.backfromeditbusinessdetails = true;

                startActivity(i);
            }
        });

        System.out.println("ST USER PROF JSON IN EDIT BUSINESS DETAILS FRAGMENT---"+ SingletonActivity.stuserprofjson);

        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

      
      
        final EditText panbusinessedttxt = (EditText)findViewById(R.id.panbusinessedt);
        panbusinessedttxt.setTypeface(source_sans_pro_normal);



        final EditText cinbusinessedttxt = (EditText)findViewById(R.id.cinbusinessedt);
        cinbusinessedttxt.setTypeface(source_sans_pro_normal);

        final EditText vattinedttxt = (EditText)findViewById(R.id.vattinedt);
        vattinedttxt.setTypeface(source_sans_pro_normal);

        final EditText ecccodeedttxt = (EditText)findViewById(R.id.ecccodeedt);
        ecccodeedttxt.setTypeface(source_sans_pro_normal);

        final EditText exciserangeedttxt = (EditText)findViewById(R.id.exciserangeedt);
        exciserangeedttxt.setTypeface(source_sans_pro_normal);

        final EditText excisedivisionedttxt = (EditText)findViewById(R.id.excisedivisionedt);
        excisedivisionedttxt.setTypeface(source_sans_pro_normal);

        final AutoCompleteTextView etBillingAddBusinesstxt = (AutoCompleteTextView)findViewById(R.id.etBillingAddBusiness);
        etBillingAddBusinesstxt.setTypeface(source_sans_pro_normal);

        etBillingAddBusinesstxt.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.google_places_list));
        etBillingAddBusinesstxt.setOnItemClickListener(EditBusinessBankDetailsActivity.this);

        citybusinessedttxt = (AutoCompleteTextView)findViewById(R.id.citybusinessedt);
        citybusinessedttxt.setTypeface(source_sans_pro_normal);



        final EditText pinbusinessedttxt = (EditText)findViewById(R.id.pinbusinessedt);
        pinbusinessedttxt.setTypeface(source_sans_pro_normal);

        etStateBusinesstxt = (AutoCompleteTextView)findViewById(R.id.etStateBusiness);
        etStateBusinesstxt.setTypeface(source_sans_pro_normal);




        establishmentdatebusinessedttxt = (EditText)findViewById(R.id.establishmentdatebusinessedt);
        establishmentdatebusinessedttxt.setTypeface(source_sans_pro_normal);

        establishmentdatebusinessedttxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");

              /*  Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                System.out.println("the selected " + mDay);
                DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                        new mDateSetListener(), mYear, mMonth, mDay);
                dialog.show();*/


            }
        });
        establishmentdatebusinessedttxt.setFocusable(false);
        establishmentdatebusinessedttxt.setClickable(true);



        if(NetworkUtility.checkConnectivity(EditBusinessBankDetailsActivity.this)){
            String getlocationurl = APIName.URL+"/home/getLocation";
            System.out.println("GET LOCATION API IS---"+ getlocationurl);
            GetLocationAPI(getlocationurl);

        }
        else{
            util.dialog(EditBusinessBankDetailsActivity.this, "Please check your internet connection.");
        }


        //-------------------------------------------------------------------------

        final EditText etBankNameBankDettxt = (EditText)findViewById(R.id.etBankNameBankDet);
        etBankNameBankDettxt.setTypeface(source_sans_pro_normal);

        final EditText etBranchBankDettxt = (EditText)findViewById(R.id.etBranchBankDet);
        etBranchBankDettxt.setTypeface(source_sans_pro_normal);

        final EditText etAccBankDettxt = (EditText)findViewById(R.id.etAccBankDet);
        etAccBankDettxt.setTypeface(source_sans_pro_normal);

        final EditText etIfscBankDettxt = (EditText)findViewById(R.id.etIfscBankDet);
        etIfscBankDettxt.setTypeface(source_sans_pro_normal);

        final EditText etCompanyWebBankDettxt = (EditText)findViewById(R.id.etCompanyWebBankDet);
        etCompanyWebBankDettxt.setTypeface(source_sans_pro_normal);

        final EditText etCompanyLogoBankDettxt = (EditText)findViewById(R.id.etCompanyLogoBankDet);
        etCompanyWebBankDettxt.setTypeface(source_sans_pro_normal);
        etCompanyLogoBankDettxt.setEnabled(false);

        final EditText grpnamebankedttxt = (EditText)findViewById(R.id.grpnamebankedt);
        grpnamebankedttxt.setTypeface(source_sans_pro_normal);
        grpnamebankedttxt.setEnabled(false);

        final EditText grpcodebankedttxt = (EditText)findViewById(R.id.grpcodebankedt);
        grpcodebankedttxt.setTypeface(source_sans_pro_normal);
        grpcodebankedttxt.setEnabled(false);


        final EditText etLocationBankDettxt = (EditText)findViewById(R.id.etLocationBankDet);
        etLocationBankDettxt.setTypeface(source_sans_pro_normal);
        etLocationBankDettxt.setEnabled(false);


      /*  try {
            if(SingletonActivity.stuserprofjson.getString("company_type").equalsIgnoreCase("Public/Private Ltd")) {

                cinbusinessedttxt.setHint("CIN*");

            }

            else
            {
                cinbusinessedttxt.setHint("CIN");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
*/

        RelativeLayout submitbtn = (RelativeLayout)findViewById(R.id.sbmteditbankrelative);
        submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                System.out.println("USER CODE IN EDIT PERSONAL DETAILS FRAGMENT---"+ userCodestr);


                boolean invalid = false;




                /* if (SingletonActivity.stuserprofjson.getString("company_type").equalsIgnoreCase("Public/Private Ltd")) {

                     if (panbusinessedttxt.getText().toString().length() != 10) {
                         invalid = true;

                         System.out.println("LOOP BUSINESS BANK DETAILS---1");

                         Toast.makeText(EditBusinessBankDetailsActivity.this, "Please Enter PAN",
                                 Toast.LENGTH_SHORT).show();
                     } else if (cinbusinessedttxt.getText().toString().trim().equals("")) {
                         invalid = true;
                         System.out.println("LOOP BUSINESS BANK DETAILS---2");


                         Toast.makeText(EditBusinessBankDetailsActivity.this,
                                 "Please Enter CIN", Toast.LENGTH_SHORT)
                                 .show();


                     } else if (vattinedttxt.getText().toString().trim().equals("")) {
                         invalid = true;
                         System.out.println("LOOP BUSINESS BANK DETAILS---3");
                         Toast.makeText(EditBusinessBankDetailsActivity.this,
                                 "Please Enter GSTIN", Toast.LENGTH_SHORT)
                                 .show();


                     } else if (etBillingAddBusinesstxt.getText().toString().trim().equals("")) {
                         invalid = true;
                         System.out.println("LOOP BUSINESS BANK DETAILS---4");
                         Toast.makeText(EditBusinessBankDetailsActivity.this,
                                 "Please Enter Billing Address", Toast.LENGTH_SHORT)
                                 .show();


                     } else if (citybusinessedttxt.getText().toString().trim().equals("")) {
                         invalid = true;
                         System.out.println("LOOP BUSINESS BANK DETAILS---5");
                         Toast.makeText(EditBusinessBankDetailsActivity.this,
                                 "Please Enter City", Toast.LENGTH_SHORT)
                                 .show();


                     } else if (etBankNameBankDettxt.getText().toString().trim().equals("")) {
                         invalid = true;
                         System.out.println("LOOP BUSINESS BANK DETAILS---6");
                         Toast.makeText(EditBusinessBankDetailsActivity.this,
                                 "Please Enter Bank Name", Toast.LENGTH_SHORT)
                                 .show();


                     } else if (etAccBankDettxt.getText().toString().trim().equals("")) {
                         invalid = true;
                         System.out.println("LOOP BUSINESS BANK DETAILS---7");
                         Toast.makeText(EditBusinessBankDetailsActivity.this,
                                 "Please Enter Bank Account No.", Toast.LENGTH_SHORT)
                                 .show();


                     } else if (etIfscBankDettxt.getText().toString().trim().equals("")) {
                         invalid = true;
                         System.out.println("LOOP BUSINESS BANK DETAILS---8");
                         Toast.makeText(EditBusinessBankDetailsActivity.this,
                                 "Please Enter Bank IFSC Code", Toast.LENGTH_SHORT)
                                 .show();


                     } else if (invalid == false) {

                         System.out.println("LOOP BUSINESS BANK DETAILS---9");
                         panbusinessedttxtstr = panbusinessedttxt.getText().toString();
                         cinbusinessedttxtstr = cinbusinessedttxt.getText().toString();
                         vattinedttxtstr = vattinedttxt.getText().toString();
                         ecccodeedttxtstr = ecccodeedttxt.getText().toString();
                         exciserangeedttxtstr = exciserangeedttxt.getText().toString();
                         excisedivisionedttxtstr = excisedivisionedttxt.getText().toString();
                         etBillingAddBusinesstxtstr = etBillingAddBusinesstxt.getText().toString();
                         citybusinessedttxtstr = citybusinessedttxt.getText().toString();
                         pinbusinessedttxtstr = pinbusinessedttxt.getText().toString();
                         etStateBusinesstxtstr = etStateBusinesstxt.getText().toString();
                         // excisecommisioneratebusinessedttxtstr = excisecommisioneratebusinessedttxt.getText().toString();
                         establishmentdatebusinessedttxtstr = establishmentdatebusinessedttxt.getText().toString();

                         etBankNameBankDettxtstr = etBankNameBankDettxt.getText().toString();
                         etBranchBankDettxtstr = etBranchBankDettxt.getText().toString();
                         etAccBankDettxtstr = etAccBankDettxt.getText().toString();
                         etIfscBankDettxtstr = etIfscBankDettxt.getText().toString();
                         etCompanyWebBankDettxtstr = etCompanyWebBankDettxt.getText().toString();
                         etCompanyLogoBankDettxtstr = etCompanyLogoBankDettxt.getText().toString();

                         grpnamebankedttxtstr = grpnamebankedttxt.getText().toString();
                         grpcodebankedttxtstr = grpcodebankedttxt.getText().toString();
                         etLocationBankDettxtstr = etLocationBankDettxt.getText().toString();


                         System.out.println("Edit Business Details:--- " + panbusinessedttxtstr + "," + cinbusinessedttxtstr + "," + vattinedttxtstr + "," + ecccodeedttxtstr + "," + exciserangeedttxtstr + "," + excisedivisionedttxtstr + "," + etBillingAddBusinesstxtstr + "," + citybusinessedttxtstr + "," + pinbusinessedttxtstr + "," + etStateBusinesstxtstr + "," + excisecommisioneratebusinessedttxtstr + "," + establishmentdatebusinessedttxtstr);
                         System.out.println("Edit Bank Details:--- " + etBankNameBankDettxtstr + "," + etBranchBankDettxtstr + "," + etAccBankDettxtstr + "," + etIfscBankDettxtstr + "," + etCompanyWebBankDettxtstr + "," + etCompanyLogoBankDettxtstr);
                         System.out.println("For Office Use Only:--- " + grpnamebankedttxtstr + "," + grpcodebankedttxtstr + "," + etLocationBankDettxtstr);


                         if (NetworkUtility.checkConnectivity(EditBusinessBankDetailsActivity.this)) {
                             String editbusinessbankdetailsurl = APIName.URL + "/user/insertUpdateBussDetails?user_code=" + userCodestr;
                             System.out.println("EDIT BUSINESS BANK DETAILS URL IS---" + editbusinessbankdetailsurl);
                             EditBusinessBankProfileAPI(editbusinessbankdetailsurl, userCodestr);

                         } else {
                             util.dialog(EditBusinessBankDetailsActivity.this, "Please check your internet connection.");
                         }

                     }
                 } else {*/
                if (panbusinessedttxt.getText().toString().equals("")) {
                    invalid = true;

                    System.out.println("LOOP BUSINESS BANK DETAILS---1");

                    Toast.makeText(EditBusinessBankDetailsActivity.this, "Please Enter PAN",
                            Toast.LENGTH_SHORT).show();
                } else if (vattinedttxt.getText().toString().equals("")) {
                    invalid = true;
                    System.out.println("LOOP BUSINESS BANK DETAILS---3");
                    Toast.makeText(EditBusinessBankDetailsActivity.this,
                            "Please Enter GSTIN", Toast.LENGTH_SHORT)
                            .show();


                } else if (etBillingAddBusinesstxt.getText().toString().equals("")) {
                    invalid = true;
                    System.out.println("LOOP BUSINESS BANK DETAILS---4");
                    Toast.makeText(EditBusinessBankDetailsActivity.this,
                            "Please Enter Billing Address", Toast.LENGTH_SHORT)
                            .show();


                } else if (citybusinessedttxt.getText().toString().equals("")) {
                    invalid = true;
                    System.out.println("LOOP BUSINESS BANK DETAILS---5");
                    Toast.makeText(EditBusinessBankDetailsActivity.this,
                            "Please Enter City", Toast.LENGTH_SHORT)
                            .show();


                } else if (etBankNameBankDettxt.getText().toString().equals("")) {
                    invalid = true;
                    System.out.println("LOOP BUSINESS BANK DETAILS---6");
                    Toast.makeText(EditBusinessBankDetailsActivity.this,
                            "Please Enter Bank Name", Toast.LENGTH_SHORT)
                            .show();


                } else if (etAccBankDettxt.getText().toString().equals("")) {
                    invalid = true;
                    System.out.println("LOOP BUSINESS BANK DETAILS---7");
                    Toast.makeText(EditBusinessBankDetailsActivity.this,
                            "Please Enter Bank Account No.", Toast.LENGTH_SHORT)
                            .show();


                } else if (etIfscBankDettxt.getText().toString().equals("")) {
                    invalid = true;
                    System.out.println("LOOP BUSINESS BANK DETAILS---8");
                    Toast.makeText(EditBusinessBankDetailsActivity.this,
                            "Please Enter Bank IFSC Code", Toast.LENGTH_SHORT)
                            .show();


                } else if (invalid == false) {

                    System.out.println("LOOP BUSINESS BANK DETAILS---9");
                    panbusinessedttxtstr = panbusinessedttxt.getText().toString();
                    cinbusinessedttxtstr = cinbusinessedttxt.getText().toString();
                    vattinedttxtstr = vattinedttxt.getText().toString();
                    ecccodeedttxtstr = ecccodeedttxt.getText().toString();
                    exciserangeedttxtstr = exciserangeedttxt.getText().toString();
                    excisedivisionedttxtstr = excisedivisionedttxt.getText().toString();
                    etBillingAddBusinesstxtstr = etBillingAddBusinesstxt.getText().toString();
                    citybusinessedttxtstr = citybusinessedttxt.getText().toString();
                    pinbusinessedttxtstr = pinbusinessedttxt.getText().toString();
                    etStateBusinesstxtstr = etStateBusinesstxt.getText().toString();
                    // excisecommisioneratebusinessedttxtstr = excisecommisioneratebusinessedttxt.getText().toString();
                    establishmentdatebusinessedttxtstr = establishmentdatebusinessedttxt.getText().toString();

                    etBankNameBankDettxtstr = etBankNameBankDettxt.getText().toString();
                    etBranchBankDettxtstr = etBranchBankDettxt.getText().toString();
                    etAccBankDettxtstr = etAccBankDettxt.getText().toString();
                    etIfscBankDettxtstr = etIfscBankDettxt.getText().toString();
                    etCompanyWebBankDettxtstr = etCompanyWebBankDettxt.getText().toString();
                    etCompanyLogoBankDettxtstr = etCompanyLogoBankDettxt.getText().toString();

                    grpnamebankedttxtstr = grpnamebankedttxt.getText().toString();
                    grpcodebankedttxtstr = grpcodebankedttxt.getText().toString();
                    etLocationBankDettxtstr = etLocationBankDettxt.getText().toString();


                    System.out.println("Edit Business Details:--- " + panbusinessedttxtstr + "," + cinbusinessedttxtstr + "," + vattinedttxtstr + "," + ecccodeedttxtstr + "," + exciserangeedttxtstr + "," + excisedivisionedttxtstr + "," + etBillingAddBusinesstxtstr + "," + citybusinessedttxtstr + "," + pinbusinessedttxtstr + "," + etStateBusinesstxtstr + "," + excisecommisioneratebusinessedttxtstr + "," + establishmentdatebusinessedttxtstr);
                    System.out.println("Edit Bank Details:--- " + etBankNameBankDettxtstr + "," + etBranchBankDettxtstr + "," + etAccBankDettxtstr + "," + etIfscBankDettxtstr + "," + etCompanyWebBankDettxtstr + "," + etCompanyLogoBankDettxtstr);
                    System.out.println("For Office Use Only:--- " + grpnamebankedttxtstr + "," + grpcodebankedttxtstr + "," + etLocationBankDettxtstr);


                    if (NetworkUtility.checkConnectivity(EditBusinessBankDetailsActivity.this)) {
                        String editbusinessbankdetailsurl = APIName.URL + "/user/insertUpdateBussDetails?user_code=" + userCodestr;
                        System.out.println("EDIT BUSINESS BANK DETAILS URL IS---" + editbusinessbankdetailsurl);
                        EditBusinessBankProfileAPI(editbusinessbankdetailsurl, userCodestr);

                    } else {
                        util.dialog(EditBusinessBankDetailsActivity.this, "Please check your internet connection.");
                    }

               // }
            }


            }
        });




        if (SingletonActivity.stuserprofjson != null) {
            try {


                panbusinessedttxt.setText(SingletonActivity.stuserprofjson.getString("pan"));
                cinbusinessedttxt.setText(SingletonActivity.stuserprofjson.getString("cin"));
                vattinedttxt.setText(SingletonActivity.stuserprofjson.getString("vat"));
                ecccodeedttxt.setText(SingletonActivity.stuserprofjson.getString("excise_ecc_code"));
                //     exciserangeedttxt.setText(SingletonActivity.stuserprofjson.getString("excise_range"));
                //   excisedivisionedttxt.setText(SingletonActivity.stuserprofjson.getString("excise_division"));
                if(!SingletonActivity.stuserprofjson.getString("billing_address").equalsIgnoreCase("null")) {
                    etBillingAddBusinesstxt.setText(SingletonActivity.stuserprofjson.getString("billing_address"));
                }
                if(!SingletonActivity.stuserprofjson.getString("billing_city").equalsIgnoreCase("null")) {
                    citybusinessedttxt.setText(SingletonActivity.stuserprofjson.getString("billing_city"));
                }
                if(!SingletonActivity.stuserprofjson.getString("billing_pincode").equalsIgnoreCase("null")) {
                    pinbusinessedttxt.setText(SingletonActivity.stuserprofjson.getString("billing_pincode"));
                }
                if(!SingletonActivity.stuserprofjson.getString("billing_state").equalsIgnoreCase("null")) {
                    etStateBusinesstxt.setText(SingletonActivity.stuserprofjson.getString("billing_state"));
                }
               /* if(!SingletonActivity.stuserprofjson.getString("ecc_rate").equalsIgnoreCase("null")) {
                    excisecommisioneratebusinessedttxt.setText(SingletonActivity.stuserprofjson.getString("ecc_rate"));
                }*/
                if(!SingletonActivity.stuserprofjson.getString("establishment_date").equalsIgnoreCase("null")) {
                    establishmentdatebusinessedttxt.setText(SingletonActivity.stuserprofjson.getString("establishment_date"));
                }
                etBankNameBankDettxt.setText(SingletonActivity.stuserprofjson.getString("bank_account_name"));
                etBranchBankDettxt.setText(SingletonActivity.stuserprofjson.getString("bank_branch_name"));
                etAccBankDettxt.setText(SingletonActivity.stuserprofjson.getString("bank_account_number"));
                etIfscBankDettxt.setText(SingletonActivity.stuserprofjson.getString("bank_ifsc_code"));

                if(!SingletonActivity.stuserprofjson.getString("comp_website_url").equalsIgnoreCase("null")) {
                    etCompanyWebBankDettxt.setText(SingletonActivity.stuserprofjson.getString("comp_website_url"));
                }
                //  etCompanyLogoBankDettxt.setText(SingletonActivity.stuserprofjson.getString("picture"));
                //  grpnamebankedttxt.setText(SingletonActivity.stuserprofjson.getString("company_name"));
                //  grpnamebankedttxt.setTextColor(Color.parseColor("#000000"));
                //   grpcodebankedttxt.setText(SingletonActivity.stuserprofjson.getString("user_code"));
                //   grpcodebankedttxt.setTextColor(Color.parseColor("#000000"));
                etLocationBankDettxt.setText(SingletonActivity.stuserjson.getString("location_name"));
                etLocationBankDettxt.setTextColor(Color.parseColor("#000000"));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(EditBusinessBankDetailsActivity.this, HomeActivity.class);

        SingletonActivity.backfromeditpersonaldetails = false;
        SingletonActivity.fromviewlivetrade = false;
        SingletonActivity.fromaddnewoffer = false;
        SingletonActivity.isNotificationClicked = false;
        // SingletonActivity.index = index;
        SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
        SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
        SingletonActivity.FromAddNewOfferTabOne=false;
        SingletonActivity.FromAddNewOfferTabZero=false;
        SingletonActivity.isNewEnquiryClicked = false;
        SingletonActivity.frombackofbuyerdispatch = false;
        SingletonActivity.frombackofsellerdispatch = false;
        SingletonActivity.frombackofbuyerpayment = false;
        SingletonActivity.frombackofsellerpayment = false;
        SingletonActivity.fromselltodaysoffer = false;
        SingletonActivity.backfromeditbusinessdetails = true;

        startActivity(i);

    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            //sb.append("&components=country:in");
            sb.append("&components=");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: "+url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        String str = (String) adapterView.getItemAtPosition(position);
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;


        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user

            String daynew ="";
            String monthnew ="";

            if(day<10)
            {
                daynew = "0"+Integer.toString(day);
            }
            else
            {
                daynew = Integer.toString(day);
            }

            if(month+1<10)
            {
                monthnew = "0"+ Integer.toString(month+1);
            }
            else
            {
                monthnew = Integer.toString(month+1);
            }


            establishmentdatebusinessedttxt.setText(daynew + "/"
                    + monthnew + "/" + year);




        }
    }


    private void GetLocationAPI(String url) {
       /* pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();*/

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(EditBusinessBankDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };



        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //  pdia.dismiss();

                        cityal.clear();
                        stateal.clear();

                        System.out.println("RESPONSE OF GET LOCATION API IS---" + response);
                        final JSONArray jsonArray;
                        try {
                            //jsonArray = new JSONArray(response);


                            JSONObject getlocationjson = new JSONObject(response);
                            System.out.println("GET LOCATION JSON IS---" + getlocationjson);

                            String statusstr = getlocationjson.getString("status");
                            String msgstr = getlocationjson.getString("message");

                            JSONArray locationJSONArray = getlocationjson.getJSONArray("location");
                            System.out.println("GET LOCATION JSON ARRAY IS---" + locationJSONArray);

                            for(int i = 0;i < locationJSONArray.length();i++)
                            {
                                String citystr = locationJSONArray.getJSONObject(i).getString("city");
                                String statestr = locationJSONArray.getJSONObject(i).getString("state");

                                cityal.add(citystr);
                                stateal.add(statestr);

                                HashSet hs = new HashSet();

                                hs.addAll(stateal); // demoArrayList= name of arrayList from which u want to remove duplicates

                                stateal.clear();
                                stateal.addAll(hs);



                            }

                            System.out.println("CITIES FROM LOCATION API ARE-----"+ cityal);
                            System.out.println("STATES FROM LOCATION API ARE-----"+ stateal);

                            ArrayAdapter<String> cityadapter = new ArrayAdapter<String>(EditBusinessBankDetailsActivity.this,android.R.layout.simple_list_item_1,cityal);
                            citybusinessedttxt.setAdapter(cityadapter);

                            ArrayAdapter<String> stateadapter = new ArrayAdapter<String>(EditBusinessBankDetailsActivity.this,android.R.layout.simple_list_item_1,stateal);
                            etStateBusinesstxt.setAdapter(stateadapter);



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            //  pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        //pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(EditBusinessBankDetailsActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(EditBusinessBankDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(EditBusinessBankDetailsActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("getlocation params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditBusinessBankDetailsActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void EditBusinessBankProfileAPI(final String url,final String usercode) {
        pdia = new ProgressDialog(EditBusinessBankDetailsActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(EditBusinessBankDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        System.out.println("EDIT BUSINESS BANK DETAILS URL in resp IS---"+ url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();



                        System.out.println("RESPONSE OF EDIT BUSINESS BANK API IS---" + response);



                        try {



                            JSONObject editbusinessbankjson = new JSONObject(response);
                            System.out.println("EDIT BUSINESS BANK JSON IS---" + editbusinessbankjson);

                            String statusstr = editbusinessbankjson.getString("status");
                            String msgstr = editbusinessbankjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Toast.makeText(EditBusinessBankDetailsActivity.this,msgstr,Toast.LENGTH_SHORT).show();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        SingletonActivity.backfromeditbusinessdetails = true;
                                        Intent i = new Intent(EditBusinessBankDetailsActivity.this, HomeActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditBusinessBankDetailsActivity.this,error.toString(),Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("ERROR CODE--------" + networkResponse.statusCode);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(EditBusinessBankDetailsActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(EditBusinessBankDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(EditBusinessBankDetailsActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("pan",panbusinessedttxtstr);
                params.put("cin",cinbusinessedttxtstr);
                params.put("vat",vattinedttxtstr);
                params.put("excise_ecc_code",ecccodeedttxtstr);
                params.put("billing_address",etBillingAddBusinesstxtstr);
                params.put("billing_city",citybusinessedttxtstr);
                params.put("billing_state",etStateBusinesstxtstr);
                params.put("billing_pincode",pinbusinessedttxtstr);
                params.put("establishment_date",establishmentdatebusinessedttxtstr);
                params.put("bank_branch_name",etBranchBankDettxtstr);
                params.put("bank_account_number",etAccBankDettxtstr);
                params.put("bank_account_name",etBankNameBankDettxtstr);
                params.put("bank_ifsc_code",etIfscBankDettxtstr);
                params.put("comp_website_url",etCompanyWebBankDettxtstr);
                params.put("picture",etCompanyLogoBankDettxtstr);

/*
==========================================================================================

                params.put("excise_range",exciserangeedttxtstr);
                params.put("excise_division",excisedivisionedttxtstr);

                params.put("ecc_rate",excisecommisioneratebusinessedttxtstr);
*/





                System.out.println("edit business bank details params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditBusinessBankDetailsActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
        //  return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                System.out.println("OVERFLOWMENU ITEM 1 CLICKED");

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("login",false);


                editor.clear();
                editor.commit();

                sessionManager.logoutUser(mobile_num,fcm_id);


                // sessionManager.logoutUser();

            /*    Intent i = new Intent(LeaveManagementActivity.this,LoginActivity.class);
                startActivity(i);
*/
                return true;
          /*  case R.id.overflowmenuitem2:
                System.out.println("OVERFLOWMENU ITEM 2 CLICKED");

                return true;
            case R.id.overflowmenuitem3:
                System.out.println("OVERFLOWMENU ITEM 3 CLICKED");

                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
