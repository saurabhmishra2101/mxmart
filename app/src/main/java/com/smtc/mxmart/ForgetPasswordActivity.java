package com.smtc.mxmart;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by 10161 on 11/5/2017.
 */

public class ForgetPasswordActivity extends Activity {

    Typeface source_sans_pro_normal;
    EditText etMobile,etMobileOTP,etNwPassword,etConfrmPassword;
    TextView verifyotptxt,genotptxt,submitotptxt,mobileotptxt,successtxt1;
    RelativeLayout sbmtrelative,cancelrelative;
    UtilsDialog util = new UtilsDialog();
    ProgressDialog pdia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.forget_password);

        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

        etMobile = (EditText)findViewById(R.id.etMobile);
        etMobileOTP = (EditText)findViewById(R.id.etMobileOTP);
        successtxt1 = (TextView) findViewById(R.id.successtxt1);

        etNwPassword = (EditText)findViewById(R.id.etNwPassword);
        etConfrmPassword = (EditText)findViewById(R.id.etConfrmPassword);

        verifyotptxt = (TextView) findViewById(R.id.verifyotptxt);
        genotptxt = (TextView) findViewById(R.id.genotptxt);
        submitotptxt = (TextView) findViewById(R.id.submitotptxt);

        sbmtrelative = (RelativeLayout) findViewById(R.id.sbmtrelative);
        cancelrelative = (RelativeLayout) findViewById(R.id.cancelrelative);

        mobileotptxt = (TextView) findViewById(R.id.mobileotptxt);

        etMobileOTP.setTypeface(source_sans_pro_normal);
        etMobile.setTypeface(source_sans_pro_normal);
        etNwPassword.setTypeface(source_sans_pro_normal);
        etConfrmPassword.setTypeface(source_sans_pro_normal);
        verifyotptxt.setTypeface(source_sans_pro_normal);
        genotptxt.setTypeface(source_sans_pro_normal);

        submitotptxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String mobilenumstr = etMobile.getText().toString();

                if(mobilenumstr.length()==10) {

                    if (NetworkUtility.checkConnectivity(ForgetPasswordActivity.this)) {
                        String generateotpurl = APIName.URL + "/user/get_user_otp";
                        System.out.println("GENERATE OTP URL IS---" + generateotpurl);
                        GenerateOTPAPI(generateotpurl, mobilenumstr);

                    } else {
                        util.dialog(ForgetPasswordActivity.this, "Please check your internet connection.");
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile No.",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

        genotptxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String mobilenumstr = etMobile.getText().toString();

                if(mobilenumstr.length()==10) {

                    if (NetworkUtility.checkConnectivity(ForgetPasswordActivity.this)) {
                        String generateotpurl = APIName.URL + "/user/get_user_otp";
                        System.out.println("GENERATE OTP URL IS---" + generateotpurl);
                        GenerateOTPAPI(generateotpurl, mobilenumstr);

                    } else {
                        util.dialog(ForgetPasswordActivity.this, "Please check your internet connection.");
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile No.",
                            Toast.LENGTH_SHORT).show();
                }


            }
        });

        verifyotptxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean invalid = false;

                if (!isValidOTP(etMobileOTP.getText().toString())) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile OTP",
                            Toast.LENGTH_SHORT).show();
                } else if (invalid == false) {

                    if(etMobileOTP.getText().toString().equalsIgnoreCase(SingletonActivity.stotp))
                    {

                            etMobileOTP.setEnabled(false);

                            verifyotptxt.setText("Verified");
                            genotptxt.setEnabled(false);
                            verifyotptxt.setTextColor(Color.parseColor("#e4e4e4"));
                            genotptxt.setTextColor(Color.parseColor("#e4e4e4"));
                            etNwPassword.setEnabled(true);
                            etConfrmPassword.setEnabled(true);
                            etNwPassword.setVisibility(View.VISIBLE);
                            etConfrmPassword.setVisibility(View.VISIBLE);
                            sbmtrelative.setVisibility(View.VISIBLE);
                            cancelrelative.setVisibility(View.VISIBLE);




                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Wrong Mobile OTP",
                                Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        sbmtrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(((etNwPassword.length()>5)&&(!etNwPassword.getText().toString().equalsIgnoreCase("")))&&((etConfrmPassword.length()>5)&&(!etConfrmPassword.getText().toString().equalsIgnoreCase("")))) {


                    if (etConfrmPassword.getText().toString().equalsIgnoreCase(etNwPassword.getText().toString())) {
                        if(SingletonActivity.fromforgot==true) {
                            sbmtrelative.setEnabled(false);
                             if (NetworkUtility.checkConnectivity(ForgetPasswordActivity.this)) {
                                String updatepwdurl = APIName.URL + "/user/updatePassword";
                                System.out.println("UPDATE PASSWORD URL IS---" + updatepwdurl);
                                 UpdatePasswordAPI(updatepwdurl,etMobile.getText().toString(),etNwPassword.getText().toString(),SingletonActivity.stotp);


                            } else {
                                util.dialog(ForgetPasswordActivity.this, "Please check your internet connection.");
                            }
                        }else
                        {


                        }

                    } else {
                        Toast.makeText(ForgetPasswordActivity.this, "Password Not Matching", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {


                        if(etConfrmPassword.length()>5&&etNwPassword.length()>5)
                        {
                            if (etConfrmPassword.getText().toString().equalsIgnoreCase(etNwPassword.getText().toString())) {

                                System.out.println("FROM FORGOT VALUE==="+ SingletonActivity.fromforgot);

                                if(SingletonActivity.fromforgot==true) {
                                    sbmtrelative.setEnabled(false);
                                    if (NetworkUtility.checkConnectivity(ForgetPasswordActivity.this)) {
                                        String updatepwdurl = APIName.URL + "/user/updatePassword";
                                        System.out.println("UPDATE PASSWORD URL IS---" + updatepwdurl);
                                        UpdatePasswordAPI(updatepwdurl,etMobile.getText().toString(),etNwPassword.getText().toString(),SingletonActivity.stotp);


                                    } else {
                                        util.dialog(ForgetPasswordActivity.this, "Please check your internet connection.");
                                    }
                                }
                                else
                                {
                                   /* if (NetworkUtility.checkConnectivity(VerifyOtpActivity.this)) {
                                        String updatepwdurl = APIName.URL + "/user/updatePassword";
                                        System.out.println("UPDATE PASSWORD URL IS---" + updatepwdurl);
                                        UpdatePasswordAPI(updatepwdurl, etMobileNum.getText().toString(), etNewPassword.getText().toString(),SingletonActivity.stotp);


                                    } else {
                                        util.dialog(VerifyOtpActivity.this, "Please check your internet connection.");
                                    }*/
                                }

                            } else {
                                Toast.makeText(ForgetPasswordActivity.this, "Password Not Matching", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else
                        {
                            Toast.makeText(ForgetPasswordActivity.this, "Please Enter Atleast 6 Digit Password", Toast.LENGTH_SHORT).show();
                        }

                    }

                }


        });

       /*



        System.out.println("FROM FORGOT 1===="+ SingletonActivity.fromforgot);

        if (SingletonActivity.fromforgot == true)
        {
            resetpasswordtxt.setVisibility(View.VISIBLE);
            etMobileNum.setVisibility(View.VISIBLE);
            etMobileNum.setEnabled(true);
            genotptxt.setVisibility(View.VISIBLE);
            etOtp.setVisibility(View.GONE);
            verifytxt.setVisibility(View.GONE);
        }
        else
        {
            resetpasswordtxt.setVisibility(View.INVISIBLE);
            etMobileNum.setVisibility(View.GONE);
            genotptxt.setVisibility(View.GONE);
        }




       */

       cancelrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  finish();
            }
        });

    }
    public  final boolean isValidOTP(String mobile) {
        if (mobile.length()!=6) {



          /*  Toast.makeText(this,
                    "Please Enter Valid Mobile No.", Toast.LENGTH_SHORT)
                    .show();
*/
            return false;
        } else {
            return true;
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SingletonActivity.fromforgot = false;
    }

    private void UpdatePasswordAPI(String url, final String mobilenum, final String pwd, final String otp) {
        pdia = new ProgressDialog(ForgetPasswordActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(ForgetPasswordActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        System.out.println("RESPONSE OF UPDATE PASSWORD OTP API IS---" + response);


                        try {
                            //jsonArray = new JSONArray(response);


                            JSONObject updatepwdjson = new JSONObject(response);
                            System.out.println("UPDATE PASSWORD OTP RESPONSE JSON IS---" + updatepwdjson);

                            String statusstr = updatepwdjson.getString("status");





                            if (statusstr.equalsIgnoreCase("true")) {

                                AlertDialog.Builder builder1 = new AlertDialog.Builder(ForgetPasswordActivity.this);
                                builder1.setMessage(updatepwdjson.getString("message"));
                                builder1.setCancelable(true);

                                builder1.setPositiveButton(
                                        "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent i = new Intent(ForgetPasswordActivity.this,LoginActivity.class);
                                                startActivity(i);
                                            }
                                        });



                                AlertDialog alert11 = builder1.create();
                                alert11.show();






                            }

                            else
                            {
                                String msgstr = updatepwdjson.getString("message");
                                Toast.makeText(ForgetPasswordActivity.this,msgstr,Toast.LENGTH_SHORT).show();
                            }




                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(ForgetPasswordActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(ForgetPasswordActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(ForgetPasswordActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_num",mobilenum);
                params.put("password",pwd);
                params.put("otp",otp);
                params.put("type","1");




                System.out.println("update pwd params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }



    private void GenerateOTPAPI(String url,final String mobilenumstr) {
        pdia = new ProgressDialog(ForgetPasswordActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(ForgetPasswordActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        System.out.println("RESPONSE OF GENERATE OTP API IS---" + response);



                        try {


                            final JSONObject generateotpjson = new JSONObject(response);
                            System.out.println("GENERATE OTP RESPONSE JSON IS---" + generateotpjson);

                            String statusstr = generateotpjson.getString("status");


                            if (statusstr.equalsIgnoreCase("true")) {


                                String otpstr = generateotpjson.getString("otp");
                                SingletonActivity.stotp = otpstr;

                                submitotptxt.setEnabled(false);
                                submitotptxt.setTextColor(Color.parseColor("#e4e4e4"));
                                etMobile.setEnabled(false);
                                mobileotptxt.setVisibility(View.VISIBLE);
                                etMobileOTP.setVisibility(View.VISIBLE);
                                successtxt1.setVisibility(View.VISIBLE);
                                verifyotptxt.setVisibility(View.VISIBLE);
                                genotptxt.setVisibility(View.VISIBLE);


                            }

                            else
                            {
                                Toast.makeText(ForgetPasswordActivity.this,generateotpjson.getString("message"),Toast.LENGTH_SHORT).show();
                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(ForgetPasswordActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(ForgetPasswordActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(ForgetPasswordActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_num",mobilenumstr);

                System.out.println("generate otp params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }




}
