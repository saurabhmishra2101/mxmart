package com.smtc.mxmart;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CheckBox;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import android.text.Spannable;
import android.text.SpannableString;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.TooManyListenersException;
import android.text.style.ClickableSpan;
import android.text.Spanned;
import android.text.TextPaint;
import android.content.DialogInterface;

import android.text.method.LinkMovementMethod;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by 10161 on 11/2/2017.
 */

public class RegistrationActivity extends AppCompatActivity {

    Typeface source_sans_pro_normal,raleway_normal;
    TextView createaccounttxt,changingwaytxt,indiafirsttxt,regtext,signintxt;
    EditText etEmail,etMobileNum,etPassword;
    RelativeLayout regrelative;
    UtilsDialog util = new UtilsDialog();
    ProgressDialog pdia;
    SessionManager session;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    CheckBox chkbox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_registration);

        session = new SessionManager(getApplicationContext());

        chkbox = (CheckBox)findViewById(R.id.chkbox);
        createaccounttxt = (TextView)findViewById(R.id.createaccounttxt);
        changingwaytxt = (TextView)findViewById(R.id.changingwaytxt);
        indiafirsttxt = (TextView)findViewById(R.id.indiafirsttxt);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etMobileNum = (EditText)findViewById(R.id.etMobileNum);
        etPassword = (EditText)findViewById(R.id.etPassword);
        regtext = (TextView)findViewById(R.id.regtext);
        signintxt = (TextView)findViewById(R.id.signintxt);
        regrelative = (RelativeLayout)findViewById(R.id.regrelative);

      /*  etEmail.setText("saurabhmishra2101@gmail.com");
        etMobileNum.setText("8971131199");*/


        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");
        raleway_normal = Typeface.createFromAsset(getAssets(), "raleway/Raleway-Regular.ttf");

        createaccounttxt.setTypeface(raleway_normal);
        changingwaytxt.setTypeface(source_sans_pro_normal);
        indiafirsttxt.setTypeface(source_sans_pro_normal);
        etEmail.setTypeface(source_sans_pro_normal);
        etMobileNum.setTypeface(source_sans_pro_normal);
        etPassword.setTypeface(source_sans_pro_normal);
        regtext.setTypeface(source_sans_pro_normal);
        signintxt.setTypeface(source_sans_pro_normal);


        signintxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Spannable wordtoSpan = new SpannableString("I agree to the Mxmart terms & conditions*");




        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setTitle("Terms & Conditions");
        builder.setView(R.layout.activity_terms_and_conditions);
        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // dismiss dialog
                dialogInterface.dismiss();
            }
        });
        builder.show();


            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        wordtoSpan.setSpan(clickableSpan1, 22, 41, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        chkbox.setText(wordtoSpan);
        chkbox.setTypeface(source_sans_pro_normal);
        chkbox.setClickable(true);
        chkbox.setMovementMethod(LinkMovementMethod.getInstance());

        regrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailstr = etEmail.getText().toString();
                String mobilenumstr = etMobileNum.getText().toString();
               // String pwdstr = etPassword.getText().toString();

                boolean invalid = false;

                if (!isValidEmail(etEmail.getText().toString())) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(),
                            "Please Enter Valid Email", Toast.LENGTH_SHORT)
                            .show();
                   }
                else if (!isValidMobileNumber(mobilenumstr)) {
                    invalid = true;
                           Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number",
                                   Toast.LENGTH_SHORT).show();
                }


                else if (invalid == false)
                {

                     if(chkbox.isChecked()==false){
                        Toast.makeText(RegistrationActivity.this,"Please accept the Terms & Conditions",Toast.LENGTH_SHORT).show();
                      }
                      else
                     {

                         SingletonActivity.useremail = emailstr;
                         SingletonActivity.usermobile = mobilenumstr;

                    if(NetworkUtility.checkConnectivity(RegistrationActivity.this)){
                        String verifyusermobileurl = APIName.URL+"/user/verifyUserMobile";
                        System.out.println("VERIFY USER MOBILE URL IS---"+ verifyusermobileurl);
                        VerifyUserMobileAPI(verifyusermobileurl,emailstr,mobilenumstr);

                    }
                    else{
                        util.dialog(RegistrationActivity.this, "Please check your internet connection.");
                    }
                   }
                   /* Intent i = new Intent(RegistrationActivity.this,VerifyOtpActivity.class);
                    SingletonActivity.regmobileno = mobilenumstr;
                    SingletonActivity.regemail = emailstr;
                    startActivity(i);*/

                }

            }
        });

    }

    public final  boolean isValidEmail(String email) {
        if (email == null) {

           /* Toast.makeText(this,
                    "Please Enter Valid Email", Toast.LENGTH_SHORT)
                    .show();
*/

            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public  final boolean isValidMobileNumber(String mobile) {
        if (mobile.length()!=10) {



          /*  Toast.makeText(this,
                    "Please Enter Valid Mobile No.", Toast.LENGTH_SHORT)
                    .show();
*/
            return false;
        } else {
            return true;
        }
    }

    private void VerifyUserMobileAPI(String url,final String email,final String mobilestr) {
        pdia = new ProgressDialog(RegistrationActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(RegistrationActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        System.out.println("RESPONSE OF VERIFY USER MOBILE API IS---" + response);
                        final JSONArray jsonArray;
                        try {
                            //jsonArray = new JSONArray(response);


                            JSONObject verifyusermobilejson = new JSONObject(response);
                            System.out.println("VERIFY USER MOBILE JSON IS---" + verifyusermobilejson);

                            String statusstr = verifyusermobilejson.getString("status");

                            if (statusstr.equalsIgnoreCase("true")) {

                                String msgstr = verifyusermobilejson.getString("message");
                                String stotpstr = verifyusermobilejson.getString("st_otp");
                                String usercodestr = verifyusermobilejson.getString("user_code");
                                String mail_otp_str = verifyusermobilejson.getString("mail_otp");




                                Intent i = new Intent(RegistrationActivity.this,VerifyOtpActivity.class);
                                SingletonActivity.regmobileno = mobilestr;
                                SingletonActivity.regemail = email;
                                SingletonActivity.stotp = stotpstr;
                                SingletonActivity.mail_otp_str = mail_otp_str;
                                startActivity(i);


                            }
                            else
                            {
                                String msgstr = verifyusermobilejson.getString("message");
                                Toast.makeText(RegistrationActivity.this,msgstr,Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(RegistrationActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(RegistrationActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(RegistrationActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_num",mobilestr);
                params.put("email",email);

               /* params.put("company_name",cmpnyname);
                params.put("first_name",firstnamestr);*/


                System.out.println("verifymobileuser params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }



}
