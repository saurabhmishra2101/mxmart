package com.smtc.mxmart;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import android.os.Build;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.WeakHashMap;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by 10161 on 8/21/2017.
 */

public class BuyerPaymentDetailsActivity extends AppCompatActivity {

    ImageView backiv;
    SessionManager sessionManager;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    UtilsDialog util = new UtilsDialog();
    ImageView deletepaymentiv, empty_payment_iv, addpaymentiv;
    ProgressDialog pdia;
    String userCodestr, mobilenumstr, paymentdate;
    AlertDialog b;
    int selected = 0;
    ArrayList<String> paymentIdArray = new ArrayList<String>();
    CustomAdap customadap;
    ListView buypaymentdetailsnewlistvw;
    int mYear, mMonth, mDay;
    String paymentchqnum, datestr, banknamestr, amtstr;
    TextView notification_total_count, nopaymentdetailstxtvw, clickonplusbtntxtvw;
    String commentStr, tradeStr;
    TextView todays_offer_count, new_enquiry_count, seller_accepted_count, seller_rejected_count, sp_generated_count, buyer_rejected_count;
    int index;
    JSONArray getCurrentDealsPaymentJsonArray, PaymentDetailsJsonArray;
    ArrayList<String> tradeidlist = new ArrayList<String>();
    static List<String> expandableListTitle;
    static WeakHashMap<String, List<String>> expandableListDetail;
    JSONArray PaymentJsonArray, payment_resolved_jsonarray;
    String mobile_num, fcm_id, paymentremainingpaystr, buydeals,rate_str,quoted_rate_str,israte,final_exceeded_qty_str;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buyer_payment_new);
        notification_total_count = (TextView) findViewById(R.id.notification_total_count);

        deletepaymentiv = (ImageView) findViewById(R.id.deletepaymentiv);
        empty_payment_iv = (ImageView) findViewById(R.id.empty_payment_iv);
        nopaymentdetailstxtvw = (TextView) findViewById(R.id.nopaymentdetailstxtvw);
        clickonplusbtntxtvw = (TextView) findViewById(R.id.clickonplusbtntxtvw);
        addpaymentiv = (ImageView) findViewById(R.id.addpaymentiv);

        System.out.println("BUYER PAYMENT DETAILS JSONARRAY---" + SingletonActivity.paymentDetailsJSONArray);
        System.out.println("BUYER PAYMENT TRADE ID---" + SingletonActivity.tradeid);

        buypaymentdetailsnewlistvw = (ListView) findViewById(R.id.buypaymentdetailsnewlistvw);
        sessionManager = new SessionManager(getApplicationContext());

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        mobile_num = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id", null);
        buydeals = prefs.getString("buydeals", null);

        System.out.println("USER CODE IN MAIN ACTIVITY---" + userCodestr);


        backiv = (ImageView) findViewById(R.id.back_payment);


        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SingletonActivity.splited = "";
                Intent i = new Intent(BuyerPaymentDetailsActivity.this, HomeActivity.class);

                SingletonActivity.fromsellcurrentdeals = false;
                SingletonActivity.frombuycurrentdeals = false;
                SingletonActivity.frombackofsellerdispatch = false;
                SingletonActivity.frombackofbuyerdispatch = true;
                SingletonActivity.splited = "";
                startActivity(i);
            }
        });

        Intent i = getIntent();
        SingletonActivity.splited = i.getStringExtra("splitted");
        SingletonActivity.buyParent = i.getStringExtra("buyParent");


        System.out.println("trade type in payment details===" + SingletonActivity.buyParent);

        if (SingletonActivity.splited.equalsIgnoreCase("split")) {

            if (NetworkUtility.checkConnectivity(BuyerPaymentDetailsActivity.this)) {
                String GetCurrentDealsPaymentURL = APIName.URL + "/buyer/getCurrentDealsPayment?user_code=" + userCodestr + "&tradeId=" + SingletonActivity.tradeid + "&trade_type=" + SingletonActivity.buyParent;
                System.out.println("URL IS---1: " + GetCurrentDealsPaymentURL);
                GetCurrentDealsPaymentAPI(GetCurrentDealsPaymentURL);

            } else {
                util.dialog(BuyerPaymentDetailsActivity.this, "Please check your internet connection.");
            }
        } else {

            if (NetworkUtility.checkConnectivity(BuyerPaymentDetailsActivity.this)) {
                String GetBuyerPaymentDetailsURL = APIName.URL + "/buyer/getCurrentDealsByTrade?user_code=" + userCodestr + "&tradeId=" + SingletonActivity.tradeid;
                System.out.println("URL IS---2: " + GetBuyerPaymentDetailsURL);
                GetBuyerPaymentDetailsAPI(GetBuyerPaymentDetailsURL);
            } else {
                util.dialog(BuyerPaymentDetailsActivity.this, "Please check your internet connection.");
            }

        }

        addpaymentiv.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                if (buydeals.equalsIgnoreCase("0")) {
                    AddPaymentDialog();
                }
                else {
                    Toast.makeText(BuyerPaymentDetailsActivity.this, "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        if (selected == 0) {
            deletepaymentiv.setBackgroundResource(R.mipmap.ic_delete_disabled);
        }


    }

    @Override
    public void onBackPressed() {

        //  finish();
        //  SingletonActivity.splited = "";
        Intent i = new Intent(BuyerPaymentDetailsActivity.this, HomeActivity.class);
        SingletonActivity.fromsellcurrentdeals = false;
        SingletonActivity.frombuycurrentdeals = false;
        SingletonActivity.frombackofsellerdispatch = false;
        SingletonActivity.frombackofbuyerdispatch = true;
        SingletonActivity.splited = "";
        startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void AddPaymentDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BuyerPaymentDetailsActivity.this);
        LayoutInflater inflater = BuyerPaymentDetailsActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.add_payment_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);


        final EditText date_desc_edt = (EditText) dialogView.findViewById(R.id.date_desc_edt);
        final EditText utrchq_num_edt = (EditText) dialogView.findViewById(R.id.utrchq_num_edt);
        final EditText bank_name_edt = (EditText) dialogView.findViewById(R.id.bank_name_edt);
        final EditText amount_edt = (EditText) dialogView.findViewById(R.id.amount_edt);
        final TextView remaininginvoiceamttxtvw = (TextView) dialogView.findViewById(R.id.remaininginvoiceamttxtvw);

        //   final TextView addpaymenttxtvw = (TextView)dialogView.findViewById(R.id.addpaymenttxtvw);
        //   final TextView cancelpaymenttxtvw = (TextView)dialogView.findViewById(R.id.cancelpaymenttxtvw);

        final RelativeLayout sbmtrelative = (RelativeLayout) dialogView.findViewById(R.id.sbmtrelative);
        sbmtrelative.setEnabled(true);
        final RelativeLayout cancelrelative = (RelativeLayout) dialogView.findViewById(R.id.cancelrelative);
        final TextView sbmttext = (TextView) dialogView.findViewById(R.id.sbmttext);
        String exceeded_qty_str = final_exceeded_qty_str;

        if(Double.parseDouble(exceeded_qty_str)>0)
        {
            double final_rate = Double.parseDouble(rate_str);
          //  Toast.makeText(BuyerPaymentDetailsActivity.this,"FINAL RATE: ->"+ final_rate,Toast.LENGTH_SHORT).show();

            double paymentremainingpaystr_double_val = Double.parseDouble(paymentremainingpaystr);
            double exceed_qty_val = Double.parseDouble(exceeded_qty_str);
            String gst_str = SingletonActivity.gst_str;
            double gst_val = Double.parseDouble(gst_str);
            double rate_pmt_val = final_rate;


            double exceeded_gst_rate_val = (rate_pmt_val * exceed_qty_val * gst_val)/100;

            double final_amount_exceed = (rate_pmt_val* exceed_qty_val)+ exceeded_gst_rate_val + paymentremainingpaystr_double_val;
            DecimalFormat df2 = new DecimalFormat("0.00");


            remaininginvoiceamttxtvw.setText("Exceeded Invoice Amount: " + df2.format(final_amount_exceed));
            amount_edt.setText(df2.format(final_amount_exceed));


        }
        else {


            remaininginvoiceamttxtvw.setText("Remaining Invoice Amount: " + paymentremainingpaystr);

            amount_edt.setText(paymentremainingpaystr);
        }

        if (paymentremainingpaystr.equalsIgnoreCase("0.00")) {
            sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
            sbmttext.setTextColor(Color.parseColor("#808080"));
            sbmtrelative.setEnabled(false);
        } else {
            sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
            sbmttext.setTextColor(Color.WHITE);
            sbmtrelative.setEnabled(true);
        }

        date_desc_edt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (v == date_desc_edt) {

                    // Process to get Current Date
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);

                    // Launch Date Picker Dialog
                    DatePickerDialog dialog = new DatePickerDialog(BuyerPaymentDetailsActivity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    // Display Selected date in textbox
                                    paymentdate = dayOfMonth + "/"
                                            + (monthOfYear + 1) + "/" + year;

                                    System.out.println("Payment Date is--" + paymentdate);

                                    date_desc_edt.setText(paymentdate);


                                }

                            }, mDay, mMonth, mYear);

                    DateFormat formatt = new SimpleDateFormat("dd-MM-yyyy");
                    Date date = null;
                    try {
                        date = formatt.parse(SingletonActivity.invoice_gen_date);


                        System.out.println("Date in add payment=====:" + formatt.format(date));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    dialog.getDatePicker().setMinDate(date.getTime());
                    dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
                    dialog.show();
                }

            }
        });


        amount_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {

                    float amount_edt_float_val = Float.parseFloat(amount_edt.getText().toString());
                    float paymentremainingpay_float_val = Float.parseFloat(paymentremainingpaystr);


                    if ((Float.parseFloat(amount_edt.getText().toString()) > paymentremainingpay_float_val)) {


                        Toast.makeText(BuyerPaymentDetailsActivity.this, "Amount can't be greater than the remaining amount.", Toast.LENGTH_SHORT).show();


                        //   addpaymenttxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                        //   addpaymenttxtvw.setEnabled(false);

                        sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                        sbmttext.setTextColor(Color.parseColor("#808080"));
                        sbmtrelative.setEnabled(false);

                    } else {


                        //   addpaymenttxtvw.setTextColor(Color.parseColor("#000000"));
                        //    addpaymenttxtvw.setEnabled(true);

                        sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                        sbmttext.setTextColor(Color.WHITE);
                        sbmtrelative.setEnabled(true);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {


                String temp = s.toString();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) {
                    return;
                }
                if (temp.length() - posDot - 1 > 2) {
                    s.delete(posDot + 3, posDot + 4);
                }
            }
        });


        sbmtrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                paymentchqnum = utrchq_num_edt.getText().toString();
                datestr = date_desc_edt.getText().toString();
                banknamestr = bank_name_edt.getText().toString();
                amtstr = amount_edt.getText().toString();


                boolean invalid = false;

                if (date_desc_edt.getText().toString().equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please Select Date",
                            Toast.LENGTH_SHORT).show();
                } else if (utrchq_num_edt.getText().toString().equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(),
                            "Please Enter UTR/Chq Num", Toast.LENGTH_SHORT)
                            .show();
                } else if (bank_name_edt.getText().toString().equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(),
                            "Please Enter Bank Name", Toast.LENGTH_SHORT)
                            .show();
                } else if (amount_edt.getText().toString().equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(),
                            "Please Enter Amount", Toast.LENGTH_SHORT)
                            .show();
                } else if (invalid == false) {


                    if (NetworkUtility.checkConnectivity(BuyerPaymentDetailsActivity.this)) {

                        sbmtrelative.setEnabled(false);
                        String adddispatchurl = APIName.URL + "/buyer/addPayment";
                        System.out.println("ADD PAYMENT URL IS---" + adddispatchurl);
                        AddPaymentAPI(adddispatchurl);

                    } else {
                        util.dialog(BuyerPaymentDetailsActivity.this, "Please check your internet connection.");
                    }


                }

            }
        });


        cancelrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
        b = dialogBuilder.create();
        b.show();

    }


    private void AddPaymentAPI(String url) {
        pdia = new ProgressDialog(BuyerPaymentDetailsActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(BuyerPaymentDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        System.out.println("RESPONSE OF ADD PAYMENT API IS---" + response);
                        //Toast.makeText(BuyerPaymentDetailsActivity.this,response,Toast.LENGTH_SHORT).show();

                        JSONObject AddPaymentJson = null;

                        try {
                            AddPaymentJson = new JSONObject(response);
                            String statusstr = AddPaymentJson.getString("status");


                            if (statusstr.equalsIgnoreCase("true")) {
                                Toast.makeText(BuyerPaymentDetailsActivity.this, AddPaymentJson.getString("message"), Toast.LENGTH_SHORT).show();
                                b.dismiss();

                                if (SingletonActivity.splited.equalsIgnoreCase("split")) {

                                    if (NetworkUtility.checkConnectivity(BuyerPaymentDetailsActivity.this)) {
                                        String GetCurrentDealsPaymentURL = APIName.URL + "/buyer/getCurrentDealsPayment?user_code=" + userCodestr + "&tradeId=" + SingletonActivity.tradeid + "&trade_type=" + SingletonActivity.buyParent;
                                        System.out.println("URL IS---1: " + GetCurrentDealsPaymentURL);
                                        GetCurrentDealsPaymentAPI(GetCurrentDealsPaymentURL);

                                    } else {
                                        util.dialog(BuyerPaymentDetailsActivity.this, "Please check your internet connection.");
                                    }
                                } else {

                                    if (NetworkUtility.checkConnectivity(BuyerPaymentDetailsActivity.this)) {
                                        String GetBuyerPaymentDetailsURL = APIName.URL + "/buyer/getCurrentDealsByTrade?user_code=" + userCodestr + "&tradeId=" + SingletonActivity.tradeid;
                                        System.out.println("URL IS---2: " + GetBuyerPaymentDetailsURL);
                                        GetBuyerPaymentDetailsAPI(GetBuyerPaymentDetailsURL);
                                    } else {
                                        util.dialog(BuyerPaymentDetailsActivity.this, "Please check your internet connection.");
                                    }

                                }
                            } else {
                                Toast.makeText(BuyerPaymentDetailsActivity.this, AddPaymentJson.getString("message"), Toast.LENGTH_SHORT).show();
                                b.dismiss();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(BuyerPaymentDetailsActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(BuyerPaymentDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            util.dialog(BuyerPaymentDetailsActivity.this, "Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("trade_id", SingletonActivity.tradeid);
                params.put("user_code", userCodestr);
                params.put("mobile_num", mobilenumstr);
                params.put("sell_id", SingletonActivity.sellid);
                params.put("invoice_type", "Invoice");
                //  params.put("invoice_type","");
                params.put("payment_ref_number", paymentchqnum);
                params.put("payment_ref_date", datestr);
                params.put("bank_name", banknamestr);
                params.put("amount", amtstr);


                System.out.println("add payment params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }


    public void deletePayment() {
        String paymentIdStr = "";
        for (int str = 0; str < paymentIdArray.size(); str++) {
            if (paymentIdStr.length() > 0) {
                paymentIdStr = paymentIdStr + "," + paymentIdArray.get(str);
            } else {
                paymentIdStr = paymentIdArray.get(str);
            }
        }
        System.out.println("PAYMENT ID------" + paymentIdStr);

        String paymentcsv = new LinkedHashSet<String>(Arrays.asList(paymentIdStr.split(","))).toString().replaceAll("(^\\[|\\]$)", "").replace(", ", ",");
        System.out.println("PAYMEMT CSV ID------" + paymentcsv);

        String tradeid = null;
        String sellerusercode = null;

        tradeid = SingletonActivity.tradeid;
        sellerusercode = SingletonActivity.sellerusercode;

        if (!paymentcsv.equalsIgnoreCase("")) {

            if (NetworkUtility.checkConnectivity(BuyerPaymentDetailsActivity.this)) {
                String DeletePaymentURL = APIName.URL + "/buyer/deletePayment?trade_id=" + tradeid + "&auto_payment_id=" + paymentcsv + "&seller_user_code=" + sellerusercode;
                System.out.println("DELETE PAYMENT URL IS---" + DeletePaymentURL);
                DeletePaymentAPI(DeletePaymentURL);

            } else {
                util.dialog(BuyerPaymentDetailsActivity.this, "Please check your internet connection.");
            }
        }


    }

    private void GetCurrentDealsPaymentAPI(String url) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(BuyerPaymentDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        tradeidlist.clear();


                        System.out.println("RESPONSE OF GET CURRENT DEALS PAYMENT DETAILS API IS---" + response);


                        JSONObject GetCurrentDealsPaymentJson = null;
                        try {
                            GetCurrentDealsPaymentJson = new JSONObject(response);


                            String statusstr = GetCurrentDealsPaymentJson.getString("status");
                            System.out.println("STATUS OF GET CURRENT DEALS PAYMENT DETAILS JSON IS---" + statusstr);

                            if (statusstr.equalsIgnoreCase("true")) {


                                getCurrentDealsPaymentJsonArray = GetCurrentDealsPaymentJson.getJSONArray("cd_Offer");
                                System.out.println("cd_Offer JSONARRAY payment===" + getCurrentDealsPaymentJsonArray.length());


                                for (int i = 0; i < getCurrentDealsPaymentJsonArray.length(); i++) {
                                    String paymentdetails = getCurrentDealsPaymentJsonArray.getJSONObject(i).getString("payment_details");


                                    if (getCurrentDealsPaymentJsonArray.getJSONObject(i).getString("trade_id").length() != 0) {
                                        tradeidlist.add(getCurrentDealsPaymentJsonArray.getJSONObject(i).getString("trade_id"));

                                        PaymentDetailsJsonArray = getCurrentDealsPaymentJsonArray.getJSONObject(i).getJSONArray("payment_details");


                                        SingletonActivity.paymentDetailsJSONArray = PaymentDetailsJsonArray;
                                        System.out.println("PaymentDetailsJsonArray length in payment===" + getCurrentDealsPaymentJsonArray.getJSONObject(i).getJSONArray("payment_details").length());


                                        if (SingletonActivity.paymentDetailsJSONArray.length() > 0) {
                                            SingletonActivity.invoice_gen_date = PaymentDetailsJsonArray.getJSONObject(0).getString("when");
                                            System.out.println("SingletonActivity.invoice_gen_date===" + SingletonActivity.invoice_gen_date);


                                        }

                                        System.out.println("PaymentDetailsJsonArray 9999a==" + PaymentDetailsJsonArray);
                                        System.out.println("PaymentDetailsJsonArray 9999B==" + getCurrentDealsPaymentJsonArray);
                                        System.out.println("tradeidlist 9999c==" + tradeidlist.size());


                                        CustomAdap customadap = new CustomAdap(BuyerPaymentDetailsActivity.this, getCurrentDealsPaymentJsonArray, PaymentDetailsJsonArray, tradeidlist);
                                        buypaymentdetailsnewlistvw.setAdapter(customadap);


                                    }


                                }


                            } else {
                                empty_payment_iv.setVisibility(View.GONE);
                                nopaymentdetailstxtvw.setVisibility(View.VISIBLE);
                                clickonplusbtntxtvw.setVisibility(View.GONE);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(BuyerPaymentDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            util.dialog(BuyerPaymentDetailsActivity.this, "Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(BuyerPaymentDetailsActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }


    private void DeletePaymentAPI(String url) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(BuyerPaymentDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF DELETE PAYMENT API IS---" + response);


                        JSONObject DeletePaymentJson = null;
                        try {
                            DeletePaymentJson = new JSONObject(response);


                            String statusstr = DeletePaymentJson.getString("status");
                            System.out.println("STATUS OF DELETE PAYMENT API IS---" + statusstr);

                            if (statusstr.equalsIgnoreCase("true")) {

                                Toast.makeText(BuyerPaymentDetailsActivity.this, DeletePaymentJson.getString("message"), Toast.LENGTH_SHORT).show();


                                if (SingletonActivity.splited.equalsIgnoreCase("split")) {

                                    if (NetworkUtility.checkConnectivity(BuyerPaymentDetailsActivity.this)) {
                                        String GetCurrentDealsPaymentURL = APIName.URL + "/buyer/getCurrentDealsPayment?user_code=" + userCodestr + "&tradeId=" + SingletonActivity.tradeid + "&trade_type=" + SingletonActivity.buyParent;
                                        System.out.println("URL IS---1: " + GetCurrentDealsPaymentURL);
                                        GetCurrentDealsPaymentAPI(GetCurrentDealsPaymentURL);

                                    } else {
                                        util.dialog(BuyerPaymentDetailsActivity.this, "Please check your internet connection.");
                                    }
                                } else {

                                    if (NetworkUtility.checkConnectivity(BuyerPaymentDetailsActivity.this)) {
                                        String GetBuyerPaymentDetailsURL = APIName.URL + "/buyer/getCurrentDealsByTrade?user_code=" + userCodestr + "&tradeId=" + SingletonActivity.tradeid;
                                        System.out.println("URL IS---2: " + GetBuyerPaymentDetailsURL);
                                        GetBuyerPaymentDetailsAPI(GetBuyerPaymentDetailsURL);
                                    } else {
                                        util.dialog(BuyerPaymentDetailsActivity.this, "Please check your internet connection.");
                                    }

                                }

                            } else {
                                Toast.makeText(BuyerPaymentDetailsActivity.this, DeletePaymentJson.getString("message"), Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(BuyerPaymentDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            util.dialog(BuyerPaymentDetailsActivity.this, "Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(BuyerPaymentDetailsActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }


    private class PaymentAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        private JSONArray PaymentDetailsJSONArray;



        public PaymentAdap(Context mainActivity, JSONArray PaymentDetailsJSONArray) {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.PaymentDetailsJSONArray = PaymentDetailsJSONArray;


            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            System.out.println("SIZE OF PaymentDetailsJSONArray===" + PaymentDetailsJSONArray.length());
            return PaymentDetailsJSONArray.length();
            //return tradeidlist.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub


            final Holder1 holder1 = new Holder1();

            final View rowView;


                rowView = inflater.inflate(R.layout.buyer_payment_new_row, null);


                holder1.date = (TextView) rowView.findViewById(R.id.date);
                holder1.paymenttypetxt = (TextView) rowView.findViewById(R.id.paymenttypetxt);
                holder1.utrchqnumdesctxtvw = (TextView) rowView.findViewById(R.id.utrchqnumdesctxtvw);
                holder1.banknamedesctxtvw = (TextView) rowView.findViewById(R.id.banknamedesctxtvw);
                holder1.amtdesctxtvw = (TextView) rowView.findViewById(R.id.amtdesctxtvw);
                holder1.checkbox = (CheckBox) rowView.findViewById(R.id.checkbox);
                holder1.disputeiv = (ImageView) rowView.findViewById(R.id.disputeiv);


                try {


                    System.out.println("IN PAYMENT ADAP 2----" + PaymentJsonArray.getJSONObject(position).getString("payment_ref_number"));

                    holder1.date.setText(PaymentJsonArray.getJSONObject(position).getString("payment_ref_date"));
                    holder1.paymenttypetxt.setText("Payment Type: " + PaymentJsonArray.getJSONObject(position).getString("invoice_type"));

                    if(!PaymentJsonArray.getJSONObject(position).getString("payment_ref_number").equalsIgnoreCase("null")) {

                        holder1.utrchqnumdesctxtvw.setText(PaymentJsonArray.getJSONObject(position).getString("payment_ref_number"));
                    }
                    else
                    {
                        holder1.utrchqnumdesctxtvw.setText("--");
                    }

                    if(!PaymentJsonArray.getJSONObject(position).getString("bank_name").equalsIgnoreCase("null")) {
                        holder1.banknamedesctxtvw.setText(PaymentJsonArray.getJSONObject(position).getString("bank_name"));
                    }
                    else
                    {
                        holder1.banknamedesctxtvw.setText("--");
                    }
                    holder1.amtdesctxtvw.setText(PaymentJsonArray.getJSONObject(position).getString("amount"));


                    try {


                        payment_resolved_jsonarray = getCurrentDealsPaymentJsonArray.getJSONObject(0).getJSONArray("payment_resolved");


                        if (payment_resolved_jsonarray.length() > 0) {

                            holder1.disputeiv.setVisibility(View.VISIBLE);

                            for(int i = 0 ; i < payment_resolved_jsonarray.length();i++) {
                                if(PaymentJsonArray.getJSONObject(position).getString("auto_payment_id").equalsIgnoreCase(payment_resolved_jsonarray.getJSONObject(i).getString("auto_payment_id"))) {
                                    holder1.disputeiv.setVisibility(View.VISIBLE);
                                    if (payment_resolved_jsonarray.getJSONObject(i).getString("flag").equalsIgnoreCase("1")) {
                                        holder1.disputeiv.setBackgroundResource(R.mipmap.dispute_resolved_orange_info);
                                    } else {
                                        holder1.disputeiv.setBackgroundResource(R.mipmap.dispute_raised_exclamatory);
                                    }

                                    final int disputePos = i ;

                                    holder1.disputeiv.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            try {
                                                String dispute_raised_comment = payment_resolved_jsonarray.getJSONObject(disputePos).getString("dispute_comment");
                                                String dispute_resolution_comment = payment_resolved_jsonarray.getJSONObject(disputePos).getString("dispute_resolution_comment");
                                                String pos = Integer.toString(position);

                                                DisputeDetailsPaymentDialog(dispute_raised_comment, dispute_resolution_comment, pos);

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }


                                        }
                                    });
                                }
                                /*else
                                {
                                    holder1.disputeiv.setVisibility(View.GONE);
                                }*/
                            }





                        } else {
                            holder1.disputeiv.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    holder1.checkbox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder1.checkbox.setEnabled(true);
                            holder1.checkbox.setBackgroundResource(R.mipmap.ic_checkbox);


                            try {
                                if (holder1.checkbox.isChecked() == false) {

                                    deletepaymentiv.setBackgroundResource(R.mipmap.ic_delete_disabled);

                                    holder1.checkbox.setBackgroundResource(R.mipmap.ic_unchecked);

                                    holder1.checkbox.setChecked(false);
                                    selected = selected - 1;

                                    for (int j = 0; j < paymentIdArray.size(); j++) {


                                        if (paymentIdArray.get(j).equalsIgnoreCase(PaymentJsonArray.getJSONObject(position).getString("auto_payment_id"))) {
                                            System.out.println("DISPATCH ID BEFORE REMOVING----" + paymentIdArray);
                                            paymentIdArray.remove(j);
                                            System.out.println("DISPATCH ID AFTER REMOVING----" + paymentIdArray);
                                        }

                                    }


                                } else {

                                    deletepaymentiv.setBackgroundResource(R.mipmap.ic_delete_dispatch);

                             /*   if(selected == 0)
                                {
                                    deletepaymentiv.setBackgroundResource(R.mipmap.ic_delete_disabled);
                                }
                                else
                                {
                                    deletepaymentiv.setBackgroundResource(R.mipmap.ic_delete_dispatch);
                                }*/


                                    selected = selected + 1;

                                    paymentIdArray.add(PaymentJsonArray.getJSONObject(position).getString("auto_payment_id"));
                                    holder1.checkbox.setChecked(true);


                                }

                                deletepaymentiv.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        if (buydeals.equalsIgnoreCase("0")) {
                                            deletePayment();
                                        } else {
                                            Toast.makeText(BuyerPaymentDetailsActivity.this, "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }


                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    if (PaymentJsonArray.getJSONObject(position).getString("payment_ref_number") == null || PaymentJsonArray.getJSONObject(position).getString("bank_name") == null)

                    {
                        holder1.checkbox.setEnabled(false);

                    } else {
                        holder1.checkbox.setEnabled(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                try {
                    if (PaymentJsonArray.getJSONObject(position).getString("ack_status").equalsIgnoreCase("1")) {
                        holder1.checkbox.setEnabled(false);
                    } else {
                        holder1.checkbox.setEnabled(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return rowView;

            }


        }

        public void DisputeDetailsPaymentDialog(String dispute_raised_comment, String dispute_resolution_comment, String pos) {
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BuyerPaymentDetailsActivity.this);
            LayoutInflater inflater = BuyerPaymentDetailsActivity.this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.seller_dispatch_dispute_dialog, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);


            TextView disputeraiseddesctxtvw = (TextView) dialogView.findViewById(R.id.disputeraiseddesctxtvw);
            TextView resolvedcommenttxtvw = (TextView) dialogView.findViewById(R.id.resolvedcommenttxtvw);
            TextView resolvedcommentdesctxtvw = (TextView) dialogView.findViewById(R.id.resolvedcommentdesctxtvw);

            TextView closedisputetv = (TextView) dialogView.findViewById(R.id.closetxtvw);
            RelativeLayout closerelative = (RelativeLayout) dialogView.findViewById(R.id.closerelative);

            disputeraiseddesctxtvw.setText(dispute_raised_comment);


            int position = Integer.parseInt(pos);
            try {
             if (payment_resolved_jsonarray.getJSONObject(position).getString("flag").equalsIgnoreCase("1"))  {
                    resolvedcommenttxtvw.setVisibility(View.VISIBLE);
                    resolvedcommentdesctxtvw.setVisibility(View.VISIBLE);
                    resolvedcommentdesctxtvw.setText(dispute_resolution_comment);
                } else {
                    resolvedcommenttxtvw.setVisibility(View.GONE);
                    resolvedcommentdesctxtvw.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            b = dialogBuilder.create();
            b.show();


            closerelative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    b.dismiss();
                }
            });


        }

        private class CustomAdap extends BaseAdapter {
            private ArrayList<String> data;
            private Context c;
            private LayoutInflater inflater = null;
            private JSONArray getCurrentDealsPaymentJsonArray;
            private ArrayList<String> tradeidlist;
            private JSONArray BuyerPaymentDetailsJSONArray;


            // public CustomAdap(Context mainActivity)
            public CustomAdap(Context mainActivity, JSONArray getCurrentDealsPaymentJsonArray, JSONArray BuyerPaymentDetailsJSONArray, ArrayList<String> tradeidlist) {
                // TODO Auto-generated constructor stub
                //  this.data = leavetypelist;
                this.c = mainActivity;
                this.getCurrentDealsPaymentJsonArray = getCurrentDealsPaymentJsonArray;
                this.BuyerPaymentDetailsJSONArray = BuyerPaymentDetailsJSONArray;
                this.tradeidlist = tradeidlist;
                inflater = (LayoutInflater) c
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            }

            @Override
            public int getCount() {
                // TODO Auto-generated method stub


                System.out.println("SIZE OF CUSTOM ADAP===" + tradeidlist.size());

                return tradeidlist.size();

                // return getCurrentDealsPaymentJsonArray.length();
            }

            @Override
            public Object getItem(int position) {
                // TODO Auto-generated method stub
                return position;
            }

            @Override
            public long getItemId(int position) {
                // TODO Auto-generated method stub
                return position;
            }

            @Override
            public View getView(final int position, View convertView,
                                ViewGroup parent) {
                // TODO Auto-generated method stub


                final Holder holder = new Holder();

                final View rowView;


                rowView = inflater.inflate(R.layout.buyer_payment_details, null);


                holder.tradeidtxt = (TextView) rowView.findViewById(R.id.tradeidtxt);
                holder.paymentdetailslistvw = (ListView) rowView.findViewById(R.id.paymentdetailslistvw);


                holder.nodatarelative = (RelativeLayout) rowView.findViewById(R.id.nodatarelative);


                try {

                    if (getCurrentDealsPaymentJsonArray.getJSONObject(position).getString("trade_id").equals("")) {
                        rowView.setVisibility(View.GONE);
                        holder.nodatarelative.setVisibility(View.GONE);
                    }

                    holder.tradeidtxt.setText("Trade ID: " + getCurrentDealsPaymentJsonArray.getJSONObject(position).getString("trade_id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (SingletonActivity.buyParent.equalsIgnoreCase("parent")) {
                    System.out.println("In payment parent===");

                    addpaymentiv.setVisibility(View.GONE);
                    deletepaymentiv.setVisibility(View.GONE);

                } else {
                    System.out.println("In payment child===");

                    addpaymentiv.setVisibility(View.VISIBLE);
                    deletepaymentiv.setVisibility(View.VISIBLE);

                }


                try {

                    PaymentJsonArray = getCurrentDealsPaymentJsonArray.getJSONObject(position).getJSONArray("payment_details");


                    // Toast.makeText(BuyerPaymentDetailsActivity.this,"payment_resolved_jsonarray=="+ payment_resolved_jsonarray,Toast.LENGTH_SHORT).show();
                    System.out.println("IN PaymentJsonArray----" + PaymentJsonArray);


                    if (PaymentJsonArray.length() == 0) {
                        holder.nodatarelative.setVisibility(View.VISIBLE);
                        holder.paymentdetailslistvw.setVisibility(View.GONE);
                    } else {
                        holder.nodatarelative.setVisibility(View.GONE);
                        holder.paymentdetailslistvw.setVisibility(View.VISIBLE);


                        PaymentAdap paymentadap = new PaymentAdap(BuyerPaymentDetailsActivity.this, PaymentJsonArray);
                        holder.paymentdetailslistvw.setAdapter(paymentadap);



/*

                    //  if ([[appD.defaults objectForKey:@"buyPaymentTradeId"] isEqualToString:[NSString stringWithFormat:@"%@",[[self.buyPaymentChildDetailsArray objectAtIndex:indexPath.section]objectForKey:@"trade_id"]]]) {

                    if (holder.tradeidtxt.getText().toString().equalsIgnoreCase(SingletonActivity.tradeid)) {

                        //   if (![[self buyDealsChildPaymentIdValidNullString:[NSString stringWithFormat:@"%@",[[[[self.buyPaymentChildDetailsArray objectAtIndex:indexPath.section] objectForKey:@"payment_details"] objectAtIndex:indexPath.row] objectForKey:@"flag"]]] isEqualToString:@""]) {
                        if (!PaymentJsonArray.getJSONObject(0).getString("flag").equalsIgnoreCase(null)) {
                       */
/* UIButton *disputeInfoBtn = [[UIButton alloc]initWithFrame:CGRectMake(3*headerWidth+5, 2,35, 35)];
                        disputeInfoBtn.tag = (indexPath.section * 1000) + indexPath.row;
                        [disputeInfoBtn addTarget:self action:@selector(dispInfoChildPaymentBtnOnClick:) forControlEvents:UIControlEventTouchUpInside];*//*


                            holder.disputeiv.setVisibility(View.VISIBLE);

                            if (PaymentJsonArray.getJSONObject(0).getString("flag").equalsIgnoreCase("0")) {

                                holder.disputeiv.setBackgroundResource(R.mipmap.ic_info_red);

                                // [disputeInfoBtn setImage:[UIImage imageNamed:@"icn_info_red"] forState:UIControlStateNormal];
                            }else{
                                holder.disputeiv.setBackgroundResource(R.mipmap.ic_info);
                                //  [disputeInfoBtn setImage:[UIImage imageNamed:@"icn_info_green"] forState:UIControlStateNormal];
                            }

                      */
/*  if ([[NSString stringWithFormat:@"%@",[[[[self.buyPaymentChildDetailsArray objectAtIndex:indexPath.section] objectForKey:@"dispatch_details"] objectAtIndex:indexPath.row] objectForKey:@"flag"]] isEqualToString:@"0"]) {
                            [disputeInfoBtn setImage:[UIImage imageNamed:@"icn_info_red"] forState:UIControlStateNormal];
                        }else{
                            [disputeInfoBtn setImage:[UIImage imageNamed:@"icn_info_green"] forState:UIControlStateNormal];
                        }*//*

                            // [headerView addSubview:disputeInfoBtn];
                        }else{

                            holder.checkbox.setVisibility(View.VISIBLE);
                        */
/*UIButton *selectionBackBtn = [[UIButton alloc]initWithFrame:CGRectMake(3*headerWidth+5, 2,35, 35)];
                        [headerView addSubview:selectionBackBtn];
                        [selectionBackBtn setImage:[UIImage imageNamed:@"icn_checkbox2_selected"] forState:UIControlStateSelected];
                        [selectionBackBtn setImage:[UIImage imageNamed:@"icn_checkbox2"] forState:UIControlStateNormal];
                        [headerView addSubview: selectionBackBtn];
                        selectionBackBtn.tag = (indexPath.section * 1000) + indexPath.row;
                        [selectionBackBtn addTarget:self action:@selector(selectionBackBtnClicked:) forControlEvents:UIControlEventTouchUpInside];*//*

                        }
                    }
*/


                    }

             /*   holder.disputeiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        DisputeDetailsPaymentDialog();
                    }
                });





                holder.checkbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.checkbox.setEnabled(true);
                        holder.checkbox.setBackgroundResource(R.mipmap.ic_checkbox);

                        // Toast.makeText(context,"Checkbox Clicked",Toast.LENGTH_SHORT).show();



                        try {
                            if(holder.checkbox.isChecked()==false)
                            {



                                holder.checkbox.setBackgroundResource(R.mipmap.ic_unchecked);

                                holder.checkbox.setChecked(false);
                                selected = selected - 1;

                                for (int j = 0 ; j < paymentIdArray.size() ; j++) {



                                    if (paymentIdArray.get(j).equalsIgnoreCase(PaymentJsonArray.getJSONObject(0).getString("auto_payment_id"))) {
                                        System.out.println("DISPATCH ID BEFORE REMOVING----"+ paymentIdArray);
                                        paymentIdArray.remove(j);
                                        System.out.println("DISPATCH ID AFTER REMOVING----"+ paymentIdArray);
                                    }

                                }


                            }

                            else
                            {






                                selected = selected+1;

                                paymentIdArray.add(PaymentJsonArray.getJSONObject(0).getString("auto_payment_id"));
                                holder.checkbox.setChecked(true);



                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                });
*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return rowView;

            }


        }


        public class Holder {
            TextView tradeidtxt;
            ImageView addpaymentiv, empty_payment_iv, deletepaymentiv;
            RelativeLayout nodatarelative, rel;
            TextView nopaymentdetailstxtvw, clickonplusbtntxtvw;
            ListView paymentdetailslistvw;


        }

        public class Holder1 {
            TextView date, paymenttypetxt, utrchqnumdesctxtvw, banknamedesctxtvw, amtdesctxtvw;
            CheckBox checkbox;
            ImageView disputeiv;


        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.action_settings) {

                sessionManager.logoutUser(mobile_num, fcm_id);
                return true;
            }

            return super.onOptionsItemSelected(item);
        }


        private void GetBuyerPaymentDetailsAPI(String url) {

            HurlStack hurlStack = new HurlStack() {
                @Override
                protected HttpURLConnection createConnection(java.net.URL url)
                        throws IOException {
                    HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                            .createConnection(url);
                    try {
                        httpsURLConnection
                                .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(BuyerPaymentDetailsActivity.this));
                        // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return httpsURLConnection;
                }
            };

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {


                            System.out.println("RESPONSE OF GET BUYER PAYMENT DETAILS API IS---" + response);


                            JSONObject GetPaymentDetailsJson = null;
                            try {
                                GetPaymentDetailsJson = new JSONObject(response);


                          /*  String statusstr = GetPaymentDetailsJson.getString("status");
                            System.out.println("STATUS OF GET BUYER PAYMENT DETAILS JSON IS---" + statusstr);*/
/*
                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                JSONArray GetPaymentDetailsJsonArray = GetPaymentDetailsJson.getJSONArray("cd_Offer");
                                SingletonActivity.SellerpaymentDetailsJSONArray = GetPaymentDetailsJsonArray.getJSONObject(0).getJSONArray("payment_details");


                                customadap = new BuyerPaymentDetailsActivity.CustomAdap(BuyerPaymentDetailsActivity.this,SingletonActivity.SellerpaymentDetailsJSONArray);
                                sellerpaymentlistvw.setAdapter(customadap);

                            }
                            else
                            {
                                Toast.makeText(BuyerPaymentDetailsActivity.this,GetPaymentDetailsJson.getString("message"),Toast.LENGTH_SHORT).show();
                            }*/
                                JSONObject GetCurrentDealsPaymentJson = null;

                                GetCurrentDealsPaymentJson = new JSONObject(response);


                                String statussstr = GetCurrentDealsPaymentJson.getString("status");
                                System.out.println("STATUS OF GET CURRENT DEALS PAYMENT DETAILS JSON IS---" + statussstr);

                                if (statussstr.equalsIgnoreCase("true")) {


                                    nopaymentdetailstxtvw.setVisibility(View.GONE);
                                    buypaymentdetailsnewlistvw.setVisibility(View.VISIBLE);
                                    //   empty_payment_iv.setVisibility(View.GONE);

                                    getCurrentDealsPaymentJsonArray = GetCurrentDealsPaymentJson.getJSONArray("cd_Offer");
                                    System.out.println("cd_Offer JSONARRAY payment===" + getCurrentDealsPaymentJsonArray.length());

                                    SingletonActivity.invoice_gen_date = getCurrentDealsPaymentJsonArray.getJSONObject(0).getString("when");
                                    paymentremainingpaystr = getCurrentDealsPaymentJsonArray.getJSONObject(0).getString("payment_remain_pay");
                                    rate_str = getCurrentDealsPaymentJsonArray.getJSONObject(0).getString("rate");
                                    quoted_rate_str = getCurrentDealsPaymentJsonArray.getJSONObject(0).getString("quoted_rate");
                                    israte = getCurrentDealsPaymentJsonArray.getJSONObject(0).getString("israte");
                                    final_exceeded_qty_str =  getCurrentDealsPaymentJsonArray.getJSONObject(0).getString("final_exceed_quantity");
                                    if (!israte.equalsIgnoreCase("1")) {
                                        if (!quoted_rate_str.equalsIgnoreCase("0.00")){


                                            double price = Double.parseDouble(rate_str) - Double.parseDouble(quoted_rate_str);

                                            rate_str = Double.toString(price);

                                        }else{



                                            rate_str = getCurrentDealsPaymentJsonArray.getJSONObject(0).getString("rate");
                                        }
                                    }else{


                                        rate_str = getCurrentDealsPaymentJsonArray.getJSONObject(0).getString("rate");
                                    }


                                    System.out.println("SingletonActivity.invoice_gen_date===" + SingletonActivity.invoice_gen_date);

                                    for (int i = 0; i < getCurrentDealsPaymentJsonArray.length(); i++) {
                                        String paymentdetails = getCurrentDealsPaymentJsonArray.getJSONObject(i).getString("payment_details");


                                        System.out.println("PaymentDetailsJsonArray 991==" + paymentdetails.length());

                                        if (paymentdetails.length() > 5) {

                                            if (getCurrentDealsPaymentJsonArray.getJSONObject(i).getString("trade_id").length() != 0) {
                                                tradeidlist.add(getCurrentDealsPaymentJsonArray.getJSONObject(i).getString("trade_id"));
                                                PaymentDetailsJsonArray = getCurrentDealsPaymentJsonArray.getJSONObject(i).getJSONArray("payment_details");
                                                System.out.println("PaymentDetailsJsonArray 9999==" + PaymentDetailsJsonArray);

                                                SingletonActivity.paymentDetailsJSONArray = PaymentDetailsJsonArray;

                                       /* if(SingletonActivity.paymentDetailsJSONArray.length()==0)
                                        {
                                            empty_payment_iv.setVisibility(View.VISIBLE);
                                            nopaymentdetailstxtvw.setVisibility(View.VISIBLE);
                                            clickonplusbtntxtvw.setVisibility(View.VISIBLE);

                                        }
                                        else
                                        {
                                            empty_payment_iv.setVisibility(View.GONE);
                                            nopaymentdetailstxtvw.setVisibility(View.GONE);
                                            clickonplusbtntxtvw.setVisibility(View.GONE);




                                        }*/

                                                CustomAdap customadap = new CustomAdap(BuyerPaymentDetailsActivity.this, getCurrentDealsPaymentJsonArray, PaymentDetailsJsonArray, tradeidlist);
                                                buypaymentdetailsnewlistvw.setAdapter(customadap);

                                            }
                                        } else {
                                            System.out.println("Loop of else==1");

                                            //   empty_payment_iv.setVisibility(View.VISIBLE);
                                            nopaymentdetailstxtvw.setVisibility(View.VISIBLE);
                                            clickonplusbtntxtvw.setVisibility(View.GONE);
                                            buypaymentdetailsnewlistvw.setVisibility(View.INVISIBLE);
                                            deletepaymentiv.setBackgroundResource(R.mipmap.ic_delete_disabled);
                                        }


                                    }


                                } else {
                                    System.out.println("Loop of else==2");

                                    //  empty_payment_iv.setVisibility(View.VISIBLE);
                                    nopaymentdetailstxtvw.setVisibility(View.VISIBLE);
                                    clickonplusbtntxtvw.setVisibility(View.GONE);

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                            // pdia.dismiss();

                            VolleyLog.d("TAG", "Error: " + error.getMessage());
                            NetworkResponse networkResponse = error.networkResponse;
                            System.out.println("network RESPONSE CODE--------" + networkResponse);

                            if (networkResponse != null) {
                                int respCode = networkResponse.statusCode;


                                //  String respcodestr = Integer.toString(respCode);
                                if (respCode == 404) {
                                    String respData = new String(networkResponse.data);
                                    System.out.println("RESPONSE DATA--------" + respData);
                                    try {
                                        JSONObject jsonObject = new JSONObject(respData);
                                        String statusstr = jsonObject.getString("status");
                                        String messagestr = jsonObject.getString("message");
                                        if (statusstr.equalsIgnoreCase("false")) {

                                            // noliveoffertxt.setVisibility(View.VISIBLE);
                                            //lv.setVisibility(View.GONE);

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Toast.makeText(BuyerPaymentDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                util.dialog(BuyerPaymentDetailsActivity.this, "Some Error Occured,Please try after some time");
                            }

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();


                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    return params;
                }

            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            RequestQueue requestQueue = Volley.newRequestQueue(BuyerPaymentDetailsActivity.this,hurlStack);
            requestQueue.add(stringRequest);
        }



}
