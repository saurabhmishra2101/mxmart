package com.smtc.mxmart;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smtc.mxmart.Fragment.BuyCurrentDealsFragment;
import com.smtc.mxmart.Fragment.SellCurrentDealsFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by 10161 on 8/21/2017.
 */

public class BuyerDispatchActivity extends AppCompatActivity {

    ImageView backiv;
    SessionManager sessionManager;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    UtilsDialog util = new UtilsDialog();

    TextView clickonplusbtntxtvw;
    RelativeLayout seller_dispatch_layout;
    ImageView adddispatchiv;
    String tradeid,sellid,invoice_amount_str,dispatch_date_str,truck_num_str,transport_name_str,mobile_num_str,invoice_num_str,invoice_due_date_str,invoice_qty_str;
    ProgressDialog pdia;
    EditText invoice_amount_edt;
    String userCodestr,mobilenumstr,dispatchdate,invoiceduedate;
    int mYear,mMonth,mDay;
    int mYear1,mMonth1,mDay1;

    CustomExpandableListAdapter expandableListAdapter;
    static List<String> expandableListTitle;
    static WeakHashMap<String, List<String>> expandableListDetail;
    AlertDialog b;
    ImageView deletedispatchiv;
    int selected = 0;
    ArrayList<String> dispatchIdArray = new ArrayList<String>();
    JSONArray  BuyerCurrentDealsByTradeJsonArray;
    TextView notification_total_count;
    String commentStr,tradeStr;
    TextView tradeidtxt,todays_offer_count,new_enquiry_count,seller_accepted_count,seller_rejected_count,sp_generated_count,buyer_rejected_count;
    int index;
    ListView buyer_dispatch_listview;
    ArrayList<Integer> dispatchlengthal = new ArrayList<Integer>();
    ArrayList<String> tradeidlist = new ArrayList<String>();
    JSONArray DispatchDetailsJsonArray,dispatch_resolved_jsonarray;
    RelativeLayout nodatarelative;
    ImageView empty_dispatch_iv;
    TextView nodispatchdetailstxtvw;
    TextView materialreceipttxtvw;
    String mobile_num,fcm_id,buydeals;
    boolean isTradeIdMatched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buyer_dispatch_new);

        nodatarelative = (RelativeLayout) findViewById(R.id.nodatarelative);
        notification_total_count = (TextView)findViewById(R.id.notification_total_count);
        materialreceipttxtvw = (TextView)findViewById(R.id.materialreceipttxtvw);

        clickonplusbtntxtvw = (TextView)findViewById(R.id.clickonplusbtntxtvw);

        seller_dispatch_layout = (RelativeLayout)findViewById(R.id.seller_dispatch_layout);
        adddispatchiv = (ImageView)findViewById(R.id.adddispatchiv);
        deletedispatchiv = (ImageView)findViewById(R.id.deletedispatchiv);
        empty_dispatch_iv = (ImageView)findViewById(R.id.empty_dispatch_iv);
        nodispatchdetailstxtvw = (TextView)findViewById(R.id.nodispatchdetailstxtvw);


        buyer_dispatch_listview = (ListView) findViewById(R.id.buycurrentdealsnewlistvw);



        sessionManager = new SessionManager(getApplicationContext());

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        mobile_num = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id",null);
        buydeals = prefs.getString("buydeals",null);


        Intent i = getIntent();
        SingletonActivity.splited = i.getStringExtra("splitted");
        SingletonActivity.buyParent = i.getStringExtra("buyParent");

        System.out.println("SingletonActivity.splited in buyer dispatch activity---"+ SingletonActivity.splited);

        System.out.println("trade type in buyer deals dispatch---"+ SingletonActivity.buyParent);

       /* Toolbar toolbar = (Toolbar)findViewById(R.id.sellerdispatchtoolbar);
        setSupportActionBar(toolbar);*/

        backiv = (ImageView)findViewById(R.id.back_dispatch);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                    Intent i = new Intent(BuyerDispatchActivity.this,HomeActivity.class);

                SingletonActivity.fromsellcurrentdeals = false;
                SingletonActivity.frombuycurrentdeals = false;
                SingletonActivity.frombackofsellerdispatch = false;
                SingletonActivity.frombackofbuyerdispatch = true;
                    startActivity(i);


            }
        });

        System.out.println("SingletonActivity.splited in Buyer Dispatch from sell current deals---"+ SingletonActivity.splited);

        if(SingletonActivity.splited.equalsIgnoreCase("not split")) {
          if(NetworkUtility.checkConnectivity(BuyerDispatchActivity.this)){
            String getCurrentDealsByTradeURL = APIName.URL+"/buyer/getCurrentDealsByTrade?user_code="+SingletonActivity.usercode+"&tradeId="+SingletonActivity.tradeid;
            System.out.println("GET CURRENT DEALS BY TRADE BUYER URL IS---"+ getCurrentDealsByTradeURL);
            GetCurrentDealsByTradeAPI(getCurrentDealsByTradeURL);

        }
        else{
            util.dialog(BuyerDispatchActivity.this, "Please check your internet connection.");
        }

        }
        if(SingletonActivity.splited.equalsIgnoreCase("split")) {
            if (NetworkUtility.checkConnectivity(BuyerDispatchActivity.this)) {
                String getCurrentDealsDispatchURL = APIName.URL + "/buyer/getCurrentDealsDispatch?user_code=" + SingletonActivity.usercode + "&tradeId=" + SingletonActivity.tradeid + "&trade_type=" + SingletonActivity.buyParent;
                System.out.println("GET CURRENT DEALS DISPATCH URL IS---" + getCurrentDealsDispatchURL);
                getCurrentDealsDispatchAPI(getCurrentDealsDispatchURL);

            } else {
                util.dialog(BuyerDispatchActivity.this, "Please check your internet connection.");
            }
        }

    }


    @Override
    public void onBackPressed() {


        Intent i = new Intent(BuyerDispatchActivity.this,HomeActivity.class);


        SingletonActivity.fromsellcurrentdeals = false;
        SingletonActivity.frombuycurrentdeals = false;
        SingletonActivity.frombackofsellerdispatch = false;
        SingletonActivity.frombackofbuyerdispatch = true;
        startActivity(i);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {



            sessionManager.logoutUser(mobile_num,fcm_id);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void RaiseDispatchDisputeAPI(String url,final String tradeid,final String autodispatchid,final String buyerusercode,final String buyermobilenum,final String sellerusercode,final String sellermobilenum,final String comment) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(BuyerDispatchActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF ADD DISPATCH DISPUTE API IS---" + response);



                        JSONObject AddDispatchDisputeJson = null;
                        try {
                            AddDispatchDisputeJson = new JSONObject(response);


                            String statusstr = AddDispatchDisputeJson.getString("status");
                            System.out.println("STATUS OF ADD DISPATCH DISPUTE API IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {


                                Toast.makeText(BuyerDispatchActivity.this,AddDispatchDisputeJson.getString("message"),Toast.LENGTH_SHORT).show();
                                b.dismiss();

                               /* Intent i = new Intent(BuyerDispatchActivity.this,BuyerDispatchActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
*/
                                if(SingletonActivity.splited.equalsIgnoreCase("not split")) {
                                    if(NetworkUtility.checkConnectivity(BuyerDispatchActivity.this)){
                                        String getCurrentDealsByTradeURL = APIName.URL+"/buyer/getCurrentDealsByTrade?user_code="+SingletonActivity.usercode+"&tradeId="+SingletonActivity.tradeid;
                                        System.out.println("GET CURRENT DEALS BY TRADE BUYER URL IS---"+ getCurrentDealsByTradeURL);
                                        GetCurrentDealsByTradeAPI(getCurrentDealsByTradeURL);

                                    }
                                    else{
                                        util.dialog(BuyerDispatchActivity.this, "Please check your internet connection.");
                                    }

                                }
                                if(SingletonActivity.splited.equalsIgnoreCase("split")) {
                                    if (NetworkUtility.checkConnectivity(BuyerDispatchActivity.this)) {
                                        String getCurrentDealsDispatchURL = APIName.URL + "/buyer/getCurrentDealsDispatch?user_code=" + SingletonActivity.usercode + "&tradeId=" + SingletonActivity.tradeid + "&trade_type=" + SingletonActivity.buyParent;
                                        System.out.println("GET CURRENT DEALS DISPATCH URL IS---" + getCurrentDealsDispatchURL);
                                        getCurrentDealsDispatchAPI(getCurrentDealsDispatchURL);

                                    } else {
                                        util.dialog(BuyerDispatchActivity.this, "Please check your internet connection.");
                                    }
                                }


                            }
                            else
                            {
                                Toast.makeText(BuyerDispatchActivity.this,AddDispatchDisputeJson.getString("message"),Toast.LENGTH_SHORT).show();

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(BuyerDispatchActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(BuyerDispatchActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("trade_id",tradeid);
                params.put("auto_dispatch_id",autodispatchid);
                params.put("seller_user_code",sellerusercode);
                params.put("seller_mobile_num",sellermobilenum);
                params.put("buyer_user_code",buyerusercode);
                params.put("buyer_mobile_num",buyermobilenum);
                params.put("dispute_comment",comment);


                System.out.println("add dispatch dispute params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(BuyerDispatchActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }


    private void AcknowledgeDispatchAPI(String url,final String tradeid,final String sellid,final String autodispatchid,final String buyerusercode,final String buyermobilenum,final String sellerusercode,final String sellermobilenum) {


        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(BuyerDispatchActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF ACKNOWLEDGE DISPATCH API IS---" + response);



                        JSONObject AcknowledgeDispatchJson = null;
                        try {
                            AcknowledgeDispatchJson = new JSONObject(response);


                            String statusstr = AcknowledgeDispatchJson.getString("status");
                            System.out.println("STATUS OF GET CURRENT DEALS BY TRADE BUYER API IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {


                                Toast.makeText(BuyerDispatchActivity.this,AcknowledgeDispatchJson.getString("message"),Toast.LENGTH_SHORT).show();

                             /*   Intent i = new Intent(BuyerDispatchActivity.this,BuyerDispatchActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);*/

                                if(SingletonActivity.splited.equalsIgnoreCase("not split")) {
                                    if(NetworkUtility.checkConnectivity(BuyerDispatchActivity.this)){
                                        String getCurrentDealsByTradeURL = APIName.URL+"/buyer/getCurrentDealsByTrade?user_code="+SingletonActivity.usercode+"&tradeId="+SingletonActivity.tradeid;
                                        System.out.println("GET CURRENT DEALS BY TRADE BUYER URL IS---"+ getCurrentDealsByTradeURL);
                                        GetCurrentDealsByTradeAPI(getCurrentDealsByTradeURL);

                                    }
                                    else{
                                        util.dialog(BuyerDispatchActivity.this, "Please check your internet connection.");
                                    }

                                }
                                if(SingletonActivity.splited.equalsIgnoreCase("split")) {
                                    if (NetworkUtility.checkConnectivity(BuyerDispatchActivity.this)) {
                                        String getCurrentDealsDispatchURL = APIName.URL + "/buyer/getCurrentDealsDispatch?user_code=" + SingletonActivity.usercode + "&tradeId=" + SingletonActivity.tradeid + "&trade_type=" + SingletonActivity.buyParent;
                                        System.out.println("GET CURRENT DEALS DISPATCH URL IS---" + getCurrentDealsDispatchURL);
                                        getCurrentDealsDispatchAPI(getCurrentDealsDispatchURL);

                                    } else {
                                        util.dialog(BuyerDispatchActivity.this, "Please check your internet connection.");
                                    }
                                }
                            }
                            else
                            {
                                Toast.makeText(BuyerDispatchActivity.this,AcknowledgeDispatchJson.getString("message"),Toast.LENGTH_SHORT).show();

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(BuyerDispatchActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(BuyerDispatchActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("trade_id",tradeid);
                params.put("sell_id",sellid);
                params.put("auto_dispatch_id",autodispatchid);
                params.put("seller_user_code",sellerusercode);
                params.put("seller_mobile_num",sellermobilenum);
                params.put("buyer_user_code",buyerusercode);
                params.put("buyer_mobile_num",buyermobilenum);



                System.out.println("acknowledge dispute params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(BuyerDispatchActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }


    private void GetCurrentDealsByTradeAPI(String url) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(BuyerDispatchActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        tradeidlist.clear();


                        System.out.println("RESPONSE OF GetCurrentDealsByTradeAPI IS---" + response);



                        JSONObject GetCurrentDealsByTradeJSON = null;
                        try {
                            GetCurrentDealsByTradeJSON = new JSONObject(response);


                            String statusstr = GetCurrentDealsByTradeJSON.getString("status");
                            System.out.println("STATUS OF GET CURRENT DEALS BY TRADE BUYER API IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {



                                BuyerCurrentDealsByTradeJsonArray = GetCurrentDealsByTradeJSON.getJSONArray("cd_Offer");
                                System.out.println("cd_Offer JSONARRAY length---" + BuyerCurrentDealsByTradeJsonArray.length());

                                for(int i = 0; i < BuyerCurrentDealsByTradeJsonArray.length(); i++) {
                                    String dispatchdetails = BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("dispatch_details");
                                    System.out.println("TRADE ID LENGTH==" + BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("trade_id").length());

                                    if(BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("trade_id").length()!=0)
                                    {
                                        tradeidlist.add(BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("trade_id"));
                                    }

                                    System.out.println("dispatchdetails length IS---" + dispatchdetails.length());




                                    if(dispatchdetails.length()>5) {


                                        DispatchDetailsJsonArray = BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getJSONArray("dispatch_details");
                                        System.out.println("auto_dispatch_id DispatchDetailsJsonArray IS---" + DispatchDetailsJsonArray);



                                        CustomAdap customadap = new CustomAdap(BuyerDispatchActivity.this,BuyerCurrentDealsByTradeJsonArray,DispatchDetailsJsonArray,tradeidlist);
                                        buyer_dispatch_listview.setAdapter(customadap);
                                    }



                                    else {
                                        empty_dispatch_iv.setVisibility(View.VISIBLE);
                                        nodispatchdetailstxtvw.setVisibility(View.VISIBLE);
                                        buyer_dispatch_listview.setVisibility(View.GONE);
                                        materialreceipttxtvw.setVisibility(View.VISIBLE);
                                    }


                                }

                                System.out.println("cd_Offer JSONARRAY length---" + BuyerCurrentDealsByTradeJsonArray.length());





                            }
                            else {
                                empty_dispatch_iv.setVisibility(View.VISIBLE);
                                nodispatchdetailstxtvw.setVisibility(View.VISIBLE);
                                buyer_dispatch_listview.setVisibility(View.GONE);
                                materialreceipttxtvw.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(BuyerDispatchActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(BuyerDispatchActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();



                System.out.println("get seller current enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(BuyerDispatchActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }



    private void getCurrentDealsDispatchAPI(String url) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(BuyerDispatchActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        tradeidlist.clear();


                        System.out.println("RESPONSE OF getCurrentDealsDispatchAPI IS---" + response);



                        JSONObject getCurrentDealsDispatchAPIJSON = null;
                        try {
                            getCurrentDealsDispatchAPIJSON = new JSONObject(response);


                            String statusstr = getCurrentDealsDispatchAPIJSON.getString("status");
                            System.out.println("STATUS OF GET CURRENT DEALS BY TRADE BUYER API IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {



                               BuyerCurrentDealsByTradeJsonArray = getCurrentDealsDispatchAPIJSON.getJSONArray("cd_Offer");
                                System.out.println("cd_Offer JSONARRAY length---" + BuyerCurrentDealsByTradeJsonArray.length());

                                for(int i = 0; i < BuyerCurrentDealsByTradeJsonArray.length(); i++) {
                                    String dispatchdetails = BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("dispatch_details");
                                    System.out.println("TRADE ID LENGTH==" + BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("trade_id").length());

                                    if (!BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("trade_id").equalsIgnoreCase("")) {

                                        System.out.println("Trade id Length is--->>>" + BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("trade_id").length());

                                        tradeidlist.add(BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("trade_id"));
                                    }

                                    if(dispatchdetails.length()>2) {


                                        DispatchDetailsJsonArray = BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getJSONArray("dispatch_details");
                                        System.out.println("auto_dispatch_id DispatchDetailsJsonArray IS---" + DispatchDetailsJsonArray);

                                    }

                                   if (BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("trade_id").length() != 0) {

                                        try {
                                            System.out.println("trade id 1========" + BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("trade_id"));
                                            System.out.println("trade id 2========" + SingletonActivity.tradeid);


                                            if (BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("trade_id").equalsIgnoreCase(SingletonActivity.tradeid))
                                            //   if(holder.tradeidtxt.getText().toString().equalsIgnoreCase(SingletonActivity.tradeid))
                                            {
                                               isTradeIdMatched = true;
                                               // BuyerCurrentDealsByTradeJsonArray = sortJsonArray(BuyerCurrentDealsByTradeJsonArray);
                                                System.out.println("trade id matched---"+ BuyerCurrentDealsByTradeJsonArray);


                                            }
                                            else
                                            {

                                                System.out.println("trade id not matched 1---" + BuyerCurrentDealsByTradeJsonArray.getJSONObject(i).getString("trade_id"));
                                                System.out.println("trade id not matched 2---" + SingletonActivity.tradeid);

                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                   }


                                  /*
                                    System.out.println("isTradeIdMatched value in customadap==="+ isTradeIdMatched);
                                    if(isTradeIdMatched == true)
                                    {
                                        Collections.reverse(tradeidlist);
                                        isTradeIdMatched = false;
                                    }


                                    System.out.println("Trade id list is--->>>" + tradeidlist);*/


                                }

                                CustomAdap customadap = new CustomAdap(BuyerDispatchActivity.this, BuyerCurrentDealsByTradeJsonArray, DispatchDetailsJsonArray, tradeidlist);
                                buyer_dispatch_listview.setAdapter(customadap);

                                System.out.println("cd_Offer JSONARRAY length---" + BuyerCurrentDealsByTradeJsonArray.length());





                            }
                            else {
                                empty_dispatch_iv.setVisibility(View.VISIBLE);
                                nodispatchdetailstxtvw.setVisibility(View.VISIBLE);
                                buyer_dispatch_listview.setVisibility(View.GONE);
                                materialreceipttxtvw.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(BuyerDispatchActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(BuyerDispatchActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("get seller current enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(BuyerDispatchActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, CoordinatorLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }


    public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

        private Context context;
        JSONArray DispatchDetailsJsonArray;
        JSONArray BuyerCurrentDealsByTradeJsonArray;
        JSONArray CertificateFileJsonArray;
        String sellid;
        String buyerusercode,buyermobilenum,sellerusercode,sellermobilenum;

        public CustomExpandableListAdapter(Context context,JSONArray  CertificateFileJsonArray,JSONArray DispatchDetailsJsonArray,JSONArray BuyerCurrentDealsByTradeJsonArray,String sellid,String buyerusercode,String buyermobilenum,String sellerusercode,String sellermobilenum) {
            this.context = context;
            this.DispatchDetailsJsonArray = DispatchDetailsJsonArray;
            this.BuyerCurrentDealsByTradeJsonArray = BuyerCurrentDealsByTradeJsonArray;
            this.sellid = sellid;
            this.buyerusercode = buyerusercode;
            this.buyermobilenum = buyermobilenum;
            this.sellerusercode = sellerusercode;
            this.sellermobilenum = sellermobilenum;
            this.CertificateFileJsonArray = CertificateFileJsonArray;

        }

        @Override
        public Object getChild(int listPosition, int expandedListPosition) {

            System.out.println("IN GETCHILD listPosition---"+ listPosition);

            System.out.println("IN GETCHILD expandedListPosition---"+ expandedListPosition);

            return listPosition;




        }

        @Override
        public long getChildId(int listPosition, int expandedListPosition) {

            System.out.println("IN GETCHILDID listPosition---"+ listPosition);

            return listPosition;
        }

        @Override
        public View getChildView(final int listPosition, final int expandedListPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            //   final String expandedListText = (String) getChild(listPosition, expandedListPosition);
            System.out.println("IN GETCHILDVIEW---");

            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) this.context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.buyer_dispatch_details_child_row, null);
            }


            TextView trucknodesctxtvw = (TextView) convertView
                    .findViewById(R.id.trucknodesctxtvw);

            TextView transportnamedesctxtvw = (TextView) convertView
                    .findViewById(R.id.transportnamedesctxtvw);

            TextView qtydesctxtvw = (TextView) convertView
                    .findViewById(R.id.qtydesctxtvw);

            TextView mobilenodesctxtvw = (TextView) convertView
                    .findViewById(R.id.mobilenodesctxtvw);

            TextView duedatedesctxtvw = (TextView) convertView
                    .findViewById(R.id.duedatedesctxtvw);

            TextView invoiceamountdesctxtvw = (TextView) convertView
                    .findViewById(R.id.invoiceamountdesctxtvw);

            final TextView acktxtvw = (TextView) convertView
                    .findViewById(R.id.acktxtvw);

            TextView disputetxtvw = (TextView) convertView
                    .findViewById(R.id.disputetxtvw);


            final ImageView viewdociv1 = (ImageView) convertView
                    .findViewById(R.id.viewdociv1);
            final ImageView viewdociv2 = (ImageView) convertView
                    .findViewById(R.id.viewdociv2);
            final ImageView viewdociv3 = (ImageView) convertView
                    .findViewById(R.id.viewdociv3);
            final ImageView viewdociv4 = (ImageView) convertView
                    .findViewById(R.id.viewdociv4);
            final ImageView viewdociv5 = (ImageView) convertView
                    .findViewById(R.id.viewdociv5);
            final ImageView viewdociv6 = (ImageView) convertView
                    .findViewById(R.id.viewdociv6);

            viewdociv1.setVisibility(View.GONE);
            viewdociv2.setVisibility(View.GONE);
            viewdociv3.setVisibility(View.GONE);
            viewdociv4.setVisibility(View.GONE);
            viewdociv5.setVisibility(View.GONE);
            viewdociv6.setVisibility(View.GONE);


            try {
                trucknodesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("truck_number"));
                transportnamedesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("transport_name"));
                qtydesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_qty"));
                mobilenodesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("dispatch_mobile"));
                duedatedesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_due_date"));
                invoiceamountdesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_amount"));

                final JSONArray certificate_file_jsonarray = BuyerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("certificate_file");

                System.out.println("certificate_file_jsonarray length=="+certificate_file_jsonarray.length());

                for (int i = 0; i < CertificateFileJsonArray.length(); i++) {

                    for (int j = 0; j < DispatchDetailsJsonArray.length(); j++) {


                        if (CertificateFileJsonArray.getJSONObject(i).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {

                            if (certificate_file_jsonarray.length() == 1) {
                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    viewdociv1.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv1.setImageBitmap(bmp);

                                                }


                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                            }


                            if (certificate_file_jsonarray.length() == 2) {

                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    viewdociv1.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    System.out.println("IN T1==b");
                                                    viewdociv1.setImageBitmap(bmp);

                                                }


                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                    viewdociv2.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv2.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                            }

                            if (certificate_file_jsonarray.length() == 3) {
                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    viewdociv1.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv1.setImageBitmap(bmp);

                                                }


                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                    viewdociv2.setVisibility(View.VISIBLE);

                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv2.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                    viewdociv3.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv3.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                            }
                            if (certificate_file_jsonarray.length() == 4) {

                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    viewdociv1.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv1.setImageBitmap(bmp);

                                                }


                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                    viewdociv2.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv2.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                    viewdociv3.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv3.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                    viewdociv4.setVisibility(View.VISIBLE);

                                    final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv4.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv4.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv4.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv4.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }
                            }

                            if (certificate_file_jsonarray.length() == 5) {


                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    viewdociv1.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv1.setImageBitmap(bmp);

                                                }


                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                    viewdociv2.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv2.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                    viewdociv3.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv3.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                    viewdociv4.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv4.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv4.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv4.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv4.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file").length() > 0) {

                                    viewdociv5.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv5.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv5.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv5.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv5.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                            }
                            if (certificate_file_jsonarray.length() == 6)

                            {


                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    viewdociv1.setVisibility(View.VISIBLE);

                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    System.out.println("IN T1==b");
                                                    viewdociv1.setImageBitmap(bmp);

                                                }


                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                    viewdociv2.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv2.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                    viewdociv3.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv3.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                    viewdociv4.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv4.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv4.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv4.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv4.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file").length() > 0) {

                                    viewdociv5.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv5.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv5.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv5.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv5.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }

                                if (certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file").length() > 0) {

                                    viewdociv6.setVisibility(View.VISIBLE);


                                    final String urlstr = certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file");
                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                    final URL url = new URL(urlstr);

                                    Thread thread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                //Your code goes here

                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                System.out.println("EXTENSION IS===" + extension);
                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                    viewdociv6.setBackgroundResource(R.mipmap.pdf);

                                                } else if (extension.equalsIgnoreCase(".doc")) {
                                                    viewdociv6.setBackgroundResource(R.mipmap.doc2);
                                                } else if (extension.equalsIgnoreCase(".docx")) {
                                                    viewdociv6.setBackgroundResource(R.mipmap.docx);
                                                } else {


                                                    viewdociv6.setImageBitmap(bmp);
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    thread.start();


                                }
                            }


                        }
                    }
                }

            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (JSONException e1) {
                e1.printStackTrace();
            }




            if(SingletonActivity.buyParent.equalsIgnoreCase("Parent"))
            {
                acktxtvw.setVisibility(View.GONE);
                disputetxtvw.setVisibility(View.GONE);
            }
            else {
                acktxtvw.setVisibility(View.VISIBLE);
                disputetxtvw.setVisibility(View.VISIBLE);
            }
            System.out.println("list position ---"+ listPosition);
            System.out.println("expandedListPosition position ---"+ expandedListPosition);
            System.out.println("Dispatch Details JSONArray---"+ DispatchDetailsJsonArray);

            try {
                if((DispatchDetailsJsonArray.getJSONObject(listPosition).getString("ack_status").equalsIgnoreCase("1"))||(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("is_active").equalsIgnoreCase("9")))
                {
                    acktxtvw.setEnabled(false);
                    disputetxtvw.setEnabled(false);
                    acktxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                    disputetxtvw.setTextColor(Color.parseColor("#d3d3d3"));


                    System.out.println("list position ---if"+ listPosition);
                }
                else
                {
                    acktxtvw.setEnabled(true);
                    disputetxtvw.setEnabled(true);
                    acktxtvw.setTextColor(Color.parseColor("#008000"));
                    disputetxtvw.setTextColor(Color.parseColor("#ff0000"));


                    System.out.println("list position ---else"+ listPosition);

                }
                } catch (JSONException e) {
                e.printStackTrace();
            }


            try {
                trucknodesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("truck_number"));
                transportnamedesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("transport_name"));
                qtydesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_qty"));
                mobilenodesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("dispatch_mobile"));
                duedatedesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_due_date"));
                invoiceamountdesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_amount"));

                final String tradeid = DispatchDetailsJsonArray.getJSONObject(listPosition).getString("trade_id");
              //  final String sellid = BuyerCurrentDealsByTradeJsonArray.getJSONObject(listPosition).getString("sell_id");
                final String autodispatchid = DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id");



             //   Toast.makeText(BuyerDispatchActivity.this, "buyerusercode:"+buyerusercode+",buyermobilenum:"+ buyermobilenum + ",sellerusercode:"+ sellerusercode +",sellermobilenum:"+ sellermobilenum, Toast.LENGTH_SHORT).show();


                if(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("trade_id").equalsIgnoreCase(SingletonActivity.tradeid))
                {

                    System.out.println("trade id matched---");
                    acktxtvw.setVisibility(View.VISIBLE);
                    disputetxtvw.setVisibility(View.VISIBLE);
                }
                else {

                    System.out.println("trade id not matched---");
                    acktxtvw.setVisibility(View.GONE);
                    disputetxtvw.setVisibility(View.GONE);
                }

                final JSONArray certificate_file_jsonarray;

                certificate_file_jsonarray = BuyerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("certificate_file");
                viewdociv1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(BuyerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });

                viewdociv2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(BuyerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });

                viewdociv3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(BuyerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });

                viewdociv4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(BuyerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });

                viewdociv5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(BuyerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });

                viewdociv6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(BuyerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });


                disputetxtvw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (buydeals.equalsIgnoreCase("0")) {
                            DisputeDialog(tradeid, autodispatchid, buyerusercode, buyermobilenum, sellerusercode, sellermobilenum);
                        }
                        else
                        {
                            Toast.makeText(BuyerDispatchActivity.this, "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                        }



                    }
                });

                acktxtvw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (buydeals.equalsIgnoreCase("0")) {


                            if (NetworkUtility.checkConnectivity(BuyerDispatchActivity.this)) {
                                acktxtvw.setEnabled(false);

                                String AcknowledgeDispatchURL = APIName.URL + "/buyer/ackdispatch";
                                System.out.println("ACKNOWLEDGE DISPATCH URL IS---" + AcknowledgeDispatchURL);
                                AcknowledgeDispatchAPI(AcknowledgeDispatchURL, tradeid, sellid, autodispatchid, buyerusercode, buyermobilenum, sellerusercode, sellermobilenum);

                            } else {
                                util.dialog(BuyerDispatchActivity.this, "Please check your internet connection.");
                            }
                        }
                        else
                        {
                            Toast.makeText(BuyerDispatchActivity.this, "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                        }


                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                if((DispatchDetailsJsonArray.getJSONObject(listPosition).getString("action").equalsIgnoreCase("1"))&&(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_number").equalsIgnoreCase("")))
                {
                    convertView.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return convertView;
        }

        @Override
        public int getChildrenCount(int listPosition) {

            System.out.println("IN GETCHILDRENCOUNT---"+ listPosition);
            return 1;
           // return DispatchDetailsJsonArray.length();

        }

        @Override
        public Object getGroup(int listPosition) {

            System.out.println("IN GETGROUP---");
            return 1;

        }

        @Override
        public int getGroupCount() {

            System.out.println("IN GETGROUPCOUNT---");
            return DispatchDetailsJsonArray.length();
        }

        @Override
        public long getGroupId(int listPosition) {
            return listPosition;
        }

        @Override
        public View getGroupView(final int listPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
//            String listTitle = (String) getGroup(listPosition);

            System.out.println("IN GETGROUPVIEW---");

            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) this.context.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.buyer_dispatch_details_title_row, null);
            }

//            Typeface roboto_medium_font = Typeface.createFromAsset(context.getAssets(),  "roboto/Roboto-Medium.ttf");

            TextView listTitleTextView = (TextView) convertView
                    .findViewById(R.id.date);
            TextView invoicenumtxt = (TextView) convertView
                    .findViewById(R.id.invoicenumtxt);
            ImageView disputeiv = (ImageView) convertView
                    .findViewById(R.id.disputeiv);



            final  CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.checkbox);

            try {

                // Toast.makeText(BuyerPaymentDetailsActivity.this,"payment_resolved_jsonarray RESOLUTION LENGTH ==" + payment_resolved_jsonarray.length(),Toast.LENGTH_SHORT).show();

                // String payment_resolved_str = payment_resolved_jsonarray.getJSONObject(position).getString("payment_resolved");
                dispatch_resolved_jsonarray = BuyerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("dispatch_resolved");

                if (dispatch_resolved_jsonarray.length() > 0) {

                    disputeiv.setVisibility(View.VISIBLE);

                    //Toast.makeText(BuyerPaymentDetailsActivity.this,"DISPUTE RESOLUTION LENGTH ==" +payment_resolved_jsonarray.getJSONObject(0).getString("dispute_resolution_comment").length(),Toast.LENGTH_SHORT).show();

                    for(int i = 0 ; i < dispatch_resolved_jsonarray.length();i++) {
                        if(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id").equalsIgnoreCase(dispatch_resolved_jsonarray.getJSONObject(i).getString("auto_dispatch_id"))) {
                            disputeiv.setVisibility(View.VISIBLE);
                            if (dispatch_resolved_jsonarray.getJSONObject(i).getString("flag").equalsIgnoreCase("1")) {
                                disputeiv.setBackgroundResource(R.mipmap.dispute_resolved_orange_info);
                            } else {
                                disputeiv.setBackgroundResource(R.mipmap.dispute_raised_exclamatory);
                            }

                            final int disputePos = i ;

                            disputeiv.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    try {
                                        String dispute_raised_comment = dispatch_resolved_jsonarray.getJSONObject(disputePos).getString("dispute_comment");
                                        String dispute_resolution_comment = dispatch_resolved_jsonarray.getJSONObject(disputePos).getString("dispute_resolution_comment");
                                        String pos = Integer.toString(listPosition);

                                        DisputeDetailsDispatchDialog(dispute_raised_comment, dispute_resolution_comment, pos);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            });
                        }
                       /* else
                        {
                            disputeiv.setVisibility(View.GONE);
                        }*/
                    }


                } else {
                    disputeiv.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }



            try {
                listTitleTextView.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("dispatch_date"));
                invoicenumtxt.setText("Invoice No. "+DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_number"));

                // listTitleTextView.setTypeface(roboto_medium_font);
                // invoicenumtxt.setTypeface(roboto_medium_font);

                if((DispatchDetailsJsonArray.getJSONObject(listPosition).getString("ack_status").equalsIgnoreCase("1"))||(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("is_active").equalsIgnoreCase("9")))
                {
                    checkBox.setEnabled(false);
                    checkBox.setBackgroundResource(R.mipmap.ic_checkbox_disabled);

                }
                else
                {
                    checkBox.setEnabled(true);
                    checkBox.setBackgroundResource(R.mipmap.ic_unchecked);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkBox.setEnabled(true);
                    checkBox.setBackgroundResource(R.mipmap.ic_checkbox);

                    // Toast.makeText(context,"Checkbox Clicked",Toast.LENGTH_SHORT).show();

                    try {
                        if(checkBox.isChecked()==false)
                        {



                            checkBox.setBackgroundResource(R.mipmap.ic_unchecked);

                            checkBox.setChecked(false);
                            selected = selected - 1;

                            for (int j = 0 ; j < dispatchIdArray.size() ; j++) {



                                if (dispatchIdArray.get(j).equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                    System.out.println("DISPATCH ID BEFORE REMOVING----"+ dispatchIdArray);
                                    dispatchIdArray.remove(j);
                                    System.out.println("DISPATCH ID AFTER REMOVING----"+ dispatchIdArray);
                                }

                            }


                        }

                        else
                        {



                            selected = selected+1;

                            dispatchIdArray.add(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"));
                            checkBox.setChecked(true);



                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });


            try {
                if((DispatchDetailsJsonArray.getJSONObject(listPosition).getString("action").equalsIgnoreCase("1"))&&(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_number").equalsIgnoreCase("")))
                {
                    convertView.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            System.out.println("IN HASSTABLEIDS---");
            return false;
        }

        @Override
        public boolean isChildSelectable(int listPosition, int expandedListPosition) {
            System.out.println("IN ISCHILDSELECTABLE---");
            return true;
        }
    }

    public void DisputeDetailsDispatchDialog(String  dispute_raised_comment,String dispute_resolution_comment,String pos){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BuyerDispatchActivity.this);
        LayoutInflater inflater = BuyerDispatchActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.seller_dispatch_dispute_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);


        TextView disputeraiseddesctxtvw = (TextView)dialogView.findViewById(R.id.disputeraiseddesctxtvw);
        TextView resolvedcommenttxtvw = (TextView)dialogView.findViewById(R.id.resolvedcommenttxtvw);
        TextView resolvedcommentdesctxtvw = (TextView)dialogView.findViewById(R.id.resolvedcommentdesctxtvw);

        TextView closedisputetv = (TextView)dialogView.findViewById(R.id.closetxtvw);
        RelativeLayout closerelative = (RelativeLayout)dialogView.findViewById(R.id.closerelative);

        disputeraiseddesctxtvw.setText(dispute_raised_comment);



        int position = Integer.parseInt(pos);
        try {
            if (dispatch_resolved_jsonarray.getJSONObject(position).getString("flag").equalsIgnoreCase("1"))  {
                resolvedcommenttxtvw.setVisibility(View.VISIBLE);
                resolvedcommentdesctxtvw.setVisibility(View.VISIBLE);
                resolvedcommentdesctxtvw.setText(dispute_resolution_comment);
            } else {
                resolvedcommenttxtvw.setVisibility(View.GONE);
                resolvedcommentdesctxtvw.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }




        closerelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });





        b = dialogBuilder.create();
        b.show();


        closerelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }



    public void DisputeDialog(final String tradeid,final String autodispatchid,final String buyerusercode,final String buyermobilenum,final String sellerusercode,final String sellermobilenum){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BuyerDispatchActivity.this);
        LayoutInflater inflater = BuyerDispatchActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dispute_dialog, null);
        dialogBuilder.setView(dialogView);


        //final EditText dispatch_date_edt = (EditText)dialogView.findViewById(R.id.dispatch_date_edt);



        final EditText commentdesctxt = (EditText) dialogView.findViewById(R.id.commentdesctxt);
       final TextView addtxtvw = (TextView)dialogView.findViewById(R.id.addtxtvw);
        TextView canceltxtvw = (TextView)dialogView.findViewById(R.id.canceltxtvw);


        addtxtvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String comment = commentdesctxt.getText().toString();



                boolean invalid = false;

                if (comment.equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please fill the comment",
                            Toast.LENGTH_SHORT).show();
                }


                else if (invalid == false)
                {



                    if(NetworkUtility.checkConnectivity(BuyerDispatchActivity.this)){

                        addtxtvw.setEnabled(false);

                        String raisedispatchdisputeurl = APIName.URL+"/buyer/addDispatchDispute";
                        System.out.println("RAISE DISPATCH DISPUTE URL IS---"+ raisedispatchdisputeurl);
                        RaiseDispatchDisputeAPI(raisedispatchdisputeurl,tradeid,autodispatchid,buyerusercode,buyermobilenum,sellerusercode,sellermobilenum,comment);

                    }
                    else{
                        util.dialog(BuyerDispatchActivity.this, "Please check your internet connection.");
                    }


                }

            }
        });



        b = dialogBuilder.create();
        b.show();


        canceltxtvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }

    private class CustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        private JSONArray BuyerCurrentDealsByTradeJsonArray;
        private JSONArray DispatchDetailsJsonArray;
        private ArrayList<String> tradeidlist;




        // public CustomAdap(Context mainActivity)
        public CustomAdap(Context mainActivity,JSONArray BuyerCurrentDealsByTradeJsonArray,JSONArray DispatchDetailsJsonArray,ArrayList<String> tradeidlist)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.BuyerCurrentDealsByTradeJsonArray = BuyerCurrentDealsByTradeJsonArray;
            this.DispatchDetailsJsonArray = DispatchDetailsJsonArray;

            this.tradeidlist = tradeidlist;
                inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return tradeidlist.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub


            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub

            final Holder holder ;

            holder = new Holder();
            View rowView = null;




            if(rowView==null){
               /* LayoutInflater inflater =(LayoutInflater)
                        ((Activity)context).getSystemService(Context.LAYOUT_INFLATER_SERVICE);*/
                rowView = inflater.inflate(R.layout.buyer_dispatch, null);


                holder.tradeidtxt = (TextView)rowView.findViewById(R.id.tradeidtxt);
                holder.buyerdispatchexpandablelistvw = (ExpandableListView)rowView.findViewById(R.id.buyerdispatchexpandablelistvw);
                holder.empty_dispatch_iv = (ImageView)rowView.findViewById(R.id.empty_dispatch_iv);
                holder.nodispatchdetailstxtvw = (TextView)rowView.findViewById(R.id.nodispatchdetailstxtvw);
                holder.nodatarelative = (RelativeLayout) rowView.findViewById(R.id.nodatarelative);




                try {

                    holder.tradeidtxt.setText("Trade ID: " + BuyerCurrentDealsByTradeJsonArray.getJSONObject(position).getString("trade_id"));
                    String dispatchdetails =  BuyerCurrentDealsByTradeJsonArray.getJSONObject(position).getString("dispatch_details");
                //    String paymentdetails =   BuyerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("payment_details");
                    System.out.println("DISPATCH DETAILS JSONARRAY==="+ BuyerCurrentDealsByTradeJsonArray);


                    System.out.println("TRADE ID VALUE IS==="+ BuyerCurrentDealsByTradeJsonArray.getJSONObject(position).getString("trade_id"));




                    if(dispatchdetails.length()>5)
                    {

                    expandableListTitle = new ArrayList<String>();
                    expandableListDetail = new WeakHashMap<String, List<String>>();

                    JSONArray DispatchDetailsJsonArray =  BuyerCurrentDealsByTradeJsonArray.getJSONObject(position).getJSONArray("dispatch_details");
                    System.out.println("DispatchDetailsJsonArray length IS---" + DispatchDetailsJsonArray.length());

                        JSONArray CertificateFileJsonArray = BuyerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("certificate_file");


                        String sell_id = BuyerCurrentDealsByTradeJsonArray.getJSONObject(position).getString("sell_id");
                   String buyerusercode = BuyerCurrentDealsByTradeJsonArray.getJSONObject(position).getString("user_code");
                   String buyermobilenum = BuyerCurrentDealsByTradeJsonArray.getJSONObject(position).getString("mobile_num");
                   String sellerusercode =  BuyerCurrentDealsByTradeJsonArray.getJSONObject(position).getString("seller_user_code");
                   String sellermobilenum = BuyerCurrentDealsByTradeJsonArray.getJSONObject(position).getString("seller_mobile_num");


                        expandableListAdapter = new CustomExpandableListAdapter(BuyerDispatchActivity.this,CertificateFileJsonArray,DispatchDetailsJsonArray,BuyerCurrentDealsByTradeJsonArray,sell_id,buyerusercode,buyermobilenum,sellerusercode,sellermobilenum);
                    holder.buyerdispatchexpandablelistvw.setAdapter(expandableListAdapter);
                  //  setListViewHeightBasedOnChildren(holder.buyerdispatchexpandablelistvw);

                    holder.buyerdispatchexpandablelistvw.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                        @Override
                        public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                            System.out.println("ON GROUP CLICKED--------"+ groupPosition);
                            // parent.expandGroup(groupPosition);
                            return false;
                        }
                    });



                    holder.buyerdispatchexpandablelistvw.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                        @Override
                        public boolean onChildClick(ExpandableListView parent, View v,
                                                    int groupPosition, int childPosition, long id) {


                            return false;
                        }
                    });

                    holder.buyerdispatchexpandablelistvw.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                        int previousGroup = -1;

                        @Override
                        public void onGroupExpand(int groupPosition) {
                           if(groupPosition != previousGroup)
                                holder.buyerdispatchexpandablelistvw.collapseGroup(previousGroup);
                            previousGroup = groupPosition;

                         /*   int len = expandableListAdapter.getGroupCount();

                            for (int i = 0; i < len; i++) {
                                if (i != groupPosition) {
                                    holder.buyerdispatchexpandablelistvw.collapseGroup(i);
                                }
                            }
*/

                        }
                    });


                }
                                else
                                {

                                    holder.tradeidtxt.setText("Trade ID: " + BuyerCurrentDealsByTradeJsonArray.getJSONObject(position).getString("trade_id"));
                                    holder.nodatarelative.setVisibility(View.VISIBLE);                                  //  holder.buyerdispatchexpandablelistvw.setVisibility(View.GONE);
                                    /*holder.empty_dispatch_iv.setVisibility(View.VISIBLE);
                                    holder.nodispatchdetailstxtvw.setVisibility(View.VISIBLE);
                                    holder.buyerdispatchexpandablelistvw.setVisibility(View.GONE);*/
                                }



                } catch (JSONException e) {
                    e.printStackTrace();
                }



                rowView.setTag(holder);
            }
           // holder = (Holder) rowView.getTag();






            return rowView;

        }


    }

    public static JSONArray sortJsonArray(JSONArray array) {
        final List<JSONObject> jsons = new ArrayList<JSONObject>();
        for (int i = 0; i < array.length(); i++) {
            try {
                jsons.add(array.getJSONObject(i));
                Collections.sort(jsons, new Comparator<JSONObject>() {
                    @Override
                    public int compare(JSONObject lhs, JSONObject rhs) {
                        String lid = "";
                        String rid = "";
                        try {
                             lid = lhs.getString("trade_id");
                             rid = rhs.getString("trade_id");

                            // Here you could parse string id to integer and then compare.


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        return lid.compareTo(rid);
                    }

                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return new JSONArray(jsons);
    }

    public class Holder {
        TextView tradeidtxt,nodispatchdetailstxtvw;
        ExpandableListView buyerdispatchexpandablelistvw;
        ImageView empty_dispatch_iv;
        RelativeLayout nodatarelative;

    }


}
