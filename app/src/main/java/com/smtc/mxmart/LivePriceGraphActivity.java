package com.smtc.mxmart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;

import android.net.ParseException;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.PointsGraphSeries;
import com.jjoe64.graphview.series.Series;
import com.smtc.mxmart.Fragment.SellCurrentEnquiriesFragment;
import com.smtc.mxmart.Fragment.SellTodaysOfferFragment;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

public class LivePriceGraphActivity extends AppCompatActivity {

    private final Handler mHandler = new Handler();
    private Runnable mTimer;
    private double graphLastXValue = 5d;
    private LineGraphSeries<DataPoint> mSeries;
    ArrayList<String> citylist = new ArrayList<>();
    GraphView graph,graph1,graph2;
    Spinner location_spinner;
    UtilsDialog util = new UtilsDialog();
    ProgressDialog pdia;
    TextView headertitle,highestratetitle,highestratedatetitle,lowestratetitle,lowestratedatetitle;
    TextView highestratetitle1,highestratedatetitle1,lowestratetitle1,lowestratedatetitle1;
    TextView highestratetitle2,highestratedatetitle2,lowestratetitle2,lowestratedatetitle2;
    ImageView backicon;
    JSONArray xAxisJSONArray,yAxisJSONArray,xAxisJSONArray1,yAxisJSONArray1,xAxisJSONArray2,yAxisJSONArray2;
    LineGraphSeries<DataPoint> series2;
    ArrayList<String> xaxislist = new ArrayList<>();
    ArrayList<String> yaxislist = new ArrayList<>();
    ArrayList<String> xaxislist1 = new ArrayList<>();
    ArrayList<String> yaxislist1 = new ArrayList<>();
    ArrayList<String> xaxislist2 = new ArrayList<>();
    ArrayList<String> yaxislist2 = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_price_graph);

        location_spinner = (Spinner)
                findViewById(R.id.location_spinner);
        headertitle = (TextView)findViewById(R.id.headertitle);
        backicon = (ImageView)findViewById(R.id.backicon);
        highestratetitle = (TextView)findViewById(R.id.highestratetitle);
        highestratedatetitle = (TextView)findViewById(R.id.highestratedatetitle);
        lowestratetitle = (TextView)findViewById(R.id.lowestratetitle);
        lowestratedatetitle = (TextView)findViewById(R.id.lowestratedatetitle);

        highestratetitle1 = (TextView)findViewById(R.id.highestratetitle1);
        highestratedatetitle1 = (TextView)findViewById(R.id.highestratedatetitle1);
        lowestratetitle1 = (TextView)findViewById(R.id.lowestratetitle1);
        lowestratedatetitle1 = (TextView)findViewById(R.id.lowestratedatetitle1);

        highestratetitle2 = (TextView)findViewById(R.id.highestratetitle2);
        highestratedatetitle2 = (TextView)findViewById(R.id.highestratedatetitle2);
        lowestratetitle2 = (TextView)findViewById(R.id.lowestratetitle2);
        lowestratedatetitle2 = (TextView)findViewById(R.id.lowestratedatetitle2);

        graph = (GraphView) findViewById(R.id.graph);
        graph1 = (GraphView) findViewById(R.id.graph1);
        graph2 = (GraphView) findViewById(R.id.graph2);



        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LivePriceGraphActivity.this,HomeActivity.class);
                SingletonActivity.fromaddnewoffer = false;
                SingletonActivity.fromsellerhistorywebview = false;
                SingletonActivity.frombuyerhistorywebview = false;
                SingletonActivity.fromupdatenewoffer = false;
                SingletonActivity.isNewEnquiryClicked = false;
                SingletonActivity.fromviewlivetrade = false;
                SingletonActivity.frombackofbuyerdispatch = false;
                SingletonActivity.frombackofsellerdispatch = false;
                SingletonActivity.frombackofbuyerpayment = false;
                SingletonActivity.fromselltodaysoffer = false;
                SingletonActivity.fromlivepricegraph = true;

                startActivity(i);

            }
        });


       // SingletonActivity.graphtitle = "Ingots";

          if(SingletonActivity.graphtitle.equalsIgnoreCase("Ingots")) {
              headertitle.setText(SingletonActivity.graphtitle+"-MS(Mild Steel)");
          }

        if(SingletonActivity.graphtitle.equalsIgnoreCase("Billets")) {
            headertitle.setText(SingletonActivity.graphtitle+"-MS(Mild Steel)");
        }

        if(SingletonActivity.graphtitle.equalsIgnoreCase("Sponge Iron")) {
            headertitle.setText(SingletonActivity.graphtitle+" (DRCLO)");
        }

        if(SingletonActivity.graphtitle.equalsIgnoreCase("Pellets")) {
            headertitle.setText(SingletonActivity.graphtitle+"-Uniform");
        }


      if(NetworkUtility.checkConnectivity(LivePriceGraphActivity.this)){
            String getCityForGraphUrl = APIName.URL+"/home/getCityForGraph";
            System.out.println("GET CITY FOR GRAPH URL IS---"+ getCityForGraphUrl);
            getCityForGraphAPI(getCityForGraphUrl);

        }
        else{
            util.dialog(LivePriceGraphActivity.this, "Please check your internet connection.");
        }








      }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(LivePriceGraphActivity.this,HomeActivity.class);
        SingletonActivity.fromaddnewoffer = false;
        SingletonActivity.fromsellerhistorywebview = false;
        SingletonActivity.frombuyerhistorywebview = false;
        SingletonActivity.fromupdatenewoffer = false;
        SingletonActivity.isNewEnquiryClicked = false;
        SingletonActivity.fromviewlivetrade = false;
        SingletonActivity.frombackofbuyerdispatch = false;
        SingletonActivity.frombackofsellerdispatch = false;
        SingletonActivity.frombackofbuyerpayment = false;
        SingletonActivity.fromselltodaysoffer = false;
        SingletonActivity.fromlivepricegraph = true;
        startActivity(i);



    }

    private void getCityForGraphAPI(String url) {

      /*  pdia = new ProgressDialog(LivePriceGraphActivity.this());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();*/

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(LivePriceGraphActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                       // pdia.dismiss();

                        citylist.clear();


                        System.out.println("RESPONSE OF getCityForGraphAPI API IS---" + response);


                        JSONObject getCityForGraphAPIJson = null;
                        try {
                            getCityForGraphAPIJson = new JSONObject(response);


                            String statusstr = getCityForGraphAPIJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {


                                JSONArray CityForGraphJsonArray = getCityForGraphAPIJson.getJSONArray("CityForGraph");
                                System.out.println("CityForGraphJsonArray IS---" + CityForGraphJsonArray);

                                for(int i = 0; i < CityForGraphJsonArray.length();i++)
                                {
                                    citylist.add(CityForGraphJsonArray.getJSONObject(i).getString("location_name"));
                                }

                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(LivePriceGraphActivity.this,
                                       R.layout.spinner_center, citylist);

                             /*   ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(LivePriceGraphActivity.this,
                                        android.R.layout.simple_spinner_item, citylist);*/
                              //  arrayAdapter.setDropDownViewResource(android.R.layout
                                //        .simple_spinner_dropdown_item);

                                location_spinner.setAdapter(arrayAdapter);


                                // seller_history_spinner.setTypeface(source_sans_pro_normal);
                                //  locationMaterialBetterSpinner = "All";
                                location_spinner.setSelection(0);






                                location_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                        graph.removeAllSeries();
                                        graph1.removeAllSeries();
                                        graph2.removeAllSeries();
                                        xaxislist.clear();
                                        yaxislist.clear();
                                        xaxislist1.clear();
                                        yaxislist1.clear();
                                        xaxislist2.clear();
                                        yaxislist2.clear();



                                        String category_val = SingletonActivity.graphtitle;


                                          if(NetworkUtility.checkConnectivity(LivePriceGraphActivity.this)){
                                            String GetLivePriceGraphUrl = APIName.URL+"/home/getLivePriceForGraph?city="+location_spinner.getSelectedItem().toString()+"&category="+URLEncoder.encode(category_val)+"&type=Weekly";
                                            System.out.println("GET LIVE PRICE GRAPH URL IS WEEKLY---"+ GetLivePriceGraphUrl);
                                            GetLivePriceGraphAPI(GetLivePriceGraphUrl);

                                         }
                                        else{
                                            util.dialog(LivePriceGraphActivity.this, "Please check your internet connection.");
                                          }



                                        if(NetworkUtility.checkConnectivity(LivePriceGraphActivity.this)){
                                            String GetLivePriceGraphUrl = APIName.URL+"/home/getLivePriceForGraph?city="+location_spinner.getSelectedItem().toString()+"&category="+URLEncoder.encode(category_val)+"&type=Monthly";
                                            System.out.println("GET LIVE PRICE GRAPH URL IS MONTHLY---"+ GetLivePriceGraphUrl);
                                            GetLivePriceGraphMonthlyAPI(GetLivePriceGraphUrl);

                                        }
                                        else{
                                            util.dialog(LivePriceGraphActivity.this, "Please check your internet connection.");
                                        }



                                        if(NetworkUtility.checkConnectivity(LivePriceGraphActivity.this)){
                                            String GetLivePriceGraphYearlyUrl = APIName.URL+"/home/getLivePriceForGraph?city="+location_spinner.getSelectedItem().toString()+"&category="+ URLEncoder.encode(category_val)+"&type=All";
                                            System.out.println("GET LIVE PRICE GRAPH URL IS YEARLY---"+ GetLivePriceGraphYearlyUrl);
                                            GetLivePriceGraphYearlyAPI(GetLivePriceGraphYearlyUrl);

                                        }
                                        else{
                                            util.dialog(LivePriceGraphActivity.this, "Please check your internet connection.");
                                        }


                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });


                            }
                            else
                            {

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                           // pdia.dismiss();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                      //  pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(LivePriceGraphActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(LivePriceGraphActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/




                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LivePriceGraphActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void GetLivePriceGraphMonthlyAPI(String url) {

      /*  pdia = new ProgressDialog(LivePriceGraphActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();*/

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(LivePriceGraphActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };
        String tag_json_obj = "json_obj_req";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //pdia.dismiss();


                        System.out.println("RESPONSE OF GetLivePriceGraphAPI MONTHLY IS---" + response);
                     //   Toast.makeText(LivePriceGraphActivity.this,"Response:======>>>"+ response,Toast.LENGTH_SHORT).show();



                        JSONObject GetLivePriceGraphMonthlyAPIJson = null;
                        try {
                            GetLivePriceGraphMonthlyAPIJson = new JSONObject(response);


                            String statusstr = GetLivePriceGraphMonthlyAPIJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true")) {



                                JSONObject CityForGraphMonthlyJsonObj = GetLivePriceGraphMonthlyAPIJson.getJSONObject("CityForGraph");
                                System.out.println("CityForGraphMonthlyJsonObj IS---" + CityForGraphMonthlyJsonObj);

                                xAxisJSONArray1 = CityForGraphMonthlyJsonObj.getJSONArray("xaxis");
                                System.out.println("xAxisJSONArray1 IS---" + xAxisJSONArray1);

                                yAxisJSONArray1 = CityForGraphMonthlyJsonObj.getJSONArray("yaxis");
                                System.out.println("yAxisJSONArray1 IS---" + yAxisJSONArray1);

                                /*JSONObject highestRateMonthlyJSONObj = CityForGraphMonthlyJsonObj.getJSONObject("highestRate");
                                System.out.println("highestRateMonthlyJSONObj IS---" + highestRateMonthlyJSONObj);
                                highestratetitle1.setText("H :" + highestRateMonthlyJSONObj.getString("rate"));
                                highestratedatetitle1.setText(highestRateMonthlyJSONObj.getString("for_date"));


                                JSONObject lowestRateMonthlyJSONObj = CityForGraphMonthlyJsonObj.getJSONObject("lowestRate");
                                System.out.println("lowestRateMonthlyJSONObj IS---" + lowestRateMonthlyJSONObj);
                                lowestratetitle1.setText("L :" + lowestRateMonthlyJSONObj.getString("rate"));
                                lowestratedatetitle1.setText(lowestRateMonthlyJSONObj.getString("for_date"));*/

                                if(CityForGraphMonthlyJsonObj.optJSONObject("highestRate")!=null) {

                                    JSONObject highestRateMonthlyJSONObj = CityForGraphMonthlyJsonObj.getJSONObject("highestRate");
                                    System.out.println("highestRateMonthlyJSONObj IS---" + highestRateMonthlyJSONObj);

                                    highestratetitle1.setText("H :" + highestRateMonthlyJSONObj.getString("rate"));
                                    highestratedatetitle1.setVisibility(View.VISIBLE);
                                    highestratedatetitle1.setText(highestRateMonthlyJSONObj.getString("for_date"));
                                }
                                else
                                {
                                    highestratetitle1.setText("H : 0");
                                    highestratedatetitle1.setVisibility(View.INVISIBLE);
                                }

                                if(CityForGraphMonthlyJsonObj.optJSONObject("lowestRate")!=null) {

                                    JSONObject lowestRateMonthlyJSONObj = CityForGraphMonthlyJsonObj.getJSONObject("lowestRate");
                                    System.out.println("lowestRateMonthlyJSONObj IS---" + lowestRateMonthlyJSONObj);


                                    lowestratetitle1.setText("L :" + lowestRateMonthlyJSONObj.getString("rate"));
                                    lowestratedatetitle1.setVisibility(View.VISIBLE);
                                    lowestratedatetitle1.setText(lowestRateMonthlyJSONObj.getString("for_date"));

                                }
                                else {
                                    lowestratetitle1.setText("L : 0");
                                    lowestratedatetitle1.setVisibility(View.INVISIBLE);
                                }


                                initGraph1(graph1);







                            }
                            else
                            {
                                String messagestr = GetLivePriceGraphMonthlyAPIJson.getString("message");
                                Toast.makeText(LivePriceGraphActivity.this,messagestr,Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            //pdia.dismiss();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                     //   pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(LivePriceGraphActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(LivePriceGraphActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LivePriceGraphActivity.this,hurlStack);
        requestQueue.add(stringRequest);

       // SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }


    private void GetLivePriceGraphAPI(String url) {

        pdia = new ProgressDialog(LivePriceGraphActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(LivePriceGraphActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        String tag_json_obj = "json_obj_req";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();


                        System.out.println("RESPONSE OF GetLivePriceGraphAPI IS---" + response);



                        JSONObject GetLivePriceGraphAPIJson = null;
                        try {
                            GetLivePriceGraphAPIJson = new JSONObject(response);


                            String statusstr = GetLivePriceGraphAPIJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true")) {



                                JSONObject CityForGraphJsonObj = GetLivePriceGraphAPIJson.getJSONObject("CityForGraph");
                                System.out.println("CityForGraphJsonObj IS---" + CityForGraphJsonObj);

                                xAxisJSONArray = CityForGraphJsonObj.getJSONArray("xaxis");
                                System.out.println("xAxisJSONArray IS---" + xAxisJSONArray);

                               yAxisJSONArray = CityForGraphJsonObj.getJSONArray("yaxis");
                                System.out.println("yAxisJSONArray IS---" + yAxisJSONArray);




                                //if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {
                                //Toast.makeText(LivePriceGraphActivity.this, "highestRateJSONObj length==="+ CityForGraphJsonObj.getJSONObject("highestRate").toString().length(), Toast.LENGTH_SHORT).show();

                                if(CityForGraphJsonObj.optJSONObject("highestRate")!=null) {

                                    JSONObject highestRateJSONObj = CityForGraphJsonObj.getJSONObject("highestRate");
                                    System.out.println("highestRateJSONObj IS---" + highestRateJSONObj);

                                    highestratetitle.setText("H :" + highestRateJSONObj.getString("rate"));
                                    highestratedatetitle.setVisibility(View.VISIBLE);
                                    highestratedatetitle.setText(highestRateJSONObj.getString("for_date"));
                                }
                                else
                                {
                                    highestratetitle.setText("H : 0");
                                    highestratedatetitle.setVisibility(View.INVISIBLE);
                                }

                                if(CityForGraphJsonObj.optJSONObject("lowestRate")!=null) {

                                    JSONObject lowestRateJSONObj = CityForGraphJsonObj.getJSONObject("lowestRate");
                                    System.out.println("lowestRateJSONObj IS---" + lowestRateJSONObj);


                                        lowestratetitle.setText("L :" + lowestRateJSONObj.getString("rate"));
                                        lowestratedatetitle.setVisibility(View.VISIBLE);
                                        lowestratedatetitle.setText(lowestRateJSONObj.getString("for_date"));

                                }
                                else {
                                    lowestratetitle.setText("L : 0");
                                    lowestratedatetitle.setVisibility(View.INVISIBLE);
                                }

                                initGraph(graph);




                            }
                            else
                            {
                                String messagestr = GetLivePriceGraphAPIJson.getString("message");
                                Toast.makeText(LivePriceGraphActivity.this,messagestr,Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia.dismiss();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(LivePriceGraphActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(LivePriceGraphActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LivePriceGraphActivity.this,hurlStack);
        requestQueue.add(stringRequest);

      //  SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void initGraph(GraphView graph) {





        xaxislist.clear();
        yaxislist.clear();



        for(int i = 0 ; i < xAxisJSONArray.length(); i++) {
            try {
                String [] dateWeekly = xAxisJSONArray.get(i).toString().split("/");

                xaxislist.add(dateWeekly[0]+"/"+dateWeekly[1]);
              //  xaxislist.add(xAxisJSONArray.get(i).toString());
                yaxislist.add(yAxisJSONArray.get(i).toString());


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


        LineGraphSeries<DataPoint> series;       //an Object of the PointsGraphSeries for plotting scatter graphs

        series= new LineGraphSeries<>(data());   //initializing/defining series to get the data from the method 'data()'
        graph.addSeries(series);                   //adding the series to the GraphView
       // series.setShape(PointsGraphSeries.Shape.POINT);
      //  series.setSize(5);

                String[] arr = xaxislist.toArray(new String[xaxislist.size()]);

                series.setTitle("speed");
              //  series.setDrawBackground(true);



                  series.setColor(Color.parseColor("#FFF15922"));

               //  series.setBackgroundColor(Color.parseColor("#FFF15922"));

                series.setDrawAsPath(true);

                series.setDrawDataPoints(true);
                series.setThickness(3);
        graph.getGridLabelRenderer().setGridColor(Color.parseColor("#D3D3D3"));

               // graph.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_bright));
                //series2.setColor(Color.GREEN);
                graph.getViewport().setScalable(false);
                graph.getViewport().setScrollable(false);
                graph.addSeries(series);

              // set date label formatter


                // as we use dates as labels, the human rounding to nice readable numbers
                // is not nessecary
                graph.getGridLabelRenderer().setHumanRounding(true);

                series.setAnimated(true);


               // graph.getGridLabelRenderer().setNumHorizontalLabels(xaxislist.size());

        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(LivePriceGraphActivity.this));
        graph.getGridLabelRenderer().setNumHorizontalLabels(6); // only 4 because of the space

// set manual x bounds to have nice steps
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(5);
        graph.getViewport().setXAxisBoundsManual(true);


        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);

        if(xaxislist.size()>1) {

            String[] namesArr = (String[]) xaxislist.toArray(new String[xaxislist.size()]);
            staticLabelsFormatter.setHorizontalLabels(namesArr);
            graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        }
        else
        {
            xaxislist.clear();
            yaxislist.clear();



            try {
                xaxislist.add(xAxisJSONArray.get(0).toString());

                String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());

                xaxislist.add(date);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            String[] namesArr = (String[]) xaxislist.toArray(new String[xaxislist.size()]);


            staticLabelsFormatter.setHorizontalLabels(namesArr);



            graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);



        }




        graph.getViewport().setScrollable(false);
        graph.getGridLabelRenderer().setPadding(20);
        graph.getGridLabelRenderer().setTextSize(15);
        graph.getGridLabelRenderer().reloadStyles();


        series.setOnDataPointTapListener(new OnDataPointTapListener() {
                    @Override
                    public void onTap(Series series, DataPointInterface dataPoint) {
                        Toast.makeText(LivePriceGraphActivity.this, "Live Price : "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
                    }
                });

        }




    /*    // second series
        LineGraphSeries<DataPoint> series2 = new LineGraphSeries<>(new DataPoint[] {
          *//*      new DataPoint(1, 22.000000),
                new DataPoint(2, 22.000000),
                new DataPoint(3, 11111.000000),
                new DataPoint(4, 11111.000000),
                new DataPoint(5, 11111.000000),*//*

        });*/


        // legend
        //graph.getLegendRenderer().setVisible(true);
        //graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

    @RequiresApi(api = Build.VERSION_CODES.N)
    public DataPoint[] data(){
        int n= xaxislist.size();     //to find out the no. of data-points
        DataPoint[] values = new DataPoint[n];     //creating an object of type DataPoint[] of size 'n'
        for(int i=0;i<n;i++){

        //    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");

            try {
                Date d = dateFormat.parse(xaxislist.get(i));
                DataPoint v = new DataPoint(i,Double.parseDouble(yaxislist.get(i)));
                values[i] = v;
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }


        }
        return values;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void initGraph2(GraphView graph) {





        xaxislist2.clear();
        yaxislist2.clear();





        for(int i = 0 ; i < xAxisJSONArray2.length(); i++) {
            try {
                xaxislist2.add(xAxisJSONArray2.get(i).toString());

                if(yAxisJSONArray2.get(i).toString().length()==4) {
                    yaxislist2.add("0");
                }
                else
                {
                    yaxislist2.add(yAxisJSONArray2.get(i).toString());
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }




        LineGraphSeries<DataPoint> series2;       //an Object of the PointsGraphSeries for plotting scatter graphs

        series2= new LineGraphSeries<>(data2());   //initializing/defining series to get the data from the method 'data()'
        graph2.addSeries(series2);                   //adding the series to the GraphView
        // series.setShape(PointsGraphSeries.Shape.POINT);
        //  series.setSize(5);

        String[] arr2 = xaxislist2.toArray(new String[xaxislist2.size()]);

        series2.setTitle("speed");
       // series2.setDrawBackground(true);



        series2.setColor(Color.parseColor("#FFF15922"));

   //     series2.setBackgroundColor(Color.parseColor("#FFF15922"));

        series2.setDrawAsPath(true);
        series2.setThickness(3);
        series2.setDrawDataPoints(true);

        // graph.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_bright));
        //series2.setColor(Color.GREEN);
        graph2.getViewport().setScalable(false);
        graph2.getViewport().setScrollable(false);
        graph2.addSeries(series2);

        // set date label formatter


        // as we use dates as labels, the human rounding to nice readable numbers
        // is not nessecary
        graph2.getGridLabelRenderer().setHumanRounding(true);
        graph2.getGridLabelRenderer().setGridColor(Color.parseColor("#D3D3D3"));
        series2.setAnimated(true);


        // graph.getGridLabelRenderer().setNumHorizontalLabels(xaxislist.size());

        graph2.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(LivePriceGraphActivity.this));
        graph2.getGridLabelRenderer().setNumHorizontalLabels(10); // only 4 because of the space

// set manual x bounds to have nice steps
        graph2.getViewport().setMinX(0);
        graph2.getViewport().setMaxX(11);
        graph2.getViewport().setXAxisBoundsManual(true);


        StaticLabelsFormatter staticLabelsFormatter2 = new StaticLabelsFormatter(graph2);

        Log.e("length of x-axis","LENGTH OF X- AXIS 2==>>>>>>>>>>>>"+ xaxislist2);

        if(xaxislist2.size()>1) {

            String[] namesArr2 = (String[]) xaxislist2.toArray(new String[xaxislist2.size()]);
            staticLabelsFormatter2.setHorizontalLabels(namesArr2);
            graph2.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter2);
        }
        else
        {
            xaxislist2.clear();
            yaxislist2.clear();



            try {
                xaxislist2.add(xAxisJSONArray2.get(0).toString());

                String date2 = new SimpleDateFormat("MMMM", Locale.getDefault()).format(new Date());

                xaxislist2.add(date2);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            String[] namesArr2 = (String[]) xaxislist2.toArray(new String[xaxislist2.size()]);


            staticLabelsFormatter2.setHorizontalLabels(namesArr2);



            graph2.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter2);


        }




        graph2.getViewport().setScrollable(false);
        graph2.getGridLabelRenderer().setPadding(20);
        graph.getGridLabelRenderer().setTextSize(13);
        graph2.getGridLabelRenderer().reloadStyles();

        series2.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(LivePriceGraphActivity.this, "Live Price : "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
            }
        });

    }




    /*    // second series
        LineGraphSeries<DataPoint> series2 = new LineGraphSeries<>(new DataPoint[] {
          *//*      new DataPoint(1, 22.000000),
                new DataPoint(2, 22.000000),
                new DataPoint(3, 11111.000000),
                new DataPoint(4, 11111.000000),
                new DataPoint(5, 11111.000000),*//*

        });*/


    // legend
    //graph.getLegendRenderer().setVisible(true);
    //graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

    @RequiresApi(api = Build.VERSION_CODES.N)
    public DataPoint[] data2(){
        int n2= xaxislist2.size();     //to find out the no. of data-points
        DataPoint[] values2 = new DataPoint[n2];     //creating an object of type DataPoint[] of size 'n'
        for(int i=0;i<n2;i++){

            SimpleDateFormat dateFormat2 = new SimpleDateFormat("MMMM");
            try {
                Date d2 = dateFormat2.parse(xaxislist2.get(i));
                DataPoint v2 = new DataPoint(i,Double.parseDouble(yaxislist2.get(i)));
                values2[i] = v2;
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }


        }
        return values2;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void initGraph1(GraphView graph) {





        xaxislist1.clear();
        yaxislist1.clear();





        for(int i = 0 ; i < xAxisJSONArray1.length(); i++) {
            try {

                String [] dateMonthly = xAxisJSONArray1.get(i).toString().split("/");

                xaxislist1.add(dateMonthly[0]+"/"+dateMonthly[1]);

               // xaxislist1.add(xAxisJSONArray1.get(i).toString());

                if(yAxisJSONArray1.get(i).toString().length()==4) {
                    yaxislist1.add("0");
                }
                else
                {
                    yaxislist1.add(yAxisJSONArray1.get(i).toString());
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }




        LineGraphSeries<DataPoint> series1;       //an Object of the PointsGraphSeries for plotting scatter graphs

        series1= new LineGraphSeries<>(data1());   //initializing/defining series to get the data from the method 'data()'
        graph1.addSeries(series1);                   //adding the series to the GraphView
        // series.setShape(PointsGraphSeries.Shape.POINT);
        //  series.setSize(5);

        String[] arr = xaxislist.toArray(new String[xaxislist.size()]);

        series1.setTitle("speed");
        //series1.setDrawBackground(true);



        series1.setColor(Color.parseColor("#FFF15922"));

      //  series1.setBackgroundColor(Color.parseColor("#FFF15922"));

        series1.setDrawAsPath(true);
        series1.setThickness(3);
        series1.setDrawDataPoints(true);

        // graph.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_bright));
        //series2.setColor(Color.GREEN);
        graph1.getViewport().setScalable(false);
        graph1.getViewport().setScrollable(false);
        graph1.addSeries(series1);

        // set date label formatter


        // as we use dates as labels, the human rounding to nice readable numbers
        // is not nessecary
        graph1.getGridLabelRenderer().setHumanRounding(true);
        graph1.getGridLabelRenderer().setGridColor(Color.parseColor("#D3D3D3"));

        series1.setAnimated(true);


        // graph.getGridLabelRenderer().setNumHorizontalLabels(xaxislist.size());

        graph1.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(LivePriceGraphActivity.this));
        graph1.getGridLabelRenderer().setNumHorizontalLabels(6); // only 4 because of the space


// set manual x bounds to have nice steps
        graph1.getViewport().setMinX(0);
        graph1.getViewport().setMaxX(5);
        graph1.getViewport().setXAxisBoundsManual(true);


        StaticLabelsFormatter staticLabelsFormatter1 = new StaticLabelsFormatter(graph);

        Log.e("length of x-axis","LENGTH OF X- AXIS=="+ xaxislist1);

        if(xaxislist1.size()>1) {

            String[] namesArr1 = (String[]) xaxislist1.toArray(new String[xaxislist1.size()]);
            staticLabelsFormatter1.setHorizontalLabels(namesArr1);
            graph1.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter1);
        }
        else
        {
            xaxislist1.clear();
            yaxislist1.clear();



            try {
                xaxislist1.add(xAxisJSONArray1.get(0).toString());

                String date1 = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());

                xaxislist1.add(date1);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            String[] namesArr1 = (String[]) xaxislist1.toArray(new String[xaxislist1.size()]);


            staticLabelsFormatter1.setHorizontalLabels(namesArr1);



            graph1.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter1);


        }




        graph1.getViewport().setScrollable(false);
        graph1.getGridLabelRenderer().setPadding(20);
        graph.getGridLabelRenderer().setTextSize(15);


        graph1.getGridLabelRenderer().reloadStyles();

        series1.setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                Toast.makeText(LivePriceGraphActivity.this, "Live Price : "+dataPoint.getY(), Toast.LENGTH_SHORT).show();
            }
        });

    }




    /*    // second series
        LineGraphSeries<DataPoint> series2 = new LineGraphSeries<>(new DataPoint[] {
          *//*      new DataPoint(1, 22.000000),
                new DataPoint(2, 22.000000),
                new DataPoint(3, 11111.000000),
                new DataPoint(4, 11111.000000),
                new DataPoint(5, 11111.000000),*//*

        });*/


    // legend
    //graph.getLegendRenderer().setVisible(true);
    //graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

    @RequiresApi(api = Build.VERSION_CODES.N)
    public DataPoint[] data1(){
        int n1= xaxislist1.size();     //to find out the no. of data-points
        DataPoint[] values1 = new DataPoint[n1];     //creating an object of type DataPoint[] of size 'n'
        for(int i=0;i<n1;i++){

            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM");
          //  SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date d1 = dateFormat1.parse(xaxislist1.get(i));
                DataPoint v1 = new DataPoint(i,Double.parseDouble(yaxislist1.get(i)));
                values1[i] = v1;
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }


        }
        return values1;
    }

    private void GetLivePriceGraphYearlyAPI(String url) {

      /*  pdia = new ProgressDialog(LivePriceGraphActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();*/
        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(LivePriceGraphActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        String tag_json_obj = "json_obj_req";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //pdia.dismiss();


                        System.out.println("RESPONSE OF GetLivePriceGraphYearlyAPI MONTHLY IS---" + response);
                        //   Toast.makeText(LivePriceGraphActivity.this,"Response:======>>>"+ response,Toast.LENGTH_SHORT).show();



                        JSONObject GetLivePriceGraphYearlyAPIJson = null;
                        try {
                            GetLivePriceGraphYearlyAPIJson = new JSONObject(response);


                            String statusstr = GetLivePriceGraphYearlyAPIJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true")) {



                                JSONObject CityForGraphYearlyJsonObj = GetLivePriceGraphYearlyAPIJson.getJSONObject("CityForGraph");
                                System.out.println("CityForGraphYearlyJsonObj IS---" + CityForGraphYearlyJsonObj);

                                xAxisJSONArray2 = CityForGraphYearlyJsonObj.getJSONArray("xaxis");
                                System.out.println("xAxisJSONArray2 IS---" + xAxisJSONArray2);

                                yAxisJSONArray2 = CityForGraphYearlyJsonObj.getJSONArray("yaxis");
                                System.out.println("yAxisJSONArray2 IS---" + yAxisJSONArray2);

                               /* JSONObject highestRateYearlyJSONObj = CityForGraphYearlyJsonObj.getJSONObject("highestRate");
                                System.out.println("highestRateYearlyJSONObj IS---" + highestRateYearlyJSONObj);
                                highestratetitle2.setText("H :" + highestRateYearlyJSONObj.getString("rate"));
                                highestratedatetitle2.setText(highestRateYearlyJSONObj.getString("for_date"));


                                JSONObject lowestRateYearlyJSONObj = CityForGraphYearlyJsonObj.getJSONObject("lowestRate");
                                System.out.println("lowestRateYearlyJSONObj IS---" + lowestRateYearlyJSONObj);
                                lowestratetitle2.setText("L :" + lowestRateYearlyJSONObj.getString("rate"));
                                lowestratedatetitle2.setText(lowestRateYearlyJSONObj.getString("for_date"));*/

                                if(CityForGraphYearlyJsonObj.optJSONObject("highestRate")!=null) {

                                    JSONObject highestRateYearlyJSONObj = CityForGraphYearlyJsonObj.getJSONObject("highestRate");
                                    System.out.println("highestRateYearlyJSONObj IS---" + highestRateYearlyJSONObj);

                                    highestratetitle2.setText("H :" + highestRateYearlyJSONObj.getString("rate"));
                                    highestratedatetitle2.setVisibility(View.VISIBLE);
                                    highestratedatetitle2.setText(highestRateYearlyJSONObj.getString("for_date"));
                                }
                                else
                                {
                                    highestratetitle2.setText("H : 0");
                                    highestratedatetitle2.setVisibility(View.INVISIBLE);
                                }

                                if(CityForGraphYearlyJsonObj.optJSONObject("lowestRate")!=null) {

                                    JSONObject lowestRateYearlyJSONObj = CityForGraphYearlyJsonObj.getJSONObject("lowestRate");
                                    System.out.println("lowestRateYearlyJSONObj IS---" + lowestRateYearlyJSONObj);


                                    lowestratetitle2.setText("L :" + lowestRateYearlyJSONObj.getString("rate"));
                                    lowestratedatetitle2.setVisibility(View.VISIBLE);
                                    lowestratedatetitle2.setText(lowestRateYearlyJSONObj.getString("for_date"));

                                }
                                else {
                                    lowestratetitle2.setText("L : 0");
                                    lowestratedatetitle2.setVisibility(View.INVISIBLE);
                                }

                                initGraph2(graph2);




                            }
                            else
                            {
                                String messagestr = GetLivePriceGraphYearlyAPIJson.getString("message");
                                Toast.makeText(LivePriceGraphActivity.this,messagestr,Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            //pdia.dismiss();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        //   pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(LivePriceGraphActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(LivePriceGraphActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LivePriceGraphActivity.this,hurlStack);
        requestQueue.add(stringRequest);

       // SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }

}
