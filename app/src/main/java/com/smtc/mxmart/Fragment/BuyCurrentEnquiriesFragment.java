package com.smtc.mxmart.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.smtc.mxmart.APIName;
import com.smtc.mxmart.HomeActivity;
import com.smtc.mxmart.NetworkUtility;
import com.smtc.mxmart.R;
import com.smtc.mxmart.SingletonActivity;
import com.smtc.mxmart.UtilsDialog;
import com.smtc.mxmart.ViewLiveTradeActivity;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by 10161 on 8/2/2017.
 */

public class BuyCurrentEnquiriesFragment extends Fragment {

    Typeface source_sans_pro_normal;
    ListView buycurrentenquirylistvw;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String userCodestr,mobilenumstr,qtyofferedstrs,buyenquiry;
    UtilsDialog util = new UtilsDialog();
    CustomAdap customadap;
    AlertDialog b;
    String qtyofferedstr,remaining_quantity;
    TextView noenquiriesfoundtxt;
    ProgressDialog pdia,pdia1;
    private boolean _hasLoadedOnce= false; // your boolean field
    private ProgressBar bar;
    Handler handler = new Handler();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        System.out.println("ST USER PROF JSON IN EDIT BUSINESS DETAILS FRAGMENT---" + SingletonActivity.stuserprofjson);

        source_sans_pro_normal = Typeface.createFromAsset(getActivity().getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");



        View view = inflater.inflate(R.layout.fragment_buy_current_enquiries, container, false);

        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        buyenquiry = prefs.getString("buyenquiry",null);

        bar = (ProgressBar)view.findViewById(R.id.progressBar);
        buycurrentenquirylistvw = (ListView)view.findViewById(R.id.buycurrentenquirieslistvw);
        noenquiriesfoundtxt = (TextView)view.findViewById(R.id.noenquiriesfoundtxt);

      //  SingletonActivity.clickedBuyCurrentEnquiry = true;



        if (NetworkUtility.checkConnectivity(getActivity())) {


            String BuyerGetCurrentEnquiryUrl = APIName.URL + "/buyer/getCurrentEnq?user_code=" + userCodestr;
            System.out.println("BUYER GET CURRENT ENQUIRY URL IS---" + BuyerGetCurrentEnquiryUrl);
            BuyerGetCurrentEnquiryAPI(BuyerGetCurrentEnquiryUrl);


        } else {
            util.dialog(getActivity(), "Please check your internet connection.");
        }



        return view;
    }



    private void BuyerGetCurrentEnquiryAPI(String url) {

        /*pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();*/

       // getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
           //     WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        bar.setVisibility(View.VISIBLE);

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {

                       //  pdia.dismiss();
                      //  getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                bar.setVisibility(View.GONE);

                                System.out.println("RESPONSE OF BUYER GET CURRENT ENQUIRY API IS---" + response);



                                JSONObject BuyerTodaysOfferJson = null;
                                try {
                                    BuyerTodaysOfferJson = new JSONObject(response);


                                    String statusstr = BuyerTodaysOfferJson.getString("status");


                                    if(statusstr.equalsIgnoreCase("true"))
                                    {

                                        String get_limit_value_str = BuyerTodaysOfferJson.getString("get_limit_value");
                                        String limit_id = BuyerTodaysOfferJson.getString("limit_id");


                                        JSONArray BuyerCurrentEnquiryJsonArray = BuyerTodaysOfferJson.getJSONArray("ce_Offer");
                                        System.out.println("BUYER CURRENT ENQUIRY JSONARRAY IS---" + BuyerCurrentEnquiryJsonArray);

                                        if (getActivity()!=null) {
                                            customadap = new CustomAdap(getActivity(), BuyerCurrentEnquiryJsonArray, get_limit_value_str, limit_id);
                                            buycurrentenquirylistvw.setAdapter(customadap);
                                        }

                                    }
                                    else
                                    {

                                        String messagestr = BuyerTodaysOfferJson.getString("message");
                                        // Toast.makeText(getActivity(),messagestr,Toast.LENGTH_SHORT).show();
                                        noenquiriesfoundtxt.setText(messagestr);;
                                        noenquiriesfoundtxt.setVisibility(View.VISIBLE);

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                  //  pdia.dismiss();

                                //    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            //dialog.dismiss();
                                            bar.setVisibility(View.GONE);

                                        }
                                    }, 3000); // 3000 milliseconds delay


                                }




                            }
                        }, 3000); // 3000 milliseconds delay


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                       // getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //dialog.dismiss();
                                bar.setVisibility(View.GONE);

                            }
                        }, 3000); // 3000 milliseconds delay



                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("get buyer current enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }

    private class CustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        private JSONArray BuyCurrentEnquiryJSONArray;
        String get_limit_value_str;
        String limit_id;




        public CustomAdap(Context mainActivity,JSONArray BuyCurrentEnquiryJSONArray,String get_limit_value_str,String limit_id)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.BuyCurrentEnquiryJSONArray = BuyCurrentEnquiryJSONArray;
            this.get_limit_value_str =get_limit_value_str;
            this.limit_id = limit_id;
            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return BuyCurrentEnquiryJSONArray.length();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub



            final Holder holder = new Holder();

            final View rowView;


            rowView = inflater.inflate(R.layout.buyer_current_enquiries_row, null);




            holder.cmpnyname = (TextView)rowView.findViewById(R.id.cmpnynametxtvw);
            holder.ratingBar = (RatingBar)rowView.findViewById(R.id.ratingBar);
            holder.qtydesctxtvw = (TextView)rowView.findViewById(R.id.qtydesctxtvw);
            holder.tradeiddesc = (TextView)rowView.findViewById(R.id.tradeiddesctxtvw);
            holder.proddesc = (TextView) rowView.findViewById(R.id.categorytxtvw);

            holder.acceptiv = (ImageView)rowView.findViewById(R.id.acceptiv);
            holder.rejectiv = (ImageView)rowView.findViewById(R.id.rejectiv);
            holder.statustxt = (TextView)rowView.findViewById(R.id.statusdesctxtvw);
            holder.gradetxtvw = (TextView)rowView.findViewById(R.id.gradetxtvw);


            try {
                if(!BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("isquantity").equalsIgnoreCase("1"))
                {
                    holder.qtydesctxtvw.setText(BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity"));

                }
                else if(BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("isquantity").equalsIgnoreCase("1"))
                {
                    holder.qtydesctxtvw.setTextColor(Color.RED);
                    holder.qtydesctxtvw.setText(BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity_modified"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }





            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    try {
                        String tradeidstr = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("trade_id");
                        String companyname = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("seller_company_name");
                        String categorydesc = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("category_name");
                        String subcategorydesc = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("sub_category_name");
                        String locationdesc = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("seller_city");
                         qtyofferedstr = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity");
                        String ratestr = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("rate");
                       /* String q1val = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q1");
                        String q2val = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q2");
                        String q3val = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q3");
                        String q4val = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q4");
                        String q5val = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q5");
                        String q6val = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q6");
                        String q7val = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q7");
                        String q8val = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q8");*/
                        String tncval = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("terms");
                        String isterm = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("isterm");
                        String israte = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("israte");
                        String isquantity = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("isquantity");
                        String quantity_modified =  BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity_modified");
                        String orginial_rate =  BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("orginial_rate");


                        String markdownpriceval = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("quoted_rate");
                        String tradestatusval = holder.statustxt.getText().toString();
                        String rate_type_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("rate_type");
                        String category_grade_name_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("category_grade_name");
                        String category_grade_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("category_grade");
                        String size_range_start_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("size_range_start");
                        String size_range_end_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("size_range_end");
                        String q1_title_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q1_title");
                        String q1_value_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q1_value");
                        String q2_title_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q2_title");
                        String q2_value_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q2_value");
                        String q3_title_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q3_title");
                        String q3_value_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q3_value");
                        String q4_title_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q4_title");
                        String q4_value_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q4_value");
                        String q5_title_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q5_title");
                        String q5_value_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q5_value");
                        String q6_title_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q6_title");
                        String q6_value_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q6_value");
                        String q7_title_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q7_title");
                        String q7_value_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q7_value");
                        String q8_title_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q8_title");
                        String q8_value_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("q8_value");
                        String transport_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("transport");
                        String loading_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("loading");
                        String gst_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("gst");
                        String insurance_select_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("insurance_select");
                        String insurance_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("insurance");
                        String categoryid = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("category_id");
                        String length_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("length");
                        String promo_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("promo_code");
                        String modified_field_str = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("modified_field");

                        JSONArray detailJSONArray = BuyCurrentEnquiryJSONArray.getJSONObject(position).getJSONArray("detail");


                        final String delivery_type_str = detailJSONArray.getJSONObject(0).getString("delivery_type");
                        final String delivery_in_days_str = detailJSONArray.getJSONObject(0).getString("delivery_in_days");
                        final String material_inspection_str = detailJSONArray.getJSONObject(0).getString("material_inspection_type");
                        final String weightment_type_str = detailJSONArray.getJSONObject(0).getString("weightment_type");
                        final String test_certificate_type_str = detailJSONArray.getJSONObject(0).getString("test_certificate_type");


                        ViewOfferDialog(modified_field_str,delivery_type_str,delivery_in_days_str,material_inspection_str,weightment_type_str,test_certificate_type_str,promo_str,length_str,categoryid,transport_str,loading_str,gst_str,insurance_select_str,insurance_str,category_grade_str,size_range_start_str,size_range_end_str,q1_title_str,q1_value_str,q2_title_str,q2_value_str,q3_title_str,q3_value_str,q4_title_str,q4_value_str,q5_title_str,q5_value_str,q6_title_str,q6_value_str,q7_title_str,q7_value_str,q8_title_str,q8_value_str,category_grade_name_str,rate_type_str,orginial_rate,quantity_modified,isquantity,tradeidstr,companyname,categorydesc,subcategorydesc,locationdesc,qtyofferedstr,ratestr,tncval,markdownpriceval,tradestatusval,isterm,israte);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });




            holder.acceptiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(buyenquiry.equalsIgnoreCase("0"))
                    {

                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setMessage("Do you really want to Accept Offer?")
                                // .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        String id = null;
                                        String quantity = null;
                                        try {
                                            id = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("id");

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        String acceptstatus = "4";
                                        System.out.println("STATUS PASSED ON CLICK OF ACCEPT===" + acceptstatus);

                                        if (NetworkUtility.checkConnectivity(getActivity())) {


                                            String SellerAcceptUrl = APIName.URL + "/buyer/changeTradeStatusNew?id=" + id + "&status=" + acceptstatus + "&quantity=" + holder.qtydesctxtvw.getText().toString();
                                            System.out.println("SELLER ACCEPT URL IS---" + SellerAcceptUrl);
                                            BuyerActionAPI(SellerAcceptUrl);


                                        } else {
                                            util.dialog(getActivity(), "Please check your internet connection.");
                                        }

                                    }
                                })
                                .setNegativeButton(android.R.string.no, null).show();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                    }


                }
            });

            holder.rejectiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(buyenquiry.equalsIgnoreCase("0")) {

                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setMessage("Do you really want to Reject Offer?")
                                // .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {

                                        String id = null;
                                        try {
                                            id = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("id");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        String rejectstatus = "3";
                                        System.out.println("STATUS PASSED ON CLICK OF REJECT===" + rejectstatus);


                                        if (NetworkUtility.checkConnectivity(getActivity())) {


                                            String SellerAcceptUrl = APIName.URL + "/buyer/changeTradeStatusNew?id=" + id + "&status=" + rejectstatus + "&quantity=" + holder.qtydesctxtvw.getText().toString();
                                            ;
                                            System.out.println("SELLER REJECT URL IS---" + SellerAcceptUrl);
                                            BuyerActionAPI(SellerAcceptUrl);


                                        } else {
                                            util.dialog(getActivity(), "Please check your internet connection.");
                                        }


                                    }
                                })
                                .setNegativeButton(android.R.string.no, null).show();

                    }
                    else
                    {
                        Toast.makeText(getActivity(), "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                    }



                }
            });


            try {
                holder.cmpnyname.setText(BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("seller_company_name"));
                holder.ratingBar.setRating(Float.parseFloat(BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("rating")));
                holder.tradeiddesc.setText(BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("trade_id"));
                holder.proddesc.setText(BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("category_name"));
                holder.gradetxtvw.setText(BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("category_grade_name"));

                String trade_status = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("trade_status");
                String trade_flag = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("trade_flag");
                remaining_quantity = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("remaining_quantity");

                System.out.println("TRADE STATUS-----"+ trade_status);
                System.out.println("TRADE FLAG-----"+ trade_flag);


                //For displaying action buttons--------->

                if(trade_flag.equalsIgnoreCase("0")){
                    if(trade_status.equalsIgnoreCase("Trade Removed") || trade_status.equalsIgnoreCase("Trade Modified")){

                        holder.acceptiv.setVisibility(View.INVISIBLE);
                        holder.rejectiv.setVisibility(View.INVISIBLE);
                        holder.acceptiv.setEnabled(false);
                        holder.rejectiv.setEnabled(false);
                    }else{
                        holder.acceptiv.setBackgroundResource(R.mipmap.ic_waiting);
                        holder.acceptiv.setVisibility(View.VISIBLE);
                        holder.rejectiv.setVisibility(View.INVISIBLE);
                        holder.acceptiv.setEnabled(false);
                        holder.rejectiv.setEnabled(false);
                    }

                }else if(trade_flag.equalsIgnoreCase("1")){
                    holder.acceptiv.setVisibility(View.INVISIBLE);
                    holder.rejectiv.setVisibility(View.INVISIBLE);
                    holder.acceptiv.setEnabled(false);
                    holder.rejectiv.setEnabled(false);

                }else if(trade_flag.equalsIgnoreCase("2")){

                    if(trade_status.equalsIgnoreCase("Trade Removed") || trade_status.equalsIgnoreCase("Trade Modified")){
                        holder.acceptiv.setVisibility(View.INVISIBLE);
                        holder.rejectiv.setVisibility(View.INVISIBLE);
                        holder.acceptiv.setEnabled(false);
                        holder.rejectiv.setEnabled(false);
                    }else{
		/* Accept & Reject Button */
                        holder.acceptiv.setBackgroundResource(R.mipmap.ic_accept);
                        holder.rejectiv.setBackgroundResource(R.mipmap.ic_reject);
                        holder.acceptiv.setVisibility(View.VISIBLE);
                        holder.rejectiv.setVisibility(View.VISIBLE);
                        holder.acceptiv.setEnabled(true);
                        holder.rejectiv.setEnabled(true);

                        final String quantity = holder.qtydesctxtvw.getText().toString();

                        if(Float.parseFloat(quantity) <= Float.parseFloat(get_limit_value_str))
                        {
                           //accept
                        }
                        else
                        {
                            holder.acceptiv.setBackgroundResource(R.mipmap.edit);

                            holder.acceptiv.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    //quantity required is
                                    setUpdateBuyingLimitDialog(quantity,get_limit_value_str,limit_id);

                                }
                            });
                        }





                    }
                }else if(trade_flag.equalsIgnoreCase("3")){
                    holder.acceptiv.setVisibility(View.INVISIBLE);
                    holder.rejectiv.setVisibility(View.INVISIBLE);
                    holder.acceptiv.setEnabled(false);
                    holder.rejectiv.setEnabled(false);
                }else if(trade_flag.equalsIgnoreCase("4")){
                    //    green icon - (SP. Generated)
                    holder.acceptiv.setBackgroundResource(R.mipmap.ic_saudapatra);
                    holder.acceptiv.setVisibility(View.VISIBLE);
                    holder.rejectiv.setVisibility(View.INVISIBLE);
                    holder.acceptiv.setEnabled(false);
                    holder.rejectiv.setEnabled(false);
                }else if(trade_flag.equalsIgnoreCase("5")){
                    // empty - (limit Reached)
                   /* if(!remaining_quantity.equalsIgnoreCase("0")){
                        holder.acceptiv.setBackgroundResource(R.mipmap.ic_requote);
                        holder.acceptiv.setVisibility(View.VISIBLE);
                        holder.rejectiv.setVisibility(View.INVISIBLE);
                        holder.acceptiv.setEnabled(true);
                        holder.rejectiv.setEnabled(false);

                        holder.acceptiv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    qtyofferedstr = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity");
                                    remaining_quantity = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("remaining_quantity");
                                     String id = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("id");
                                    String tradeid = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("trade_id");
                                    String sellid = BuyCurrentEnquiryJSONArray.getJSONObject(position).getString("sell_id");

                                    RequoteDialog(qtyofferedstr,remaining_quantity,id,tradeid,sellid);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                    }
*/

                }else if(trade_flag.equalsIgnoreCase("6")){
                    //green icon - (Deal Closed)
                    holder.acceptiv.setBackgroundResource(R.mipmap.ic_accept);
                    holder.acceptiv.setVisibility(View.INVISIBLE);
                    holder.rejectiv.setVisibility(View.INVISIBLE);
                    holder.acceptiv.setEnabled(true);
                    holder.rejectiv.setEnabled(false);
                }





                String statusstr = "";

                if(trade_flag.equalsIgnoreCase("0")){

                    if(trade_status.equalsIgnoreCase("Trade Removed")){
                        statusstr = "Trade Deleted";

                        //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));

                    }else if(trade_status.equalsIgnoreCase("Trade Modified")){
                        statusstr = "Trade Modified";

                        //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));

                    }else{
                        statusstr = "Pending Acceptance";

                        //GREY
                        System.out.println("STATUS IS--"+statusstr);
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#8E8E8E"));

                    }

                }else if(trade_flag.equalsIgnoreCase("1")){

                    if(trade_status.equalsIgnoreCase("Trade Removed")){
                        statusstr = "Trade Deleted";

                        //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));


                    }else if(trade_status.equalsIgnoreCase("Trade Modified")){
                        statusstr = "Trade Modified";

                        //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));


                    }else{
                        statusstr = "Seller Rejected";

                        //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));


                    }

                }else if(trade_flag.equalsIgnoreCase("2")){

                    if(trade_status.equalsIgnoreCase("Trade Removed")){
                        statusstr = "Trade Deleted";

                        //RED


                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));


                    }else if(trade_status.equalsIgnoreCase("Trade Modified")){
                        statusstr = "Trade Modified";
                        //RED

                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));

                    }else{
                        statusstr = "Seller Accepted";
                        //ORANGE

                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#F0A33F"));

                    }

                }else if(trade_flag.equalsIgnoreCase("3")){

                    if(trade_status.equalsIgnoreCase("Trade Removed")){
                        statusstr = "Trade Deleted";

                        //RED

                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));

                    }else if(trade_status.equalsIgnoreCase("Trade Modified")){
                        statusstr = "Trade Modified";

                        //RED

                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));


                    }else{
                        statusstr = "Buyer Denied";

                        //RED

                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);

                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));

                    }

                }else if(trade_flag.equalsIgnoreCase("4")){
                    statusstr = "SP. Generated";

                    //GREEN

                    String[] splitStr = statusstr.split("\\s+");
                    holder.statustxt.setText(statusstr);

                    holder.statustxt.setTextColor(Color.parseColor("#009E55"));


                }else if(trade_flag.equalsIgnoreCase("5")){
                    //statusstr = "Limit reached";
                    statusstr = "Buyer Denied";
                    //RED

                    String[] splitStr = statusstr.split("\\s+");
                    holder.statustxt.setText(statusstr);

                    holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));



                }else if(trade_flag.equalsIgnoreCase("6")){
                    statusstr = "Deal Closed";

                    //RED

                    String[] splitStr = statusstr.split("\\s+");
                    holder.statustxt.setText(statusstr);

                    holder.statustxt.setTextColor(Color.parseColor("#009E55"));


                }






            } catch (JSONException e) {
                e.printStackTrace();
            }


            return rowView;

        }


    }


    public void setUpdateBuyingLimitDialog(String quantity,String get_limit_value_str,final String limitidstr) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.edit_buying_daily_limit1, null);
        dialogBuilder.setView(dialogView);


        final TextView qtyrequiredtxt = (TextView) dialogView.findViewById(R.id.qtyrequiredtxt);
        final EditText presentlimitedttxt = (EditText) dialogView.findViewById(R.id.presentlimitedt);
        final EditText newlimitedttxt = (EditText) dialogView.findViewById(R.id.newlimitedt);
        final RelativeLayout updaterel = (RelativeLayout) dialogView.findViewById(R.id.updaterelative);
        final TextView updatetextvw = (TextView) dialogView.findViewById(R.id.updatetext);
        final RelativeLayout cancelrelative = (RelativeLayout) dialogView.findViewById(R.id.cancelrelative);
        final TextView canceltxtvw = (TextView) dialogView.findViewById(R.id.canceltext);

        qtyrequiredtxt.setText("Quantity required is "+ quantity);


        presentlimitedttxt.setText(get_limit_value_str+" mt");





        final  AlertDialog b = dialogBuilder.create();
        b.show();


        updaterel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String newlimitstr = newlimitedttxt.getText().toString();

                if(newlimitstr.length()>0) {

                    int newlimitint = Integer.parseInt(newlimitstr);

                    if (newlimitint % 50 == 0 && newlimitint > 0) {

                        b.dismiss();


                        if (NetworkUtility.checkConnectivity(getActivity())) {
                            String UpdateDailyLimitUrl = APIName.URL + "/liveTrading/insertUpdateDailyLimit?limit_id=" + limitidstr;
                            System.out.println("UPDATE DAILY LIMIT URL IS---" + UpdateDailyLimitUrl);
                            UpdateDailyLimitAPI(UpdateDailyLimitUrl, newlimitstr);

                        } else {
                            util.dialog(getActivity(), "Please check your internet connection.");
                        }
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Entered value should be multiple of 50", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getActivity(), "Please Enter New Limit", Toast.LENGTH_SHORT).show();
                }




            }
        });
        cancelrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }

    private void UpdateDailyLimitAPI(String url, final String newlimit) {

        pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            if ((pdia != null) && pdia.isShowing()) {
                                pdia.dismiss();
                            }
                        } catch (final IllegalArgumentException e) {
                            // Handle or log or ignore
                        } catch (final Exception e) {
                            // Handle or log or ignore
                        } finally {
                            pdia = null;
                        }

                        System.out.println("RESPONSE OF UPDATE DAILY LIMIT API IS---" + response);


                        if(NetworkUtility.checkConnectivity(getActivity())){


                            String BuyerGetCurrentEnquiryUrl = APIName.URL+"/buyer/getCurrentEnq?user_code="+userCodestr;
                            System.out.println("BUYER GET CURRENT ENQUIRY URL IS---"+ BuyerGetCurrentEnquiryUrl);
                            BuyerGetCurrentEnquiryAPI(BuyerGetCurrentEnquiryUrl);


                        }
                        else{
                            util.dialog(getActivity(), "Please check your internet connection.");
                        }



                     /*   new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                        Intent i = new Intent(getActivity(), HomeActivity.class);
                                        SingletonActivity.frombuycurrentenquiry = true;
                                        startActivity(i);
                              *//*  FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.detach(BuyCurrentEnquiriesFragment.this).attach(BuyCurrentEnquiriesFragment.this).commit();*//*


                            }
                        }, 10);*/


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(getActivity(), messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_code",userCodestr);
                params.put("mobile_number",mobilenumstr);
                String fordate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                System.out.println("FOR DATE IS---"+ fordate);

                params.put("for_date",fordate);
                params.put("add_limit",newlimit);





                System.out.println("Set Daily Limit params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }

    public void RequoteDialog(String qtyofferedstr,String remaining_quantity,final String id,final String tradeid,final String sellid){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.requote_dialog, null);
        dialogBuilder.setView(dialogView);

        TextView requotetext = (TextView)dialogView.findViewById(R.id.requotetext);
        TextView resendtxt = (TextView)dialogView.findViewById(R.id.resendtxt);

        requotetext.setText("Seller have "+remaining_quantity+" MT currently available for this offer.\n Your Available Buying Limit is "+qtyofferedstr+" MT.");



        int qtyoffered = Integer.parseInt(qtyofferedstr);
        int remaining_quantity_int = Integer.parseInt(remaining_quantity);



        int minimumValue = Math.min(qtyoffered, remaining_quantity_int);
        System.out.println("MINIMUM VALUE IN REQUOTE DIALOG---" + minimumValue);

        ArrayList<Integer> qtyal = new ArrayList<>();
        qtyal.clear();


        for (int i = 50; i <= minimumValue; i = i + 50) {
            qtyal.add(i);
        }
        System.out.println("QTY VALUE ARRAYLIST IN REQUOTE DIALOG---" + qtyal);



        final MaterialBetterSpinner requote_spinner = (MaterialBetterSpinner) dialogView.findViewById(R.id.requote_price_spinner);




        ArrayAdapter<Integer> requoteAdapter = new ArrayAdapter<Integer>(getActivity(), android.R.layout.simple_spinner_dropdown_item, qtyal);
        requoteAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        requote_spinner.setSelection(0);
        requote_spinner.setAdapter(requoteAdapter);
        requoteAdapter.notifyDataSetChanged();



        b = dialogBuilder.create();
        b.show();

        resendtxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(requote_spinner.getText().toString().length()>0) {
                    String requoteval = requote_spinner.getText().toString();
                    System.out.println("REQUOTE SELECTED VALUE IS===" + requoteval);

                if(NetworkUtility.checkConnectivity(getActivity())){


                    String RequoteUrl = APIName.URL+"/buyer/requoteTrade";
                    System.out.println("BUYER GET CURRENT ENQUIRY URL IS---"+ RequoteUrl);
                    RequoteAPI(RequoteUrl,id,tradeid,sellid);


                }
                else{
                    util.dialog(getActivity(), "Please check your internet connection.");
                }
                }
                else
                {
                    Toast.makeText(getActivity(),"Please Select Requote Price",Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    public class Holder {
        TextView cmpnyname,proddesc,gradetxtvw,statustxt,tradeiddesc,status2,qtydesctxtvw;
        ImageView viewofferiv,acceptiv,rejectiv;
        RatingBar ratingBar;



    }

    private void RequoteAPI(String url,final String id,final String tradeid,final String sellid) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF REQUOTE API IS---" + response);



                        JSONObject RequoteJson = null;
                        try {
                            RequoteJson = new JSONObject(response);


                            String statusstr = RequoteJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                b.dismiss();
                                String messagestr = RequoteJson.getString("message");
                                Toast.makeText(getActivity(),messagestr,Toast.LENGTH_SHORT).show();

                                if(NetworkUtility.checkConnectivity(getActivity())){


                                    String BuyerGetCurrentEnquiryUrl = APIName.URL+"/buyer/getCurrentEnq?user_code="+userCodestr;
                                    System.out.println("BUYER GET CURRENT ENQUIRY URL IS---"+ BuyerGetCurrentEnquiryUrl);
                                    BuyerGetCurrentEnquiryAPI(BuyerGetCurrentEnquiryUrl);


                                }
                                else{
                                    util.dialog(getActivity(), "Please check your internet connection.");
                                }

                            }
                            else
                            {

                                String messagestr = RequoteJson.getString("message");
                                Toast.makeText(getActivity(),messagestr,Toast.LENGTH_SHORT).show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured, Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("id",id);
                params.put("sell_id",sellid);
                params.put("trade_id",tradeid);
                params.put("quantity",id);

                System.out.println("requote params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }

    private void BuyerActionAPI(String url) {

        pdia1 = new ProgressDialog(getActivity());
        pdia1.setMessage("Please Wait...");
        pdia1.setCanceledOnTouchOutside(false);
        pdia1.setCancelable(false);
        pdia1.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia1.dismiss();


                        System.out.println("RESPONSE OF BUYER ACCEPT/REJECT API IS---" + response);



                        JSONObject BuyerTodaysOfferJson = null;
                        try {
                            BuyerTodaysOfferJson = new JSONObject(response);


                            String statusstr = BuyerTodaysOfferJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                String msgstr = BuyerTodaysOfferJson.getString("message");
                                Toast.makeText(getActivity(),msgstr,Toast.LENGTH_SHORT).show();



                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                       /* Intent i = new Intent(getActivity(), SellActivity.class);
                                        startActivity(i);*/
                                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.detach(BuyCurrentEnquiriesFragment.this).attach(BuyCurrentEnquiriesFragment.this).commit();


                                    }
                                }, 10);


                            }
                            else
                            {
                                Toast.makeText(getActivity(),response,Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia1.dismiss();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia1.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();



                System.out.println("get buyer current enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }


    public void ViewOfferDialog(String modified_field_str,String delivery_type_str,String delivery_in_days_str,String material_inspection_str,String weighment_type_str,String test_certificate_type_str,String promo_str,String length_str,String categoryid,String transport_str,String loading_str,String gst_str,String insurance_select_str,String insurance_str,String category_grade_str,String size_range_start_str,String size_range_end_str,String q1_title_str,String q1_value_str,String q2_title_str,String q2_value_str,String q3_title_str,String q3_value_str,String q4_title_str,String q4_value_str,String q5_title_str,String q5_value_str,String q6_title_str,String q6_value_str,String q7_title_str,String q7_value_str,String q8_title_str,String q8_value_str,String category_grade_name_str,String rate_type_str,String orginial_rate,String quantity_modified, String isquantity,String tradeidstr,String companyname,String categorydescstr,String subcategorydescstr,String locationdesc,String qtyofferedstr,String ratestr,String terms,String markdownpriceval,String tradestatusval,String isterm,String israte) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.view_buyer_current_enquiries1, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);


        final TextView tradeidtitletxtvw = (TextView)dialogView.findViewById(R.id.tradeidtitletxtvw);
        final TextView cmpnynamedesctxt = (TextView) dialogView.findViewById(R.id.cmpnynamedesctxt);
        final TextView categorydesctxt = (TextView) dialogView.findViewById(R.id.categorydesctxt);
        final TextView subcategorydesctxt = (TextView) dialogView.findViewById(R.id.subcategorydesctxt);
        final TextView locationdesctxt = (TextView) dialogView.findViewById(R.id.locationdesctxt);
        final TextView qtyoffereddesctxtvw = (TextView)dialogView.findViewById(R.id.qtyoffereddesctxtvw);
        final TextView ratedesctxtvw = (TextView)dialogView.findViewById(R.id.ratedesctxtvw);
        final TextView pricetxt1 = (TextView) dialogView.findViewById(R.id.pricetxt1);
        final TextView pricetxt2 = (TextView) dialogView.findViewById(R.id.pricetxt2);
        final TextView pricetxt3 = (TextView) dialogView.findViewById(R.id.pricetxt3);
        final TextView pricetxt4 = (TextView) dialogView.findViewById(R.id.pricetxt4);
        final TextView pricetxt5 = (TextView) dialogView.findViewById(R.id.pricetxt5);
        final TextView pricetxt6 = (TextView) dialogView.findViewById(R.id.pricetxt6);
        final TextView pricetxt7 = (TextView) dialogView.findViewById(R.id.pricetxt7);
        final TextView pricetxt8 = (TextView) dialogView.findViewById(R.id.pricetxt8);
        final TextView termsdesctxt = (TextView) dialogView.findViewById(R.id.termsdesctxt);
        final TextView markdownpricedesctxt = (TextView) dialogView.findViewById(R.id.markdownpricedesctxt);
        final TextView statusdesctxt = (TextView) dialogView.findViewById(R.id.statusdesctxt);
        final TextView revisedpricedesctxt = (TextView) dialogView.findViewById(R.id.revisedpricedesctxt);

        final TextView transportationdesctxt = (TextView) dialogView.findViewById(R.id.transportationdesctxt);
        final TextView loadingdesctxt = (TextView) dialogView.findViewById(R.id.loadingdesctxt);
        final TextView gstdesctxt = (TextView) dialogView.findViewById(R.id.gstdesctxt);
        final TextView insurancedesctxt = (TextView) dialogView.findViewById(R.id.insurancedesctxt);
        final TextView promodesctxt = (TextView) dialogView.findViewById(R.id.promodesctxt);

        final TextView q1title = (TextView) dialogView.findViewById(R.id.q1title);
        final TextView q2title = (TextView) dialogView.findViewById(R.id.q2title);
        final TextView q3title = (TextView) dialogView.findViewById(R.id.q3title);
        final TextView q4title = (TextView) dialogView.findViewById(R.id.q4title);
        final TextView q5title = (TextView) dialogView.findViewById(R.id.q5title);
        final TextView q6title = (TextView) dialogView.findViewById(R.id.q6title);
        final TextView q7title = (TextView) dialogView.findViewById(R.id.q7title);
        final TextView q8title = (TextView) dialogView.findViewById(R.id.q8title);
        final TextView gradedesctxt = (TextView) dialogView.findViewById(R.id.gradedesctxt);

        final TextView lengthtitletxtvw =  (TextView) dialogView.findViewById(R.id.lengthtitletxtvw);
        final TextView lengthdesctxt = (TextView) dialogView.findViewById(R.id.lengthdesctxt);
        final View line16 = (View)dialogView.findViewById(R.id.line16);

        final TextView deliveryindaysdesctxt = (TextView)dialogView.findViewById(R.id.deliveryindaysdesctxt);
        final TextView materialinspectiondesctxt = (TextView)dialogView.findViewById(R.id.materialinspectiondesctxt);
        final TextView weighmentdesctxt = (TextView)dialogView.findViewById(R.id.weighmentdesctxt);
        final TextView testcertificatedesctxt = (TextView)dialogView.findViewById(R.id.testcertificatedesctxt);

        if(delivery_type_str.equalsIgnoreCase("1"))
        {
            deliveryindaysdesctxt.setText("As per Aachar Sanhita");
        }
        if(delivery_type_str.equalsIgnoreCase("2"))
        {
            deliveryindaysdesctxt.setText(delivery_in_days_str+ " days");
        }

        if(material_inspection_str.equalsIgnoreCase("1"))
        {
            materialinspectiondesctxt.setText("Buyer's Place");
        }
        if(material_inspection_str.equalsIgnoreCase("2"))
        {
            materialinspectiondesctxt.setText("Seller's Place");
        }

        if(weighment_type_str.equalsIgnoreCase("1"))
        {
            weighmentdesctxt.setText("Buyer's Place");
        }
        if(weighment_type_str.equalsIgnoreCase("2"))
        {
            weighmentdesctxt.setText("Seller's Place");
        }
        if(weighment_type_str.equalsIgnoreCase("3"))
        {
            weighmentdesctxt.setText("Buyer & Seller's Place");
        }



        if(test_certificate_type_str.equalsIgnoreCase("1"))
        {
            testcertificatedesctxt.setText("Yes");
           // testcertificatedesctxt.setText("Required");
        }
        if(test_certificate_type_str.equalsIgnoreCase("2"))
        {
            testcertificatedesctxt.setText("No");
           // testcertificatedesctxt.setText("Not Required");
        }


        if(length_str.length()!=4) {
         //   lengthdesctxt.setText(length_str+" m");

            line16.setVisibility(View.VISIBLE);
            lengthtitletxtvw.setVisibility(View.VISIBLE);
            lengthdesctxt.setVisibility(View.VISIBLE);

            if(categoryid.equalsIgnoreCase("1")) {

                lengthtitletxtvw.setText("Length");
                lengthdesctxt.setText(length_str+" m");
            }

            if(categoryid.equalsIgnoreCase("2"))
            {
                lengthtitletxtvw.setText("Weight");
                lengthdesctxt.setText(length_str+" kg");
            }
        }
        else
        {
            line16.setVisibility(View.GONE);
            lengthtitletxtvw.setVisibility(View.GONE);
            lengthdesctxt.setVisibility(View.GONE);
        }


        gradedesctxt.setText(category_grade_name_str);
        pricetxt1.setText(q1_value_str);
        pricetxt2.setText(q2_value_str);
        pricetxt3.setText(q3_value_str);
        pricetxt4.setText(q4_value_str);
        pricetxt5.setText(q5_value_str);
        pricetxt6.setText(q6_value_str);
    //    pricetxt7.setText(q7_value_str);
    //    pricetxt8.setText(q8_value_str);

        if(!q7_value_str.equalsIgnoreCase(""))
        {
            pricetxt7.setText(q7_value_str);
        }
        else {
            pricetxt7.setText("NA");
        }

        if(!q8_value_str.equalsIgnoreCase(""))
        {
            pricetxt8.setText(q8_value_str);
        }
        else
        {
            pricetxt8.setText("NA");
        }

        List<String> modified_field_List = Arrays.asList(modified_field_str.split(","));


        if (modified_field_List.contains("-3-"))
        {
            pricetxt1.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-4-"))
        {
            pricetxt2.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-5-"))
        {
            pricetxt3.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-6-"))
        {
            pricetxt4.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-7-"))
        {
            pricetxt5.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-8-"))
        {
            pricetxt6.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-9-"))
        {
            pricetxt7.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-10-"))
        {
            pricetxt8.setTextColor(Color.RED);
        }

        q1title.setText(q1_title_str);
        q2title.setText(q2_title_str);
        q3title.setText(q3_title_str);
        q4title.setText(q4_title_str);
        q5title.setText(q5_title_str);
        q6title.setText(q6_title_str);
        q7title.setText(q7_title_str);
        q8title.setText(q8_title_str);


        if(transport_str.equalsIgnoreCase("1"))
        {
            transportationdesctxt.setText("Seller's End");
        }
        if(transport_str.equalsIgnoreCase("2"))
        {
            transportationdesctxt.setText("Buyer's End");
        }
        if(transport_str.equalsIgnoreCase("3"))
        {
            transportationdesctxt.setText("Mxmart's End");
        }

        if(loading_str.equalsIgnoreCase("NA"))
        {
            loadingdesctxt.setText(loading_str);
        }
        else {
            loadingdesctxt.setText(loading_str + " \u20B9"+"/MT");
        }
        gstdesctxt.setText(gst_str);

        if(insurance_select_str.equalsIgnoreCase("NA"))
        {
            insurancedesctxt.setText("NA");
        }
        else {

            if (insurance_str.equalsIgnoreCase("1")) {
                insurancedesctxt.setText("Seller");
            } else {
                insurancedesctxt.setText("Buyer");
            }
        }


        if(categoryid.equalsIgnoreCase("1")||categoryid.equalsIgnoreCase("2")) {
            subcategorydesctxt.setText(subcategorydescstr);
        }
        else {
            subcategorydesctxt.setText(size_range_start_str + "-" + size_range_end_str);
        }


        if((size_range_start_str.equalsIgnoreCase("null"))&&(size_range_end_str.equalsIgnoreCase("null")))
        {
            subcategorydesctxt.setText("--");
        }

        tradeidtitletxtvw.setText("Trade ID :"+tradeidstr);
        cmpnynamedesctxt.setText(companyname);
        categorydesctxt.setText(categorydescstr);
       // subcategorydesctxt.setText(subcategorydescstr);
        locationdesctxt.setText(locationdesc);


        if(rate_type_str.equalsIgnoreCase("1"))
        {
            ratedesctxtvw.setText(orginial_rate+"(A)");
        }

        if(rate_type_str.equalsIgnoreCase("2"))
        {
            ratedesctxtvw.setText(orginial_rate+"(N)");
        }

        if(rate_type_str.equalsIgnoreCase("3"))
        {
            ratedesctxtvw.setText(orginial_rate+"(R)");
        }


        if(!(terms.length()==4)) {
            termsdesctxt.setText(terms);
        }
        else
        {
            termsdesctxt.setText("--");
        }

        if(!(promo_str.equalsIgnoreCase("0"))){
            promodesctxt.setText(promo_str);

            if((promo_str.equalsIgnoreCase("(null)")))
            {
                promodesctxt.setText("--");
            }
        }
        else
        {
            promodesctxt.setText("--");
        }



        if (isterm.equalsIgnoreCase("1")) {

            //Orange Color Code----------
            termsdesctxt.setTextColor(Color.parseColor("#FFF15922"));
            //termsDtlLbl.textColor = [UIColor colorWithRed:234/255.0f green:112/255.0f blue:45/255.0f alpha:1.0f];
        }
        else if(isterm.equalsIgnoreCase("2"))
        {
            termsdesctxt.setTextColor(Color.RED);
        }
        else
        {
            termsdesctxt.setTextColor(Color.parseColor("#000000"));

        }


     /*   if(israte.equalsIgnoreCase("1")) {
            ratedesctxtvw.setTextColor(Color.parseColor("#FFF15922"));
        }
        else
        {
            ratedesctxtvw.setTextColor(Color.parseColor("#000000"));
        }*/


        if (!israte.equalsIgnoreCase("1")) {
            if (!markdownpriceval.equalsIgnoreCase("0.00")) {
                // mrkDwnPrceDescLbl.textColor = [UIColor redColor];

                System.out.println("israte LOOP1------");

           //     Toast.makeText(getActivity(),"israte LOOP1A------",Toast.LENGTH_SHORT).show();


              //  double diff1 = Double.parseDouble(orginial_rate) - Double.parseDouble(ratestr);

                markdownpricedesctxt.setText(markdownpriceval);

                //red COLOR
                markdownpricedesctxt.setTextColor(Color.parseColor("#ff0000"));
                //Orange
            //    markdownpricedesctxt.setTextColor(Color.parseColor("#FFF15922"));
            }
            else
            {
                markdownpricedesctxt.setText(markdownpriceval);
          //      Toast.makeText(getActivity(),"israte LOOP1B------",Toast.LENGTH_SHORT).show();
                //Black COLOR
                markdownpricedesctxt.setTextColor(Color.parseColor("#000000"));
            }
        }else if(israte.equalsIgnoreCase("1")){
            if (Double.parseDouble(ratestr)>Double.parseDouble(orginial_rate)) {

                double diff = Double.parseDouble(ratestr) - Double.parseDouble(orginial_rate);

                markdownpricedesctxt.setText("(+)"+Double.toString(diff)+"0");

                System.out.println("israte LOOP2------");
                //Green color code-----
               // markdownpricedesctxt.setTextColor(Color.GREEN);
                markdownpricedesctxt.setTextColor(Color.parseColor("#3EBA3E"));

             //   Toast.makeText(getActivity(),"israte LOOP2------",Toast.LENGTH_SHORT).show();

                // mrkDwnPrceDescLbl.text = [NSString stringWithFormat:@"(+)%d",[[[self.sellEnquiryArray objectAtIndex:indexPath.row]objectForKey:@"rate"] intValue]-[[[self.sellEnquiryArray objectAtIndex:indexPath.row]objectForKey:@"orginial_rate"] intValue]];
                //  mrkDwnPrceDescLbl.textColor = [UIColor colorWithRed:7/255.0f green:158/255.0f blue:4/255.0f alpha:1.0f];
            }else{

                System.out.println("ORIGINAL RATE------"+ orginial_rate);
                System.out.println("RATE------"+ ratestr);

                double diff1 = Double.parseDouble(orginial_rate) - Double.parseDouble(ratestr);

                markdownpricedesctxt.setText(Double.toString(diff1)+"0");
                //red COLOR
                markdownpricedesctxt.setTextColor(Color.parseColor("#FFF15922"));

                System.out.println("israte LOOP3------");

             //   Toast.makeText(getActivity(),"israte LOOP3------",Toast.LENGTH_SHORT).show();
                //  mrkDwnPrceDescLbl.text = [NSString stringWithFormat:@"%d",[[[self.sellEnquiryArray objectAtIndex:indexPath.row]objectForKey:@"orginial_rate"] intValue]-[[[self.sellEnquiryArray objectAtIndex:indexPath.row]objectForKey:@"rate"] intValue]];
                //  mrkDwnPrceDescLbl.textColor = [UIColor colorWithRed:234/255.0f green:112/255.0f blue:45/255.0f alpha:1.0f];
            }
        }


        if (!israte.equalsIgnoreCase("1")) {
            if (!markdownpriceval.equalsIgnoreCase("0.00")){

                double difference = Double.parseDouble(ratestr) - Double.parseDouble(markdownpriceval);


                revisedpricedesctxt.setText(Double.toString(difference)+"0");
                //Orange Color Code
                revisedpricedesctxt.setTextColor(Color.parseColor("#FFF15922"));

                //revicedPrceDescLbl.text = [NSString stringWithFormat:@"%d", [[[self.sellEnquiryArray objectAtIndex:indexPath.row]objectForKey:@"rate"] intValue]-[[[self.sellEnquiryArray objectAtIndex:indexPath.row]objectForKey:@"quoted_rate"] intValue]];
                //revicedPrceDescLbl.textColor =  [UIColor redColor];
            }else{
                // revicedPrceDescLbl.text = @"--";
                revisedpricedesctxt.setText("--");
            }
        }else{

            revisedpricedesctxt.setText(ratestr);
            //Orange Color
            revisedpricedesctxt.setTextColor(Color.RED);
            /*revicedPrceDescLbl.text =[NSString stringWithFormat:@"%@", [[self.sellEnquiryArray objectAtIndex:indexPath.row]objectForKey:@"rate"]];
            revicedPrceDescLbl.textColor = [UIColor colorWithRed:234/255.0f green:112/255.0f blue:45/255.0f alpha:1.0f];*/

        }



        //   markdownpricedesctxt.setText(markdownpriceval);
        statusdesctxt.setText(tradestatusval);


        if(!isquantity.equalsIgnoreCase("1"))
        {
            qtyoffereddesctxtvw.setText(qtyofferedstr);

        }
        else if(isquantity.equalsIgnoreCase("1"))
        {
            qtyoffereddesctxtvw.setTextColor(Color.RED);
            qtyoffereddesctxtvw.setText(quantity_modified);
        }





        final AlertDialog b = dialogBuilder.create();
        b.show();


        ImageView ic_close = (ImageView)dialogView.findViewById(R.id.ic_close);
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }


}
