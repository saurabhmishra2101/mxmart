package com.smtc.mxmart.Fragment;

/**
 * Created by 10161 on 11/14/2017.
 */
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smtc.mxmart.APIName;
import com.smtc.mxmart.BuyerPaymentDetailsActivity;
import com.smtc.mxmart.LoginActivity;
import com.smtc.mxmart.NetworkUtility;
import com.smtc.mxmart.R;
import com.smtc.mxmart.SingletonActivity;
import com.smtc.mxmart.UtilsDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;

public class SellCurrentEnquiriesFragment extends Fragment {

    Typeface source_sans_pro_normal;
    String userCodestr,mobilenumstr,sellenquiry;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    UtilsDialog util = new UtilsDialog();
    ListView selltodayscurrentenquirylistvw;
    CustomAdap customadap;
    Context c;
    TextView nocurrentenqtxt;
    AlertDialog b,b1;
    ProgressDialog pdia,pdia1;
    String revisedpricestr;
    TextView exceedtxt;
    TextView exceedtxt1,youneedtotxtvw;
    EditText qty_desc_edt;
    RelativeLayout sbmtrelative;
    private boolean _hasLoadedOnce= false; // your boolean field
    private ProgressBar bar;
    Handler handler = new Handler();

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


            View view = inflater.inflate(R.layout.fragment_sell_current_enquiries, container, false);

           // SingletonActivity.isNewEnquiryClicked= false;


            System.out.println("ST USER PROF JSON IN EDIT BUSINESS DETAILS FRAGMENT---" + SingletonActivity.stuserprofjson);

            source_sans_pro_normal = Typeface.createFromAsset(getActivity().getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

            nocurrentenqtxt = (TextView)view.findViewById(R.id.noenquiriesfoundtxt);
            bar = (ProgressBar)view.findViewById(R.id.progressBar);

            SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            userCodestr = prefs.getString("user_code", null);
            mobilenumstr = prefs.getString("mobile_num", null);
            sellenquiry = prefs.getString("sellenquiry",null);



            selltodayscurrentenquirylistvw = (ListView)view.findViewById(R.id.sellcurrentenquirieslistvw);

            if (NetworkUtility.checkConnectivity(getActivity())) {


                String SellerGetCurrentEnquiryUrl = APIName.URL + "/seller/getCurrentEnq?user_code=" + userCodestr;
                System.out.println("SELLER GET CURRENT ENQUIRY URL IS---" + SellerGetCurrentEnquiryUrl);
                SellerGetCurrentEnquiryAPI(SellerGetCurrentEnquiryUrl);


            } else {
                util.dialog(getActivity(), "Please check your internet connection.");
            }

            return view;

        }


    private void SellerGetCurrentEnquiryAPI(String url) {

    /*    pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();
*/
      //  getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
         //       WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        bar.setVisibility(View.VISIBLE);

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {

                        //pdia.dismiss();
                        bar.setVisibility(View.GONE);
                       // getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                bar.setVisibility(View.GONE);

                                System.out.println("RESPONSE OF SELLER GET CURRENT ENQUIRY API IS---" + response);



                                JSONObject SellerCurrentEnquiryJson = null;
                                try {
                                    SellerCurrentEnquiryJson = new JSONObject(response);


                                    String statusstr = SellerCurrentEnquiryJson.getString("status");

                                    if(statusstr.equalsIgnoreCase("true")) {


                                        selltodayscurrentenquirylistvw.setVisibility(View.VISIBLE);
                                        nocurrentenqtxt.setVisibility(View.INVISIBLE);

                                        JSONArray SellerCurrentEnquiryJsonArray = SellerCurrentEnquiryJson.getJSONArray("ce_Offer");
                                        System.out.println("SELLER CURRENT ENQUIRY JSONARRAY IS---" + SellerCurrentEnquiryJsonArray);



                                        if(getActivity()!=null) {

                                            customadap = new CustomAdap(getActivity(), SellerCurrentEnquiryJsonArray);
                                            selltodayscurrentenquirylistvw.setAdapter(customadap);
                                        }


                                    }
                                    else
                                    {
                                        String messagestr = SellerCurrentEnquiryJson.getString("message");
                                        //Toast.makeText(getActivity(),messagestr,Toast.LENGTH_SHORT).show();

                                        selltodayscurrentenquirylistvw.setVisibility(View.INVISIBLE);
                                        nocurrentenqtxt.setVisibility(View.VISIBLE);
                                        nocurrentenqtxt.setText(messagestr);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                   // pdia.dismiss();
                                   // getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            //dialog.dismiss();
                                            bar.setVisibility(View.GONE);

                                        }
                                    }, 3000); // 3000 milliseconds delay

                                }


                            }
                        }, 3000); // 3000 milliseconds delay





                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                       //  pdia.dismiss();

                      // getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //dialog.dismiss();
                                bar.setVisibility(View.GONE);

                            }
                        }, 3000); // 3000 milliseconds delay


                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get seller current enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }




    private void EditSellerEnquiriesAPI(String url,final String tradeid,final String id,final String rate,final String tncstr,final String type_desc_edt_str,final String tnc_desc_edt_str,final String sellidstr,final String quantitystr,final String qty_desc_edt_str) {

        pdia1 = new ProgressDialog(getActivity());
        pdia1.setMessage("Please Wait...");
        pdia1.setCanceledOnTouchOutside(false);
        pdia1.setCancelable(false);
        pdia1.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia1.dismiss();



                        System.out.println("RESPONSE OF SELLER EditSellerEnquiriesAPI IS---" + response);



                        JSONObject EditSellerEnquiriesAPIJson = null;
                        try {
                            EditSellerEnquiriesAPIJson = new JSONObject(response);


                            String statusstr = EditSellerEnquiriesAPIJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                String msgstr = EditSellerEnquiriesAPIJson.getString("message");
                                Toast.makeText(getActivity(),msgstr,Toast.LENGTH_SHORT).show();

                              /*  String id  = null;
                                try {
                                    id = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("id");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }*/
                               /* String acceptstatus = "2";
                                System.out.println("STATUS PASSED ON CLICK OF ACCEPT==="+ acceptstatus);

                                if(NetworkUtility.checkConnectivity(getActivity())){

                                    sbmtrelative.setEnabled(false);

                                    String SellerAcceptUrl = APIName.URL+"/seller/changeTradeStatus?id="+id+"&status="+acceptstatus;
                                    System.out.println("SELLER ACCEPT URL IS---"+ SellerAcceptUrl);
                                    SellerActionAPI(SellerAcceptUrl);


                                }
                                else{
                                    util.dialog(getActivity(), "Please check your internet connection.");
                                }*/


                            }
                            else
                            {
                                Toast.makeText(getActivity(),EditSellerEnquiriesAPIJson.getString("message"),Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia1.dismiss();


                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia1.dismiss();



                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("terms_modified",tnc_desc_edt_str);
                params.put("terms",tncstr);
                params.put("rate_modified",type_desc_edt_str);
                params.put("rate",rate);
                params.put("sell_id",sellidstr);
                params.put("quantity",quantitystr);
                params.put("quantity_modified",qty_desc_edt_str);

                System.out.println("EditSellerEnquiriesAPI params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }


    private void SellerActionAPI(String url) {

        pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();


        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                     pdia.dismiss();




                        System.out.println("RESPONSE OF SELLER ACCEPT/REJECT API IS---" + response);



                        JSONObject SellerActionJson = null;
                        try {
                            SellerActionJson = new JSONObject(response);


                            String statusstr = SellerActionJson.getString("status");
                          //  b.dismiss();


                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                String msgstr = SellerActionJson.getString("message");
                                Toast.makeText(getActivity(),msgstr,Toast.LENGTH_SHORT).show();

                          //      b.dismiss();
/*

                                if (NetworkUtility.checkConnectivity(getActivity())) {


                                    String SellerGetCurrentEnquiryUrl = APIName.URL + "/seller/getCurrentEnq?user_code=" + userCodestr;
                                    System.out.println("SELLER GET CURRENT ENQUIRY URL IS---" + SellerGetCurrentEnquiryUrl);
                                    SellerGetCurrentEnquiryAPI(SellerGetCurrentEnquiryUrl);


                                } else {
                                    util.dialog(getActivity(), "Please check your internet connection.");
                                }
*/


                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.detach(SellCurrentEnquiriesFragment.this).attach(SellCurrentEnquiriesFragment.this).commit();

                            }
                            else
                            {
                               // Toast.makeText(getActivity(),response,Toast.LENGTH_SHORT).show();

                                String msgstr = SellerActionJson.getString("message");
                                Toast.makeText(getActivity(),msgstr,Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        //    b.dismiss();
                            pdia.dismiss();

                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia.dismiss();
                      //  b.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get seller current enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }

    private class CustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        private JSONArray SellCurrentEnquiryJSONArray;





        public CustomAdap(Context mainActivity,JSONArray SellCurrentEnquiryJSONArray)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.SellCurrentEnquiryJSONArray = SellCurrentEnquiryJSONArray;

            inflater = (LayoutInflater)c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return SellCurrentEnquiryJSONArray.length();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub



            final Holder holder = new Holder();

            final View rowView;


            rowView = inflater.inflate(R.layout.seller_current_enquiries_row, null);

            holder.cmpnyname = (TextView)rowView.findViewById(R.id.cmpnynametxtvw);
            holder.ratingBar = (RatingBar)rowView.findViewById(R.id.ratingBar);
            holder.tradeiddesc = (TextView)rowView.findViewById(R.id.tradeiddesctxtvw);
            holder.proddesc = (TextView) rowView.findViewById(R.id.categorytxtvw);
            holder.qtydesctxtvw = (TextView) rowView.findViewById(R.id.qtydesctxtvw);
            holder.acceptiv = (ImageView)rowView.findViewById(R.id.acceptiv);
            holder.rejectiv = (ImageView)rowView.findViewById(R.id.rejectiv);
            holder.editiv  = (ImageView)rowView.findViewById(R.id.editiv);
            holder.statustxt = (TextView)rowView.findViewById(R.id.statusdesctxtvw);
            holder.gradetxtvw = (TextView)rowView.findViewById(R.id.gradetxtvw);




            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        String tradeidstr = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("trade_id");
                        String companyname = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("buyer_company_name");
                        String categorydesc = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("category_name");
                        String subcategorydesc = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("sub_category_name");
                        String locationdesc = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("buyer_city");
                        String qtyofferedstr = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity");
                        String ratestr = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("rate");
                      /*  String q1val = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q1");
                        String q2val = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q2");
                        String q3val = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q3");
                        String q4val = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q4");
                        String q5val = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q5");
                        String q6val = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q6");
                        String q7val = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q7");
                        String q8val = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q8");*/
                        String tncval = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("terms");
                        String markdownpriceval = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("quoted_rate");
                        String tradestatusval = holder.statustxt.getText().toString();
                        String istermval = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("isterm");
                        String isquantityval =  SellCurrentEnquiryJSONArray.getJSONObject(position).getString("isquantity");
                        String quantity_modified_val =  SellCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity_modified");
                        String israteval =  SellCurrentEnquiryJSONArray.getJSONObject(position).getString("israte");
                        String originalratestr = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("orginial_rate");
                        String rate_type_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("rate_type");
                        String category_grade_name_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("category_grade_name");
                        String category_grade_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("category_grade");
                        String size_range_start_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("size_range_start");
                        String size_range_end_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("size_range_end");
                        String q1_title_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q1_title");
                        String q1_value_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q1_value");
                        String q2_title_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q2_title");
                        String q2_value_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q2_value");
                        String q3_title_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q3_title");
                        String q3_value_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q3_value");
                        String q4_title_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q4_title");
                        String q4_value_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q4_value");
                        String q5_title_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q5_title");
                        String q5_value_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q5_value");
                        String q6_title_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q6_title");
                        String q6_value_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q6_value");
                        String q7_title_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q7_title");
                        String q7_value_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q7_value");
                        String q8_title_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q8_title");
                        String q8_value_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("q8_value");
                        String transport_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("transport");
                        String loading_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("loading");
                        String gst_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("gst");
                        String insurance_select_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("insurance_select");
                        String insurance_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("insurance");
                        String categoryid = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("category_id");
                        String length_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("length");
                        String promo_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("promo_code");
                        String modified_field_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("modified_field");
                        String revised_price_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("revised_price");
                        String quoted_rate_colour = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("quoted_rate_colour");
                        String revised_rate_color = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("revised_price_colour");

                        JSONArray detailJSONArray = SellCurrentEnquiryJSONArray.getJSONObject(position).getJSONArray("detail");


                        final String delivery_type_str = detailJSONArray.getJSONObject(0).getString("delivery_type");
                        final String delivery_in_days_str = detailJSONArray.getJSONObject(0).getString("delivery_in_days");
                        final String material_inspection_str = detailJSONArray.getJSONObject(0).getString("material_inspection_type");
                        final String weightment_type_str = detailJSONArray.getJSONObject(0).getString("weightment_type");
                        final String test_certificate_type_str = detailJSONArray.getJSONObject(0).getString("test_certificate_type");


                        ViewOfferDialog(revised_price_str,quoted_rate_colour,revised_rate_color,modified_field_str,delivery_type_str,delivery_in_days_str,material_inspection_str,weightment_type_str,test_certificate_type_str,promo_str,length_str,categoryid,transport_str,loading_str,gst_str,insurance_select_str,insurance_str,category_grade_str,size_range_start_str,size_range_end_str,q1_title_str,q1_value_str,q2_title_str,q2_value_str,q3_title_str,q3_value_str,q4_title_str,q4_value_str,q5_title_str,q5_value_str,q6_title_str,q6_value_str,q7_title_str,q7_value_str,q8_title_str,q8_value_str,category_grade_name_str,rate_type_str,originalratestr,israteval,quantity_modified_val,isquantityval,istermval,tradeidstr,companyname,categorydesc,subcategorydesc,locationdesc,qtyofferedstr,ratestr,tncval,markdownpriceval,tradestatusval);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


            holder.acceptiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (sellenquiry.equalsIgnoreCase("0")) {

                        try {
                            String rate_type_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("rate_type");
                            String rate_str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("rate");
                            String terms_Str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("terms");
                            String isterms_Str = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("isterm");
                            String tradeidstr = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("trade_id");
                            String sellidstr = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("sell_id");
                            String quantitystr = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity");
                            String idstr = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("id");
                            String qtyremainingstr = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity_remaining");
                            String postedqtystr = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity_posted");
                            String markdownpriceval = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("quoted_rate");

                           // holder.acceptiv.setEnabled(false);

                            EditSellCurrentEnquiriesDialog(markdownpriceval, postedqtystr, qtyremainingstr, idstr, rate_type_str, rate_str, terms_Str, isterms_Str, tradeidstr, sellidstr, quantitystr);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                    }


                   /* new AlertDialog.Builder(getActivity())
                            .setTitle("Alert")
                            .setMessage("Do you really want to Accept Offer?")
                            // .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    String id  = null;
                                    try {
                                        id = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("id");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    String acceptstatus = "2";
                                    System.out.println("STATUS PASSED ON CLICK OF ACCEPT==="+ acceptstatus);

                                    if(NetworkUtility.checkConnectivity(getActivity())){


                                        String SellerAcceptUrl = APIName.URL+"/seller/changeTradeStatus?id="+id+"&status="+acceptstatus;
                                        System.out.println("SELLER ACCEPT URL IS---"+ SellerAcceptUrl);
                                        SellerActionAPI(SellerAcceptUrl);


                                    }
                                    else{
                                        util.dialog(getActivity(), "Please check your internet connection.");
                                    }
                                }})
                            .setNegativeButton(android.R.string.no, null).show();
                            */

                }
            });

            holder.rejectiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (sellenquiry.equalsIgnoreCase("0")) {

                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setMessage("Do you really want to Reject Offer?")
                                // .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {


                                        String id = null;
                                        try {
                                            id = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("id");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        String rejectstatus = "1";
                                        System.out.println("STATUS PASSED ON CLICK OF REJECT===" + rejectstatus);


                                        if (NetworkUtility.checkConnectivity(getActivity())) {

                                            holder.rejectiv.setEnabled(false);



                                            String SellerAcceptUrl = APIName.URL + "/seller/changeTradeStatus?id=" + id + "&status=" + rejectstatus;
                                            System.out.println("SELLER REJECT URL IS---" + SellerAcceptUrl);
                                            SellerActionAPI(SellerAcceptUrl);


                                        } else {
                                            util.dialog(getActivity(), "Please check your internet connection.");
                                        }


                                    }
                                })
                                .setNegativeButton(android.R.string.no, null).show();
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            holder.editiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });




            try {
                holder.cmpnyname.setText(SellCurrentEnquiryJSONArray.getJSONObject(position).getString("buyer_company_name"));
                holder.ratingBar.setRating(Float.parseFloat(SellCurrentEnquiryJSONArray.getJSONObject(position).getString("rating")));
                holder.tradeiddesc.setText(SellCurrentEnquiryJSONArray.getJSONObject(position).getString("trade_id"));
                holder.proddesc.setText(SellCurrentEnquiryJSONArray.getJSONObject(position).getString("category_name"));
                holder.gradetxtvw.setText(SellCurrentEnquiryJSONArray.getJSONObject(position).getString("category_grade_name"));

                String trade_status = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("trade_status");
                String trade_flag = SellCurrentEnquiryJSONArray.getJSONObject(position).getString("trade_flag");

                //New Enhancement Condition for listview--------
                if (SellCurrentEnquiryJSONArray.getJSONObject(position).getString("isquantity").equalsIgnoreCase("1")) {

                    //Orange Color code

                    holder.qtydesctxtvw.setTextColor(Color.parseColor("#FFF15922"));
                    holder.qtydesctxtvw.setText(SellCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity_modified"));
                   /* qtyDetailLbl.textColor = [UIColor colorWithRed:234/255.0f green:112/255.0f blue:45/255.0f alpha:1.0f];
                    qtyDetailLbl.text = [NSString stringWithFormat:@"%@",[[self.sellEnquiryArray objectAtIndex:indexPath.row]objectForKey:@"quantity_modified"]];*/
                }
                else
                {
                   // holder.qtydesctxtvw.setTextColor(Color.parseColor("#000000"));
                    holder.qtydesctxtvw.setText(SellCurrentEnquiryJSONArray.getJSONObject(position).getString("quantity"));
                }


                System.out.println("TRADE STATUS-----"+ trade_status);
                System.out.println("TRADE FLAG-----"+ trade_flag);



                //---For displaying action buttons--------->

                if(trade_flag.equalsIgnoreCase("0")){

                    if(trade_status.equalsIgnoreCase("Trade Removed") || trade_status.equalsIgnoreCase("Trade Modified")){
                        //empty - (Others)

                        System.out.println("IN IF LOOP OF TRADE FLAG 0");

                        holder.acceptiv.setVisibility(View.INVISIBLE);
                        holder.rejectiv.setVisibility(View.INVISIBLE);
                        holder.editiv.setVisibility(View.INVISIBLE);
                        holder.acceptiv.setEnabled(false);
                        holder.rejectiv.setEnabled(false);
                        holder.editiv.setEnabled(false);
                    }else{
		/* Accept & Reject Button */
                        //   check and cross icon - (Deal Requested)

                        //holder.acceptiv.setBackgroundResource(R.mipmap.ic_accept);
                        holder.acceptiv.setBackgroundResource(R.drawable.accept_drawable);
                        holder.rejectiv.setBackgroundResource(R.drawable.reject_drawable);

                      //  holder.rejectiv.setBackgroundResource(R.mipmap.ic_reject);
                        holder.editiv.setBackgroundResource(R.mipmap.ic_edit);
                        holder.acceptiv.setVisibility(View.VISIBLE);
                        holder.rejectiv.setVisibility(View.VISIBLE);
                        holder.editiv.setVisibility(View.INVISIBLE);
                        holder.acceptiv.setEnabled(true);
                        holder.rejectiv.setEnabled(true);
                        holder.editiv.setEnabled(true);


                    }

                }else if(trade_flag.equalsIgnoreCase("1")){
                    // empty - (Seller Rejected)
                    holder.acceptiv.setVisibility(View.INVISIBLE);
                    holder.rejectiv.setVisibility(View.INVISIBLE);
                    holder.acceptiv.setEnabled(false);
                    holder.rejectiv.setEnabled(false);

                }else if(trade_flag.equalsIgnoreCase("2")){

                    if(trade_status.equalsIgnoreCase("Trade Removed") || trade_status.equalsIgnoreCase("Trade Modified")){
                        //  empty - (Other)
                        holder.acceptiv.setVisibility(View.INVISIBLE);
                        holder.rejectiv.setVisibility(View.INVISIBLE);
                        holder.acceptiv.setEnabled(false);
                        holder.rejectiv.setEnabled(false);
                    }else{
                        // Waiting - (Seller Accepted)
                        holder.acceptiv.setBackgroundResource(R.mipmap.ic_waiting);
                        holder.acceptiv.setVisibility(View.VISIBLE);
                        holder.rejectiv.setVisibility(View.INVISIBLE);
                        holder.acceptiv.setEnabled(false);
                        holder.rejectiv.setEnabled(false);

                    }
                }else if(trade_flag.equalsIgnoreCase("3")){
                    //  empty - (Buyer Denied)
                    holder.acceptiv.setVisibility(View.INVISIBLE);
                    holder.rejectiv.setVisibility(View.INVISIBLE);
                    holder.acceptiv.setEnabled(false);
                    holder.rejectiv.setEnabled(false);

                }else if(trade_flag.equalsIgnoreCase("4")){
                    //green icon - (SP. Generated)
                    holder.acceptiv.setBackgroundResource(R.mipmap.ic_saudapatra);
                    holder.acceptiv.setVisibility(View.VISIBLE);
                    holder.rejectiv.setVisibility(View.INVISIBLE);
                    holder.editiv.setVisibility(View.INVISIBLE);
                    holder.acceptiv.setEnabled(false);
                    holder.rejectiv.setEnabled(false);
                    holder.editiv.setEnabled(false);

                }else if(trade_flag.equalsIgnoreCase("5")){
                    //  empty - (limit Reached)
                    holder.acceptiv.setVisibility(View.INVISIBLE);
                    holder.rejectiv.setVisibility(View.INVISIBLE);
                    holder.acceptiv.setEnabled(false);
                    holder.rejectiv.setEnabled(false);

                }else if(trade_flag.equalsIgnoreCase("6")){
                    //  green icon - (Deal Closed)
                    //holder.acceptiv.setBackgroundResource(R.mipmap.ic_accept);
                    holder.acceptiv.setBackgroundResource(R.drawable.accept_drawable);

                    holder.acceptiv.setVisibility(View.INVISIBLE);
                    holder.rejectiv.setVisibility(View.INVISIBLE);
                    holder.acceptiv.setEnabled(true);
                    holder.rejectiv.setEnabled(false);
                }

                String statusstr = "";

                //For Status display condition---------->
                if(trade_flag.equalsIgnoreCase("0")){

                    if(trade_status.equalsIgnoreCase("Trade Removed")){
                        statusstr = "Trade Deleted";

                        //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);
                      /*  holder.statustxt.setText(splitStr[0]);
                        holder.status2.setText(splitStr[1]);*/
                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));

                    }else if(trade_status.equalsIgnoreCase("Trade Modified")){
                        statusstr = "Trade Modified";
                        //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);
                      /*  holder.statustxt.setText(splitStr[0]);
                        holder.status2.setText(splitStr[1]);*/
                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));

                    }else{
                        statusstr = "Pending";

                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);
                        //holder.statustxt.setText(splitStr[0]);
                        //      holder.status2.setText(splitStr[1]);
                        holder.statustxt.setTextColor(Color.parseColor("#8E8E8E"));
                        //     holder.status2.setTextColor(Color.parseColor("#8E8E8E"));
                    }

                }else if(trade_flag.equalsIgnoreCase("1")){

                    if(trade_status.equalsIgnoreCase("Trade Removed")){
                        statusstr = "Trade Deleted";

                        //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);
                       /* holder.statustxt.setText(splitStr[0]);
                        holder.status2.setText(splitStr[1]);*/
                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));

                    }else if(trade_status.equalsIgnoreCase("Trade Modified")){
                        statusstr = "Trade Modified";

                        //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);
                      /*  holder.statustxt.setText(splitStr[0]);
                        holder.status2.setText(splitStr[1]);*/
                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));


                    }else{
                        statusstr = "Rejected";   //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);
                      //  holder.statustxt.setText(splitStr[0]);
                        //    holder.status2.setText(splitStr[1]);
                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));
                        //    holder.status2.setTextColor(Color.parseColor("#E24B4B"));



                    }

                }else if(trade_flag.equalsIgnoreCase("2")){

                    if(trade_status.equalsIgnoreCase("Trade Removed")){
                        statusstr = "Trade Deleted";

                        //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);
                      /*  holder.statustxt.setText(splitStr[0]);
                        holder.status2.setText(splitStr[1]);*/
                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));


                    }else if(trade_status.equalsIgnoreCase("Trade Modified")){
                        statusstr = "Trade Modified";

                        //RED
                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);
                       /* holder.statustxt.setText(splitStr[0]);
                        holder.status2.setText(splitStr[1]);*/
                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));



                    }else{
                        statusstr = "Accepted";

                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);
                       // holder.statustxt.setText(splitStr[0]);
                        //       holder.status2.setText(splitStr[1]);
                        holder.statustxt.setTextColor(Color.parseColor("#F0A33F"));
                        //       holder.status2.setTextColor(Color.parseColor("#F0A33F"));
                    }

                }else if(trade_flag.equalsIgnoreCase("3")){

                    if(trade_status.equalsIgnoreCase("Trade Removed")){
                        statusstr = "Trade Deleted";

                        //RED

                        String[] splitStr = statusstr.split("\\s+");
                     /*   holder.statustxt.setText(splitStr[0]);
                        holder.status2.setText(splitStr[1]);*/
                        holder.statustxt.setText(statusstr);
                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));



                    }else if(trade_status.equalsIgnoreCase("Trade Modified")){
                        statusstr = "Trade Modified";


                        String[] splitStr = statusstr.split("\\s+");
                       /* holder.statustxt.setText(splitStr[0]);
                        holder.status2.setText(splitStr[1]);*/
                        holder.statustxt.setText(statusstr);
                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));

                    }else{
                        statusstr ="Buyer Denied";


                        String[] splitStr = statusstr.split("\\s+");
                        holder.statustxt.setText(statusstr);
                     /*   holder.statustxt.setText(splitStr[0]);
                        holder.status2.setText(splitStr[1]);*/
                        holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));


                    }

                }else if(trade_flag.equalsIgnoreCase("4")){

                    statusstr = "SP. Generated";

                    String[] splitStr = statusstr.split("\\s+");
                    holder.statustxt.setText(statusstr);
                   /* holder.status2.setText(splitStr[1]);
                    holder.statustxt.setText(splitStr[0]);*/

                    holder.statustxt.setTextColor(Color.parseColor("#009E55"));



                }else if(trade_flag.equalsIgnoreCase("5")){

                    //statusstr = "Limit Reached";
                    statusstr = "Buyer Denied";


                    //RED

                    String[] splitStr = statusstr.split("\\s+");
                   /* holder.statustxt.setText(splitStr[0]);
                    holder.status2.setText(splitStr[1]);*/
                    holder.statustxt.setText(statusstr);
                    holder.statustxt.setTextColor(Color.parseColor("#E24B4B"));



                }else if(trade_flag.equalsIgnoreCase("6")){

                    statusstr = "Deal Closed";

                    //RED

                    String[] splitStr = statusstr.split("\\s+");
                  /*  holder.statustxt.setText(splitStr[0]);
                    holder.status2.setText(splitStr[1]);*/
                    holder.statustxt.setText(statusstr);
                    holder.statustxt.setTextColor(Color.parseColor("#009E55"));



                }






            } catch (JSONException e) {
                e.printStackTrace();
            }


            return rowView;

        }


    }

    public void EditRemainingQuantityDialog(final String qtyremainingstr,final String sellidstr,final String postedqty,final String quantitystr,final String tradeid){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.seller_edit_remaining_quantity_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final EditText posted_qty_desc_edt =  (EditText)dialogView.findViewById(R.id.posted_qty_desc_edt);
        final EditText rem_qty_desc_edt = (EditText)dialogView.findViewById(R.id.current_rem_qty_desc_edt);
        final EditText qtytobeincreaseddescedt = (EditText)dialogView.findViewById(R.id.qtytobeincreaseddescedt);
        final EditText finallyupdatedqtydescedt = (EditText)dialogView.findViewById(R.id.finallyupdatedqtydescedt);
        final TextView buyerrequestesqtytxt = (TextView)dialogView.findViewById(R.id.buyerrequestesqtytxt);

    //    final TextView savetxtvw = (TextView)dialogView.findViewById(R.id.savetxtvw);
    //    TextView canceltxtvw = (TextView)dialogView.findViewById(R.id.canceltxtvw);

        final RelativeLayout sbmtrelative = (RelativeLayout)dialogView.findViewById(R.id.sbmtrelative);
        final RelativeLayout cancelrelative = (RelativeLayout)dialogView.findViewById(R.id.cancelrelative);
        final TextView sbmttext = (TextView)dialogView.findViewById(R.id.sbmttext);


        posted_qty_desc_edt.setText(postedqty);
        rem_qty_desc_edt.setText(qtyremainingstr);
        buyerrequestesqtytxt.setText("Buyer Requested Quantity: "+quantitystr);

        float qtytobeincreasedval = Float.parseFloat(quantitystr) - Float.parseFloat(qtyremainingstr);
        DecimalFormat dfs = new DecimalFormat("#.000");
        qtytobeincreaseddescedt.setText(dfs.format(qtytobeincreasedval));


        qtytobeincreaseddescedt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable edt) {

                String temp = edt.toString ();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 3) {
                    edt.delete(posDot + 4, posDot + 5);
                }
            }
        });


        float finalupdatedqty = Float.parseFloat(qtyremainingstr) + qtytobeincreasedval;
        DecimalFormat df = new DecimalFormat("#.000");
        finallyupdatedqtydescedt.setText(df.format(finalupdatedqty));


        if(Float.parseFloat(rem_qty_desc_edt.getText().toString())>0) {
            float remaining_qty_int_val = Float.parseFloat(rem_qty_desc_edt.getText().toString());
            float qtyremainingintval = Float.parseFloat(qtyremainingstr);



            if (remaining_qty_int_val < qtyremainingintval) {

              //  rem_qty_desc_edt.setTextColor(Color.parseColor("#FFF15922"));

             //   savetxtvw.setTextColor(Color.parseColor("#696969"));
              //  savetxtvw.setEnabled(false);

                sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                sbmttext.setTextColor(Color.parseColor("#808080"));
                sbmtrelative.setEnabled(false);

            } else {

             //   rem_qty_desc_edt.setTextColor(Color.parseColor("#000000"));

              //  savetxtvw.setTextColor(Color.parseColor("#FFF15922"));
              // savetxtvw.setEnabled(true);

                sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                sbmttext.setTextColor(Color.WHITE);
                sbmtrelative.setEnabled(true);

            }
        }

        qtytobeincreaseddescedt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {

                    float remaining_qty_float_val = Float.parseFloat(rem_qty_desc_edt.getText().toString());
                    float qtytobeincreasedval = Float.parseFloat(quantitystr) - Float.parseFloat(qtyremainingstr);

                    float finallyupdtedqtysum = remaining_qty_float_val + Float.parseFloat(qtytobeincreaseddescedt.getText().toString());

                    DecimalFormat dfs = new DecimalFormat("#.000");
                    finallyupdatedqtydescedt.setText(dfs.format(finallyupdtedqtysum));

                    //finallyupdatedqtydescedt.setText(Float.toString(finallyupdtedqtysum));


                    if ((Float.parseFloat(qtytobeincreaseddescedt.getText().toString()) < qtytobeincreasedval)) {

               //      Toast.makeText(getActivity(),"Quantity to be increased should not be less than the difference of Buyer Requested Quantity and Currently Remaining Quantity",Toast.LENGTH_SHORT).show();

                        Toast.makeText(getActivity(),"Finally Updated Quantity should be greater than or equal to Buyer Requested Quantity.",Toast.LENGTH_SHORT).show();

                      //  savetxtvw.setTextColor(Color.parseColor("#696969"));
                     //   savetxtvw.setEnabled(false);

                        sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                        sbmttext.setTextColor(Color.parseColor("#808080"));
                        sbmtrelative.setEnabled(false);

                    } else {

                       // Toast.makeText(getActivity(),"QUANTITY -",Toast.LENGTH_SHORT).show();
                     //   savetxtvw.setTextColor(Color.parseColor("#FFF15922"));
                      //  savetxtvw.setEnabled(true);

                        sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                        sbmttext.setTextColor(Color.WHITE);
                        sbmtrelative.setEnabled(true);

                    }

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        rem_qty_desc_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {



            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                  //  if (Integer.parseInt(rem_qty_desc_edt.getText().toString()) > 0) {
                        float remaining_qty_int_val = Float.parseFloat(rem_qty_desc_edt.getText().toString());
                        float qtyremainingintval = Float.parseFloat(qtyremainingstr);


                        if (remaining_qty_int_val < qtyremainingintval) {

                           // savetxtvw.setTextColor(Color.parseColor("#696969"));
                           // savetxtvw.setEnabled(false);

                            sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                            sbmttext.setTextColor(Color.parseColor("#808080"));
                            sbmtrelative.setEnabled(false);

                        } else {

                          /*  savetxtvw.setTextColor(Color.parseColor("#FFF15922"));
                            savetxtvw.setEnabled(true);*/

                            sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                            sbmttext.setTextColor(Color.WHITE);
                            sbmtrelative.setEnabled(true);


                        }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {




            }
        });












        sbmtrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String rem_qty_new_str = rem_qty_desc_edt.getText().toString();

                b1.dismiss();

                if(NetworkUtility.checkConnectivity(getActivity())){

                    sbmtrelative.setEnabled(false);



                    String SellerUpdateOfferQuantityUrl = APIName.URL+"/seller/updateOfferQuantity?sell_id="+sellidstr;
                    System.out.println("SELLER Update Offer Quantity URL IS---"+ SellerUpdateOfferQuantityUrl);
                    SellerUpdateOfferQuantityAPI(SellerUpdateOfferQuantityUrl,posted_qty_desc_edt.getText().toString(),rem_qty_new_str,qtyremainingstr,postedqty,qtytobeincreaseddescedt.getText().toString(),finallyupdatedqtydescedt.getText().toString(),tradeid);


                }
                else{
                    util.dialog(getActivity(), "Please check your internet connection.");
                }


            }
        });



        b1 = dialogBuilder.create();
        b1.show();


        cancelrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b1.dismiss();
            }
        });


    }


    public void EditSellCurrentEnquiriesDialog(final String markdownpriceval,final String postedqty,final String qtyremainingstr,final String id,String rate_type,final String rate,final String tncstr,String isTerm,final String tradeid,final String sellidstr,final String quantitystr){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.sell_current_enquiries_edit, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        youneedtotxtvw =  (TextView)dialogView.findViewById(R.id.youneedtotxtvw);
        final EditText type_desc_edt = (EditText)dialogView.findViewById(R.id.type_desc_edt);
        final EditText tnc_desc_edt = (EditText)dialogView.findViewById(R.id.tnc_desc_edt);
         qty_desc_edt = (EditText)dialogView.findViewById(R.id.qty_desc_edt);
        final TextView typetitletxtvw = (TextView)dialogView.findViewById(R.id.typetitletxtvw);
         exceedtxt = (TextView)dialogView.findViewById(R.id.exceedqtytxtvw);
         exceedtxt1 = (TextView)dialogView.findViewById(R.id.exceedqtytxtvw1);

      //  editbtntxtvw = (TextView)dialogView.findViewById(R.id.editbtntxtvw);
        //   TextView canceltxtvw = (TextView)dialogView.findViewById(R.id.canceltxtvw);

        sbmtrelative = (RelativeLayout)dialogView.findViewById(R.id.sbmtrelative);
        final RelativeLayout cancelrelative = (RelativeLayout)dialogView.findViewById(R.id.cancelrelative);
        final TextView sbmttext = (TextView)dialogView.findViewById(R.id.sbmttext);

        final TextView increasequantitytxtvw = (TextView)dialogView.findViewById(R.id.increasequantitytxtvw);



        qty_desc_edt.setText(quantitystr);

        if(qtyremainingstr.length()>0 && qty_desc_edt.getText().toString().length()>0) {
            float remaining_qty_int_val = Float.parseFloat(qtyremainingstr);
            float qty_entered_val = Float.parseFloat(qty_desc_edt.getText().toString());
            float posted_qty_int_val =  Float.parseFloat(postedqty);


            if (remaining_qty_int_val < qty_entered_val) {
                exceedtxt.setVisibility(View.VISIBLE);
                exceedtxt.setTextColor(Color.parseColor("#000000"));
                exceedtxt1.setTextColor(Color.parseColor("#000000"));
                exceedtxt.setText("Posted Quantity" + postedqty+".");
                exceedtxt1.setText("Remaining quantity " + qtyremainingstr + ".");
                youneedtotxtvw.setText("You need to");
              //  editbtntxtvw.setTextColor(Color.parseColor("#696969"));
              //  editbtntxtvw.setEnabled(false);

                sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                sbmttext.setTextColor(Color.parseColor("#808080"));
                sbmtrelative.setEnabled(false);

                System.out.println("qty_entered_val is==="+ qty_entered_val);
                System.out.println("posted_qty_int_val is==="+ posted_qty_int_val);

                if(posted_qty_int_val >= qty_entered_val)
                {
                   // editbtntxtvw.setEnabled(true);

                    sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                    sbmttext.setTextColor(Color.WHITE);
                    sbmtrelative.setEnabled(true);
                }

                qty_desc_edt.setTextColor(Color.RED);

                increasequantitytxtvw.setVisibility(View.VISIBLE);
                increasequantitytxtvw.setPaintFlags(increasequantitytxtvw.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            } else {
                exceedtxt.setVisibility(View.VISIBLE);
                exceedtxt.setTextColor(Color.parseColor("#000000"));
                exceedtxt1.setTextColor(Color.parseColor("#000000"));
              //  editbtntxtvw.setTextColor(Color.parseColor("#FFF15922"));
              //  editbtntxtvw.setEnabled(true);

                sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                sbmttext.setTextColor(Color.WHITE);
                sbmtrelative.setEnabled(true);

                exceedtxt.setText("Posted Quantity" + postedqty);
                exceedtxt1.setText("Remaining quantity " + qtyremainingstr);
                youneedtotxtvw.setVisibility(View.GONE);
                increasequantitytxtvw.setVisibility(View.GONE);

            }
        }



        qty_desc_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {



            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(qtyremainingstr.length()>0 && qty_desc_edt.getText().toString().length()>0) {
                    float remaining_qty_int_val = Float.parseFloat(qtyremainingstr);
                    float qty_entered_val = Float.parseFloat(qty_desc_edt.getText().toString());
                    float posted_qty_int_val =  Float.parseFloat(postedqty);


                    if (remaining_qty_int_val < qty_entered_val) {
                        exceedtxt.setVisibility(View.VISIBLE);
                        exceedtxt.setTextColor(Color.parseColor("#000000"));
                        exceedtxt1.setTextColor(Color.parseColor("#000000"));
                        exceedtxt.setText("Posted Quantity" + postedqty +".");
                        exceedtxt1.setText("Remaining quantity " + qtyremainingstr + ".");
                        youneedtotxtvw.setText("You need to");
                        //editbtntxtvw.setTextColor(Color.parseColor("#696969"));
                      //  editbtntxtvw.setEnabled(false);

                        sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                        sbmttext.setTextColor(Color.parseColor("#808080"));
                        sbmtrelative.setEnabled(false);

                        qty_desc_edt.setTextColor(Color.RED);
                        increasequantitytxtvw.setVisibility(View.VISIBLE);
                        increasequantitytxtvw.setPaintFlags(increasequantitytxtvw.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

                    } else {
                        exceedtxt.setVisibility(View.VISIBLE);
                        exceedtxt.setTextColor(Color.parseColor("#000000"));
                        exceedtxt1.setTextColor(Color.parseColor("#000000"));
                       // editbtntxtvw.setTextColor(Color.parseColor("#FFF15922"));
                      //  editbtntxtvw.setEnabled(true);

                        sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                        sbmttext.setTextColor(Color.WHITE);
                        sbmtrelative.setEnabled(true);

                        exceedtxt.setText("Posted Quantity" + postedqty+".");
                        exceedtxt1.setText("Remaining quantity " + qtyremainingstr);
                        youneedtotxtvw.setVisibility(View.GONE);
                        increasequantitytxtvw.setVisibility(View.GONE);

                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {




            }
        });







        if(rate_type.equalsIgnoreCase("1"))
        {
            typetitletxtvw.setText("Advance");
        }

        if(rate_type.equalsIgnoreCase("2"))
        {
            typetitletxtvw.setText("Next");
        }

        if(rate_type.equalsIgnoreCase("3"))
        {
            typetitletxtvw.setText("Regular");
        }

        // type_desc_edt.setText(revisedpricestr);

        if (markdownpriceval.equalsIgnoreCase("0.00"))
        {
            type_desc_edt.setText(rate);
        }else{

            double revisedpriceintval = Double.parseDouble(rate) - Double.parseDouble(markdownpriceval);
            DecimalFormat df = new DecimalFormat("#.00");

            type_desc_edt.setText((df.format(revisedpriceintval)));


            type_desc_edt.setTextColor(Color.parseColor("#ff0000"));

        }

        type_desc_edt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable edt) {

                String temp = edt.toString ();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 2) {
                    edt.delete(posDot + 3, posDot + 4);
                }
            }
        });




        if(tncstr.length()!=4) {
            tnc_desc_edt.setText(tncstr);
        }

        tnc_desc_edt.setTextColor(Color.parseColor("#000000"));

        /*if(isTerm.equalsIgnoreCase("1"))
        {
            tnc_desc_edt.setTextColor(Color.parseColor("#FFF15922"));
        }
        else
        {
            tnc_desc_edt.setTextColor(Color.parseColor("#000000"));
        }*/




        increasequantitytxtvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
                EditRemainingQuantityDialog(qtyremainingstr,sellidstr,postedqty,quantitystr,tradeid);
            }
        });

        sbmtrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                b.dismiss();


              String type_desc_edt_str = type_desc_edt.getText().toString();
               String tnc_desc_edt_str = tnc_desc_edt.getText().toString();
                String qty_desc_edt_str = qty_desc_edt.getText().toString();


                if(NetworkUtility.checkConnectivity(getActivity())){

                    sbmtrelative.setEnabled(false);


                    String SellerupdateTradeUrl = APIName.URL+"/seller/updateTrade?trade_id="+tradeid;
                    System.out.println("SELLER update Trade URL IS---"+ SellerupdateTradeUrl);
                    EditSellerEnquiriesAPI(SellerupdateTradeUrl,tradeid,id,rate,tncstr,type_desc_edt_str,tnc_desc_edt_str,sellidstr,quantitystr,qty_desc_edt_str);

                    String acceptstatus = "2";
                    System.out.println("STATUS PASSED ON CLICK OF ACCEPT==="+ acceptstatus);


                        String SellerAcceptUrl = APIName.URL+"/seller/changeTradeStatus?id="+id+"&status="+acceptstatus;
                        System.out.println("SELLER ACCEPT URL IS---"+ SellerAcceptUrl);
                        SellerActionAPI(SellerAcceptUrl);




                }
                else{
                    util.dialog(getActivity(), "Please check your internet connection.");
                }


            }
        });



        b = dialogBuilder.create();
        b.show();


        cancelrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }



    public class Holder {
        TextView cmpnyname,proddesc,qtydesctxtvw,statustxt,tradeiddesc,gradetxtvw;
        ImageView viewofferiv,acceptiv,rejectiv,editiv;
        RatingBar ratingBar;



    }

    private void SellerUpdateOfferQuantityAPI(String url,final String posted_qty,final String rem_qty_new_str,final String qtyremainingstr,final String postedqty,final String qtytobeincreasedstr,final String finallyupdatedqtystr,final String tradeid) {

        pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();



                        System.out.println("RESPONSE OF SELLER SellerUpdateOfferQuantityAPI IS---" + response);
                       // Toast.makeText(getActivity(),response,Toast.LENGTH_SHORT).show();



                        JSONObject SellerUpdateOfferQuantityAPIJson = null;
                        try {
                            SellerUpdateOfferQuantityAPIJson = new JSONObject(response);


                            String statusstr = SellerUpdateOfferQuantityAPIJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                              /*  if(NetworkUtility.checkConnectivity(getActivity())){


                                    String SellerGetCurrentEnquiryUrl = APIName.URL+"/seller/getCurrentEnq?user_code="+userCodestr;
                                    System.out.println("SELLER GET CURRENT ENQUIRY URL IS---"+ SellerGetCurrentEnquiryUrl);
                                    SellerGetCurrentEnquiryAPI(SellerGetCurrentEnquiryUrl);


                                }
                                else{
                                    util.dialog(getActivity(), "Please check your internet connection.");
                                }*/

                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.detach(SellCurrentEnquiriesFragment.this).attach(SellCurrentEnquiriesFragment.this).commit();




                              //  exceedtxt.setText("is" + rem_qty_new_str +".");

                              //  float remaining_qty_new_val = Float.parseFloat(rem_qty_new_str);
                             //   float remaining_qty_prev_val = Float.parseFloat(qtyremainingstr);
                                float posted_qty = Float.parseFloat(postedqty);
                                float finallyupdatedqty = Float.parseFloat(finallyupdatedqtystr);

                                float sum = posted_qty + finallyupdatedqty;


                              //  float final_rem_qty= remaining_qty_new_val - remaining_qty_prev_val + posted_qty;

                                exceedtxt.setText("Posted Quantity" + Float.toString(sum) +".");
                                exceedtxt1.setText("Remaining quantity " + finallyupdatedqtystr + ".");
                                youneedtotxtvw.setText("You need to");

                                qty_desc_edt.setTextColor(Color.parseColor("#000000"));


                                //   exceedtxt1.setText("Remaining quantity" + Float.toString(final_rem_qty) + ".You need to .");



                          //      float qty_entered_val = Float.parseFloat(qty_desc_edt.getText().toString());

                            /*    if(remaining_qty_new_val >= qty_entered_val)
                                {
                                    editbtntxtvw.setTextColor(Color.parseColor("#FFF15922"));
                                    editbtntxtvw.setEnabled(true);
                                }
*/
                                b.show();

                            }
                            else
                            {
                                Toast.makeText(getActivity(),SellerUpdateOfferQuantityAPIJson.getString("message"),Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia.dismiss();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("posted_quantity",posted_qty);
                params.put("updated_quantity",finallyupdatedqtystr);
                params.put("increased_quantity",qtytobeincreasedstr);
                params.put("trade_id",tradeid);

                System.out.println("SellerUpdateOfferQuantityAPI params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }




    public void ViewOfferDialog(String revised_price_str,String quoted_rate_colour,String revised_rate_color,String modified_field_str,String delivery_type_str,String delivery_in_days_str,String material_inspection_str,String weighment_type_str,String test_certificate_type_str,String promo_str,String length_str,String categoryid,String transport_str,String loading_str,String gst_str,String insurance_select_str,String insurance_str,String category_grade_str,String size_range_start_str,String size_range_end_str,String q1_title_str,String q1_value_str,String q2_title_str,String q2_value_str,String q3_title_str,String q3_value_str,String q4_title_str,String q4_value_str,String q5_title_str,String q5_value_str,String q6_title_str,String q6_value_str,String q7_title_str,String q7_value_str,String q8_title_str,String q8_value_str,String category_grade_name_str,String rate_type_str,String originalratestr,String israteval,String quantity_modified_val,String isquantityval,String isterm,String tradeidstr,String companyname,String categorydescstr,String subcategorydescstr,String locationdesc,String qtyofferedstr,String ratestr,String terms,String markdownpriceval,String tradestatusval) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.view_seller_current_enquiries1, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView tradeidtitletxtvw = (TextView)dialogView.findViewById(R.id.tradeidtitletxtvw);
        final TextView cmpnynamedesctxt = (TextView) dialogView.findViewById(R.id.cmpnynamedesctxt);
        final TextView categorydesctxt = (TextView) dialogView.findViewById(R.id.categorydesctxt);
        final TextView subcategorydesctxt = (TextView) dialogView.findViewById(R.id.subcategorydesctxt);
        final TextView locationdesctxt = (TextView) dialogView.findViewById(R.id.locationdesctxt);
        final TextView qtyoffereddesctxtvw = (TextView)dialogView.findViewById(R.id.qtyoffereddesctxtvw);
        final TextView ratedesctxtvw = (TextView)dialogView.findViewById(R.id.ratedesctxtvw);
        final TextView pricetxt1 = (TextView) dialogView.findViewById(R.id.pricetxt1);
        final TextView pricetxt2 = (TextView) dialogView.findViewById(R.id.pricetxt2);
        final TextView pricetxt3 = (TextView) dialogView.findViewById(R.id.pricetxt3);
        final TextView pricetxt4 = (TextView) dialogView.findViewById(R.id.pricetxt4);
        final TextView pricetxt5 = (TextView) dialogView.findViewById(R.id.pricetxt5);
        final TextView pricetxt6 = (TextView) dialogView.findViewById(R.id.pricetxt6);
        final TextView pricetxt7 = (TextView) dialogView.findViewById(R.id.pricetxt7);
        final TextView pricetxt8 = (TextView) dialogView.findViewById(R.id.pricetxt8);
        final TextView termsdesctxt = (TextView) dialogView.findViewById(R.id.termsdesctxt);
        final TextView markdownpricedesctxt = (TextView) dialogView.findViewById(R.id.markdownpricedesctxt);
        final TextView statusdesctxt = (TextView) dialogView.findViewById(R.id.statusdesctxt);
        final TextView revisedpricedesctxt = (TextView) dialogView.findViewById(R.id.revisedpricedesctxt);

        final TextView transportationdesctxt = (TextView) dialogView.findViewById(R.id.transportationdesctxt);
        final TextView loadingdesctxt = (TextView) dialogView.findViewById(R.id.loadingdesctxt);
        final TextView gstdesctxt = (TextView) dialogView.findViewById(R.id.gstdesctxt);
        final TextView insurancedesctxt = (TextView) dialogView.findViewById(R.id.insurancedesctxt);


        final TextView q1title = (TextView) dialogView.findViewById(R.id.q1title);
        final TextView q2title = (TextView) dialogView.findViewById(R.id.q2title);
        final TextView q3title = (TextView) dialogView.findViewById(R.id.q3title);
        final TextView q4title = (TextView) dialogView.findViewById(R.id.q4title);
        final TextView q5title = (TextView) dialogView.findViewById(R.id.q5title);
        final TextView q6title = (TextView) dialogView.findViewById(R.id.q6title);
        final TextView q7title = (TextView) dialogView.findViewById(R.id.q7title);
        final TextView q8title = (TextView) dialogView.findViewById(R.id.q8title);
        final TextView gradedesctxt = (TextView) dialogView.findViewById(R.id.gradedesctxt);

        final TextView lengthtitletxtvw =  (TextView) dialogView.findViewById(R.id.lengthtitletxtvw);
        final TextView lengthdesctxt = (TextView) dialogView.findViewById(R.id.lengthdesctxt);
        final View line16 = (View)dialogView.findViewById(R.id.line16);
        final TextView promodesctxt =  (TextView) dialogView.findViewById(R.id.promodesctxt);

        final TextView deliveryindaysdesctxt = (TextView)dialogView.findViewById(R.id.deliveryindaysdesctxt);
        final TextView materialinspectiondesctxt = (TextView)dialogView.findViewById(R.id.materialinspectiondesctxt);
        final TextView weighmentdesctxt = (TextView)dialogView.findViewById(R.id.weighmentdesctxt);
        final TextView testcertificatedesctxt = (TextView)dialogView.findViewById(R.id.testcertificatedesctxt);

        if(delivery_type_str.equalsIgnoreCase("1"))
        {
            deliveryindaysdesctxt.setText("As per Aachar Sanhita");
        }
        if(delivery_type_str.equalsIgnoreCase("2"))
        {
            deliveryindaysdesctxt.setText(delivery_in_days_str+ " days");
        }

        if(material_inspection_str.equalsIgnoreCase("1"))
        {
            materialinspectiondesctxt.setText("Buyer's Place");
        }
        if(material_inspection_str.equalsIgnoreCase("2"))
        {
            materialinspectiondesctxt.setText("Seller's Place");
        }

        if(weighment_type_str.equalsIgnoreCase("1"))
        {
            weighmentdesctxt.setText("Buyer's Place");
        }
        if(weighment_type_str.equalsIgnoreCase("2"))
        {
            weighmentdesctxt.setText("Seller's Place");
        }
        if(weighment_type_str.equalsIgnoreCase("3"))
        {
            weighmentdesctxt.setText("Buyer & Seller's Place");
        }


        if(test_certificate_type_str.equalsIgnoreCase("1"))
        {
            testcertificatedesctxt.setText("Yes");
           // testcertificatedesctxt.setText("Required");
        }
        if(test_certificate_type_str.equalsIgnoreCase("2"))
        {
            testcertificatedesctxt.setText("No");
            //testcertificatedesctxt.setText("Not Required");
        }



        if(!(promo_str.equalsIgnoreCase("0"))){
            promodesctxt.setText(promo_str);

            if((promo_str.equalsIgnoreCase("(null)")))
            {
                promodesctxt.setText("--");
            }
        }
        else
        {
            promodesctxt.setText("--");
        }


        if(length_str.length()!=4) {
            //lengthdesctxt.setText(length_str+" m");

            line16.setVisibility(View.VISIBLE);
            lengthtitletxtvw.setVisibility(View.VISIBLE);
            lengthdesctxt.setVisibility(View.VISIBLE);

            if(categoryid.equalsIgnoreCase("1")) {

                lengthtitletxtvw.setText("Length");
                lengthdesctxt.setText(length_str+" m");
            }

            if(categoryid.equalsIgnoreCase("2"))
            {
                lengthtitletxtvw.setText("Weight");
                lengthdesctxt.setText(length_str+" kg");
            }
        }
        else
        {
            line16.setVisibility(View.GONE);
            lengthtitletxtvw.setVisibility(View.GONE);
            lengthdesctxt.setVisibility(View.GONE);
        }


        gradedesctxt.setText(category_grade_name_str);



        tradeidtitletxtvw.setText("Trade ID :"+tradeidstr);
        cmpnynamedesctxt.setText(companyname);
        categorydesctxt.setText(categorydescstr);
      //  subcategorydesctxt.setText(subcategorydescstr);
        locationdesctxt.setText(locationdesc);
       // qtyoffereddesctxtvw.setText(qtyofferedstr);

        if(rate_type_str.equalsIgnoreCase("1"))
        {
            ratedesctxtvw.setText(ratestr+"(A)");
        }

        if(rate_type_str.equalsIgnoreCase("2"))
        {
            ratedesctxtvw.setText(ratestr+"(N)");
        }

        if(rate_type_str.equalsIgnoreCase("3"))
        {
            ratedesctxtvw.setText(ratestr+"(R)");
        }



        pricetxt1.setText(q1_value_str);
        pricetxt2.setText(q2_value_str);
        pricetxt3.setText(q3_value_str);
        pricetxt4.setText(q4_value_str);
        pricetxt5.setText(q5_value_str);
        pricetxt6.setText(q6_value_str);
    //    pricetxt7.setText(q7_value_str);
     //   pricetxt8.setText(q8_value_str);

        if(!q7_value_str.equalsIgnoreCase(""))
        {
            pricetxt7.setText(q7_value_str);
        }
        else {
            pricetxt7.setText("NA");
        }

        if(!q8_value_str.equalsIgnoreCase(""))
        {
            pricetxt8.setText(q8_value_str);
        }
        else
        {
            pricetxt8.setText("NA");
        }

        List<String> modified_field_List = Arrays.asList(modified_field_str.split(","));


        if (modified_field_List.contains("-3-"))
        {
            pricetxt1.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-4-"))
        {
            pricetxt2.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-5-"))
        {
            pricetxt3.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-6-"))
        {
            pricetxt4.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-7-"))
        {
            pricetxt5.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-8-"))
        {
            pricetxt6.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-9-"))
        {
            pricetxt7.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-10-"))
        {
            pricetxt8.setTextColor(Color.RED);
        }


        q1title.setText(q1_title_str);
        q2title.setText(q2_title_str);
        q3title.setText(q3_title_str);
        q4title.setText(q4_title_str);
        q5title.setText(q5_title_str);
        q6title.setText(q6_title_str);
        q7title.setText(q7_title_str);
        q8title.setText(q8_title_str);


        if(transport_str.equalsIgnoreCase("1"))
        {
            transportationdesctxt.setText("Seller's End");
        }
        if(transport_str.equalsIgnoreCase("2"))
        {
            transportationdesctxt.setText("Buyer's End");
        }
        if(transport_str.equalsIgnoreCase("3"))
        {
            transportationdesctxt.setText("Mxmart's End");
        }

        if(loading_str.equalsIgnoreCase("NA"))
        {
            loadingdesctxt.setText(loading_str);
        }
        else {
            loadingdesctxt.setText(loading_str + " \u20B9"+"/MT");
        }
        gstdesctxt.setText(gst_str);

        if(insurance_select_str.equalsIgnoreCase("NA"))
        {
            insurancedesctxt.setText("NA");
        }
        else {

            if (insurance_str.equalsIgnoreCase("1")) {
                insurancedesctxt.setText("Seller");
            } else {
                insurancedesctxt.setText("Buyer");
            }
        }


        if(categoryid.equalsIgnoreCase("1")||categoryid.equalsIgnoreCase("2")) {
            subcategorydesctxt.setText(subcategorydescstr);
        }
        else {
            subcategorydesctxt.setText(size_range_start_str + "-" + size_range_end_str);
        }


        System.out.println("TERMS ARE==="+ terms);
        if(!(terms.length()==4)) {
            termsdesctxt.setText(terms);
        }
        else
        {
            termsdesctxt.setText("--");
        }
      /*  if (markdownpriceval.equalsIgnoreCase("0.00"))
        {
            markdownpricedesctxt.setText(markdownpriceval);
        }
        else
        {
            markdownpricedesctxt.setText(markdownpriceval);
        }*/

        markdownpricedesctxt.setText(markdownpriceval);

        statusdesctxt.setText(tradestatusval);


        if (markdownpriceval.equalsIgnoreCase("0.00"))
        {
            revisedpricedesctxt.setText("--");


        }else{

            double revisedprice_dval = Double.parseDouble(revised_price_str);
            String revisedprice_str = String.format("%.2f", revisedprice_dval);
            revisedpricedesctxt.setText(revisedprice_str);



        }

        if(!markdownpricedesctxt.getText().toString().equalsIgnoreCase("--")) {
            String markdownpricestr = markdownpricedesctxt.getText().toString();
            double markdownprice = Double.parseDouble(markdownpricestr);
            Double d = new Double(markdownprice);
            int i = d.intValue();
            System.out.println("MARK DOWN PRICE IS========" + i);

           // markdownpricedesctxt.setText(Integer.toString(i) + ".00");
        }

       /*

        if(!revisedpricedesctxt.getText().toString().equalsIgnoreCase("--")) {
            String revisedpricedescstr = revisedpricedesctxt.getText().toString();
            double revisedprice = Double.parseDouble(revisedpricedescstr);
            Double dd = new Double(revisedprice);
            int ii = dd.intValue();
            System.out.println("REVISED PRICE IS========" + ii);

           // Toast.makeText(getActivity(),"T 3",Toast.LENGTH_SHORT).show();

            revisedpricedesctxt.setText(Integer.toString(ii) + ".00");
        }

*/
        //New Enhancement color conditions ----------

        //SellCurrentEnquiryJSONArray.getJSONObject(position).getString("terms");

        if (isterm.equalsIgnoreCase("1")) {

            //Orange Color Code----------
            termsdesctxt.setTextColor(Color.RED);
            //termsDtlLbl.textColor = [UIColor colorWithRed:234/255.0f green:112/255.0f blue:45/255.0f alpha:1.0f];
        }
        else if(isterm.equalsIgnoreCase("2"))
        {
            termsdesctxt.setTextColor(Color.parseColor("#FFF15922"));
        }
        else
        {
            termsdesctxt.setTextColor(Color.parseColor("#000000"));

        }

        if (isquantityval.equalsIgnoreCase("1")) {

            qtyoffereddesctxtvw.setTextColor(Color.parseColor("#FFF15922"));
            qtyoffereddesctxtvw.setText(quantity_modified_val);


            //qtyDetailLbl.textColor = [UIColor colorWithRed:234/255.0f green:112/255.0f blue:45/255.0f alpha:1.0f];
           // qtyDetailLbl.text = [NSString stringWithFormat:@"%@",[[self.sellEnquiryArray objectAtIndex:indexPath.row]objectForKey:@"quantity_modified"]];
        }
        else
        {
            qtyoffereddesctxtvw.setTextColor(Color.parseColor("#000000"));
            qtyoffereddesctxtvw.setText(qtyofferedstr);
        }

        if(quoted_rate_colour.equalsIgnoreCase("green"))
        {
            markdownpricedesctxt.setTextColor(Color.parseColor("#3EBA3E"));
            double markdownprice_dval = Double.parseDouble(markdownpriceval);
            String markdownprice_str = String.format("%.2f", markdownprice_dval);
            markdownpricedesctxt.setText("(+)" + markdownprice_str);
        }
        if(quoted_rate_colour.equalsIgnoreCase("red"))
        {
            markdownpricedesctxt.setTextColor(Color.parseColor("#FF0000"));
            double markdownprice_dval = Double.parseDouble(markdownpriceval);
            String markdownprice_str = String.format("%.2f", markdownprice_dval);
            markdownpricedesctxt.setText(markdownprice_str);
        }
        if(quoted_rate_colour.equalsIgnoreCase("orange"))
        {
            markdownpricedesctxt.setTextColor(Color.parseColor("#FFF15922"));
            double markdownprice_dval = Double.parseDouble(markdownpriceval);
            String markdownprice_str = String.format("%.2f", markdownprice_dval);
            markdownpricedesctxt.setText(markdownprice_str);
        }

        if(revised_rate_color.equalsIgnoreCase("green"))
        {
            revisedpricedesctxt.setTextColor(Color.parseColor("#3EBA3E"));

        }
        if(revised_rate_color.equalsIgnoreCase("red"))
        {
            revisedpricedesctxt.setTextColor(Color.parseColor("#FF0000"));

        }
        if(revised_rate_color.equalsIgnoreCase("orange"))
        {
            revisedpricedesctxt.setTextColor(Color.parseColor("#FFF15922"));

        }




        revisedpricestr = revisedpricedesctxt.getText().toString();


        final  AlertDialog b = dialogBuilder.create();
        b.show();


        ImageView ic_close = (ImageView)dialogView.findViewById(R.id.ic_close);
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }



}
