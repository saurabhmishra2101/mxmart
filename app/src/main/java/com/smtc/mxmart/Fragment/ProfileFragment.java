package com.smtc.mxmart.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Handler;


import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;

import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smtc.mxmart.APIName;
import com.smtc.mxmart.CircleTransform;
import com.smtc.mxmart.DocWebViewActivity;
import com.smtc.mxmart.EditBusinessBankDetailsActivity;
import com.smtc.mxmart.EditContactDetailsActivity;
import com.smtc.mxmart.EditInstalledCapacityActivity;
import com.smtc.mxmart.EditPersonalDetailsActivity;
import com.smtc.mxmart.EditReferenceActivity;
import com.smtc.mxmart.HomeActivity;
import com.smtc.mxmart.NetworkUtility;
import com.smtc.mxmart.PicassoTrustAll;
import com.smtc.mxmart.R;
import com.smtc.mxmart.SellerAddDispatchActivity;
import com.smtc.mxmart.SellerDispatchActivity;
import com.smtc.mxmart.SingletonActivity;
import com.smtc.mxmart.UtilsDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.IllegalFormatCodePointException;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;


public class ProfileFragment extends Fragment {
    TextView usercodetxt,companynametxt,citytxt,profilecompletenesstxt,percenttxt,lastupdatedtxt,upload_from_memory,click_image_from_camera;
    Typeface  source_sans_pro_normal;
    ProgressBar progress;
    public ImageView profileiv;
    ImageView cancel_dialog;
    private static final int CAPTURE_IMAGE_CAPTURE_CODE = 1888;
    private static final int SELECT_PICTURE = 1;
    String attachmentstr,userCodestr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    UtilsDialog util = new UtilsDialog();
    boolean isMandatoryFilled = true;
    ProgressDialog pdia;
    JSONObject stuserprofjson;
    JSONObject userdetailsjson;
    RelativeLayout profilerelative;
    int clickedcount = 0;
    TextView forofficeuseonlytxt,groupnametitletxt,groupnamedettxt,groupcodetitletxt,groupcodedettxt,locationtitletxt,locationdettxt;
    JSONArray contactjsonArray, referencejsonArray,installedCapacityjsonArray;
    TextView personaldetailstxt,editpersonaldetailstxt,nametitletxt,namedettxt,dobtitletxt,dobdetailtxt,companytypetitletxt,companytypedetailtxt,companynametitletxt,companynamedetailtxt,designationtitletxt,designationdetailtxt,emailtitletxt,emaildetailtxt,landlinetitletxt,landlinedetailtxt,mobiletitletxt,mobiledetailtxt,addresstitletxt,addressdetailtxt;
    TextView nullbusinessbanktxt,businessdetailstxt,editbusinessdetailstxt,pannotitletxt,pannodettxt,cintitletxt,cindetailtxt,gstintitletxt,gstindetailtxt,udyogaadharnumbertitletxt,udyogaadhaarnumberdetailtxt,billingaddresstitletxt,billingaddressdetailtxt,citytitletxt,citydetailtxt,statetitletxt,statedetailtxt,pincodetitletxt,pincodedetailtxt,establishmentdatetitletxt,establishmentdatedetailtxt;
    RelativeLayout personaldetailsrel,businessdetailsrel,bankdetailsrel,maincontactrel,installedcapacitydetailsrel,mainreferencerel;
    TextView bankdetailstxt,editbankdetailstxt,banknametitletxt,banknamedettxt,branchtitletxt,branchdetailtxt,accountnotitletxt,accountnodetailtxt,ifsccodetitletxt,ifsccodedetailtxt;
    TextView nullcontacttxt,accountnotitletxtvw,contactdetailstxt,editcontactdetailstxt,nameacctitletxtvw,nameaccdesctxtvw,phonenoacctitletxtvw,phonenoaccdesctxtvw,mobilenoacctitletxtvw,mobilenoaccdesctxtvw,emailidacctitletxtvw,emailidaccdesctxtvw;
    TextView salesnmarkettitletxtvw,namesalesmarkettitletxtvw,namesalesmarketdesctxtvw,phonenosalesmarkettitletxtvw,phonenosalesmarketdesctxtvw,mobilenosalesmarkettitletxtvw,mobilenosalesmarketdesctxtvw,emailidsalesmarkettitletxtvw,emailidsalesmarketdesctxtvw;
    TextView dispatchtitletxtvw,namedispatchdesctxtvw,phonenodispatchtitletxtvw,phonenodispatchdesctxtvw,mobilenodispatchtitletxtvw,mobilenodispatchdesctxtvw,emailiddispatchtitletxtvw,emailiddispatchdesctxtvw,namedispatchtitletxtvw;
    TextView nullinstalltxt,installedcapacitydetailstxt,editinstalledcapacitydetailstxt,typeofmaterialstitletxt,capacitytxt,pellettitletxt,pelletcapacitytxt,spongeirontitletxt,spongeironcapacitytxt,ingotstitletxt,ingotsdetailtxt,billetstitletxt,billetsdetailtxt;
    TextView referencedetailstxt,editreferencedetailstxt,refonetitletxtvw,cmpnynamereftitletxtvw,cmpnynamerefdesctxtvw,namereftitletxtvw,namerefdesctxtvw,mobilenoreftitletxtvw,mobilenorefdesctxtvw,emailidreftitletxtvw,emailidrefdesctxtvw;
    TextView reftwotitletxtvw,cmpnynamereftwotitletxtvw,cmpnynamereftwodesctxtvw,namereftwotitletxtvw,namereftwodesctxtvw,mobilenoreftwotitletxtvw,mobilenoreftwodesctxtvw,emailidreftwotitletxtvw,emailidreftwodesctxtvw;
    TextView cmplteproftxt,nullreftxt,refthreetitletxtvw,cmpnynamerefthreetitletxtvw,cmpnynamerefthreedesctxtvw,namerefthreetitletxtvw,namerefthreedesctxtvw,mobilenorefthreetitletxtvw,mobilenorefthreedesctxtvw,emailidrefthreetitletxtvw,emailidrefthreedesctxtvw;
    TextView nulldoctxt,documentchecklisttxt,compregcerttxt,dicregtxt,pancardtxt,gstinregcerttxt,udyogaadharmemotxt,cancelledchqtxt;
    ImageView active_iv1,active_iv2,active_iv3,active_iv4,active_iv5,active_iv6,viewdociv1,viewdociv2,viewdociv3,viewdociv4,viewdociv5,viewdociv6;
    private ProgressBar bar;
    Handler handler = new Handler();
    private static final int PERMISSION_REQUEST_CODE = 200;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.fragment_profile, container, false);



        bar = (ProgressBar)view.findViewById(R.id.progressBar);
        nulldoctxt = (TextView)view.findViewById(R.id.nulldoctxt);
        documentchecklisttxt = (TextView)view.findViewById(R.id.documentchecklisttxt);
        compregcerttxt = (TextView)view.findViewById(R.id.compregcerttxt);
        dicregtxt =  (TextView)view.findViewById(R.id.dicregtxt);
        pancardtxt =  (TextView)view.findViewById(R.id.pancardtxt);
        gstinregcerttxt = (TextView)view.findViewById(R.id.gstinregcerttxt);
        udyogaadharmemotxt = (TextView)view.findViewById(R.id.udyogaadharmemotxt);
        cancelledchqtxt = (TextView)view.findViewById(R.id.cancelledchqtxt);

        active_iv1 = (ImageView)view.findViewById(R.id.active_iv1);
        active_iv2 = (ImageView)view.findViewById(R.id.active_iv2);
        active_iv3 = (ImageView)view.findViewById(R.id.active_iv3);
        active_iv4 = (ImageView)view.findViewById(R.id.active_iv4);
        active_iv5 = (ImageView)view.findViewById(R.id.active_iv5);
        active_iv6 = (ImageView)view.findViewById(R.id.active_iv6);

        viewdociv1 = (ImageView)view.findViewById(R.id.viewdociv1);
        viewdociv2 = (ImageView)view.findViewById(R.id.viewdociv2);
        viewdociv3 = (ImageView)view.findViewById(R.id.viewdociv3);
        viewdociv4 = (ImageView)view.findViewById(R.id.viewdociv4);
        viewdociv5 = (ImageView)view.findViewById(R.id.viewdociv5);
        viewdociv6 = (ImageView)view.findViewById(R.id.viewdociv6);

        forofficeuseonlytxt = (TextView)view.findViewById(R.id.forofficeuseonlytxt);
        groupnametitletxt = (TextView)view.findViewById(R.id.groupnametitletxt);
        groupnamedettxt = (TextView)view.findViewById(R.id.groupnamedettxt);
        groupcodetitletxt = (TextView)view.findViewById(R.id.groupcodetitletxt);
        groupcodedettxt = (TextView)view.findViewById(R.id.groupcodedettxt);
        locationtitletxt = (TextView)view.findViewById(R.id.locationtitletxt);
        locationdettxt = (TextView)view.findViewById(R.id.locationdettxt);



        profilerelative = (RelativeLayout) view.findViewById(R.id.profilerelative);
        cmplteproftxt = (TextView)view.findViewById(R.id.cmplteproftxt);
        usercodetxt = (TextView)view.findViewById(R.id.usercodetxt);
        companynametxt = (TextView)view.findViewById(R.id.companynametxt);
        citytxt = (TextView)view.findViewById(R.id.citytxt);
        profilecompletenesstxt = (TextView)view.findViewById(R.id.profilecompletenesstxt);
        percenttxt = (TextView)view.findViewById(R.id.percenttxt);
        progress = (ProgressBar)view.findViewById(R.id.progress);
        lastupdatedtxt = (TextView)view.findViewById(R.id.lastupdatedtxt);
        profileiv = (ImageView)view.findViewById(R.id.profileiv);

        System.out.println("SELECTED FILE URI IS==="+ SingletonActivity.selectedFileURI);

        if(SingletonActivity.selectedFileURI != null) {
            Picasso.with(getActivity()).load(SingletonActivity.selectedFileURI).noPlaceholder().centerCrop().fit()
                    .into(profileiv);
        }

        personaldetailstxt = (TextView)view.findViewById(R.id.personaldetailstxt);
        editpersonaldetailstxt = (TextView)view.findViewById(R.id.editpersonaldetailstxt);



        nullreftxt = (TextView)view.findViewById(R.id.nullreftxt);
        nullbusinessbanktxt = (TextView)view.findViewById(R.id.nullbusinessbanktxt);

        referencedetailstxt= (TextView)view.findViewById(R.id.referencedetailstxt);
        editreferencedetailstxt= (TextView)view.findViewById(R.id.editreferencedetailstxt);

        nametitletxt = (TextView)view.findViewById(R.id.nametitletxt);
        namedettxt = (TextView)view.findViewById(R.id.namedettxt);
        dobtitletxt = (TextView)view.findViewById(R.id.dobtitletxt);
        dobdetailtxt = (TextView)view.findViewById(R.id.dobdetailtxt);
        companytypetitletxt = (TextView)view.findViewById(R.id.companytypetitletxt);
        companytypedetailtxt = (TextView)view.findViewById(R.id.companytypedetailtxt);
        companynametitletxt = (TextView)view.findViewById(R.id.companynametitletxt);
        companynamedetailtxt = (TextView)view.findViewById(R.id.companynamedetailtxt);
        designationtitletxt = (TextView)view.findViewById(R.id.designationtitletxt);
        designationdetailtxt = (TextView)view.findViewById(R.id.designationdetailtxt);
        emailtitletxt =  (TextView)view.findViewById(R.id.emailtitletxt);
        emaildetailtxt =  (TextView)view.findViewById(R.id.emaildetailtxt);
        landlinetitletxt = (TextView)view.findViewById(R.id.landlinetitletxt);
        landlinedetailtxt = (TextView)view.findViewById(R.id.landlinedetailtxt);
        mobiletitletxt = (TextView)view.findViewById(R.id.mobiletitletxt);
        mobiledetailtxt = (TextView)view.findViewById(R.id.mobiledetailtxt);
        addresstitletxt = (TextView)view.findViewById(R.id.addresstitletxt);
        addressdetailtxt = (TextView)view.findViewById(R.id.addressdetailtxt);

        personaldetailsrel = (RelativeLayout)view.findViewById(R.id.personaldetailsrel);

      /* personaldetailsrel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               Intent i = new Intent(getActivity(), EditPersonalDetailsActivity.class);
                startActivity(i);
            }
        });*/
        businessdetailsrel = (RelativeLayout)view.findViewById(R.id.businessdetailsrel);
        bankdetailsrel = (RelativeLayout)view.findViewById(R.id.bankdetailsrel);
        installedcapacitydetailsrel= (RelativeLayout)view.findViewById(R.id.installedcapacitydetailsrel);
        maincontactrel = (RelativeLayout)view.findViewById(R.id.maincontactrel);
        mainreferencerel = (RelativeLayout)view.findViewById(R.id.mainreferencerel);

        businessdetailstxt = (TextView)view.findViewById(R.id.businessdetailstxt);
        editbusinessdetailstxt = (TextView)view.findViewById(R.id.editbusinessdetailstxt);




        pannotitletxt = (TextView)view.findViewById(R.id.pannotitletxt);
        pannodettxt = (TextView)view.findViewById(R.id.pannodettxt);
        cintitletxt = (TextView)view.findViewById(R.id.cintitletxt);
        cindetailtxt = (TextView)view.findViewById(R.id.cindetailtxt);
        gstintitletxt = (TextView)view.findViewById(R.id.gstintitletxt);
        gstindetailtxt = (TextView)view.findViewById(R.id.gstindetailtxt);
        udyogaadharnumbertitletxt = (TextView)view.findViewById(R.id.udyogaadharnumbertitletxt);
        udyogaadhaarnumberdetailtxt = (TextView)view.findViewById(R.id.udyogaadhaarnumberdetailtxt);
        billingaddresstitletxt = (TextView)view.findViewById(R.id.billingaddresstitletxt);
        billingaddressdetailtxt = (TextView)view.findViewById(R.id.billingaddressdetailtxt);
        citytitletxt = (TextView)view.findViewById(R.id.citytitletxt);
        citydetailtxt = (TextView)view.findViewById(R.id.citydetailtxt);
        statetitletxt = (TextView)view.findViewById(R.id.statetitletxt);
        statedetailtxt = (TextView)view.findViewById(R.id.statedetailtxt);
        pincodetitletxt = (TextView)view.findViewById(R.id.pincodetitletxt);
        pincodedetailtxt = (TextView)view.findViewById(R.id.pincodedetailtxt);
        establishmentdatetitletxt = (TextView)view.findViewById(R.id.establishmentdatetitletxt);
        establishmentdatedetailtxt = (TextView)view.findViewById(R.id.establishmentdatedetailtxt);

        bankdetailstxt = (TextView)view.findViewById(R.id.bankdetailstxt);
        editbankdetailstxt = (TextView)view.findViewById(R.id.editbankdetailstxt);

        banknametitletxt = (TextView)view.findViewById(R.id.banknametitletxt);
        banknamedettxt = (TextView)view.findViewById(R.id.banknamedettxt);
        branchtitletxt = (TextView)view.findViewById(R.id.branchtitletxt);
        branchdetailtxt = (TextView)view.findViewById(R.id.branchdetailtxt);
        accountnotitletxt = (TextView)view.findViewById(R.id.accountnotitletxt);
        accountnodetailtxt = (TextView)view.findViewById(R.id.accountnodetailtxt);
        ifsccodetitletxt = (TextView)view.findViewById(R.id.ifsccodetitletxt);
        ifsccodedetailtxt = (TextView)view.findViewById(R.id.ifsccodedetailtxt);

        installedcapacitydetailstxt =  (TextView)view.findViewById(R.id.installedcapacitydetailstxt);
        editinstalledcapacitydetailstxt =  (TextView)view.findViewById(R.id.editinstalledcapacitydetailstxt);


        typeofmaterialstitletxt =  (TextView)view.findViewById(R.id.typeofmaterialstitletxt);
        capacitytxt =  (TextView)view.findViewById(R.id.capacitytxt);
        pellettitletxt =  (TextView)view.findViewById(R.id.pellettitletxt);
        pelletcapacitytxt =  (TextView)view.findViewById(R.id.pelletcapacitytxt);
        spongeirontitletxt =  (TextView)view.findViewById(R.id.spongeirontitletxt);
        spongeironcapacitytxt =  (TextView)view.findViewById(R.id.spongeironcapacitytxt);
        ingotstitletxt =  (TextView)view.findViewById(R.id.ingotstitletxt);
        ingotsdetailtxt =  (TextView)view.findViewById(R.id.ingotsdetailtxt);
        billetstitletxt =  (TextView)view.findViewById(R.id.billetstitletxt);
        billetsdetailtxt =  (TextView)view.findViewById(R.id.billetsdetailtxt);

        nullinstalltxt =  (TextView)view.findViewById(R.id.nullinstalltxt);
        nullcontacttxt = (TextView)view.findViewById(R.id.nullcontacttxt);


        source_sans_pro_normal = Typeface.createFromAsset(getActivity().getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");


        usercodetxt.setTypeface(source_sans_pro_normal);  accountnotitletxtvw = (TextView)view.findViewById(R.id.accountnotitletxtvw);
        contactdetailstxt = (TextView)view.findViewById(R.id.contactdetailstxt);
        editcontactdetailstxt = (TextView)view.findViewById(R.id.editcontactdetailstxt);
        nameacctitletxtvw = (TextView)view.findViewById(R.id.nameacctitletxtvw);
        nameaccdesctxtvw = (TextView)view.findViewById(R.id.nameaccdesctxtvw);
        phonenoacctitletxtvw = (TextView)view.findViewById(R.id.phonenoacctitletxtvw);
        phonenoaccdesctxtvw = (TextView)view.findViewById(R.id.phonenoaccdesctxtvw);
        mobilenoacctitletxtvw = (TextView)view.findViewById(R.id.mobilenoacctitletxtvw);
        mobilenoaccdesctxtvw = (TextView)view.findViewById(R.id.mobilenoaccdesctxtvw);
        emailidacctitletxtvw = (TextView)view.findViewById(R.id.emailidacctitletxtvw);
        emailidaccdesctxtvw = (TextView)view.findViewById(R.id.emailidaccdesctxtvw);

        salesnmarkettitletxtvw = (TextView)view.findViewById(R.id.salesnmarkettitletxtvw);
        namesalesmarkettitletxtvw = (TextView)view.findViewById(R.id.namesalesmarkettitletxtvw);
        namesalesmarketdesctxtvw = (TextView)view.findViewById(R.id.namesalesmarketdesctxtvw);
        phonenosalesmarkettitletxtvw = (TextView)view.findViewById(R.id.phonenosalesmarkettitletxtvw);
        phonenosalesmarketdesctxtvw = (TextView)view.findViewById(R.id.phonenosalesmarketdesctxtvw);
        mobilenosalesmarkettitletxtvw = (TextView)view.findViewById(R.id.mobilenosalesmarkettitletxtvw);
        mobilenosalesmarketdesctxtvw = (TextView)view.findViewById(R.id.mobilenosalesmarketdesctxtvw);
        emailidsalesmarkettitletxtvw = (TextView)view.findViewById(R.id.emailidsalesmarkettitletxtvw);
        emailidsalesmarketdesctxtvw = (TextView)view.findViewById(R.id.emailidsalesmarketdesctxtvw);

        dispatchtitletxtvw = (TextView)view.findViewById(R.id.dispatchtitletxtvw);
        namedispatchdesctxtvw = (TextView)view.findViewById(R.id.namedispatchdesctxtvw);
        phonenodispatchtitletxtvw = (TextView)view.findViewById(R.id.phonenodispatchtitletxtvw);
        phonenodispatchdesctxtvw = (TextView)view.findViewById(R.id.phonenodispatchdesctxtvw);
        mobilenodispatchtitletxtvw = (TextView)view.findViewById(R.id.mobilenodispatchtitletxtvw);
        mobilenodispatchdesctxtvw = (TextView)view.findViewById(R.id.mobilenodispatchdesctxtvw);
        emailiddispatchtitletxtvw = (TextView)view.findViewById(R.id.emailiddispatchtitletxtvw);
        emailiddispatchdesctxtvw = (TextView)view.findViewById(R.id.emailiddispatchdesctxtvw);
        namedispatchtitletxtvw = (TextView)view.findViewById(R.id.namedispatchtitletxtvw);

        refonetitletxtvw = (TextView)view.findViewById(R.id.refonetitletxtvw);
        cmpnynamereftitletxtvw = (TextView)view.findViewById(R.id.cmpnynamereftitletxtvw);
        cmpnynamerefdesctxtvw = (TextView)view.findViewById(R.id.cmpnynamerefdesctxtvw);
        namereftitletxtvw = (TextView)view.findViewById(R.id.namereftitletxtvw);
        namerefdesctxtvw = (TextView)view.findViewById(R.id.namerefdesctxtvw);
        mobilenoreftitletxtvw = (TextView)view.findViewById(R.id.mobilenoreftitletxtvw);
        mobilenorefdesctxtvw = (TextView)view.findViewById(R.id.mobilenorefdesctxtvw);
        emailidreftitletxtvw = (TextView)view.findViewById(R.id.emailidreftitletxtvw);
        emailidrefdesctxtvw = (TextView)view.findViewById(R.id.emailidrefdesctxtvw);

        reftwotitletxtvw = (TextView)view.findViewById(R.id.reftwotitletxtvw);
        cmpnynamereftwotitletxtvw = (TextView)view.findViewById(R.id.cmpnynamereftwotitletxtvw);
        cmpnynamereftwodesctxtvw = (TextView)view.findViewById(R.id.cmpnynamereftwodesctxtvw);
        namereftwotitletxtvw = (TextView)view.findViewById(R.id.namereftwotitletxtvw);
        namereftwodesctxtvw = (TextView)view.findViewById(R.id.namereftwodesctxtvw);
        mobilenoreftwotitletxtvw = (TextView)view.findViewById(R.id.mobilenoreftwotitletxtvw);
        mobilenoreftwodesctxtvw = (TextView)view.findViewById(R.id.mobilenoreftwodesctxtvw);
        emailidreftwotitletxtvw = (TextView)view.findViewById(R.id.emailidreftwotitletxtvw);
        emailidreftwodesctxtvw = (TextView)view.findViewById(R.id.emailidreftwodesctxtvw);



        refthreetitletxtvw = (TextView)view.findViewById(R.id.refthreetitletxtvw);
        cmpnynamerefthreetitletxtvw = (TextView)view.findViewById(R.id.cmpnynamerefthreetitletxtvw);
        cmpnynamerefthreedesctxtvw = (TextView)view.findViewById(R.id.cmpnynamerefthreedesctxtvw);
        namerefthreetitletxtvw = (TextView)view.findViewById(R.id.namerefthreetitletxtvw);
        namerefthreedesctxtvw = (TextView)view.findViewById(R.id.namerefthreedesctxtvw);
        mobilenorefthreetitletxtvw = (TextView)view.findViewById(R.id.mobilenorefthreetitletxtvw);
        mobilenorefthreedesctxtvw = (TextView)view.findViewById(R.id.mobilenorefthreedesctxtvw);
        emailidrefthreetitletxtvw = (TextView)view.findViewById(R.id.emailidrefthreetitletxtvw);
        emailidrefthreedesctxtvw = (TextView)view.findViewById(R.id.emailidrefthreedesctxtvw);

        editbusinessdetailstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), EditBusinessBankDetailsActivity.class);
                startActivity(i);

            }
        });
        editpersonaldetailstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), EditPersonalDetailsActivity.class);
                startActivity(i);
            }
        });


        nulldoctxt.setTypeface(source_sans_pro_normal);
        documentchecklisttxt.setTypeface(source_sans_pro_normal);
        compregcerttxt.setTypeface(source_sans_pro_normal);
        dicregtxt.setTypeface(source_sans_pro_normal);
        pancardtxt.setTypeface(source_sans_pro_normal);
        gstinregcerttxt.setTypeface(source_sans_pro_normal);
        udyogaadharmemotxt.setTypeface(source_sans_pro_normal);
        cancelledchqtxt.setTypeface(source_sans_pro_normal);


        forofficeuseonlytxt.setTypeface(source_sans_pro_normal);
        groupnametitletxt.setTypeface(source_sans_pro_normal);
        groupnamedettxt.setTypeface(source_sans_pro_normal);
        groupcodetitletxt.setTypeface(source_sans_pro_normal);
        groupcodedettxt.setTypeface(source_sans_pro_normal);
        locationtitletxt.setTypeface(source_sans_pro_normal);
        locationdettxt.setTypeface(source_sans_pro_normal);

        companynametxt.setTypeface(source_sans_pro_normal);
        nullbusinessbanktxt.setTypeface(source_sans_pro_normal);

        personaldetailstxt.setTypeface(source_sans_pro_normal);
        editpersonaldetailstxt.setTypeface(source_sans_pro_normal);

        businessdetailstxt.setTypeface(source_sans_pro_normal);
        editbusinessdetailstxt.setTypeface(source_sans_pro_normal);

        citytxt.setTypeface(source_sans_pro_normal);
        profilecompletenesstxt.setTypeface(source_sans_pro_normal);
        percenttxt.setTypeface(source_sans_pro_normal);
        lastupdatedtxt.setTypeface(source_sans_pro_normal);

        nametitletxt.setTypeface(source_sans_pro_normal);
        namedettxt.setTypeface(source_sans_pro_normal);
        dobtitletxt.setTypeface(source_sans_pro_normal);
        dobdetailtxt.setTypeface(source_sans_pro_normal);
        companytypetitletxt.setTypeface(source_sans_pro_normal);
        companytypedetailtxt.setTypeface(source_sans_pro_normal);
        companynametitletxt.setTypeface(source_sans_pro_normal);
        companynamedetailtxt.setTypeface(source_sans_pro_normal);
        designationtitletxt.setTypeface(source_sans_pro_normal);
        designationdetailtxt.setTypeface(source_sans_pro_normal);
        emailtitletxt.setTypeface(source_sans_pro_normal);
        emaildetailtxt.setTypeface(source_sans_pro_normal);
        landlinetitletxt.setTypeface(source_sans_pro_normal);
        landlinedetailtxt.setTypeface(source_sans_pro_normal);
        mobiletitletxt.setTypeface(source_sans_pro_normal);
        mobiledetailtxt.setTypeface(source_sans_pro_normal);
        addresstitletxt.setTypeface(source_sans_pro_normal);
        addressdetailtxt.setTypeface(source_sans_pro_normal);

        pannotitletxt.setTypeface(source_sans_pro_normal);
        pannodettxt.setTypeface(source_sans_pro_normal);
        cintitletxt.setTypeface(source_sans_pro_normal);
        cindetailtxt.setTypeface(source_sans_pro_normal);
        gstintitletxt.setTypeface(source_sans_pro_normal);
        gstindetailtxt.setTypeface(source_sans_pro_normal);
        udyogaadharnumbertitletxt.setTypeface(source_sans_pro_normal);
        udyogaadhaarnumberdetailtxt.setTypeface(source_sans_pro_normal);
        billingaddresstitletxt.setTypeface(source_sans_pro_normal);
        billingaddressdetailtxt.setTypeface(source_sans_pro_normal);
        citytitletxt.setTypeface(source_sans_pro_normal);
        citydetailtxt.setTypeface(source_sans_pro_normal);
        statetitletxt.setTypeface(source_sans_pro_normal);
        statedetailtxt.setTypeface(source_sans_pro_normal);
        pincodetitletxt.setTypeface(source_sans_pro_normal);
        pincodedetailtxt.setTypeface(source_sans_pro_normal);
        establishmentdatetitletxt.setTypeface(source_sans_pro_normal);
        establishmentdatedetailtxt.setTypeface(source_sans_pro_normal);

        bankdetailstxt.setTypeface(source_sans_pro_normal);
        editbankdetailstxt.setTypeface(source_sans_pro_normal);

        banknametitletxt.setTypeface(source_sans_pro_normal);
        banknamedettxt.setTypeface(source_sans_pro_normal);
        branchtitletxt.setTypeface(source_sans_pro_normal);
        branchdetailtxt.setTypeface(source_sans_pro_normal);
        accountnotitletxt.setTypeface(source_sans_pro_normal);
        accountnodetailtxt.setTypeface(source_sans_pro_normal);
        ifsccodetitletxt.setTypeface(source_sans_pro_normal);
        ifsccodedetailtxt.setTypeface(source_sans_pro_normal);

        installedcapacitydetailstxt.setTypeface(source_sans_pro_normal);
        editinstalledcapacitydetailstxt.setTypeface(source_sans_pro_normal);

        accountnotitletxtvw.setTypeface(source_sans_pro_normal);
        contactdetailstxt.setTypeface(source_sans_pro_normal);
        editcontactdetailstxt.setTypeface(source_sans_pro_normal);
        nameacctitletxtvw.setTypeface(source_sans_pro_normal);
        nameaccdesctxtvw.setTypeface(source_sans_pro_normal);
        phonenoacctitletxtvw.setTypeface(source_sans_pro_normal);
        phonenoaccdesctxtvw.setTypeface(source_sans_pro_normal);
        mobilenoacctitletxtvw.setTypeface(source_sans_pro_normal);
        mobilenoaccdesctxtvw.setTypeface(source_sans_pro_normal);
        emailidacctitletxtvw.setTypeface(source_sans_pro_normal);
        emailidaccdesctxtvw.setTypeface(source_sans_pro_normal);

        salesnmarkettitletxtvw.setTypeface(source_sans_pro_normal);
        namesalesmarkettitletxtvw.setTypeface(source_sans_pro_normal);
        namesalesmarketdesctxtvw.setTypeface(source_sans_pro_normal);
        phonenosalesmarkettitletxtvw.setTypeface(source_sans_pro_normal);
        phonenosalesmarketdesctxtvw.setTypeface(source_sans_pro_normal);
        mobilenosalesmarkettitletxtvw.setTypeface(source_sans_pro_normal);
        mobilenosalesmarketdesctxtvw.setTypeface(source_sans_pro_normal);
        emailidsalesmarkettitletxtvw.setTypeface(source_sans_pro_normal);
        emailidsalesmarketdesctxtvw.setTypeface(source_sans_pro_normal);

        dispatchtitletxtvw.setTypeface(source_sans_pro_normal);
        namedispatchdesctxtvw.setTypeface(source_sans_pro_normal);
        phonenodispatchtitletxtvw.setTypeface(source_sans_pro_normal);
        phonenodispatchdesctxtvw.setTypeface(source_sans_pro_normal);
        mobilenodispatchtitletxtvw.setTypeface(source_sans_pro_normal);
        mobilenodispatchdesctxtvw.setTypeface(source_sans_pro_normal);
        emailiddispatchtitletxtvw.setTypeface(source_sans_pro_normal);
        emailiddispatchdesctxtvw.setTypeface(source_sans_pro_normal);
        namedispatchtitletxtvw.setTypeface(source_sans_pro_normal);



        typeofmaterialstitletxt.setTypeface(source_sans_pro_normal);
        capacitytxt.setTypeface(source_sans_pro_normal);
        pellettitletxt.setTypeface(source_sans_pro_normal);
        pelletcapacitytxt.setTypeface(source_sans_pro_normal);
        spongeirontitletxt.setTypeface(source_sans_pro_normal);
        spongeironcapacitytxt.setTypeface(source_sans_pro_normal);
        ingotstitletxt.setTypeface(source_sans_pro_normal);
        ingotsdetailtxt.setTypeface(source_sans_pro_normal);
        billetstitletxt.setTypeface(source_sans_pro_normal);
        billetsdetailtxt.setTypeface(source_sans_pro_normal);

        nullinstalltxt.setTypeface(source_sans_pro_normal);
        nullcontacttxt.setTypeface(source_sans_pro_normal);

        referencedetailstxt.setTypeface(source_sans_pro_normal);
        editreferencedetailstxt.setTypeface(source_sans_pro_normal);

        refonetitletxtvw.setTypeface(source_sans_pro_normal);
        cmpnynamereftitletxtvw.setTypeface(source_sans_pro_normal);
        cmpnynamerefdesctxtvw.setTypeface(source_sans_pro_normal);
        namereftitletxtvw.setTypeface(source_sans_pro_normal);
        namerefdesctxtvw.setTypeface(source_sans_pro_normal);
        mobilenoreftitletxtvw.setTypeface(source_sans_pro_normal);
        mobilenorefdesctxtvw.setTypeface(source_sans_pro_normal);
        emailidreftitletxtvw.setTypeface(source_sans_pro_normal);
        emailidrefdesctxtvw.setTypeface(source_sans_pro_normal);

        reftwotitletxtvw.setTypeface(source_sans_pro_normal);
        cmpnynamereftwotitletxtvw.setTypeface(source_sans_pro_normal);
        cmpnynamereftwodesctxtvw.setTypeface(source_sans_pro_normal);
        namereftwotitletxtvw.setTypeface(source_sans_pro_normal);
        namereftwodesctxtvw.setTypeface(source_sans_pro_normal);
        mobilenoreftwotitletxtvw.setTypeface(source_sans_pro_normal);
        mobilenoreftwodesctxtvw.setTypeface(source_sans_pro_normal);
        emailidreftwotitletxtvw.setTypeface(source_sans_pro_normal);
        emailidreftwodesctxtvw.setTypeface(source_sans_pro_normal);



        refthreetitletxtvw.setTypeface(source_sans_pro_normal);
        cmpnynamerefthreetitletxtvw.setTypeface(source_sans_pro_normal);
        cmpnynamerefthreedesctxtvw.setTypeface(source_sans_pro_normal);
        namerefthreetitletxtvw.setTypeface(source_sans_pro_normal);
        namerefthreedesctxtvw.setTypeface(source_sans_pro_normal);
        mobilenorefthreetitletxtvw.setTypeface(source_sans_pro_normal);
        mobilenorefthreedesctxtvw.setTypeface(source_sans_pro_normal);
        emailidrefthreetitletxtvw.setTypeface(source_sans_pro_normal);
        emailidrefthreedesctxtvw.setTypeface(source_sans_pro_normal);
        nullreftxt.setTypeface(source_sans_pro_normal);

        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);



        if (NetworkUtility.checkConnectivity(getActivity())) {
            String getuserdetailsurl = APIName.URL + "/user/getUserDetails?user_code=" + userCodestr ;
            System.out.println("GET USER DETAILS URL IS---" + getuserdetailsurl);
            GetUserDetailsAPI(getuserdetailsurl);

        } else {
            util.dialog(getActivity(), "Please check your internet connection.");
        }

        editcontactdetailstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), EditContactDetailsActivity.class);
                startActivity(i);
            }
        });


        editinstalledcapacitydetailstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), EditInstalledCapacityActivity.class);
                startActivity(i);
            }
        });

        editreferencedetailstxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), EditReferenceActivity.class);
                startActivity(i);
            }
        });


        profileiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                final Dialog mDialog = new Dialog(getActivity(), R.style.DialogSlideAnim);
                mDialog.setCancelable(false);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                mDialog.setContentView(R.layout.dialog_upload_image);
                cancel_dialog = (ImageView) mDialog
                        .findViewById(R.id.cancel_dialog);
                upload_from_memory = (TextView) mDialog
                        .findViewById(R.id.upload_from_memory);
                click_image_from_camera = (TextView)mDialog
                        .findViewById(R.id.click_image_from_camera);
                upload_from_memory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                      //  HomeActivity parentActivity = (HomeActivity)getActivity().getParent();
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        getActivity().startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
                        mDialog.dismiss();

                    }
                });
                click_image_from_camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       /* Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(i, CAPTURE_IMAGE_CAPTURE_CODE);
                        mDialog.dismiss();*/

                        if (checkPermission()) {
                            //main logic or main code

                            // . write your main code to execute, It will execute if the permission is already given.
                            Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(i, CAPTURE_IMAGE_CAPTURE_CODE);
                            mDialog.dismiss();

                        } else {

                            requestPermission();
                           // mDialog.dismiss();
                        }


                    }
                });
                cancel_dialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();

            }
        });




        return  view;

    }

    private boolean checkPermission() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CAMERA);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);


        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED;

    }


    private void requestPermission() {

       /* ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);

        //new
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this,  new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
            return;
        }*/


        ActivityCompat.requestPermissions(getActivity(), new String[]
                {
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,

                }, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
        /*    case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                    Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(i, CAPTURE_IMAGE_CAPTURE_CODE);
                  //  mDialog.dismiss();



                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;*/

            case PERMISSION_REQUEST_CODE:

                if (grantResults.length > 0) {

                    boolean CameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean WriteExternalStoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (CameraPermission && WriteExternalStoragePermission) {

                        Toast.makeText(getActivity(), "Permission Granted", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getActivity(),"Permission Denied",Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    private void GetUserDetailsAPI(String url) {
      /*  pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();
*/
      //  getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
       //         WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        bar.setVisibility(View.VISIBLE);
        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                     //   pdia.dismiss();

                    //    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //dialog.dismiss();
                                bar.setVisibility(View.GONE);

                                usercodetxt.setText("User Code : "+userCodestr);

                                System.out.println("RESPONSE OF GET USER DETAILS API IS---" + response);

                                //   Toast.makeText(getApplicationContext(), response,
                                //         Toast.LENGTH_SHORT).show();

                                try {
                                    //jsonArray = new JSONArray(response);


                                    userdetailsjson = new JSONObject(response);


                                    SingletonActivity.getUserDetailsJson = userdetailsjson;

                                    final JSONObject userdetjson = userdetailsjson.getJSONObject("userDetails");
                                    System.out.println("USER DET JSON IS---" + userdetjson);

                                    JSONObject stuserjson = userdetjson.getJSONObject("st_user");
                                    System.out.println("ST USER JSON IS---" + stuserjson);
                                    SingletonActivity.stuserjson = stuserjson;


                                    stuserprofjson = userdetjson.getJSONObject("st_user_prof");
                                    System.out.println("ST USER PROF JSON IS---" + stuserprofjson);


                                    if(userdetjson.getString("contact").length()>0) {
                                        contactjsonArray = userdetjson.getJSONArray("contact");
                                        System.out.println("CONTACT JSONARRAY IS---" + contactjsonArray);
                                        SingletonActivity.contactjsonArray = contactjsonArray;
                                    }


                                    if(userdetjson.getString("comment").length()>0) {

                                        installedCapacityjsonArray = userdetjson.getJSONArray("comment");
                                        System.out.println("INSTALLED CAPACITY JSONARRAY IS---" + installedCapacityjsonArray);
                                        SingletonActivity.installedCapacityjsonArray = installedCapacityjsonArray;
                                    }

                                    if(userdetjson.getString("reference_info").length()>0) {
                                        referencejsonArray = userdetjson.getJSONArray("reference_info");
                                        System.out.println("REFERENCE INFO JSONARRAY IS---" + referencejsonArray);
                                        SingletonActivity.referencejsonArray = referencejsonArray;
                                    }


                                    if(userdetjson.getString("doc").length()>0)
                                    {


                           /*     registration_certificate_file
                                        vat_registration_file
                                dic_registration_file
                                        pan_card_file
                                cancelled_cheque_file
                                        udyog_aadhar_memorandum_file*/

                                        if(!userdetjson.getJSONObject("doc").getString("registration_certificate_file").equalsIgnoreCase(""))
                                        {


                                            viewdociv1.setVisibility(View.VISIBLE);
                                            compregcerttxt.setVisibility(View.VISIBLE);

                                        }
                                        else
                                        {
                                            viewdociv1.setVisibility(View.GONE);
                                            compregcerttxt.setVisibility(View.GONE);
                                            active_iv1.setVisibility(View.GONE);
                                        }

                                        if(!userdetjson.getJSONObject("doc").getString("dic_registration_file").equalsIgnoreCase(""))
                                        {


                                            viewdociv2.setVisibility(View.VISIBLE);
                                            dicregtxt.setVisibility(View.VISIBLE);
                                            //   Toast.makeText(getActivity(), "T 1===",
                                            //       Toast.LENGTH_SHORT).show();


                                        }
                                        else
                                        {
                                            viewdociv2.setVisibility(View.GONE);
                                            dicregtxt.setVisibility(View.GONE);
                                            active_iv2.setVisibility(View.GONE);
                                            //  Toast.makeText(getActivity(), "T 2===",
                                            //         Toast.LENGTH_SHORT).show();
                                        }

                                        if(!userdetjson.getJSONObject("doc").getString("pan_card_file").equalsIgnoreCase(""))
                                        {


                                            viewdociv3.setVisibility(View.VISIBLE);
                                            pancardtxt.setVisibility(View.VISIBLE);

                                        }
                                        else
                                        {
                                            viewdociv3.setVisibility(View.GONE);
                                            pancardtxt.setVisibility(View.GONE);
                                            active_iv3.setVisibility(View.GONE);
                                        }

                                        if(!userdetjson.getJSONObject("doc").getString("vat_registration_file").equalsIgnoreCase(""))
                                        {


                                            viewdociv4.setVisibility(View.VISIBLE);
                                            gstinregcerttxt.setVisibility(View.VISIBLE);

                                        }
                                        else
                                        {
                                            viewdociv4.setVisibility(View.GONE);
                                            gstinregcerttxt.setVisibility(View.GONE);
                                            active_iv4.setVisibility(View.GONE);
                                        }

                                        if(!userdetjson.getJSONObject("doc").getString("udyog_aadhar_memorandum_file").equalsIgnoreCase(""))
                                        {


                                            viewdociv5.setVisibility(View.VISIBLE);
                                            udyogaadharmemotxt.setVisibility(View.VISIBLE);
                                            active_iv5.setVisibility(View.VISIBLE);

                                        }
                                        else
                                        {
                                            viewdociv5.setVisibility(View.GONE);
                                            udyogaadharmemotxt.setVisibility(View.GONE);
                                            active_iv5.setVisibility(View.GONE);
                                        }

                                        if(!userdetjson.getJSONObject("doc").getString("cancelled_cheque_file").equalsIgnoreCase(""))
                                        {


                                            viewdociv6.setVisibility(View.VISIBLE);
                                            cancelledchqtxt.setVisibility(View.VISIBLE);

                                        }
                                        else
                                        {
                                            viewdociv6.setVisibility(View.GONE);
                                            cancelledchqtxt.setVisibility(View.GONE);
                                            active_iv6.setVisibility(View.GONE);
                                        }


                                        if(userdetjson.getJSONObject("doc").getString("registration_certificate").equalsIgnoreCase("1"))
                                        {
                                            active_iv1.setVisibility(View.VISIBLE);
                                            active_iv1.setBackgroundResource(R.mipmap.checkbox_on);
                                        }
                                        else
                                        {
                                            active_iv1.setVisibility(View.VISIBLE);
                                            active_iv1.setBackgroundResource(R.mipmap.checkbox_off);


                                            if(userdetjson.getJSONObject("doc").getString("registration_certificate_file").equalsIgnoreCase(""))
                                            {
                                                active_iv1.setVisibility(View.GONE);
                                                compregcerttxt.setVisibility(View.GONE);
                                            }
                                        }

                                        if(userdetjson.getJSONObject("doc").getString("dic_registration").equalsIgnoreCase("1"))
                                        {
                                            active_iv2.setVisibility(View.VISIBLE);
                                            active_iv2.setBackgroundResource(R.mipmap.checkbox_on);

                                            //    Toast.makeText(getActivity(), "T 3===",
                                            //    Toast.LENGTH_SHORT).show();
                                        }
                                        else
                                        {
                                            active_iv2.setVisibility(View.VISIBLE);
                                            active_iv2.setBackgroundResource(R.mipmap.checkbox_off);

                                            //     Toast.makeText(getActivity(), "T 4===",
                                            //            Toast.LENGTH_SHORT).show();

                                            if(userdetjson.getJSONObject("doc").getString("dic_registration_file").equalsIgnoreCase(""))
                                            {
                                                active_iv2.setVisibility(View.GONE);
                                                dicregtxt.setVisibility(View.GONE);

                                                //  Toast.makeText(getActivity(), "T 5===",
                                                //        Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                        if(userdetjson.getJSONObject("doc").getString("pan_card").equalsIgnoreCase("1"))
                                        {
                                            active_iv3.setVisibility(View.VISIBLE);
                                            active_iv3.setBackgroundResource(R.mipmap.checkbox_on);
                                        }
                                        else
                                        {
                                            active_iv3.setVisibility(View.VISIBLE);
                                            active_iv3.setBackgroundResource(R.mipmap.checkbox_off);

                                            if(userdetjson.getJSONObject("doc").getString("pan_card_file").equalsIgnoreCase(""))
                                            {
                                                active_iv3.setVisibility(View.GONE);
                                                pancardtxt.setVisibility(View.GONE);
                                            }
                                        }

                                        if(userdetjson.getJSONObject("doc").getString("vat_registration").equalsIgnoreCase("1"))
                                        {
                                            active_iv4.setVisibility(View.VISIBLE);
                                            active_iv4.setBackgroundResource(R.mipmap.checkbox_on);
                                        }
                                        else
                                        {
                                            active_iv4.setVisibility(View.VISIBLE);
                                            active_iv4.setBackgroundResource(R.mipmap.checkbox_off);

                                            if(userdetjson.getJSONObject("doc").getString("vat_registration_file").equalsIgnoreCase(""))
                                            {
                                                active_iv4.setVisibility(View.GONE);
                                                gstinregcerttxt.setVisibility(View.GONE);
                                            }
                                        }

                                        if(userdetjson.getJSONObject("doc").getString("udyog_aadhar_memorandum").equalsIgnoreCase("1"))
                                        {
                                            active_iv5.setVisibility(View.VISIBLE);
                                            active_iv5.setBackgroundResource(R.mipmap.checkbox_on);
                                        }
                                        else
                                        {
                                            active_iv5.setVisibility(View.VISIBLE);
                                            active_iv5.setBackgroundResource(R.mipmap.checkbox_off);

                                            if(userdetjson.getJSONObject("doc").getString("udyog_aadhar_memorandum_file").equalsIgnoreCase(""))
                                            {
                                                active_iv5.setVisibility(View.GONE);
                                                udyogaadharmemotxt.setVisibility(View.GONE);
                                            }
                                        }

                                        if(userdetjson.getJSONObject("doc").getString("cancelled_cheque").equalsIgnoreCase("1"))
                                        {
                                            active_iv6.setVisibility(View.VISIBLE);
                                            active_iv6.setBackgroundResource(R.mipmap.checkbox_on);
                                        }
                                        else
                                        {
                                            active_iv6.setVisibility(View.VISIBLE);
                                            active_iv6.setBackgroundResource(R.mipmap.checkbox_off);

                                            if(userdetjson.getJSONObject("doc").getString("cancelled_cheque_file").equalsIgnoreCase(""))
                                            {
                                                active_iv6.setVisibility(View.GONE);
                                                cancelledchqtxt.setVisibility(View.GONE);
                                            }

                                        }

                                        nulldoctxt.setVisibility(View.GONE);



/*
                                nulldoctxt.setVisibility(View.GONE);
                                compregcerttxt.setVisibility(View.VISIBLE);
                                dicregtxt.setVisibility(View.VISIBLE);
                                pancardtxt.setVisibility(View.VISIBLE);
                                gstinregcerttxt.setVisibility(View.VISIBLE);
                                udyogaadharmemotxt.setVisibility(View.VISIBLE);
                                cancelledchqtxt.setVisibility(View.VISIBLE);*/
/*
                                active_iv1.setVisibility(View.VISIBLE);
                                active_iv2.setVisibility(View.VISIBLE);
                                active_iv3.setVisibility(View.VISIBLE);
                                active_iv4.setVisibility(View.VISIBLE);
                                active_iv5.setVisibility(View.VISIBLE);
                                active_iv6.setVisibility(View.VISIBLE);

                                viewdociv1.setVisibility(View.VISIBLE);
                                viewdociv2.setVisibility(View.VISIBLE);
                                viewdociv3.setVisibility(View.VISIBLE);
                                viewdociv4.setVisibility(View.VISIBLE);
                                viewdociv5.setVisibility(View.VISIBLE);
                                viewdociv6.setVisibility(View.VISIBLE);*/

                                        viewdociv1.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                Intent i = new Intent(getActivity(), DocWebViewActivity.class);
                                                try {
                                                    SingletonActivity.doc = userdetjson.getJSONObject("doc").getString("registration_certificate_file");
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                startActivity(i);
                                            }
                                        });

                                        viewdociv2.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                Intent i = new Intent(getActivity(), DocWebViewActivity.class);
                                                try {
                                                    SingletonActivity.doc = userdetjson.getJSONObject("doc").getString("dic_registration_file");
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                startActivity(i);
                                            }
                                        });

                                        viewdociv3.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                Intent i = new Intent(getActivity(), DocWebViewActivity.class);
                                                try {
                                                    SingletonActivity.doc = userdetjson.getJSONObject("doc").getString("pan_card_file");
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                startActivity(i);
                                            }
                                        });

                                        viewdociv4.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                Intent i = new Intent(getActivity(), DocWebViewActivity.class);
                                                try {
                                                    SingletonActivity.doc = userdetjson.getJSONObject("doc").getString("vat_registration_file");
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                startActivity(i);
                                            }
                                        });

                                        viewdociv5.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                Intent i = new Intent(getActivity(), DocWebViewActivity.class);
                                                try {
                                                    SingletonActivity.doc = userdetjson.getJSONObject("doc").getString("udyog_aadhar_memorandum_file");
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                startActivity(i);
                                            }
                                        });

                                        viewdociv6.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                Intent i = new Intent(getActivity(), DocWebViewActivity.class);
                                                try {
                                                    SingletonActivity.doc = userdetjson.getJSONObject("doc").getString("cancelled_cheque_file");
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                startActivity(i);
                                            }
                                        });

                                        //    Toast.makeText(getActivity(),"RCF=="+userdetjson.getJSONObject("doc").getString("udyog_aadhar_memorandum").length(),Toast.LENGTH_SHORT).show();
                                    }
                                    else
                                    {
                                        //     Toast.makeText(getActivity(),"NO RCF==",Toast.LENGTH_SHORT).show();



                                        nulldoctxt.setVisibility(View.VISIBLE);
                                        compregcerttxt.setVisibility(View.GONE);
                                        dicregtxt.setVisibility(View.GONE);
                                        pancardtxt.setVisibility(View.GONE);
                                        gstinregcerttxt.setVisibility(View.GONE);
                                        udyogaadharmemotxt.setVisibility(View.GONE);
                                        cancelledchqtxt.setVisibility(View.GONE);

                                        active_iv1.setVisibility(View.GONE);
                                        active_iv2.setVisibility(View.GONE);
                                        active_iv3.setVisibility(View.GONE);
                                        active_iv4.setVisibility(View.GONE);
                                        active_iv5.setVisibility(View.GONE);
                                        active_iv6.setVisibility(View.GONE);

                                        viewdociv1.setVisibility(View.GONE);
                                        viewdociv2.setVisibility(View.GONE);
                                        viewdociv3.setVisibility(View.GONE);
                                        viewdociv4.setVisibility(View.GONE);
                                        viewdociv5.setVisibility(View.GONE);
                                        viewdociv6.setVisibility(View.GONE);


                                    }



                                    //=================================================================



                                    int emptycount = 0;


                                    if(stuserjson.getString("first_name").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }

                                    System.out.println("Empty 1---"+ emptycount);

                                    if(stuserjson.getString("last_name").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }

                                    System.out.println("Empty 2---"+ emptycount);

                                    if(stuserprofjson.getString("company_type").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }

                                    System.out.println("Empty 3---"+ emptycount);

                                    System.out.println("company_type " + stuserprofjson.getString("company_type"));
                                    System.out.println("cin " + stuserprofjson.getString("cin"));
                                    System.out.println("isactive--- " + stuserjson.getString("is_active"));

                                    if(stuserprofjson.getString("company_name").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }

                                    System.out.println("Empty 4---"+ emptycount);

                                    if(stuserjson.getString("email").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }

                                    System.out.println("Empty 5---"+ emptycount);

                                    if(stuserjson.getString("mobile_num").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }

                                    System.out.println("Empty 6---"+ emptycount);

                                   /* if(stuserjson.getString("dob").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }*/

                                    System.out.println("Empty 7---"+ emptycount);

                                    if(stuserjson.getString("address").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }

                                    System.out.println("Empty 8---"+ emptycount);

                                    if(stuserjson.getString("landmark").length()==0)
                                    {
                                        emptycount++;
                                    }
                                    System.out.println("Empty 9---"+ emptycount);

                                    if(stuserjson.getString("city").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;

                                    }
                                    System.out.println("Empty 10---"+ emptycount);

                                    if(stuserjson.getString("pincode").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }

                                    System.out.println("Empty 11---"+ emptycount);





                                    if(stuserjson.getString("district").length()==0)
                                    {
                                        emptycount++;

                                    }

                                    System.out.println("Empty 12---"+ emptycount);



                                    if(stuserjson.getString("state").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }
                                    System.out.println("Empty 13---"+ emptycount);

                                    if(stuserjson.getString("phone").length()==0)
                                    {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }

                                    System.out.println("Empty 14---"+ emptycount);

                                    System.out.println("Total Empty Fields in StUserJson 1---"+ emptycount);

                                    System.out.println("isMandatory 1---"+ isMandatoryFilled);

                                    //----------------------------------------------






                                    if (stuserprofjson.getString("pan").length() == 0) {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }

                                    System.out.println("isMandatory 21---"+ isMandatoryFilled);


                                    if(stuserprofjson.getString("cin").length()==0)
                                    {
                                        emptycount++;

                                    }

                                    System.out.println("isMandatory 22---"+ isMandatoryFilled);

                                    if (stuserprofjson.getString("vat").length() == 0) {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }

                                    System.out.println("isMandatory 23---"+ isMandatoryFilled);
                                    if (stuserprofjson.getString("excise_ecc_code").length() == 0) {
                                        emptycount++;

                                    }
                                    System.out.println("isMandatory 24---"+ isMandatoryFilled);
                              /*  if (stuserprofjson.getString("excise_range").length() == 0) {
                                    emptycount++;
                                    isMandatoryFilled = false;
                                }*/
                                    System.out.println("isMandatory 25---"+ isMandatoryFilled);
                             /*   if (stuserprofjson.getString("excise_division").length() == 0) {
                                    emptycount++;
                                    isMandatoryFilled = false;
                                }*/
                                    System.out.println("isMandatory 26---"+ isMandatoryFilled);
                                    if (stuserprofjson.getString("billing_address").length() == 0) {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }
                                    System.out.println("isMandatory 27---"+ isMandatoryFilled);
                                    if (stuserprofjson.getString("billing_city").length() == 0) {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }
                                    if (stuserprofjson.getString("billing_pincode").length() == 0) {
                                        emptycount++;
                                    }

                                    if (stuserprofjson.getString("billing_state").length() == 0) {
                                        emptycount++;
                                    }

                            /*    if (stuserprofjson.getString("ecc_rate").length() == 0) {
                                    emptycount++;
                                }
*/
                                    if (stuserprofjson.getString("establishment_date").length() == 0) {
                                        emptycount++;
                                    }

                                    if (stuserprofjson.getString("bank_account_name").length() == 0) {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }
                                    System.out.println("isMandatory 28---"+ isMandatoryFilled);
                                    if (stuserprofjson.getString("bank_branch_name").length() == 0) {
                                        emptycount++;
                                    }
                                    System.out.println("isMandatory 29---"+ isMandatoryFilled);
                                    if (stuserprofjson.getString("bank_account_number").length() == 0) {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }
                                    System.out.println("isMandatory 210---"+ isMandatoryFilled);
                                    if (stuserprofjson.getString("bank_ifsc_code").length() == 0) {
                                        emptycount++;
                                        isMandatoryFilled = false;
                                    }
                                    System.out.println("isMandatory 211---"+ isMandatoryFilled);
                                    if (stuserprofjson.getString("comp_website_url").length() == 0) {
                                        emptycount++;
                                    }

                                    if (stuserprofjson.getString("picture").length() == 0) {
                                        emptycount++;
                                    }


                                    System.out.println("Total Empty Fields in StUserJson 2---"+ emptycount);

                                    System.out.println("isMandatory 2---"+ isMandatoryFilled);
                                    //--------------------------------------------------------


                                    if(userdetjson.getString("contact").length()>0) {

                                    }


                                    else
                                    {

                                      //  emptycount = emptycount + 12;
                                        emptycount = emptycount + 8;
                                        isMandatoryFilled = false;

                                    }


                                    System.out.println("Total Empty Fields in StUserJson 3---"+ emptycount);

                                    System.out.println("isMandatory 3---"+ isMandatoryFilled);

                                    //--------------------------------------------------------


                                    if(userdetjson.getString("comment").length()>0) {

                                    }


                                    else
                                    {

                                        emptycount = emptycount + 4;
                                        isMandatoryFilled = false;

                                    }

                                    System.out.println("Total Empty Fields in StUserJson 4---"+ emptycount);

                                    System.out.println("isMandatory 4---"+ isMandatoryFilled);
                                    //--------------------------------------

                                    //   System.out.println("Reference Info Length---" + referencejsonArray.length());
                                    if(userdetjson.getString("reference_info").length()>0) {
                                        if (referencejsonArray.length() == 1) {


                                            try {
                                                if (referencejsonArray.getJSONObject(0).getString("ref_company_name").length() == 0) {
                                                    emptycount++;

                                                }

                                                if (referencejsonArray.getJSONObject(0).getString("ref_name").length() == 0) {
                                                    emptycount++;
                                                }

                                                if (referencejsonArray.getJSONObject(0).getString("ref_mobile").length() == 0) {
                                                    emptycount++;
                                                }
                                                if (referencejsonArray.getJSONObject(0).getString("ref_email").length() == 0) {
                                                    emptycount++;
                                                }

                                            } catch (JSONException e1) {
                                                e1.printStackTrace();
                                            }


                                        }

                                        if (referencejsonArray.length() == 2) {


                                            try {
                                                if (referencejsonArray.getJSONObject(0).getString("ref_company_name").length() == 0) {
                                                    emptycount++;

                                                }

                                                if (referencejsonArray.getJSONObject(0).getString("ref_name").length() == 0) {
                                                    emptycount++;
                                                }

                                                if (referencejsonArray.getJSONObject(0).getString("ref_mobile").length() == 0) {
                                                    emptycount++;
                                                }
                                                if (referencejsonArray.getJSONObject(0).getString("ref_email").length() == 0) {
                                                    emptycount++;
                                                }

                                                if (referencejsonArray.getJSONObject(1).getString("ref_company_name").length() == 0) {
                                                    emptycount++;

                                                }

                                                if (referencejsonArray.getJSONObject(1).getString("ref_name").length() == 0) {
                                                    emptycount++;
                                                }

                                                if (referencejsonArray.getJSONObject(1).getString("ref_mobile").length() == 0) {
                                                    emptycount++;
                                                }
                                                if (referencejsonArray.getJSONObject(1).getString("ref_email").length() == 0) {
                                                    emptycount++;
                                                }

                                            } catch (JSONException e1) {
                                                e1.printStackTrace();
                                            }


                                        }

                                        if (referencejsonArray.length() == 3) {


                                            try {
                                                if (referencejsonArray.getJSONObject(0).getString("ref_company_name").length() == 0) {
                                                    emptycount++;

                                                }

                                                if (referencejsonArray.getJSONObject(0).getString("ref_name").length() == 0) {
                                                    emptycount++;
                                                }

                                                if (referencejsonArray.getJSONObject(0).getString("ref_mobile").length() == 0) {
                                                    emptycount++;
                                                }
                                                if (referencejsonArray.getJSONObject(0).getString("ref_email").length() == 0) {
                                                    emptycount++;
                                                }

                                                if (referencejsonArray.getJSONObject(1).getString("ref_company_name").length() == 0) {
                                                    emptycount++;

                                                }

                                                if (referencejsonArray.getJSONObject(1).getString("ref_name").length() == 0) {
                                                    emptycount++;
                                                }

                                                if (referencejsonArray.getJSONObject(1).getString("ref_mobile").length() == 0) {
                                                    emptycount++;
                                                }
                                                if (referencejsonArray.getJSONObject(1).getString("ref_email").length() == 0) {
                                                    emptycount++;
                                                }

                                                if (referencejsonArray.getJSONObject(2).getString("ref_company_name").length() == 0) {
                                                    emptycount++;

                                                }

                                                if (referencejsonArray.getJSONObject(2).getString("ref_name").length() == 0) {
                                                    emptycount++;
                                                }

                                                if (referencejsonArray.getJSONObject(2).getString("ref_mobile").length() == 0) {
                                                    emptycount++;
                                                }
                                                if (referencejsonArray.getJSONObject(2).getString("ref_email").length() == 0) {
                                                    emptycount++;
                                                }

                                            } catch (JSONException e1) {
                                                e1.printStackTrace();
                                            }


                                        }
                                    }
                                    else
                                    {

                                        emptycount = emptycount + 12;
                                        //isMandatoryFilled = false;

                                    }

                                    System.out.println("Total Empty Fields in StUserJson 5---"+ emptycount);

                                    System.out.println("isMandatory 5---"+ isMandatoryFilled);


                                    //----------------------------




                                    System.out.println("Total Empty Fields in StUserJson---"+ emptycount);

                                    System.out.println("Mandatory fields filled status 2---"+ isMandatoryFilled);

                                    if(isMandatoryFilled == false)
                                    {

                                        cmplteproftxt.setVisibility(View.VISIBLE);
                                        cmplteproftxt.setText("Complete Your Profile to go to Trading Board.");
                                        cmplteproftxt.setTextColor(Color.parseColor("#ff0000"));

                                        SingletonActivity.menu.getItem(0).setEnabled(false);
                                        SingletonActivity.menu.getItem(1).setEnabled(false);
                                        SingletonActivity.menu.getItem(2).setEnabled(false);


                                        System.out.println("IN MP1---");

                                    }

                                    else
                                    {
                                        System.out.println("isactive status---"+ stuserjson.getString("is_active"));

                                        if (stuserjson.getString("is_active").equalsIgnoreCase("0")) {
                                            cmplteproftxt.setVisibility(View.VISIBLE);
                                            cmplteproftxt.setText("Admin has not yet approved your profile." +
                                                    "\n Once approved then you can move to Trading Board.");
                                            cmplteproftxt.setTextColor(Color.parseColor("#ff0000"));

                                            SingletonActivity.menu.getItem(0).setEnabled(false);
                                            SingletonActivity.menu.getItem(1).setEnabled(false);
                                            SingletonActivity.menu.getItem(2).setEnabled(false);

                                            System.out.println("IN MP2---");

                                        }

                                        if (stuserjson.getString("is_active").equalsIgnoreCase("1")) {
                                            if(SingletonActivity.isPassedFromLogin==true)
                                            {
                                                System.out.println("IN MP3A---");

                                                SingletonActivity.menu.getItem(0).setEnabled(true);
                                                SingletonActivity.menu.getItem(1).setEnabled(true);
                                                SingletonActivity.menu.getItem(2).setEnabled(true);

                                                selectFragment(SingletonActivity.menu.getItem(0));


                                            }
                                            else
                                            {
                                                System.out.println("IN MP3B---");
                                            }

                                        }

                                        System.out.println("IN MP3---");



                                    }




                                    int totalcount  = 63;
                                    int filledcount = totalcount - emptycount;

                                    System.out.println("Fill Fields in StUserJson---"+ filledcount);

                                    float fractionpercentage = (float)(((float)filledcount/(float)totalcount) * 100);




                                    int percentage = (int)Math.round(fractionpercentage);
                                    System.out.println("Profile Percentage---"+ percentage);

                                    progress.setProgress(percentage);

                                    percenttxt.setText(percentage+ "%");

                                    //===============================================================================
                                    System.out.println("CITY NAME IS---" + stuserjson.getString("city"));



                                    SingletonActivity.stuserprofjson = stuserprofjson;


                                    String companynamestr = stuserprofjson.getString("company_name");
                                    String designationstr = stuserprofjson.getString("designation");

                                    if(designationstr.equalsIgnoreCase("null"))
                                    {
                                        designationdetailtxt.setText("--");
                                    }
                                    else {
                                        designationdetailtxt.setText(designationstr);
                                    }

                                    if(companynamestr.equalsIgnoreCase(""))
                                    {
                                        companynametxt.setTextColor(Color.parseColor("#d3d3d3"));
                                    }
                                    else {
                                        companynametxt.setText(companynamestr);
                                        companynametxt.setTextColor(Color.parseColor("#FF2F2F2F"));
                                    }

                                    if(stuserjson.getString("city").equalsIgnoreCase(""))
                                    {
                                        citytxt.setTextColor(Color.parseColor("#d3d3d3"));
                                    }
                                    else {
                                        citytxt.setText(stuserjson.getString("city"));
                                        citytxt.setTextColor(Color.parseColor("#FF2F2F2F"));
                                    }

                                    if(userdetjson.getString("picture").length()>0) {


                                        String picture = userdetjson.getString("picture");
                                        System.out.println("PICTURE IS---" + picture);

                                        if (!picture.equalsIgnoreCase("")) {


                                         //  Picasso.with(getActivity()).load(picture).resize(314,512).transform(new CircleTransform()).into((profileiv));



                                            PicassoTrustAll.getInstance(getActivity())
                                                    .load(picture)
                                                    .into(profileiv);


                                        } else {

                                            profileiv.setImageResource(R.mipmap.ic_cam);
                                            profileiv.setBackgroundResource(R.drawable.circle);
                                            profileiv.setPadding(30, 30, 30, 30);

                                        }
                                    }

                                    if(!(SingletonActivity.stuserjson.getString("first_name").equalsIgnoreCase(""))&&(!SingletonActivity.stuserjson.getString("last_name").equalsIgnoreCase(""))) {
                                        namedettxt.setText(SingletonActivity.stuserjson.getString("first_name") + " " + SingletonActivity.stuserjson.getString("last_name"));
                                    }
                                    else
                                    {
                                        namedettxt.setText("--");
                                    }

                                    if(!SingletonActivity.stuserprofjson.getString("company_type").equalsIgnoreCase("null")) {
                                        companytypedetailtxt.setText(SingletonActivity.stuserprofjson.getString("company_type"));
                                    }
                                    else
                                    {
                                        companytypedetailtxt.setText("--");
                                    }

                                    if(!SingletonActivity.stuserprofjson.getString("company_name").equalsIgnoreCase("")) {
                                        companynamedetailtxt.setText(SingletonActivity.stuserprofjson.getString("company_name"));
                                    }
                                    else
                                    {
                                        companynamedetailtxt.setText("--");
                                    }



                                    emaildetailtxt.setText(SingletonActivity.stuserjson.getString("email"));
                                    mobiledetailtxt.setText(SingletonActivity.stuserjson.getString("mobile_num"));

                                    if(!SingletonActivity.stuserjson.getString("phone").equalsIgnoreCase("")) {
                                        landlinedetailtxt.setText(SingletonActivity.stuserjson.getString("phone"));
                                    }
                                    else
                                    {
                                        landlinedetailtxt.setText("--");
                                    }

                                    if(!(SingletonActivity.stuserjson.getString("address").equalsIgnoreCase(""))&&!(SingletonActivity.stuserjson.getString("city").equalsIgnoreCase(""))&&!(SingletonActivity.stuserjson.getString("address").equalsIgnoreCase(""))&&!(SingletonActivity.stuserjson.getString("city").equalsIgnoreCase(""))) {
                                        addressdetailtxt.setText(SingletonActivity.stuserjson.getString("address") + "," + SingletonActivity.stuserjson.getString("city") + "," + SingletonActivity.stuserjson.getString("state") + "," + SingletonActivity.stuserjson.getString("pincode"));
                                    }
                                    else
                                    {
                                        addressdetailtxt.setText("--");
                                    }


                                    if(!SingletonActivity.stuserjson.getString("dob").equalsIgnoreCase("null")) {
                                        dobdetailtxt.setText(SingletonActivity.stuserjson.getString("dob"));

                                        if(!SingletonActivity.stuserjson.getString("dob").equalsIgnoreCase("")) {
                                            dobdetailtxt.setText(SingletonActivity.stuserjson.getString("dob"));
                                        }
                                        else
                                        {
                                            dobdetailtxt.setText("--");

                                        }
                                    }
                                    else
                                    {
                                        dobdetailtxt.setText("--");

                                    }


                                    //=================================================================================//

                                    int businessdetailemptycount = 0;

                                    if(!SingletonActivity.stuserprofjson.getString("pan").equalsIgnoreCase(""))
                                    {
                                        pannodettxt.setText(SingletonActivity.stuserprofjson.getString("pan"));
                                        businessdetailemptycount++;
                                    }
                                    else{
                                        pannodettxt.setText("--");
                                    }

                                    if(!SingletonActivity.stuserprofjson.getString("cin").equalsIgnoreCase(""))
                                    {
                                        cindetailtxt.setText(SingletonActivity.stuserprofjson.getString("cin"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        cindetailtxt.setText("--");

                                    }

                                    if(!SingletonActivity.stuserprofjson.getString("vat").equalsIgnoreCase(""))
                                    {
                                        gstindetailtxt.setText(SingletonActivity.stuserprofjson.getString("vat"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        gstindetailtxt.setText("--");
                                    }

                                    if(!SingletonActivity.stuserprofjson.getString("excise_ecc_code").equalsIgnoreCase(""))
                                    {
                                        udyogaadhaarnumberdetailtxt.setText(SingletonActivity.stuserprofjson.getString("excise_ecc_code"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        udyogaadhaarnumberdetailtxt.setText("--");
                                    }



                                    if((!SingletonActivity.stuserprofjson.getString("billing_address").equalsIgnoreCase(""))) {
                                        billingaddressdetailtxt.setText(SingletonActivity.stuserprofjson.getString("billing_address"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        billingaddressdetailtxt.setText("--");
                                    }

                                    if((!SingletonActivity.stuserprofjson.getString("billing_address").equalsIgnoreCase("null"))) {
                                        billingaddressdetailtxt.setText(SingletonActivity.stuserprofjson.getString("billing_address"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        billingaddressdetailtxt.setText("--");
                                    }

                                    if((!SingletonActivity.stuserprofjson.getString("billing_city").equalsIgnoreCase(""))) {
                                        citydetailtxt.setText(SingletonActivity.stuserprofjson.getString("billing_city"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        citydetailtxt.setText("--");
                                    }

                                    if((!SingletonActivity.stuserprofjson.getString("billing_city").equalsIgnoreCase("null"))) {
                                        citydetailtxt.setText(SingletonActivity.stuserprofjson.getString("billing_city"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        citydetailtxt.setText("--");
                                    }

                                    if((!SingletonActivity.stuserprofjson.getString("billing_state").equalsIgnoreCase(""))) {
                                        statedetailtxt.setText(SingletonActivity.stuserprofjson.getString("billing_state"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        statedetailtxt.setText("--");
                                    }

                                    if((!SingletonActivity.stuserprofjson.getString("billing_state").equalsIgnoreCase("null"))) {
                                        statedetailtxt.setText(SingletonActivity.stuserprofjson.getString("billing_state"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        statedetailtxt.setText("--");
                                    }


                                    if((!SingletonActivity.stuserprofjson.getString("billing_pincode").equalsIgnoreCase(""))) {
                                        pincodedetailtxt.setText(SingletonActivity.stuserprofjson.getString("billing_pincode"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        pincodedetailtxt.setText("--");
                                    }

                                    if((!SingletonActivity.stuserprofjson.getString("billing_pincode").equalsIgnoreCase("null"))) {
                                        pincodedetailtxt.setText(SingletonActivity.stuserprofjson.getString("billing_pincode"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        pincodedetailtxt.setText("--");
                                    }

                                    if((!SingletonActivity.stuserprofjson.getString("establishment_date").equalsIgnoreCase(""))) {
                                        establishmentdatedetailtxt.setText(SingletonActivity.stuserprofjson.getString("establishment_date"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        establishmentdatedetailtxt.setText("--");
                                    }

                                    if((!SingletonActivity.stuserprofjson.getString("establishment_date").equalsIgnoreCase("null"))) {
                                        establishmentdatedetailtxt.setText(SingletonActivity.stuserprofjson.getString("establishment_date"));
                                        businessdetailemptycount++;
                                    }
                                    else
                                    {
                                        establishmentdatedetailtxt.setText("--");
                                    }




                                    //=============================================================================

                                    if(!SingletonActivity.stuserprofjson.getString("bank_account_name").equalsIgnoreCase(""))
                                    {
                                        banknamedettxt.setText(SingletonActivity.stuserprofjson.getString("bank_account_name"));
                                        businessdetailemptycount++;

                                    }
                                    else
                                    {
                                        banknamedettxt.setText("--");
                                    }

                                    if(!SingletonActivity.stuserprofjson.getString("bank_branch_name").equalsIgnoreCase(""))
                                    {
                                        branchdetailtxt.setText(SingletonActivity.stuserprofjson.getString("bank_branch_name"));
                                        businessdetailemptycount++;

                                    }
                                    else
                                    {
                                        branchdetailtxt.setText("--");
                                    }

                                    if(!SingletonActivity.stuserprofjson.getString("bank_account_number").equalsIgnoreCase(""))
                                    {
                                        accountnodetailtxt.setText(SingletonActivity.stuserprofjson.getString("bank_account_number"));
                                        businessdetailemptycount++;

                                    }
                                    else
                                    {
                                        accountnodetailtxt.setText("--");
                                    }

                                    if(!SingletonActivity.stuserprofjson.getString("bank_ifsc_code").equalsIgnoreCase(""))
                                    {
                                        ifsccodedetailtxt.setText(SingletonActivity.stuserprofjson.getString("bank_ifsc_code"));
                                        businessdetailemptycount++;

                                    }
                                    else
                                    {
                                        ifsccodedetailtxt.setText("--");
                                    }



                                    if(!SingletonActivity.stuserprofjson.getString("group_company_name").equalsIgnoreCase(""))
                                    {
                                        groupnamedettxt.setText(SingletonActivity.stuserprofjson.getString("group_company_name"));
                                        businessdetailemptycount++;

                                    }
                                    else
                                    {
                                        groupnamedettxt.setText("--");
                                    }

                                    if((SingletonActivity.stuserprofjson.getString("group_company_code").length()!=4))
                                    {
                                        groupcodedettxt.setText(SingletonActivity.stuserprofjson.getString("group_company_code"));
                                        businessdetailemptycount++;

                                    }
                                    else
                                    {
                                        groupcodedettxt.setText("--");
                                    }

                                    if(!SingletonActivity.stuserjson.getString("location_name").equalsIgnoreCase(""))
                                    {
                                        locationdettxt.setText(SingletonActivity.stuserjson.getString("location_name"));
                                        businessdetailemptycount++;

                                    }
                                    else
                                    {
                                        locationdettxt.setText("--");
                                    }




                                    System.out.println("businessdetailemptycount========="+ businessdetailemptycount);

                                    //  if(businessdetailemptycount==0||businessdetailemptycount==5||businessdetailemptycount==12)
                                    if(businessdetailemptycount==0||businessdetailemptycount==5)
                                    {
                                        businessdetailsrel.setVisibility(View.GONE);
                                        bankdetailsrel.setVisibility(View.GONE);
                                        bankdetailstxt.setVisibility(View.GONE);
                                        nullbusinessbanktxt.setVisibility(View.VISIBLE);
                                        nullbusinessbanktxt.setText("No Data");
                                    }
                                    else
                                    {
                                        businessdetailsrel.setVisibility(View.VISIBLE);
                                        bankdetailsrel.setVisibility(View.VISIBLE);
                                        nullbusinessbanktxt.setVisibility(View.GONE);
                                        editbusinessdetailstxt.setText("EDIT");

                                    }

                                    //Installed Capacity======================================================================


                                    if(SingletonActivity.installedCapacityjsonArray!=null)
                                    {

                                        installedcapacitydetailsrel.setVisibility(View.VISIBLE);
                                        nullinstalltxt.setVisibility(View.GONE);
                                        editinstalledcapacitydetailstxt.setText("EDIT");



                                        for (int i = 0; i < SingletonActivity.installedCapacityjsonArray.length(); i++) {

                                            System.out.println("INSTALLED CAPACITY SINGLETON JSON ARRAY---"+ SingletonActivity.installedCapacityjsonArray);

                                            if (SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category").equalsIgnoreCase("Billets")) {
                                                billetsdetailtxt.setText(SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity") + " MT/day");
                                            }


                                            if (SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category").equalsIgnoreCase("Sponge Iron")) {
                                                spongeironcapacitytxt.setText(SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity") + " MT/day");
                                            }

                                            if (SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category").equalsIgnoreCase("Ingots")) {
                                                ingotsdetailtxt.setText(SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity") + " MT/day");
                                            }


                                            if (SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category").equalsIgnoreCase("Pellets")) {

                                                pelletcapacitytxt.setText(SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity") + " MT/day");
                                            }



                                        }

                                    }

                                    else
                                    {
                                        installedcapacitydetailsrel.setVisibility(View.GONE);
                                        nullinstalltxt.setVisibility(View.VISIBLE);
                                        nullinstalltxt.setText("No Data");

                                    }



                                    //Contact Details========================================================
                                    if (SingletonActivity.contactjsonArray!=null) {

                                        maincontactrel.setVisibility(View.VISIBLE);
                                        nullcontacttxt.setVisibility(View.GONE);
                                        editcontactdetailstxt.setText("EDIT");


                                        for (int i = 0; i < SingletonActivity.contactjsonArray.length(); i++) {
                                            if (SingletonActivity.contactjsonArray.getJSONObject(i).getString("user_type").equalsIgnoreCase("22")) {
                                                nameaccdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("first_name"));

                                                if(!SingletonActivity.contactjsonArray.getJSONObject(i).getString("phone").equalsIgnoreCase("")) {
                                                    phonenoaccdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("phone"));
                                                }
                                                else
                                                {
                                                    phonenoaccdesctxtvw.setText("--");
                                                }


                                                    mobilenoaccdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("mobile_num"));


                                                    emailidaccdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("email"));

                                            }


                                            if (SingletonActivity.contactjsonArray.getJSONObject(i).getString("user_type").equalsIgnoreCase("23")) {
                                                namesalesmarketdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("first_name"));

                                                if(!SingletonActivity.contactjsonArray.getJSONObject(i).getString("phone").equalsIgnoreCase("")) {
                                                    phonenosalesmarketdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("phone"));
                                                }
                                                else
                                                {
                                                    phonenosalesmarketdesctxtvw.setText("--");
                                                }
                                                mobilenosalesmarketdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("mobile_num"));
                                                emailidsalesmarketdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("email"));
                                            }

                                            if (SingletonActivity.contactjsonArray.getJSONObject(i).getString("user_type").equalsIgnoreCase("21")) {
                                                namedispatchdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("first_name"));

                                                if(!SingletonActivity.contactjsonArray.getJSONObject(i).getString("phone").equalsIgnoreCase("")) {
                                                    phonenodispatchdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("phone"));
                                                }
                                                else
                                                {
                                                    phonenodispatchdesctxtvw.setText("--");
                                                }
                                                mobilenodispatchdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("mobile_num"));
                                                emailiddispatchdesctxtvw.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("email"));
                                            }
                                        }
                                    }
                                    else
                                    {

                                        maincontactrel.setVisibility(View.GONE);
                                        nullcontacttxt.setVisibility(View.VISIBLE);
                                        nullcontacttxt.setText("No Data");

                                    }
                                    //References=====================================================================

                                    if(SingletonActivity.referencejsonArray!=null) {

                                        mainreferencerel.setVisibility(View.VISIBLE);
                                        nullreftxt.setVisibility(View.GONE);
                                        editreferencedetailstxt.setText("EDIT");

                                        System.out.println("referencejsonArray.length ==="+ SingletonActivity.referencejsonArray);

                                        if(SingletonActivity.referencejsonArray.length()==1) {


                                            reftwotitletxtvw.setVisibility(View.GONE);
                                            cmpnynamereftwotitletxtvw.setVisibility(View.GONE);
                                            namereftwotitletxtvw.setVisibility(View.GONE);
                                            mobilenoreftwotitletxtvw.setVisibility(View.GONE);
                                            emailidreftwotitletxtvw.setVisibility(View.GONE);

                                            cmpnynamereftwodesctxtvw.setVisibility(View.GONE);
                                            namereftwodesctxtvw.setVisibility(View.GONE);
                                            mobilenoreftwodesctxtvw.setVisibility(View.GONE);
                                            emailidreftwodesctxtvw.setVisibility(View.GONE);


                                            refthreetitletxtvw.setVisibility(View.GONE);
                                            cmpnynamerefthreetitletxtvw.setVisibility(View.GONE);
                                            namerefthreetitletxtvw.setVisibility(View.GONE);
                                            mobilenorefthreetitletxtvw.setVisibility(View.GONE);
                                            emailidrefthreetitletxtvw.setVisibility(View.GONE);


                                            cmpnynamerefthreedesctxtvw.setVisibility(View.GONE);
                                            namerefthreedesctxtvw.setVisibility(View.GONE);
                                            mobilenorefthreedesctxtvw.setVisibility(View.GONE);
                                            emailidrefthreedesctxtvw.setVisibility(View.GONE);


                                            if(!SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_company_name").equalsIgnoreCase(""))
                                            {
                                                cmpnynamerefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_company_name"));
                                            }
                                            else
                                            {
                                                cmpnynamereftitletxtvw.setVisibility(View.GONE);
                                                cmpnynamerefdesctxtvw.setVisibility(View.GONE);
                                            }

                                            if(!SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_name").equalsIgnoreCase(""))
                                            {
                                                namerefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_name"));
                                            }
                                            else
                                            {
                                                namereftitletxtvw.setVisibility(View.GONE);
                                                namerefdesctxtvw.setVisibility(View.GONE);
                                            }

                                            if(!SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_mobile").equalsIgnoreCase(""))
                                            {
                                                mobilenorefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_mobile"));
                                            }
                                            else
                                            {
                                                mobilenoreftitletxtvw.setVisibility(View.GONE);
                                                mobilenorefdesctxtvw.setVisibility(View.GONE);
                                            }

                                            if(!SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_email").equalsIgnoreCase(""))
                                            {
                                                emailidrefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_email"));
                                            }
                                            else
                                            {
                                                emailidreftitletxtvw.setVisibility(View.GONE);
                                                emailidrefdesctxtvw.setVisibility(View.GONE);
                                            }

                                            if(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_company_name").equalsIgnoreCase("")&&SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_name").equalsIgnoreCase("")&&SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_mobile").equalsIgnoreCase("")&&SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_email").equalsIgnoreCase(""))
                                            {
                                                nullreftxt.setVisibility(View.VISIBLE);
                                                refonetitletxtvw.setVisibility(View.GONE);
                                                nullreftxt.setText("No Data");
                                            }



                                        }

                                        if(SingletonActivity.referencejsonArray.length()==2) {

                                            refthreetitletxtvw.setVisibility(View.GONE);
                                            cmpnynamerefthreetitletxtvw.setVisibility(View.GONE);
                                            namerefthreetitletxtvw.setVisibility(View.GONE);
                                            mobilenorefthreetitletxtvw.setVisibility(View.GONE);
                                            emailidrefthreetitletxtvw.setVisibility(View.GONE);

                                            cmpnynamerefthreedesctxtvw.setVisibility(View.GONE);
                                            namerefthreedesctxtvw.setVisibility(View.GONE);
                                            mobilenorefthreedesctxtvw.setVisibility(View.GONE);
                                            emailidrefthreedesctxtvw.setVisibility(View.GONE);

                                            cmpnynamerefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_company_name"));
                                            namerefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_name"));
                                            mobilenorefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_mobile"));
                                            emailidrefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_email"));

                                            cmpnynamereftwodesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_company_name"));
                                            namereftwodesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_name"));
                                            mobilenoreftwodesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_mobile"));
                                            emailidreftwodesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_email"));


                                        }

                                        if(SingletonActivity.referencejsonArray.length()==3) {

                                            cmpnynamerefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_company_name"));
                                            namerefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_name"));
                                            mobilenorefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_mobile"));
                                            emailidrefdesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_email"));

                                            cmpnynamereftwodesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_company_name"));
                                            namereftwodesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_name"));
                                            mobilenoreftwodesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_mobile"));
                                            emailidreftwodesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_email"));



                                            cmpnynamerefthreedesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_company_name"));
                                            namerefthreedesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_name"));
                                            mobilenorefthreedesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_mobile"));
                                            emailidrefthreedesctxtvw.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_email"));

                                        }

                                    }

                                    else
                                    {
                                        mainreferencerel.setVisibility(View.GONE);
                                        nullreftxt.setVisibility(View.VISIBLE);
                                        nullreftxt.setText("No Data");

                              /* refonetitletxtvw.setVisibility(View.GONE);
                               cmpnynamereftitletxtvw.setVisibility(View.GONE);
                               namereftitletxtvw.setVisibility(View.GONE);
                               mobilenoreftitletxtvw.setVisibility(View.GONE);
                               emailidreftitletxtvw.setVisibility(View.GONE);*/
                                    }
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                  //  pdia.dismiss();
                                 //   getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            //dialog.dismiss();
                                            bar.setVisibility(View.GONE);

                                        }
                                    }, 3000); // 3000 milliseconds delay


                                    e.printStackTrace();
                                    //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }, 3000); // 3000 milliseconds delay



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                      //  pdia.dismiss();

                      //  getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //dialog.dismiss();
                                bar.setVisibility(View.GONE);

                            }
                        }, 3000); // 3000 milliseconds delay


                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        if(networkResponse!=null) {

                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(getActivity(), messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                System.out.println("get user details params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }

    protected void selectFragment(MenuItem item) {

        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.menu_home:
                // Action to perform when Home Menu item is selected.
                pushFragment(new HomeFragment());
                break;

            case R.id.menu_trade:

                clickedcount = clickedcount + 1;
                SingletonActivity.clickedcount = clickedcount;
                // Action to perform when Account Menu item is selected.
                pushFragment(new TradeFragment());
                break;

            case R.id.menu_transaction:
                // Action to perform when Bag Menu item is selected.
                pushFragment(new TransactionFragment());
                break;

            case R.id.menu_profile:

                // Action to perform when Account Menu item is selected.
                pushFragment(new ProfileFragment());
                break;
         /*   case R.id.menu_more:
                // Action to perform when Account Menu item is selected.
                pushFragment(new MoreFragment());
                break;*/
        }
    }


    /**
     * Method to push any fragment into given id.
     *
     * @param fragment An instance of Fragment to show into the given id.
     */
    protected void pushFragment(Fragment fragment) {
        if (fragment == null)
            return;

        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (ft != null) {
                ft.replace(R.id.rootLayout, fragment);
                //ft.commit();
                ft.commitAllowingStateLoss();
            }
        }
    }




}
