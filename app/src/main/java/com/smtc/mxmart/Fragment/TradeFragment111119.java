package com.smtc.mxmart.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smtc.mxmart.APIName;
import com.smtc.mxmart.AddNewOfferLiveTradeEnhancementActivity0712;
import com.smtc.mxmart.EnquiryActivity;
import com.smtc.mxmart.HomeActivity;
import com.smtc.mxmart.NetworkUtility;
import com.smtc.mxmart.PicassoTrustAll;
import com.smtc.mxmart.R;
import com.smtc.mxmart.SingletonActivity;
import com.smtc.mxmart.UtilsDialog;
import com.smtc.mxmart.ViewLiveTradeActivity1012;
import com.smtc.mxmart.ViewPigIronActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;


public class TradeFragment111119 extends Fragment {

    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String userCodestr,mobilenumstr;
    UtilsDialog util = new UtilsDialog();
    boolean isDailyLimitAvailable = false;
    String limitidstr,addlimitstr;
    TextView setdailylimittext,noenquirytxt;
    RelativeLayout setdailylimitrelative;
    ProgressDialog pdia;
    CustomAdap customadap;
    CustomFinishedEnqAdap customfinishedenqadap;
    PigIronCustomAdap pigIronCustomAdap;
    ListView livetradelistvw,enquirylistvw,pigironlistvw;
    TextView noliveoffertxt,addnewoffertext,nopigirontxt;
    String tag_json_obj = "json_obj_req";
    int i = 0;
    RelativeLayout pigironrelative,addnewofferrelative,semifinishedrelative,finishedrelative,semifinisheduirelative,finisheduirelative,pigironuirelative;
    Context thiscontext;
    String  livesales;
    private ProgressBar bar,progressBarFinished;
    Handler handler = new Handler();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.fragment_trade2, container, false);


        System.out.println("IN TRADE FRAGMENT=========================");
        thiscontext = container.getContext();

        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        livesales = prefs.getString("livesales",null);

        nopigirontxt = (TextView)view.findViewById(R.id.nopigirontxt);
        noenquirytxt = (TextView)view.findViewById(R.id.noenquirytxt);
        semifinisheduirelative = (RelativeLayout)view.findViewById(R.id.semifinisheduirelative);
        finisheduirelative = (RelativeLayout)view.findViewById(R.id.finisheduirelative);
        semifinishedrelative = (RelativeLayout)view.findViewById(R.id.semifinishedrelative);
        finishedrelative = (RelativeLayout)view.findViewById(R.id.finishedrelative);
        addnewoffertext = (TextView)view.findViewById(R.id.addnewoffertext);
        bar = (ProgressBar)view.findViewById(R.id.progressBar);
        progressBarFinished = (ProgressBar)view.findViewById(R.id.progressBarFinished);
        setdailylimittext = (TextView)view.findViewById(R.id.setdailylimittext);
        setdailylimitrelative = (RelativeLayout) view.findViewById(R.id.setdailylimitrelative);
        livetradelistvw = (ListView)view.findViewById(R.id.livetradelistvw);
        enquirylistvw = (ListView)view.findViewById(R.id.enquirylistvw);
        pigironlistvw = (ListView)view.findViewById(R.id.pigironlistvw);
        noliveoffertxt = (TextView)view.findViewById(R.id.noliveoffertxt);
        addnewofferrelative = (RelativeLayout)view.findViewById(R.id.addnewofferrelative);
        pigironuirelative = (RelativeLayout)view.findViewById(R.id.pigironuirelative);

        addnewofferrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
        addnewoffertext.setTextColor(Color.parseColor("#808080"));
        addnewofferrelative.setEnabled(false);


        setdailylimitrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
        setdailylimittext.setTextColor(Color.parseColor("#808080"));
        setdailylimitrelative.setEnabled(false);

        SingletonActivity.isUpdateClicked = false;
        SingletonActivity.isLiveSalesClicked = false;

        semifinisheduirelative = (RelativeLayout)view.findViewById(R.id.semifinisheduirelative);
        finisheduirelative = (RelativeLayout)view.findViewById(R.id.finisheduirelative);
        semifinishedrelative = (RelativeLayout)view.findViewById(R.id.semifinishedrelative);
        finishedrelative = (RelativeLayout)view.findViewById(R.id.finishedrelative);
        pigironrelative = (RelativeLayout)view.findViewById(R.id.pigironrelative);


        if (NetworkUtility.checkConnectivity(getActivity())) {
            String ValidateTradeTimesUrl = APIName.URL + "/home/validateTradeTime";
            System.out.println("VALIDATE TRADE TIMES URL IS---" + ValidateTradeTimesUrl);
            ValidateTradeTimesAPI(ValidateTradeTimesUrl);

        } else {
            util.dialog(getActivity(), "Please check your internet connection.");
        }



        semifinishedrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                semifinisheduirelative.setVisibility(View.VISIBLE);
                finisheduirelative.setVisibility(View.GONE);
                pigironuirelative.setVisibility(View.GONE);
                semifinishedrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                finishedrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
                pigironrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
            }
        });

        finishedrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                semifinisheduirelative.setVisibility(View.GONE);
                finisheduirelative.setVisibility(View.VISIBLE);
                pigironuirelative.setVisibility(View.GONE);
                finishedrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                semifinishedrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
                pigironrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);

                if (NetworkUtility.checkConnectivity(getActivity())) {
                    String FinishedEnqListUrl = APIName.URL + "/livetrading/materialList";
                    System.out.println("FINISHED ENQUIRY LIST URL IS---" + FinishedEnqListUrl);
                    FinishedEnqListAPI(FinishedEnqListUrl);

                } else {
                    util.dialog(getActivity(), "Please check your internet connection.");
                }

            }
        });


        pigironrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                semifinisheduirelative.setVisibility(View.GONE);
                finisheduirelative.setVisibility(View.GONE);
                pigironuirelative.setVisibility(View.VISIBLE);
                finishedrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
                semifinishedrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
                pigironrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);

                if (NetworkUtility.checkConnectivity(getActivity())) {
                    String GetLiveOfferPigIronUrl = APIName.URL+"/liveTrading/getLiveOfferPig";
                    System.out.println("GET LIVE OFFER PIG IRON URL IS---"+ GetLiveOfferPigIronUrl);
                    GetLiveOfferPigIronAPI(GetLiveOfferPigIronUrl);

                } else {
                    util.dialog(getActivity(), "Please check your internet connection.");
                }


            }
        });


        if(SingletonActivity.enquiryfromplatespage == true)
        {
            semifinisheduirelative.setVisibility(View.GONE);
            finisheduirelative.setVisibility(View.VISIBLE);
            pigironuirelative.setVisibility(View.GONE);
            finishedrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
            semifinishedrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
            pigironrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);

            if (NetworkUtility.checkConnectivity(getActivity())) {
                String FinishedEnqListUrl = APIName.URL + "/livetrading/materialList";
                System.out.println("FINISHED ENQUIRY LIST URL IS---" + FinishedEnqListUrl);
                FinishedEnqListAPI(FinishedEnqListUrl);

            } else {
                util.dialog(getActivity(), "Please check your internet connection.");
            }
        }

        if(SingletonActivity.fromviewpigiron == true)
        {

            semifinisheduirelative.setVisibility(View.GONE);
            finisheduirelative.setVisibility(View.GONE);
            pigironuirelative.setVisibility(View.VISIBLE);
            finishedrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
            semifinishedrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
            pigironrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);

            if (NetworkUtility.checkConnectivity(getActivity())) {
                String GetLiveOfferPigIronUrl = APIName.URL+"/liveTrading/getLiveOfferPig";
                System.out.println("GET LIVE OFFER PIG IRON URL IS---"+ GetLiveOfferPigIronUrl);
                GetLiveOfferPigIronAPI(GetLiveOfferPigIronUrl);

            } else {
                util.dialog(getActivity(), "Please check your internet connection");
            }
        }




        if(SingletonActivity.fromviewlivetrade == true)
        {

            semifinisheduirelative.setVisibility(View.VISIBLE);
            finisheduirelative.setVisibility(View.GONE);
            pigironuirelative.setVisibility(View.GONE);
            semifinishedrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
            finishedrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
            pigironrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
        }

        addnewofferrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SingletonActivity.fromselltodaysoffer = false;

               /* Intent i = new Intent(getActivity(), AddNewOfferLiveTradeEnhancementActivity.classss);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);*/

               Intent i = new Intent(getActivity(), AddNewOfferLiveTradeEnhancementActivity0712.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

            }
        });

        return  view;

    }

    private void ValidateTradeTimesAPI(String url) {
       /*pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();
*/
       // getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        //        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        bar.setVisibility(View.VISIBLE);

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                         // pdia.dismiss();

                      //  getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


                        handler.postDelayed(new Runnable() {
                            public void run() {
                                bar.setVisibility(View.GONE);

                                System.out.println("RESPONSE OF VALIDATE TRADE TIMING API IS---" + response);



                                try {



                                    JSONObject validatetradetimejson = new JSONObject(response);
                                    System.out.println("VALIDATE TRADE TIMING JSON IS---" + validatetradetimejson);

                                    String statusstr = validatetradetimejson.getString("status");
                                    System.out.println("VALIDATE TRADE TIMING STATUS IS---" + statusstr);


                                    if (statusstr.equalsIgnoreCase("true")) {

                                        SingletonActivity.isValidTradeTimeOpen = true;

                                        String currentdate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                                        System.out.println("CURRENT DATE IS---"+ currentdate);
                                        // String currentdate = "20-06-2017";


                                        String GetLiveOfferUrl = APIName.URL+"/liveTrading/getLiveOffer";
                                        System.out.println("GET LIVE OFFER URL IS---"+ GetLiveOfferUrl);
                                        GetLiveOfferAPI(GetLiveOfferUrl);

                                        //userCodestr = "1000";
                                        String GetDailyLimitUrl = APIName.URL+"/liveTrading/getDailyLimit?user_code="+userCodestr;
                                        System.out.println("GET DAILY LIMIT URL IS---"+ GetDailyLimitUrl);
                                        GetDailyLimitAPI(GetDailyLimitUrl);





                                        addnewofferrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                        addnewoffertext.setTextColor(Color.WHITE);
                                        addnewofferrelative.setEnabled(true);


                                        setdailylimitrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                        setdailylimittext.setTextColor(Color.WHITE);
                                        setdailylimitrelative.setEnabled(true);

                                    }
                                    else {


                                        SingletonActivity.isValidTradeTimeOpen = false;
                                        UtilsDialog util = new UtilsDialog();
                                        util.dialog(getActivity(), validatetradetimejson.getString("message"));

                                        noliveoffertxt.setText(validatetradetimejson.getString("message"));

                                        noliveoffertxt.setVisibility(View.VISIBLE);
                                        livetradelistvw.setVisibility(View.GONE);


                                        addnewofferrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                        addnewoffertext.setTextColor(Color.parseColor("#808080"));
                                        addnewofferrelative.setEnabled(false);

                                        setdailylimitrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                        setdailylimittext.setTextColor(Color.parseColor("#808080"));
                                        setdailylimitrelative.setEnabled(false);
                                    }


                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                  //  pdia.dismiss();

                                 //   getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            //dialog.dismiss();
                                            bar.setVisibility(View.GONE);

                                        }
                                    }, 3000); // 3000 milliseconds delay


                                    e.printStackTrace();

                                }
                            }
                        }, 3000); // 3000 milliseconds delay




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                       // getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //dialog.dismiss();
                                bar.setVisibility(View.GONE);

                            }
                        }, 3000); // 3000 milliseconds delay


                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(getActivity(), messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();



                System.out.println("validate trade times params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

      /*  RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
*/
        if(SingletonActivity.clickedcount == 0) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
            requestQueue.add(stringRequest);
        }
        else
        {
            SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }

       // SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }

    private void GetDailyLimitAPI(String url) {

     /*   pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();
*/

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      //   pdia.dismiss();


                        System.out.println("RESPONSE OF GET DAILY LIMIT API IS---" + response);

                        try {



                            JSONObject getdailylimitjson = new JSONObject(response);
                            System.out.println("GET DAILY LIMIT JSON IS---" + getdailylimitjson);

                            String statusstr = getdailylimitjson.getString("status");




                            if (statusstr.equalsIgnoreCase("true")) {

                                JSONObject daily_limit_json = getdailylimitjson.getJSONObject("dailyLimit");


                                limitidstr = daily_limit_json.getString("limit_id");
                                addlimitstr = daily_limit_json.getString("add_limit");


                                setdailylimittext.setText(addlimitstr+" MT");

                                isDailyLimitAvailable = true;

                                System.out.println("isDailyLimitAvailable true---" + isDailyLimitAvailable);



                                setdailylimitrelative.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        setUpdateLimitDialog();


                                    }
                                });



                            }
                            else
                            {

                                isDailyLimitAvailable = false;
                                System.out.println("isDailyLimitAvailable else---" + isDailyLimitAvailable);

                                setdailylimittext.setText("Set Buying Limit");

                               // UtilsDialog util = new UtilsDialog();
                                util.dialog(getActivity(), "Please Set Buying Limit");




                                setdailylimitrelative.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        setBuyingLimitDialog();
                                    }
                                });
                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                           // pdia.dismiss();
                            e.printStackTrace();

                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                      //   pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");

                                    if (statusstr.equalsIgnoreCase("false")) {



                                        isDailyLimitAvailable = false;

                                        System.out.println("isDailyLimitAvailable false---" + isDailyLimitAvailable);

                                        util.dialog(getActivity(), "Please Set Buying Limit");

                                        if (isDailyLimitAvailable == false) {

                                            setdailylimitrelative.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    setBuyingLimitDialog();
                                                }
                                            });
                                        }



                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get live news params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

      /*  RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
*/
        if(SingletonActivity.clickedcount == 0) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
            requestQueue.add(stringRequest);
        }
        else
        {
            SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }

//        SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }

    public void setBuyingLimitDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.set_buying_limit_dialog, null);
        dialogBuilder.setView(dialogView);


        final EditText buyinglimitedt = (EditText) dialogView.findViewById(R.id.setbuyinglimitedt);
        final RelativeLayout setrel = (RelativeLayout) dialogView.findViewById(R.id.setrelative);
        final TextView settextvw = (TextView) dialogView.findViewById(R.id.settext);
        final RelativeLayout cancelrelative = (RelativeLayout) dialogView.findViewById(R.id.cancelrelative);
        final TextView canceltxtvw = (TextView) dialogView.findViewById(R.id.canceltext);

        final  AlertDialog b = dialogBuilder.create();
        b.show();


        setrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (livesales.equalsIgnoreCase("0")) {

                    String buyinglimitstr = buyinglimitedt.getText().toString();

                    if (buyinglimitstr.length() > 0) {

                        int newlimitint = Integer.parseInt(buyinglimitstr);

                        if (newlimitint % 50 == 0 && newlimitint > 0) {


                            if (NetworkUtility.checkConnectivity(getActivity())) {
                                String SetDailyLimitUrl = APIName.URL + "/liveTrading/insertUpdateDailyLimit";
                                System.out.println("SET DAILY LIMIT URL IS---" + SetDailyLimitUrl);
                                SetDailyLimitAPI(SetDailyLimitUrl, buyinglimitstr);

                            } else {
                                util.dialog(getActivity(), "Please check your internet connection.");
                            }

                        } else {
                            Toast.makeText(getActivity(), "Entered value should be multiple of 50", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Please Enter Buying Limit", Toast.LENGTH_SHORT).show();
                    }


                    // setdailylimittext.setText(buyinglimitedt.getText().toString()+"mt");
                    b.dismiss();
                }
                else
                {
                    Toast.makeText(getActivity(), "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        cancelrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }


    private void SetDailyLimitAPI(String url,final String buyinglimit) {

        pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);

        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        try {
                            if ((pdia != null) && pdia.isShowing()) {
                                pdia.dismiss();
                            }
                        } catch (final IllegalArgumentException e) {
                            // Handle or log or ignore
                        } catch (final Exception e) {
                            // Handle or log or ignore
                        } finally {
                            pdia = null;
                        }



                        System.out.println("RESPONSE OF SET DAILY LIMIT API IS---" + response);

                        Intent i = new Intent(getActivity(),HomeActivity.class);
                        SingletonActivity.setdailylimitfromtrade = true;
                        startActivity(i);






                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(getActivity(), messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_code",userCodestr);
                params.put("mobile_number",mobilenumstr);
                String fordate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                System.out.println("FOR DATE IS---"+ fordate);

                params.put("for_date",fordate);
                params.put("add_limit",buyinglimit);





                System.out.println("Set Daily Limit params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

     /*  RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);*/

        if(SingletonActivity.clickedcount == 0) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
            requestQueue.add(stringRequest);
        }
        else
        {
            SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }

        //SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }


    private void GetLiveOfferAPI(String url) {
       /* pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();
*/

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                       // pdia.dismiss();


                        System.out.println("RESPONSE OF GET LIVE OFFER API IS---" + response);


                        JSONObject GetLiveOfferJson = null;
                        try {
                            GetLiveOfferJson = new JSONObject(response);


                            String statusstr = GetLiveOfferJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                noliveoffertxt.setVisibility(View.GONE);
                                livetradelistvw.setVisibility(View.VISIBLE);

                                JSONArray liveOfferJsonArray = GetLiveOfferJson.getJSONArray("liveOffer");
                                System.out.println("GET LIVE OFFER JSONARRAY IS---" + liveOfferJsonArray);



                                    JSONArray ingotOfferJsonArray = GetLiveOfferJson.getJSONArray("ingot_offer");
                                    System.out.println("INGOT OFFER JSONARRAY IS---" + ingotOfferJsonArray);

                                    JSONArray billetOfferJsonArray = GetLiveOfferJson.getJSONArray("billet_offer");
                                    System.out.println("BILLET OFFER JSONARRAY IS---" + billetOfferJsonArray);

                                    JSONArray spongeOfferJsonArray = GetLiveOfferJson.getJSONArray("sponge_offer");
                                    System.out.println("SPONGE OFFER JSONARRAY IS---" + spongeOfferJsonArray);

                                    JSONArray pilletOfferJsonArray = GetLiveOfferJson.getJSONArray("pillet_offer");
                                    System.out.println("PILLET OFFER JSONARRAY IS---" + pilletOfferJsonArray);



                                    for (int y = 0; y < ingotOfferJsonArray.length(); y++) {
                                        liveOfferJsonArray.put(ingotOfferJsonArray.getJSONObject(y));
                                    }

                                    for (int x = 0; x < billetOfferJsonArray.length(); x++) {
                                        liveOfferJsonArray.put(billetOfferJsonArray.getJSONObject(x));
                                    }

                                    for (int z = 0; z < spongeOfferJsonArray.length(); z++) {
                                        liveOfferJsonArray.put(spongeOfferJsonArray.getJSONObject(z));
                                    }

                                    for (int w = 0; w < pilletOfferJsonArray.length(); w++) {
                                        liveOfferJsonArray.put(pilletOfferJsonArray.getJSONObject(w));
                                    }

                                    SingletonActivity.liveOfferJsonArray = liveOfferJsonArray;



                                if (getActivity() != null) {
                                    customadap = new CustomAdap(getActivity(), SingletonActivity.liveOfferJsonArray);//,leavetypelist,messagetypelist,fromdatelist,todatelist,postdatelist,statuslist);
                                    livetradelistvw.setAdapter(customadap);
                                }

                            }
                           else {

                                noliveoffertxt.setVisibility(View.VISIBLE);
                                livetradelistvw.setVisibility(View.GONE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                           // pdia.dismiss();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        //pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        noliveoffertxt.setVisibility(View.VISIBLE);
                                        livetradelistvw.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get live offer params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            System.out.println("SingletonActivity.clickedcount no.====="+ SingletonActivity.clickedcount);

           if(SingletonActivity.clickedcount == 0) {
               RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
               requestQueue.add(stringRequest);
           }
        else
           {
               SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
           }

    }

    public void setUpdateLimitDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.edit_buying_daily_limit, null);
        dialogBuilder.setView(dialogView);



        final EditText presentlimitedttxt = (EditText) dialogView.findViewById(R.id.presentlimitedt);
        final EditText newlimitedttxt = (EditText) dialogView.findViewById(R.id.newlimitedt);
        final RelativeLayout updaterel = (RelativeLayout) dialogView.findViewById(R.id.updaterelative);
        final TextView updatetextvw = (TextView) dialogView.findViewById(R.id.updatetext);
        final RelativeLayout cancelrelative = (RelativeLayout) dialogView.findViewById(R.id.cancelrelative);
        final TextView canceltxtvw = (TextView) dialogView.findViewById(R.id.canceltext);


        presentlimitedttxt.setText(addlimitstr);





        final  AlertDialog b = dialogBuilder.create();
        b.show();


        updaterel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (livesales.equalsIgnoreCase("0")) {

                    String newlimitstr = newlimitedttxt.getText().toString();

                    if (newlimitstr.length() > 0) {

                        int newlimitint = Integer.parseInt(newlimitstr);

                        if (newlimitint % 50 == 0 && newlimitint > 0) {

                            b.dismiss();


                            if (NetworkUtility.checkConnectivity(getActivity())) {
                                String UpdateDailyLimitUrl = APIName.URL + "/liveTrading/insertUpdateDailyLimit?limit_id=" + limitidstr;
                                System.out.println("UPDATE DAILY LIMIT URL IS---" + UpdateDailyLimitUrl);
                                UpdateDailyLimitAPI(UpdateDailyLimitUrl, newlimitstr);

                            } else {
                                util.dialog(getActivity(), "Please check your internet connection.");
                            }
                        } else {
                            Toast.makeText(getActivity(), "Entered value should be multiple of 50", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Please Enter New Limit", Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    Toast.makeText(getActivity(), "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                }


            }
        });
        cancelrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }

    private void UpdateDailyLimitAPI(String url, final String newlimit) {

        pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            if ((pdia != null) && pdia.isShowing()) {
                                pdia.dismiss();
                            }
                        } catch (final IllegalArgumentException e) {
                            // Handle or log or ignore
                        } catch (final Exception e) {
                            // Handle or log or ignore
                        } finally {
                            pdia = null;
                        }

                        System.out.println("RESPONSE OF UPDATE DAILY LIMIT API IS---" + response);

                        Intent i = new Intent(getActivity(),HomeActivity.class);
                        SingletonActivity.updatedailylimitfromtrade=true;
                        startActivity(i);






                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(getActivity(), messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_code",userCodestr);
                params.put("mobile_number",mobilenumstr);
                String fordate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                System.out.println("FOR DATE IS---"+ fordate);

                params.put("for_date",fordate);
                params.put("add_limit",newlimit);





                System.out.println("Set Daily Limit params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }

    private class CustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        LayoutInflater inflater = null;
        private JSONArray LiveOfferJSONArray;



        public CustomAdap(Context mainActivity,JSONArray liveOfferJSONArray)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.LiveOfferJSONArray = liveOfferJSONArray;
            inflater = (LayoutInflater)c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return LiveOfferJSONArray.length();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub



            final Holder holder = new Holder();

            final View rowView;


            rowView = inflater.inflate(R.layout.live_todays_offer_row2, null);

            holder.cmpnydesc = (TextView) rowView.findViewById(R.id.cmpnynametxtvw);
            holder.proddesc = (TextView) rowView.findViewById(R.id.proddesctxtvw);
            holder.ratingBar = (RatingBar)rowView.findViewById(R.id.ratingBar);
            holder.sub_cat_txt = (TextView) rowView.findViewById(R.id.subproddesctxtvw);
            holder.remqty = (TextView)rowView.findViewById(R.id.qtydesctxtvw);
            holder.minrateval = (TextView)rowView.findViewById(R.id.ratetxtvw);
            holder.dispatch_icon = (ImageView)rowView.findViewById(R.id.dispatch_icon);
            holder.dispatchtxt = (TextView)rowView.findViewById(R.id.dispatchtxt);
            holder.loading_icon = (ImageView)rowView.findViewById(R.id.loading_icon);
            holder.loadingtxt = (TextView)rowView.findViewById(R.id.loadingtxt);
            holder.insurance_icon = (ImageView)rowView.findViewById(R.id.insurance_icon);
            holder.insurancetxt= (TextView)rowView.findViewById(R.id.insurancetxt);
            holder.enqtext = (TextView)rowView.findViewById(R.id.enqtext);
            holder.enquiryrelative = (RelativeLayout)rowView.findViewById(R.id.enquiryrelative);
            holder.pricetxtvw = (TextView)rowView.findViewById(R.id.pricetxtvw);

            System.out.println("LiveOfferJSONArray-----"+ LiveOfferJSONArray);


            try {
                final String companyname = LiveOfferJSONArray.getJSONObject(position).getString("company_name");
                final  String categorydesc = LiveOfferJSONArray.getJSONObject(position).getString("category_name");
                final String subcategorydesc = LiveOfferJSONArray.getJSONObject(position).getString("sub_category_name");
                final String size_range_start_str = LiveOfferJSONArray.getJSONObject(position).getString("size_range_start");
                final String size_range_end_str = LiveOfferJSONArray.getJSONObject(position).getString("size_range_end");

                final String advanceratedesc = LiveOfferJSONArray.getJSONObject(position).getString("advance_rate");
                final String nextdayratedesc = LiveOfferJSONArray.getJSONObject(position).getString("next_day_rate");
                final  String regularratedesc = LiveOfferJSONArray.getJSONObject(position).getString("regular_rate");

                final  String remaining_quantity = LiveOfferJSONArray.getJSONObject(position).getString("remaining_quantity");

                final String sellidstr = LiveOfferJSONArray.getJSONObject(position).getString("sell_id");

                final String categoryidstr = LiveOfferJSONArray.getJSONObject(position).getString("category");
                final  String subcategoryidstr = LiveOfferJSONArray.getJSONObject(position).getString("sub_category");
                final String tncstr = LiveOfferJSONArray.getJSONObject(position).getString("sell_tandc");

                final String q1val = LiveOfferJSONArray.getJSONObject(position).getString("q1");
                final String q2val = LiveOfferJSONArray.getJSONObject(position).getString("q2");
                final String q3val = LiveOfferJSONArray.getJSONObject(position).getString("q3");
                final String q4val = LiveOfferJSONArray.getJSONObject(position).getString("q4");
                final String q5val = LiveOfferJSONArray.getJSONObject(position).getString("q5");
                final String q6val = LiveOfferJSONArray.getJSONObject(position).getString("q6");
                final String q7val = LiveOfferJSONArray.getJSONObject(position).getString("q7");
                final String q8val = LiveOfferJSONArray.getJSONObject(position).getString("q8");
                final String location = LiveOfferJSONArray.getJSONObject(position).getString("location");
                final String promo_code = LiveOfferJSONArray.getJSONObject(position).getString("promo_code");
                final String user_code = LiveOfferJSONArray.getJSONObject(position).getString("user_code");
                final String modified_field = LiveOfferJSONArray.getJSONObject(position).getString("modified_field");

                final String transport_str = LiveOfferJSONArray.getJSONObject(position).getString("transport");
                final String loading_str = LiveOfferJSONArray.getJSONObject(position).getString("loading");
                final String insurance_select_str = LiveOfferJSONArray.getJSONObject(position).getString("insurance_select");
                final String insurance_str = LiveOfferJSONArray.getJSONObject(position).getString("insurance");
                final String gst_str = LiveOfferJSONArray.getJSONObject(position).getString("gst");

                final String q1_title_str = LiveOfferJSONArray.getJSONObject(position).getString("q1_title");
                final String q1_value_str = LiveOfferJSONArray.getJSONObject(position).getString("q1_value");
                final String q2_title_str = LiveOfferJSONArray.getJSONObject(position).getString("q2_title");
                final String q2_value_str = LiveOfferJSONArray.getJSONObject(position).getString("q2_value");
                final String q3_title_str = LiveOfferJSONArray.getJSONObject(position).getString("q3_title");
                final String q3_value_str = LiveOfferJSONArray.getJSONObject(position).getString("q3_value");
                final String q4_title_str = LiveOfferJSONArray.getJSONObject(position).getString("q4_title");
                final String q4_value_str = LiveOfferJSONArray.getJSONObject(position).getString("q4_value");
                final String q5_title_str = LiveOfferJSONArray.getJSONObject(position).getString("q5_title");
                final String q5_value_str = LiveOfferJSONArray.getJSONObject(position).getString("q5_value");
                final String q6_title_str = LiveOfferJSONArray.getJSONObject(position).getString("q6_title");
                final String q6_value_str = LiveOfferJSONArray.getJSONObject(position).getString("q6_value");
                final String q7_title_str = LiveOfferJSONArray.getJSONObject(position).getString("q7_title");
                final String q7_value_str = LiveOfferJSONArray.getJSONObject(position).getString("q7_value");
                final String q8_title_str = LiveOfferJSONArray.getJSONObject(position).getString("q8_title");
                final String q8_value_str = LiveOfferJSONArray.getJSONObject(position).getString("q8_value");

                final String length_str = LiveOfferJSONArray.getJSONObject(position).getString("length");
                final String category_grade_name_str = LiveOfferJSONArray.getJSONObject(position).getString("category_grade_name");
                final String user_code_str = LiveOfferJSONArray.getJSONObject(position).getString("user_code");

                final String delivery_type_str = LiveOfferJSONArray.getJSONObject(position).getString("delivery_type");
                final String delivery_in_days_str = LiveOfferJSONArray.getJSONObject(position).getString("delivery_in_days");
                final String material_inspection_str = LiveOfferJSONArray.getJSONObject(position).getString("material_inspection_type");
                final String weightment_type_str = LiveOfferJSONArray.getJSONObject(position).getString("weightment_type");
                final String test_certificate_type_str = LiveOfferJSONArray.getJSONObject(position).getString("test_certificate_type");

                final String whenstr = LiveOfferJSONArray.getJSONObject(position).getString("when");

                String datestr = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                //  Toast.makeText(ViewLiveTradeActivity1012.this,"CURRENT SYSTEM DATE : ="+ datestr,Toast.LENGTH_SHORT).show();

                String str = whenstr;
                String[] splitStr = str.split("\\s+");

                if(splitStr[0].equalsIgnoreCase(datestr))
                {


                    holder.enqtext.setText("Buy");
                    holder.pricetxtvw.setVisibility(View.VISIBLE);
                    holder.minrateval.setVisibility(View.VISIBLE);

                }
                else
                {
                    holder.enqtext.setText("Enquiry");
                    holder.pricetxtvw.setVisibility(View.INVISIBLE);
                    holder.minrateval.setVisibility(View.INVISIBLE);
                }


                if(transport_str.equalsIgnoreCase("1"))
                {

                    holder.dispatchtxt.setText("Seller");

                }
                if(transport_str.equalsIgnoreCase("2"))
                {
                    holder.dispatchtxt.setVisibility(View.VISIBLE);
                    holder.dispatchtxt.setText("Buyer");
                }
                if(transport_str.equalsIgnoreCase("3"))
                {
                    holder.dispatchtxt.setVisibility(View.VISIBLE);
                    holder.dispatchtxt.setText("Mxmart");
                }


                holder.loadingtxt.setText(loading_str+" \u20B9"+"/MT");
                if(insurance_str.equalsIgnoreCase("0"))
                    {
                        holder.insurancetxt.setText("NA");
                    }
                    if(insurance_str.equalsIgnoreCase("1"))
                    {
                        holder.insurancetxt.setText("Seller");
                    }

                    if(insurance_str.equalsIgnoreCase("2"))
                    {
                      holder.insurancetxt.setText("Buyer");
                    }


                holder.enquiryrelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(isDailyLimitAvailable==false)
                        {


                            util.dialog(getActivity(), "Please Set Buying Limit");
                            SingletonActivity.isdailylimitAvailable = false;
                        }
                        else
                        {

                            SingletonActivity.isdailylimitAvailable = true;

                        }


                        Intent i = new Intent(getActivity(), ViewLiveTradeActivity1012.class);
                        i.putExtra("companyname",companyname);
                        i.putExtra("length",length_str);
                        i.putExtra("categorydesc",categorydesc);
                        i.putExtra("subcategorydesc",subcategorydesc);
                        i.putExtra("advanceratedesc",advanceratedesc);
                        i.putExtra("nextdayratedesc",nextdayratedesc);
                        i.putExtra("regularratedesc",regularratedesc);
                        i.putExtra("remaining_quantity",remaining_quantity);
                        i.putExtra("sellidstr",sellidstr);
                        i.putExtra("categoryidstr",categoryidstr);
                        i.putExtra("subcategoryidstr",subcategoryidstr);
                        i.putExtra("tncstr",tncstr);
                        i.putExtra("q1val",q1val);
                        i.putExtra("q2val",q2val);
                        i.putExtra("q3val",q3val);
                        i.putExtra("q4val",q4val);
                        i.putExtra("q5val",q5val);
                        i.putExtra("q6val",q6val);
                        i.putExtra("q7val",q7val);
                        i.putExtra("q8val",q8val);
                        i.putExtra("location",location);
                        i.putExtra("promo_code",promo_code);
                        i.putExtra("addlimitstr",addlimitstr);
                        i.putExtra("user_code",user_code);
                        i.putExtra("modified_field",modified_field);
                        i.putExtra("categoryidstr",categoryidstr);
                        i.putExtra("size_range_start_str",size_range_start_str);
                        i.putExtra("size_range_end_str",size_range_end_str);
                        i.putExtra("q1_title",q1_title_str);
                        i.putExtra("q1_value",q1_value_str);
                        i.putExtra("q2_title",q2_title_str);
                        i.putExtra("q2_value",q2_value_str);
                        i.putExtra("q3_title",q3_title_str);
                        i.putExtra("q3_value",q3_value_str);
                        i.putExtra("q4_title",q4_title_str);
                        i.putExtra("q4_value",q4_value_str);
                        i.putExtra("q5_title",q5_title_str);
                        i.putExtra("q5_value",q5_value_str);
                        i.putExtra("q6_title",q6_title_str);
                        i.putExtra("q6_value",q6_value_str);
                        i.putExtra("q7_title",q7_title_str);
                        i.putExtra("q7_value",q7_value_str);
                        i.putExtra("q8_title",q8_title_str);
                        i.putExtra("q8_value",q8_value_str);
                        i.putExtra("transport",transport_str);
                        i.putExtra("loading",loading_str);
                        i.putExtra("gst",gst_str);
                        i.putExtra("insurance_select",insurance_select_str);
                        i.putExtra("insurance",insurance_str);
                        i.putExtra("category_grade_name",category_grade_name_str);
                        i.putExtra("user_code_str",user_code_str);

                        i.putExtra("delivery_type",delivery_type_str);
                        i.putExtra("delivery_in_days",delivery_in_days_str);
                        i.putExtra("material_inspection",material_inspection_str);
                        i.putExtra("weighment_type",weightment_type_str);
                        i.putExtra("test_certificate_type",test_certificate_type_str);
                        i.putExtra("when",whenstr);



                        startActivity(i);
                    }
                });



                    rowView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(isDailyLimitAvailable==false)
                        {


                            util.dialog(getActivity(), "Please Set Buying Limit");
                            SingletonActivity.isdailylimitAvailable = false;
                        }
                        else
                        {

                            SingletonActivity.isdailylimitAvailable = true;

                        }


                        Intent i = new Intent(getActivity(), ViewLiveTradeActivity1012.class);
                        i.putExtra("companyname",companyname);
                        i.putExtra("length",length_str);
                        i.putExtra("categorydesc",categorydesc);
                        i.putExtra("subcategorydesc",subcategorydesc);
                        i.putExtra("advanceratedesc",advanceratedesc);
                        i.putExtra("nextdayratedesc",nextdayratedesc);
                        i.putExtra("regularratedesc",regularratedesc);
                        i.putExtra("remaining_quantity",remaining_quantity);
                        i.putExtra("sellidstr",sellidstr);
                        i.putExtra("categoryidstr",categoryidstr);
                        i.putExtra("subcategoryidstr",subcategoryidstr);
                        i.putExtra("tncstr",tncstr);
                        i.putExtra("q1val",q1val);
                        i.putExtra("q2val",q2val);
                        i.putExtra("q3val",q3val);
                        i.putExtra("q4val",q4val);
                        i.putExtra("q5val",q5val);
                        i.putExtra("q6val",q6val);
                        i.putExtra("q7val",q7val);
                        i.putExtra("q8val",q8val);
                        i.putExtra("location",location);
                        i.putExtra("promo_code",promo_code);
                        i.putExtra("addlimitstr",addlimitstr);
                        i.putExtra("user_code",user_code);
                        i.putExtra("modified_field",modified_field);
                        i.putExtra("categoryidstr",categoryidstr);
                        i.putExtra("size_range_start_str",size_range_start_str);
                        i.putExtra("size_range_end_str",size_range_end_str);
                        i.putExtra("q1_title",q1_title_str);
                        i.putExtra("q1_value",q1_value_str);
                        i.putExtra("q2_title",q2_title_str);
                        i.putExtra("q2_value",q2_value_str);
                        i.putExtra("q3_title",q3_title_str);
                        i.putExtra("q3_value",q3_value_str);
                        i.putExtra("q4_title",q4_title_str);
                        i.putExtra("q4_value",q4_value_str);
                        i.putExtra("q5_title",q5_title_str);
                        i.putExtra("q5_value",q5_value_str);
                        i.putExtra("q6_title",q6_title_str);
                        i.putExtra("q6_value",q6_value_str);
                        i.putExtra("q7_title",q7_title_str);
                        i.putExtra("q7_value",q7_value_str);
                        i.putExtra("q8_title",q8_title_str);
                        i.putExtra("q8_value",q8_value_str);
                        i.putExtra("transport",transport_str);
                        i.putExtra("loading",loading_str);
                        i.putExtra("gst",gst_str);
                        i.putExtra("insurance_select",insurance_select_str);
                        i.putExtra("insurance",insurance_str);
                        i.putExtra("category_grade_name",category_grade_name_str);
                        i.putExtra("user_code_str",user_code_str);

                        i.putExtra("delivery_type",delivery_type_str);
                        i.putExtra("delivery_in_days",delivery_in_days_str);
                        i.putExtra("material_inspection",material_inspection_str);
                        i.putExtra("weighment_type",weightment_type_str);
                        i.putExtra("test_certificate_type",test_certificate_type_str);
                        i.putExtra("when",whenstr);



                        startActivity(i);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }






            try {

                System.out.println("REMAINING QUANTITY-----"+ LiveOfferJSONArray.getJSONObject(position).getString("remaining_quantity"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            System.out.println("SubCategoryArrayList in Home Activity-----"+ SingletonActivity.SubCategoryArrayList);








            try {
                final String companynamestr = LiveOfferJSONArray.getJSONObject(position).getString("company_name");
                holder.cmpnydesc.setText(LiveOfferJSONArray.getJSONObject(position).getString("company_name"));



                holder.ratingBar.setRating(Float.parseFloat(LiveOfferJSONArray.getJSONObject(position).getString("rating")));

                final String categorynamestr = LiveOfferJSONArray.getJSONObject(position).getString("category_name");
                final String subcategorynamestr =  LiveOfferJSONArray.getJSONObject(position).getString("sub_category_name");

                final String categoryidstr =  LiveOfferJSONArray.getJSONObject(position).getString("category");

                if(LiveOfferJSONArray.getJSONObject(position).getString("category_name").equalsIgnoreCase("false"))
                {
                    holder.proddesc.setText("");
                }
                else {
                    holder.proddesc.setText(LiveOfferJSONArray.getJSONObject(position).getString("category_name"));
                }

                if(categoryidstr.equalsIgnoreCase("1") || categoryidstr.equalsIgnoreCase("2") )
                {
                    holder.sub_cat_txt.setText(LiveOfferJSONArray.getJSONObject(position).getString("sub_category_name"));
                }
                else {
                    holder.sub_cat_txt.setText(LiveOfferJSONArray.getJSONObject(position).getString("size_range_start")+"-"+LiveOfferJSONArray.getJSONObject(position).getString("size_range_end"));
                }

                if(LiveOfferJSONArray.getJSONObject(position).getString("size_range_start").equalsIgnoreCase("null")&&(LiveOfferJSONArray.getJSONObject(position).getString("size_range_end").equalsIgnoreCase("null")))
                {
                    holder.sub_cat_txt.setVisibility(View.INVISIBLE);
                }
                else
                {
                    holder.sub_cat_txt.setVisibility(View.VISIBLE);
                }

                ArrayList<Float> priceAl =  new ArrayList<Float>();

                priceAl.clear();

                double a = 0;
                double n = 0;
                double r = 0;

                if(!LiveOfferJSONArray.getJSONObject(position).getString("advance_rate").equalsIgnoreCase("null"))
                {
                     a = Double.parseDouble(LiveOfferJSONArray.getJSONObject(position).getString("advance_rate"));

                }

                if(!LiveOfferJSONArray.getJSONObject(position).getString("next_day_rate").equalsIgnoreCase("null"))
                {
                     n = Double.parseDouble(LiveOfferJSONArray.getJSONObject(position).getString("next_day_rate"));

                }

                if(!LiveOfferJSONArray.getJSONObject(position).getString("regular_rate").equalsIgnoreCase("null"))
                {
                     r = Double.parseDouble(LiveOfferJSONArray.getJSONObject(position).getString("regular_rate"));

                }



                HashMap<String,Double> hashMap = new HashMap<String, Double>();

                String ratestr = "";
                if(a>0.00||n>0.00||r>0.00) {

                    if(a!=0.0) {

                        hashMap.put("Adv.",a);


                    }

                    if(n!=0.0) {

                        hashMap.put("Nxt.",n);

                    }

                    if(r!=0.0) {

                        hashMap.put("Reg.",r);

                    }
                }

                System.out.println("HASHMAP ---"+ hashMap.isEmpty());

                if(hashMap.isEmpty()==true)
                {
                    hashMap.put("Adv.",0.00);
                    hashMap.put("Nxt.",0.00);
                    hashMap.put("Reg.",0.00);
                }

                else {

                    Double min = Collections.min(hashMap.values());
                    System.out.println("min value in hashmap---" + min); // 0.1

                    for (Map.Entry<String, Double> entry : hashMap.entrySet()) {
                        if (entry.getValue().equals(min)) {
                            System.out.println("key of min value---" + entry.getKey());
                            holder.minrateval.setText(min + "0" + "(" + entry.getKey() + ")");

                            String modified_field = LiveOfferJSONArray.getJSONObject(position).getString("modified_field");
                            List<String> modified_field_List = Arrays.asList(modified_field.split(","));

                            if (entry.getKey().equalsIgnoreCase("Adv.")) {


                                System.out.println("modified_field_List---" + modified_field_List);

                                if (modified_field_List.contains("-12-")) {
                                    holder.minrateval.setTextColor(Color.RED);

                                }
                            } else if (entry.getKey().equalsIgnoreCase("Reg.")) {
                                if (modified_field_List.contains("-14-")) {
                                    holder.minrateval.setTextColor(Color.RED);
                                }
                            } else if (entry.getKey().equalsIgnoreCase("Nxt.")) {
                                if (modified_field_List.contains("-13-")) {
                                    holder.minrateval.setTextColor(Color.RED);
                                }
                            }

                            if (modified_field_List.contains("-2-")) {
                                holder.sub_cat_txt.setTextColor(Color.RED);
                            }

                        }


                    }


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


            return rowView;

        }


    }


    public class Holder {
        TextView dispatchtxt,loadingtxt,cmpnydesc,proddesc,sub_cat_txt,minrateval,insurancetxt,enqtext,pricetxtvw;
        TextView remqty;
        ImageView dispatch_icon,loading_icon,insurance_icon;
        RatingBar ratingBar;
        RelativeLayout enquiryrelative;



    }

    private void FinishedEnqListAPI(String url) {

       /* pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();
*/
        progressBarFinished.setVisibility(View.VISIBLE);
        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {

                        // pdia.dismiss();

                        handler.postDelayed(new Runnable() {
                            public void run() {

                                progressBarFinished.setVisibility(View.GONE);
                                System.out.println("RESPONSE OF FINISHED ENQUIRY LIST API IS---" + response);


                                JSONObject FinishedEnqListJson = null;
                                try {
                                    FinishedEnqListJson = new JSONObject(response);


                                    String statusstr = FinishedEnqListJson.getString("status");

                                    if (statusstr.equalsIgnoreCase("true")) {

                                        noenquirytxt.setVisibility(View.GONE);
                                        enquirylistvw.setVisibility(View.VISIBLE);



                                     //   JSONArray FinishedEnqJSONArray = FinishedEnqListJson.getJSONArray("material_lists");
                                        JSONArray newJSONArray = new JSONArray();

                                        for(int i = 0; i < FinishedEnqListJson.getJSONArray("material_lists").length();i++)
                                        {
                                            if(FinishedEnqListJson.getJSONArray("material_lists").getJSONObject(i).getString("is_delete").equalsIgnoreCase("1"))

                                            {
                                                newJSONArray.put(FinishedEnqListJson.getJSONArray("material_lists").get(i));

                                                if (getActivity() != null) {

                                                    SingletonActivity.FinishedEnqJSONArray =  newJSONArray;


                                                    customfinishedenqadap = new CustomFinishedEnqAdap(getActivity(), SingletonActivity.FinishedEnqJSONArray);
                                                    enquirylistvw.setAdapter(customfinishedenqadap);
                                                }
                                            }
                                        }







                              /*  livetradelistvw.invalidate();
                                customadap.notifyDataSetChanged();*/


                                    } else {

                                        noenquirytxt.setVisibility(View.VISIBLE);
                                        enquirylistvw.setVisibility(View.GONE);

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    // pdia.dismiss();
                                }
                            }
                        }, 3000); // 3000 milliseconds delay






                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        //pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        noenquirytxt.setVisibility(View.VISIBLE);
                                        enquirylistvw.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            util.dialog(getActivity(), "Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("finished enquiry list params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        System.out.println("SingletonActivity.clickedcount no.=====" + SingletonActivity.clickedcount);

        if (SingletonActivity.clickedcount == 0) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), hurlStack);
            requestQueue.add(stringRequest);
        } else {
            SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }

    }

    private class CustomFinishedEnqAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        LayoutInflater inflater = null;
        private JSONArray FinishedEnqJSONArray;



        public CustomFinishedEnqAdap(Context mainActivity,JSONArray finishedEnqJSONArray)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.FinishedEnqJSONArray = finishedEnqJSONArray;
            inflater = (LayoutInflater)c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return FinishedEnqJSONArray.length();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub



            final CustomFinishedEnqHolder customFinishedEnqHolder = new CustomFinishedEnqHolder();

            final View rowView;


            rowView = inflater.inflate(R.layout.finished_enquiry_row, null);

            customFinishedEnqHolder.steelimgicon = (ImageView) rowView.findViewById(R.id.steelimgicon);
            customFinishedEnqHolder.platestxt = (TextView) rowView.findViewById(R.id.platestxt);
            customFinishedEnqHolder.cmpnytxt = (TextView)rowView.findViewById(R.id.cmpnytxt);
            customFinishedEnqHolder.stdtxt = (TextView) rowView.findViewById(R.id.stdtxt);
            customFinishedEnqHolder.sizetxt = (TextView)rowView.findViewById(R.id.sizetxt);
            customFinishedEnqHolder.enquiryrelative = (RelativeLayout)rowView.findViewById(R.id.enquiryrelative);



            try {


                customFinishedEnqHolder.platestxt.setText(FinishedEnqJSONArray.getJSONObject(position).getString("material_name"));
                customFinishedEnqHolder.cmpnytxt.setText(FinishedEnqJSONArray.getJSONObject(position).getString("material_brand"));
                customFinishedEnqHolder.stdtxt.setText(FinishedEnqJSONArray.getJSONObject(position).getString("material_standard"));

                final String material_id = FinishedEnqJSONArray.getJSONObject(position).getString("material_id");
                final String material_name = FinishedEnqJSONArray.getJSONObject(position).getString("material_name");
                final String material_thickness = FinishedEnqJSONArray.getJSONObject(position).getString("material_thickness");
                final String material_brand = FinishedEnqJSONArray.getJSONObject(position).getString("material_brand");
                final String material_standard = FinishedEnqJSONArray.getJSONObject(position).getString("material_standard");
                final String material_grade = FinishedEnqJSONArray.getJSONObject(position).getString("material_grade");
                final String material_image = FinishedEnqJSONArray.getJSONObject(position).getString("material_image");

                final String material_specification = FinishedEnqJSONArray.getJSONObject(position).getString("material_specification");
                final String material_location = FinishedEnqJSONArray.getJSONObject(position).getString("material_location");
                final String material_size_excel = FinishedEnqJSONArray.getJSONObject(position).getString("material_size_excel");


                String imageURL = APIName.IMAGE_URL + "/" + material_image;
                System.out.println("imageURL IS---" + imageURL);

                PicassoTrustAll.getInstance(getActivity())
                        .load(imageURL)
                        .into(customFinishedEnqHolder.steelimgicon);

                if(FinishedEnqJSONArray.getJSONObject(position).getString("material_size").equalsIgnoreCase("null"))
                {

                   /* customFinishedEnqHolder.enquiryrelative.setEnabled(false);
                    customFinishedEnqHolder.enquiryrelative.setClickable(false);
                    customFinishedEnqHolder.enquiryrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
*/
                    customFinishedEnqHolder.enquiryrelative.setEnabled(true);
                    customFinishedEnqHolder.enquiryrelative.setClickable(true);
                    customFinishedEnqHolder.enquiryrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                    //Toast.makeText(getActivity(),"DISABLE position: "+ position,Toast.LENGTH_LONG).show();

                }
                else
                {
                    customFinishedEnqHolder.enquiryrelative.setEnabled(true);
                    customFinishedEnqHolder.enquiryrelative.setClickable(true);
                    customFinishedEnqHolder.enquiryrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);

                    //  Toast.makeText(getActivity(),"CLICKABLE position: "+ position,Toast.LENGTH_LONG).show();
                }





                customFinishedEnqHolder.enquiryrelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getActivity(), EnquiryActivity.class);
                        i.putExtra("materialId",material_id);
                        i.putExtra("materialname",material_name);
                        i.putExtra("materialthickness",material_thickness);
                        i.putExtra("materialbrand",material_brand);
                        i.putExtra("materialstandard",material_standard);
                        i.putExtra("materialgrade",material_grade);
                        i.putExtra("materialimage",material_image);

                        i.putExtra("materialspec",material_specification);
                        i.putExtra("materialloc",material_location);
                        i.putExtra("material_size_excel",material_size_excel);

                        try {
                            i.putExtra("materialsize", FinishedEnqJSONArray.getJSONObject(position).getString("material_size"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        SingletonActivity.enquiryfromhomepage = false;
                        SingletonActivity.enquiryfromplatespage = true;

                        startActivity(i);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }



            return rowView;

        }


    }


    public class CustomFinishedEnqHolder {
        TextView platestxt,cmpnytxt,stdtxt,sizetxt;
        ImageView steelimgicon;
        RelativeLayout enquiryrelative;


    }

    private void GetLiveOfferPigIronAPI(String url) {
       /* pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();
*/

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        // pdia.dismiss();


                        System.out.println("RESPONSE OF GET LIVE OFFER PIG IRON API IS---" + response);


                        JSONObject GetLiveOfferPigIronJson = null;
                        try {
                            GetLiveOfferPigIronJson = new JSONObject(response);


                            String statusstr = GetLiveOfferPigIronJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                nopigirontxt.setVisibility(View.GONE);
                                pigironlistvw.setVisibility(View.VISIBLE);

                                JSONArray liveOfferPigIronJsonArray = GetLiveOfferPigIronJson.getJSONArray("liveOffer");
                                System.out.println("GET LIVE OFFER PIG IRON JSONARRAY IS---" + liveOfferPigIronJsonArray);


                                JSONArray pigOfferPrevJsonArray = GetLiveOfferPigIronJson.getJSONArray("pig_offer_previous");
                                System.out.println("PIG OFFER PREVIOUS JSONARRAY IS---" + pigOfferPrevJsonArray);

                                for (int x = 0; x < pigOfferPrevJsonArray.length(); x++) {
                                    liveOfferPigIronJsonArray.put(pigOfferPrevJsonArray.getJSONObject(x));
                                }




                                SingletonActivity.liveOfferPigIronJsonArray = liveOfferPigIronJsonArray;



                                if (getActivity() != null) {
                                    pigIronCustomAdap = new PigIronCustomAdap(getActivity(), SingletonActivity.liveOfferPigIronJsonArray);//,leavetypelist,messagetypelist,fromdatelist,todatelist,postdatelist,statuslist);
                                    pigironlistvw.setAdapter(pigIronCustomAdap);
                                }

                            }
                            else {

                                nopigirontxt.setVisibility(View.VISIBLE);
                                pigironlistvw.setVisibility(View.GONE);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            // pdia.dismiss();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        //pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        noliveoffertxt.setVisibility(View.VISIBLE);
                                        livetradelistvw.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get live offer params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        System.out.println("SingletonActivity.clickedcount no.====="+ SingletonActivity.clickedcount);

        if(SingletonActivity.clickedcount == 0) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
            requestQueue.add(stringRequest);
        }
        else
        {
            SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }

    }

    private class PigIronCustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        LayoutInflater inflater = null;
        private JSONArray LiveOfferPigJSONArray;



        public PigIronCustomAdap(Context mainActivity,JSONArray liveOfferpigJSONArray)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.LiveOfferPigJSONArray = liveOfferpigJSONArray;
            inflater = (LayoutInflater)c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return LiveOfferPigJSONArray.length();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub



            final PigIronHolder holder = new PigIronHolder();

            final View rowView;


            rowView = inflater.inflate(R.layout.live_todays_offer_pigiron_row, null);

            holder.cmpnydesc = (TextView) rowView.findViewById(R.id.cmpnynametxtvw);
            holder.proddesc = (TextView) rowView.findViewById(R.id.proddesctxtvw);
            holder.ratingBar = (RatingBar)rowView.findViewById(R.id.ratingBar);
            holder.sub_cat_txt = (TextView) rowView.findViewById(R.id.subproddesctxtvw);
            holder.remqty = (TextView)rowView.findViewById(R.id.qtydesctxtvw);
            holder.minrateval = (TextView)rowView.findViewById(R.id.ratetxtvw);
            holder.dispatch_icon = (ImageView)rowView.findViewById(R.id.dispatch_icon);
            holder.dispatchtxt = (TextView)rowView.findViewById(R.id.dispatchtxt);
            holder.loading_icon = (ImageView)rowView.findViewById(R.id.loading_icon);
            holder.loadingtxt = (TextView)rowView.findViewById(R.id.loadingtxt);
            holder.insurance_icon = (ImageView)rowView.findViewById(R.id.insurance_icon);
            holder.insurancetxt= (TextView)rowView.findViewById(R.id.insurancetxt);
            holder.enqtext = (TextView)rowView.findViewById(R.id.enqtext);
            holder.enquiryrelative = (RelativeLayout)rowView.findViewById(R.id.enquiryrelative);
            holder.pricetxtvw = (TextView) rowView.findViewById(R.id.pricetxtvw);

            System.out.println("LiveOfferJSONArray-----"+ LiveOfferPigJSONArray);


            try {
                final String companyname = LiveOfferPigJSONArray.getJSONObject(position).getString("company_name");
                final  String categorydesc = LiveOfferPigJSONArray.getJSONObject(position).getString("category_name");
                final String subcategorydesc = LiveOfferPigJSONArray.getJSONObject(position).getString("sub_category_name");
                final String size_range_start_str = LiveOfferPigJSONArray.getJSONObject(position).getString("size_range_start");
                final String size_range_end_str = LiveOfferPigJSONArray.getJSONObject(position).getString("size_range_end");

                final String advanceratedesc = LiveOfferPigJSONArray.getJSONObject(position).getString("advance_rate");
                final String nextdayratedesc = LiveOfferPigJSONArray.getJSONObject(position).getString("next_day_rate");
                final  String regularratedesc = LiveOfferPigJSONArray.getJSONObject(position).getString("regular_rate");

                final  String remaining_quantity = LiveOfferPigJSONArray.getJSONObject(position).getString("remaining_quantity");

                final String sellidstr = LiveOfferPigJSONArray.getJSONObject(position).getString("sell_id");

                final String categoryidstr = LiveOfferPigJSONArray.getJSONObject(position).getString("category");
                final  String subcategoryidstr = LiveOfferPigJSONArray.getJSONObject(position).getString("sub_category");
                final String tncstr = LiveOfferPigJSONArray.getJSONObject(position).getString("sell_tandc");

                final String q1val = LiveOfferPigJSONArray.getJSONObject(position).getString("q1");
                final String q2val = LiveOfferPigJSONArray.getJSONObject(position).getString("q2");
                final String q3val = LiveOfferPigJSONArray.getJSONObject(position).getString("q3");
                final String q4val = LiveOfferPigJSONArray.getJSONObject(position).getString("q4");
                final String q5val = LiveOfferPigJSONArray.getJSONObject(position).getString("q5");
                final String q6val = LiveOfferPigJSONArray.getJSONObject(position).getString("q6");
                final String q7val = LiveOfferPigJSONArray.getJSONObject(position).getString("q7");
                final String q8val = LiveOfferPigJSONArray.getJSONObject(position).getString("q8");
                final String location = LiveOfferPigJSONArray.getJSONObject(position).getString("location");
                final String promo_code = LiveOfferPigJSONArray.getJSONObject(position).getString("promo_code");
                final String user_code = LiveOfferPigJSONArray.getJSONObject(position).getString("user_code");
                final String modified_field = LiveOfferPigJSONArray.getJSONObject(position).getString("modified_field");

                final String transport_str = LiveOfferPigJSONArray.getJSONObject(position).getString("transport");
                final String loading_str = LiveOfferPigJSONArray.getJSONObject(position).getString("loading");
                final String insurance_select_str = LiveOfferPigJSONArray.getJSONObject(position).getString("insurance_select");
                final String insurance_str = LiveOfferPigJSONArray.getJSONObject(position).getString("insurance");
                final String gst_str = LiveOfferPigJSONArray.getJSONObject(position).getString("gst");

                final String q1_title_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q1_title");
                final String q1_value_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q1_value");
                final String q2_title_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q2_title");
                final String q2_value_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q2_value");
                final String q3_title_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q3_title");
                final String q3_value_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q3_value");
                final String q4_title_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q4_title");
                final String q4_value_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q4_value");
                final String q5_title_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q5_title");
                final String q5_value_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q5_value");
                final String q6_title_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q6_title");
                final String q6_value_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q6_value");
                final String q7_title_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q7_title");
                final String q7_value_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q7_value");
                final String q8_title_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q8_title");
                final String q8_value_str = LiveOfferPigJSONArray.getJSONObject(position).getString("q8_value");

                final String length_str = LiveOfferPigJSONArray.getJSONObject(position).getString("length");
                final String category_grade_name_str = LiveOfferPigJSONArray.getJSONObject(position).getString("category_grade_name");
                final String user_code_str = LiveOfferPigJSONArray.getJSONObject(position).getString("user_code");

                final String delivery_type_str = LiveOfferPigJSONArray.getJSONObject(position).getString("delivery_type");
                final String delivery_in_days_str = LiveOfferPigJSONArray.getJSONObject(position).getString("delivery_in_days");
                final String material_inspection_str = LiveOfferPigJSONArray.getJSONObject(position).getString("material_inspection_type");
                final String weightment_type_str = LiveOfferPigJSONArray.getJSONObject(position).getString("weightment_type");
                final String test_certificate_type_str = LiveOfferPigJSONArray.getJSONObject(position).getString("test_certificate_type");

                final String whenstr = LiveOfferPigJSONArray.getJSONObject(position).getString("when");
                final String category_make_str = LiveOfferPigJSONArray.getJSONObject(position).getString("category_make");

                String datestr = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                //  Toast.makeText(ViewLiveTradeActivity1012.this,"CURRENT SYSTEM DATE : ="+ datestr,Toast.LENGTH_SHORT).show();

                String str = whenstr;
                String[] splitStr = str.split("\\s+");

                if(splitStr[0].equalsIgnoreCase(datestr))
                {


                    holder.enqtext.setText("Buy");

                  //  holder.minrateval.setVisibility(View.VISIBLE);



                }
                else
                {
                    holder.enqtext.setText("Enquiry");

                 //   holder.minrateval.setVisibility(View.INVISIBLE);

                }


                if(transport_str.equalsIgnoreCase("1"))
                {

                    holder.dispatchtxt.setText("Seller");

                }
                if(transport_str.equalsIgnoreCase("2"))
                {
                    holder.dispatchtxt.setVisibility(View.VISIBLE);
                    holder.dispatchtxt.setText("Buyer");
                }
                if(transport_str.equalsIgnoreCase("3"))
                {
                    holder.dispatchtxt.setVisibility(View.VISIBLE);
                    holder.dispatchtxt.setText("Mxmart");
                }


                holder.loadingtxt.setText(loading_str+" \u20B9"+"/MT");
                if(insurance_str.equalsIgnoreCase("0"))
                {
                    holder.insurancetxt.setText("NA");
                }
                if(insurance_str.equalsIgnoreCase("1"))
                {
                    holder.insurancetxt.setText("Seller");
                }

                if(insurance_str.equalsIgnoreCase("2"))
                {
                    holder.insurancetxt.setText("Buyer");
                }



                holder.enquiryrelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(isDailyLimitAvailable==false)
                        {


                            util.dialog(getActivity(), "Please Set Buying Limit");
                            SingletonActivity.isdailylimitAvailable = false;
                        }
                        else
                        {

                            SingletonActivity.isdailylimitAvailable = true;

                        }


                        Intent i = new Intent(getActivity(), ViewPigIronActivity.class);
                        i.putExtra("companyname",companyname);
                        i.putExtra("length",length_str);
                        i.putExtra("categorydesc",categorydesc);
                        i.putExtra("subcategorydesc",subcategorydesc);
                        i.putExtra("advanceratedesc",advanceratedesc);
                        i.putExtra("nextdayratedesc",nextdayratedesc);
                        i.putExtra("regularratedesc",regularratedesc);
                        i.putExtra("remaining_quantity",remaining_quantity);
                        i.putExtra("sellidstr",sellidstr);
                        i.putExtra("categoryidstr",categoryidstr);
                        i.putExtra("subcategoryidstr",subcategoryidstr);
                        i.putExtra("tncstr",tncstr);
                        i.putExtra("q1val",q1val);
                        i.putExtra("q2val",q2val);
                        i.putExtra("q3val",q3val);
                        i.putExtra("q4val",q4val);
                        i.putExtra("q5val",q5val);
                        i.putExtra("q6val",q6val);
                        i.putExtra("q7val",q7val);
                        i.putExtra("q8val",q8val);
                        i.putExtra("location",location);
                        i.putExtra("promo_code",promo_code);
                        i.putExtra("addlimitstr",addlimitstr);
                        i.putExtra("user_code",user_code);
                        i.putExtra("modified_field",modified_field);
                        i.putExtra("categoryidstr",categoryidstr);
                        i.putExtra("size_range_start_str",size_range_start_str);
                        i.putExtra("size_range_end_str",size_range_end_str);
                        i.putExtra("q1_title",q1_title_str);
                        i.putExtra("q1_value",q1_value_str);
                        i.putExtra("q2_title",q2_title_str);
                        i.putExtra("q2_value",q2_value_str);
                        i.putExtra("q3_title",q3_title_str);
                        i.putExtra("q3_value",q3_value_str);
                        i.putExtra("q4_title",q4_title_str);
                        i.putExtra("q4_value",q4_value_str);
                        i.putExtra("q5_title",q5_title_str);
                        i.putExtra("q5_value",q5_value_str);
                        i.putExtra("q6_title",q6_title_str);
                        i.putExtra("q6_value",q6_value_str);
                        i.putExtra("q7_title",q7_title_str);
                        i.putExtra("q7_value",q7_value_str);
                        i.putExtra("q8_title",q8_title_str);
                        i.putExtra("q8_value",q8_value_str);
                        i.putExtra("transport",transport_str);
                        i.putExtra("loading",loading_str);
                        i.putExtra("gst",gst_str);
                        i.putExtra("insurance_select",insurance_select_str);
                        i.putExtra("insurance",insurance_str);
                        i.putExtra("category_grade_name",category_grade_name_str);
                        i.putExtra("user_code_str",user_code_str);

                        i.putExtra("delivery_type",delivery_type_str);
                        i.putExtra("delivery_in_days",delivery_in_days_str);
                        i.putExtra("material_inspection",material_inspection_str);
                        i.putExtra("weighment_type",weightment_type_str);
                        i.putExtra("test_certificate_type",test_certificate_type_str);
                        i.putExtra("when",whenstr);
                        i.putExtra("category_make_str",category_make_str);



                        startActivity(i);
                    }
                });



                rowView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(isDailyLimitAvailable==false)
                        {


                            util.dialog(getActivity(), "Please Set Buying Limit");
                            SingletonActivity.isdailylimitAvailable = false;
                        }
                        else
                        {

                            SingletonActivity.isdailylimitAvailable = true;

                        }


                        Intent i = new Intent(getActivity(), ViewPigIronActivity.class);
                        i.putExtra("companyname",companyname);
                        i.putExtra("length",length_str);
                        i.putExtra("categorydesc",categorydesc);
                        i.putExtra("subcategorydesc",subcategorydesc);
                        i.putExtra("advanceratedesc",advanceratedesc);
                        i.putExtra("nextdayratedesc",nextdayratedesc);
                        i.putExtra("regularratedesc",regularratedesc);
                        i.putExtra("remaining_quantity",remaining_quantity);
                        i.putExtra("sellidstr",sellidstr);
                        i.putExtra("categoryidstr",categoryidstr);
                        i.putExtra("subcategoryidstr",subcategoryidstr);
                        i.putExtra("tncstr",tncstr);
                        i.putExtra("q1val",q1val);
                        i.putExtra("q2val",q2val);
                        i.putExtra("q3val",q3val);
                        i.putExtra("q4val",q4val);
                        i.putExtra("q5val",q5val);
                        i.putExtra("q6val",q6val);
                        i.putExtra("q7val",q7val);
                        i.putExtra("q8val",q8val);
                        i.putExtra("location",location);
                        i.putExtra("promo_code",promo_code);
                        i.putExtra("addlimitstr",addlimitstr);
                        i.putExtra("user_code",user_code);
                        i.putExtra("modified_field",modified_field);
                        i.putExtra("categoryidstr",categoryidstr);
                        i.putExtra("size_range_start_str",size_range_start_str);
                        i.putExtra("size_range_end_str",size_range_end_str);
                        i.putExtra("q1_title",q1_title_str);
                        i.putExtra("q1_value",q1_value_str);
                        i.putExtra("q2_title",q2_title_str);
                        i.putExtra("q2_value",q2_value_str);
                        i.putExtra("q3_title",q3_title_str);
                        i.putExtra("q3_value",q3_value_str);
                        i.putExtra("q4_title",q4_title_str);
                        i.putExtra("q4_value",q4_value_str);
                        i.putExtra("q5_title",q5_title_str);
                        i.putExtra("q5_value",q5_value_str);
                        i.putExtra("q6_title",q6_title_str);
                        i.putExtra("q6_value",q6_value_str);
                        i.putExtra("q7_title",q7_title_str);
                        i.putExtra("q7_value",q7_value_str);
                        i.putExtra("q8_title",q8_title_str);
                        i.putExtra("q8_value",q8_value_str);
                        i.putExtra("transport",transport_str);
                        i.putExtra("loading",loading_str);
                        i.putExtra("gst",gst_str);
                        i.putExtra("insurance_select",insurance_select_str);
                        i.putExtra("insurance",insurance_str);
                        i.putExtra("category_grade_name",category_grade_name_str);
                        i.putExtra("user_code_str",user_code_str);

                        i.putExtra("delivery_type",delivery_type_str);
                        i.putExtra("delivery_in_days",delivery_in_days_str);
                        i.putExtra("material_inspection",material_inspection_str);
                        i.putExtra("weighment_type",weightment_type_str);
                        i.putExtra("test_certificate_type",test_certificate_type_str);
                        i.putExtra("when",whenstr);
                        i.putExtra("category_make_str",category_make_str);



                        startActivity(i);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }






            try {

                System.out.println("REMAINING QUANTITY-----"+ LiveOfferPigJSONArray.getJSONObject(position).getString("remaining_quantity"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            System.out.println("SubCategoryArrayList in Home Activity-----"+ SingletonActivity.SubCategoryArrayList);








            try {
                final String companynamestr = LiveOfferPigJSONArray.getJSONObject(position).getString("company_name");
              //  holder.cmpnydesc.setText(LiveOfferJSONArray.getJSONObject(position).getString("company_name"));



                holder.ratingBar.setRating(Float.parseFloat(LiveOfferPigJSONArray.getJSONObject(position).getString("rating")));

                final String categorynamestr = LiveOfferPigJSONArray.getJSONObject(position).getString("category_name");
                final String subcategorynamestr =  LiveOfferPigJSONArray.getJSONObject(position).getString("sub_category_name");

                final String categoryidstr =  LiveOfferPigJSONArray.getJSONObject(position).getString("category");

                if(LiveOfferPigJSONArray.getJSONObject(position).getString("category_name").equalsIgnoreCase("false"))
                {
                    holder.proddesc.setText("");
                }
                else {
                    holder.proddesc.setText(LiveOfferPigJSONArray.getJSONObject(position).getString("category_name"));
                }

                holder.sub_cat_txt.setText(LiveOfferPigJSONArray.getJSONObject(position).getString("category_grade_name"));

                if(LiveOfferPigJSONArray.getJSONObject(position).getString("category_grade_name").equalsIgnoreCase("false"))
                {
                    holder.sub_cat_txt.setVisibility(View.INVISIBLE);
                }
                else
                {
                    holder.sub_cat_txt.setVisibility(View.VISIBLE);
                }

              /*  if(categoryidstr.equalsIgnoreCase("1") || categoryidstr.equalsIgnoreCase("2") )
                {
                    holder.sub_cat_txt.setText(LiveOfferPigJSONArray.getJSONObject(position).getString("sub_category_name"));
                }
                else {
                    holder.sub_cat_txt.setText(LiveOfferPigJSONArray.getJSONObject(position).getString("size_range_start")+"-"+LiveOfferPigJSONArray.getJSONObject(position).getString("size_range_end"));
                }*/

                ArrayList<Float> priceAl =  new ArrayList<Float>();

                priceAl.clear();

                double a = 0;
                double n = 0;
                double r = 0;

                if(!LiveOfferPigJSONArray.getJSONObject(position).getString("advance_rate").equalsIgnoreCase("null"))
                {
                    a = Double.parseDouble(LiveOfferPigJSONArray.getJSONObject(position).getString("advance_rate"));

                }

                if(!LiveOfferPigJSONArray.getJSONObject(position).getString("next_day_rate").equalsIgnoreCase("null"))
                {
                    n = Double.parseDouble(LiveOfferPigJSONArray.getJSONObject(position).getString("next_day_rate"));

                }

                if(!LiveOfferPigJSONArray.getJSONObject(position).getString("regular_rate").equalsIgnoreCase("null"))
                {
                    r = Double.parseDouble(LiveOfferPigJSONArray.getJSONObject(position).getString("regular_rate"));

                }



                HashMap<String,Double> hashMap = new HashMap<String, Double>();

                String ratestr = "";
                if(a>0.00||n>0.00||r>0.00) {

                    if(a!=0.0) {

                        hashMap.put("Adv.",a);


                    }

                    if(n!=0.0) {

                        hashMap.put("Nxt.",n);

                    }

                    if(r!=0.0) {

                        hashMap.put("Reg.",r);

                    }
                }

                System.out.println("HASHMAP ---"+ hashMap.isEmpty());

                if(hashMap.isEmpty()==true)
                {
                    hashMap.put("Adv.",0.00);
                    hashMap.put("Nxt.",0.00);
                    hashMap.put("Reg.",0.00);
                }

                else {

                    Double min = Collections.min(hashMap.values());
                    System.out.println("min value in hashmap---" + min); // 0.1

                    for (Map.Entry<String, Double> entry : hashMap.entrySet()) {
                        if (entry.getValue().equals(min)) {
                            System.out.println("key of min value---" + entry.getKey());
                           // holder.minrateval.setText(min + "0" + "(" + entry.getKey() + ")");

                            String modified_field = LiveOfferPigJSONArray.getJSONObject(position).getString("modified_field");
                            List<String> modified_field_List = Arrays.asList(modified_field.split(","));

                            if (entry.getKey().equalsIgnoreCase("Adv.")) {


                                System.out.println("modified_field_List---" + modified_field_List);

                                if (modified_field_List.contains("-12-")) {
                                //    holder.minrateval.setTextColor(Color.RED);

                                }
                            } else if (entry.getKey().equalsIgnoreCase("Reg.")) {
                                if (modified_field_List.contains("-14-")) {
                                //    holder.minrateval.setTextColor(Color.RED);
                                }
                            } else if (entry.getKey().equalsIgnoreCase("Nxt.")) {
                                if (modified_field_List.contains("-13-")) {
                               //     holder.minrateval.setTextColor(Color.RED);
                                }
                            }

                            if (modified_field_List.contains("-2-")) {
                              //  holder.sub_cat_txt.setTextColor(Color.RED);
                            }

                        }


                    }


                }

                if(LiveOfferPigJSONArray.getJSONObject(position).getString("category_make").equalsIgnoreCase("null"))
                {
                    holder.minrateval.setText("--");
                }
                else
                {
                    holder.minrateval.setText(LiveOfferPigJSONArray.getJSONObject(position).getString("category_make"));
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


            return rowView;

        }


    }


    public class PigIronHolder {
        TextView dispatchtxt,loadingtxt,cmpnydesc,proddesc,sub_cat_txt,minrateval,insurancetxt,enqtext,pricetxtvw;
        TextView remqty;
        ImageView dispatch_icon,loading_icon,insurance_icon,buyenquiry_icon;
        RatingBar ratingBar;
        RelativeLayout enquiryrelative;




    }


}
