package com.smtc.mxmart.Fragment;





import android.app.ProgressDialog;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.smtc.mxmart.BuyTabsAdapter;
import com.smtc.mxmart.R;
import com.smtc.mxmart.SampleActivity;
import com.smtc.mxmart.SegmentedGroup;
import com.smtc.mxmart.SellTabsAdapter;
import com.smtc.mxmart.SingletonActivity;

import static android.content.Context.MODE_PRIVATE;


public class TransactionFragment extends Fragment {

    static  RadioButton sellbtn,buybtn;
    static SegmentedGroup segmented3;
    ProgressDialog pdia;
    private boolean _hasLoadedOnce= false; // your boolean field


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.fragment_transaction, container, false);





          getFragmentManager().beginTransaction()
                    .add(R.id.rootLayout, new PlaceholderFragment())
                    .commit();


        return  view;

    }




    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {


        public static final String MY_PREFS_NAME = "MyPrefsFile";
        TabLayout selltabLayout,buytabLayout;
        ViewPager sellviewPager,buyviewPager;
        String userCodestr,mobilenumstr;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                  Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_sample, container, false);




            segmented3 = (SegmentedGroup) rootView.findViewById(R.id.segmented3);

//            Tint color, and text color when checked
            segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));

            sellbtn = (RadioButton)rootView.findViewById(R.id.sell_btn);
            buybtn = (RadioButton)rootView.findViewById(R.id.buy_btn);

           final View sellview = rootView.findViewById(R.id.frag_sell);
            final View buyview = rootView.findViewById(R.id.frag_buy);


            SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            userCodestr = prefs.getString("user_code", null);
            mobilenumstr = prefs.getString("mobile_num", null);



            buybtn.setTextColor(Color.parseColor("#FF2F2F2F"));

            if(SingletonActivity.isNewEnquiryClicked==true)
            {
                sellbtn.setChecked(true);
                buybtn.setChecked(false);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                buybtn.setTextColor(Color.parseColor("#FF2F2F2F"));


                sellview.setVisibility(View.VISIBLE);
                buyview.setVisibility(View.GONE);

            }

            if(SingletonActivity.isSellerAcceptedClicked==true)
            {
                sellbtn.setChecked(false);
                buybtn.setChecked(true);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                sellbtn.setTextColor(Color.parseColor("#FF2F2F2F"));

                sellview.setVisibility(View.GONE);
                buyview.setVisibility(View.VISIBLE);

            }

            if(SingletonActivity.isSellerRejectedClicked==true)
            {
                sellbtn.setChecked(false);
                buybtn.setChecked(true);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                sellbtn.setTextColor(Color.parseColor("#FF2F2F2F"));

                sellview.setVisibility(View.GONE);
                buyview.setVisibility(View.VISIBLE);

            }

            //=========== if trade == buyer == sp clicked==========

            if(SingletonActivity.isSpTradeBuyerClicked == true) {

                sellbtn.setChecked(false);
                buybtn.setChecked(true);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                sellbtn.setTextColor(Color.parseColor("#FF2F2F2F"));

                sellview.setVisibility(View.GONE);
                buyview.setVisibility(View.VISIBLE);
            }

            //=========== if trade == seller == sp clicked==========

            if(SingletonActivity.isSpTradeSellerClicked == true)
            {
                sellbtn.setChecked(true);
                buybtn.setChecked(false);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                buybtn.setTextColor(Color.parseColor("#FF2F2F2F"));


                sellview.setVisibility(View.VISIBLE);
                buyview.setVisibility(View.GONE);
            }

            if(SingletonActivity.isIsBuyerRejectedClicked==true)
            {
                sellbtn.setChecked(true);
                buybtn.setChecked(false);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                buybtn.setTextColor(Color.parseColor("#FF2F2F2F"));


                sellview.setVisibility(View.VISIBLE);
                buyview.setVisibility(View.GONE);

            }

            if(SingletonActivity.frombuycurrentenquiry==true)
            {
                sellbtn.setChecked(false);
                buybtn.setChecked(true);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                sellbtn.setTextColor(Color.parseColor("#FF2F2F2F"));

                sellview.setVisibility(View.GONE);
                buyview.setVisibility(View.VISIBLE);

            }


            if(SingletonActivity.frombackofsellerdispatch==true)
            {
                sellbtn.setChecked(true);
                buybtn.setChecked(false);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                buybtn.setTextColor(Color.parseColor("#FF2F2F2F"));


                sellview.setVisibility(View.VISIBLE);
                buyview.setVisibility(View.GONE);

            }


            if(SingletonActivity.frombackofsellerpayment == true)
            {
                sellbtn.setChecked(true);
                buybtn.setChecked(false);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                buybtn.setTextColor(Color.parseColor("#FF2F2F2F"));


                sellview.setVisibility(View.VISIBLE);
                buyview.setVisibility(View.GONE);
            }

            if(SingletonActivity.fromsellerhistorywebview == true)
            {
                sellbtn.setChecked(true);
                buybtn.setChecked(false);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                buybtn.setTextColor(Color.parseColor("#FF2F2F2F"));


                sellview.setVisibility(View.VISIBLE);
                buyview.setVisibility(View.GONE);
            }


            if(SingletonActivity.fromupdatenewoffer == true)
            {
                sellbtn.setChecked(true);
                buybtn.setChecked(false);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                buybtn.setTextColor(Color.parseColor("#FF2F2F2F"));


                sellview.setVisibility(View.VISIBLE);
                buyview.setVisibility(View.GONE);
            }



            //==================================================================

            if(SingletonActivity.frombuyerhistorywebview == true)
            {
                sellbtn.setChecked(false);
                buybtn.setChecked(true);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                sellbtn.setTextColor(Color.parseColor("#FF2F2F2F"));

                sellview.setVisibility(View.GONE);
                buyview.setVisibility(View.VISIBLE);
            }



            if(SingletonActivity.frombackofbuyerdispatch==true)
            {
                sellbtn.setChecked(false);
                buybtn.setChecked(true);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                sellbtn.setTextColor(Color.parseColor("#FF2F2F2F"));

                sellview.setVisibility(View.GONE);
                buyview.setVisibility(View.VISIBLE);
            }


            if(SingletonActivity.frombackofbuyerpayment == true)
            {
                sellbtn.setChecked(false);
                buybtn.setChecked(true);
                segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                sellbtn.setTextColor(Color.parseColor("#FF2F2F2F"));

                sellview.setVisibility(View.GONE);
                buyview.setVisibility(View.VISIBLE);
            }



            sellbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sellbtn.setChecked(true);
                    buybtn.setChecked(false);
                    segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                    buybtn.setTextColor(Color.parseColor("#FF2F2F2F"));


                    sellview.setVisibility(View.VISIBLE);
                    buyview.setVisibility(View.GONE);

                }
            });

            buybtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sellbtn.setChecked(false);
                    buybtn.setChecked(true);
                    segmented3.setTintColor(Color.parseColor("#E64A19"), Color.parseColor("#ffffffff"));
                    sellbtn.setTextColor(Color.parseColor("#FF2F2F2F"));

                    sellview.setVisibility(View.GONE);
                    buyview.setVisibility(View.VISIBLE);


                }
            });

            //=====For Seller================

            selltabLayout = (TabLayout)rootView.findViewById(R.id.sell_tab_layout);
            sellviewPager = (ViewPager)rootView.findViewById(R.id.sellviewpager);


            selltabLayout.addTab(selltabLayout.newTab().setText("Today's Offer"));
            selltabLayout.addTab(selltabLayout.newTab().setText("My Enquiries"));
            selltabLayout.addTab(selltabLayout.newTab().setText("My Deals"));
            selltabLayout.addTab(selltabLayout.newTab().setText("My History"));


            selltabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

            final SellTabsAdapter sellTabsAdapter = new SellTabsAdapter
                    (getChildFragmentManager(), selltabLayout.getTabCount());
            sellviewPager.setAdapter(sellTabsAdapter);
            selltabLayout.getTabAt(0).select();

            sellviewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(selltabLayout));

            selltabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    sellviewPager.setCurrentItem(tab.getPosition());

                 //   SingletonActivity.clickedSellCurrentEnquiry = true;

                  //  Toast.makeText(getActivity(),"onTabSelected SELL POS=="+ tab.getPosition(),Toast.LENGTH_SHORT).show();


                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                    sellviewPager.setCurrentItem(tab.getPosition());
                  //  Toast.makeText(getActivity(),"onTabUnselected SELL POS=="+ tab.getPosition(),Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                    sellviewPager.setCurrentItem(tab.getPosition());
                  //  Toast.makeText(getActivity(),"onTabReselected SELL POS=="+ tab.getPosition(),Toast.LENGTH_SHORT).show();
                }
            });


            System.out.println("SINGLETON fromselltodaysoffer==="+ SingletonActivity.fromselltodaysoffer);

           // if(SingletonActivity.isSpTradeSellerClicked != true) {




          //  }

            if(SingletonActivity.isNewEnquiryClicked==true)
            {

                selltabLayout.getTabAt(1).select();

            }

            if(SingletonActivity.fromselltodaysoffer==true)
            {

                selltabLayout.getTabAt(0).select();

            }


            if(SingletonActivity.isSpTradeSellerClicked == true) {
                selltabLayout.getTabAt(2).select();
            }



            if(SingletonActivity.isIsBuyerRejectedClicked==true)
            {

                selltabLayout.getTabAt(1).select();
            }

            if(SingletonActivity.frombackofsellerdispatch==true)
            {
                selltabLayout.getTabAt(2).select();
            }

            if(SingletonActivity.frombackofsellerpayment == true)
            {
                selltabLayout.getTabAt(2).select();
            }

            if(SingletonActivity.fromsellerhistorywebview == true)
            {
                selltabLayout.getTabAt(3).select();
            }


            //=========For Buyer=======================================

            buytabLayout = (TabLayout)rootView.findViewById(R.id.buy_tab_layout);
            buyviewPager = (ViewPager)rootView.findViewById(R.id.buyviewpager);



            buytabLayout.addTab(buytabLayout.newTab().setText("My Enquiries"));
            buytabLayout.addTab(buytabLayout.newTab().setText("My Deals"));
            buytabLayout.addTab(buytabLayout.newTab().setText("My History"));
            buytabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

           // buytabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

            final BuyTabsAdapter buyTabsAdapter = new BuyTabsAdapter
                    (getFragmentManager(), buytabLayout.getTabCount());
            buyviewPager.setAdapter(buyTabsAdapter);

            buytabLayout.getTabAt(0).select();

            buyviewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(buytabLayout));

            buytabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    buyviewPager.setCurrentItem(tab.getPosition());


                 //   Toast.makeText(getActivity(),"onTabSelected BUY POS=="+ tab.getPosition(),Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                    buyviewPager.setCurrentItem(tab.getPosition());
                  //  Toast.makeText(getActivity(),"onTabUnselected BUY POS=="+ tab.getPosition(),Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    buyviewPager.setCurrentItem(tab.getPosition());

                  //  Toast.makeText(getActivity(),"onTabReselected BUY POS=="+ tab.getPosition(),Toast.LENGTH_SHORT).show();
                }
            });



            if(SingletonActivity.isSellerAcceptedClicked==true)
            {
                buytabLayout.getTabAt(0).select();
            }

            if(SingletonActivity.isSellerRejectedClicked==true){

                buytabLayout.getTabAt(0).select();
            }

            if(SingletonActivity.isSpTradeBuyerClicked == true) {

                buytabLayout.getTabAt(1).select();
            }

            if(SingletonActivity.frombackofbuyerdispatch==true)
            {
                buytabLayout.getTabAt(1).select();
            }

            if(SingletonActivity.frombackofbuyerpayment == true)
            {
                buytabLayout.getTabAt(1).select();
            }

            if(SingletonActivity.frombuyerhistorywebview == true)
            {
                buytabLayout.getTabAt(2).select();
            }


            return rootView;
        }

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {

                case R.id.sell_btn:


                    break;

                case R.id.buy_btn:


                    break;


                default:
                    // Nothing to do
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                default:
                    // Nothing to do
            }
        }


    }

}
