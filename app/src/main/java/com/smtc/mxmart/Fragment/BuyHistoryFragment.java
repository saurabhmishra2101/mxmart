package com.smtc.mxmart.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smtc.mxmart.APIName;
import com.smtc.mxmart.BuyerHistoryWebViewActivity;
import com.smtc.mxmart.NetworkUtility;
import com.smtc.mxmart.R;
import com.smtc.mxmart.SellerDispatchActivity;
import com.smtc.mxmart.SingletonActivity;
import com.smtc.mxmart.UtilsDialog;
import com.smtc.mxmart.WebViewActivity;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by 10161 on 11/14/2017.
 */

public class BuyHistoryFragment extends Fragment {



    //String[] SPINNERLIST = {"All", "Trade Modified", "Trade Deleted", "Seller Accepted","Seller Rejected","Buyer Denied","Limit Reached","Deal Closed"};
    String[] SPINNERLIST = {"All", "Trade Modified", "Trade Deleted", "Seller Accepted","Seller Rejected","Buyer Denied","Dispute Raised","Dispute Resolved","Deal Closed"};
    Spinner seller_history_spinner;
    Typeface source_sans_pro_normal;
    String selectedspinnerstr;
    UtilsDialog util = new UtilsDialog();
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String userCodestr,mobilenumstr;
    static List<String> expandableListTitle;
    static WeakHashMap<String, List<String>> expandableListDetail;
    CustomExpandableListAdapter expandableListAdapter;
    ExpandableListView buyerhistoryexpandablelistvw;
    ArrayList<String> selleridlist = new ArrayList<String>();
    ArrayList<String> categoryidlist = new ArrayList<String>();
    ArrayList<String> buyercompanynamelist = new ArrayList<String>();
    ArrayList<String> qtylist = new ArrayList<String>();
    ArrayList<String> tradestatuslist = new ArrayList<String>();
    ArrayList<String> ratelist = new ArrayList<String>();
    ArrayList<String> ratetypelist = new ArrayList<String>();
    ArrayList<String> saledatelist = new ArrayList<String>();
    ArrayList<String> tradeidlist = new ArrayList<String>();
    ArrayList<String> isquantitylist = new ArrayList<String>();
    ArrayList<String> tradequantitylist = new ArrayList<String>();
    ArrayList<String> quantitymodifiedlist = new ArrayList<String>();
    ArrayList<String> tradeflaglist = new ArrayList<String>();
    ArrayList<String> isratelist = new ArrayList<String>();
    ArrayList<String> quoted_rate_list = new ArrayList<String>();
    ArrayList<String> sauda_patrak_url_list = new ArrayList<String>();
    boolean isEmpty = false;
    TextView nodatafoundtxt;
    private ProgressBar bar;
    Handler handler = new Handler();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {




        View view = inflater.inflate(R.layout.fragment_buy_history, container, false);
        source_sans_pro_normal = Typeface.createFromAsset(getActivity().getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);

        buyerhistoryexpandablelistvw = (ExpandableListView)view.findViewById(R.id.buyerhistoryexpandablelistvw);
        nodatafoundtxt = (TextView) view.findViewById(R.id.nodatafoundtxt);
        seller_history_spinner = (Spinner)view.
                findViewById(R.id.buyer_history_spinner);
        bar = (ProgressBar)view.findViewById(R.id.progressBar);



 /*       if(NetworkUtility.checkConnectivity(getActivity())){
            String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
            System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
            BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
        }
        else{
            util.dialog(getActivity(), "Please check your internet connection.");
        }*/


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, SPINNERLIST);
          /*  seller_history_spinner = (MaterialBetterSpinner)view.
                    findViewById(R.id.seller_history_spinner);*/


        seller_history_spinner.setAdapter(arrayAdapter);

        seller_history_spinner.setSelection(0);
        selectedspinnerstr = "All";


        // seller_history_spinner.setText("All");




        seller_history_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

           //     ((TextView)view.findViewById(R.id.spinner_txt)).setTextColor(Color.WHITE);
                categoryidlist.clear();
                selleridlist.clear();
                buyercompanynamelist.clear();
                qtylist.clear();
                tradestatuslist.clear();
                ratelist.clear();
                ratetypelist.clear();
                saledatelist.clear();
                tradeidlist.clear();
                isquantitylist.clear();
                tradequantitylist.clear();
                quantitymodifiedlist.clear();
                tradeflaglist.clear();
                isratelist.clear();
                quoted_rate_list.clear();
                sauda_patrak_url_list.clear();

                selectedspinnerstr = parent.getItemAtPosition(position).toString();


                if(position==0)
                {


                  //  ((TextView)view.findViewById(R.id.spinner_txt)).setTextColor(Color.WHITE);
                    selectedspinnerstr = "All";


                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==1)
                {
                   // ((TextView)view.findViewById(R.id.spinner_txt)).setTextColor(Color.WHITE);
                    selectedspinnerstr = "Trade Modified";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==2)
                {
                   // ((TextView)view.findViewById(R.id.spinner_txt)).setTextColor(Color.WHITE);
                    selectedspinnerstr = "Trade Deleted";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==3)
                {
                   // ((TextView)view.findViewById(R.id.spinner_txt)).setTextColor(Color.WHITE);
                    selectedspinnerstr = "Seller Accepted";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==4)
                {
                 //   ((TextView)view.findViewById(R.id.spinner_txt)).setTextColor(Color.WHITE);
                    selectedspinnerstr = "Seller Rejected";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==5)
                {
                 //   ((TextView)view.findViewById(R.id.spinner_txt)).setTextColor(Color.WHITE);
                    selectedspinnerstr = "Buyer Denied";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==6)
                {
                    //  ((TextView)view.findViewById(R.id.spinner_txt)).setTextColor(Color.WHITE);
                    selectedspinnerstr = "Dispute Raised";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }
                }

                if(position==7)
                {
                    //  ((TextView)view.findViewById(R.id.spinner_txt)).setTextColor(Color.WHITE);
                    selectedspinnerstr = "Dispute Resolved";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }
                }

                if(position==8)
                {
                 //   ((TextView)view.findViewById(R.id.spinner_txt)).setTextColor(Color.WHITE);
                    selectedspinnerstr = "Deal Closed";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

      /*  seller_history_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                categoryidlist.clear();
                selleridlist.clear();
                buyercompanynamelist.clear();
                qtylist.clear();
                tradestatuslist.clear();
                ratelist.clear();
                ratetypelist.clear();
                saledatelist.clear();
                tradeidlist.clear();
                isquantitylist.clear();
                tradequantitylist.clear();
                quantitymodifiedlist.clear();
                tradeflaglist.clear();
                isratelist.clear();
                quoted_rate_list.clear();

                selectedspinnerstr = parent.getItemAtPosition(position).toString();

                if(position==0)
                {
                    selectedspinnerstr = "All";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==1)
                {
                    selectedspinnerstr = "Trade Modified";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==2)
                {
                    selectedspinnerstr = "Trade Deleted";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==3)
                {
                    selectedspinnerstr = "Seller Accepted";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==4)
                {
                    selectedspinnerstr = "Seller Rejected";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==5)
                {
                    selectedspinnerstr = "Buyer Denied";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==6)
                {
                    selectedspinnerstr = "Limit Reached";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

                if(position==7)
                {
                    selectedspinnerstr = "Deal Closed";

                    if(NetworkUtility.checkConnectivity(getActivity())){
                        String BuyerGetDealHistoryUrl = APIName.URL+"/buyer/getDealHistory?user_code="+userCodestr;
                        System.out.println("BUYER GET DEAL HISTORY URL IS---"+ BuyerGetDealHistoryUrl);
                        BuyerGetDealHistoryAPI(BuyerGetDealHistoryUrl);
                    }
                    else{
                        util.dialog(getActivity(), "Please check your internet connection.");
                    }

                }

            }
        });

*/



        return view;

    }

    private void BuyerGetDealHistoryAPI(String url) {

       /* final ProgressDialog pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();*/

     //  getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
         //       WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        bar.setVisibility(View.VISIBLE);

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                     //   pdia.dismiss();

                      //  getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                             //   dialog.dismiss();

                                bar.setVisibility(View.GONE);
                                System.out.println("RESPONSE OF BUYER GET DEAL HISTORY API IS---" + response);

                                JSONObject BuyerGetDealHistoryJson = null;
                                try {
                                    BuyerGetDealHistoryJson = new JSONObject(response);


                                    String statusstr = BuyerGetDealHistoryJson.getString("status");

                                    if(statusstr.equalsIgnoreCase("true")) {


                                        if (!BuyerGetDealHistoryJson.getString("message").equalsIgnoreCase("Failed to retrieve trade history.")) {


                                            JSONArray BuyerGetDealHistoryJsonArray = BuyerGetDealHistoryJson.getJSONArray("history_info");


                                            SingletonActivity.BuyerHistoryJSONArray = BuyerGetDealHistoryJsonArray;
                                            System.out.println("BUYER GET DEALS HISTORY DEALS JSONARRAY LENGTH IS---" +  SingletonActivity.BuyerHistoryJSONArray.length());

                                            if (selectedspinnerstr.equalsIgnoreCase("All")) {

                                                if (SingletonActivity.BuyerHistoryJSONArray.length() > 0) {

                                                    for (int i = 0; i < SingletonActivity.BuyerHistoryJSONArray.length(); i++) {
                                                        try {
                                                        //    if (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Waiting") || (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Limit Reached"))) {
                                                                if (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Waiting")) {
                                                                System.out.println("IN LOOP1");


                                                            } else {

                                                                isEmpty = true;

                                                                System.out.println("IN LOOP2");
                                                                String categoryid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("category_id");
                                                                String buyercompanyname = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("seller_company_name");
                                                                String quantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity");
                                                                String sellerid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sell_id");
                                                                String tradestatus = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status");
                                                                String rate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate");
                                                                String ratetype = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate_type");
                                                                String saledate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sale_date");
                                                                String tradeid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_id");
                                                                String isquantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("isquantity");
                                                                String tradequantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_quantity");
                                                                String quantitymodified = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity_modified");
                                                                String tradeflag = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_flag");
                                                                String israte = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("israte");
                                                                String quotedrate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quoted_rate");
                                                                String saudapatrakurl = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("saudaPatrak_PDF");

                                                                selleridlist.add(sellerid);
                                                                categoryidlist.add(categoryid);
                                                                buyercompanynamelist.add(buyercompanyname);
                                                                qtylist.add(quantity);
                                                                tradestatuslist.add(tradestatus);
                                                                ratelist.add(rate);
                                                                ratetypelist.add(ratetype);
                                                                saledatelist.add(saledate);
                                                                tradeidlist.add(tradeid);
                                                                isquantitylist.add(isquantity);
                                                                tradequantitylist.add(tradequantity);
                                                                quantitymodifiedlist.add(quantitymodified);
                                                                tradeflaglist.add(tradeflag);
                                                                isratelist.add(israte);
                                                                quoted_rate_list.add(quotedrate);
                                                                sauda_patrak_url_list.add(saudapatrakurl);

                                                            }

                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } else {
                                                    nodatafoundtxt.setVisibility(View.VISIBLE);
                                                    buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                                }
                                            }

                                            if (selectedspinnerstr.equalsIgnoreCase("Trade Modified")) {

                                                if (SingletonActivity.BuyerHistoryJSONArray.length() > 0) {


                                                    for (int i = 0; i < SingletonActivity.BuyerHistoryJSONArray.length(); i++) {
                                                        try {
                                                            if (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Trade Modified")) {

                                                                isEmpty = true;

                                                                String categoryid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("category_id");
                                                                String buyercompanyname = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("seller_company_name");
                                                                String quantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity");
                                                                String sellerid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sell_id");
                                                                String tradestatus = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status");
                                                                String rate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate");
                                                                String ratetype = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate_type");
                                                                String saledate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sale_date");
                                                                String tradeid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_id");
                                                                String isquantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("isquantity");
                                                                String tradequantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_quantity");
                                                                String quantitymodified = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity_modified");
                                                                String tradeflag = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_flag");
                                                                String israte = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("israte");
                                                                String quotedrate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quoted_rate");

                                                                selleridlist.add(sellerid);
                                                                categoryidlist.add(categoryid);
                                                                buyercompanynamelist.add(buyercompanyname);
                                                                qtylist.add(quantity);
                                                                tradestatuslist.add(tradestatus);
                                                                ratelist.add(rate);
                                                                ratetypelist.add(ratetype);
                                                                saledatelist.add(saledate);
                                                                tradeidlist.add(tradeid);
                                                                isquantitylist.add(isquantity);
                                                                tradequantitylist.add(tradequantity);
                                                                quantitymodifiedlist.add(quantitymodified);
                                                                tradeflaglist.add(tradeflag);
                                                                isratelist.add(israte);
                                                                quoted_rate_list.add(quotedrate);


                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }
                                                } else {
                                                    nodatafoundtxt.setVisibility(View.VISIBLE);
                                                    buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                                }
                                            }

                                            if (selectedspinnerstr.equalsIgnoreCase("Trade Deleted")) {

                                                if (SingletonActivity.BuyerHistoryJSONArray.length() > 0) {

                                                    for (int i = 0; i < SingletonActivity.BuyerHistoryJSONArray.length(); i++) {
                                                        try {
                                                            if (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Trade Removed")) {

                                                                isEmpty = true;

                                                                String categoryid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("category_id");
                                                                String buyercompanyname = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("seller_company_name");
                                                                String quantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity");
                                                                String sellerid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sell_id");
                                                                String tradestatus = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status");
                                                                String rate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate");
                                                                String ratetype = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate_type");
                                                                String saledate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sale_date");
                                                                String tradeid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_id");
                                                                String isquantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("isquantity");
                                                                String tradequantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_quantity");
                                                                String quantitymodified = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity_modified");
                                                                String tradeflag = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_flag");
                                                                String israte = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("israte");
                                                                String quotedrate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quoted_rate");


                                                                selleridlist.add(sellerid);
                                                                categoryidlist.add(categoryid);
                                                                buyercompanynamelist.add(buyercompanyname);
                                                                qtylist.add(quantity);
                                                                tradestatuslist.add(tradestatus);
                                                                ratelist.add(rate);
                                                                ratetypelist.add(ratetype);
                                                                saledatelist.add(saledate);
                                                                tradeidlist.add(tradeid);
                                                                isquantitylist.add(isquantity);
                                                                tradequantitylist.add(tradequantity);
                                                                quantitymodifiedlist.add(quantitymodified);
                                                                tradeflaglist.add(tradeflag);
                                                                isratelist.add(israte);
                                                                quoted_rate_list.add(quotedrate);


                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }
                                                } else {
                                                    nodatafoundtxt.setVisibility(View.VISIBLE);
                                                    buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                                }
                                            }

                                            if (selectedspinnerstr.equalsIgnoreCase("Seller Accepted")) {

                                                if (SingletonActivity.BuyerHistoryJSONArray.length() > 0) {
                                                    for (int i = 0; i < SingletonActivity.BuyerHistoryJSONArray.length(); i++) {
                                                        try {
                                                            if (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Seller Accepted")) {

                                                                isEmpty = true;

                                                                String categoryid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("category_id");
                                                                String buyercompanyname = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("seller_company_name");
                                                                String quantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity");
                                                                String sellerid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sell_id");
                                                                String tradestatus = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status");
                                                                String rate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate");
                                                                String ratetype = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate_type");
                                                                String saledate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sale_date");
                                                                String tradeid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_id");
                                                                String isquantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("isquantity");
                                                                String tradequantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_quantity");
                                                                String quantitymodified = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity_modified");
                                                                String tradeflag = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_flag");
                                                                String israte = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("israte");
                                                                String quotedrate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quoted_rate");


                                                                selleridlist.add(sellerid);
                                                                categoryidlist.add(categoryid);
                                                                buyercompanynamelist.add(buyercompanyname);
                                                                qtylist.add(quantity);
                                                                tradestatuslist.add(tradestatus);
                                                                ratelist.add(rate);
                                                                ratetypelist.add(ratetype);
                                                                saledatelist.add(saledate);
                                                                tradeidlist.add(tradeid);
                                                                isquantitylist.add(isquantity);
                                                                tradequantitylist.add(tradequantity);
                                                                quantitymodifiedlist.add(quantitymodified);
                                                                tradeflaglist.add(tradeflag);
                                                                isratelist.add(israte);
                                                                quoted_rate_list.add(quotedrate);


                                                            }
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }
                                                } else {
                                                    nodatafoundtxt.setVisibility(View.VISIBLE);
                                                    buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                                }
                                            }

                                            if (selectedspinnerstr.equalsIgnoreCase("Seller Rejected")) {

                                                if (SingletonActivity.BuyerHistoryJSONArray.length() > 0) {
                                                    for (int i = 0; i < SingletonActivity.BuyerHistoryJSONArray.length(); i++) {
                                                        try {
                                                            if (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Seller Rejected")) {

                                                                isEmpty = true;

                                                                String categoryid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("category_id");
                                                                String buyercompanyname = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("seller_company_name");
                                                                String quantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity");
                                                                String sellerid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sell_id");
                                                                String tradestatus = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status");
                                                                String rate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate");
                                                                String ratetype = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate_type");
                                                                String saledate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sale_date");
                                                                String tradeid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_id");
                                                                String isquantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("isquantity");
                                                                String tradequantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_quantity");
                                                                String quantitymodified = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity_modified");
                                                                String tradeflag = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_flag");
                                                                String israte = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("israte");
                                                                String quotedrate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quoted_rate");

                                                                selleridlist.add(sellerid);
                                                                categoryidlist.add(categoryid);
                                                                buyercompanynamelist.add(buyercompanyname);
                                                                qtylist.add(quantity);
                                                                tradestatuslist.add(tradestatus);
                                                                ratelist.add(rate);
                                                                ratetypelist.add(ratetype);
                                                                saledatelist.add(saledate);
                                                                tradeidlist.add(tradeid);
                                                                isquantitylist.add(isquantity);
                                                                tradequantitylist.add(tradequantity);
                                                                quantitymodifiedlist.add(quantitymodified);
                                                                tradeflaglist.add(tradeflag);
                                                                isratelist.add(israte);
                                                                quoted_rate_list.add(quotedrate);


                                                            }

                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }
                                                } else {
                                                    nodatafoundtxt.setVisibility(View.VISIBLE);
                                                    buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                                }
                                            }

                                            if (selectedspinnerstr.equalsIgnoreCase("Buyer Denied")) {
                                                if (SingletonActivity.BuyerHistoryJSONArray.length() > 0) {
                                                    for (int i = 0; i < SingletonActivity.BuyerHistoryJSONArray.length(); i++) {
                                                        try {
                                                            if (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Buyer Denied")) {

                                                                isEmpty = true;

                                                                String categoryid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("category_id");
                                                                String buyercompanyname = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("seller_company_name");
                                                                String quantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity");
                                                                String sellerid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sell_id");
                                                                String tradestatus = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status");
                                                                String rate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate");
                                                                String ratetype = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate_type");
                                                                String saledate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sale_date");
                                                                String tradeid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_id");
                                                                String isquantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("isquantity");
                                                                String tradequantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_quantity");
                                                                String quantitymodified = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity_modified");
                                                                String tradeflag = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_flag");
                                                                String israte = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("israte");
                                                                String quotedrate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quoted_rate");


                                                                selleridlist.add(sellerid);
                                                                categoryidlist.add(categoryid);
                                                                buyercompanynamelist.add(buyercompanyname);
                                                                qtylist.add(quantity);
                                                                tradestatuslist.add(tradestatus);
                                                                ratelist.add(rate);
                                                                ratetypelist.add(ratetype);
                                                                saledatelist.add(saledate);
                                                                tradeidlist.add(tradeid);
                                                                isquantitylist.add(isquantity);
                                                                tradequantitylist.add(tradequantity);
                                                                quantitymodifiedlist.add(quantitymodified);
                                                                tradeflaglist.add(tradeflag);
                                                                isratelist.add(israte);
                                                                quoted_rate_list.add(quotedrate);


                                                            }

                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }
                                                } else {
                                                    nodatafoundtxt.setVisibility(View.VISIBLE);
                                                    buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                                }
                                            }

                                   /* if (selectedspinnerstr.equalsIgnoreCase("Limit Reached")) {

                                        if (SingletonActivity.BuyerHistoryJSONArray.length() > 0) {
                                            for (int i = 0; i < SingletonActivity.BuyerHistoryJSONArray.length(); i++) {
                                                try {
                                                    if (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Limit Reached")) {
                                                        String categoryid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("category_id");
                                                        String buyercompanyname = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("seller_company_name");
                                                        String quantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity");
                                                        String sellerid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sell_id");
                                                        String tradestatus = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status");
                                                        String rate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate");
                                                        String ratetype = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate_type");
                                                        String saledate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sale_date");
                                                        String tradeid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_id");
                                                        String isquantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("isquantity");
                                                        String tradequantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_quantity");
                                                        String quantitymodified = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity_modified");
                                                        String tradeflag = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_flag");
                                                        String israte = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("israte");
                                                        String quotedrate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quoted_rate");

                                                        selleridlist.add(sellerid);
                                                        categoryidlist.add(categoryid);
                                                        buyercompanynamelist.add(buyercompanyname);
                                                        qtylist.add(quantity);
                                                        tradestatuslist.add(tradestatus);
                                                        ratelist.add(rate);
                                                        ratetypelist.add(ratetype);
                                                        saledatelist.add(saledate);
                                                        tradeidlist.add(tradeid);
                                                        isquantitylist.add(isquantity);
                                                        tradequantitylist.add(tradequantity);
                                                        quantitymodifiedlist.add(quantitymodified);
                                                        tradeflaglist.add(tradeflag);
                                                        isratelist.add(israte);
                                                        quoted_rate_list.add(quotedrate);


                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        } else {
                                            nodatafoundtxt.setVisibility(View.VISIBLE);
                                            buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                        }
                                    }
*/
                                            // DISPUTE RAISED=========================================================

                                            if (selectedspinnerstr.equalsIgnoreCase("Dispute Raised")) {
                                                if (SingletonActivity.BuyerHistoryJSONArray.length() > 0) {
                                                    for (int i = 0; i < SingletonActivity.BuyerHistoryJSONArray.length(); i++) {
                                                        try {
                                                            if (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Dispute on Payment")
                                                                    || SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Dispute on Dispatch")
                                                                    || SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Dispute on Dispatch & Payment")
                                                                    || SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Dispute on Payment & Dispatch")) {

                                                                isEmpty = true;

                                                                String categoryid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("category_id");
                                                                String buyercompanyname = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("seller_company_name");
                                                                String quantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity");
                                                                String sellerid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sell_id");
                                                                String tradestatus = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status");
                                                                String rate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate");
                                                                String ratetype = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate_type");
                                                                String saledate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sale_date");
                                                                String tradeid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_id");
                                                                String isquantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("isquantity");
                                                                String tradequantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_quantity");
                                                                String quantitymodified = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity_modified");
                                                                String tradeflag = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_flag");
                                                                String israte = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("israte");
                                                                String quotedrate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quoted_rate");


                                                                selleridlist.add(sellerid);
                                                                categoryidlist.add(categoryid);
                                                                buyercompanynamelist.add(buyercompanyname);
                                                                qtylist.add(quantity);
                                                                tradestatuslist.add(tradestatus);
                                                                ratelist.add(rate);
                                                                ratetypelist.add(ratetype);
                                                                saledatelist.add(saledate);
                                                                tradeidlist.add(tradeid);
                                                                isquantitylist.add(isquantity);
                                                                tradequantitylist.add(tradequantity);
                                                                quantitymodifiedlist.add(quantitymodified);
                                                                tradeflaglist.add(tradeflag);
                                                                isratelist.add(israte);
                                                                quoted_rate_list.add(quotedrate);


                                                            }

                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }
                                                } else {
                                                    nodatafoundtxt.setVisibility(View.VISIBLE);
                                                    buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                                }
                                            }


                                            // DISPUTE RESOLVED=========================================================

                                            if (selectedspinnerstr.equalsIgnoreCase("Dispute Resolved")) {
                                                if (SingletonActivity.BuyerHistoryJSONArray.length() > 0) {
                                                    for (int i = 0; i < SingletonActivity.BuyerHistoryJSONArray.length(); i++) {
                                                        try {
                                                            if (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Dispute Resolved on Dispatch")
                                                                    || SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Dispute Resolved on Payment")
                                                                    || SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Dispute Resolved on Payment & Dispatch")
                                                                    || SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Dispute Resolved on Dispatch & Payment")) {

                                                                isEmpty = true;
                                                                String categoryid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("category_id");
                                                                String buyercompanyname = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("seller_company_name");
                                                                String quantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity");
                                                                String sellerid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sell_id");
                                                                String tradestatus = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status");
                                                                String rate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate");
                                                                String ratetype = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate_type");
                                                                String saledate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sale_date");
                                                                String tradeid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_id");
                                                                String isquantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("isquantity");
                                                                String tradequantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_quantity");
                                                                String quantitymodified = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity_modified");
                                                                String tradeflag = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_flag");
                                                                String israte = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("israte");
                                                                String quotedrate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quoted_rate");


                                                                selleridlist.add(sellerid);
                                                                categoryidlist.add(categoryid);
                                                                buyercompanynamelist.add(buyercompanyname);
                                                                qtylist.add(quantity);
                                                                tradestatuslist.add(tradestatus);
                                                                ratelist.add(rate);
                                                                ratetypelist.add(ratetype);
                                                                saledatelist.add(saledate);
                                                                tradeidlist.add(tradeid);
                                                                isquantitylist.add(isquantity);
                                                                tradequantitylist.add(tradequantity);
                                                                quantitymodifiedlist.add(quantitymodified);
                                                                tradeflaglist.add(tradeflag);
                                                                isratelist.add(israte);
                                                                quoted_rate_list.add(quotedrate);


                                                            }

                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }
                                                } else {
                                                    nodatafoundtxt.setVisibility(View.VISIBLE);
                                                    buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                                }
                                            }

                                            //=========================================================

                                            if (selectedspinnerstr.equalsIgnoreCase("Deal Closed")) {

                                                System.out.println("DEAL CLOSED CLICKED--------");

                                                if (SingletonActivity.BuyerHistoryJSONArray.length() > 0) {
                                                    for (int i = 0; i < SingletonActivity.BuyerHistoryJSONArray.length(); i++) {
                                                        try {
                                                            if (SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status").equalsIgnoreCase("Deal Closed")) {

                                                                isEmpty = true;
                                                                String categoryid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("category_id");
                                                                String buyercompanyname = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("seller_company_name");
                                                                String quantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity");
                                                                String sellerid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sell_id");
                                                                String tradestatus = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_status");
                                                                String rate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate");
                                                                String ratetype = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("rate_type");
                                                                String saledate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("sale_date");
                                                                String tradeid = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_id");
                                                                String isquantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("isquantity");
                                                                String tradequantity = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_quantity");
                                                                String quantitymodified = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quantity_modified");
                                                                String tradeflag = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("trade_flag");
                                                                String israte = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("israte");
                                                                String quotedrate = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("quoted_rate");
                                                                String saudapatrakurl = SingletonActivity.BuyerHistoryJSONArray.getJSONObject(i).getString("saudaPatrak_PDF");

                                                                selleridlist.add(sellerid);
                                                                categoryidlist.add(categoryid);
                                                                buyercompanynamelist.add(buyercompanyname);
                                                                qtylist.add(quantity);
                                                                tradestatuslist.add(tradestatus);
                                                                ratelist.add(rate);
                                                                ratetypelist.add(ratetype);
                                                                saledatelist.add(saledate);
                                                                tradeidlist.add(tradeid);
                                                                isquantitylist.add(isquantity);
                                                                tradequantitylist.add(tradequantity);
                                                                quantitymodifiedlist.add(quantitymodified);
                                                                tradeflaglist.add(tradeflag);
                                                                isratelist.add(israte);
                                                                quoted_rate_list.add(quotedrate);
                                                                sauda_patrak_url_list.add(saudapatrakurl);


                                                            }

                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }
                                                } else {
                                                    nodatafoundtxt.setVisibility(View.VISIBLE);
                                                    buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                                }
                                            }


                                            if (isEmpty == true) {
                                                buyerhistoryexpandablelistvw.setVisibility(View.VISIBLE);
                                                nodatafoundtxt.setVisibility(View.GONE);


                                                expandableListTitle = new ArrayList<String>();
                                                expandableListDetail = new WeakHashMap<String, List<String>>();

                                                if (selleridlist.size() != 0) {
                                                    expandableListAdapter = new CustomExpandableListAdapter(getActivity(), sauda_patrak_url_list,selleridlist, categoryidlist, buyercompanynamelist, qtylist, tradestatuslist, ratelist, ratetypelist, saledatelist, tradeidlist, isquantitylist, tradequantitylist, quantitymodifiedlist, tradeflaglist, isratelist, quoted_rate_list);
                                                    buyerhistoryexpandablelistvw.setAdapter(expandableListAdapter);
                                                } else {


                                                    buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                                    nodatafoundtxt.setVisibility(View.VISIBLE);
                                                }

                                                buyerhistoryexpandablelistvw.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                                                    @Override
                                                    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                                                        System.out.println("ON GROUP CLICKED--------" + groupPosition);
                                                        // parent.expandGroup(groupPosition);
                                                        return false;
                                                    }
                                                });


                                                buyerhistoryexpandablelistvw.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                                                    @Override
                                                    public boolean onChildClick(ExpandableListView parent, View v,
                                                                                int groupPosition, int childPosition, long id) {


                                                        return false;
                                                    }
                                                });

                                                buyerhistoryexpandablelistvw.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                                                    int previousGroup = -1;

                                                    @Override
                                                    public void onGroupExpand(int groupPosition) {
                                                        if (groupPosition != previousGroup)
                                                            buyerhistoryexpandablelistvw.collapseGroup(previousGroup);
                                                        previousGroup = groupPosition;
                                                    }
                                                });


                                            } else {


                                                buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                                nodatafoundtxt.setVisibility(View.VISIBLE);
                                                seller_history_spinner.setVisibility(View.GONE);

                                                System.out.println("IN ELSE 1=============");
                                            }

                                        }
                                        else
                                        {
                                            System.out.println("IN ELSE 2=============");

                                            buyerhistoryexpandablelistvw.setVisibility(View.GONE);
                                            seller_history_spinner.setVisibility(View.GONE);
                                            nodatafoundtxt.setVisibility(View.VISIBLE);
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(getActivity(),BuyerGetDealHistoryJson.getString("message"),Toast.LENGTH_SHORT).show();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    // pdia.dismiss();

                                 //   getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            //dialog.dismiss();
                                            bar.setVisibility(View.GONE);

                                        }
                                    }, 3000); // 3000 milliseconds delay


                                }



                            }
                        }, 3000); // 3000 milliseconds delay


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                       // pdia.dismiss();

                      //  getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //dialog.dismiss();
                                bar.setVisibility(View.GONE);

                            }
                        }, 3000); // 3000 milliseconds delay



                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 04) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();





                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }

    public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

        private Context context;
        ArrayList<String> selleridlist;
        ArrayList<String> categoryidlist;
        ArrayList<String> buyercompanynamelist;
        ArrayList<String> qtylist;
        ArrayList<String> tradestatuslist;
        ArrayList<String> ratelist;
        ArrayList<String> ratetypelist;
        ArrayList<String> saledatelist;
        ArrayList<String> tradeidlist;
        ArrayList<String> isquantitylist;
        ArrayList<String> tradequantitylist;
        ArrayList<String> quantitymodifiedlist;
        ArrayList<String> tradeflaglist;
        ArrayList<String> isratelist;
        ArrayList<String> quoted_rate_list;
        ArrayList<String> sauda_patrak_url_list;

        public CustomExpandableListAdapter(Context context,ArrayList<String> sauda_patrak_url_list,ArrayList<String> selleridlist,ArrayList<String> categoryidlist,ArrayList<String> buyercompanynamelist,ArrayList<String> qtylist,ArrayList<String> tradestatuslist,ArrayList<String> ratelist, ArrayList<String> ratetypelist, ArrayList<String> saledatelist, ArrayList<String> tradeidlist,ArrayList<String> isquantitylist, ArrayList<String> tradequantitylist,ArrayList<String> quantitymodifiedlist,ArrayList<String> tradeflaglist,  ArrayList<String> isratelist,ArrayList<String> quoted_rate_list) {
            this.context = context;
            this.selleridlist = selleridlist;
            this.categoryidlist = categoryidlist;
            this.buyercompanynamelist = buyercompanynamelist;
            this.qtylist = qtylist;
            this.tradestatuslist = tradestatuslist;
            this.ratelist = ratelist;
            this.ratetypelist = ratetypelist;
            this.saledatelist = saledatelist;
            this.tradeidlist = tradeidlist;
            this.isquantitylist = isquantitylist;
            this.tradequantitylist = tradequantitylist;
            this.quantitymodifiedlist = quantitymodifiedlist;
            this.tradeflaglist = tradeflaglist;
            this.isratelist = isratelist;
            this.quoted_rate_list = quoted_rate_list;
            this.sauda_patrak_url_list = sauda_patrak_url_list;
        }

        @Override
        public Object getChild(int listPosition, int expandedListPosition) {

            System.out.println("IN GETCHILD listPosition---"+ listPosition);

            System.out.println("IN GETCHILD expandedListPosition---"+ expandedListPosition);

            return listPosition;




        }

        @Override
        public long getChildId(int listPosition, int expandedListPosition) {

            System.out.println("IN GETCHILDID listPosition---"+ listPosition);

            return listPosition;
        }

        @Override
        public View getChildView(final int listPosition, final int expandedListPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            //   final String expandedListText = (String) getChild(listPosition, expandedListPosition);
            System.out.println("IN GETCHILDVIEW---");

            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) this.context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.buyer_history_all_tab_child_row, null);
            }




            TextView materialdesctxt = (TextView) convertView
                    .findViewById(R.id.materialdesctxt);


            TextView buyerdesctxtvw = (TextView) convertView
                    .findViewById(R.id.buyerdesctxtvw);

            TextView qtydesctxtvw = (TextView) convertView
                    .findViewById(R.id.qtydesctxtvw);

            TextView saleiddesctxtvw = (TextView) convertView
                    .findViewById(R.id.saleiddesctxtvw);

            TextView statusdesctxtvw = (TextView) convertView
                    .findViewById(R.id.statusdesctxtvw);

            TextView pricetontxtvw = (TextView) convertView
                    .findViewById(R.id.pricetontxtvw);

            TextView paymenttypedesctxtvw = (TextView) convertView
                    .findViewById(R.id.paymenttypedesctxtvw);

            TextView saudapatraktitletxtvw =  (TextView) convertView
                    .findViewById(R.id.saudapatraktitletxtvw);

            ImageView saudapatraiv = (ImageView) convertView
                    .findViewById(R.id.saudapatraiv);





            String category = "";

            if(categoryidlist.get(listPosition).equalsIgnoreCase("1"))
            {
                category = "Billet";
            }

            if(categoryidlist.get(listPosition).equalsIgnoreCase("2"))
            {
                category = "MS Ingot";
            }
            if(categoryidlist.get(listPosition).equalsIgnoreCase("3"))
            {
                category = "Sponge Iron";
            }

            if(categoryidlist.get(listPosition).equalsIgnoreCase("4"))
            {
                category = "Sponge Iron DR-CLO";
            }


            materialdesctxt.setText(category);
            buyerdesctxtvw.setText(buyercompanynamelist.get(listPosition));
            qtydesctxtvw.setText(qtylist.get(listPosition));
            saleiddesctxtvw.setText(selleridlist.get(listPosition));
            statusdesctxtvw.setText(tradestatuslist.get(listPosition));

            if(tradestatuslist.get(listPosition).equalsIgnoreCase("Deal Closed"))
            {
                //statusdesctxtvw.setTextColor(Color.parseColor("#00ff00"));
                statusdesctxtvw.setTextColor(Color.parseColor("#008000"));

                saudapatraktitletxtvw.setVisibility(View.VISIBLE);
                saudapatraiv.setVisibility(View.VISIBLE);


                saudapatraiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String saudapatraurl = sauda_patrak_url_list.get(listPosition);

                        Intent i =  new Intent(getActivity(),BuyerHistoryWebViewActivity.class);
                        i.putExtra("saudapatraurl",saudapatraurl);
                        startActivity(i);

                    }
                });


            }
            else {

                saudapatraktitletxtvw.setVisibility(View.INVISIBLE);
                saudapatraiv.setVisibility(View.INVISIBLE);

                statusdesctxtvw.setTextColor(Color.parseColor("#ff0000"));


                if(tradestatuslist.get(listPosition).equalsIgnoreCase("Seller Accepted")||tradestatuslist.get(listPosition).equalsIgnoreCase("Sauda Patrak Generat"))
                {
                    statusdesctxtvw.setTextColor(Color.parseColor("#008000"));



                }


            }

            if(tradestatuslist.get(listPosition).equalsIgnoreCase("Sauda Patrak Generat"))
            {
                statusdesctxtvw.setText("Sauda Patrak Generated");
            }

            pricetontxtvw.setText(ratelist.get(listPosition));

            String paymenttypestr ="";

            if(ratetypelist.get(listPosition).equalsIgnoreCase("1"))
            {
                paymenttypestr = "Advance";
            }
            if(ratetypelist.get(listPosition).equalsIgnoreCase("2"))
            {
                paymenttypestr = "Next Day";
            }

            if(ratetypelist.get(listPosition).equalsIgnoreCase("3"))
            {
                paymenttypestr = "Regular";
            }

            if(!tradestatuslist.get(listPosition).equalsIgnoreCase("Deal Closed")){
                if(isquantitylist.get(listPosition).equalsIgnoreCase("0"))
                {
                    // qtyDetLbl.text = [NSString stringWithFormat:@"%@",[[self.sellHistoryArray objectAtIndex:adjustedIndexPath.row]objectForKey:@"trade_quantity"]];
                    qtydesctxtvw.setText(tradequantitylist.get(listPosition)+" Tons");

                }
                else
                {
                    //qtyDetLbl.text = [NSString stringWithFormat:@"%@",[[self.sellHistoryArray objectAtIndex:adjustedIndexPath.row]objectForKey:@"quantity_modified"]];
                    qtydesctxtvw.setText(quantitymodifiedlist.get(listPosition)+" Tons");
                }
            }else {
               /* qtyDetLbl.text =[NSString stringWithFormat:@ "%@",[[
                self.sellHistoryArray objectAtIndex:adjustedIndexPath.row]objectForKey:
                @ "trade_quantity"]];*/
                qtydesctxtvw.setText(tradequantitylist.get(listPosition)+" Tons");
            }


                if (!isratelist.get(listPosition).equalsIgnoreCase("1")) {
                    if (!quoted_rate_list.get(listPosition).equalsIgnoreCase("0.00")){
                        //  inVoiceAmntDetLbl.text = [NSString stringWithFormat:@"%d", [[[self.sellHistoryArray objectAtIndex:adjustedIndexPath.row]objectForKey:@"rate"] intValue]-[[[self.sellHistoryArray objectAtIndex:adjustedIndexPath.row]objectForKey:@"quoted_rate"] intValue]];

                        System.out.println("LIST POSITION IN LOOP 1==="+ listPosition);
                        double price = Double.parseDouble(ratelist.get(listPosition)) - Double.parseDouble(quoted_rate_list.get(listPosition));
                        String pricestr = String.format("%.2f", price);
                      //  pricetontxtvw.setText(Double.toString(price));
                        pricetontxtvw.setText(pricestr+"");

                    }else{
                        // inVoiceAmntDetLbl.text = [NSString stringWithFormat:@"%@", [[self.sellHistoryArray objectAtIndex:adjustedIndexPath.row]objectForKey:@"rate"]];

                        System.out.println("LIST POSITION IN LOOP 2==="+ listPosition);
                        pricetontxtvw.setText(ratelist.get(listPosition));
                      //  pricetontxtvw.setText(String.format("%.2f", ratelist.get(listPosition))+"");
                    }
                }else{
                    // inVoiceAmntDetLbl.text =[NSString stringWithFormat:@"%@", [[self.sellHistoryArray objectAtIndex:adjustedIndexPath.row]objectForKey:@"rate"]];
                    System.out.println("LIST POSITION IN LOOP 3==="+ listPosition);
                    pricetontxtvw.setText(ratelist.get(listPosition));
                   // pricetontxtvw.setText(String.format("%.2f", ratelist.get(listPosition))+"");
                }


            paymenttypedesctxtvw.setText(paymenttypestr);

            if(tradestatuslist.get(listPosition).equalsIgnoreCase("Trade Removed"))
            {
                statusdesctxtvw.setText("Trade Deleted");



            }


            if (tradestatuslist.get(listPosition).equalsIgnoreCase("Dispute Resolved on Dispatch")
                    || tradestatuslist.get(listPosition).equalsIgnoreCase("Dispute Resolved on Payment")
                    || tradestatuslist.get(listPosition).equalsIgnoreCase("Dispute Resolved on Payment & Dispatch")
                    || tradestatuslist.get(listPosition).equalsIgnoreCase("Dispute Resolved on Dispatch & Payment")) {

                statusdesctxtvw.setTextColor(Color.parseColor("#008000"));
            }



            return convertView;
        }

        @Override
        public int getChildrenCount(int listPosition) {

            System.out.println("IN GETCHILDRENCOUNT---"+ listPosition);
            return 1;

        }

        @Override
        public Object getGroup(int listPosition) {

            System.out.println("IN GETGROUP---");
            return 1;

        }

        @Override
        public int getGroupCount() {

            System.out.println("IN GETGROUPCOUNT---");
            return selleridlist.size();
        }

        @Override
        public long getGroupId(int listPosition) {
            return listPosition;
        }

        @Override
        public View getGroupView(final int listPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
//            String listTitle = (String) getGroup(listPosition);

            System.out.println("IN GETGROUPVIEW---");

            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) this.context.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.buyer_history_all_tab_header_row, null);
            }

            source_sans_pro_normal = Typeface.createFromAsset(getActivity().getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

            TextView listTitleTextView = (TextView) convertView
                    .findViewById(R.id.date);
            TextView tradeidtxt = (TextView) convertView
                    .findViewById(R.id.tradeidtxt);






            listTitleTextView.setText(saledatelist.get(listPosition));
            tradeidtxt.setText("Trade ID. "+tradeidlist.get(listPosition));

            listTitleTextView.setTypeface(source_sans_pro_normal);
            tradeidtxt.setTypeface(source_sans_pro_normal);






            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            System.out.println("IN HASSTABLEIDS---");
            return false;
        }

        @Override
        public boolean isChildSelectable(int listPosition, int expandedListPosition) {
            System.out.println("IN ISCHILDSELECTABLE---");
            return true;
        }
    }
}
