package com.smtc.mxmart.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smtc.mxmart.APIName;
import com.smtc.mxmart.BuyerDispatchActivity;
import com.smtc.mxmart.NetworkUtility;
import com.smtc.mxmart.R;
import com.smtc.mxmart.SellerDispatchActivity;
import com.smtc.mxmart.SellerPaymentDetailsActivity;
import com.smtc.mxmart.SingletonActivity;
import com.smtc.mxmart.UtilsDialog;
import com.smtc.mxmart.WebViewActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by 10161 on 11/14/2017.
 */

public class SellCurrentDealsFragment extends Fragment {

    Typeface source_sans_pro_normal;
    String userCodestr,mobilenumstr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    UtilsDialog util = new UtilsDialog();
    ProgressDialog pdia;
    ListView selltodayscurrentdealslistvw;
    CustomAdap customadap;
    Context c;
    String tradeid,sellid;
    TextView nodealsfoundtxt;
    private boolean _hasLoadedOnce= false; // your boolean field
    private ProgressBar bar;
    Handler handler = new Handler();

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


            View view = inflater.inflate(R.layout.fragment_sell_current_deals, container, false);

            source_sans_pro_normal = Typeface.createFromAsset(getActivity().getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");
            nodealsfoundtxt = (TextView)view.findViewById(R.id.nodealsfoundtxt);
           // nodealsfoundtxt.setTypeface(source_sans_pro_normal);

            selltodayscurrentdealslistvw = (ListView)view.findViewById(R.id.sellcurrentdealslistvw);
            bar = (ProgressBar)view.findViewById(R.id.progressBar);

            SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            userCodestr = prefs.getString("user_code", null);
            mobilenumstr = prefs.getString("mobile_num", null);



            if(NetworkUtility.checkConnectivity(getActivity())){

                //  String SellerGetCurrentDealsUrl = APIName.URL+"/seller/getCurrentDeals?user_code="+userCodestr;
                String SellerGetCurrentDealsUrl = APIName.URL+"/seller/getCurrentDeals?user_code="+userCodestr+"&type=1";
                System.out.println("SELLER GET CURRENT DEALS URL IS---"+ SellerGetCurrentDealsUrl);
                SellerGetCurrentDealsAPI(SellerGetCurrentDealsUrl);


            }
            else
            {
                util.dialog(getActivity(), "Please check your internet connection.");
            }


            return view;

        }


    private void SellerGetCurrentDealsAPI(String url) {

       // getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
           //     WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        bar.setVisibility(View.VISIBLE);

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {

                     //   getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {

                                bar.setVisibility(View.GONE);
                                System.out.println("RESPONSE OF SELLER GET CURRENT DEALS API IS---" + response);



                                JSONObject SellerGetCurrentDealsJson = null;
                                try {
                                    SellerGetCurrentDealsJson = new JSONObject(response);


                                    String statusstr = SellerGetCurrentDealsJson.getString("status");

                                    if(statusstr.equalsIgnoreCase("true"))
                                    {

                                        nodealsfoundtxt.setVisibility(View.GONE);
                                        selltodayscurrentdealslistvw.setVisibility(View.VISIBLE);

                                        JSONArray SellerCurrentDealsJsonArray = SellerGetCurrentDealsJson.getJSONArray("cd_Offer");
                                        System.out.println("SELLER CURRENT DEALS JSONARRAY IS---" + SellerCurrentDealsJsonArray);

                                        if(getActivity()!=null) {
                                            customadap = new CustomAdap(getActivity(), SellerCurrentDealsJsonArray);
                                            selltodayscurrentdealslistvw.setAdapter(customadap);
                                        }

                                    }
                                    else
                                    {
                                        nodealsfoundtxt.setVisibility(View.VISIBLE);
                                        selltodayscurrentdealslistvw.setVisibility(View.GONE);

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();

                                 //  getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            //dialog.dismiss();
                                            bar.setVisibility(View.GONE);

                                        }
                                    }, 3000); // 3000 milliseconds delay



                                }


                            }
                        }, 3000); // 3000 milliseconds delay




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                     //   getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //dialog.dismiss();
                                bar.setVisibility(View.GONE);

                            }
                        }, 3000); // 3000 milliseconds delay


                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();



                System.out.println("get seller current enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }



    public void ViewOfferDialog(String sell_tandc_str,String modified_field_str,String delivery_type_str,String delivery_in_days_str,String material_inspection_str,String weighment_type_str,String test_certificate_type_str,String promo_str,String length_str,String categoryid,String transport_str,String loading_str,String gst_str,String insurance_select_str,String insurance_str,String category_grade_str,String size_range_start_str,String size_range_end_str,String q1_title_str,String q1_value_str,String q2_title_str,String q2_value_str,String q3_title_str,String q3_value_str,String q4_title_str,String q4_value_str,String q5_title_str,String q5_value_str,String q6_title_str,String q6_value_str,String q7_title_str,String q7_value_str,String q8_title_str,String q8_value_str,String category_grade_name_str,String rate_type_str,String israte,String tradeidstr,String companyname,String categorydescstr,String subcategorydescstr,String locationdesc,String qtyofferedstr,String ratestr,String terms,String markdownpriceval,String tradestatusval) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.view_seller_current_deals, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);


        final TextView tradeidtitletxtvw = (TextView)dialogView.findViewById(R.id.tradeidtitletxtvw);
        final TextView cmpnynamedesctxt = (TextView) dialogView.findViewById(R.id.cmpnynamedesctxt);
        final TextView categorydesctxt = (TextView) dialogView.findViewById(R.id.categorydesctxt);
        final TextView subcategorydesctxt = (TextView) dialogView.findViewById(R.id.subcategorydesctxt);
        final TextView locationdesctxt = (TextView) dialogView.findViewById(R.id.locationdesctxt);
        final TextView qtyoffereddesctxtvw = (TextView)dialogView.findViewById(R.id.qtyoffereddesctxtvw);
        final TextView ratedesctxtvw = (TextView)dialogView.findViewById(R.id.ratedesctxtvw);
        final TextView pricetxt1 = (TextView) dialogView.findViewById(R.id.pricetxt1);
        final TextView pricetxt2 = (TextView) dialogView.findViewById(R.id.pricetxt2);
        final TextView pricetxt3 = (TextView) dialogView.findViewById(R.id.pricetxt3);
        final TextView pricetxt4 = (TextView) dialogView.findViewById(R.id.pricetxt4);
        final TextView pricetxt5 = (TextView) dialogView.findViewById(R.id.pricetxt5);
        final TextView pricetxt6 = (TextView) dialogView.findViewById(R.id.pricetxt6);
        final TextView pricetxt7 = (TextView) dialogView.findViewById(R.id.pricetxt7);
        final TextView pricetxt8 = (TextView) dialogView.findViewById(R.id.pricetxt8);
        final TextView termsdesctxt = (TextView) dialogView.findViewById(R.id.termsdesctxt);
        final TextView markdownpricedesctxt = (TextView) dialogView.findViewById(R.id.markdownpricedesctxt);
        final TextView statusdesctxt = (TextView) dialogView.findViewById(R.id.statusdesctxt);

        final TextView transportationdesctxt = (TextView) dialogView.findViewById(R.id.transportationdesctxt);
        final TextView loadingdesctxt = (TextView) dialogView.findViewById(R.id.loadingdesctxt);
        final TextView gstdesctxt = (TextView) dialogView.findViewById(R.id.gstdesctxt);
        final TextView insurancedesctxt = (TextView) dialogView.findViewById(R.id.insurancedesctxt);


        final TextView q1title = (TextView) dialogView.findViewById(R.id.q1title);
        final TextView q2title = (TextView) dialogView.findViewById(R.id.q2title);
        final TextView q3title = (TextView) dialogView.findViewById(R.id.q3title);
        final TextView q4title = (TextView) dialogView.findViewById(R.id.q4title);
        final TextView q5title = (TextView) dialogView.findViewById(R.id.q5title);
        final TextView q6title = (TextView) dialogView.findViewById(R.id.q6title);
        final TextView q7title = (TextView) dialogView.findViewById(R.id.q7title);
        final TextView q8title = (TextView) dialogView.findViewById(R.id.q8title);
        final TextView gradedesctxt = (TextView) dialogView.findViewById(R.id.gradedesctxt);

        final TextView lengthtitletxtvw =  (TextView) dialogView.findViewById(R.id.lengthtitletxtvw);
        final TextView lengthdesctxt = (TextView) dialogView.findViewById(R.id.lengthdesctxt);
        final View line16 = (View)dialogView.findViewById(R.id.line16);
        final TextView promodesctxt =  (TextView) dialogView.findViewById(R.id.promodesctxt);

        final TextView deliveryindaysdesctxt = (TextView)dialogView.findViewById(R.id.deliveryindaysdesctxt);
        final TextView materialinspectiondesctxt = (TextView)dialogView.findViewById(R.id.materialinspectiondesctxt);
        final TextView weighmentdesctxt = (TextView)dialogView.findViewById(R.id.weighmentdesctxt);
        final TextView testcertificatedesctxt = (TextView)dialogView.findViewById(R.id.testcertificatedesctxt);

        if(delivery_type_str.equalsIgnoreCase("1"))
        {
            deliveryindaysdesctxt.setText("As per Aachar Sanhita");
        }
        if(delivery_type_str.equalsIgnoreCase("2"))
        {
            deliveryindaysdesctxt.setText(delivery_in_days_str+ " days");
        }

        if(material_inspection_str.equalsIgnoreCase("1"))
        {
            materialinspectiondesctxt.setText("Buyer's Place");
        }
        if(material_inspection_str.equalsIgnoreCase("2"))
        {
            materialinspectiondesctxt.setText("Seller's Place");
        }

        if(weighment_type_str.equalsIgnoreCase("1"))
        {
            weighmentdesctxt.setText("Buyer's Place");
        }
        if(weighment_type_str.equalsIgnoreCase("2"))
        {
            weighmentdesctxt.setText("Seller's Place");
        }
        if(weighment_type_str.equalsIgnoreCase("3"))
        {
            weighmentdesctxt.setText("Buyer & Seller's Place");
        }

        if(test_certificate_type_str.equalsIgnoreCase("1"))
        {
            testcertificatedesctxt.setText("Yes");
           // testcertificatedesctxt.setText("Required");
        }
        if(test_certificate_type_str.equalsIgnoreCase("2"))
        {
            testcertificatedesctxt.setText("No");
         //   testcertificatedesctxt.setText("Not Required");
        }


        if(!(promo_str.equalsIgnoreCase("0"))){
            promodesctxt.setText(promo_str);

            if((promo_str.equalsIgnoreCase("(null)")))
            {
                promodesctxt.setText("--");
            }
        }
        else
        {
            promodesctxt.setText("--");
        }



        if(length_str.length()!=4) {

           // lengthdesctxt.setText(length_str+" m");
            line16.setVisibility(View.VISIBLE);
            lengthtitletxtvw.setVisibility(View.VISIBLE);
            lengthdesctxt.setVisibility(View.VISIBLE);

            if(categoryid.equalsIgnoreCase("1")) {

                lengthtitletxtvw.setText("Length");
                lengthdesctxt.setText(length_str+" m");
            }

            if(categoryid.equalsIgnoreCase("2"))
            {
                lengthtitletxtvw.setText("Weight");
                lengthdesctxt.setText(length_str+" kg");
            }
        }
        else
        {
            line16.setVisibility(View.GONE);
            lengthtitletxtvw.setVisibility(View.GONE);
            lengthdesctxt.setVisibility(View.GONE);
        }

        gradedesctxt.setText(category_grade_name_str);

        pricetxt1.setText(q1_value_str);
        pricetxt2.setText(q2_value_str);
        pricetxt3.setText(q3_value_str);
        pricetxt4.setText(q4_value_str);
        pricetxt5.setText(q5_value_str);
        pricetxt6.setText(q6_value_str);

        //pricetxt7.setText(q7_value_str);
       // pricetxt8.setText(q8_value_str);

        if(!q7_value_str.equalsIgnoreCase(""))
        {
            pricetxt7.setText(q7_value_str);
        }
        else {
            pricetxt7.setText("NA");
        }

        if(!q8_value_str.equalsIgnoreCase(""))
        {
            pricetxt8.setText(q8_value_str);
        }
        else
        {
            pricetxt8.setText("NA");
        }

        List<String> modified_field_List = Arrays.asList(modified_field_str.split(","));


        if (modified_field_List.contains("-3-"))
        {
            pricetxt1.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-4-"))
        {
            pricetxt2.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-5-"))
        {
            pricetxt3.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-6-"))
        {
            pricetxt4.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-7-"))
        {
            pricetxt5.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-8-"))
        {
            pricetxt6.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-9-"))
        {
            pricetxt7.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-10-"))
        {
            pricetxt8.setTextColor(Color.RED);
        }

        q1title.setText(q1_title_str);
        q2title.setText(q2_title_str);
        q3title.setText(q3_title_str);
        q4title.setText(q4_title_str);
        q5title.setText(q5_title_str);
        q6title.setText(q6_title_str);
        q7title.setText(q7_title_str);
        q8title.setText(q8_title_str);


        if(transport_str.equalsIgnoreCase("1"))
        {
            transportationdesctxt.setText("Seller's End");
        }
        if(transport_str.equalsIgnoreCase("2"))
        {
            transportationdesctxt.setText("Buyer's End");
        }
        if(transport_str.equalsIgnoreCase("3"))
        {
            transportationdesctxt.setText("Mxmart's End");
        }

        if(loading_str.equalsIgnoreCase("NA"))
        {
            loadingdesctxt.setText(loading_str);
        }
        else {
            loadingdesctxt.setText(loading_str + " \u20B9"+"/MT");
        }
        gstdesctxt.setText(gst_str);

        if(insurance_select_str.equalsIgnoreCase("NA"))
        {
            insurancedesctxt.setText("NA");
        }
        else {

            if (insurance_str.equalsIgnoreCase("1")) {
                insurancedesctxt.setText("Seller");
            } else {
                insurancedesctxt.setText("Buyer");
            }
        }


        if(categoryid.equalsIgnoreCase("1")||categoryid.equalsIgnoreCase("2")) {
            subcategorydesctxt.setText(subcategorydescstr);
        }
        else {
            subcategorydesctxt.setText(size_range_start_str + "-" + size_range_end_str);
        }


        System.out.println("TERMS ARE==="+ terms.length());
        if(!(terms.length()==4)) {
            termsdesctxt.setText(sell_tandc_str);
        }


        tradeidtitletxtvw.setText("Trade ID :"+tradeidstr);
        cmpnynamedesctxt.setText(companyname);
        categorydesctxt.setText(categorydescstr);
       // subcategorydesctxt.setText(subcategorydescstr);
        locationdesctxt.setText(locationdesc);
        qtyoffereddesctxtvw.setText(qtyofferedstr);


       // ratedesctxtvw.setText(ratestr);



        if(rate_type_str.equalsIgnoreCase("1"))
        {
            rate_type_str = "(A)";
        }

        if(rate_type_str.equalsIgnoreCase("2"))
        {
            rate_type_str = "(N)";
        }

        if(rate_type_str.equalsIgnoreCase("3"))
        {
            rate_type_str = "(R)";
        }


        if (!israte.equalsIgnoreCase("1")) {
            if (!markdownpriceval.equalsIgnoreCase("0.00")){


                double price = Double.parseDouble(ratestr) - Double.parseDouble(markdownpriceval);

                ratedesctxtvw.setText(Double.toString(price)+""+rate_type_str);

            }else{



                ratedesctxtvw.setText(ratestr+""+rate_type_str);
            }
        }else{


            ratedesctxtvw.setText(ratestr+""+rate_type_str);
        }



        //termsdesctxt.setText(terms);
        markdownpricedesctxt.setText(markdownpriceval);
        System.out.println("TRADE STATUS VALUE IS==="+ tradestatusval);

        if(tradestatusval.equalsIgnoreCase("Sauda Patrak Generat"))
        {
            tradestatusval = "Sauda Patrak Generated";
        }
        statusdesctxt.setText(tradestatusval);




      /*  if(tradestatusval.equalsIgnoreCase()) {
            statusdesctxt.setText(tradestatusval);
        }*/





        final AlertDialog b = dialogBuilder.create();
        b.show();


        ImageView ic_close = (ImageView)dialogView.findViewById(R.id.ic_close);
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }

    private class CustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        private JSONArray SellCurrentDealsJSONArray;



        // public CustomAdap(Context mainActivity)
        public CustomAdap(Context mainActivity,JSONArray SellCurrentDealsJSONArray)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.SellCurrentDealsJSONArray = SellCurrentDealsJSONArray;
            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            // return 7;
            return SellCurrentDealsJSONArray.length();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub

             final Holder holder = new Holder();


            View rowView = null;




            if(rowView==null){
               /* LayoutInflater inflater =(LayoutInflater)
                        ((Activity)context).getSystemService(Context.LAYOUT_INFLATER_SERVICE);*/
                rowView = inflater.inflate(R.layout.sell_current_deals_row, null);


                holder.cmpnyname = (TextView)rowView.findViewById(R.id.cmpnynametxtvw);
                holder.ratingBar = (RatingBar)rowView.findViewById(R.id.ratingBar);
                holder.tradeiddesc = (TextView)rowView.findViewById(R.id.tradeiddesctxtvw);
                holder.proddesc = (TextView) rowView.findViewById(R.id.categorytxtvw);
             //   holder.sub_cat_txt = (TextView) rowView.findViewById(R.id.sub_cat_txt);
                holder.qtydesc = (TextView) rowView.findViewById(R.id.qtydesctxtvw);
                holder.saudapatraiv = (ImageView) rowView.findViewById(R.id.saudapatraiv);
                holder.dispatchiv = (ImageView)  rowView.findViewById(R.id.dispatchiv);
                holder.tradeidtitletxtvw = (TextView) rowView.findViewById(R.id.tradeidtitletxtvw);
                holder.paymentiv = (ImageView)  rowView.findViewById(R.id.offeriv);
                holder.closeiv = (ImageView)  rowView.findViewById(R.id.closeiv);
                holder.disputeiv = (ImageView) rowView.findViewById(R.id.disputeiv);
                rowView.setTag(holder);
            }
           // holder = (Holder) rowView.getTag();


            SingletonActivity.splited = "not split";

            holder.dispatchiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {




                    try {
                        tradeid = SellCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                        sellid = SellCurrentDealsJSONArray.getJSONObject(position).getString("sell_id");
                        String sauda_patra_gen_date = SellCurrentDealsJSONArray.getJSONObject(position).getString("sauda_patra_gen_date");
                        String gst_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("gst");


                        SingletonActivity.tradeid = tradeid;
                        SingletonActivity.sellid = sellid;
                        SingletonActivity.sauda_patra_gen_date = sauda_patra_gen_date;
                        SingletonActivity.fromsellcurrentdeals = true;
                        SingletonActivity.frombuycurrentdeals = true;
                        SingletonActivity.gst_str = gst_str;

                     //   if(SellCurrentDealsJSONArray.getJSONObject(position).getString("transport").equalsIgnoreCase("1")) {
                            Intent i = new Intent(getActivity(), SellerDispatchActivity.class);
                            SingletonActivity.fromsellcurrentdeals = true;
                            SingletonActivity.frombuycurrentdeals = false;
                            i.putExtra("tradeid", tradeid);
                            i.putExtra("sellid", sellid);
                            startActivity(i);

                   //     }

                       /* else
                        {



                            Intent i = new Intent(getActivity(), BuyerDispatchActivity.class);
                            SingletonActivity.fromsellcurrentdeals = true;
                            SingletonActivity.frombuycurrentdeals = false;

                            i.putExtra("splitted",SingletonActivity.splited);
*//*
                            i.putExtra("tradeid", tradeid);
                            i.putExtra("sellid", sellid);*//*
                            startActivity(i);
                        }
*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            holder.saudapatraiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        String saudapatraurl = SellCurrentDealsJSONArray.getJSONObject(position).getString("saudaPatrak_PDF");

                        Intent i =  new Intent(getActivity(),WebViewActivity.class);
                        SingletonActivity.fromsellcurrentdeals = true;
                        SingletonActivity.frombuycurrentdeals = false;
                        i.putExtra("saudapatraurl",saudapatraurl);
                        startActivity(i);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            System.out.println("SELLER CURRENT DEALS JSONARRAY IN CUSTOMADAP---"+ SellCurrentDealsJSONArray);

            JSONArray  dispatchStr ;
            final JSONArray  paymentStr ;
            try {



                dispatchStr = SellCurrentDealsJSONArray.getJSONObject(position).getJSONArray("dispatch_details");
                paymentStr = SellCurrentDealsJSONArray.getJSONObject(position).getJSONArray("payment_details");

                System.out.println("dispatchStr---"+ dispatchStr);
                System.out.println("dispatchStr Length---"+ dispatchStr.length());
                System.out.println("paymentStr---"+ paymentStr);
                System.out.println("paymentStr Length---"+ paymentStr.length());

                int dispathDispute = 1;
                int paymentDispute = 1;
                int dispatch_ack = 1;
                int payment_ack = 1;


                if (dispatchStr.length() == 0 && paymentStr.length() == 0) {
                    //sp green color
                    System.out.println("position green---"+ position);
                    holder.saudapatraiv.setBackgroundResource(R.mipmap.ic_saudapatra);
                    holder.saudapatraiv.setVisibility(View.VISIBLE);

                } else if (dispatchStr.length() > 0 || paymentStr.length() > 0) {
                    if (dispatchStr.length() > 0) {
                        for (int i = 0; i < dispatchStr.length(); i++) {

                            //System.out.println("ack status--" + dispatchStr.getJSONObject(position).getString("ack_status"));

                            if (dispatchStr.getJSONObject(i).getString("ack_status").equalsIgnoreCase("0")) {
                                if (dispatch_ack == 1) {
                                    dispatch_ack = 0;
                                }
                            }
                            if (dispatchStr.getJSONObject(i).getString("is_active").equalsIgnoreCase("9")) {
                                holder.saudapatraiv.setBackgroundResource(R.mipmap.ic_saudapatra_grey);
                                holder.saudapatraiv.setVisibility(View.VISIBLE);
                                System.out.println("position grey---"+ position);
                                dispathDispute = 9;
                                break;
                            }
                        }

                    }
                    if (paymentStr.length() > 0) {
                        for (int j = 0; j < paymentStr.length(); j++) {
                            if (paymentStr.getJSONObject(j).getString("ack_status").equalsIgnoreCase("0")) {
                                if (payment_ack == 1) {
                                    payment_ack = 0;
                                }
                            }
                            if (paymentStr.getJSONObject(j).getString("is_active").equalsIgnoreCase("9")) {
                                holder.saudapatraiv.setBackgroundResource(R.mipmap.ic_saudapatra_grey);
                                holder.saudapatraiv.setVisibility(View.VISIBLE);
                                System.out.println("position grey---"+ position);
                                paymentDispute = 9;
                                break;
                            }
                        }
                    }
                    System.out.println("paymentDispute--" + paymentDispute);
                    System.out.println("dispathDispute--" + dispathDispute);
                    System.out.println("tradeId--" + SellCurrentDealsJSONArray.getJSONObject(position).getString("trade_id"));
                    if (paymentDispute == 9 || dispathDispute == 9) {
                        //sp grey
                        holder.saudapatraiv.setBackgroundResource(R.mipmap.ic_saudapatra_grey);
                        holder.saudapatraiv.setVisibility(View.VISIBLE);
                    } else {
                        //sp green
                        holder.saudapatraiv.setBackgroundResource(R.mipmap.ic_saudapatra);
                        holder.saudapatraiv.setVisibility(View.VISIBLE);
                    }
                }

                //disptch(old)
                System.out.println("splited_trade_id--" + SellCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id"));
               /* if (!SellCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id").equals("null")) {
                    //sp grey
                    System.out.println("in splited_1--");

                    holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                    holder.dispatchiv.setVisibility(View.VISIBLE);
                } else if (dispatchStr.length() == 0) {
                    //truck grey

                    System.out.println("in splited_2--");
                    holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                    holder.dispatchiv.setVisibility(View.VISIBLE);
                } else {
                    if (dispathDispute == 9 || paymentDispute == 9) {
                        //truck grey
                        System.out.println("in splited_3--");
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    } else if (dispatch_ack == 1) {
                        //truck green
                        System.out.println("in splited_4--");
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    } else {
                        //truck red

                        System.out.println("in splited_5--");
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_red);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    }
                }*/

                if (!SellCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id").equals("null")) {
                    //sp grey
                    System.out.println("in splited_1--");

                    holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                    holder.dispatchiv.setVisibility(View.VISIBLE);
                } else if (paymentDispute == 9) {
                    //truck grey

                    System.out.println("in splited_2--");
                    holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                    holder.dispatchiv.setVisibility(View.VISIBLE);
                } else if (dispathDispute == 9) {
                        //truck grey
                        System.out.println("in splited_3--");
                       // holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_red);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                } else {

                    if(dispatchStr.length() == 0)
                    {
                        holder.dispatchiv.setBackgroundResource(R.mipmap.dispatch);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    }else if(dispatch_ack == 1){
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    }
                     else if(dispatch_ack == 0){
                     //Blue icons==
                        holder.dispatchiv.setBackgroundResource(R.mipmap.dispatch_blue);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    }

                    else{
                        holder.dispatchiv.setBackgroundResource(R.mipmap.dispatch);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    }

                }




                //payment(old)
              /*  if (!SellCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id").equals("null")) {
                    //rupees grey color
                    System.out.println("pay splited_1--");
                    holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_displayed);
                    holder.paymentiv.setVisibility(View.VISIBLE);

                } else if (paymentStr.length() == 0) {
                    //rupees grey
                    System.out.println("pay splited_2--");
                    holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_displayed);
                    holder.paymentiv.setVisibility(View.VISIBLE);
                } else {
                    if (dispathDispute == 9 || paymentDispute == 9) {
                        //rupees grey
                        System.out.println("pay splited_3--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_displayed);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                    } else if (payment_ack == 1) {
                        //rupees green
                        System.out.println("pay splited_4--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                    } else {
                        System.out.println("pay splited_5--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_red);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                        //rupees red
                    }
                }
*/

                if (!SellCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id").equals("null")) {
                    //rupees grey color
                    System.out.println("pay splited_1--");
                    holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_displayed);
                    holder.paymentiv.setVisibility(View.VISIBLE);

                } else if (paymentDispute == 9) {
                    //rupees grey
                    System.out.println("pay splited_2--");
                    holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_red);
                    holder.paymentiv.setVisibility(View.VISIBLE);
                } else if (dispathDispute == 9) {
                        //rupees grey
                        System.out.println("pay splited_3--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_displayed);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                } else {
                    if (paymentStr.length() == 0) {
                        //rupees green
                        System.out.println("pay splited_4--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.payment);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                    } else if(payment_ack == 1){
                        System.out.println("pay splited_5--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                        //rupees red
                    }
                     else if(payment_ack == 0){
                        System.out.println("blue icon of payment--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.payment_blue);
                        holder.paymentiv.setVisibility(View.VISIBLE);

                    }
                    else {
                        holder.paymentiv.setBackgroundResource(R.mipmap.payment);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                    }

                }


                //REd==========
                if(dispathDispute == 9 && paymentDispute == 9){
                    //sellDealPaymentImg.image = [UIImage imageNamed:@"icn_offer_red"];//Payment
                    holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_red);

                    //sellDealDspImg.image = [UIImage imageNamed:@"icn_dispatch"];//Dispatch
                    holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_red);
                }



                // Deal close

                if (dispatchStr.length() != 0 && paymentStr.length() != 0) {
                    if (dispathDispute == 9 || paymentDispute == 9) {
                        // grey cross icon
                        // title="Dispute has been raised."

                        System.out.println("in deal close loop---1");
                       // holder.closeiv.setBackgroundResource(R.mipmap.ic_close_disabled);
                        holder.closeiv.setBackgroundResource(R.mipmap.dealclose_handshake_grey);
                        holder.closeiv.setVisibility(View.VISIBLE);
                        holder.closeiv.setEnabled(false);
                    } else if (dispatch_ack == 1 && payment_ack == 1) {
                        if (SellCurrentDealsJSONArray.getJSONObject(position).getString("deal_close").equals("null")||SellCurrentDealsJSONArray.getJSONObject(position).getString("deal_close").equalsIgnoreCase("B"))
                        {
                            System.out.println("in deal close loop---2");
                          //  holder.closeiv.setBackgroundResource(R.mipmap.dealclose);
                            holder.closeiv.setBackgroundResource(R.mipmap.dealclose_handshake_orange);
                            holder.closeiv.setVisibility(View.VISIBLE);
                            holder.closeiv.setEnabled(true);
                        }
                        else if (SellCurrentDealsJSONArray.getJSONObject(position).getString("deal_close").equalsIgnoreCase("S")) {
                            //
                            // title="Waiting for Buyer to close the deal."
                            System.out.println("in deal close loop---3");
                            //orange
                           // holder.closeiv.setBackgroundResource(R.mipmap.dealclose_green);
                            holder.closeiv.setBackgroundResource(R.mipmap.dealclose_handshake_green);
                            holder.closeiv.setVisibility(View.VISIBLE);
                            holder.closeiv.setEnabled(false);
                        }



                    }


                }

                System.out.println("position ppp---"+ position);

                String  dispatch_resolved = SellCurrentDealsJSONArray.getJSONObject(position).getString("dispatch_resolved");
                String  payment_resolved = SellCurrentDealsJSONArray.getJSONObject(position).getString("payment_resolved");

                if (dispathDispute == 9 || paymentDispute == 9) {

                    holder.disputeiv.setBackgroundResource(R.mipmap.exclamation);
                    holder.disputeiv.setVisibility(View.VISIBLE);
                }

                else   if ((dispatch_resolved.length() > 2) || (payment_resolved.length() > 2)) {
                    holder.disputeiv.setBackgroundResource(R.mipmap.orange_info);
                    holder.disputeiv.setVisibility(View.VISIBLE);

                }



                holder.closeiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {



                        if(NetworkUtility.checkConnectivity(getActivity())){

                            try {
                                holder.closeiv.setEnabled(false);
                                String SellerCloseDealUrl = APIName.URL+"/seller/closeDeal?trade_id="+SellCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                                System.out.println("SELLER CLOSE DEAL API URL IS---"+ SellerCloseDealUrl);
                                SellerCloseDealsAPI(SellerCloseDealUrl);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }



                        }
                        else
                        {
                            util.dialog(getActivity(), "Please check your internet connection.");
                        }



                    }
                });

                holder.paymentiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent i = new Intent(getActivity(), SellerPaymentDetailsActivity.class);
                        SingletonActivity.SellerpaymentDetailsJSONArray = paymentStr;
                           SingletonActivity.fromsellcurrentdeals = true;
                           SingletonActivity.frombuycurrentdeals = false;
                        // SingletonActivity.sellerusercode = sellerusercode;
                        try {
                            SingletonActivity.tradeid = SellCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                            SingletonActivity.sellid = SellCurrentDealsJSONArray.getJSONObject(position).getString("sell_id");
                            SingletonActivity.buyerusercode = SellCurrentDealsJSONArray.getJSONObject(position).getString("buyer_user_code");
                            SingletonActivity.buyermobilenum = SellCurrentDealsJSONArray.getJSONObject(position).getString("buyer_mobile_num");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);
                    }
                });



            } catch (JSONException e) {
                e.printStackTrace();
            }


           rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    try {
                        String tradeidstr = SellCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                        String companyname = SellCurrentDealsJSONArray.getJSONObject(position).getString("buyer_company_name");
                        String categorydesc = SellCurrentDealsJSONArray.getJSONObject(position).getString("category_name");
                        String subcategorydesc = SellCurrentDealsJSONArray.getJSONObject(position).getString("sub_category_name");
                        String locationdesc = SellCurrentDealsJSONArray.getJSONObject(position).getString("buyer_city");
                        String qtyofferedstr = SellCurrentDealsJSONArray.getJSONObject(position).getString("quantity");
                        String ratestr = SellCurrentDealsJSONArray.getJSONObject(position).getString("rate");
                       /* String q1val = SellCurrentDealsJSONArray.getJSONObject(position).getString("q1");
                        String q2val = SellCurrentDealsJSONArray.getJSONObject(position).getString("q2");
                        String q3val = SellCurrentDealsJSONArray.getJSONObject(position).getString("q3");
                        String q4val = SellCurrentDealsJSONArray.getJSONObject(position).getString("q4");
                        String q5val = SellCurrentDealsJSONArray.getJSONObject(position).getString("q5");
                        String q6val = SellCurrentDealsJSONArray.getJSONObject(position).getString("q6");
                        String q7val = SellCurrentDealsJSONArray.getJSONObject(position).getString("q7");
                        String q8val = SellCurrentDealsJSONArray.getJSONObject(position).getString("q8");*/
                        String tncval = SellCurrentDealsJSONArray.getJSONObject(position).getString("terms");
                        String markdownpriceval = SellCurrentDealsJSONArray.getJSONObject(position).getString("quoted_rate");
                        String tradestatusval = SellCurrentDealsJSONArray.getJSONObject(position).getString("trade_status");
                        String israte = SellCurrentDealsJSONArray.getJSONObject(position).getString("israte");
                        String rate_type_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("rate_type");
                        String category_grade_name_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("category_grade_name");
                        String category_grade_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("category_grade");
                        String size_range_start_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("size_range_start");
                        String size_range_end_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("size_range_end");
                        String q1_title_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q1_title");
                        String q1_value_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q1_value");
                        String q2_title_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q2_title");
                        String q2_value_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q2_value");
                        String q3_title_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q3_title");
                        String q3_value_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q3_value");
                        String q4_title_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q4_title");
                        String q4_value_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q4_value");
                        String q5_title_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q5_title");
                        String q5_value_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q5_value");
                        String q6_title_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q6_title");
                        String q6_value_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q6_value");
                        String q7_title_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q7_title");
                        String q7_value_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q7_value");
                        String q8_title_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q8_title");
                        String q8_value_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("q8_value");
                        String transport_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("transport");
                        String loading_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("loading");
                        String gst_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("gst");
                        String insurance_select_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("insurance_select");
                        String insurance_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("insurance");
                        String categoryid = SellCurrentDealsJSONArray.getJSONObject(position).getString("category_id");
                        String length_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("length");
                        String promo_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("promo_code");
                        String modified_field_str = SellCurrentDealsJSONArray.getJSONObject(position).getString("modified_field");

                        JSONArray detailJSONArray = SellCurrentDealsJSONArray.getJSONObject(position).getJSONArray("detail");


                        final String delivery_type_str = detailJSONArray.getJSONObject(0).getString("delivery_type");
                        final String delivery_in_days_str = detailJSONArray.getJSONObject(0).getString("delivery_in_days");
                        final String material_inspection_str = detailJSONArray.getJSONObject(0).getString("material_inspection_type");
                        final String weightment_type_str = detailJSONArray.getJSONObject(0).getString("weightment_type");
                        final String test_certificate_type_str = detailJSONArray.getJSONObject(0).getString("test_certificate_type");
                        final String sell_tandc_str = detailJSONArray.getJSONObject(0).getString("sell_tandc");



                        ViewOfferDialog(sell_tandc_str,modified_field_str,delivery_type_str,delivery_in_days_str,material_inspection_str,weightment_type_str,test_certificate_type_str,promo_str,length_str,categoryid,transport_str,loading_str,gst_str,insurance_select_str,insurance_str,category_grade_str,size_range_start_str,size_range_end_str,q1_title_str,q1_value_str,q2_title_str,q2_value_str,q3_title_str,q3_value_str,q4_title_str,q4_value_str,q5_title_str,q5_value_str,q6_title_str,q6_value_str,q7_title_str,q7_value_str,q8_title_str,q8_value_str,category_grade_name_str,rate_type_str,israte,tradeidstr,companyname,categorydesc,subcategorydesc,locationdesc,qtyofferedstr,ratestr,tncval,markdownpriceval,tradestatusval);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });








            try {
                holder.cmpnyname.setText(SellCurrentDealsJSONArray.getJSONObject(position).getString("buyer_company_name"));
                holder.ratingBar.setRating(Float.parseFloat(SellCurrentDealsJSONArray.getJSONObject(position).getString("rate_type")));

                if(!SellCurrentDealsJSONArray.getJSONObject(position).getString("trade_id").equalsIgnoreCase("")) {
                    holder.tradeiddesc.setVisibility(View.VISIBLE);
                    holder.tradeiddesc.setText(SellCurrentDealsJSONArray.getJSONObject(position).getString("trade_id"));
                }
                else
                {
                    holder.tradeiddesc.setVisibility(View.INVISIBLE);
                }
                holder.proddesc.setText(SellCurrentDealsJSONArray.getJSONObject(position).getString("category_name"));
                System.out.println("SUB CATEGORY NAME IN SELL CURRENT DEALS---"+ SellCurrentDealsJSONArray.getJSONObject(position).getString("sub_category_name"));
//                holder.sub_cat_txt.setText(SellCurrentDealsJSONArray.getJSONObject(position).getString("sub_category_name"));
                holder.qtydesc.setText(SellCurrentDealsJSONArray.getJSONObject(position).getString("quantity"));






            } catch (JSONException e) {
                e.printStackTrace();
            }



            return rowView;

        }


    }


    public class Holder {
        TextView cmpnyname,proddesc,sub_cat_txt,qtydesc,tradeiddesc,tradeidtitletxtvw;
        ImageView saudapatraiv,dispatchiv,paymentiv,closeiv,disputeiv;
        RatingBar ratingBar;



    }

    private void SellerCloseDealsAPI(String url) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF SELLER CLOSE DEALS API IS---" + response);




                        JSONObject SellerCloseDealJson = null;
                        try {
                            SellerCloseDealJson = new JSONObject(response);


                            String statusstr = SellerCloseDealJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                Toast.makeText(getActivity(),SellerCloseDealJson.getString("message"),Toast.LENGTH_SHORT).show();

                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                ft.detach(SellCurrentDealsFragment.this).attach(SellCurrentDealsFragment.this).commit();




                              /*  if(NetworkUtility.checkConnectivity(getActivity())){

                                    //  String SellerGetCurrentDealsUrl = APIName.URL+"/seller/getCurrentDeals?user_code="+userCodestr;
                                    String SellerGetCurrentDealsUrl = APIName.URL+"/seller/getCurrentDeals?user_code="+userCodestr+"&type=1";
                                    System.out.println("SELLER GET CURRENT DEALS URL IS---"+ SellerGetCurrentDealsUrl);
                                    SellerGetCurrentDealsAPI(SellerGetCurrentDealsUrl);


                                }
                                else{
                                    util.dialog(getActivity(), "Please check your internet connection.");
                                }*/



                            }
                            else
                            {
                                Toast.makeText(getActivity(),SellerCloseDealJson.getString("message"),Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();





                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }

}


