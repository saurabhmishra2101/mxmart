package com.smtc.mxmart.Fragment;


import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smtc.mxmart.APIName;
import com.smtc.mxmart.NetworkUtility;
import com.smtc.mxmart.R;
import com.smtc.mxmart.SingletonActivity;
import com.smtc.mxmart.UtilsDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class HomeFragmentNew extends Fragment {

    TextView categorytitletxt1, subcategorytitletxt1, pricetxt1;
    TextView categorytitletxt2, subcategorytitletxt2, pricetxt2;
    TextView categorytitletxt3, subcategorytitletxt3, pricetxt3;
    TextView categorytitletxt4, subcategorytitletxt4, pricetxt4;
    TextView categorytitletxt5, subcategorytitletxt5, pricetxt5;
    TextView categorytitletxt6, subcategorytitletxt6, pricetxt6;
    UtilsDialog util = new UtilsDialog();
    ArrayList<String> categoryal = new ArrayList<String>();
    ArrayList<String> subcategoryal = new ArrayList<String>();
    ArrayList<String> rateal = new ArrayList<String>();

    ArrayList<String> categoryraigarhal = new ArrayList<String>();
    ArrayList<String> subcategoryraigarhal = new ArrayList<String>();
    ArrayList<String> rateraigarhal = new ArrayList<String>();

    ArrayList<String> categorygobindgarhal = new ArrayList<String>();
    ArrayList<String> subcategorygobindgarhal = new ArrayList<String>();
    ArrayList<String> rategobindgarhal = new ArrayList<String>();

    ArrayList<String> categorymumbaial = new ArrayList<String>();
    ArrayList<String> subcategorymumbaial = new ArrayList<String>();
    ArrayList<String> ratemumbaial = new ArrayList<String>();

    ArrayList<String> categorydurgapural = new ArrayList<String>();
    ArrayList<String> subcategorydurgapural = new ArrayList<String>();
    ArrayList<String> ratedurgapural = new ArrayList<String>();

    ArrayList<String> categoryhyderabadal = new ArrayList<String>();
    ArrayList<String> subcategoryhyderabadal = new ArrayList<String>();
    ArrayList<String> ratehyderabadal = new ArrayList<String>();
    ProgressDialog pdia;
    boolean isAnimationRunning = false;
    int i = 0;
    Thread t;
    TextView livepricetitletxt,raipurtitletxt,raigarhtitletxt,gobindgarhtitletxt,mumbaititletxt,durgapurtitletxt,hyderabadtitletxt;
    Typeface source_sans_pro_normal,montserrrat_normal;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.fragment_home_new, container, false);
        livepricetitletxt = (TextView)view.findViewById(R.id.livepricetitletxt);
        raipurtitletxt =  (TextView)view.findViewById(R.id.raipurtitletxt);
        raigarhtitletxt = (TextView)view.findViewById(R.id.raigarhtitletxt);
        gobindgarhtitletxt = (TextView)view.findViewById(R.id.gobindgarhtitletxt);
        mumbaititletxt = (TextView)view.findViewById(R.id.mumbaititletxt);
        durgapurtitletxt = (TextView)view.findViewById(R.id.durgapurtitletxt);
        hyderabadtitletxt = (TextView)view.findViewById(R.id.hyderabadtitletxt);

        categorytitletxt1 = (TextView) view.findViewById(R.id.categorytitletxt1);
        subcategorytitletxt1 = (TextView) view.findViewById(R.id.subcategorytitletxt1);
        pricetxt1 = (TextView) view.findViewById(R.id.pricetxt1);

        categorytitletxt2 = (TextView) view.findViewById(R.id.categorytitletxt2);
        subcategorytitletxt2 = (TextView) view.findViewById(R.id.subcategorytitletxt2);
        pricetxt2 = (TextView) view.findViewById(R.id.pricetxt2);

        categorytitletxt3 = (TextView) view.findViewById(R.id.categorytitletxt3);
        subcategorytitletxt3 = (TextView) view.findViewById(R.id.subcategorytitletxt3);
        pricetxt3 = (TextView) view.findViewById(R.id.pricetxt3);

        categorytitletxt4 = (TextView) view.findViewById(R.id.categorytitletxt4);
        subcategorytitletxt4 = (TextView) view.findViewById(R.id.subcategorytitletxt4);
        pricetxt4 = (TextView) view.findViewById(R.id.pricetxt4);

        categorytitletxt5 = (TextView) view.findViewById(R.id.categorytitletxt5);
        subcategorytitletxt5 = (TextView) view.findViewById(R.id.subcategorytitletxt5);
        pricetxt5 = (TextView) view.findViewById(R.id.pricetxt5);

        categorytitletxt6 = (TextView)view.findViewById(R.id.categorytitletxt6);
        subcategorytitletxt6 = (TextView)view.findViewById(R.id.subcategorytitletxt6);
        pricetxt6 = (TextView) view.findViewById(R.id.pricetxt6);

        source_sans_pro_normal = Typeface.createFromAsset(getActivity().getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");
        montserrrat_normal = Typeface.createFromAsset(getActivity().getAssets(), "montserrat/Montserrat-Regular.ttf");

        livepricetitletxt.setTypeface(source_sans_pro_normal);

        raipurtitletxt.setTypeface(montserrrat_normal);
        raigarhtitletxt.setTypeface(montserrrat_normal);
        gobindgarhtitletxt.setTypeface(montserrrat_normal);
        mumbaititletxt.setTypeface(montserrrat_normal);
        durgapurtitletxt.setTypeface(montserrrat_normal);
        hyderabadtitletxt.setTypeface(montserrrat_normal);

        categorytitletxt1.setTypeface(montserrrat_normal);
        subcategorytitletxt1.setTypeface(montserrrat_normal);
        pricetxt1.setTypeface(montserrrat_normal);

        categorytitletxt2.setTypeface(montserrrat_normal);
        subcategorytitletxt2.setTypeface(montserrrat_normal);
        pricetxt2.setTypeface(montserrrat_normal);

        categorytitletxt3.setTypeface(montserrrat_normal);
        subcategorytitletxt3.setTypeface(montserrrat_normal);
        pricetxt3.setTypeface(montserrrat_normal);

        categorytitletxt4.setTypeface(montserrrat_normal);
        subcategorytitletxt4.setTypeface(montserrrat_normal);
        pricetxt4.setTypeface(montserrrat_normal);

        categorytitletxt5.setTypeface(montserrrat_normal);
        subcategorytitletxt5.setTypeface(montserrrat_normal);
        pricetxt5.setTypeface(montserrrat_normal);

        categorytitletxt6.setTypeface(montserrrat_normal);
        subcategorytitletxt6.setTypeface(montserrrat_normal);
        pricetxt6.setTypeface(montserrrat_normal);

        SingletonActivity.isPassedFromLogin=false;

        if(NetworkUtility.checkConnectivity(getActivity())){
            String GetLivePriceUrl = APIName.URL+"/home/getLivePrice";
            System.out.println("GET LIVE PRICE URL IS---"+ GetLivePriceUrl);
            GetLivePriceAPI(GetLivePriceUrl);

        }
        else{
            util.dialog(getActivity(), "Please check your internet connection.");
        }
        return  view;

    }

    private void GetLivePriceAPI(String url) {

        pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        pdia.dismiss();

                        categoryal.clear();
                        subcategoryal.clear();
                        rateal.clear();


                        categoryraigarhal.clear();
                        subcategoryraigarhal.clear();
                        rateraigarhal.clear();

                        categorygobindgarhal.clear();
                        subcategorygobindgarhal.clear();
                        rategobindgarhal.clear();

                        categorymumbaial.clear();
                        subcategorymumbaial.clear();
                        ratemumbaial.clear();

                        categorydurgapural.clear();
                        subcategorydurgapural.clear();
                        ratedurgapural.clear();

                        categoryhyderabadal.clear();
                        subcategoryhyderabadal.clear();
                        ratehyderabadal.clear();

                        System.out.println("RESPONSE OF GET LIVE PRICE API IS---" + response);

                        try {

                            JSONObject getlivepricejson = new JSONObject(response);
                            System.out.println("GET LIVE PRICE JSON IS---" + getlivepricejson);

                            String statusstr = getlivepricejson.getString("status");

                            if (statusstr.equalsIgnoreCase("true")) {

                                JSONObject LivePriceJson = getlivepricejson.getJSONObject("livePrice");
                                System.out.println("LIVEPRICE JSON IS---" + LivePriceJson);

                                //For Raipur------------------------->

                                JSONObject RaipurJson = LivePriceJson.getJSONObject("Raipur");
                                System.out.println("RAIPUR JSON IS---" + RaipurJson);


                                String BilletRaipurJsonStr = RaipurJson.getString("Billet");
                                System.out.println("RAIPUR BILLET JSON STRING IS---" + BilletRaipurJsonStr);

                                if(BilletRaipurJsonStr.equalsIgnoreCase(""))
                                {
                                    categoryal.add("-");
                                    subcategoryal.add("-");
                                    rateal.add("-");

                                }
                                else
                                {
                                    JSONObject BilletRaipurJson = new JSONObject(BilletRaipurJsonStr);

                                    System.out.println("RAIPUR BILLET JSON IS---" + BilletRaipurJson);

                                    String categorystr = BilletRaipurJson.getString("category");
                                    // String subcategorystr = BilletRaipurJson.getString("sub_category");
                                    String ratestr = BilletRaipurJson.getString("rate");

                                    categoryal.add(categorystr);

                                    subcategoryal.add("-");
                                    rateal.add(ratestr);

                                    System.out.println("RAIPUR SUB CAT VALUE  IS---" + subcategoryal);

                                }

                                String MSIngotRaipurJsonStr = RaipurJson.getString("MS Ingot");
                                System.out.println("RAIPUR MS INGOT JSON STRING IS---" + MSIngotRaipurJsonStr);


                                if(MSIngotRaipurJsonStr.equalsIgnoreCase(""))
                                {
                                    categoryal.add("-");
                                    subcategoryal.add("-");
                                    rateal.add("-");

                                }
                                else
                                {
                                    JSONObject MSIngotRaipurJson = new JSONObject(MSIngotRaipurJsonStr);
                                    System.out.println("RAIPUR MS INGOT JSON IS---" + MSIngotRaipurJson);

                                    String categorystr = MSIngotRaipurJson.getString("category");
                                    // String subcategorystr = MSIngotRaipurJson.getString("sub_category");
                                    String ratestr = MSIngotRaipurJson.getString("rate");

                                    categoryal.add(categorystr);
                                    subcategoryal.add("-");
                                    rateal.add(ratestr);

                                }


                                String SpongeIronRaipurJsonStr = RaipurJson.getString("Sponge Iron");
                                System.out.println("RAIPUR SPONGE IRON JSON STRING IS---" + SpongeIronRaipurJsonStr);


                                if(SpongeIronRaipurJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronRaipurJson = new JSONObject(SpongeIronRaipurJsonStr);
                                    System.out.println("RAIPUR SPONGE IRON JSON IS---" + SpongeIronRaipurJson);



                                    String lumpsjsonstr = SpongeIronRaipurJson.getString("Lumps");
                                    System.out.println("RAIPUR SPONGE IRON LUMPS JSON STRING IS---" + lumpsjsonstr);

                                    if(!lumpsjsonstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsJSON = new JSONObject(lumpsjsonstr);
                                        System.out.println("RAIPUR SPONGE IRON LUMPS JSON IS---" + LumpsJSON);

                                        String categorystr = LumpsJSON.getString("category");
                                        String subcategorystr = LumpsJSON.getString("sub_category");
                                        String ratestr = LumpsJSON.getString("rate");

                                        categoryal.add(categorystr);
                                        subcategoryal.add(subcategorystr);
                                        rateal.add(ratestr);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }

                                    String finesjsonstr= SpongeIronRaipurJson.getString("Fines");
                                    System.out.println("RAIPUR SPONGE IRON FINES JSON IS---" + finesjsonstr);

                                    if(!finesjsonstr.equalsIgnoreCase("")) {

                                        JSONObject FinesJSON = new JSONObject(finesjsonstr);

                                        String categoryfinesstr = FinesJSON.getString("category");
                                        String subcategoryfinesstr = FinesJSON.getString("sub_category");
                                        String ratefinesstr = FinesJSON.getString("rate");

                                        categoryal.add(categoryfinesstr);
                                        subcategoryal.add(subcategoryfinesstr);
                                        rateal.add(ratefinesstr);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }

                                    String pelletjsonstr = SpongeIronRaipurJson.getString("Pellet");
                                    System.out.println("RAIPUR SPONGE IRON PELLET JSON IS---" + pelletjsonstr);

                                    if(!pelletjsonstr.equalsIgnoreCase("")) {

                                        JSONObject PelletJSON = new JSONObject(pelletjsonstr);

                                        String categorypelletstr = PelletJSON.getString("category");
                                        String subcategorypelletstr = PelletJSON.getString("sub_category");
                                        String ratepelletstr = PelletJSON.getString("rate");

                                        categoryal.add(categorypelletstr);
                                        subcategoryal.add(subcategorypelletstr);
                                        rateal.add(ratepelletstr);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }



                                    String mix020jsonstr = SpongeIronRaipurJson.getString("Mix 0-20");
                                    System.out.println("RAIPUR SPONGE IRON MIX 0-20 JSON IS---" + mix020jsonstr);

                                    if(!mix020jsonstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020JSON = new JSONObject(mix020jsonstr);

                                        String categorymix020str = Mix020JSON.getString("category");
                                        String subcategorymix020str = Mix020JSON.getString("sub_category");
                                        String ratemix020str = Mix020JSON.getString("rate");

                                        categoryal.add(categorymix020str);
                                        subcategoryal.add(subcategorymix020str);
                                        rateal.add(ratemix020str);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }


                                }


                                String SpongeIronDRCLORaipurJsonStr = RaipurJson.getString("Sponge Iron DR-CLO");
                                System.out.println("RAIPUR SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLORaipurJsonStr);


                                if(SpongeIronDRCLORaipurJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLORaipurJson = new JSONObject(SpongeIronDRCLORaipurJsonStr);
                                    System.out.println("RAIPUR SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLORaipurJson);


                                    String LumpsDRCLOJSONstr = SpongeIronDRCLORaipurJson.getString("Lumps DR-CLO");
                                    System.out.println("RAIPUR SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLOJSONstr);

                                    if(!LumpsDRCLOJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLOJSON = new JSONObject(LumpsDRCLOJSONstr);

                                        String categorystr = LumpsDRCLOJSON.getString("category");
                                        String subcategorystr = LumpsDRCLOJSON.getString("sub_category");
                                        String ratestr = LumpsDRCLOJSON.getString("rate");

                                        categoryal.add(categorystr);
                                        subcategoryal.add(subcategorystr);
                                        rateal.add(ratestr);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }

                                    String FinesDRCLOJSONstr = SpongeIronDRCLORaipurJson.getString("Fines DR-CLO");
                                    System.out.println("RAIPUR SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLOJSONstr);

                                    if(!FinesDRCLOJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLOJSON = new JSONObject(FinesDRCLOJSONstr);

                                        String categoryfinesdrclostr = FinesDRCLOJSON.getString("category");
                                        String subcategoryfinesdrclostr = FinesDRCLOJSON.getString("sub_category");
                                        String ratefinesdrclostr = FinesDRCLOJSON.getString("rate");

                                        categoryal.add(categoryfinesdrclostr);
                                        subcategoryal.add(subcategoryfinesdrclostr);
                                        rateal.add(ratefinesdrclostr);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }

                                    String Mix020DRCLOJSONstr = SpongeIronDRCLORaipurJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("RAIPUR SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLOJSONstr);

                                    if(!Mix020DRCLOJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DRCLOJSON = new JSONObject(Mix020DRCLOJSONstr);

                                        String categorymix020drclostr = Mix020DRCLOJSON.getString("category");
                                        String subcategorydrclostr = Mix020DRCLOJSON.getString("sub_category");
                                        String ratedrclostr = Mix020DRCLOJSON.getString("rate");

                                        categoryal.add(categorymix020drclostr);
                                        subcategoryal.add(subcategorydrclostr);
                                        rateal.add(ratedrclostr);


                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }

                                }

                                System.out.println("RAIPUR CATEGORY LIST IS---" + categoryal);
                                System.out.println("RAIPUR SUB CATEGORY LIST IS---" + subcategoryal);
                                System.out.println("RAIPUR RATE LIST IS---" + rateal);



                                //For Raigarh-----------------------



                                JSONObject RaigarhJson = LivePriceJson.getJSONObject("Raigarh");
                                System.out.println("RAIGARH JSON IS---" + RaipurJson);


                                String BilletRaigarhJsonStr = RaigarhJson.getString("Billet");
                                System.out.println("RAIGARH BILLET JSON STRING IS---" + BilletRaigarhJsonStr);

                                if(BilletRaigarhJsonStr.equalsIgnoreCase(""))
                                {
                                    categoryraigarhal.add("-");
                                    subcategoryraigarhal.add("-");
                                    rateraigarhal.add("-");

                                }
                                else
                                {
                                    JSONObject BilletRaigarhJson = new JSONObject(BilletRaigarhJsonStr);


                                    System.out.println("RAIGARH BILLET JSON IS---" + BilletRaigarhJson);

                                    String categoryraigarhbilletstr = BilletRaigarhJson.getString("category");
                                    String subcategoryraigarhbilletstr = BilletRaigarhJson.getString("sub_category");
                                    String rateraigarhbilletstr = BilletRaigarhJson.getString("rate");

                                    if(subcategoryraigarhbilletstr.equalsIgnoreCase("")||subcategoryraigarhbilletstr.equalsIgnoreCase("0"))
                                    {
                                        subcategoryraigarhal.add("-");
                                    }
                                    else
                                    {
                                        subcategoryraigarhal.add(subcategoryraigarhbilletstr);
                                    }

                                    categoryraigarhal.add(categoryraigarhbilletstr);
                                    System.out.println("RAIGARH categoryraigarhal 1---" + categoryraigarhal);

                                    rateraigarhal.add(rateraigarhbilletstr);
                                }

                                String MSIngotRaigarhJsonStr = RaigarhJson.getString("MS Ingot");
                                System.out.println("RAIGARH MS INGOT JSON STRING IS---" + MSIngotRaigarhJsonStr);


                                if(MSIngotRaigarhJsonStr.equalsIgnoreCase(""))
                                {
                                    categoryraigarhal.add("-");
                                    subcategoryraigarhal.add("-");
                                    rateraigarhal.add("-");

                                }
                                else
                                {
                                    JSONObject MSIngotRaigarhJson = new JSONObject(MSIngotRaigarhJsonStr);
                                    System.out.println("RAIGARH MS INGOT JSON IS---" + MSIngotRaigarhJson);

                                    String categoryraigarhmsingotstr = MSIngotRaigarhJson.getString("category");
                                    String subcategoryraigarhmsingotstr = MSIngotRaigarhJson.getString("sub_category");
                                    String rateraigarhmsingotstr = MSIngotRaigarhJson.getString("rate");

                                    if(subcategoryraigarhmsingotstr.equalsIgnoreCase("")||subcategoryraigarhmsingotstr.equalsIgnoreCase("0"))
                                    {
                                        subcategoryraigarhal.add("-");
                                    }
                                    else
                                    {
                                        subcategoryraigarhal.add(subcategoryraigarhmsingotstr);
                                    }

                                    categoryraigarhal.add(categoryraigarhmsingotstr);
                                    System.out.println("RAIGARH categoryraigarhal 2---" + categoryraigarhal);


                                    rateraigarhal.add(rateraigarhmsingotstr);

                                }


                                String SpongeIronRaigarhJsonStr = RaigarhJson.getString("Sponge Iron");
                                System.out.println("RAIGARH SPONGE IRON JSON STRING IS---" + SpongeIronRaigarhJsonStr);


                                if(SpongeIronRaigarhJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronRaigarhJson = new JSONObject(SpongeIronRaigarhJsonStr);
                                    System.out.println("RAIGARH SPONGE IRON JSON IS---" + SpongeIronRaigarhJson);

                                    String lumpsraigarhjsonstr = SpongeIronRaigarhJson.getString("Lumps");
                                    System.out.println("LUMPS RAIGARH SPONGE STRING IS---" + lumpsraigarhjsonstr);

                                    if(!lumpsraigarhjsonstr.equalsIgnoreCase("")) {


                                        JSONObject LumpsRaigarhJSON = new JSONObject(lumpsraigarhjsonstr);
                                        System.out.println("RAIGARH SPONGE IRON LUMPS JSON IS---" + LumpsRaigarhJSON);

                                        String categorylumpsraigarhstr = LumpsRaigarhJSON.getString("category");
                                        String subcategorylumpsraigarhstr = LumpsRaigarhJSON.getString("sub_category");
                                        String ratelumpsraigarhstr = LumpsRaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categorylumpsraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 3---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategorylumpsraigarhstr);
                                        rateraigarhal.add(ratelumpsraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }

                                    String FinesRaigarhJSONstr = SpongeIronRaigarhJson.getString("Fines");
                                    System.out.println("RAIGARH SPONGE IRON FINES JSON IS---" + FinesRaigarhJSONstr);

                                    if(!FinesRaigarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesRaigarhJSON = new JSONObject(FinesRaigarhJSONstr);

                                        String categoryfinesraigarhstr = FinesRaigarhJSON.getString("category");
                                        String subcategoryfinesraigarhstr = FinesRaigarhJSON.getString("sub_category");
                                        String ratefinesraigarhstr = FinesRaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categoryfinesraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 4---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategoryfinesraigarhstr);
                                        rateraigarhal.add(ratefinesraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }

                                    String PelletRaigarhJSONstr = SpongeIronRaigarhJson.getString("Pellet");
                                    System.out.println("RAIGARH SPONGE IRON PELLET JSON IS---" + PelletRaigarhJSONstr);

                                    if(!PelletRaigarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject PelletRaigarhJSON = new JSONObject(PelletRaigarhJSONstr);

                                        String categorypelletraigarhstr = PelletRaigarhJSON.getString("category");
                                        String subcategorypelletraigarhstr = PelletRaigarhJSON.getString("sub_category");
                                        String ratepelletraigarhstr = PelletRaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categorypelletraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 5---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategorypelletraigarhstr);
                                        rateraigarhal.add(ratepelletraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }


                                    String Mix020RaigarhJSONstr = SpongeIronRaigarhJson.getString("Mix 0-20");
                                    System.out.println("RAIGARH SPONGE IRON MIX 0-20 JSON IS---" + Mix020RaigarhJSONstr);

                                    if(!Mix020RaigarhJSONstr.equalsIgnoreCase("")) {
                                        JSONObject Mix020RaigarhJSON = new JSONObject(Mix020RaigarhJSONstr);

                                        String categorymix020raigarhstr = Mix020RaigarhJSON.getString("category");
                                        String subcategorymix020raigarhstr = Mix020RaigarhJSON.getString("sub_category");
                                        String ratemix020raigarhstr = Mix020RaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categorymix020raigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 6---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategorymix020raigarhstr);
                                        rateraigarhal.add(ratemix020raigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }


                                }


                                String SpongeIronDRCLORaigarhJsonStr = RaigarhJson.getString("Sponge Iron DR-CLO");
                                System.out.println("RAIGARH SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLORaigarhJsonStr);


                                if(SpongeIronDRCLORaigarhJsonStr.equalsIgnoreCase(""))
                                {


                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLORaigarhJson = new JSONObject(SpongeIronDRCLORaigarhJsonStr);
                                    System.out.println("RAIGARH SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLORaigarhJson);


                                    String LumpsDRCLORaigarhJSONstr = SpongeIronDRCLORaigarhJson.getString("Lumps DR-CLO");
                                    System.out.println("RAIGARH SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLORaigarhJSONstr);

                                    if(!LumpsDRCLORaigarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLORaigarhJSON = new JSONObject(LumpsDRCLORaigarhJSONstr);

                                        System.out.println("RAIGARH SPONGE IRON LUMPS DR CLO JSON IS 2---" + LumpsDRCLORaigarhJSON);

                                        String categorydrcloraigarhstr = LumpsDRCLORaigarhJSON.getString("category");
                                        String subcategorydrcloraigarhstr = LumpsDRCLORaigarhJSON.getString("sub_category");
                                        String ratedrcloraigarhstr = LumpsDRCLORaigarhJSON.getString("rate");


                                        categoryraigarhal.add(categorydrcloraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 7---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategorydrcloraigarhstr);
                                        rateraigarhal.add(ratedrcloraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }

                                    String FinesDRCLORaigarhJSONstr = SpongeIronDRCLORaigarhJson.getString("Fines DR-CLO");
                                    System.out.println("RAIGARH SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLORaigarhJSONstr);

                                    if(!FinesDRCLORaigarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLORaigarhJSON = new JSONObject(FinesDRCLORaigarhJSONstr);

                                        String categoryfinesdrcloraigarhstr = FinesDRCLORaigarhJSON.getString("category");
                                        String subcategoryfinesdrcloraigarhstr = FinesDRCLORaigarhJSON.getString("sub_category");
                                        String ratefinesdrcloraigarhstr = FinesDRCLORaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categoryfinesdrcloraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 8---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategoryfinesdrcloraigarhstr);
                                        rateraigarhal.add(ratefinesdrcloraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }

                                    String Mix020DRCLORaigarhJSONstr = SpongeIronDRCLORaigarhJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("RAIGARH SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLORaigarhJSONstr);

                                    if(!Mix020DRCLORaigarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DRCLORaigarhJSON = new JSONObject(Mix020DRCLORaigarhJSONstr);

                                        String categorymix020drcloraigarhstr = Mix020DRCLORaigarhJSON.getString("category");
                                        String subcategorymix020drcloraigarhstr = Mix020DRCLORaigarhJSON.getString("sub_category");
                                        String ratemix020drcloraigarhstr = Mix020DRCLORaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categorymix020drcloraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 9---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategorymix020drcloraigarhstr);
                                        rateraigarhal.add(ratemix020drcloraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }




                                }

                                System.out.println("RAIGARH CATEGORY LIST IS---" + categoryraigarhal);
                                System.out.println("RAIGARH SUB CATEGORY LIST IS---" + subcategoryraigarhal);
                                System.out.println("RAIGARH RATE LIST IS---" + rateraigarhal);




                                //For Gobindgarh-----------------------



                                JSONObject GobindgarhJson = LivePriceJson.getJSONObject("Gobindgarh");
                                System.out.println("GOBINDGARH JSON IS---" + GobindgarhJson);


                                String BilletGobindgarhJsonStr = GobindgarhJson.getString("Billet");
                                System.out.println("GOBINDGARH BILLET JSON STRING IS---" + BilletGobindgarhJsonStr);

                                if(BilletGobindgarhJsonStr.equalsIgnoreCase(""))
                                {



                                    categorygobindgarhal.add("-");
                                    subcategorygobindgarhal.add("-");
                                    rategobindgarhal.add("-");

                                }
                                else
                                {
                                    JSONObject BilletGobindgarhJson = new JSONObject(BilletGobindgarhJsonStr);


                                    System.out.println("GOBINDGARH BILLET JSON IS---" + BilletGobindgarhJson);

                                    String categorygobindgarhbilletstr = BilletGobindgarhJson.getString("category");
                                    String subcategorygobindgarhbilletstr = BilletGobindgarhJson.getString("sub_category");
                                    String rategobindgarhbilletstr = BilletGobindgarhJson.getString("rate");

                                    if(subcategorygobindgarhbilletstr.equalsIgnoreCase("")||subcategorygobindgarhbilletstr.equalsIgnoreCase("0"))
                                    {
                                        subcategorygobindgarhal.add("-");
                                    }
                                    else
                                    {
                                        subcategorygobindgarhal.add(subcategorygobindgarhbilletstr);
                                    }

                                    categorygobindgarhal.add(categorygobindgarhbilletstr);

                                    rategobindgarhal.add(rategobindgarhbilletstr);
                                }

                                String MSIngotGobindgarhJsonStr = GobindgarhJson.getString("MS Ingot");
                                System.out.println("GOBINDGARH MS INGOT JSON STRING IS---" + MSIngotGobindgarhJsonStr);


                                if(MSIngotGobindgarhJsonStr.equalsIgnoreCase(""))
                                {
                                    categorygobindgarhal.add("-");
                                    subcategorygobindgarhal.add("-");
                                    rategobindgarhal.add("-");
                                }
                                else
                                {
                                    JSONObject MSIngotGobindgarhJson = new JSONObject(MSIngotGobindgarhJsonStr);
                                    System.out.println("GOBINDGARH MS INGOT JSON IS---" + MSIngotGobindgarhJson);

                                    String categorygobindgarhmsingotstr = MSIngotGobindgarhJson.getString("category");
                                    String subcategorygobindgarhmsingotstr = MSIngotGobindgarhJson.getString("sub_category");
                                    String rategobindgarhmsingotstr = MSIngotGobindgarhJson.getString("rate");

                                    if(subcategorygobindgarhmsingotstr.equalsIgnoreCase("")||subcategorygobindgarhmsingotstr.equalsIgnoreCase("0"))
                                    {
                                        subcategorygobindgarhal.add("-");
                                    }
                                    else
                                    {
                                        subcategorygobindgarhal.add(subcategorygobindgarhmsingotstr);
                                    }

                                    categorygobindgarhal.add(categorygobindgarhmsingotstr);

                                    rategobindgarhal.add(rategobindgarhmsingotstr);

                                }


                                String SpongeIronGobindgarhJsonStr = GobindgarhJson.getString("Sponge Iron");
                                System.out.println("GOBINDGARH SPONGE IRON JSON STRING IS---" + SpongeIronGobindgarhJsonStr);


                                if(SpongeIronGobindgarhJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronGobindgarhJson = new JSONObject(SpongeIronGobindgarhJsonStr);
                                    System.out.println("GOBINDGARH SPONGE IRON JSON IS---" + SpongeIronGobindgarhJson);

                                    String lumpsgobindgarhjsonstr = SpongeIronGobindgarhJson.getString("Lumps");

                                    if(!lumpsgobindgarhjsonstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsGobindgarhJSON = new JSONObject(lumpsgobindgarhjsonstr);
                                        System.out.println("GOBINDGARH SPONGE IRON LUMPS JSON IS---" + LumpsGobindgarhJSON);

                                        String categorylumpsgobindgarhstr = LumpsGobindgarhJSON.getString("category");
                                        String subcategorylumpsgobindgarhstr = LumpsGobindgarhJSON.getString("sub_category");
                                        String ratelumpsgobindgarhstr = LumpsGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categorylumpsgobindgarhstr);
                                        subcategorygobindgarhal.add(subcategorylumpsgobindgarhstr);
                                        rategobindgarhal.add(ratelumpsgobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }

                                    String FinesGobindgarhJSONstr = SpongeIronGobindgarhJson.getString("Fines");
                                    System.out.println("GOBINDGARH SPONGE IRON FINES JSON IS---" + FinesGobindgarhJSONstr);

                                    if(!FinesGobindgarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesGobindgarhJSON = new JSONObject(FinesGobindgarhJSONstr);

                                        String categoryfinesgobindgarhstr = FinesGobindgarhJSON.getString("category");
                                        String subcategoryfinesgobindgarhstr = FinesGobindgarhJSON.getString("sub_category");
                                        String ratefinesgobindgarhstr = FinesGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categoryfinesgobindgarhstr);
                                        subcategorygobindgarhal.add(subcategoryfinesgobindgarhstr);
                                        rategobindgarhal.add(ratefinesgobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }

                                    String PelletGobindgarhJSONstr = SpongeIronGobindgarhJson.getString("Pellet");
                                    System.out.println("GOBINDGARH SPONGE IRON PELLET JSON IS---" + PelletGobindgarhJSONstr);

                                    if(!PelletGobindgarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject PelletGobindgarhJSON = new JSONObject(PelletGobindgarhJSONstr);

                                        String categorypelletgobindgarhstr = PelletGobindgarhJSON.getString("category");
                                        String subcategorypelletgobindgarhstr = PelletGobindgarhJSON.getString("sub_category");
                                        String ratepelletgobindgarhstr = PelletGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categorypelletgobindgarhstr);
                                        subcategorygobindgarhal.add(subcategorypelletgobindgarhstr);
                                        rategobindgarhal.add(ratepelletgobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }


                                    String Mix020GobindgarhJSONstr = SpongeIronGobindgarhJson.getString("Mix 0-20");
                                    System.out.println("GOBINDGARH SPONGE IRON MIX 0-20 JSON IS---" + Mix020GobindgarhJSONstr);

                                    if(!Mix020GobindgarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020GobindgarhJSON = new JSONObject(Mix020GobindgarhJSONstr);

                                        String categorymix020gobindgarhstr = Mix020GobindgarhJSON.getString("category");
                                        String subcategorymix020gobindgarhstr = Mix020GobindgarhJSON.getString("sub_category");
                                        String ratemix020gobindgarhstr = Mix020GobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categorymix020gobindgarhstr);
                                        subcategorygobindgarhal.add(subcategorymix020gobindgarhstr);
                                        rategobindgarhal.add(ratemix020gobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }


                                }


                                String SpongeIronDRCLOGobindgarhJsonStr = GobindgarhJson.getString("Sponge Iron DR-CLO");
                                System.out.println("GOBINDGARH SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLOGobindgarhJsonStr);


                                if(SpongeIronDRCLOGobindgarhJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLOGobindgarhJson = new JSONObject(SpongeIronDRCLOGobindgarhJsonStr);
                                    System.out.println("GOBINDGARH SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLOGobindgarhJson);


                                    String LumpsDRCLOGobindgarhJSONstr = SpongeIronDRCLOGobindgarhJson.getString("Lumps DR-CLO");
                                    System.out.println("GOBINDGARH SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLOGobindgarhJSONstr);

                                    if(!LumpsDRCLOGobindgarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLOGobindgarhJSON = new JSONObject(LumpsDRCLOGobindgarhJSONstr);

                                        String categorydrclogobindgarhstr = LumpsDRCLOGobindgarhJSON.getString("category");
                                        String subcategorydrclogobindgarhstr = LumpsDRCLOGobindgarhJSON.getString("sub_category");
                                        String ratedrclogobindgarhstr = LumpsDRCLOGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categorydrclogobindgarhstr);
                                        subcategorygobindgarhal.add(subcategorydrclogobindgarhstr);
                                        rategobindgarhal.add(ratedrclogobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }

                                    String FinesDRCLOGobindgarhJSONstr = SpongeIronDRCLOGobindgarhJson.getString("Fines DR-CLO");
                                    System.out.println("GOBINDGARH SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLOGobindgarhJSONstr);

                                    if(!FinesDRCLOGobindgarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLOGobindgarhJSON = new JSONObject(FinesDRCLOGobindgarhJSONstr);

                                        String categoryfinesdrclogobindgarhstr = FinesDRCLOGobindgarhJSON.getString("category");
                                        String subcategoryfinesdrclogobindgarhstr = FinesDRCLOGobindgarhJSON.getString("sub_category");
                                        String ratefinesdrclogobindgarhstr = FinesDRCLOGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categoryfinesdrclogobindgarhstr);
                                        subcategorygobindgarhal.add(subcategoryfinesdrclogobindgarhstr);
                                        rategobindgarhal.add(ratefinesdrclogobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }

                                    String Mix020DRCLOGobindgarhJSONstr = SpongeIronDRCLOGobindgarhJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("GOBINDGARH SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLOGobindgarhJSONstr);

                                    if(!Mix020DRCLOGobindgarhJSONstr.equalsIgnoreCase(""))
                                    {
                                        JSONObject Mix020DRCLOGobindgarhJSON = new JSONObject(Mix020DRCLOGobindgarhJSONstr);

                                        String categorymix020drclogobindgarhstr = Mix020DRCLOGobindgarhJSON.getString("category");
                                        String subcategorymix020drclogobindgarhstr = Mix020DRCLOGobindgarhJSON.getString("sub_category");
                                        String ratemix020drclogobindgarhstr = Mix020DRCLOGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categorymix020drclogobindgarhstr);
                                        subcategorygobindgarhal.add(subcategorymix020drclogobindgarhstr);
                                        rategobindgarhal.add(ratemix020drclogobindgarhstr);

                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }


                                }

                                System.out.println("GOBINDGARH CATEGORY LIST IS---" + categorygobindgarhal);
                                System.out.println("GOBINDGARH SUB CATEGORY LIST IS---" + subcategorygobindgarhal);
                                System.out.println("GOBINDGARH RATE LIST IS---" + rategobindgarhal);




                                //For Mumbai---------------


                                JSONObject MumbaiJson = LivePriceJson.getJSONObject("Mumbai");
                                System.out.println("MUMBAI JSON IS---" + MumbaiJson);


                                String BilletMumbaiJsonStr = MumbaiJson.getString("Billet");
                                System.out.println("MUMBAI BILLET JSON STRING IS---" + BilletMumbaiJsonStr);

                                if(BilletMumbaiJsonStr.equalsIgnoreCase(""))
                                {

                                    categorymumbaial.add("-");
                                    subcategorymumbaial.add("-");
                                    ratemumbaial.add("-");

                                }
                                else
                                {
                                    JSONObject BilletMumbaiJson = new JSONObject(BilletMumbaiJsonStr);


                                    System.out.println("MUMBAI BILLET JSON IS---" + BilletMumbaiJson);

                                    String categorymumbaibilletstr = BilletMumbaiJson.getString("category");
                                    String subcategorymumbaibilletstr = BilletMumbaiJson.getString("sub_category");
                                    String ratemumbaibilletstr = BilletMumbaiJson.getString("rate");

                                    if(subcategorymumbaibilletstr.equalsIgnoreCase("")||subcategorymumbaibilletstr.equalsIgnoreCase("0"))
                                    {
                                        subcategorymumbaial.add("-");
                                    }
                                    else
                                    {
                                        subcategorymumbaial.add(subcategorymumbaibilletstr);
                                    }

                                    categorymumbaial.add(categorymumbaibilletstr);

                                    ratemumbaial.add(ratemumbaibilletstr);
                                }

                                String MSIngotMumbaiJsonStr = MumbaiJson.getString("MS Ingot");
                                System.out.println("MUMBAI MS INGOT JSON STRING IS---" + MSIngotMumbaiJsonStr);


                                if(MSIngotMumbaiJsonStr.equalsIgnoreCase(""))
                                {
                                    categorymumbaial.add("-");
                                    subcategorymumbaial.add("-");
                                    ratemumbaial.add("-");

                                }
                                else
                                {
                                    JSONObject MSIngotMumbaiJson = new JSONObject(MSIngotMumbaiJsonStr);
                                    System.out.println("MUMBAI MS INGOT JSON IS---" + MSIngotMumbaiJson);

                                    String categorymumbaimsingotstr = MSIngotMumbaiJson.getString("category");
                                    String subcategorymumbaimsingotstr = MSIngotMumbaiJson.getString("sub_category");
                                    String ratemumbaimsingotstr = MSIngotMumbaiJson.getString("rate");

                                    if(subcategorymumbaimsingotstr.equalsIgnoreCase("")||subcategorymumbaimsingotstr.equalsIgnoreCase("0"))
                                    {
                                        subcategorymumbaial.add("-");
                                    }
                                    else
                                    {
                                        subcategorymumbaial.add(subcategorymumbaimsingotstr);
                                    }

                                    categorymumbaial.add(categorymumbaimsingotstr);

                                    ratemumbaial.add(ratemumbaimsingotstr);

                                }


                                String SpongeIronMumbaiJsonStr = MumbaiJson.getString("Sponge Iron");
                                System.out.println("MUMBAI SPONGE IRON JSON STRING IS---" + SpongeIronMumbaiJsonStr);


                                if(SpongeIronMumbaiJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronMumbaiJson = new JSONObject(SpongeIronMumbaiJsonStr);
                                    System.out.println("MUMBAI SPONGE IRON JSON IS---" + SpongeIronMumbaiJson);

                                    String lumpsmumbaijsonstr = SpongeIronMumbaiJson.getString("Lumps");

                                    if(!lumpsmumbaijsonstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsMumbaiJSON = new JSONObject(lumpsmumbaijsonstr);
                                        System.out.println("MUMBAI SPONGE IRON LUMPS JSON IS---" + LumpsMumbaiJSON);

                                        String categorylumpsmumbaistr = LumpsMumbaiJSON.getString("category");
                                        String subcategorylumpsmumbaistr = LumpsMumbaiJSON.getString("sub_category");
                                        String ratelumpsmumbaistr = LumpsMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categorylumpsmumbaistr);
                                        subcategorymumbaial.add(subcategorylumpsmumbaistr);
                                        ratemumbaial.add(ratelumpsmumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }

                                    String FinesMumbaiJSONstr = SpongeIronMumbaiJson.getString("Fines");
                                    System.out.println("MUMBAI SPONGE IRON FINES JSON IS---" + FinesMumbaiJSONstr);

                                    if(!FinesMumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesMumbaiJSON = new JSONObject(FinesMumbaiJSONstr);
                                        String categoryfinesmumbaistr = FinesMumbaiJSON.getString("category");
                                        String subcategoryfinesmumbaistr = FinesMumbaiJSON.getString("sub_category");
                                        String ratefinesmumbaistr = FinesMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categoryfinesmumbaistr);
                                        subcategorymumbaial.add(subcategoryfinesmumbaistr);
                                        ratemumbaial.add(ratefinesmumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }

                                    String PelletMumbaiJSONstr = SpongeIronMumbaiJson.getString("Pellet");
                                    System.out.println("MUMBAI SPONGE IRON PELLET JSON IS---" + PelletMumbaiJSONstr);

                                    if(!PelletMumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject PelletMumbaiJSON = new JSONObject(PelletMumbaiJSONstr);

                                        String categorypelletmumbaistr = PelletMumbaiJSON.getString("category");
                                        String subcategorypelletmumbaistr = PelletMumbaiJSON.getString("sub_category");
                                        String ratepelletmumbaistr = PelletMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categorypelletmumbaistr);
                                        subcategorymumbaial.add(subcategorypelletmumbaistr);
                                        ratemumbaial.add(ratepelletmumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }


                                    String Mix020MumbaiJSONstr = SpongeIronMumbaiJson.getString("Mix 0-20");
                                    System.out.println("MUMBAI SPONGE IRON MIX 0-20 JSON IS---" + Mix020MumbaiJSONstr);

                                    if(!Mix020MumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020MumbaiJSON = new JSONObject(Mix020MumbaiJSONstr);

                                        String categorymix020mumbaistr = Mix020MumbaiJSON.getString("category");
                                        String subcategorymix020mumbaistr = Mix020MumbaiJSON.getString("sub_category");
                                        String ratemix020mumbaistr = Mix020MumbaiJSON.getString("rate");

                                        categorymumbaial.add(categorymix020mumbaistr);
                                        subcategorymumbaial.add(subcategorymix020mumbaistr);
                                        ratemumbaial.add(ratemix020mumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }


                                }


                                String SpongeIronDRCLOMumbaiJsonStr = MumbaiJson.getString("Sponge Iron DR-CLO");
                                System.out.println("MUMBAI SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLOMumbaiJsonStr);


                                if(SpongeIronDRCLOMumbaiJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLOMumbaiJson = new JSONObject(SpongeIronDRCLOMumbaiJsonStr);
                                    System.out.println("MUMBAI SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLOMumbaiJson);


                                    String LumpsDRCLOMumbaiJSONstr = SpongeIronDRCLOMumbaiJson.getString("Lumps DR-CLO");
                                    System.out.println("MUMBAI SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLOMumbaiJSONstr);

                                    if(!LumpsDRCLOMumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLOMumbaiJSON = new JSONObject(LumpsDRCLOMumbaiJSONstr);

                                        String categorydrclomumbaistr = LumpsDRCLOMumbaiJSON.getString("category");
                                        String subcategorydrclomumbaistr = LumpsDRCLOMumbaiJSON.getString("sub_category");
                                        String ratedrclomumbaistr = LumpsDRCLOMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categorydrclomumbaistr);
                                        subcategorymumbaial.add(subcategorydrclomumbaistr);
                                        ratemumbaial.add(ratedrclomumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }

                                    String FinesDRCLOMumbaiJSONstr = SpongeIronDRCLOMumbaiJson.getString("Fines DR-CLO");
                                    System.out.println("MUMBAI SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLOMumbaiJSONstr);

                                    if(!FinesDRCLOMumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLOMumbaiJSON = new JSONObject(FinesDRCLOMumbaiJSONstr);

                                        String categoryfinesdrclomumbaistr = FinesDRCLOMumbaiJSON.getString("category");
                                        String subcategoryfinesdrclomumbaistr = FinesDRCLOMumbaiJSON.getString("sub_category");
                                        String ratefinesdrclomumbaistr = FinesDRCLOMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categoryfinesdrclomumbaistr);
                                        subcategorymumbaial.add(subcategoryfinesdrclomumbaistr);
                                        ratemumbaial.add(ratefinesdrclomumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }

                                    String Mix020DRCLOMumbaiJSONstr = SpongeIronDRCLOMumbaiJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("MUMBAI SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLOMumbaiJSONstr);

                                    if(!Mix020DRCLOMumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DRCLOMumbaiJSON = new JSONObject(Mix020DRCLOMumbaiJSONstr);

                                        String categorymix020drclomumbaistr = Mix020DRCLOMumbaiJSON.getString("category");
                                        String subcategorymix020drclomumbaistr = Mix020DRCLOMumbaiJSON.getString("sub_category");
                                        String ratemix020drclomumbaistr = Mix020DRCLOMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categorymix020drclomumbaistr);
                                        subcategorymumbaial.add(subcategorymix020drclomumbaistr);
                                        ratemumbaial.add(ratemix020drclomumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }




                                }

                                System.out.println("MUMBAI CATEGORY LIST IS---" + categorymumbaial);
                                System.out.println("MUMBAI SUB CATEGORY LIST IS---" + subcategorymumbaial);
                                System.out.println("MUMBAI RATE LIST IS---" + ratemumbaial);

                                //-------END OF MUMBAI DATA---------------------------------------------


                                //For Durgapur---------------


                                JSONObject DurgapurJson = LivePriceJson.getJSONObject("Durgapur");
                                System.out.println("DURGAPUR JSON IS---" + DurgapurJson);


                                String BilletDurgapurJsonStr = DurgapurJson.getString("Billet");
                                System.out.println("DURGAPUR BILLET JSON STRING IS---" + BilletDurgapurJsonStr);

                                if(BilletDurgapurJsonStr.equalsIgnoreCase(""))
                                {


                                    categorydurgapural.add("-");
                                    subcategorydurgapural.add("-");
                                    ratedurgapural.add("-");

                                }
                                else
                                {
                                    JSONObject BilletDurgapurJson = new JSONObject(BilletDurgapurJsonStr);


                                    System.out.println("DURGAPUR BILLET JSON IS---" + BilletDurgapurJson);

                                    String categorydurgapurbilletstr = BilletDurgapurJson.getString("category");
                                    String subcategorydurgapurbilletstr = BilletDurgapurJson.getString("sub_category");
                                    String ratedurgapurbilletstr = BilletDurgapurJson.getString("rate");

                                    if(subcategorydurgapurbilletstr.equalsIgnoreCase("")||subcategorydurgapurbilletstr.equalsIgnoreCase(""))
                                    {
                                        subcategorydurgapural.add("-");
                                    }
                                    else
                                    {
                                        subcategorydurgapural.add(subcategorydurgapurbilletstr);
                                    }

                                    categorydurgapural.add(categorydurgapurbilletstr);

                                    ratedurgapural.add(ratedurgapurbilletstr);
                                }

                                String MSIngotDurgapurJsonStr = DurgapurJson.getString("MS Ingot");
                                System.out.println("DURGAPUR MS INGOT JSON STRING IS---" + MSIngotDurgapurJsonStr);


                                if(MSIngotDurgapurJsonStr.equalsIgnoreCase(""))
                                {
                                    categorydurgapural.add("-");
                                    subcategorydurgapural.add("-");
                                    ratedurgapural.add("-");
                                }
                                else
                                {
                                    JSONObject MSIngotDurgapurJson = new JSONObject(MSIngotDurgapurJsonStr);
                                    System.out.println("DURGAPUR MS INGOT JSON IS---" + MSIngotDurgapurJson);

                                    String categorydurgapurmsingotstr = MSIngotDurgapurJson.getString("category");
                                    String subcategorydurgapurmsingotstr = MSIngotDurgapurJson.getString("sub_category");
                                    String ratedurgapurmsingotstr = MSIngotDurgapurJson.getString("rate");

                                    if(subcategorydurgapurmsingotstr.equalsIgnoreCase("")||subcategorydurgapurmsingotstr.equalsIgnoreCase(""))
                                    {
                                        subcategorydurgapural.add("-");
                                    }
                                    else
                                    {
                                        subcategorydurgapural.add(subcategorydurgapurmsingotstr);
                                    }

                                    categorydurgapural.add(categorydurgapurmsingotstr);

                                    ratedurgapural.add(ratedurgapurmsingotstr);

                                }


                                String SpongeIronDurgapurJsonStr = DurgapurJson.getString("Sponge Iron");
                                System.out.println("DURGAPUR SPONGE IRON JSON STRING IS---" + SpongeIronDurgapurJsonStr);


                                if(SpongeIronDurgapurJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDurgapurJson = new JSONObject(SpongeIronDurgapurJsonStr);
                                    System.out.println("DURGAPUR SPONGE IRON JSON IS---" + SpongeIronDurgapurJson);

                                    String lumpsdurgapurjsonstr = SpongeIronDurgapurJson.getString("Lumps");

                                    if(!lumpsdurgapurjsonstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDurgapurJSON = new JSONObject(lumpsdurgapurjsonstr);
                                        System.out.println("DURGAPUR SPONGE IRON LUMPS JSON IS---" + LumpsDurgapurJSON);

                                        String categorylumpsdurgapurstr = LumpsDurgapurJSON.getString("category");
                                        String subcategorylumpsdurgapurstr = LumpsDurgapurJSON.getString("sub_category");
                                        String ratelumpsdurgapurstr = LumpsDurgapurJSON.getString("rate");

                                        categorydurgapural.add(categorylumpsdurgapurstr);
                                        subcategorydurgapural.add(subcategorylumpsdurgapurstr);
                                        ratedurgapural.add(ratelumpsdurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }

                                    String FinesDurgapurJSONstr = SpongeIronDurgapurJson.getString("Fines");
                                    System.out.println("DURGAPUR SPONGE IRON FINES JSON IS---" + FinesDurgapurJSONstr);

                                    if(!FinesDurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDurgapurJSON = new JSONObject(FinesDurgapurJSONstr);

                                        String categoryfinesdurgapurstr = FinesDurgapurJSON.getString("category");
                                        String subcategoryfinesdurgapurstr = FinesDurgapurJSON.getString("sub_category");
                                        String ratefinesdurgapurstr = FinesDurgapurJSON.getString("rate");

                                        categorydurgapural.add(categoryfinesdurgapurstr);
                                        subcategorydurgapural.add(subcategoryfinesdurgapurstr);
                                        ratedurgapural.add(ratefinesdurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }

                                    String PelletDurgapurJSONstr = SpongeIronDurgapurJson.getString("Pellet");
                                    System.out.println("DURGAPUR SPONGE IRON PELLET JSON IS---" + PelletDurgapurJSONstr);

                                    if(!PelletDurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject PelletDurgapurJSON = new JSONObject(PelletDurgapurJSONstr);


                                        String categorypelletdurgapurstr = PelletDurgapurJSON.getString("category");
                                        String subcategorypelletdurgapurstr = PelletDurgapurJSON.getString("sub_category");
                                        String ratepelletdurgapurstr = PelletDurgapurJSON.getString("rate");

                                        categorydurgapural.add(categorypelletdurgapurstr);
                                        subcategorydurgapural.add(subcategorypelletdurgapurstr);
                                        ratedurgapural.add(ratepelletdurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }

                                    String Mix020DurgapurJSONstr = SpongeIronDurgapurJson.getString("Mix 0-20");
                                    System.out.println("DURGAPUR SPONGE IRON MIX 0-20 JSON IS---" + Mix020DurgapurJSONstr);

                                    if(!Mix020DurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DurgapurJSON = new JSONObject(Mix020DurgapurJSONstr);

                                        String categorymix020durgapurstr = Mix020DurgapurJSON.getString("category");
                                        String subcategorymix020durgapurstr = Mix020DurgapurJSON.getString("sub_category");
                                        String ratemix020durgapurstr = Mix020DurgapurJSON.getString("rate");

                                        categorydurgapural.add(categorymix020durgapurstr);
                                        subcategorydurgapural.add(subcategorymix020durgapurstr);
                                        ratedurgapural.add(ratemix020durgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }


                                }


                                String SpongeIronDRCLODurgapurJsonStr = DurgapurJson.getString("Sponge Iron DR-CLO");
                                System.out.println("DURGAPUR SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLODurgapurJsonStr);


                                if(SpongeIronDRCLODurgapurJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLODurgapurJson = new JSONObject(SpongeIronDRCLODurgapurJsonStr);
                                    System.out.println("DURGAPUR SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLODurgapurJson);


                                    String LumpsDRCLODurgapurJSONstr = SpongeIronDRCLODurgapurJson.getString("Lumps DR-CLO");
                                    System.out.println("DURGAPUR SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLODurgapurJSONstr);

                                    if(!LumpsDRCLODurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLODurgapurJSON = new JSONObject(LumpsDRCLODurgapurJSONstr);

                                        String categorydrclodurgapurstr = LumpsDRCLODurgapurJSON.getString("category");
                                        String subcategorydrclodurgapurstr = LumpsDRCLODurgapurJSON.getString("sub_category");
                                        String ratedrclodurgapurstr = LumpsDRCLODurgapurJSON.getString("rate");

                                        categorydurgapural.add(categorydrclodurgapurstr);
                                        subcategorydurgapural.add(subcategorydrclodurgapurstr);
                                        ratedurgapural.add(ratedrclodurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }

                                    String FinesDRCLODurgapurJSONstr = SpongeIronDRCLODurgapurJson.getString("Fines DR-CLO");
                                    System.out.println("DURGAPUR SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLODurgapurJSONstr);

                                    if(!FinesDRCLODurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLODurgapurJSON = new JSONObject(FinesDRCLODurgapurJSONstr);

                                        String categoryfinesdrclodurgapurstr = FinesDRCLODurgapurJSON.getString("category");
                                        String subcategoryfinesdrclodurgapurstr = FinesDRCLODurgapurJSON.getString("sub_category");
                                        String ratefinesdrclodurgapurstr = FinesDRCLODurgapurJSON.getString("rate");

                                        categorydurgapural.add(categoryfinesdrclodurgapurstr);
                                        subcategorydurgapural.add(subcategoryfinesdrclodurgapurstr);
                                        ratedurgapural.add(ratefinesdrclodurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }
                                    String Mix020DRCLODurgapurJSONstr = SpongeIronDRCLODurgapurJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("DURGAPUR SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLODurgapurJSONstr);

                                    if(!Mix020DRCLODurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DRCLODurgapurJSON = new JSONObject(Mix020DRCLODurgapurJSONstr);

                                        String categorymix020drclodurgapurstr = Mix020DRCLODurgapurJSON.getString("category");
                                        String subcategorymix020drclodurgapurstr = Mix020DRCLODurgapurJSON.getString("sub_category");
                                        String ratemix020drclodurgapurstr = Mix020DRCLODurgapurJSON.getString("rate");

                                        categorydurgapural.add(categorymix020drclodurgapurstr);
                                        subcategorydurgapural.add(subcategorymix020drclodurgapurstr);
                                        ratedurgapural.add(ratemix020drclodurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }



                                }

                                System.out.println("DURGAPUR CATEGORY LIST IS---" + categorydurgapural);
                                System.out.println("DURGAPUR SUB CATEGORY LIST IS---" + subcategorydurgapural);
                                System.out.println("DURGAPUR RATE LIST IS---" + ratedurgapural);



                                //-------END OF DURGAPUR DATA---------------------------------------------


                                //For Hyderabad---------------


                                JSONObject HyderabadJson = LivePriceJson.getJSONObject("Hyderabad");
                                System.out.println("HYDERABAD JSON IS---" + HyderabadJson);


                                String BilletHyderabadJsonStr = HyderabadJson.getString("Billet");
                                System.out.println("HYDERABAD BILLET JSON STRING IS---" + BilletHyderabadJsonStr);

                                if(BilletHyderabadJsonStr.equalsIgnoreCase(""))
                                {

                                    categoryhyderabadal.add("-");
                                    subcategoryhyderabadal.add("-");
                                    ratehyderabadal.add("-");

                                }
                                else
                                {
                                    JSONObject BilletHyderabadJson = new JSONObject(BilletHyderabadJsonStr);


                                    System.out.println("HYDERABAD BILLET JSON IS---" + BilletHyderabadJson);

                                    String categoryhyderabadbilletstr = BilletHyderabadJson.getString("category");
                                    String subcategoryhyderabadbilletstr = BilletHyderabadJson.getString("sub_category");
                                    String ratehyderabadbilletstr = BilletHyderabadJson.getString("rate");

                                    if(subcategoryhyderabadbilletstr.equalsIgnoreCase("")||subcategoryhyderabadbilletstr.equalsIgnoreCase("0"))
                                    {
                                        subcategoryhyderabadal.add("-");
                                    }
                                    else
                                    {
                                        subcategoryhyderabadal.add(subcategoryhyderabadbilletstr);
                                    }

                                    categoryhyderabadal.add(categoryhyderabadbilletstr);

                                    ratehyderabadal.add(ratehyderabadbilletstr);
                                }

                                String MSIngotHyderabadJsonStr = HyderabadJson.getString("MS Ingot");
                                System.out.println("HYDERABAD MS INGOT JSON STRING IS---" + MSIngotHyderabadJsonStr);


                                if(MSIngotHyderabadJsonStr.equalsIgnoreCase(""))
                                {
                                    categoryhyderabadal.add("-");
                                    subcategoryhyderabadal.add("-");
                                    ratehyderabadal.add("-");
                                }
                                else
                                {
                                    JSONObject MSIngotHyderabadJson = new JSONObject(MSIngotHyderabadJsonStr);
                                    System.out.println("HYDERABAD MS INGOT JSON IS---" + MSIngotHyderabadJson);

                                    String categoryhyderabadmsingotstr = MSIngotHyderabadJson.getString("category");
                                    String subcategoryhyderabadmsingotstr = MSIngotHyderabadJson.getString("sub_category");
                                    String ratehyderabadmsingotstr = MSIngotHyderabadJson.getString("rate");

                                    if(subcategoryhyderabadmsingotstr.equalsIgnoreCase("")||subcategoryhyderabadmsingotstr.equalsIgnoreCase("0"))
                                    {
                                        subcategoryhyderabadal.add("-");
                                    }
                                    else
                                    {
                                        subcategoryhyderabadal.add(subcategoryhyderabadmsingotstr);
                                    }

                                    categoryhyderabadal.add(categoryhyderabadmsingotstr);
                                    // subcategoryhyderabadal.add(subcategoryhyderabadmsingotstr);
                                    ratehyderabadal.add(ratehyderabadmsingotstr);

                                }


                                String SpongeIronHyderabadJsonStr = HyderabadJson.getString("Sponge Iron");
                                System.out.println("HYDERABAD SPONGE IRON JSON STRING IS---" + SpongeIronHyderabadJsonStr);


                                if(SpongeIronHyderabadJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronHyderabadJson = new JSONObject(SpongeIronHyderabadJsonStr);
                                    System.out.println("HYDERABAD SPONGE IRON JSON IS---" + SpongeIronHyderabadJson);

                                    String lumpshyderabadjsonstr = SpongeIronHyderabadJson.getString("Lumps");

                                    if(!lumpshyderabadjsonstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsHyderabadJSON = new JSONObject(lumpshyderabadjsonstr);
                                        System.out.println("HYDERABAD SPONGE IRON LUMPS JSON IS---" + LumpsHyderabadJSON);

                                        String categorylumpshyderabadstr = LumpsHyderabadJSON.getString("category");
                                        String subcategorylumpshyderabadstr = LumpsHyderabadJSON.getString("sub_category");
                                        String ratelumpshyderabadstr = LumpsHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categorylumpshyderabadstr);
                                        subcategoryhyderabadal.add(subcategorylumpshyderabadstr);
                                        ratehyderabadal.add(ratelumpshyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }

                                    String FinesHyderabadJSONstr = SpongeIronHyderabadJson.getString("Fines");
                                    System.out.println("HYDERABAD SPONGE IRON FINES JSON IS---" + FinesHyderabadJSONstr);

                                    if(!FinesHyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesHyderabadJSON = new JSONObject(FinesHyderabadJSONstr);

                                        String categoryfineshyderabadstr = FinesHyderabadJSON.getString("category");
                                        String subcategoryfineshyderabadstr = FinesHyderabadJSON.getString("sub_category");
                                        String ratefineshdyerabadstr = FinesHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categoryfineshyderabadstr);
                                        subcategoryhyderabadal.add(subcategoryfineshyderabadstr);
                                        ratehyderabadal.add(ratefineshdyerabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }

                                    String PelletHyderabadJSONstr = SpongeIronHyderabadJson.getString("Pellet");
                                    System.out.println("HYDERABAD SPONGE IRON PELLET JSON IS---" + PelletHyderabadJSONstr);

                                    if(!PelletHyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject PelletHyderabadJSON = new JSONObject(PelletHyderabadJSONstr);

                                        String categorypellethyderabadstr = PelletHyderabadJSON.getString("category");
                                        String subcategorypellethyderabadstr = PelletHyderabadJSON.getString("sub_category");
                                        String ratepellethyderabadstr = PelletHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categorypellethyderabadstr);
                                        subcategoryhyderabadal.add(subcategorypellethyderabadstr);
                                        ratehyderabadal.add(ratepellethyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }


                                    String Mix020HyderabadJSONstr = SpongeIronHyderabadJson.getString("Mix 0-20");
                                    System.out.println("HYDERABAD SPONGE IRON MIX 0-20 JSON IS---" + Mix020HyderabadJSONstr);

                                    if(!Mix020HyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020HyderabadJSON = new JSONObject(Mix020HyderabadJSONstr);

                                        String categorymix020hyderabadstr = Mix020HyderabadJSON.getString("category");
                                        String subcategorymix020hyderabadstr = Mix020HyderabadJSON.getString("sub_category");
                                        String ratemix020hyderabadstr = Mix020HyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categorymix020hyderabadstr);
                                        subcategoryhyderabadal.add(subcategorymix020hyderabadstr);
                                        ratehyderabadal.add(ratemix020hyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }


                                }


                                String SpongeIronDRCLOHyderabadJsonStr = HyderabadJson.getString("Sponge Iron DR-CLO");
                                System.out.println("HYDERABAD SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLOHyderabadJsonStr);


                                if(SpongeIronDRCLOHyderabadJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLOHyderabadJson = new JSONObject(SpongeIronDRCLOHyderabadJsonStr);
                                    System.out.println("HYDERABAD SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLOHyderabadJson);


                                    String LumpsDRCLOHyderabadJSONstr = SpongeIronDRCLOHyderabadJson.getString("Lumps DR-CLO");
                                    System.out.println("HYDERABAD SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLOHyderabadJSONstr);

                                    if(!LumpsDRCLOHyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLOHyderabadJSON = new JSONObject(LumpsDRCLOHyderabadJSONstr);

                                        String categorydrclohyderabadstr = LumpsDRCLOHyderabadJSON.getString("category");
                                        String subcategorydrclohyderabadstr = LumpsDRCLOHyderabadJSON.getString("sub_category");
                                        String ratedrclohyderabadstr = LumpsDRCLOHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categorydrclohyderabadstr);
                                        subcategoryhyderabadal.add(subcategorydrclohyderabadstr);
                                        ratehyderabadal.add(ratedrclohyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }

                                    String FinesDRCLOHyderabadJSONstr = SpongeIronDRCLOHyderabadJson.getString("Fines DR-CLO");
                                    System.out.println("HYDERABAD SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLOHyderabadJSONstr);

                                    if(!FinesDRCLOHyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLOHyderabadJSON = new JSONObject(FinesDRCLOHyderabadJSONstr);

                                        String categoryfinesdrclohyderabadstr = FinesDRCLOHyderabadJSON.getString("category");
                                        String subcategoryfinesdrclohyderabadstr = FinesDRCLOHyderabadJSON.getString("sub_category");
                                        String ratefinesdrclohyderabadstr = FinesDRCLOHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categoryfinesdrclohyderabadstr);
                                        subcategoryhyderabadal.add(subcategoryfinesdrclohyderabadstr);
                                        ratehyderabadal.add(ratefinesdrclohyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }

                                    String Mix020DRCLOHyderabadJSONstr = SpongeIronDRCLOHyderabadJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("HYDERABAD SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLOHyderabadJSONstr);

                                    if(!Mix020DRCLOHyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DRCLOHyderabadJSON = new JSONObject(Mix020DRCLOHyderabadJSONstr);

                                        String categorymix020drclohyderabadstr = Mix020DRCLOHyderabadJSON.getString("category");
                                        String subcategorymix020drclohyderabadstr = Mix020DRCLOHyderabadJSON.getString("sub_category");
                                        String ratemix020drclohyderabadstr = Mix020DRCLOHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categorymix020drclohyderabadstr);
                                        subcategoryhyderabadal.add(subcategorymix020drclohyderabadstr);
                                        ratehyderabadal.add(ratemix020drclohyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }




                                }

                                System.out.println("HYDERABAD CATEGORY LIST IS---" + categoryhyderabadal);
                                System.out.println("HYDERABAD SUB CATEGORY LIST IS---" + subcategoryhyderabadal);
                                System.out.println("HYDERABAD RATE LIST IS---" + ratehyderabadal);



                                isAnimationRunning = true;

                                if (getActivity() == null) {
                                   // isAnimationRunning == true
                                    return;
                                }
                                else {


                                    if (isAnimationRunning == true) {

                                        //    if(getActivity()!=null) {
                                        t = new Thread() {

                                            @Override
                                            public void run() {
                                                try {


                                                    while (!isInterrupted()) {

                                                        Thread.sleep(5000);

                                                        if (getActivity() != null) {


                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    // update TextView here!


                                                                    //START RAIPUR ---------------------------

                                                                    System.out.println("raipur boga IS---" + categoryal.get(i));
                                                                    System.out.println("raigarh bugman IS---" + categoryraigarhal);

                                                                    categorytitletxt1.setVisibility(View.VISIBLE);

                                                                    if (categoryal.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt1.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt1.setText("");
                                                                    } else {
                                                                        categorytitletxt1.setText(categoryal.get(i));
                                                                    }

                                                                    subcategorytitletxt1.setVisibility(View.VISIBLE);

                                                                    if (subcategoryal.get(i).equalsIgnoreCase("-")) {
                                                                        subcategorytitletxt1.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt1.setText("");
                                                                    } else {
                                                                        subcategorytitletxt1.setText(subcategoryal.get(i));
                                                                    }

                                                                    pricetxt1.setVisibility(View.VISIBLE);

                                                                    if (rateal.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt1.setVisibility(View.INVISIBLE);
                                                                        pricetxt1.setText("");
                                                                    } else {
                                                                        pricetxt1.setText(rateal.get(i));
                                                                    }


                                                                    //END OF RAIPUR --------------------------


                                                                    //START RAIGARH ---------------------------


                                                                    categorytitletxt2.setVisibility(View.VISIBLE);

                                                                    System.out.println("CATEGORY RAIGARH ALS---==" + categoryraigarhal);

                                                                    if (categoryraigarhal.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt2.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt2.setText("");
                                                                    } else {
                                                                        categorytitletxt2.setText(categoryraigarhal.get(i));
                                                                    }

                                                                    subcategorytitletxt2.setVisibility(View.VISIBLE);

                                                                    if (subcategoryraigarhal.get(i).equalsIgnoreCase("-")) {
                                                                        subcategorytitletxt2.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt2.setText("");
                                                                    } else {
                                                                        subcategorytitletxt2.setText(subcategoryraigarhal.get(i));
                                                                    }

                                                                    pricetxt2.setVisibility(View.VISIBLE);

                                                                    if (rateraigarhal.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt2.setVisibility(View.INVISIBLE);
                                                                        pricetxt2.setText("");
                                                                    } else {
                                                                        pricetxt2.setText(rateraigarhal.get(i));
                                                                    }

                                                                    //END OF RAIGARH ---------------------------


                                                                    //START GOBINDGARH ---------------------------

                                                                    categorytitletxt3.setVisibility(View.VISIBLE);

                                                                    if (categorygobindgarhal.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt3.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt3.setText("");
                                                                    } else {
                                                                        categorytitletxt3.setText(categorygobindgarhal.get(i));
                                                                    }

                                                                    subcategorytitletxt3.setVisibility(View.VISIBLE);

                                                                    if (subcategorygobindgarhal.get(i).equalsIgnoreCase("-")) {

                                                                        subcategorytitletxt3.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt3.setText("");
                                                                    } else {
                                                                        subcategorytitletxt3.setText(subcategorygobindgarhal.get(i));
                                                                    }


                                                                    pricetxt3.setVisibility(View.VISIBLE);

                                                                    if (rategobindgarhal.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt3.setVisibility(View.INVISIBLE);
                                                                        pricetxt3.setText("");
                                                                    } else {
                                                                        pricetxt3.setText(rategobindgarhal.get(i));
                                                                    }


                                                                    //END OF GOBINDGARH ---------------------------


                                                                    //START MUMBAI ---------------------------

                                                                    categorytitletxt4.setVisibility(View.VISIBLE);

                                                                    if (categorymumbaial.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt4.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt4.setText("");
                                                                    } else {
                                                                        categorytitletxt4.setText(categorymumbaial.get(i));
                                                                    }

                                                                    subcategorytitletxt4.setVisibility(View.VISIBLE);

                                                                    if (subcategorymumbaial.get(i).equalsIgnoreCase("-")) {
                                                                        subcategorytitletxt4.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt4.setText("");
                                                                    } else {
                                                                        subcategorytitletxt4.setText(subcategorymumbaial.get(i));
                                                                    }

                                                                    pricetxt4.setVisibility(View.VISIBLE);

                                                                    if (ratemumbaial.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt4.setVisibility(View.INVISIBLE);
                                                                        pricetxt4.setText("");
                                                                    } else {
                                                                        pricetxt4.setText(ratemumbaial.get(i));
                                                                    }

                                                                    //END OF MUMBAI ---------------------------


                                                                    //START DURGAPUR ---------------------------


                                                                    categorytitletxt5.setVisibility(View.VISIBLE);

                                                                    if (categorydurgapural.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt5.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt5.setText("");
                                                                    } else {
                                                                        categorytitletxt5.setText(categorydurgapural.get(i));
                                                                    }

                                                                    subcategorytitletxt5.setVisibility(View.VISIBLE);

                                                                    if (subcategorydurgapural.get(i).equalsIgnoreCase("-")) {
                                                                        subcategorytitletxt5.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt5.setText("");
                                                                    } else {
                                                                        subcategorytitletxt5.setText(subcategorydurgapural.get(i));
                                                                    }

                                                                    pricetxt5.setVisibility(View.VISIBLE);

                                                                    if (ratedurgapural.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt5.setVisibility(View.INVISIBLE);
                                                                        pricetxt5.setText("");
                                                                    } else {
                                                                        pricetxt5.setText(ratedurgapural.get(i));
                                                                    }

                                                                    categorytitletxt6.setVisibility(View.VISIBLE);

                                                                    if (categoryhyderabadal.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt6.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt6.setText("");
                                                                    } else {
                                                                        categorytitletxt6.setText(categoryhyderabadal.get(i));
                                                                    }


                                                                    subcategorytitletxt6.setVisibility(View.VISIBLE);

                                                                    if (subcategoryhyderabadal.get(i).equalsIgnoreCase("-")) {
                                                                        subcategorytitletxt6.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt6.setText("");
                                                                    } else {
                                                                        subcategorytitletxt6.setText(subcategoryhyderabadal.get(i));
                                                                    }

                                                                    pricetxt6.setVisibility(View.VISIBLE);

                                                                    if (ratehyderabadal.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt6.setVisibility(View.INVISIBLE);
                                                                        pricetxt6.setText("");
                                                                    } else {
                                                                        pricetxt6.setText(ratehyderabadal.get(i));
                                                                    }


                                                                    //END OF HYDERABAD --------------------------i


                                                                    Animation topTobottom = AnimationUtils.loadAnimation(getActivity(), R.anim.top_bottom);


                                                                    categorytitletxt1.setDrawingCacheEnabled(true);
                                                                    categorytitletxt1.startAnimation(topTobottom);


                                                                    subcategorytitletxt1.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt1.startAnimation(topTobottom);

                                                                    pricetxt1.setDrawingCacheEnabled(true);
                                                                    pricetxt1.startAnimation(topTobottom);

                                                                    categorytitletxt2.setDrawingCacheEnabled(true);
                                                                    categorytitletxt2.startAnimation(topTobottom);

                                                                    subcategorytitletxt2.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt2.startAnimation(topTobottom);

                                                                    pricetxt2.setDrawingCacheEnabled(true);
                                                                    pricetxt2.startAnimation(topTobottom);

                                                                    categorytitletxt3.setDrawingCacheEnabled(true);
                                                                    categorytitletxt3.startAnimation(topTobottom);

                                                                    subcategorytitletxt3.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt3.startAnimation(topTobottom);

                                                                    pricetxt3.setDrawingCacheEnabled(true);
                                                                    pricetxt3.startAnimation(topTobottom);

                                                                    categorytitletxt4.setDrawingCacheEnabled(true);
                                                                    categorytitletxt4.startAnimation(topTobottom);

                                                                    subcategorytitletxt4.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt4.startAnimation(topTobottom);

                                                                    pricetxt4.setDrawingCacheEnabled(true);
                                                                    pricetxt4.startAnimation(topTobottom);

                                                                    categorytitletxt5.setDrawingCacheEnabled(true);
                                                                    categorytitletxt5.startAnimation(topTobottom);

                                                                    subcategorytitletxt5.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt5.startAnimation(topTobottom);

                                                                    pricetxt5.setDrawingCacheEnabled(true);
                                                                    pricetxt5.startAnimation(topTobottom);


                                                                    categorytitletxt6.setDrawingCacheEnabled(true);
                                                                    categorytitletxt6.startAnimation(topTobottom);

                                                                    subcategorytitletxt6.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt6.startAnimation(topTobottom);

                                                                    pricetxt6.setDrawingCacheEnabled(true);
                                                                    pricetxt6.startAnimation(topTobottom);

                                                                    i = i + 1;



                                                                        new Handler().postDelayed(new Runnable() {
                                                                            @Override
                                                                            public void run() {

                                                                                if(getActivity()!=null) {
                                                                                    Animation topTobottom1 = AnimationUtils.loadAnimation(getActivity(), R.anim.top_bottom1);

                                                                                    categorytitletxt1.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt1.startAnimation(topTobottom1);
                                                                                    pricetxt1.startAnimation(topTobottom1);


                                                                                    categorytitletxt2.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt2.startAnimation(topTobottom1);
                                                                                    pricetxt2.startAnimation(topTobottom1);


                                                                                    categorytitletxt3.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt3.startAnimation(topTobottom1);
                                                                                    pricetxt3.startAnimation(topTobottom1);


                                                                                    categorytitletxt4.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt4.startAnimation(topTobottom1);
                                                                                    pricetxt4.startAnimation(topTobottom1);


                                                                                    categorytitletxt5.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt5.startAnimation(topTobottom1);
                                                                                    pricetxt5.startAnimation(topTobottom1);


                                                                                    categorytitletxt6.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt6.startAnimation(topTobottom1);
                                                                                    pricetxt6.startAnimation(topTobottom1);

                                                                                }
                                                                            }
                                                                        }, 2500);




                                                                    if (i == 9) {

                                                                        i = 0;

                                                                    }

                                                                }
                                                            });
                                                        }
                                                    }
                                                } catch (InterruptedException e) {
                                                }
                                            }
                                        };

                                        t.start();


                                        // }
                                    }
                                }

                                //-------END OF HYDERABAD DATA---------------------------------------------
                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block

                            e.printStackTrace();
                            pdia.dismiss();

                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        //Toast.makeText(getActivity(), messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get live news params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }


}
