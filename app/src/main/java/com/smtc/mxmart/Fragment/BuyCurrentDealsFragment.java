package com.smtc.mxmart.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;



import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.common.base.Strings;
import com.smtc.mxmart.APIName;
import com.smtc.mxmart.BuyerDispatchActivity;
import com.smtc.mxmart.BuyerPaymentDetailsActivity;
import com.smtc.mxmart.LoginActivity;
import com.smtc.mxmart.NetworkUtility;

import com.smtc.mxmart.R;
import com.smtc.mxmart.SellerDispatchActivity;
import com.smtc.mxmart.SingletonActivity;
import com.smtc.mxmart.SplitSaudaPatraActivity;
import com.smtc.mxmart.UtilsDialog;
import com.smtc.mxmart.WebViewActivity;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by 10161 on 8/2/2017.
 */

public class BuyCurrentDealsFragment extends Fragment {

    Typeface source_sans_pro_normal;
    ListView buytodayscurrentdealslistvw;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String userCodestr,mobilenumstr;
    UtilsDialog util = new UtilsDialog();
    CustomAdap customadap;
    String tradeid;
    ProgressDialog pdia;
    TextView nodealsfoundtxt;
    ArrayList<String> buyDealsSplitParentIdArrayList = new ArrayList<String>();
    ArrayList<Integer> buyDealsGreyArrayList = new ArrayList<Integer>();
    private boolean _hasLoadedOnce= false; // your boolean field
    private ProgressBar bar;
    Handler handler = new Handler();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



     source_sans_pro_normal = Typeface.createFromAsset(getActivity().getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");




        View view = inflater.inflate(R.layout.fragment_buy_current_deals, container, false);

        buytodayscurrentdealslistvw = (ListView)view.findViewById(R.id.buycurrentdealslistvw);
        nodealsfoundtxt = (TextView)view.findViewById(R.id.nodealsfoundtxt);
        bar = (ProgressBar)view.findViewById(R.id.progressBar);


        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);

        SingletonActivity.usercode = userCodestr;

       // SingletonActivity.clickedBuyCurrentDeals = true;



     //   if(SingletonActivity.frombackofbuyerdispatch == true || SingletonActivity.frombackofsellerdispatch == true || SingletonActivity.frombackofbuyerpayment == true)
      //  {
            //FragmentTransaction ft = getFragmentManager().beginTransaction();
            //  ft.detach(BuyCurrentDealsFragment.this).attach(BuyCurrentDealsFragment.this).commit();


            if (NetworkUtility.checkConnectivity(getActivity())) {

                //  String SellerGetCurrentDealsUrl = APIName.URL+"/seller/getCurrentDeals?user_code="+userCodestr;
                String BuyerGetCurrentDealsUrl = APIName.URL + "/buyer/getCurrentDeals?user_code=" + userCodestr + "&type=1";
                System.out.println("BUYER GET CURRENT DEALS URL IS---" + BuyerGetCurrentDealsUrl);
                BuyerGetCurrentDealsAPI(BuyerGetCurrentDealsUrl);


            } else {
                util.dialog(getActivity(), "Please check your internet connection.");
            }



     //   }

     /*   else {


            if (NetworkUtility.checkConnectivity(getActivity())) {

                //  String SellerGetCurrentDealsUrl = APIName.URL+"/seller/getCurrentDeals?user_code="+userCodestr;
                String BuyerGetCurrentDealsUrl = APIName.URL + "/buyer/getCurrentDeals?user_code=" + userCodestr + "&type=1";
                System.out.println("BUYER GET CURRENT DEALS URL IS---" + BuyerGetCurrentDealsUrl);
                BuyerGetCurrentDealsAPI(BuyerGetCurrentDealsUrl);


            } else {
                util.dialog(getActivity(), "Please check your internet connection.");
            }

        }*/




        return view;
    }




    private void BuyerGetCurrentDealsAPI(String url) {

      /*  pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();*/

      //  getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
           //     WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        bar.setVisibility(View.VISIBLE);

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };



        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                       // pdia.dismiss();

                      //  getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                bar.setVisibility(View.GONE);

                                buyDealsSplitParentIdArrayList.clear();
                                buyDealsGreyArrayList.clear();

                                System.out.println("RESPONSE OF BUYER GET CURRENT DEALS API IS---" + response);



                                JSONObject BuyerGetCurrentDealsJson = null;
                                try {
                                    BuyerGetCurrentDealsJson = new JSONObject(response);


                                    String statusstr = BuyerGetCurrentDealsJson.getString("status");

                                    if(statusstr.equalsIgnoreCase("true"))
                                    {



                                        JSONArray BuyerCurrentDealsJsonArrayUnsorted = BuyerGetCurrentDealsJson.getJSONArray("cd_Offer");
                                        System.out.println("BUYER CURRENT DEALS JSONARRAY IS---" + BuyerCurrentDealsJsonArrayUnsorted);



                                        if(getActivity()!=null) {

                                            for (int i =0; i<BuyerCurrentDealsJsonArrayUnsorted.length(); i++) {
                                                if (!BuyerCurrentDealsJsonArrayUnsorted.getJSONObject(i).getString("splited_parent_id").equalsIgnoreCase("")) {
                                                    buyDealsSplitParentIdArrayList.add(BuyerCurrentDealsJsonArrayUnsorted.getJSONObject(i).getString("splited_parent_id"));
                                                }
                                            }
                                            //  NSLog(@"self.buyDealsSplitParentIdArray :%@",self.buyDealsSplitParentIdArray);
                                            System.out.println("buyDealsSplitParentIdArray is==="+ buyDealsSplitParentIdArrayList);

                                            for (int k = 0; k < BuyerCurrentDealsJsonArrayUnsorted.length(); k++) {
                                                if (buyDealsSplitParentIdArrayList.contains(BuyerCurrentDealsJsonArrayUnsorted.getJSONObject(k).getString("trade_id"))) {
                                                    BuyerCurrentDealsJsonArrayUnsorted.remove(k);
                                                }
                                            }


                                            customadap = new CustomAdap(getActivity(), BuyerCurrentDealsJsonArrayUnsorted);
                                            buytodayscurrentdealslistvw.setAdapter(customadap);
                                        }

                                    }
                                    else
                                    {
                                        System.out.println("TOAST ====1");
                                        //  Toast.makeText(getActivity(),BuyerGetCurrentDealsJson.getString("message"),Toast.LENGTH_SHORT).show();
                                        nodealsfoundtxt.setVisibility(View.VISIBLE);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    //   pdia.dismiss();

                                  //  getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            //dialog.dismiss();
                                            bar.setVisibility(View.GONE);

                                        }
                                    }, 3000); // 3000 milliseconds delay


                                }


                            }
                        }, 3000); // 3000 milliseconds delay





                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                      //  getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //dialog.dismiss();
                                bar.setVisibility(View.GONE);

                            }
                        }, 3000); // 3000 milliseconds delay



                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                System.out.println("TOAST ====2");
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();



                System.out.println("get seller current enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }

    @Override
   public void onResume() {
        super.onResume();

     //   if(SingletonActivity.frombackofbuyerdispatch == true || SingletonActivity.frombackofsellerdispatch == true || SingletonActivity.frombackofbuyerpayment == true)
       // {
        //    FragmentTransaction ft = getFragmentManager().beginTransaction();
         //   ft.detach(BuyCurrentDealsFragment.this).attach(BuyCurrentDealsFragment.this).commit();

/*

            if (NetworkUtility.checkConnectivity(getActivity())) {

                //  String SellerGetCurrentDealsUrl = APIName.URL+"/seller/getCurrentDeals?user_code="+userCodestr;
                String BuyerGetCurrentDealsUrl = APIName.URL + "/buyer/getCurrentDeals?user_code=" + userCodestr + "&type=1";
                System.out.println("BUYER GET CURRENT DEALS URL IS---" + BuyerGetCurrentDealsUrl);
                BuyerGetCurrentDealsAPI(BuyerGetCurrentDealsUrl);


            } else {
                util.dialog(getActivity(), "Please check your internet connection.");
            }
*/



    //    }

    }
        private class CustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        private JSONArray BuyCurrentDealsJSONArray;



        // public CustomAdap(Context mainActivity)
        public CustomAdap(Context mainActivity,JSONArray BuyCurrentDealsJSONArray)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.BuyCurrentDealsJSONArray = BuyCurrentDealsJSONArray;
            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            // return 7;
            return BuyCurrentDealsJSONArray.length();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub



            final Holder holder = new Holder();
           // final Holder holder ;


            View rowView = null;




            if(rowView==null){
               /* LayoutInflater inflater =(LayoutInflater)
                        ((Activity)context).getSystemService(Context.LAYOUT_INFLATER_SERVICE);*/
                rowView = inflater.inflate(R.layout.buy_current_deals_row, null);


                holder.cmpnyname = (TextView)rowView.findViewById(R.id.cmpnynametxtvw);
                holder.ratingBar = (RatingBar)rowView.findViewById(R.id.ratingBar);
                holder.tradeiddesc = (TextView)rowView.findViewById(R.id.tradeiddesctxtvw);
                holder.proddesc = (TextView) rowView.findViewById(R.id.categorytxtvw);

                holder.qtydesc = (TextView) rowView.findViewById(R.id.qtydesctxtvw);
                holder.saudapatraiv = (ImageView)rowView.findViewById(R.id.saudapatraiv);
                holder.dispatchiv = (ImageView)rowView.findViewById(R.id.dispatchiv);
                holder.paymentiv = (ImageView)rowView.findViewById(R.id.offeriv);
                holder.closeiv = (ImageView)rowView.findViewById(R.id.closeiv);
                holder.disputeiv = (ImageView)rowView.findViewById(R.id.disputeiv);
                holder.splitiv = (ImageView)rowView.findViewById(R.id.splitiv);

                holder.line = (TextView) rowView.findViewById(R.id.line);
                holder.line1 = (TextView)rowView.findViewById(R.id.line1);
                holder.line2 = (TextView)rowView.findViewById(R.id.line2);

                rowView.setTag(holder);
            }
            //holder = (Holder) rowView.getTag();







            holder.saudapatraiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        String saudapatraurl = BuyCurrentDealsJSONArray.getJSONObject(position).getString("saudaPatrak_PDF");

                        Intent i =  new Intent(getActivity(),WebViewActivity.class);
                        SingletonActivity.fromsellcurrentdeals = false;
                        SingletonActivity.frombuycurrentdeals = true;
                        i.putExtra("saudapatraurl",saudapatraurl);
                        startActivity(i);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            System.out.println("BUYER CURRENT DEALS JSONARRAY IN CUSTOMADAP---"+ BuyCurrentDealsJSONArray);

            JSONArray  dispatchStr ;
           final JSONArray  paymentStr ;
            String dispatch_resolved;
            String payment_resolved;
            try {



                dispatchStr = BuyCurrentDealsJSONArray.getJSONObject(position).getJSONArray("dispatch_details");
                paymentStr = BuyCurrentDealsJSONArray.getJSONObject(position).getJSONArray("payment_details");
                dispatch_resolved = BuyCurrentDealsJSONArray.getJSONObject(position).getString("dispatch_resolved");
                payment_resolved = BuyCurrentDealsJSONArray.getJSONObject(position).getString("payment_resolved");

                System.out.println("dispatchStr---"+ dispatchStr);
                System.out.println("dispatchStr Length---"+ dispatchStr.length());
                System.out.println("paymentStr---"+ paymentStr);
                System.out.println("paymentStr Length---"+ paymentStr.length());
                System.out.println("dispatch_resolved string---"+ dispatch_resolved);
                System.out.println("dispatch_resolved Length---"+ dispatch_resolved.length());
                System.out.println("payment_resolved string---"+ payment_resolved);
                System.out.println("payment_resolved Length---"+ payment_resolved.length());

                int dispathDispute = 1;
                int paymentDispute = 1;
                int dispatch_ack = 1;
                int payment_ack = 1;


                if (dispatchStr.length() == 0 && paymentStr.length() == 0) {
                    //sp green color
                    System.out.println("position green---"+ position);
                    holder.saudapatraiv.setBackgroundResource(R.mipmap.ic_saudapatra);
                    holder.saudapatraiv.setVisibility(View.VISIBLE);

                } else if (dispatchStr.length() > 0 || paymentStr.length() > 0) {
                    if (dispatchStr.length() > 0) {
                        for (int i = 0; i < dispatchStr.length(); i++) {

                            //System.out.println("ack status--" + dispatchStr.getJSONObject(position).getString("ack_status"));

                            if (dispatchStr.getJSONObject(i).getString("ack_status").equalsIgnoreCase("0")) {
                                if (dispatch_ack == 1) {
                                    dispatch_ack = 0;
                                }
                            }
                            if (dispatchStr.getJSONObject(i).getString("is_active").equalsIgnoreCase("9")) {
                                holder.saudapatraiv.setBackgroundResource(R.mipmap.ic_saudapatra_grey);
                                holder.saudapatraiv.setVisibility(View.VISIBLE);
                                System.out.println("position grey---"+ position);
                                dispathDispute = 9;
                                break;
                            }
                        }

                    }
                    if (paymentStr.length() > 0) {
                        for (int j = 0; j < paymentStr.length(); j++) {
                            if (paymentStr.getJSONObject(j).getString("ack_status").equalsIgnoreCase("0")) {
                                if (payment_ack == 1) {
                                    payment_ack = 0;
                                }
                            }
                            if (paymentStr.getJSONObject(j).getString("is_active").equalsIgnoreCase("9")) {
                                holder.saudapatraiv.setBackgroundResource(R.mipmap.ic_saudapatra_grey);
                                holder.saudapatraiv.setVisibility(View.VISIBLE);
                                System.out.println("position grey---"+ position);
                                paymentDispute = 9;
                                break;
                            }
                        }
                    }
                    System.out.println("paymentDispute--" + paymentDispute);
                    System.out.println("dispathDispute--" + dispathDispute);
                    System.out.println("tradeId--" + BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id"));
                    if (paymentDispute == 9 || dispathDispute == 9) {
                        //sp grey
                        holder.saudapatraiv.setBackgroundResource(R.mipmap.ic_saudapatra_grey);
                        holder.saudapatraiv.setVisibility(View.VISIBLE);
                    } else {
                        //sp green
                        holder.saudapatraiv.setBackgroundResource(R.mipmap.ic_saudapatra);
                        holder.saudapatraiv.setVisibility(View.VISIBLE);
                    }
                }

                //Split(Old)
                System.out.println("splited_trade_id--" + BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id"));

             /*   if ((BuyCurrentDealsJSONArray.getJSONObject(position).getString("group_company_code").equals(BuyCurrentDealsJSONArray.getJSONObject(position).getString("user_code")))&&(dispatchStr.length()==0)&&(paymentStr.length()==0)) {

                    //sp grey
                    System.out.println("in splited_1--");

                    holder.splitiv.setBackgroundResource(R.mipmap.ic_split);
                    holder.splitiv.setVisibility(View.VISIBLE);
                }
                if (dispatchStr.length() == 0) {
                    //truck grey

                    System.out.println("in splited_2--");
                    holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                    holder.dispatchiv.setVisibility(View.VISIBLE);
                } else {
                    if (dispathDispute == 9 || paymentDispute == 9) {
                        //truck grey
                        System.out.println("in splited_3--");
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    } else if (dispatch_ack == 1) {
                        //truck green
                        System.out.println("in splited_4--");
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    } else {
                        //truck red

                        System.out.println("in splited_5--");
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_red);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    }
                }
*/

                //Split (New Enhancement)


                if (dispathDispute == 9 || paymentDispute == 9) {

                    if ((BuyCurrentDealsJSONArray.getJSONObject(position).getString("group_company_code").equals(BuyCurrentDealsJSONArray.getJSONObject(position).getString("user_code")))&&(dispatchStr.length()==0)&&(paymentStr.length()==0)) {
                                            //grey icon
                        System.out.println("in splited_1--");

                        holder.splitiv.setBackgroundResource(R.mipmap.split_grey);
                        holder.splitiv.setVisibility(View.VISIBLE);
                    }
                }else{
                    if ((BuyCurrentDealsJSONArray.getJSONObject(position).getString("group_company_code").equals(BuyCurrentDealsJSONArray.getJSONObject(position).getString("user_code")))&&(dispatchStr.length()==0)&&(paymentStr.length()==0)) {
                        //green icon
                        System.out.println("in splited_1--");

                        holder.splitiv.setBackgroundResource(R.mipmap.split_green);
                        holder.splitiv.setVisibility(View.VISIBLE);
                    }
                }


                //==================
                //Dispatch(old)


              /*  if (dispatchStr.length() == 0) {
                    //truck grey

                    System.out.println("in splited_2--");
                    holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                    holder.dispatchiv.setVisibility(View.VISIBLE);
                } else {
                    if (dispathDispute == 9 || paymentDispute == 9) {
                        //truck grey
                        System.out.println("in splited_3--");
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    } else if (dispatch_ack == 1) {
                        //truck green
                        System.out.println("in splited_4--");
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    } else {
                        //truck red

                        System.out.println("in splited_5--");
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_red);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    }
                }*/

          //Dispatch(New Enhancement)==========

                if (paymentDispute == 9) {
                    //truck grey

                    System.out.println("in splited_2--");
                    holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                    holder.dispatchiv.setVisibility(View.VISIBLE);
                } else if (dispathDispute == 9) {
                    //truck grey
                    System.out.println("in splited_3--");
                    // holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                    holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_red);
                    holder.dispatchiv.setVisibility(View.VISIBLE);
                } else {

                    if(dispatchStr.length() == 0)
                    {
                        holder.dispatchiv.setBackgroundResource(R.mipmap.dispatch);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    }else if(dispatch_ack == 1){
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch);
                        holder.dispatchiv.setVisibility(View.VISIBLE);

                    }
                    else if(dispatch_ack == 0){
                     //Blue icons==
                        holder.dispatchiv.setBackgroundResource(R.mipmap.dispatch_blue);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    }

                    else{
                        holder.dispatchiv.setBackgroundResource(R.mipmap.dispatch);
                        holder.dispatchiv.setVisibility(View.VISIBLE);
                    }

                }



                //======================

                //payment(old)
             /*  if (paymentStr.length() == 0) {
                    //rupees grey
                    System.out.println("pay splited_2--");
                    holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_displayed);
                    holder.paymentiv.setVisibility(View.VISIBLE);
                } else {
                    if (dispathDispute == 9 || paymentDispute == 9) {
                        //rupees grey
                        System.out.println("pay splited_3--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_displayed);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                    } else if (payment_ack == 1) {
                        //rupees green
                        System.out.println("pay splited_4--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                    } else {
                        System.out.println("pay splited_5--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_red);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                        //rupees red
                    }
                }
                */
                //payment(New Enhancement)

                if (paymentDispute == 9) {
                    //rupees grey
                    System.out.println("pay splited_2--");
                    holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_red);
                    holder.paymentiv.setVisibility(View.VISIBLE);
                } else if (dispathDispute == 9) {
                    //rupees grey
                    System.out.println("pay splited_3--");
                    holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_displayed);
                    holder.paymentiv.setVisibility(View.VISIBLE);
                } else {
                    if (paymentStr.length() == 0) {
                        //rupees green
                        System.out.println("pay splited_4--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.payment);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                    } else if(payment_ack == 1){
                        System.out.println("pay splited_5--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                        //rupees red
                    }
                    else if(payment_ack == 0){
                        System.out.println("blue icon of payment--");
                        holder.paymentiv.setBackgroundResource(R.mipmap.payment_blue);
                        holder.paymentiv.setVisibility(View.VISIBLE);

                    }

                    else {
                        holder.paymentiv.setBackgroundResource(R.mipmap.payment);
                        holder.paymentiv.setVisibility(View.VISIBLE);
                    }

                }
                //red==========

                //REd==========
                if(dispathDispute == 9 && paymentDispute == 9){
                    //sellDealPaymentImg.image = [UIImage imageNamed:@"icn_offer_red"];//Payment
                    holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_red);

                    //sellDealDspImg.image = [UIImage imageNamed:@"icn_dispatch"];//Dispatch
                    holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_red);
                }


                // Deal close
                if (dispatchStr.length() != 0 && paymentStr.length() != 0) {
                    if (dispathDispute == 9 || paymentDispute == 9) {
                        // grey cross icon
                        // title="Dispute has been raised."
                      //  holder.closeiv.setBackgroundResource(R.mipmap.ic_close_disabled);
                        holder.closeiv.setBackgroundResource(R.mipmap.dealclose_handshake_grey);
                        holder.closeiv.setVisibility(View.VISIBLE);
                        holder.closeiv.setEnabled(false);

                    } else if (dispatch_ack == 1 && payment_ack == 1) {

                        if(BuyCurrentDealsJSONArray.getJSONObject(position).getString("deal_close").equalsIgnoreCase("B"))
                        {


                         //   holder.closeiv.setBackgroundResource(R.mipmap.dealclose_green);
                            holder.closeiv.setBackgroundResource(R.mipmap.dealclose_handshake_green);
                            holder.closeiv.setVisibility(View.VISIBLE);
                            holder.closeiv.setEnabled(false);

                        }

                      else  if (BuyCurrentDealsJSONArray.getJSONObject(position).getString("deal_close").equals("null")||BuyCurrentDealsJSONArray.getJSONObject(position).getString("deal_close").equalsIgnoreCase("S"))
                        {
                            // green cross icon with action
                            // title="Deal Close"
                          //  holder.closeiv.setBackgroundResource(R.mipmap.dealclose);
                            holder.closeiv.setBackgroundResource(R.mipmap.dealclose_handshake_orange);
                            holder.closeiv.setVisibility(View.VISIBLE);
                            holder.closeiv.setEnabled(true);

                        }






                    }


                }

                System.out.println("position ppp---"+ position);

                if (dispathDispute == 9 || paymentDispute == 9) {

                    holder.disputeiv.setBackgroundResource(R.mipmap.exclamation);
                    holder.disputeiv.setVisibility(View.VISIBLE);
                }


              else   if ((dispatch_resolved.length() > 2) || (payment_resolved.length() > 2)) {
                    holder.disputeiv.setBackgroundResource(R.mipmap.orange_info);
                    holder.disputeiv.setVisibility(View.VISIBLE);

                }


                try {
//                    System.out.println("SPLITTED PARENT ID AT POSITION 9----"+ TextUtils.isEmpty(BuyCurrentDealsJSONArray.getJSONObject(9).getString("splited_parent_id")));
           //         System.out.println("SPLITTED PARENT ID AT POSITION 23----"+ TextUtils.isEmpty(BuyCurrentDealsJSONArray.getJSONObject(23).getString("splited_parent_id")));

                    if(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_parent_id")!= "null"&&(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_parent_id").length()!=0)){
                   // if (!BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_parent_id").equalsIgnoreCase("null")) {
                   /* cell.line2View.hidden = false;
                    cell.line1View.hidden = false;
                    cell.lineView.hidden = true;*/


                       holder.line1.setVisibility(View.VISIBLE);
                       holder.line2.setVisibility(View.VISIBLE);
                       holder.line.setVisibility(View.GONE);

                       //holder.line1.setBackgroundR(R.drawable.sell_todays_offer_background);


                       System.out.println("IN LOOP OF SPLIT 1========" + position);

                       SingletonActivity.splited = "split";


                    }

                    else{
                  /*  cell.line2View.hidden = true;
                    cell.line1View.hidden = true;
                    cell.lineView.hidden = false;*/

                        holder.line1.setVisibility(View.GONE);
                        holder.line2.setVisibility(View.GONE);
                        holder.line.setVisibility(View.VISIBLE);




                        SingletonActivity.splited = "not split";
                        System.out.println("IN LOOP OF NOT SPLIT========");
                    }


                    if(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id")!= "null"&&(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id").length()!=0)){
                   // if (!BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id").equalsIgnoreCase("null")) {

              /*  cell.line2View.hidden = true;
                cell.line1View.hidden = true;
                cell.lineView.hidden = false;*/

                        holder.line2.setVisibility(View.GONE);
                        holder.line1.setVisibility(View.GONE);
                        holder.line.setVisibility(View.VISIBLE);

              /*  [self.buyDealsGreyArray addObject:[NSString stringWithFormat:@"%d",(int)indexPath.row]];
                cell.lineView.backgroundColor = [UIColor lightGrayColor];
                buyDealPaymentImg.image = [UIImage imageNamed:@"icn_payment"];
                buyDealDspImg.image = [UIImage imageNamed:@"icn_dispatch_grey"];*/

                        buyDealsGreyArrayList.add(position);
                        holder.line.setBackgroundColor(Color.parseColor("#E4E4E4"));
                        holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_displayed);
                        holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                        SingletonActivity.splited = "split";


              /*  buyDealClosedBtn.hidden = true;
                buyDealSPBtn.hidden = true;
                buyDealSplitBtn.hidden = true;
              */


                        holder.closeiv.setVisibility(View.INVISIBLE);
                        holder.saudapatraiv.setVisibility(View.INVISIBLE);
                        holder.splitiv.setVisibility(View.INVISIBLE);

                        System.out.println("IN LOOP OF SPLIT 2========");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (buyDealsGreyArrayList.contains(position)) {
                    SingletonActivity.buyParent = "parent";
                }else{
                    SingletonActivity.buyParent = "child";
                }




                final String sellerusercode = BuyCurrentDealsJSONArray.getJSONObject(position).getString("seller_user_code");
                tradeid = BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");

                holder.paymentiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        try {
                            if(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_parent_id")!= "null"&&(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_parent_id").length()!=0)){
                   /* cell.line2View.hidden = false;
                    cell.line1View.hidden = false;
                    cell.lineView.hidden = true;*/

                                holder.line1.setVisibility(View.VISIBLE);
                                holder.line2.setVisibility(View.VISIBLE);
                                holder.line.setVisibility(View.GONE);

                                //holder.line1.setBackgroundR(R.drawable.sell_todays_offer_background);



                                System.out.println("IN LOOP OF SPLIT 1========"+ position);

                                SingletonActivity.splited = "split";

                            }

                            else{
                  /*  cell.line2View.hidden = true;
                    cell.line1View.hidden = true;
                    cell.lineView.hidden = false;*/

                                holder.line1.setVisibility(View.GONE);
                                holder.line2.setVisibility(View.GONE);
                                holder.line.setVisibility(View.VISIBLE);




                                SingletonActivity.splited = "not split";
                                System.out.println("IN LOOP OF NOT SPLIT========");
                            }

                            if(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id")!= "null"&&(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id").length()!=0)){

              /*  cell.line2View.hidden = true;
                cell.line1View.hidden = true;
                cell.lineView.hidden = false;*/

                                holder.line2.setVisibility(View.GONE);
                                holder.line1.setVisibility(View.GONE);
                                holder.line.setVisibility(View.VISIBLE);

              /*  [self.buyDealsGreyArray addObject:[NSString stringWithFormat:@"%d",(int)indexPath.row]];
                cell.lineView.backgroundColor = [UIColor lightGrayColor];
                buyDealPaymentImg.image = [UIImage imageNamed:@"icn_payment"];
                buyDealDspImg.image = [UIImage imageNamed:@"icn_dispatch_grey"];*/

                                buyDealsGreyArrayList.add(position);
                                holder.line.setBackgroundColor(Color.parseColor("#E4E4E4"));
                                holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_displayed);
                                holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                                SingletonActivity.splited = "split";


              /*  buyDealClosedBtn.hidden = true;
                buyDealSPBtn.hidden = true;
                buyDealSplitBtn.hidden = true;
              */


                                holder.closeiv.setVisibility(View.INVISIBLE);
                                holder.saudapatraiv.setVisibility(View.INVISIBLE);
                                holder.splitiv.setVisibility(View.INVISIBLE);

                                System.out.println("IN LOOP OF SPLIT 2========");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        System.out.println("buyDealsGreyArrayList on click of payment in BuyCurrentDealsFragment======="+ buyDealsGreyArrayList);
                        System.out.println("position on click of payment in BuyCurrentDealsFragment======="+ position);


                        if (buyDealsGreyArrayList.contains(position)) {
                            SingletonActivity.buyParent = "parent";
                        }else{
                            SingletonActivity.buyParent = "child";
                        }





                        Intent i = new Intent(getActivity(), BuyerPaymentDetailsActivity.class);
                        System.out.println("BUYPARENT VALUE ON CLICK OF BUYER CURRENT DEALS==="+ SingletonActivity.buyParent);
                        //SingletonActivity.paymentDetailsJSONArray = paymentStr;
                        i.putExtra("splitted",SingletonActivity.splited);
                        i.putExtra("buyParent",SingletonActivity.buyParent);
                            SingletonActivity.fromsellcurrentdeals = false;
                            SingletonActivity.frombuycurrentdeals = true;

                        SingletonActivity.sellerusercode = sellerusercode;
                        try {
                            SingletonActivity.tradeid =  BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                            SingletonActivity.sellid = BuyCurrentDealsJSONArray.getJSONObject(position).getString("sell_id");
                            SingletonActivity.gst_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("gst");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);
                    }
                });

                holder.dispatchiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            if(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_parent_id")!= "null"&&(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_parent_id").length()!=0)){
                   /* cell.line2View.hidden = false;
                    cell.line1View.hidden = false;
                    cell.lineView.hidden = true;*/

                                holder.line1.setVisibility(View.VISIBLE);
                                holder.line2.setVisibility(View.VISIBLE);
                                holder.line.setVisibility(View.GONE);

                                //holder.line1.setBackgroundR(R.drawable.sell_todays_offer_background);



                                System.out.println("IN LOOP OF SPLIT 11========"+ position);

                                SingletonActivity.splited = "split";

                            }

                            else{
                  /*  cell.line2View.hidden = true;
                    cell.line1View.hidden = true;
                    cell.lineView.hidden = false;*/

                                holder.line1.setVisibility(View.GONE);
                                holder.line2.setVisibility(View.GONE);
                                holder.line.setVisibility(View.VISIBLE);




                                SingletonActivity.splited = "not split";
                                System.out.println("IN LOOP OF NOT SPLIT1========");
                            }

                            if(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id")!= "null"&&(BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id").length()!=0)){

              /*  cell.line2View.hidden = true;
                cell.line1View.hidden = true;
                cell.lineView.hidden = false;*/

                                holder.line2.setVisibility(View.GONE);
                                holder.line1.setVisibility(View.GONE);
                                holder.line.setVisibility(View.VISIBLE);

              /*  [self.buyDealsGreyArray addObject:[NSString stringWithFormat:@"%d",(int)indexPath.row]];
                cell.lineView.backgroundColor = [UIColor lightGrayColor];
                buyDealPaymentImg.image = [UIImage imageNamed:@"icn_payment"];
                buyDealDspImg.image = [UIImage imageNamed:@"icn_dispatch_grey"];*/

                                buyDealsGreyArrayList.add(position);
                                holder.line.setBackgroundColor(Color.parseColor("#E4E4E4"));
                                holder.paymentiv.setBackgroundResource(R.mipmap.ic_offer_displayed);
                                holder.dispatchiv.setBackgroundResource(R.mipmap.ic_dispatch_disabled);
                                SingletonActivity.splited = "split";


              /*  buyDealClosedBtn.hidden = true;
                buyDealSPBtn.hidden = true;
                buyDealSplitBtn.hidden = true;
              */


                                holder.closeiv.setVisibility(View.INVISIBLE);
                                holder.saudapatraiv.setVisibility(View.INVISIBLE);
                                holder.splitiv.setVisibility(View.INVISIBLE);

                                System.out.println("IN LOOP OF SPLIT 22========");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                        try {

                            System.out.println("POSITION CLICKED---"+ position);
                            System.out.println("SingletonActivity.splited in buyer deals dispatch---"+ SingletonActivity.splited);

                            System.out.println("SPLITED ID LENGTH PARENT--"+BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_parent_id").length());
                            System.out.println("SPLITED ID LENGTH TRADE--"+BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id").length());

                            System.out.println("SPLITED PARENT ID--"+BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_parent_id"));
                            System.out.println("SPLITED TRADE ID--"+BuyCurrentDealsJSONArray.getJSONObject(position).getString("splited_trade_id"));

                            if (buyDealsGreyArrayList.contains(position)) {
                               SingletonActivity.buyParent = "parent";
                            }else{
                                SingletonActivity.buyParent = "child";
                            }

                            String sellermobilenum = BuyCurrentDealsJSONArray.getJSONObject(position).getString("seller_mobile_num");

                            SingletonActivity.splited = "not split";
                          /*  if(BuyCurrentDealsJSONArray.getJSONObject(position).getString("transport").equalsIgnoreCase("2")) {

                                SingletonActivity.tradeid = BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                                String sauda_patra_gen_date = BuyCurrentDealsJSONArray.getJSONObject(position).getString("sauda_patra_gen_date");
                                SingletonActivity.sellid = BuyCurrentDealsJSONArray.getJSONObject(position).getString("sell_id");
                                SingletonActivity.sauda_patra_gen_date = sauda_patra_gen_date;


                                Intent i = new Intent(getActivity(), SellerDispatchActivity.class);
                               *//* i.putExtra("splitted", SingletonActivity.splited);
                                i.putExtra("buyParent", SingletonActivity.buyParent);
                                SingletonActivity.tradeid = BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                                SingletonActivity.sellerusercode = sellerusercode;
                                SingletonActivity.sellermobilenum = sellermobilenum;*//*
                                SingletonActivity.tradeid = BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                                SingletonActivity.fromsellcurrentdeals = false;
                                SingletonActivity.frombuycurrentdeals = true;

                                startActivity(i);

                            }

                            else
                            {*/



                                Intent i = new Intent(getActivity(), BuyerDispatchActivity.class);
                                i.putExtra("splitted", SingletonActivity.splited);
                                i.putExtra("buyParent", SingletonActivity.buyParent);
                                SingletonActivity.tradeid = BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                                SingletonActivity.sellerusercode = sellerusercode;
                                SingletonActivity.sellermobilenum = sellermobilenum;
                                SingletonActivity.fromsellcurrentdeals = false;
                                SingletonActivity.frombuycurrentdeals = true;
                                startActivity(i);


                         //   }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });


                holder.splitiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String markdownpriceval = null;
                        String ratestr = null;

                        try {
                            markdownpriceval = BuyCurrentDealsJSONArray.getJSONObject(position).getString("quoted_rate");
                            ratestr = BuyCurrentDealsJSONArray.getJSONObject(position).getString("rate");
                            String israte = BuyCurrentDealsJSONArray.getJSONObject(position).getString("israte");

                            if (!israte.equalsIgnoreCase("1")) {
                                if (!markdownpriceval.equalsIgnoreCase("0.00")){


                                    double price = Double.parseDouble(ratestr) - Double.parseDouble(markdownpriceval);

                                    SingletonActivity.rate = Double.toString(price);

                                }else{

                                    SingletonActivity.rate = ratestr;

                                }
                            }else{


                                SingletonActivity.rate = ratestr;
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                        try {
                            Intent i = new Intent(getActivity(), SplitSaudaPatraActivity.class);
                            SingletonActivity.tradeid =  BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                            SingletonActivity.sellerusercode = BuyCurrentDealsJSONArray.getJSONObject(position).getString("seller_user_code");
                            SingletonActivity.splitqty = BuyCurrentDealsJSONArray.getJSONObject(position).getString("quantity");
                            SingletonActivity.ratetype = BuyCurrentDealsJSONArray.getJSONObject(position).getString("rate_type");
                           // SingletonActivity.rate = BuyCurrentDealsJSONArray.getJSONObject(position).getString("rate");
                            SingletonActivity.sellid = BuyCurrentDealsJSONArray.getJSONObject(position).getString("sell_id");
                            SingletonActivity.categoryname = BuyCurrentDealsJSONArray.getJSONObject(position).getString("category_name");
                            SingletonActivity.sellercompanyname = BuyCurrentDealsJSONArray.getJSONObject(position).getString("seller_company_name");
                            SingletonActivity.sellercity = BuyCurrentDealsJSONArray.getJSONObject(position).getString("seller_city");

                            startActivity(i);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });

                holder.closeiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(NetworkUtility.checkConnectivity(getActivity())){

                            try {
                                holder.closeiv.setEnabled(false);
                                String BuyerCloseDealUrl = APIName.URL+"/buyer/closeDeal?trade_id="+BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                                System.out.println("BUYER CLOSE DEAL API URL IS---"+ BuyerCloseDealUrl);
                                BuyerCloseDealsAPI(BuyerCloseDealUrl);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                        else{
                            util.dialog(getActivity(), "Please check your internet connection.");
                        }



                    }
                });




            } catch (JSONException e) {
                e.printStackTrace();
            }


            try {
                holder.cmpnyname.setText(BuyCurrentDealsJSONArray.getJSONObject(position).getString("seller_company_name"));
                holder.ratingBar.setRating(Float.parseFloat(BuyCurrentDealsJSONArray.getJSONObject(position).getString("rate_type")));

                if(!BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id").equalsIgnoreCase("")) {
                    holder.tradeiddesc.setVisibility(View.VISIBLE);
                    holder.tradeiddesc.setText(BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id"));
                }
                else
                {
                    holder.tradeiddesc.setVisibility(View.INVISIBLE);
                }
                holder.proddesc.setText(BuyCurrentDealsJSONArray.getJSONObject(position).getString("category_name"));
                System.out.println("SUB CATEGORY NAME IN SELL CURRENT DEALS---"+ BuyCurrentDealsJSONArray.getJSONObject(position).getString("sub_category_name"));
//                holder.sub_cat_txt.setText(BuyCurrentDealsJSONArray.getJSONObject(position).getString("sub_category_name"));
                holder.qtydesc.setText(BuyCurrentDealsJSONArray.getJSONObject(position).getString("quantity"));

                //Split and patent



                rowView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        try {
                            String tradeidstr = BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_id");
                            String companyname = BuyCurrentDealsJSONArray.getJSONObject(position).getString("seller_company_name");
                            String categorydesc = BuyCurrentDealsJSONArray.getJSONObject(position).getString("category_name");
                            String subcategorydesc = BuyCurrentDealsJSONArray.getJSONObject(position).getString("sub_category_name");
                            String locationdesc = BuyCurrentDealsJSONArray.getJSONObject(position).getString("seller_city");
                            String qtyofferedstr = BuyCurrentDealsJSONArray.getJSONObject(position).getString("quantity");
                            String ratestr = BuyCurrentDealsJSONArray.getJSONObject(position).getString("rate");
                          /*  String q1val = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q1");
                            String q2val = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q2");
                            String q3val = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q3");
                            String q4val = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q4");
                            String q5val = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q5");
                            String q6val = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q6");
                            String q7val = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q7");
                            String q8val = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q8");*/
                            String tncval = BuyCurrentDealsJSONArray.getJSONObject(position).getString("terms");
                            String markdownpriceval = BuyCurrentDealsJSONArray.getJSONObject(position).getString("quoted_rate");
                            String tradestatusval = BuyCurrentDealsJSONArray.getJSONObject(position).getString("trade_status");
                            String israte = BuyCurrentDealsJSONArray.getJSONObject(position).getString("israte");
                            String rate_type_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("rate_type");
                            String category_grade_name_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("category_grade_name");
                            String category_grade_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("category_grade");
                            String size_range_start_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("size_range_start");
                            String size_range_end_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("size_range_end");
                            String q1_title_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q1_title");
                            String q1_value_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q1_value");
                            String q2_title_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q2_title");
                            String q2_value_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q2_value");
                            String q3_title_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q3_title");
                            String q3_value_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q3_value");
                            String q4_title_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q4_title");
                            String q4_value_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q4_value");
                            String q5_title_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q5_title");
                            String q5_value_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q5_value");
                            String q6_title_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q6_title");
                            String q6_value_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q6_value");
                            String q7_title_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q7_title");
                            String q7_value_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q7_value");
                            String q8_title_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q8_title");
                            String q8_value_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("q8_value");
                            String transport_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("transport");
                            String loading_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("loading");
                            String gst_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("gst");
                            String insurance_select_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("insurance_select");
                            String insurance_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("insurance");
                            String categoryid = BuyCurrentDealsJSONArray.getJSONObject(position).getString("category_id");
                            String length_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("length");
                            String promo_str = BuyCurrentDealsJSONArray.getJSONObject(position).getString("promo_code");
                            String modified_field_str =  BuyCurrentDealsJSONArray.getJSONObject(position).getString("modified_field");

                            JSONArray detailJSONArray = BuyCurrentDealsJSONArray.getJSONObject(position).getJSONArray("detail");


                            final String delivery_type_str = detailJSONArray.getJSONObject(0).getString("delivery_type");
                            final String delivery_in_days_str = detailJSONArray.getJSONObject(0).getString("delivery_in_days");
                            final String material_inspection_str = detailJSONArray.getJSONObject(0).getString("material_inspection_type");
                            final String weightment_type_str = detailJSONArray.getJSONObject(0).getString("weightment_type");
                            final String test_certificate_type_str = detailJSONArray.getJSONObject(0).getString("test_certificate_type");
                            final String sell_tandc_str = detailJSONArray.getJSONObject(0).getString("sell_tandc");


                            ViewOfferDialog(sell_tandc_str,modified_field_str,delivery_type_str,delivery_in_days_str,material_inspection_str,weightment_type_str,test_certificate_type_str,promo_str,length_str,categoryid,transport_str,loading_str,gst_str,insurance_select_str,insurance_str,category_grade_str,size_range_start_str,size_range_end_str,q1_title_str,q1_value_str,q2_title_str,q2_value_str,q3_title_str,q3_value_str,q4_title_str,q4_value_str,q5_title_str,q5_value_str,q6_title_str,q6_value_str,q7_title_str,q7_value_str,q8_title_str,q8_value_str,category_grade_name_str,rate_type_str,israte,tradeidstr,companyname,categorydesc,subcategorydesc,locationdesc,qtyofferedstr,ratestr,tncval,markdownpriceval,tradestatusval);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });






            } catch (JSONException e) {
                e.printStackTrace();
            }



            return rowView;

        }


    }

    public void ViewOfferDialog(String sell_tandc_str,String modified_field_str,String delivery_type_str,String delivery_in_days_str,String material_inspection_str,String weighment_type_str,String test_certificate_type_str,String promo_str,String length_str,String categoryid,String transport_str,String loading_str,String gst_str,String insurance_select_str,String insurance_str,String category_grade_str,String size_range_start_str,String size_range_end_str,String q1_title_str,String q1_value_str,String q2_title_str,String q2_value_str,String q3_title_str,String q3_value_str,String q4_title_str,String q4_value_str,String q5_title_str,String q5_value_str,String q6_title_str,String q6_value_str,String q7_title_str,String q7_value_str,String q8_title_str,String q8_value_str,String category_grade_name_str,String rate_type_str,String israte,String tradeidstr,String companyname,String categorydescstr,String subcategorydescstr,String locationdesc,String qtyofferedstr,String ratestr,String terms,String markdownpriceval,String tradestatusval) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.view_buyer_current_deals, null);

        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView tradeidtitletxtvw = (TextView)dialogView.findViewById(R.id.tradeidtitletxtvw);
        final TextView cmpnynamedesctxt = (TextView) dialogView.findViewById(R.id.cmpnynamedesctxt);
        final TextView categorydesctxt = (TextView) dialogView.findViewById(R.id.categorydesctxt);
        final TextView subcategorydesctxt = (TextView) dialogView.findViewById(R.id.subcategorydesctxt);
        final TextView locationdesctxt = (TextView) dialogView.findViewById(R.id.locationdesctxt);
        final TextView qtyoffereddesctxtvw = (TextView)dialogView.findViewById(R.id.qtyoffereddesctxtvw);
        final TextView ratedesctxtvw = (TextView)dialogView.findViewById(R.id.ratedesctxtvw);
        final TextView pricetxt1 = (TextView) dialogView.findViewById(R.id.pricetxt1);
        final TextView pricetxt2 = (TextView) dialogView.findViewById(R.id.pricetxt2);
        final TextView pricetxt3 = (TextView) dialogView.findViewById(R.id.pricetxt3);
        final TextView pricetxt4 = (TextView) dialogView.findViewById(R.id.pricetxt4);
        final TextView pricetxt5 = (TextView) dialogView.findViewById(R.id.pricetxt5);
        final TextView pricetxt6 = (TextView) dialogView.findViewById(R.id.pricetxt6);
        final TextView pricetxt7 = (TextView) dialogView.findViewById(R.id.pricetxt7);
        final TextView pricetxt8 = (TextView) dialogView.findViewById(R.id.pricetxt8);
        final TextView termsdesctxt = (TextView) dialogView.findViewById(R.id.termsdesctxt);
        final TextView markdownpricedesctxt = (TextView) dialogView.findViewById(R.id.markdownpricedesctxt);
        final TextView statusdesctxt = (TextView) dialogView.findViewById(R.id.statusdesctxt);

        final TextView transportationdesctxt = (TextView) dialogView.findViewById(R.id.transportationdesctxt);
        final TextView loadingdesctxt = (TextView) dialogView.findViewById(R.id.loadingdesctxt);
        final TextView gstdesctxt = (TextView) dialogView.findViewById(R.id.gstdesctxt);
        final TextView insurancedesctxt = (TextView) dialogView.findViewById(R.id.insurancedesctxt);


        final TextView q1title = (TextView) dialogView.findViewById(R.id.q1title);
        final TextView q2title = (TextView) dialogView.findViewById(R.id.q2title);
        final TextView q3title = (TextView) dialogView.findViewById(R.id.q3title);
        final TextView q4title = (TextView) dialogView.findViewById(R.id.q4title);
        final TextView q5title = (TextView) dialogView.findViewById(R.id.q5title);
        final TextView q6title = (TextView) dialogView.findViewById(R.id.q6title);
        final TextView q7title = (TextView) dialogView.findViewById(R.id.q7title);
        final TextView q8title = (TextView) dialogView.findViewById(R.id.q8title);
        final TextView gradedesctxt = (TextView) dialogView.findViewById(R.id.gradedesctxt);

        final TextView lengthtitletxtvw =  (TextView) dialogView.findViewById(R.id.lengthtitletxtvw);
        final TextView lengthdesctxt = (TextView) dialogView.findViewById(R.id.lengthdesctxt);
        final View line16 = (View)dialogView.findViewById(R.id.line16);

        final TextView promodesctxt = (TextView) dialogView.findViewById(R.id.promodesctxt);


        final TextView deliveryindaysdesctxt = (TextView)dialogView.findViewById(R.id.deliveryindaysdesctxt);
        final TextView materialinspectiondesctxt = (TextView)dialogView.findViewById(R.id.materialinspectiondesctxt);
        final TextView weighmentdesctxt = (TextView)dialogView.findViewById(R.id.weighmentdesctxt);
        final TextView testcertificatedesctxt = (TextView)dialogView.findViewById(R.id.testcertificatedesctxt);

        if(delivery_type_str.equalsIgnoreCase("1"))
        {
            deliveryindaysdesctxt.setText("As per Aachar Sanhita");
        }
        if(delivery_type_str.equalsIgnoreCase("2"))
        {
            deliveryindaysdesctxt.setText(delivery_in_days_str+ " days");
        }

        if(material_inspection_str.equalsIgnoreCase("1"))
        {
            materialinspectiondesctxt.setText("Buyer's Place");
        }
        if(material_inspection_str.equalsIgnoreCase("2"))
        {
            materialinspectiondesctxt.setText("Seller's Place");
        }

        if(weighment_type_str.equalsIgnoreCase("1"))
        {
            weighmentdesctxt.setText("Buyer's Place");
        }
        if(weighment_type_str.equalsIgnoreCase("2"))
        {
            weighmentdesctxt.setText("Seller's Place");
        }
        if(weighment_type_str.equalsIgnoreCase("3"))
        {
            weighmentdesctxt.setText("Buyer & Seller's Place");
        }


        if(test_certificate_type_str.equalsIgnoreCase("1"))
        {
            testcertificatedesctxt.setText("Yes");
           // testcertificatedesctxt.setText("Required");
        }
        if(test_certificate_type_str.equalsIgnoreCase("2"))
        {
            testcertificatedesctxt.setText("No");
           // testcertificatedesctxt.setText("Not Required");
        }


        if(!(promo_str.equalsIgnoreCase("0"))){
            promodesctxt.setText(promo_str);

            if((promo_str.equalsIgnoreCase("(null)")))
            {
                promodesctxt.setText("--");
            }
        }
        else
        {
            promodesctxt.setText("--");
        }

        if(length_str.length()!=4) {
           // lengthdesctxt.setText(length_str+" m");

            line16.setVisibility(View.VISIBLE);
            lengthtitletxtvw.setVisibility(View.VISIBLE);
            lengthdesctxt.setVisibility(View.VISIBLE);

            if(categoryid.equalsIgnoreCase("1")) {

                lengthtitletxtvw.setText("Length");
                lengthdesctxt.setText(length_str+" m");
            }

            if(categoryid.equalsIgnoreCase("2"))
            {
                lengthtitletxtvw.setText("Weight");
                lengthdesctxt.setText(length_str+" kg");
            }
        }
        else
        {
            line16.setVisibility(View.GONE);
            lengthtitletxtvw.setVisibility(View.GONE);
            lengthdesctxt.setVisibility(View.GONE);
        }


        gradedesctxt.setText(category_grade_name_str);

        pricetxt1.setText(q1_value_str);
        pricetxt2.setText(q2_value_str);
        pricetxt3.setText(q3_value_str);
        pricetxt4.setText(q4_value_str);
        pricetxt5.setText(q5_value_str);
        pricetxt6.setText(q6_value_str);
     //   pricetxt7.setText(q7_value_str);
       // pricetxt8.setText(q8_value_str);

        if(!q7_value_str.equalsIgnoreCase(""))
        {
            pricetxt7.setText(q7_value_str);
        }
        else {
            pricetxt7.setText("NA");
        }

        if(!q8_value_str.equalsIgnoreCase(""))
        {
            pricetxt8.setText(q8_value_str);
        }
        else
        {
            pricetxt8.setText("NA");
        }

        List<String> modified_field_List = Arrays.asList(modified_field_str.split(","));


        if (modified_field_List.contains("-3-"))
        {
            pricetxt1.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-4-"))
        {
            pricetxt2.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-5-"))
        {
            pricetxt3.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-6-"))
        {
            pricetxt4.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-7-"))
        {
            pricetxt5.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-8-"))
        {
            pricetxt6.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-9-"))
        {
            pricetxt7.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-10-"))
        {
            pricetxt8.setTextColor(Color.RED);
        }

        q1title.setText(q1_title_str);
        q2title.setText(q2_title_str);
        q3title.setText(q3_title_str);
        q4title.setText(q4_title_str);
        q5title.setText(q5_title_str);
        q6title.setText(q6_title_str);
        q7title.setText(q7_title_str);
        q8title.setText(q8_title_str);


        if(transport_str.equalsIgnoreCase("1"))
        {
            transportationdesctxt.setText("Seller's End");
        }
        if(transport_str.equalsIgnoreCase("2"))
        {
            transportationdesctxt.setText("Buyer's End");
        }
        if(transport_str.equalsIgnoreCase("3"))
        {
            transportationdesctxt.setText("Mxmart's End");
        }

        if(loading_str.equalsIgnoreCase("NA"))
        {
            loadingdesctxt.setText(loading_str);
        }
        else {
            loadingdesctxt.setText(loading_str + " \u20B9"+"/MT");
        }
        gstdesctxt.setText(gst_str);

        if(insurance_select_str.equalsIgnoreCase("NA"))
        {
            insurancedesctxt.setText("NA");
        }
        else {

            if (insurance_str.equalsIgnoreCase("1")) {
                insurancedesctxt.setText("Seller");
            } else {
                insurancedesctxt.setText("Buyer");
            }
        }


        if(categoryid.equalsIgnoreCase("1")||categoryid.equalsIgnoreCase("2")) {
            subcategorydesctxt.setText(subcategorydescstr);
        }
        else {
            subcategorydesctxt.setText(size_range_start_str + "-" + size_range_end_str);
        }


        System.out.println("TERMS ARE==="+ terms.length());
        if(!(terms.length()==4)) {
            termsdesctxt.setText(sell_tandc_str);
        }



        tradeidtitletxtvw.setText("Trade ID :"+tradeidstr);
        cmpnynamedesctxt.setText(companyname);
        categorydesctxt.setText(categorydescstr);
       // subcategorydesctxt.setText(subcategorydescstr);
        locationdesctxt.setText(locationdesc);
        qtyoffereddesctxtvw.setText(qtyofferedstr);

        if(rate_type_str.equalsIgnoreCase("1"))
        {
            rate_type_str = "(A)";
        }

        if(rate_type_str.equalsIgnoreCase("2"))
        {
            rate_type_str = "(N)";
        }

        if(rate_type_str.equalsIgnoreCase("3"))
        {
            rate_type_str = "(R)";
        }



        if (!israte.equalsIgnoreCase("1")) {
            if (!markdownpriceval.equalsIgnoreCase("0.00")){


                double price = Double.parseDouble(ratestr) - Double.parseDouble(markdownpriceval);

                ratedesctxtvw.setText(Double.toString(price)+""+rate_type_str);

            }else{

                ratedesctxtvw.setText(ratestr+""+rate_type_str);

            }
        }else{


            ratedesctxtvw.setText(ratestr+""+rate_type_str);
        }


       // termsdesctxt.setText(terms);
        markdownpricedesctxt.setText(markdownpriceval);

        if(tradestatusval.equalsIgnoreCase("Sauda Patrak Generat"))
        {
            tradestatusval = "Sauda Patrak Generated";
        }

        statusdesctxt.setText(tradestatusval);


        final  AlertDialog b = dialogBuilder.create();
        b.show();


        ImageView ic_close = (ImageView)dialogView.findViewById(R.id.ic_close);
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }

    public class Holder {
        TextView cmpnyname,proddesc,sub_cat_txt,qtydesc,tradeiddesc;
        ImageView saudapatraiv,dispatchiv,paymentiv,closeiv,disputeiv,splitiv;
        RatingBar ratingBar;
        TextView line,line1,line2;



    }

    private void BuyerCloseDealsAPI(String url) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF BUYER CLOSE DEALS API IS---" + response);




                        JSONObject BuyerCloseDealJson = null;
                        try {
                            BuyerCloseDealJson = new JSONObject(response);


                            String statusstr = BuyerCloseDealJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                System.out.println("TOAST ====3");
                                Toast.makeText(getActivity(),BuyerCloseDealJson.getString("message"),Toast.LENGTH_SHORT).show();

                             /*     new android.os.Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {*/

                                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.detach(BuyCurrentDealsFragment.this).attach(BuyCurrentDealsFragment.this).commit();


                                  /*  }
                                }, 10);*/

                            /*    if(NetworkUtility.checkConnectivity(getActivity())){

                                    //  String SellerGetCurrentDealsUrl = APIName.URL+"/seller/getCurrentDeals?user_code="+userCodestr;
                                    String BuyerGetCurrentDealsUrl = APIName.URL+"/buyer/getCurrentDeals?user_code="+userCodestr+"&type=1";
                                    System.out.println("BUYER GET CURRENT DEALS URL IS---"+ BuyerGetCurrentDealsUrl);
                                    BuyerGetCurrentDealsAPI(BuyerGetCurrentDealsUrl);


                                }
                                else{
                                    util.dialog(getActivity(), "Please check your internet connection.");
                                }*/

                            }
                            else
                            {
                                System.out.println("TOAST ====4");
                                Toast.makeText(getActivity(),BuyerCloseDealJson.getString("message"),Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                        Toast.makeText(getActivity(),messagestr,Toast.LENGTH_SHORT).show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                System.out.println("TOAST ====5");
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();





                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }


}
