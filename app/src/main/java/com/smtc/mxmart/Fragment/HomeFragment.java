package com.smtc.mxmart.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smtc.mxmart.APIName;
import com.smtc.mxmart.CirclePagerIndicatorDecoration;

import com.smtc.mxmart.EnquiryActivity;
import com.smtc.mxmart.HomeActivity;
import com.smtc.mxmart.LivePriceGraphActivity;
import com.smtc.mxmart.NetworkUtility;
import com.smtc.mxmart.PicassoTrustAll;
import com.smtc.mxmart.R;
import com.smtc.mxmart.RoundedCornersTransform;
import com.smtc.mxmart.SingletonActivity;
import com.smtc.mxmart.UtilsDialog;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


public class HomeFragment extends Fragment {

    TextView categorytitletxt1, subcategorytitletxt1, pricetxt1;
    TextView categorytitletxt2, subcategorytitletxt2, pricetxt2;
    TextView categorytitletxt3, subcategorytitletxt3, pricetxt3;
    TextView categorytitletxt4, subcategorytitletxt4, pricetxt4;
    TextView categorytitletxt5, subcategorytitletxt5, pricetxt5;
    TextView categorytitletxt6, subcategorytitletxt6, pricetxt6;
    UtilsDialog util = new UtilsDialog();

    ArrayList<String> locationnameal = new ArrayList<String>();
    ArrayList<String> msingotal = new ArrayList<String>();
    ArrayList<String> billetal = new ArrayList<String>();
    ArrayList<String> spongeal = new ArrayList<String>();
    ArrayList<String> spongeirondrcloal = new ArrayList<String>();
    String tag_json_obj = "json_obj_req";

    ArrayList<String> categoryal = new ArrayList<String>();
    ArrayList<String> subcategoryal = new ArrayList<String>();
    ArrayList<String> rateal = new ArrayList<String>();

    ArrayList<String> categoryraigarhal = new ArrayList<String>();
    ArrayList<String> subcategoryraigarhal = new ArrayList<String>();
    ArrayList<String> rateraigarhal = new ArrayList<String>();

    ArrayList<String> categorygobindgarhal = new ArrayList<String>();
    ArrayList<String> subcategorygobindgarhal = new ArrayList<String>();
    ArrayList<String> rategobindgarhal = new ArrayList<String>();

    ArrayList<String> categorymumbaial = new ArrayList<String>();
    ArrayList<String> subcategorymumbaial = new ArrayList<String>();
    ArrayList<String> ratemumbaial = new ArrayList<String>();

    ArrayList<String> categorydurgapural = new ArrayList<String>();
    ArrayList<String> subcategorydurgapural = new ArrayList<String>();
    ArrayList<String> ratedurgapural = new ArrayList<String>();

    ArrayList<String> categoryhyderabadal = new ArrayList<String>();
    ArrayList<String> subcategoryhyderabadal = new ArrayList<String>();
    ArrayList<String> ratehyderabadal = new ArrayList<String>();
    ProgressDialog pdia;
    boolean isAnimationRunning = false;
    int i = 0;
    int clickedcount = 0;
    TextView etCityName;
    Thread t;
    TextView noplatestxt,livepricetitletxt,locationtitletxt,msingottitletxt,billettitletxt,spongetitletxt,spongeirondrclotitletxt;
    TextView marginpricetxt1,marginpricetxt2,marginpricetxt3,marginpricetxt4,lastupdatedtimetitletxt,lastupdateddatetitletxt;
    Typeface source_sans_pro_normal,montserrrat_normal;
    ImageView img1,img2,img3,img4;
    TableLayout TableLayout01,TableLayout02,TableLayout03,TableLayout04;
    private ProgressBar bar;
    Context context;
    Handler handler = new Handler();
    RecyclerView homeplateslistvw;
    CustomPlatesAdap customplatesadap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view =  inflater.inflate(R.layout.fragment_home_new, container, false);

   //     SingletonActivity.isLivePriceClicked = true;
        etCityName  = (TextView)view.findViewById(R.id.etCityName);
        bar = (ProgressBar)view.findViewById(R.id.progressBar);
       // new ProgressTask().execute();
        noplatestxt = (TextView)view.findViewById(R.id.noplatestxt);
        homeplateslistvw = (RecyclerView) view.findViewById(R.id.homeplateslistvw);
        livepricetitletxt = (TextView)view.findViewById(R.id.livepricetitletxt);


        msingottitletxt =  (TextView)view.findViewById(R.id.msingottitletxt);
        billettitletxt = (TextView)view.findViewById(R.id.billettitletxt);
        spongetitletxt = (TextView)view.findViewById(R.id.spongetitletxt);
        spongeirondrclotitletxt = (TextView)view.findViewById(R.id.spongeirondrclotitletxt);

        marginpricetxt1 = (TextView)view.findViewById(R.id.marginpricetxt1);
        marginpricetxt2 = (TextView)view.findViewById(R.id.marginpricetxt2);
        marginpricetxt3 = (TextView)view.findViewById(R.id.marginpricetxt3);
        marginpricetxt4 = (TextView)view.findViewById(R.id.marginpricetxt4);

        img1 = (ImageView)view.findViewById(R.id.img1);
        img2 = (ImageView)view.findViewById(R.id.img2);
        img3 = (ImageView)view.findViewById(R.id.img3);
        img4 = (ImageView)view.findViewById(R.id.img4);

      //  categorytitletxt1 = (TextView) view.findViewById(R.id.categorytitletxt1);
      //  subcategorytitletxt1 = (TextView) view.findViewById(R.id.subcategorytitletxt1);
        pricetxt1 = (TextView) view.findViewById(R.id.pricetxt1);

     //   categorytitletxt2 = (TextView) view.findViewById(R.id.categorytitletxt2);
     //   subcategorytitletxt2 = (TextView) view.findViewById(R.id.subcategorytitletxt2);
        pricetxt2 = (TextView) view.findViewById(R.id.pricetxt2);

     //   categorytitletxt3 = (TextView) view.findViewById(R.id.categorytitletxt3);
     //   subcategorytitletxt3 = (TextView) view.findViewById(R.id.subcategorytitletxt3);
        pricetxt3 = (TextView) view.findViewById(R.id.pricetxt3);

     //   categorytitletxt4 = (TextView) view.findViewById(R.id.categorytitletxt4);
     //   subcategorytitletxt4 = (TextView) view.findViewById(R.id.subcategorytitletxt4);
        pricetxt4 = (TextView) view.findViewById(R.id.pricetxt4);

        TableLayout01 = (TableLayout)view.findViewById(R.id.TableLayout01);
        TableLayout02 = (TableLayout)view.findViewById(R.id.TableLayout02);
        TableLayout03 = (TableLayout)view.findViewById(R.id.TableLayout03);
        TableLayout04 = (TableLayout)view.findViewById(R.id.TableLayout04);

        lastupdateddatetitletxt = view.findViewById(R.id.lastupdateddatetitletxt);
        lastupdatedtimetitletxt = view.findViewById(R.id.lastupdatedtimetitletxt);




        source_sans_pro_normal = Typeface.createFromAsset(getActivity().getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");
        montserrrat_normal = Typeface.createFromAsset(getActivity().getAssets(), "montserrat/Montserrat-Regular.ttf");

        livepricetitletxt.setTypeface(source_sans_pro_normal);

        etCityName.setTypeface(montserrrat_normal);
       // msingottitletxt.setTypeface(montserrrat_normal);
      //  billettitletxt.setTypeface(montserrrat_normal);
       // spongetitletxt.setTypeface(montserrrat_normal);
      //  spongeirondrclotitletxt.setTypeface(montserrrat_normal);
        marginpricetxt1.setTypeface(montserrrat_normal);
        marginpricetxt2.setTypeface(montserrrat_normal);
        marginpricetxt3.setTypeface(montserrrat_normal);
        marginpricetxt4.setTypeface(montserrrat_normal);

        TableLayout01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    Intent i = new Intent(getActivity(), LivePriceGraphActivity.class);
                    SingletonActivity.graphtitle = "Ingots";
                    SingletonActivity.fromlivepricegraph = false;
                    startActivity(i);



            }
        });

        TableLayout02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(getActivity(), LivePriceGraphActivity.class);
                SingletonActivity.graphtitle = "Billets";
                SingletonActivity.fromlivepricegraph = false;
                startActivity(i);


            }
        });

        TableLayout03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getActivity(), LivePriceGraphActivity.class);
                SingletonActivity.graphtitle = "Sponge Iron";
                SingletonActivity.fromlivepricegraph = false;
                startActivity(i);

            }
        });

        TableLayout04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getActivity(), LivePriceGraphActivity.class);
                SingletonActivity.graphtitle = "Pellets";
                SingletonActivity.fromlivepricegraph = false;
                startActivity(i);

            }
        });


        pricetxt1.setTypeface(montserrrat_normal);


        pricetxt2.setTypeface(montserrrat_normal);


        pricetxt3.setTypeface(montserrrat_normal);


        pricetxt4.setTypeface(montserrrat_normal);


       SingletonActivity.isPassedFromLogin=false;



        if(SingletonActivity.frombuycurrentenquiry == true)
        {

            System.out.println("IN HOME ACTIVITY1==============================================1");
        }

        if (SingletonActivity.isPassedFromLogin == true) {

            System.out.println("IN HOME ACTIVITY1==============================================2");
        }

        if (SingletonActivity.backfromeditpersonaldetails == true) {

            System.out.println("IN HOME ACTIVITY1==============================================3");
        }

        if (SingletonActivity.backfromeditbusinessdetails == true) {

            System.out.println("IN HOME ACTIVITY1==============================================4");
        }

        if (SingletonActivity.backfromeditcontactdetails == true) {

            System.out.println("IN HOME ACTIVITY1==============================================5");
        }

        if (SingletonActivity.backfromeditinstalledcapacitydetails == true) {

            System.out.println("IN HOME ACTIVITY1==============================================6");
        }

        if (SingletonActivity.backfromeditreferencedetails == true) {

            System.out.println("IN HOME ACTIVITY1==============================================7");
        }

        if (SingletonActivity.updatedailylimitfromtrade == true) {

            System.out.println("IN HOME ACTIVITY1==============================================8");
        }

        if (SingletonActivity.setdailylimitfromtrade == true) {

            System.out.println("IN HOME ACTIVITY1==============================================9");
        }

        if (SingletonActivity.fromaddnewoffer == true) {

            System.out.println("IN HOME ACTIVITY1==============================================10");
        }

        if (SingletonActivity.fromupdatenewoffer == true) {

            System.out.println("IN HOME ACTIVITY1=============================================11");
        }

        if (SingletonActivity.fromviewlivetrade == true) {

            System.out.println("IN HOME ACTIVITY1==============================================12");
        }

        if(SingletonActivity.frombackofsellerdispatch == true)
        {

            System.out.println("IN HOME ACTIVITY1==============================================13");
        }


        if(SingletonActivity.frombackofsellerpayment == true)
        {

            System.out.println("IN HOME ACTIVITY1==============================================14");
        }

        if(SingletonActivity.frombackofbuyerdispatch == true)
        {

            System.out.println("IN HOME ACTIVITY1==============================================15");
        }


        if(SingletonActivity.frombackofbuyerpayment == true)
        {

            System.out.println("IN HOME ACTIVITY1==============================================16");
        }

        if(SingletonActivity.isNewEnquiryClicked == true)
        {

            System.out.println("IN HOME ACTIVITY1==============================================17");
        }

        if(SingletonActivity.isSellerAcceptedClicked == true) {

            System.out.println("IN HOME ACTIVITY1==============================================18");
        }

        if(SingletonActivity.isSpTradeSellerClicked == true) {

            System.out.println("IN HOME ACTIVITY1==============================================19");
        }

        if(SingletonActivity.isSpTradeBuyerClicked == true) {

            System.out.println("IN HOME ACTIVITY1==============================================20");
        }

        if(SingletonActivity.isLiveSalesClicked == true) {

            System.out.println("IN HOME ACTIVITY1==============================================21");
        }


        if(SingletonActivity.isLivePriceClicked == true) {

            System.out.println("IN HOME ACTIVITY1==============================================22");
           // selectFragment(SingletonActivity.menu.getItem(0));

        }

        System.out.println("IN HOME ACTIVITYSF1==============================================8"+SingletonActivity.updatedailylimitfromtrade);
        System.out.println("IN HOME ACTIVITYSF1==============================================9"+SingletonActivity.setdailylimitfromtrade);
        System.out.println("IN HOME ACTIVITYSF1==============================================10"+SingletonActivity.fromaddnewoffer);
        System.out.println("IN HOME ACTIVITYSF1==============================================12"+SingletonActivity.fromviewlivetrade);
        System.out.println("IN HOME ACTIVITYSF1==============================================21"+SingletonActivity.isLiveSalesClicked);
        System.out.println("IN HOME ACTIVITYSF1==============================================22"+SingletonActivity.isLivePriceClicked);

        if(NetworkUtility.checkConnectivity(getActivity())){
            String GetLivePriceUrl = APIName.URL+"/home/getLivePriceNew";
            System.out.println("GET LIVE PRICE URL IS---"+ GetLivePriceUrl);
            GetLivePriceNewAPI(GetLivePriceUrl);

        }
        else{
            util.dialog(getActivity(), "Please check your internet connection.");
        }

        if (NetworkUtility.checkConnectivity(getActivity())) {
            String HomePlatesListUrl = APIName.URL + "/livetrading/materialListHome";
            System.out.println("HOME PLATES LIST URL IS---" + HomePlatesListUrl);
            HomePlatesListAPI(HomePlatesListUrl);

        } else {
            util.dialog(getActivity(), "Please check your internet connection.");
        }

        return  view;

    }

    private void HomePlatesListAPI(String url) {

       /* pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();
*/
      //  progressBarFinished.setVisibility(View.VISIBLE);
        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {

                        // pdia.dismiss();

                        handler.postDelayed(new Runnable() {
                            public void run() {

                               // progressBarFinished.setVisibility(View.GONE);
                                System.out.println("RESPONSE OF HOME PLATES LIST API IS---" + response);


                                JSONObject HomePlatesListJson = null;
                                try {
                                    HomePlatesListJson = new JSONObject(response);


                                    String statusstr = HomePlatesListJson.getString("status");

                                    if (statusstr.equalsIgnoreCase("true")) {

                                        noplatestxt.setVisibility(View.GONE);
                                        homeplateslistvw.setVisibility(View.VISIBLE);



                                          JSONArray HomePlatesListJSONArray = HomePlatesListJson.getJSONArray("material_lists");


                                                if (getActivity() != null) {

                                                    SingletonActivity.HomePlatesListJSONArray =  HomePlatesListJSONArray;




                                                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                                                    layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                                                    homeplateslistvw.setLayoutManager(layoutManager);

                                                  //  MyAdapter adapter = new MyAdapter(myDataset);
                                                 //   recyclerView.setAdapter(adapter);

                                                    customplatesadap = new CustomPlatesAdap(getActivity(), SingletonActivity.HomePlatesListJSONArray);
                                                    homeplateslistvw.setAdapter(customplatesadap);

                                                    /*final int radius = getResources().getDimensionPixelSize(R.dimen.home_dots_radius);
                                                    final int dotsHeight = getResources().getDimensionPixelSize(R.dimen.home_dots_height);
                                                    final int color = ContextCompat.getColor(getContext(), R.color.colorPrimary);*/
                                                    homeplateslistvw.addItemDecoration(new CirclePagerIndicatorDecoration());
                                                    new PagerSnapHelper().attachToRecyclerView(homeplateslistvw);
                                                }









                              /*  livetradelistvw.invalidate();
                                customadap.notifyDataSetChanged();*/


                                    } else {

                                        noplatestxt.setVisibility(View.VISIBLE);
                                        homeplateslistvw.setVisibility(View.GONE);

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    // pdia.dismiss();
                                }
                            }
                        }, 3000); // 3000 milliseconds delay






                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        //pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        noplatestxt.setVisibility(View.VISIBLE);
                                        homeplateslistvw.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {

                            if(getActivity() != null) {
                                util.dialog(getActivity(), "Some Error Occured,Please try after some time");
                            }
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("home plates list params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        System.out.println("SingletonActivity.clickedcount no.=====" + SingletonActivity.clickedcount);

        if (SingletonActivity.clickedcount == 0) {
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), hurlStack);
            requestQueue.add(stringRequest);
        } else {
            SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }

    }

    public class CustomPlatesAdap extends RecyclerView.Adapter<CustomPlatesAdap.MyView> {
        private ArrayList<String> data;
        private Context c;
        LayoutInflater inflater = null;
        private JSONArray HomePlatesListJSONArray;


        public class MyView extends RecyclerView.ViewHolder {

            TextView platename,thickness,make,grade,locationtxt;
            ImageView steelimg;
            RelativeLayout moredetailsrelative,callrelative;

            public MyView(View view) {
                super(view);




                steelimg = (ImageView) view.findViewById(R.id.steelimg);
                platename = (TextView) view.findViewById(R.id.platename);
                thickness = (TextView)view.findViewById(R.id.thickness);
                make = (TextView) view.findViewById(R.id.make);
                grade = (TextView)view.findViewById(R.id.grade);
                locationtxt = (TextView)view.findViewById(R.id.locationtxt);
                callrelative = (RelativeLayout)view.findViewById(R.id.callrelative);
                moredetailsrelative = (RelativeLayout)view.findViewById(R.id.moredetailsrelative);

            }
        }



        public CustomPlatesAdap(Context mainActivity,JSONArray homePlatesListJSONArray)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.HomePlatesListJSONArray = homePlatesListJSONArray;
            inflater = (LayoutInflater)c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

       /* @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return HomePlatesListJSONArray.length();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }*/

        @NonNull
        @Override
        public MyView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_plates_list_row_horizontal, parent, false);

            return new MyView(itemView);

          //  return null;
        }

        @Override
        public void onBindViewHolder(@NonNull final MyView holder, int position) {

            try {



                holder.platename.setText(HomePlatesListJSONArray.getJSONObject(position).getString("material_name"));
                holder.thickness.setText("Thickness: "+ HomePlatesListJSONArray.getJSONObject(position).getString("material_thickness"));
                holder.make.setText("Make: "+ HomePlatesListJSONArray.getJSONObject(position).getString("material_brand"));
                holder.grade.setText("Grade: "+HomePlatesListJSONArray.getJSONObject(position).getString("material_grade"));
                holder.locationtxt.setText(HomePlatesListJSONArray.getJSONObject(position).getString("material_location"));

                final String material_id = HomePlatesListJSONArray.getJSONObject(position).getString("material_id");
                final String material_name = HomePlatesListJSONArray.getJSONObject(position).getString("material_name");
                final String material_thickness = HomePlatesListJSONArray.getJSONObject(position).getString("material_thickness");
                final String material_brand = HomePlatesListJSONArray.getJSONObject(position).getString("material_brand");
                final String material_standard = HomePlatesListJSONArray.getJSONObject(position).getString("material_standard");
                final String material_grade = HomePlatesListJSONArray.getJSONObject(position).getString("material_grade");
                final String material_image = HomePlatesListJSONArray.getJSONObject(position).getString("material_image");
                final String material_size = HomePlatesListJSONArray.getJSONObject(position).getString("material_size");
                final String material_specification = HomePlatesListJSONArray.getJSONObject(position).getString("material_specification");
                final String material_location = HomePlatesListJSONArray.getJSONObject(position).getString("material_location");
                final String material_size_excel = HomePlatesListJSONArray.getJSONObject(position).getString("material_size_excel");


                String imageURL = APIName.IMAGE_URL + "/" + material_image;
                System.out.println("imageURL IS---" + imageURL);



           //     Picasso.with(activity).load(url).transform(new RoundedCornersTransform(this)).into(imageView);

               PicassoTrustAll.getInstance(getActivity())
                        .load(imageURL)
                       .transform(new RoundedCornersTransform())
                        .into(holder.steelimg);


                   if(HomePlatesListJSONArray.getJSONObject(position).getString("material_size").equalsIgnoreCase("null"))
                   {

                       /*    holder.moredetailsrelative.setEnabled(false);
                           holder.moredetailsrelative.setClickable(false);
                           holder.moredetailsrelative.setBackgroundResource(R.drawable.light_red_btn_rounded_rectangle);
*/
                           //Toast.makeText(getActivity(),"DISABLE position: "+ position,Toast.LENGTH_LONG).show();

                       holder.moredetailsrelative.setEnabled(true);
                       holder.moredetailsrelative.setClickable(true);
                       holder.moredetailsrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);


                   }
                       else
                       {
                           holder.moredetailsrelative.setEnabled(true);
                           holder.moredetailsrelative.setClickable(true);
                           holder.moredetailsrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);

                           //  Toast.makeText(getActivity(),"CLICKABLE position: "+ position,Toast.LENGTH_LONG).show();
                       }







                holder.moredetailsrelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(getActivity(), EnquiryActivity.class);
                        i.putExtra("materialId",material_id);
                        i.putExtra("materialname",material_name);
                        i.putExtra("materialthickness",material_thickness);
                        i.putExtra("materialbrand",material_brand);
                        i.putExtra("materialstandard",material_standard);
                        i.putExtra("materialgrade",material_grade);
                        i.putExtra("materialimage",material_image);
                        i.putExtra("materialsize",material_size);
                        i.putExtra("materialspec",material_specification);
                        i.putExtra("materialloc",material_location);
                        i.putExtra("material_size_excel",material_size_excel);
                        SingletonActivity.enquiryfromhomepage = true;
                        SingletonActivity.enquiryfromplatespage = false;

                        startActivity(i);
                    }
                });

                holder.callrelative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //Sunny number---
                        //   String numberstr = "1234567890";
                        String numberstr = "9827423445";
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:"+numberstr));
                        startActivity(callIntent);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

      /*  @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }
*/
        @Override
        public int getItemCount() {
            return HomePlatesListJSONArray.length();
        }

        /*@Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub



            final CustomHomePlatesHolder customHomePlatesHolder = new CustomHomePlatesHolder();

            final View rowView;


            rowView = inflater.inflate(R.layout.home_plates_list_row, null);

            customHomePlatesHolder.steelimg = (ImageView) rowView.findViewById(R.id.steelimg);
            customHomePlatesHolder.platetext = (TextView) rowView.findViewById(R.id.platetext);
            customHomePlatesHolder.platecmpnytext = (TextView)rowView.findViewById(R.id.platecmpnytext);
            customHomePlatesHolder.platestandardtext = (TextView) rowView.findViewById(R.id.platestandardtext);
            customHomePlatesHolder.platesizetext = (TextView)rowView.findViewById(R.id.platesizetext);
            customHomePlatesHolder.locationtxt = (TextView)rowView.findViewById(R.id.locationtxt);
            customHomePlatesHolder.callrelative = (RelativeLayout)rowView.findViewById(R.id.callrelative);
            customHomePlatesHolder.moredetailsrelative = (RelativeLayout)rowView.findViewById(R.id.moredetailsrelative);






            return rowView;

        }
*/

    }


  /*  public class CustomHomePlatesHolder {
        TextView platetext,platecmpnytext,platestandardtext,platesizetext,locationtxt;
        ImageView steelimg;
        RelativeLayout moredetailsrelative,callrelative;



    }
*/




    private void GetLivePriceNewAPI(String url) {

     /*   pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();*/

      //  getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
          //      WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        bar.setVisibility(View.VISIBLE);


        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {

                      //  pdia.dismiss();


                     //   getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //dialog.dismiss();
                                bar.setVisibility(View.GONE);

                                locationnameal.clear();
                                msingotal.clear();
                                billetal.clear();
                                spongeal.clear();
                                spongeirondrcloal.clear();


                                System.out.println("RESPONSE OF GetLivePriceNewAPI IS---" + response);


                                JSONObject SellerTodaysOfferJson = null;
                                try {
                                    SellerTodaysOfferJson = new JSONObject(response);


                                    String statusstr = SellerTodaysOfferJson.getString("status");
                         /*   String promocodestr = SellerTodaysOfferJson.getString("promo_code");
                            SingletonActivity.promocode = promocodestr;*/

                                    if(statusstr.equalsIgnoreCase("true"))
                                    {

                                        String last_update_date_str = SellerTodaysOfferJson.getString("last_update_date");
                                        String last_update_time_str = SellerTodaysOfferJson.getString("last_update_time");

                                        lastupdateddatetitletxt.setText(last_update_date_str);
                                        lastupdatedtimetitletxt.setText(last_update_time_str);



                                        final JSONArray livePriceJsonArray = SellerTodaysOfferJson.getJSONArray("livePrice");
                                        System.out.println("LIVE PRICE JSONARRAY IS---" + livePriceJsonArray);

                                        for(int i = 0; i < livePriceJsonArray.length();i++)
                                        {
                                            if(!livePriceJsonArray.getJSONObject(i).getString("location_name").equalsIgnoreCase("")) {



                                                locationnameal.add(livePriceJsonArray.getJSONObject(i).getString("location_name"));
                                            }
                                   /* else
                                    {
                                        locationnameal.add("");
                                    }
*/
                                            if(!livePriceJsonArray.getJSONObject(i).getString("msingot").equalsIgnoreCase("")) {
                                                msingotal.add(livePriceJsonArray.getJSONObject(i).getString("msingot"));
                                            }
                                            else
                                            {
                                                msingotal.add("NA");
                                            }

                                            if(!livePriceJsonArray.getJSONObject(i).getString("billet").equalsIgnoreCase("")) {
                                                billetal.add(livePriceJsonArray.getJSONObject(i).getString("billet"));
                                            }
                                            else
                                            {
                                                billetal.add("NA");
                                            }

                                            if(!livePriceJsonArray.getJSONObject(i).getString("spongeiron").equalsIgnoreCase("")) {
                                                spongeal.add(livePriceJsonArray.getJSONObject(i).getString("spongeiron"));
                                            }
                                            else
                                            {
                                                spongeal.add("NA");
                                            }

                                            if(!livePriceJsonArray.getJSONObject(i).getString("spongeironDRCLO").equalsIgnoreCase("")) {
                                                spongeirondrcloal.add(livePriceJsonArray.getJSONObject(i).getString("spongeironDRCLO"));
                                            }
                                            else
                                            {
                                                spongeirondrcloal.add("NA");
                                            }

                                        }



                                        isAnimationRunning = true;

                                        if (getActivity() == null) {
                                            // isAnimationRunning == true
                                            return;
                                        }
                                        else {

                                            etCityName.setVisibility(View.VISIBLE);
                                            if (locationnameal.get(i).equalsIgnoreCase("NA")) {
                                                etCityName.setVisibility(View.INVISIBLE);
                                                etCityName.setText("NA");
                                            } else {
                                                etCityName.setText(locationnameal.get(i));



                                                                       /* img1 = (ImageView)view.findViewById(R.id.img1);
                                                                        img2 = (ImageView)view.findViewById(R.id.img2);
                                                                        img3 = (ImageView)view.findViewById(R.id.img3);
                                                                        img4 = (ImageView)view.findViewById(R.id.img4);*/


                                            }



                                            pricetxt1.setVisibility(View.VISIBLE);

                                            if (msingotal.get(i).equalsIgnoreCase("NA")) {
                                                // pricetxt1.setVisibility(View.INVISIBLE);
                                                pricetxt1.setText("NA");
                                                marginpricetxt1.setText("");
                                                img1.setImageResource(0);
                                            } else {
                                                // pricetxt1.setText(msingotal.get(i));

                                                String str1 = msingotal.get(i);
                                                String[] splitStr1 = str1.split("\\s+");

                                                pricetxt1.setText(splitStr1[0]);

                                                // Toast.makeText(getActivity(),"splitStr=="+ splitStr.length,Toast.LENGTH_SHORT).show();

                                                if(splitStr1.length>1) {

                                                    int marginpriceval1 = Integer.parseInt(splitStr1[1]);

                                                    if (marginpriceval1 > 0) {
                                                        img1.setImageResource(R.mipmap.up);
                                                        marginpricetxt1.setText("(" + splitStr1[1] + ")");
                                                    } else {
                                                        if (marginpriceval1 == 0) {
                                                            marginpricetxt1.setVisibility(View.INVISIBLE);
                                                            img1.setVisibility(View.INVISIBLE);
                                                        } else {
                                                            img1.setImageResource(R.mipmap.down);
                                                            int x = Math.abs(marginpriceval1);

                                                            marginpricetxt1.setText("(" + Integer.toString(x) + ")");
                                                        }
                                                    }

                                                    if (splitStr1[1].equalsIgnoreCase("0")) {
                                                        marginpricetxt1.setVisibility(View.INVISIBLE);
                                                        img1.setVisibility(View.INVISIBLE);
                                                    }
                                                }



                                                                       /* img1 = (ImageView)view.findViewById(R.id.img1);
                                                                        img2 = (ImageView)view.findViewById(R.id.img2);
                                                                        img3 = (ImageView)view.findViewById(R.id.img3);
                                                                        img4 = (ImageView)view.findViewById(R.id.img4);*/


                                            }


                                            pricetxt2.setVisibility(View.VISIBLE);


                                            if (billetal.get(i).equalsIgnoreCase("NA")) {
                                                //pricetxt2.setVisibility(View.INVISIBLE);
                                                pricetxt2.setText("NA");
                                                marginpricetxt2.setText("");
                                                img2.setImageResource(0);


                                            } else {
                                                // pricetxt2.setText(billetal.get(i));

                                                String str2 = billetal.get(i);
                                                String[] splitStr2 = str2.split("\\s+");

                                                pricetxt2.setText(splitStr2[0]);
                                             //   Toast.makeText(getActivity(),"loc 2",Toast.LENGTH_SHORT).show();
                                                if(splitStr2.length>1) {
                                                    int marginpriceval2 = Integer.parseInt(splitStr2[1]);
                                                 //   Toast.makeText(getActivity(),"loc 3",Toast.LENGTH_SHORT).show();

                                                    if (marginpriceval2 > 0) {
                                                     //   Toast.makeText(getActivity(),"loc 4"+ splitStr2.length ,Toast.LENGTH_SHORT).show();
                                                        img2.setImageResource(R.mipmap.up);
                                                        marginpricetxt2.setText("(" + splitStr2[1] + ")");
                                                    } else {
                                                        if (marginpriceval2 == 0) {
                                                         //   Toast.makeText(getActivity(),"loc 5",Toast.LENGTH_SHORT).show();
                                                            marginpricetxt2.setVisibility(View.INVISIBLE);
                                                            img2.setVisibility(View.INVISIBLE);
                                                        } else {
                                                         //   Toast.makeText(getActivity(),"loc 6",Toast.LENGTH_SHORT).show();
                                                            img2.setImageResource(R.mipmap.down);
                                                            int x = Math.abs(marginpriceval2);

                                                            marginpricetxt2.setText("(" + Integer.toString(x) + ")");
                                                        }
                                                    }

                                                   // Toast.makeText(getActivity(),"loc 7::"+ splitStr2[1],Toast.LENGTH_SHORT).show();

                                                    if (splitStr2[1].equalsIgnoreCase("0")) {

                                                      //  Toast.makeText(getActivity(),"loc 8",Toast.LENGTH_SHORT).show();
                                                        marginpricetxt2.setVisibility(View.INVISIBLE);
                                                        img2.setVisibility(View.INVISIBLE);
                                                    }

                                                }
                                            }

                                            pricetxt3.setVisibility(View.VISIBLE);

                                            if (spongeal.get(i).equalsIgnoreCase("NA")) {
                                                //  pricetxt3.setVisibility(View.INVISIBLE);
                                                pricetxt3.setText("NA");
                                                marginpricetxt3.setText("");
                                                img3.setImageResource(0);
                                            } else {
                                                // pricetxt3.setText(spongeal.get(i));

                                                String str3 = spongeal.get(i);
                                                String[] splitStr3 = str3.split("\\s+");

                                                pricetxt3.setText(splitStr3[0]);

                                                // if(splitStr[1].equalsIgnoreCase(""))
                                                if(splitStr3.length>1) {
                                                    int marginpriceval3 = Integer.parseInt(splitStr3[1]);

                                                    if (marginpriceval3 > 0) {
                                                        img3.setImageResource(R.mipmap.up);
                                                        marginpricetxt3.setText("(" + splitStr3[1] + ")");
                                                    } else {
                                                        if (marginpriceval3 == 0) {
                                                            marginpricetxt3.setVisibility(View.INVISIBLE);
                                                            img3.setVisibility(View.INVISIBLE);
                                                        } else {
                                                            img3.setImageResource(R.mipmap.down);
                                                            int x = Math.abs(marginpriceval3);

                                                            marginpricetxt3.setText("(" + Integer.toString(x) + ")");
                                                        }
                                                    }

                                                    if (splitStr3[1].equalsIgnoreCase("0")) {
                                                        marginpricetxt3.setVisibility(View.INVISIBLE);
                                                        img3.setVisibility(View.INVISIBLE);
                                                    }
                                                }
                                            }




                                            pricetxt4.setVisibility(View.VISIBLE);

                                            if (spongeirondrcloal.get(i).equalsIgnoreCase("NA")) {
                                                // pricetxt4.setVisibility(View.INVISIBLE);
                                                pricetxt4.setText("NA");
                                                marginpricetxt4.setText("");
                                                img4.setImageResource(0);

                                            } else {
                                                //  pricetxt4.setText(spongeirondrcloal.get(i));

                                                String str4 = spongeirondrcloal.get(i);
                                                String[] splitStr4 = str4.split("\\s+");

                                                pricetxt4.setText(splitStr4[0]);
                                                if(splitStr4.length>1) {
                                                    int marginpriceval4 = Integer.parseInt(splitStr4[1]);

                                                    if (marginpriceval4 > 0) {
                                                        img4.setImageResource(R.mipmap.up);
                                                        marginpricetxt4.setText("(" + splitStr4[1] + ")");
                                                    } else {
                                                        if (marginpriceval4 == 0) {
                                                            marginpricetxt4.setVisibility(View.INVISIBLE);
                                                            img4.setVisibility(View.INVISIBLE);
                                                        } else {
                                                            img4.setImageResource(R.mipmap.down);
                                                            int x = Math.abs(marginpriceval4);

                                                            marginpricetxt4.setText("(" + Integer.toString(x) + ")");
                                                        }
                                                    }

                                                    if (splitStr4[1].equalsIgnoreCase("0")) {
                                                        marginpricetxt4.setVisibility(View.INVISIBLE);
                                                        img4.setVisibility(View.INVISIBLE);
                                                    }
                                                }

                                            }
                                            i = 1;

                                            if (isAnimationRunning == true) {

                                                //    if(getActivity()!=null) {
                                                t = new Thread() {

                                                    @Override
                                                    public void run() {
                                                        try {


                                                            while (!isInterrupted()) {

                                                                //   Thread.sleep(15000);
                                                                Thread.sleep(6000);

                                                                if (getActivity() != null) {


                                                                    getActivity().runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            // update TextView here!


                                                                            etCityName.setVisibility(View.VISIBLE);
                                                                            etCityName.setText(locationnameal.get(i));

                                                                   /* if (locationnameal.get(i).equalsIgnoreCase("")) {
                                                                        etCityName.setVisibility(View.INVISIBLE);
                                                                        etCityName.setText("NA");
                                                                    } else {
                                                                        etCityName.setText(locationnameal.get(i));




                                                                    }
*/


                                                                            pricetxt1.setVisibility(View.VISIBLE);

                                                                            if (msingotal.get(i).equalsIgnoreCase("NA")) {
                                                                                pricetxt1.setVisibility(View.INVISIBLE);
                                                                                pricetxt1.setText("NA");

                                                                                marginpricetxt1.setText("");
                                                                                img1.setImageResource(0);

                                                                            } else {
                                                                                // pricetxt1.setText(msingotal.get(i));

                                                                                String str1 = msingotal.get(i);
                                                                                String[] splitStr1 = str1.split("\\s+");


                                                                                pricetxt1.setText(splitStr1[0]);



                                                                                System.out.println("marginpriceval1 2==="+ splitStr1.length);

                                                                                if(splitStr1.length == 2) {


                                                                                    int marginpriceval1 = Integer.parseInt(splitStr1[1]);

                                                                                    if (marginpriceval1 > 0) {
                                                                                        img1.setImageResource(R.mipmap.up);
                                                                                        marginpricetxt1.setText("(" + splitStr1[1] + ")");
                                                                                    } else {
                                                                                        if (marginpriceval1 == 0) {
                                                                                            marginpricetxt1.setVisibility(View.INVISIBLE);
                                                                                            img1.setVisibility(View.INVISIBLE);
                                                                                        } else {
                                                                                            img1.setImageResource(R.mipmap.down);
                                                                                            int x = Math.abs(marginpriceval1);

                                                                                            marginpricetxt1.setText("(" + Integer.toString(x) + ")");
                                                                                        }
                                                                                    }

                                                                                    if (splitStr1[1].equalsIgnoreCase("0")) {
                                                                                        marginpricetxt1.setVisibility(View.INVISIBLE);
                                                                                        img1.setVisibility(View.INVISIBLE);
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    marginpricetxt1.setVisibility(View.INVISIBLE);
                                                                                    img1.setVisibility(View.INVISIBLE);
                                                                                }



                                                                       /* img1 = (ImageView)view.findViewById(R.id.img1);
                                                                        img2 = (ImageView)view.findViewById(R.id.img2);
                                                                        img3 = (ImageView)view.findViewById(R.id.img3);
                                                                        img4 = (ImageView)view.findViewById(R.id.img4);*/


                                                                            }





                                                                            pricetxt2.setVisibility(View.VISIBLE);

                                                                            if (billetal.get(i).equalsIgnoreCase("NA")) {




                                                                                pricetxt2.setVisibility(View.INVISIBLE);
                                                                                pricetxt2.setText("NA");
                                                                                marginpricetxt2.setText("");
                                                                                img2.setImageResource(0);

                                                                              //  Toast.makeText(getActivity(),"if index==="+ billetal.get(i),Toast.LENGTH_SHORT).show();
                                                                                //Toast.makeText(getActivity(),"loop NA IF",Toast.LENGTH_SHORT).show();



                                                                            } else {
                                                                                // pricetxt2.setText(billetal.get(i));

                                                                                String str2 = billetal.get(i);
                                                                                String[] splitStr2 = str2.split("\\s+");

                                                                               // Toast.makeText(getActivity(),"loop NA ELSE",Toast.LENGTH_SHORT).show();

                                                                                pricetxt2.setText(splitStr2[0]);

                                                                                if(splitStr2.length == 2) {
                                                                                    int marginpriceval2 = Integer.parseInt(splitStr2[1]);

                                                                                    if (marginpriceval2 > 0) {
                                                                                        img2.setImageResource(R.mipmap.up);
                                                                                        marginpricetxt2.setText("(" + splitStr2[1] + ")");
                                                                                    } else {
                                                                                        if (marginpriceval2 == 0) {
                                                                                            marginpricetxt2.setVisibility(View.INVISIBLE);
                                                                                            img2.setVisibility(View.INVISIBLE);
                                                                                        } else {
                                                                                            img2.setImageResource(R.mipmap.down);
                                                                                            int x = Math.abs(marginpriceval2);

                                                                                            marginpricetxt2.setText("(" + Integer.toString(x) + ")");
                                                                                        }
                                                                                    }

                                                                                    if (splitStr2[1].equalsIgnoreCase("0")) {
                                                                                        marginpricetxt2.setVisibility(View.INVISIBLE);
                                                                                        img2.setVisibility(View.INVISIBLE);
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    marginpricetxt2.setVisibility(View.INVISIBLE);
                                                                                    img2.setVisibility(View.INVISIBLE);
                                                                                }


                                                                            }



                                                                            pricetxt3.setVisibility(View.VISIBLE);

                                                                            if (spongeal.get(i).equalsIgnoreCase("NA")) {
                                                                                pricetxt3.setVisibility(View.INVISIBLE);
                                                                                pricetxt3.setText("NA");
                                                                                marginpricetxt3.setText("");
                                                                                img3.setImageResource(0);
                                                                            } else {
                                                                                // pricetxt3.setText(spongeal.get(i));

                                                                                String str3 = spongeal.get(i);
                                                                                String[] splitStr3 = str3.split("\\s+");

                                                                                pricetxt3.setText(splitStr3[0]);

                                                                                if(splitStr3.length == 2) {
                                                                                    int marginpriceval3 = Integer.parseInt(splitStr3[1]);

                                                                                    if (marginpriceval3 > 0) {
                                                                                        img3.setImageResource(R.mipmap.up);
                                                                                        marginpricetxt3.setText("(" + splitStr3[1] + ")");
                                                                                    } else {
                                                                                        if (marginpriceval3 == 0) {
                                                                                            marginpricetxt3.setVisibility(View.INVISIBLE);
                                                                                            img3.setVisibility(View.INVISIBLE);
                                                                                        } else {
                                                                                            img3.setImageResource(R.mipmap.down);
                                                                                            int x = Math.abs(marginpriceval3);

                                                                                            marginpricetxt3.setText("(" + Integer.toString(x) + ")");
                                                                                        }
                                                                                    }

                                                                                    if (splitStr3[1].equalsIgnoreCase("0")) {
                                                                                        marginpricetxt3.setVisibility(View.INVISIBLE);
                                                                                        img3.setVisibility(View.INVISIBLE);
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    marginpricetxt3.setVisibility(View.INVISIBLE);
                                                                                    img3.setVisibility(View.INVISIBLE);
                                                                                }
                                                                            }




                                                                            pricetxt4.setVisibility(View.VISIBLE);

                                                                            if (spongeirondrcloal.get(i).equalsIgnoreCase("NA")) {
                                                                                pricetxt4.setVisibility(View.INVISIBLE);
                                                                                pricetxt4.setText("NA");
                                                                                marginpricetxt4.setText("");
                                                                                img4.setImageResource(0);
                                                                            } else {
                                                                                //  pricetxt4.setText(spongeirondrcloal.get(i));

                                                                                String str4 = spongeirondrcloal.get(i);
                                                                                String[] splitStr4 = str4.split("\\s+");

                                                                                pricetxt4.setText(splitStr4[0]);

                                                                                if (splitStr4.length == 2) {
                                                                                    int marginpriceval1 = Integer.parseInt(splitStr4[1]);

                                                                                    if (marginpriceval1 > 0) {
                                                                                        img4.setImageResource(R.mipmap.up);
                                                                                        marginpricetxt4.setText("(" + splitStr4[1] + ")");
                                                                                    } else {
                                                                                        if (marginpriceval1 == 0) {
                                                                                            marginpricetxt4.setVisibility(View.INVISIBLE);
                                                                                            img4.setVisibility(View.INVISIBLE);
                                                                                        } else {
                                                                                            img4.setImageResource(R.mipmap.down);
                                                                                            int x = Math.abs(marginpriceval1);

                                                                                            marginpricetxt4.setText("(" + Integer.toString(x) + ")");
                                                                                        }
                                                                                    }

                                                                                    if (splitStr4[1].equalsIgnoreCase("0")) {
                                                                                        marginpricetxt4.setVisibility(View.INVISIBLE);
                                                                                        img4.setVisibility(View.INVISIBLE);
                                                                                    }

                                                                                }
                                                                                else
                                                                                {
                                                                                    marginpricetxt4.setVisibility(View.INVISIBLE);
                                                                                    img4.setVisibility(View.INVISIBLE);
                                                                                }
                                                                            }




                                                                            //END OF HYDERABAD --------------------------i


                                                                            Animation topTobottom = AnimationUtils.loadAnimation(getActivity(), R.anim.top_bottom);


                                                                            etCityName.setDrawingCacheEnabled(true);
                                                                            etCityName.startAnimation(topTobottom);


                                                                            marginpricetxt1.setDrawingCacheEnabled(true);
                                                                            marginpricetxt1.startAnimation(topTobottom);

                                                                            img1.setDrawingCacheEnabled(true);
                                                                            img1.startAnimation(topTobottom);

                                                                            pricetxt1.setDrawingCacheEnabled(true);
                                                                            pricetxt1.startAnimation(topTobottom);

                                                                            marginpricetxt2.setDrawingCacheEnabled(true);
                                                                            marginpricetxt2.startAnimation(topTobottom);

                                                                            img2.setDrawingCacheEnabled(true);
                                                                            img2.startAnimation(topTobottom);

                                                                            pricetxt2.setDrawingCacheEnabled(true);
                                                                            pricetxt2.startAnimation(topTobottom);

                                                                            marginpricetxt3.setDrawingCacheEnabled(true);
                                                                            marginpricetxt3.startAnimation(topTobottom);

                                                                            img3.setDrawingCacheEnabled(true);
                                                                            img3.startAnimation(topTobottom);

                                                                            pricetxt3.setDrawingCacheEnabled(true);
                                                                            pricetxt3.startAnimation(topTobottom);

                                                                            marginpricetxt4.setDrawingCacheEnabled(true);
                                                                            marginpricetxt4.startAnimation(topTobottom);

                                                                            img4.setDrawingCacheEnabled(true);
                                                                            img4.startAnimation(topTobottom);

                                                                            pricetxt4.setDrawingCacheEnabled(true);
                                                                            pricetxt4.startAnimation(topTobottom);




                                                                            i = i + 1;



                                                                    new Handler().postDelayed(new Runnable() {
                                                                        @Override
                                                                        public void run() {


                                                                            if(getActivity()!=null) {


                                                                                Animation topTobottom1 = AnimationUtils.loadAnimation(getActivity(), R.anim.top_bottom1);

                                                                                etCityName.startAnimation(topTobottom1);
                                                                                img1.startAnimation(topTobottom1);
                                                                                marginpricetxt1.startAnimation(topTobottom1);
                                                                                img2.startAnimation(topTobottom1);
                                                                                marginpricetxt2.startAnimation(topTobottom1);
                                                                                img3.startAnimation(topTobottom1);
                                                                                marginpricetxt3.startAnimation(topTobottom1);
                                                                                img4.startAnimation(topTobottom1);
                                                                                marginpricetxt4.startAnimation(topTobottom1);
                                                                                pricetxt1.startAnimation(topTobottom1);
                                                                                pricetxt2.startAnimation(topTobottom1);
                                                                                pricetxt3.startAnimation(topTobottom1);
                                                                                pricetxt4.startAnimation(topTobottom1);

                                                                              /*  try {
                                                                                    Thread.sleep(5000);

                                                                                    Thread.interrupted();
                                                                                } catch (InterruptedException e) {
                                                                                    e.printStackTrace();
                                                                                }
                                                                             */

                                                                            }
                                                                        }
                                                                   }, 3000);

                                                                            // }, 9999);


                                                                            if (i == livePriceJsonArray.length()) {

                                                                                i = 0;

                                                                            }

                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        } catch (InterruptedException e) {
                                                        }
                                                    }
                                                };

                                                t.start();


                                                // }
                                            }
                                        }

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    // pdia.dismiss();

                                 //   getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            //dialog.dismiss();

                                            bar.setVisibility(View.GONE);

                                        }
                                    }, 3000); // 3000 milliseconds delay
                                }


                            }
                        }, 3000); // 3000 milliseconds delay*/




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                      //  pdia.dismiss();

                       // getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //dialog.dismiss();
                                bar.setVisibility(View.GONE);

                            }
                        }, 3000); // 3000 milliseconds delay

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            if(getActivity()!=null) {
                                util.dialog(getActivity(), "Some Error Occured,Please try after some time");
                            }
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get live PRICE NEW params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }




   /* private void GetLivePriceAPI(String url) {

        pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        pdia.dismiss();


                        rateal.clear();



                        rateraigarhal.clear();


                        rategobindgarhal.clear();


                        ratemumbaial.clear();



                        System.out.println("RESPONSE OF GET LIVE PRICE API IS---" + response);

                        try {

                            JSONObject getlivepricejson = new JSONObject(response);
                            System.out.println("GET LIVE PRICE JSON IS---" + getlivepricejson);

                            String statusstr = getlivepricejson.getString("status");

                            if (statusstr.equalsIgnoreCase("true")) {

                                JSONObject LivePriceJson = getlivepricejson.getJSONObject("livePrice");
                                System.out.println("LIVEPRICE JSON IS---" + LivePriceJson);

                                //For Raipur------------------------->

                                JSONObject RaipurJson = LivePriceJson.getJSONObject("Raipur");
                                System.out.println("RAIPUR JSON IS---" + RaipurJson);


                                String BilletRaipurJsonStr = RaipurJson.getString("Billet");
                                System.out.println("RAIPUR BILLET JSON STRING IS---" + BilletRaipurJsonStr);

                                if(BilletRaipurJsonStr.equalsIgnoreCase(""))
                                {
                                    categoryal.add("-");
                                    subcategoryal.add("-");
                                    rateal.add("-");

                                }
                                else
                                {
                                    JSONObject BilletRaipurJson = new JSONObject(BilletRaipurJsonStr);

                                    System.out.println("RAIPUR BILLET JSON IS---" + BilletRaipurJson);

                                    String categorystr = BilletRaipurJson.getString("category");
                                    // String subcategorystr = BilletRaipurJson.getString("sub_category");
                                    String ratestr = BilletRaipurJson.getString("rate");

                                    categoryal.add(categorystr);

                                    subcategoryal.add("-");
                                    rateal.add(ratestr);

                                    System.out.println("RAIPUR SUB CAT VALUE  IS---" + subcategoryal);

                                }

                                String MSIngotRaipurJsonStr = RaipurJson.getString("MS Ingot");
                                System.out.println("RAIPUR MS INGOT JSON STRING IS---" + MSIngotRaipurJsonStr);


                                if(MSIngotRaipurJsonStr.equalsIgnoreCase(""))
                                {
                                    categoryal.add("-");
                                    subcategoryal.add("-");
                                    rateal.add("-");

                                }
                                else
                                {
                                    JSONObject MSIngotRaipurJson = new JSONObject(MSIngotRaipurJsonStr);
                                    System.out.println("RAIPUR MS INGOT JSON IS---" + MSIngotRaipurJson);

                                    String categorystr = MSIngotRaipurJson.getString("category");
                                    // String subcategorystr = MSIngotRaipurJson.getString("sub_category");
                                    String ratestr = MSIngotRaipurJson.getString("rate");

                                    categoryal.add(categorystr);
                                    subcategoryal.add("-");
                                    rateal.add(ratestr);

                                }


                                String SpongeIronRaipurJsonStr = RaipurJson.getString("Sponge Iron");
                                System.out.println("RAIPUR SPONGE IRON JSON STRING IS---" + SpongeIronRaipurJsonStr);


                                if(SpongeIronRaipurJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronRaipurJson = new JSONObject(SpongeIronRaipurJsonStr);
                                    System.out.println("RAIPUR SPONGE IRON JSON IS---" + SpongeIronRaipurJson);



                                    String lumpsjsonstr = SpongeIronRaipurJson.getString("Lumps");
                                    System.out.println("RAIPUR SPONGE IRON LUMPS JSON STRING IS---" + lumpsjsonstr);

                                    if(!lumpsjsonstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsJSON = new JSONObject(lumpsjsonstr);
                                        System.out.println("RAIPUR SPONGE IRON LUMPS JSON IS---" + LumpsJSON);

                                        String categorystr = LumpsJSON.getString("category");
                                        String subcategorystr = LumpsJSON.getString("sub_category");
                                        String ratestr = LumpsJSON.getString("rate");

                                        categoryal.add(categorystr);
                                        subcategoryal.add(subcategorystr);
                                        rateal.add(ratestr);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }

                                    String finesjsonstr= SpongeIronRaipurJson.getString("Fines");
                                    System.out.println("RAIPUR SPONGE IRON FINES JSON IS---" + finesjsonstr);

                                    if(!finesjsonstr.equalsIgnoreCase("")) {

                                        JSONObject FinesJSON = new JSONObject(finesjsonstr);

                                        String categoryfinesstr = FinesJSON.getString("category");
                                        String subcategoryfinesstr = FinesJSON.getString("sub_category");
                                        String ratefinesstr = FinesJSON.getString("rate");

                                        categoryal.add(categoryfinesstr);
                                        subcategoryal.add(subcategoryfinesstr);
                                        rateal.add(ratefinesstr);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }

                                    String pelletjsonstr = SpongeIronRaipurJson.getString("Pellet");
                                    System.out.println("RAIPUR SPONGE IRON PELLET JSON IS---" + pelletjsonstr);

                                    if(!pelletjsonstr.equalsIgnoreCase("")) {

                                        JSONObject PelletJSON = new JSONObject(pelletjsonstr);

                                        String categorypelletstr = PelletJSON.getString("category");
                                        String subcategorypelletstr = PelletJSON.getString("sub_category");
                                        String ratepelletstr = PelletJSON.getString("rate");

                                        categoryal.add(categorypelletstr);
                                        subcategoryal.add(subcategorypelletstr);
                                        rateal.add(ratepelletstr);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }



                                    String mix020jsonstr = SpongeIronRaipurJson.getString("Mix 0-20");
                                    System.out.println("RAIPUR SPONGE IRON MIX 0-20 JSON IS---" + mix020jsonstr);

                                    if(!mix020jsonstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020JSON = new JSONObject(mix020jsonstr);

                                        String categorymix020str = Mix020JSON.getString("category");
                                        String subcategorymix020str = Mix020JSON.getString("sub_category");
                                        String ratemix020str = Mix020JSON.getString("rate");

                                        categoryal.add(categorymix020str);
                                        subcategoryal.add(subcategorymix020str);
                                        rateal.add(ratemix020str);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }


                                }


                                String SpongeIronDRCLORaipurJsonStr = RaipurJson.getString("Sponge Iron DR-CLO");
                                System.out.println("RAIPUR SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLORaipurJsonStr);


                                if(SpongeIronDRCLORaipurJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLORaipurJson = new JSONObject(SpongeIronDRCLORaipurJsonStr);
                                    System.out.println("RAIPUR SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLORaipurJson);


                                    String LumpsDRCLOJSONstr = SpongeIronDRCLORaipurJson.getString("Lumps DR-CLO");
                                    System.out.println("RAIPUR SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLOJSONstr);

                                    if(!LumpsDRCLOJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLOJSON = new JSONObject(LumpsDRCLOJSONstr);

                                        String categorystr = LumpsDRCLOJSON.getString("category");
                                        String subcategorystr = LumpsDRCLOJSON.getString("sub_category");
                                        String ratestr = LumpsDRCLOJSON.getString("rate");

                                        categoryal.add(categorystr);
                                        subcategoryal.add(subcategorystr);
                                        rateal.add(ratestr);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }

                                    String FinesDRCLOJSONstr = SpongeIronDRCLORaipurJson.getString("Fines DR-CLO");
                                    System.out.println("RAIPUR SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLOJSONstr);

                                    if(!FinesDRCLOJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLOJSON = new JSONObject(FinesDRCLOJSONstr);

                                        String categoryfinesdrclostr = FinesDRCLOJSON.getString("category");
                                        String subcategoryfinesdrclostr = FinesDRCLOJSON.getString("sub_category");
                                        String ratefinesdrclostr = FinesDRCLOJSON.getString("rate");

                                        categoryal.add(categoryfinesdrclostr);
                                        subcategoryal.add(subcategoryfinesdrclostr);
                                        rateal.add(ratefinesdrclostr);
                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }

                                    String Mix020DRCLOJSONstr = SpongeIronDRCLORaipurJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("RAIPUR SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLOJSONstr);

                                    if(!Mix020DRCLOJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DRCLOJSON = new JSONObject(Mix020DRCLOJSONstr);

                                        String categorymix020drclostr = Mix020DRCLOJSON.getString("category");
                                        String subcategorydrclostr = Mix020DRCLOJSON.getString("sub_category");
                                        String ratedrclostr = Mix020DRCLOJSON.getString("rate");

                                        categoryal.add(categorymix020drclostr);
                                        subcategoryal.add(subcategorydrclostr);
                                        rateal.add(ratedrclostr);


                                    }
                                    else {
                                        categoryal.add("-");
                                        subcategoryal.add("-");
                                        rateal.add("-");
                                    }

                                }

                                System.out.println("RAIPUR CATEGORY LIST IS---" + categoryal);
                                System.out.println("RAIPUR SUB CATEGORY LIST IS---" + subcategoryal);
                                System.out.println("RAIPUR RATE LIST IS---" + rateal);



                                //For Raigarh-----------------------



                                JSONObject RaigarhJson = LivePriceJson.getJSONObject("Raigarh");
                                System.out.println("RAIGARH JSON IS---" + RaipurJson);


                                String BilletRaigarhJsonStr = RaigarhJson.getString("Billet");
                                System.out.println("RAIGARH BILLET JSON STRING IS---" + BilletRaigarhJsonStr);

                                if(BilletRaigarhJsonStr.equalsIgnoreCase(""))
                                {
                                    categoryraigarhal.add("-");
                                    subcategoryraigarhal.add("-");
                                    rateraigarhal.add("-");

                                }
                                else
                                {
                                    JSONObject BilletRaigarhJson = new JSONObject(BilletRaigarhJsonStr);


                                    System.out.println("RAIGARH BILLET JSON IS---" + BilletRaigarhJson);

                                    String categoryraigarhbilletstr = BilletRaigarhJson.getString("category");
                                    String subcategoryraigarhbilletstr = BilletRaigarhJson.getString("sub_category");
                                    String rateraigarhbilletstr = BilletRaigarhJson.getString("rate");

                                    if(subcategoryraigarhbilletstr.equalsIgnoreCase("")||subcategoryraigarhbilletstr.equalsIgnoreCase("0"))
                                    {
                                        subcategoryraigarhal.add("-");
                                    }
                                    else
                                    {
                                        subcategoryraigarhal.add(subcategoryraigarhbilletstr);
                                    }

                                    categoryraigarhal.add(categoryraigarhbilletstr);
                                    System.out.println("RAIGARH categoryraigarhal 1---" + categoryraigarhal);

                                    rateraigarhal.add(rateraigarhbilletstr);
                                }

                                String MSIngotRaigarhJsonStr = RaigarhJson.getString("MS Ingot");
                                System.out.println("RAIGARH MS INGOT JSON STRING IS---" + MSIngotRaigarhJsonStr);


                                if(MSIngotRaigarhJsonStr.equalsIgnoreCase(""))
                                {
                                    categoryraigarhal.add("-");
                                    subcategoryraigarhal.add("-");
                                    rateraigarhal.add("-");

                                }
                                else
                                {
                                    JSONObject MSIngotRaigarhJson = new JSONObject(MSIngotRaigarhJsonStr);
                                    System.out.println("RAIGARH MS INGOT JSON IS---" + MSIngotRaigarhJson);

                                    String categoryraigarhmsingotstr = MSIngotRaigarhJson.getString("category");
                                    String subcategoryraigarhmsingotstr = MSIngotRaigarhJson.getString("sub_category");
                                    String rateraigarhmsingotstr = MSIngotRaigarhJson.getString("rate");

                                    if(subcategoryraigarhmsingotstr.equalsIgnoreCase("")||subcategoryraigarhmsingotstr.equalsIgnoreCase("0"))
                                    {
                                        subcategoryraigarhal.add("-");
                                    }
                                    else
                                    {
                                        subcategoryraigarhal.add(subcategoryraigarhmsingotstr);
                                    }

                                    categoryraigarhal.add(categoryraigarhmsingotstr);
                                    System.out.println("RAIGARH categoryraigarhal 2---" + categoryraigarhal);


                                    rateraigarhal.add(rateraigarhmsingotstr);

                                }


                                String SpongeIronRaigarhJsonStr = RaigarhJson.getString("Sponge Iron");
                                System.out.println("RAIGARH SPONGE IRON JSON STRING IS---" + SpongeIronRaigarhJsonStr);


                                if(SpongeIronRaigarhJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronRaigarhJson = new JSONObject(SpongeIronRaigarhJsonStr);
                                    System.out.println("RAIGARH SPONGE IRON JSON IS---" + SpongeIronRaigarhJson);

                                    String lumpsraigarhjsonstr = SpongeIronRaigarhJson.getString("Lumps");
                                    System.out.println("LUMPS RAIGARH SPONGE STRING IS---" + lumpsraigarhjsonstr);

                                    if(!lumpsraigarhjsonstr.equalsIgnoreCase("")) {


                                        JSONObject LumpsRaigarhJSON = new JSONObject(lumpsraigarhjsonstr);
                                        System.out.println("RAIGARH SPONGE IRON LUMPS JSON IS---" + LumpsRaigarhJSON);

                                        String categorylumpsraigarhstr = LumpsRaigarhJSON.getString("category");
                                        String subcategorylumpsraigarhstr = LumpsRaigarhJSON.getString("sub_category");
                                        String ratelumpsraigarhstr = LumpsRaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categorylumpsraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 3---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategorylumpsraigarhstr);
                                        rateraigarhal.add(ratelumpsraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }

                                    String FinesRaigarhJSONstr = SpongeIronRaigarhJson.getString("Fines");
                                    System.out.println("RAIGARH SPONGE IRON FINES JSON IS---" + FinesRaigarhJSONstr);

                                    if(!FinesRaigarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesRaigarhJSON = new JSONObject(FinesRaigarhJSONstr);

                                        String categoryfinesraigarhstr = FinesRaigarhJSON.getString("category");
                                        String subcategoryfinesraigarhstr = FinesRaigarhJSON.getString("sub_category");
                                        String ratefinesraigarhstr = FinesRaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categoryfinesraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 4---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategoryfinesraigarhstr);
                                        rateraigarhal.add(ratefinesraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }

                                    String PelletRaigarhJSONstr = SpongeIronRaigarhJson.getString("Pellet");
                                    System.out.println("RAIGARH SPONGE IRON PELLET JSON IS---" + PelletRaigarhJSONstr);

                                    if(!PelletRaigarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject PelletRaigarhJSON = new JSONObject(PelletRaigarhJSONstr);

                                        String categorypelletraigarhstr = PelletRaigarhJSON.getString("category");
                                        String subcategorypelletraigarhstr = PelletRaigarhJSON.getString("sub_category");
                                        String ratepelletraigarhstr = PelletRaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categorypelletraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 5---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategorypelletraigarhstr);
                                        rateraigarhal.add(ratepelletraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }


                                    String Mix020RaigarhJSONstr = SpongeIronRaigarhJson.getString("Mix 0-20");
                                    System.out.println("RAIGARH SPONGE IRON MIX 0-20 JSON IS---" + Mix020RaigarhJSONstr);

                                    if(!Mix020RaigarhJSONstr.equalsIgnoreCase("")) {
                                        JSONObject Mix020RaigarhJSON = new JSONObject(Mix020RaigarhJSONstr);

                                        String categorymix020raigarhstr = Mix020RaigarhJSON.getString("category");
                                        String subcategorymix020raigarhstr = Mix020RaigarhJSON.getString("sub_category");
                                        String ratemix020raigarhstr = Mix020RaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categorymix020raigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 6---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategorymix020raigarhstr);
                                        rateraigarhal.add(ratemix020raigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }


                                }


                                String SpongeIronDRCLORaigarhJsonStr = RaigarhJson.getString("Sponge Iron DR-CLO");
                                System.out.println("RAIGARH SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLORaigarhJsonStr);


                                if(SpongeIronDRCLORaigarhJsonStr.equalsIgnoreCase(""))
                                {


                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLORaigarhJson = new JSONObject(SpongeIronDRCLORaigarhJsonStr);
                                    System.out.println("RAIGARH SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLORaigarhJson);


                                    String LumpsDRCLORaigarhJSONstr = SpongeIronDRCLORaigarhJson.getString("Lumps DR-CLO");
                                    System.out.println("RAIGARH SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLORaigarhJSONstr);

                                    if(!LumpsDRCLORaigarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLORaigarhJSON = new JSONObject(LumpsDRCLORaigarhJSONstr);

                                        System.out.println("RAIGARH SPONGE IRON LUMPS DR CLO JSON IS 2---" + LumpsDRCLORaigarhJSON);

                                        String categorydrcloraigarhstr = LumpsDRCLORaigarhJSON.getString("category");
                                        String subcategorydrcloraigarhstr = LumpsDRCLORaigarhJSON.getString("sub_category");
                                        String ratedrcloraigarhstr = LumpsDRCLORaigarhJSON.getString("rate");


                                        categoryraigarhal.add(categorydrcloraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 7---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategorydrcloraigarhstr);
                                        rateraigarhal.add(ratedrcloraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }

                                    String FinesDRCLORaigarhJSONstr = SpongeIronDRCLORaigarhJson.getString("Fines DR-CLO");
                                    System.out.println("RAIGARH SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLORaigarhJSONstr);

                                    if(!FinesDRCLORaigarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLORaigarhJSON = new JSONObject(FinesDRCLORaigarhJSONstr);

                                        String categoryfinesdrcloraigarhstr = FinesDRCLORaigarhJSON.getString("category");
                                        String subcategoryfinesdrcloraigarhstr = FinesDRCLORaigarhJSON.getString("sub_category");
                                        String ratefinesdrcloraigarhstr = FinesDRCLORaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categoryfinesdrcloraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 8---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategoryfinesdrcloraigarhstr);
                                        rateraigarhal.add(ratefinesdrcloraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }

                                    String Mix020DRCLORaigarhJSONstr = SpongeIronDRCLORaigarhJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("RAIGARH SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLORaigarhJSONstr);

                                    if(!Mix020DRCLORaigarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DRCLORaigarhJSON = new JSONObject(Mix020DRCLORaigarhJSONstr);

                                        String categorymix020drcloraigarhstr = Mix020DRCLORaigarhJSON.getString("category");
                                        String subcategorymix020drcloraigarhstr = Mix020DRCLORaigarhJSON.getString("sub_category");
                                        String ratemix020drcloraigarhstr = Mix020DRCLORaigarhJSON.getString("rate");

                                        categoryraigarhal.add(categorymix020drcloraigarhstr);
                                        System.out.println("RAIGARH categoryraigarhal 9---" + categoryraigarhal);

                                        subcategoryraigarhal.add(subcategorymix020drcloraigarhstr);
                                        rateraigarhal.add(ratemix020drcloraigarhstr);
                                    }
                                    else
                                    {
                                        categoryraigarhal.add("-");
                                        subcategoryraigarhal.add("-");
                                        rateraigarhal.add("-");
                                    }




                                }

                                System.out.println("RAIGARH CATEGORY LIST IS---" + categoryraigarhal);
                                System.out.println("RAIGARH SUB CATEGORY LIST IS---" + subcategoryraigarhal);
                                System.out.println("RAIGARH RATE LIST IS---" + rateraigarhal);




                                //For Gobindgarh-----------------------



                                JSONObject GobindgarhJson = LivePriceJson.getJSONObject("Gobindgarh");
                                System.out.println("GOBINDGARH JSON IS---" + GobindgarhJson);


                                String BilletGobindgarhJsonStr = GobindgarhJson.getString("Billet");
                                System.out.println("GOBINDGARH BILLET JSON STRING IS---" + BilletGobindgarhJsonStr);

                                if(BilletGobindgarhJsonStr.equalsIgnoreCase(""))
                                {



                                    categorygobindgarhal.add("-");
                                    subcategorygobindgarhal.add("-");
                                    rategobindgarhal.add("-");

                                }
                                else
                                {
                                    JSONObject BilletGobindgarhJson = new JSONObject(BilletGobindgarhJsonStr);


                                    System.out.println("GOBINDGARH BILLET JSON IS---" + BilletGobindgarhJson);

                                    String categorygobindgarhbilletstr = BilletGobindgarhJson.getString("category");
                                    String subcategorygobindgarhbilletstr = BilletGobindgarhJson.getString("sub_category");
                                    String rategobindgarhbilletstr = BilletGobindgarhJson.getString("rate");

                                    if(subcategorygobindgarhbilletstr.equalsIgnoreCase("")||subcategorygobindgarhbilletstr.equalsIgnoreCase("0"))
                                    {
                                        subcategorygobindgarhal.add("-");
                                    }
                                    else
                                    {
                                        subcategorygobindgarhal.add(subcategorygobindgarhbilletstr);
                                    }

                                    categorygobindgarhal.add(categorygobindgarhbilletstr);

                                    rategobindgarhal.add(rategobindgarhbilletstr);
                                }

                                String MSIngotGobindgarhJsonStr = GobindgarhJson.getString("MS Ingot");
                                System.out.println("GOBINDGARH MS INGOT JSON STRING IS---" + MSIngotGobindgarhJsonStr);


                                if(MSIngotGobindgarhJsonStr.equalsIgnoreCase(""))
                                {
                                    categorygobindgarhal.add("-");
                                    subcategorygobindgarhal.add("-");
                                    rategobindgarhal.add("-");
                                }
                                else
                                {
                                    JSONObject MSIngotGobindgarhJson = new JSONObject(MSIngotGobindgarhJsonStr);
                                    System.out.println("GOBINDGARH MS INGOT JSON IS---" + MSIngotGobindgarhJson);

                                    String categorygobindgarhmsingotstr = MSIngotGobindgarhJson.getString("category");
                                    String subcategorygobindgarhmsingotstr = MSIngotGobindgarhJson.getString("sub_category");
                                    String rategobindgarhmsingotstr = MSIngotGobindgarhJson.getString("rate");

                                    if(subcategorygobindgarhmsingotstr.equalsIgnoreCase("")||subcategorygobindgarhmsingotstr.equalsIgnoreCase("0"))
                                    {
                                        subcategorygobindgarhal.add("-");
                                    }
                                    else
                                    {
                                        subcategorygobindgarhal.add(subcategorygobindgarhmsingotstr);
                                    }

                                    categorygobindgarhal.add(categorygobindgarhmsingotstr);

                                    rategobindgarhal.add(rategobindgarhmsingotstr);

                                }


                                String SpongeIronGobindgarhJsonStr = GobindgarhJson.getString("Sponge Iron");
                                System.out.println("GOBINDGARH SPONGE IRON JSON STRING IS---" + SpongeIronGobindgarhJsonStr);


                                if(SpongeIronGobindgarhJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronGobindgarhJson = new JSONObject(SpongeIronGobindgarhJsonStr);
                                    System.out.println("GOBINDGARH SPONGE IRON JSON IS---" + SpongeIronGobindgarhJson);

                                    String lumpsgobindgarhjsonstr = SpongeIronGobindgarhJson.getString("Lumps");

                                    if(!lumpsgobindgarhjsonstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsGobindgarhJSON = new JSONObject(lumpsgobindgarhjsonstr);
                                        System.out.println("GOBINDGARH SPONGE IRON LUMPS JSON IS---" + LumpsGobindgarhJSON);

                                        String categorylumpsgobindgarhstr = LumpsGobindgarhJSON.getString("category");
                                        String subcategorylumpsgobindgarhstr = LumpsGobindgarhJSON.getString("sub_category");
                                        String ratelumpsgobindgarhstr = LumpsGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categorylumpsgobindgarhstr);
                                        subcategorygobindgarhal.add(subcategorylumpsgobindgarhstr);
                                        rategobindgarhal.add(ratelumpsgobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }

                                    String FinesGobindgarhJSONstr = SpongeIronGobindgarhJson.getString("Fines");
                                    System.out.println("GOBINDGARH SPONGE IRON FINES JSON IS---" + FinesGobindgarhJSONstr);

                                    if(!FinesGobindgarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesGobindgarhJSON = new JSONObject(FinesGobindgarhJSONstr);

                                        String categoryfinesgobindgarhstr = FinesGobindgarhJSON.getString("category");
                                        String subcategoryfinesgobindgarhstr = FinesGobindgarhJSON.getString("sub_category");
                                        String ratefinesgobindgarhstr = FinesGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categoryfinesgobindgarhstr);
                                        subcategorygobindgarhal.add(subcategoryfinesgobindgarhstr);
                                        rategobindgarhal.add(ratefinesgobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }

                                    String PelletGobindgarhJSONstr = SpongeIronGobindgarhJson.getString("Pellet");
                                    System.out.println("GOBINDGARH SPONGE IRON PELLET JSON IS---" + PelletGobindgarhJSONstr);

                                    if(!PelletGobindgarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject PelletGobindgarhJSON = new JSONObject(PelletGobindgarhJSONstr);

                                        String categorypelletgobindgarhstr = PelletGobindgarhJSON.getString("category");
                                        String subcategorypelletgobindgarhstr = PelletGobindgarhJSON.getString("sub_category");
                                        String ratepelletgobindgarhstr = PelletGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categorypelletgobindgarhstr);
                                        subcategorygobindgarhal.add(subcategorypelletgobindgarhstr);
                                        rategobindgarhal.add(ratepelletgobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }


                                    String Mix020GobindgarhJSONstr = SpongeIronGobindgarhJson.getString("Mix 0-20");
                                    System.out.println("GOBINDGARH SPONGE IRON MIX 0-20 JSON IS---" + Mix020GobindgarhJSONstr);

                                    if(!Mix020GobindgarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020GobindgarhJSON = new JSONObject(Mix020GobindgarhJSONstr);

                                        String categorymix020gobindgarhstr = Mix020GobindgarhJSON.getString("category");
                                        String subcategorymix020gobindgarhstr = Mix020GobindgarhJSON.getString("sub_category");
                                        String ratemix020gobindgarhstr = Mix020GobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categorymix020gobindgarhstr);
                                        subcategorygobindgarhal.add(subcategorymix020gobindgarhstr);
                                        rategobindgarhal.add(ratemix020gobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }


                                }


                                String SpongeIronDRCLOGobindgarhJsonStr = GobindgarhJson.getString("Sponge Iron DR-CLO");
                                System.out.println("GOBINDGARH SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLOGobindgarhJsonStr);


                                if(SpongeIronDRCLOGobindgarhJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLOGobindgarhJson = new JSONObject(SpongeIronDRCLOGobindgarhJsonStr);
                                    System.out.println("GOBINDGARH SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLOGobindgarhJson);


                                    String LumpsDRCLOGobindgarhJSONstr = SpongeIronDRCLOGobindgarhJson.getString("Lumps DR-CLO");
                                    System.out.println("GOBINDGARH SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLOGobindgarhJSONstr);

                                    if(!LumpsDRCLOGobindgarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLOGobindgarhJSON = new JSONObject(LumpsDRCLOGobindgarhJSONstr);

                                        String categorydrclogobindgarhstr = LumpsDRCLOGobindgarhJSON.getString("category");
                                        String subcategorydrclogobindgarhstr = LumpsDRCLOGobindgarhJSON.getString("sub_category");
                                        String ratedrclogobindgarhstr = LumpsDRCLOGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categorydrclogobindgarhstr);
                                        subcategorygobindgarhal.add(subcategorydrclogobindgarhstr);
                                        rategobindgarhal.add(ratedrclogobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }

                                    String FinesDRCLOGobindgarhJSONstr = SpongeIronDRCLOGobindgarhJson.getString("Fines DR-CLO");
                                    System.out.println("GOBINDGARH SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLOGobindgarhJSONstr);

                                    if(!FinesDRCLOGobindgarhJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLOGobindgarhJSON = new JSONObject(FinesDRCLOGobindgarhJSONstr);

                                        String categoryfinesdrclogobindgarhstr = FinesDRCLOGobindgarhJSON.getString("category");
                                        String subcategoryfinesdrclogobindgarhstr = FinesDRCLOGobindgarhJSON.getString("sub_category");
                                        String ratefinesdrclogobindgarhstr = FinesDRCLOGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categoryfinesdrclogobindgarhstr);
                                        subcategorygobindgarhal.add(subcategoryfinesdrclogobindgarhstr);
                                        rategobindgarhal.add(ratefinesdrclogobindgarhstr);
                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }

                                    String Mix020DRCLOGobindgarhJSONstr = SpongeIronDRCLOGobindgarhJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("GOBINDGARH SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLOGobindgarhJSONstr);

                                    if(!Mix020DRCLOGobindgarhJSONstr.equalsIgnoreCase(""))
                                    {
                                        JSONObject Mix020DRCLOGobindgarhJSON = new JSONObject(Mix020DRCLOGobindgarhJSONstr);

                                        String categorymix020drclogobindgarhstr = Mix020DRCLOGobindgarhJSON.getString("category");
                                        String subcategorymix020drclogobindgarhstr = Mix020DRCLOGobindgarhJSON.getString("sub_category");
                                        String ratemix020drclogobindgarhstr = Mix020DRCLOGobindgarhJSON.getString("rate");

                                        categorygobindgarhal.add(categorymix020drclogobindgarhstr);
                                        subcategorygobindgarhal.add(subcategorymix020drclogobindgarhstr);
                                        rategobindgarhal.add(ratemix020drclogobindgarhstr);

                                    }
                                    else {
                                        categorygobindgarhal.add("-");
                                        subcategorygobindgarhal.add("-");
                                        rategobindgarhal.add("-");
                                    }


                                }

                                System.out.println("GOBINDGARH CATEGORY LIST IS---" + categorygobindgarhal);
                                System.out.println("GOBINDGARH SUB CATEGORY LIST IS---" + subcategorygobindgarhal);
                                System.out.println("GOBINDGARH RATE LIST IS---" + rategobindgarhal);




                                //For Mumbai---------------


                                JSONObject MumbaiJson = LivePriceJson.getJSONObject("Mumbai");
                                System.out.println("MUMBAI JSON IS---" + MumbaiJson);


                                String BilletMumbaiJsonStr = MumbaiJson.getString("Billet");
                                System.out.println("MUMBAI BILLET JSON STRING IS---" + BilletMumbaiJsonStr);

                                if(BilletMumbaiJsonStr.equalsIgnoreCase(""))
                                {

                                    categorymumbaial.add("-");
                                    subcategorymumbaial.add("-");
                                    ratemumbaial.add("-");

                                }
                                else
                                {
                                    JSONObject BilletMumbaiJson = new JSONObject(BilletMumbaiJsonStr);


                                    System.out.println("MUMBAI BILLET JSON IS---" + BilletMumbaiJson);

                                    String categorymumbaibilletstr = BilletMumbaiJson.getString("category");
                                    String subcategorymumbaibilletstr = BilletMumbaiJson.getString("sub_category");
                                    String ratemumbaibilletstr = BilletMumbaiJson.getString("rate");

                                    if(subcategorymumbaibilletstr.equalsIgnoreCase("")||subcategorymumbaibilletstr.equalsIgnoreCase("0"))
                                    {
                                        subcategorymumbaial.add("-");
                                    }
                                    else
                                    {
                                        subcategorymumbaial.add(subcategorymumbaibilletstr);
                                    }

                                    categorymumbaial.add(categorymumbaibilletstr);

                                    ratemumbaial.add(ratemumbaibilletstr);
                                }

                                String MSIngotMumbaiJsonStr = MumbaiJson.getString("MS Ingot");
                                System.out.println("MUMBAI MS INGOT JSON STRING IS---" + MSIngotMumbaiJsonStr);


                                if(MSIngotMumbaiJsonStr.equalsIgnoreCase(""))
                                {
                                    categorymumbaial.add("-");
                                    subcategorymumbaial.add("-");
                                    ratemumbaial.add("-");

                                }
                                else
                                {
                                    JSONObject MSIngotMumbaiJson = new JSONObject(MSIngotMumbaiJsonStr);
                                    System.out.println("MUMBAI MS INGOT JSON IS---" + MSIngotMumbaiJson);

                                    String categorymumbaimsingotstr = MSIngotMumbaiJson.getString("category");
                                    String subcategorymumbaimsingotstr = MSIngotMumbaiJson.getString("sub_category");
                                    String ratemumbaimsingotstr = MSIngotMumbaiJson.getString("rate");

                                    if(subcategorymumbaimsingotstr.equalsIgnoreCase("")||subcategorymumbaimsingotstr.equalsIgnoreCase("0"))
                                    {
                                        subcategorymumbaial.add("-");
                                    }
                                    else
                                    {
                                        subcategorymumbaial.add(subcategorymumbaimsingotstr);
                                    }

                                    categorymumbaial.add(categorymumbaimsingotstr);

                                    ratemumbaial.add(ratemumbaimsingotstr);

                                }


                                String SpongeIronMumbaiJsonStr = MumbaiJson.getString("Sponge Iron");
                                System.out.println("MUMBAI SPONGE IRON JSON STRING IS---" + SpongeIronMumbaiJsonStr);


                                if(SpongeIronMumbaiJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronMumbaiJson = new JSONObject(SpongeIronMumbaiJsonStr);
                                    System.out.println("MUMBAI SPONGE IRON JSON IS---" + SpongeIronMumbaiJson);

                                    String lumpsmumbaijsonstr = SpongeIronMumbaiJson.getString("Lumps");

                                    if(!lumpsmumbaijsonstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsMumbaiJSON = new JSONObject(lumpsmumbaijsonstr);
                                        System.out.println("MUMBAI SPONGE IRON LUMPS JSON IS---" + LumpsMumbaiJSON);

                                        String categorylumpsmumbaistr = LumpsMumbaiJSON.getString("category");
                                        String subcategorylumpsmumbaistr = LumpsMumbaiJSON.getString("sub_category");
                                        String ratelumpsmumbaistr = LumpsMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categorylumpsmumbaistr);
                                        subcategorymumbaial.add(subcategorylumpsmumbaistr);
                                        ratemumbaial.add(ratelumpsmumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }

                                    String FinesMumbaiJSONstr = SpongeIronMumbaiJson.getString("Fines");
                                    System.out.println("MUMBAI SPONGE IRON FINES JSON IS---" + FinesMumbaiJSONstr);

                                    if(!FinesMumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesMumbaiJSON = new JSONObject(FinesMumbaiJSONstr);
                                        String categoryfinesmumbaistr = FinesMumbaiJSON.getString("category");
                                        String subcategoryfinesmumbaistr = FinesMumbaiJSON.getString("sub_category");
                                        String ratefinesmumbaistr = FinesMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categoryfinesmumbaistr);
                                        subcategorymumbaial.add(subcategoryfinesmumbaistr);
                                        ratemumbaial.add(ratefinesmumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }

                                    String PelletMumbaiJSONstr = SpongeIronMumbaiJson.getString("Pellet");
                                    System.out.println("MUMBAI SPONGE IRON PELLET JSON IS---" + PelletMumbaiJSONstr);

                                    if(!PelletMumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject PelletMumbaiJSON = new JSONObject(PelletMumbaiJSONstr);

                                        String categorypelletmumbaistr = PelletMumbaiJSON.getString("category");
                                        String subcategorypelletmumbaistr = PelletMumbaiJSON.getString("sub_category");
                                        String ratepelletmumbaistr = PelletMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categorypelletmumbaistr);
                                        subcategorymumbaial.add(subcategorypelletmumbaistr);
                                        ratemumbaial.add(ratepelletmumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }


                                    String Mix020MumbaiJSONstr = SpongeIronMumbaiJson.getString("Mix 0-20");
                                    System.out.println("MUMBAI SPONGE IRON MIX 0-20 JSON IS---" + Mix020MumbaiJSONstr);

                                    if(!Mix020MumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020MumbaiJSON = new JSONObject(Mix020MumbaiJSONstr);

                                        String categorymix020mumbaistr = Mix020MumbaiJSON.getString("category");
                                        String subcategorymix020mumbaistr = Mix020MumbaiJSON.getString("sub_category");
                                        String ratemix020mumbaistr = Mix020MumbaiJSON.getString("rate");

                                        categorymumbaial.add(categorymix020mumbaistr);
                                        subcategorymumbaial.add(subcategorymix020mumbaistr);
                                        ratemumbaial.add(ratemix020mumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }


                                }


                                String SpongeIronDRCLOMumbaiJsonStr = MumbaiJson.getString("Sponge Iron DR-CLO");
                                System.out.println("MUMBAI SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLOMumbaiJsonStr);


                                if(SpongeIronDRCLOMumbaiJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLOMumbaiJson = new JSONObject(SpongeIronDRCLOMumbaiJsonStr);
                                    System.out.println("MUMBAI SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLOMumbaiJson);


                                    String LumpsDRCLOMumbaiJSONstr = SpongeIronDRCLOMumbaiJson.getString("Lumps DR-CLO");
                                    System.out.println("MUMBAI SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLOMumbaiJSONstr);

                                    if(!LumpsDRCLOMumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLOMumbaiJSON = new JSONObject(LumpsDRCLOMumbaiJSONstr);

                                        String categorydrclomumbaistr = LumpsDRCLOMumbaiJSON.getString("category");
                                        String subcategorydrclomumbaistr = LumpsDRCLOMumbaiJSON.getString("sub_category");
                                        String ratedrclomumbaistr = LumpsDRCLOMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categorydrclomumbaistr);
                                        subcategorymumbaial.add(subcategorydrclomumbaistr);
                                        ratemumbaial.add(ratedrclomumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }

                                    String FinesDRCLOMumbaiJSONstr = SpongeIronDRCLOMumbaiJson.getString("Fines DR-CLO");
                                    System.out.println("MUMBAI SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLOMumbaiJSONstr);

                                    if(!FinesDRCLOMumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLOMumbaiJSON = new JSONObject(FinesDRCLOMumbaiJSONstr);

                                        String categoryfinesdrclomumbaistr = FinesDRCLOMumbaiJSON.getString("category");
                                        String subcategoryfinesdrclomumbaistr = FinesDRCLOMumbaiJSON.getString("sub_category");
                                        String ratefinesdrclomumbaistr = FinesDRCLOMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categoryfinesdrclomumbaistr);
                                        subcategorymumbaial.add(subcategoryfinesdrclomumbaistr);
                                        ratemumbaial.add(ratefinesdrclomumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }

                                    String Mix020DRCLOMumbaiJSONstr = SpongeIronDRCLOMumbaiJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("MUMBAI SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLOMumbaiJSONstr);

                                    if(!Mix020DRCLOMumbaiJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DRCLOMumbaiJSON = new JSONObject(Mix020DRCLOMumbaiJSONstr);

                                        String categorymix020drclomumbaistr = Mix020DRCLOMumbaiJSON.getString("category");
                                        String subcategorymix020drclomumbaistr = Mix020DRCLOMumbaiJSON.getString("sub_category");
                                        String ratemix020drclomumbaistr = Mix020DRCLOMumbaiJSON.getString("rate");

                                        categorymumbaial.add(categorymix020drclomumbaistr);
                                        subcategorymumbaial.add(subcategorymix020drclomumbaistr);
                                        ratemumbaial.add(ratemix020drclomumbaistr);
                                    }
                                    else {
                                        categorymumbaial.add("-");
                                        subcategorymumbaial.add("-");
                                        ratemumbaial.add("-");
                                    }




                                }

                                System.out.println("MUMBAI CATEGORY LIST IS---" + categorymumbaial);
                                System.out.println("MUMBAI SUB CATEGORY LIST IS---" + subcategorymumbaial);
                                System.out.println("MUMBAI RATE LIST IS---" + ratemumbaial);

                                //-------END OF MUMBAI DATA---------------------------------------------


                                //For Durgapur---------------


                                JSONObject DurgapurJson = LivePriceJson.getJSONObject("Durgapur");
                                System.out.println("DURGAPUR JSON IS---" + DurgapurJson);


                                String BilletDurgapurJsonStr = DurgapurJson.getString("Billet");
                                System.out.println("DURGAPUR BILLET JSON STRING IS---" + BilletDurgapurJsonStr);

                                if(BilletDurgapurJsonStr.equalsIgnoreCase(""))
                                {


                                    categorydurgapural.add("-");
                                    subcategorydurgapural.add("-");
                                    ratedurgapural.add("-");

                                }
                                else
                                {
                                    JSONObject BilletDurgapurJson = new JSONObject(BilletDurgapurJsonStr);


                                    System.out.println("DURGAPUR BILLET JSON IS---" + BilletDurgapurJson);

                                    String categorydurgapurbilletstr = BilletDurgapurJson.getString("category");
                                    String subcategorydurgapurbilletstr = BilletDurgapurJson.getString("sub_category");
                                    String ratedurgapurbilletstr = BilletDurgapurJson.getString("rate");

                                    if(subcategorydurgapurbilletstr.equalsIgnoreCase("")||subcategorydurgapurbilletstr.equalsIgnoreCase(""))
                                    {
                                        subcategorydurgapural.add("-");
                                    }
                                    else
                                    {
                                        subcategorydurgapural.add(subcategorydurgapurbilletstr);
                                    }

                                    categorydurgapural.add(categorydurgapurbilletstr);

                                    ratedurgapural.add(ratedurgapurbilletstr);
                                }

                                String MSIngotDurgapurJsonStr = DurgapurJson.getString("MS Ingot");
                                System.out.println("DURGAPUR MS INGOT JSON STRING IS---" + MSIngotDurgapurJsonStr);


                                if(MSIngotDurgapurJsonStr.equalsIgnoreCase(""))
                                {
                                    categorydurgapural.add("-");
                                    subcategorydurgapural.add("-");
                                    ratedurgapural.add("-");
                                }
                                else
                                {
                                    JSONObject MSIngotDurgapurJson = new JSONObject(MSIngotDurgapurJsonStr);
                                    System.out.println("DURGAPUR MS INGOT JSON IS---" + MSIngotDurgapurJson);

                                    String categorydurgapurmsingotstr = MSIngotDurgapurJson.getString("category");
                                    String subcategorydurgapurmsingotstr = MSIngotDurgapurJson.getString("sub_category");
                                    String ratedurgapurmsingotstr = MSIngotDurgapurJson.getString("rate");

                                    if(subcategorydurgapurmsingotstr.equalsIgnoreCase("")||subcategorydurgapurmsingotstr.equalsIgnoreCase(""))
                                    {
                                        subcategorydurgapural.add("-");
                                    }
                                    else
                                    {
                                        subcategorydurgapural.add(subcategorydurgapurmsingotstr);
                                    }

                                    categorydurgapural.add(categorydurgapurmsingotstr);

                                    ratedurgapural.add(ratedurgapurmsingotstr);

                                }


                                String SpongeIronDurgapurJsonStr = DurgapurJson.getString("Sponge Iron");
                                System.out.println("DURGAPUR SPONGE IRON JSON STRING IS---" + SpongeIronDurgapurJsonStr);


                                if(SpongeIronDurgapurJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDurgapurJson = new JSONObject(SpongeIronDurgapurJsonStr);
                                    System.out.println("DURGAPUR SPONGE IRON JSON IS---" + SpongeIronDurgapurJson);

                                    String lumpsdurgapurjsonstr = SpongeIronDurgapurJson.getString("Lumps");

                                    if(!lumpsdurgapurjsonstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDurgapurJSON = new JSONObject(lumpsdurgapurjsonstr);
                                        System.out.println("DURGAPUR SPONGE IRON LUMPS JSON IS---" + LumpsDurgapurJSON);

                                        String categorylumpsdurgapurstr = LumpsDurgapurJSON.getString("category");
                                        String subcategorylumpsdurgapurstr = LumpsDurgapurJSON.getString("sub_category");
                                        String ratelumpsdurgapurstr = LumpsDurgapurJSON.getString("rate");

                                        categorydurgapural.add(categorylumpsdurgapurstr);
                                        subcategorydurgapural.add(subcategorylumpsdurgapurstr);
                                        ratedurgapural.add(ratelumpsdurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }

                                    String FinesDurgapurJSONstr = SpongeIronDurgapurJson.getString("Fines");
                                    System.out.println("DURGAPUR SPONGE IRON FINES JSON IS---" + FinesDurgapurJSONstr);

                                    if(!FinesDurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDurgapurJSON = new JSONObject(FinesDurgapurJSONstr);

                                        String categoryfinesdurgapurstr = FinesDurgapurJSON.getString("category");
                                        String subcategoryfinesdurgapurstr = FinesDurgapurJSON.getString("sub_category");
                                        String ratefinesdurgapurstr = FinesDurgapurJSON.getString("rate");

                                        categorydurgapural.add(categoryfinesdurgapurstr);
                                        subcategorydurgapural.add(subcategoryfinesdurgapurstr);
                                        ratedurgapural.add(ratefinesdurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }

                                    String PelletDurgapurJSONstr = SpongeIronDurgapurJson.getString("Pellet");
                                    System.out.println("DURGAPUR SPONGE IRON PELLET JSON IS---" + PelletDurgapurJSONstr);

                                    if(!PelletDurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject PelletDurgapurJSON = new JSONObject(PelletDurgapurJSONstr);


                                        String categorypelletdurgapurstr = PelletDurgapurJSON.getString("category");
                                        String subcategorypelletdurgapurstr = PelletDurgapurJSON.getString("sub_category");
                                        String ratepelletdurgapurstr = PelletDurgapurJSON.getString("rate");

                                        categorydurgapural.add(categorypelletdurgapurstr);
                                        subcategorydurgapural.add(subcategorypelletdurgapurstr);
                                        ratedurgapural.add(ratepelletdurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }

                                    String Mix020DurgapurJSONstr = SpongeIronDurgapurJson.getString("Mix 0-20");
                                    System.out.println("DURGAPUR SPONGE IRON MIX 0-20 JSON IS---" + Mix020DurgapurJSONstr);

                                    if(!Mix020DurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DurgapurJSON = new JSONObject(Mix020DurgapurJSONstr);

                                        String categorymix020durgapurstr = Mix020DurgapurJSON.getString("category");
                                        String subcategorymix020durgapurstr = Mix020DurgapurJSON.getString("sub_category");
                                        String ratemix020durgapurstr = Mix020DurgapurJSON.getString("rate");

                                        categorydurgapural.add(categorymix020durgapurstr);
                                        subcategorydurgapural.add(subcategorymix020durgapurstr);
                                        ratedurgapural.add(ratemix020durgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }


                                }


                                String SpongeIronDRCLODurgapurJsonStr = DurgapurJson.getString("Sponge Iron DR-CLO");
                                System.out.println("DURGAPUR SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLODurgapurJsonStr);


                                if(SpongeIronDRCLODurgapurJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLODurgapurJson = new JSONObject(SpongeIronDRCLODurgapurJsonStr);
                                    System.out.println("DURGAPUR SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLODurgapurJson);


                                    String LumpsDRCLODurgapurJSONstr = SpongeIronDRCLODurgapurJson.getString("Lumps DR-CLO");
                                    System.out.println("DURGAPUR SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLODurgapurJSONstr);

                                    if(!LumpsDRCLODurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLODurgapurJSON = new JSONObject(LumpsDRCLODurgapurJSONstr);

                                        String categorydrclodurgapurstr = LumpsDRCLODurgapurJSON.getString("category");
                                        String subcategorydrclodurgapurstr = LumpsDRCLODurgapurJSON.getString("sub_category");
                                        String ratedrclodurgapurstr = LumpsDRCLODurgapurJSON.getString("rate");

                                        categorydurgapural.add(categorydrclodurgapurstr);
                                        subcategorydurgapural.add(subcategorydrclodurgapurstr);
                                        ratedurgapural.add(ratedrclodurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }

                                    String FinesDRCLODurgapurJSONstr = SpongeIronDRCLODurgapurJson.getString("Fines DR-CLO");
                                    System.out.println("DURGAPUR SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLODurgapurJSONstr);

                                    if(!FinesDRCLODurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLODurgapurJSON = new JSONObject(FinesDRCLODurgapurJSONstr);

                                        String categoryfinesdrclodurgapurstr = FinesDRCLODurgapurJSON.getString("category");
                                        String subcategoryfinesdrclodurgapurstr = FinesDRCLODurgapurJSON.getString("sub_category");
                                        String ratefinesdrclodurgapurstr = FinesDRCLODurgapurJSON.getString("rate");

                                        categorydurgapural.add(categoryfinesdrclodurgapurstr);
                                        subcategorydurgapural.add(subcategoryfinesdrclodurgapurstr);
                                        ratedurgapural.add(ratefinesdrclodurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }
                                    String Mix020DRCLODurgapurJSONstr = SpongeIronDRCLODurgapurJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("DURGAPUR SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLODurgapurJSONstr);

                                    if(!Mix020DRCLODurgapurJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DRCLODurgapurJSON = new JSONObject(Mix020DRCLODurgapurJSONstr);

                                        String categorymix020drclodurgapurstr = Mix020DRCLODurgapurJSON.getString("category");
                                        String subcategorymix020drclodurgapurstr = Mix020DRCLODurgapurJSON.getString("sub_category");
                                        String ratemix020drclodurgapurstr = Mix020DRCLODurgapurJSON.getString("rate");

                                        categorydurgapural.add(categorymix020drclodurgapurstr);
                                        subcategorydurgapural.add(subcategorymix020drclodurgapurstr);
                                        ratedurgapural.add(ratemix020drclodurgapurstr);
                                    }
                                    else {
                                        categorydurgapural.add("-");
                                        subcategorydurgapural.add("-");
                                        ratedurgapural.add("-");
                                    }



                                }

                                System.out.println("DURGAPUR CATEGORY LIST IS---" + categorydurgapural);
                                System.out.println("DURGAPUR SUB CATEGORY LIST IS---" + subcategorydurgapural);
                                System.out.println("DURGAPUR RATE LIST IS---" + ratedurgapural);



                                //-------END OF DURGAPUR DATA---------------------------------------------


                                //For Hyderabad---------------


                                JSONObject HyderabadJson = LivePriceJson.getJSONObject("Hyderabad");
                                System.out.println("HYDERABAD JSON IS---" + HyderabadJson);


                                String BilletHyderabadJsonStr = HyderabadJson.getString("Billet");
                                System.out.println("HYDERABAD BILLET JSON STRING IS---" + BilletHyderabadJsonStr);

                                if(BilletHyderabadJsonStr.equalsIgnoreCase(""))
                                {

                                    categoryhyderabadal.add("-");
                                    subcategoryhyderabadal.add("-");
                                    ratehyderabadal.add("-");

                                }
                                else
                                {
                                    JSONObject BilletHyderabadJson = new JSONObject(BilletHyderabadJsonStr);


                                    System.out.println("HYDERABAD BILLET JSON IS---" + BilletHyderabadJson);

                                    String categoryhyderabadbilletstr = BilletHyderabadJson.getString("category");
                                    String subcategoryhyderabadbilletstr = BilletHyderabadJson.getString("sub_category");
                                    String ratehyderabadbilletstr = BilletHyderabadJson.getString("rate");

                                    if(subcategoryhyderabadbilletstr.equalsIgnoreCase("")||subcategoryhyderabadbilletstr.equalsIgnoreCase("0"))
                                    {
                                        subcategoryhyderabadal.add("-");
                                    }
                                    else
                                    {
                                        subcategoryhyderabadal.add(subcategoryhyderabadbilletstr);
                                    }

                                    categoryhyderabadal.add(categoryhyderabadbilletstr);

                                    ratehyderabadal.add(ratehyderabadbilletstr);
                                }

                                String MSIngotHyderabadJsonStr = HyderabadJson.getString("MS Ingot");
                                System.out.println("HYDERABAD MS INGOT JSON STRING IS---" + MSIngotHyderabadJsonStr);


                                if(MSIngotHyderabadJsonStr.equalsIgnoreCase(""))
                                {
                                    categoryhyderabadal.add("-");
                                    subcategoryhyderabadal.add("-");
                                    ratehyderabadal.add("-");
                                }
                                else
                                {
                                    JSONObject MSIngotHyderabadJson = new JSONObject(MSIngotHyderabadJsonStr);
                                    System.out.println("HYDERABAD MS INGOT JSON IS---" + MSIngotHyderabadJson);

                                    String categoryhyderabadmsingotstr = MSIngotHyderabadJson.getString("category");
                                    String subcategoryhyderabadmsingotstr = MSIngotHyderabadJson.getString("sub_category");
                                    String ratehyderabadmsingotstr = MSIngotHyderabadJson.getString("rate");

                                    if(subcategoryhyderabadmsingotstr.equalsIgnoreCase("")||subcategoryhyderabadmsingotstr.equalsIgnoreCase("0"))
                                    {
                                        subcategoryhyderabadal.add("-");
                                    }
                                    else
                                    {
                                        subcategoryhyderabadal.add(subcategoryhyderabadmsingotstr);
                                    }

                                    categoryhyderabadal.add(categoryhyderabadmsingotstr);
                                    // subcategoryhyderabadal.add(subcategoryhyderabadmsingotstr);
                                    ratehyderabadal.add(ratehyderabadmsingotstr);

                                }


                                String SpongeIronHyderabadJsonStr = HyderabadJson.getString("Sponge Iron");
                                System.out.println("HYDERABAD SPONGE IRON JSON STRING IS---" + SpongeIronHyderabadJsonStr);


                                if(SpongeIronHyderabadJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronHyderabadJson = new JSONObject(SpongeIronHyderabadJsonStr);
                                    System.out.println("HYDERABAD SPONGE IRON JSON IS---" + SpongeIronHyderabadJson);

                                    String lumpshyderabadjsonstr = SpongeIronHyderabadJson.getString("Lumps");

                                    if(!lumpshyderabadjsonstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsHyderabadJSON = new JSONObject(lumpshyderabadjsonstr);
                                        System.out.println("HYDERABAD SPONGE IRON LUMPS JSON IS---" + LumpsHyderabadJSON);

                                        String categorylumpshyderabadstr = LumpsHyderabadJSON.getString("category");
                                        String subcategorylumpshyderabadstr = LumpsHyderabadJSON.getString("sub_category");
                                        String ratelumpshyderabadstr = LumpsHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categorylumpshyderabadstr);
                                        subcategoryhyderabadal.add(subcategorylumpshyderabadstr);
                                        ratehyderabadal.add(ratelumpshyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }

                                    String FinesHyderabadJSONstr = SpongeIronHyderabadJson.getString("Fines");
                                    System.out.println("HYDERABAD SPONGE IRON FINES JSON IS---" + FinesHyderabadJSONstr);

                                    if(!FinesHyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesHyderabadJSON = new JSONObject(FinesHyderabadJSONstr);

                                        String categoryfineshyderabadstr = FinesHyderabadJSON.getString("category");
                                        String subcategoryfineshyderabadstr = FinesHyderabadJSON.getString("sub_category");
                                        String ratefineshdyerabadstr = FinesHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categoryfineshyderabadstr);
                                        subcategoryhyderabadal.add(subcategoryfineshyderabadstr);
                                        ratehyderabadal.add(ratefineshdyerabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }

                                    String PelletHyderabadJSONstr = SpongeIronHyderabadJson.getString("Pellet");
                                    System.out.println("HYDERABAD SPONGE IRON PELLET JSON IS---" + PelletHyderabadJSONstr);

                                    if(!PelletHyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject PelletHyderabadJSON = new JSONObject(PelletHyderabadJSONstr);

                                        String categorypellethyderabadstr = PelletHyderabadJSON.getString("category");
                                        String subcategorypellethyderabadstr = PelletHyderabadJSON.getString("sub_category");
                                        String ratepellethyderabadstr = PelletHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categorypellethyderabadstr);
                                        subcategoryhyderabadal.add(subcategorypellethyderabadstr);
                                        ratehyderabadal.add(ratepellethyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }


                                    String Mix020HyderabadJSONstr = SpongeIronHyderabadJson.getString("Mix 0-20");
                                    System.out.println("HYDERABAD SPONGE IRON MIX 0-20 JSON IS---" + Mix020HyderabadJSONstr);

                                    if(!Mix020HyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020HyderabadJSON = new JSONObject(Mix020HyderabadJSONstr);

                                        String categorymix020hyderabadstr = Mix020HyderabadJSON.getString("category");
                                        String subcategorymix020hyderabadstr = Mix020HyderabadJSON.getString("sub_category");
                                        String ratemix020hyderabadstr = Mix020HyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categorymix020hyderabadstr);
                                        subcategoryhyderabadal.add(subcategorymix020hyderabadstr);
                                        ratehyderabadal.add(ratemix020hyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }


                                }


                                String SpongeIronDRCLOHyderabadJsonStr = HyderabadJson.getString("Sponge Iron DR-CLO");
                                System.out.println("HYDERABAD SPONGE IRON DR CLO JSON STRING IS---" + SpongeIronDRCLOHyderabadJsonStr);


                                if(SpongeIronDRCLOHyderabadJsonStr.equalsIgnoreCase(""))
                                {

                                }
                                else
                                {
                                    JSONObject SpongeIronDRCLOHyderabadJson = new JSONObject(SpongeIronDRCLOHyderabadJsonStr);
                                    System.out.println("HYDERABAD SPONGE IRON DR CLO JSON IS---" + SpongeIronDRCLOHyderabadJson);


                                    String LumpsDRCLOHyderabadJSONstr = SpongeIronDRCLOHyderabadJson.getString("Lumps DR-CLO");
                                    System.out.println("HYDERABAD SPONGE IRON LUMPS DR CLO JSON IS---" + LumpsDRCLOHyderabadJSONstr);

                                    if(!LumpsDRCLOHyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject LumpsDRCLOHyderabadJSON = new JSONObject(LumpsDRCLOHyderabadJSONstr);

                                        String categorydrclohyderabadstr = LumpsDRCLOHyderabadJSON.getString("category");
                                        String subcategorydrclohyderabadstr = LumpsDRCLOHyderabadJSON.getString("sub_category");
                                        String ratedrclohyderabadstr = LumpsDRCLOHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categorydrclohyderabadstr);
                                        subcategoryhyderabadal.add(subcategorydrclohyderabadstr);
                                        ratehyderabadal.add(ratedrclohyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }

                                    String FinesDRCLOHyderabadJSONstr = SpongeIronDRCLOHyderabadJson.getString("Fines DR-CLO");
                                    System.out.println("HYDERABAD SPONGE IRON FINES DR CLO JSON IS---" + FinesDRCLOHyderabadJSONstr);

                                    if(!FinesDRCLOHyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject FinesDRCLOHyderabadJSON = new JSONObject(FinesDRCLOHyderabadJSONstr);

                                        String categoryfinesdrclohyderabadstr = FinesDRCLOHyderabadJSON.getString("category");
                                        String subcategoryfinesdrclohyderabadstr = FinesDRCLOHyderabadJSON.getString("sub_category");
                                        String ratefinesdrclohyderabadstr = FinesDRCLOHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categoryfinesdrclohyderabadstr);
                                        subcategoryhyderabadal.add(subcategoryfinesdrclohyderabadstr);
                                        ratehyderabadal.add(ratefinesdrclohyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }

                                    String Mix020DRCLOHyderabadJSONstr = SpongeIronDRCLOHyderabadJson.getString("Mix 0-20 DR-CLO");
                                    System.out.println("HYDERABAD SPONGE IRON MIX 0-20 DR-CLO JSON IS---" + Mix020DRCLOHyderabadJSONstr);

                                    if(!Mix020DRCLOHyderabadJSONstr.equalsIgnoreCase("")) {

                                        JSONObject Mix020DRCLOHyderabadJSON = new JSONObject(Mix020DRCLOHyderabadJSONstr);

                                        String categorymix020drclohyderabadstr = Mix020DRCLOHyderabadJSON.getString("category");
                                        String subcategorymix020drclohyderabadstr = Mix020DRCLOHyderabadJSON.getString("sub_category");
                                        String ratemix020drclohyderabadstr = Mix020DRCLOHyderabadJSON.getString("rate");

                                        categoryhyderabadal.add(categorymix020drclohyderabadstr);
                                        subcategoryhyderabadal.add(subcategorymix020drclohyderabadstr);
                                        ratehyderabadal.add(ratemix020drclohyderabadstr);
                                    }
                                    else {
                                        categoryhyderabadal.add("-");
                                        subcategoryhyderabadal.add("-");
                                        ratehyderabadal.add("-");
                                    }




                                }

                                System.out.println("HYDERABAD CATEGORY LIST IS---" + categoryhyderabadal);
                                System.out.println("HYDERABAD SUB CATEGORY LIST IS---" + subcategoryhyderabadal);
                                System.out.println("HYDERABAD RATE LIST IS---" + ratehyderabadal);



                                isAnimationRunning = true;

                                if (getActivity() == null) {
                                   // isAnimationRunning == true
                                    return;
                                }
                                else {


                                    if (isAnimationRunning == true) {

                                        //    if(getActivity()!=null) {
                                        t = new Thread() {

                                            @Override
                                            public void run() {
                                                try {


                                                    while (!isInterrupted()) {

                                                        Thread.sleep(5000);

                                                        if (getActivity() != null) {


                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    // update TextView here!


                                                                    //START RAIPUR ---------------------------

                                                                    System.out.println("raipur boga IS---" + categoryal.get(i));
                                                                    System.out.println("raigarh bugman IS---" + categoryraigarhal);

                                                                    categorytitletxt1.setVisibility(View.VISIBLE);

                                                                    if (categoryal.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt1.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt1.setText("");
                                                                    } else {
                                                                        categorytitletxt1.setText(categoryal.get(i));
                                                                    }

                                                                    subcategorytitletxt1.setVisibility(View.VISIBLE);

                                                                    if (subcategoryal.get(i).equalsIgnoreCase("-")) {
                                                                        subcategorytitletxt1.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt1.setText("");
                                                                    } else {
                                                                        subcategorytitletxt1.setText(subcategoryal.get(i));
                                                                    }

                                                                    pricetxt1.setVisibility(View.VISIBLE);

                                                                    if (rateal.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt1.setVisibility(View.INVISIBLE);
                                                                        pricetxt1.setText("");
                                                                    } else {
                                                                        pricetxt1.setText(rateal.get(i));
                                                                    }


                                                                    //END OF RAIPUR --------------------------


                                                                    //START RAIGARH ---------------------------


                                                                    categorytitletxt2.setVisibility(View.VISIBLE);

                                                                    System.out.println("CATEGORY RAIGARH ALS---==" + categoryraigarhal);

                                                                    if (categoryraigarhal.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt2.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt2.setText("");
                                                                    } else {
                                                                        categorytitletxt2.setText(categoryraigarhal.get(i));
                                                                    }

                                                                    subcategorytitletxt2.setVisibility(View.VISIBLE);

                                                                    if (subcategoryraigarhal.get(i).equalsIgnoreCase("-")) {
                                                                        subcategorytitletxt2.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt2.setText("");
                                                                    } else {
                                                                        subcategorytitletxt2.setText(subcategoryraigarhal.get(i));
                                                                    }

                                                                    pricetxt2.setVisibility(View.VISIBLE);

                                                                    if (rateraigarhal.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt2.setVisibility(View.INVISIBLE);
                                                                        pricetxt2.setText("");
                                                                    } else {
                                                                        pricetxt2.setText(rateraigarhal.get(i));
                                                                    }

                                                                    //END OF RAIGARH ---------------------------


                                                                    //START GOBINDGARH ---------------------------

                                                                    categorytitletxt3.setVisibility(View.VISIBLE);

                                                                    if (categorygobindgarhal.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt3.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt3.setText("");
                                                                    } else {
                                                                        categorytitletxt3.setText(categorygobindgarhal.get(i));
                                                                    }

                                                                    subcategorytitletxt3.setVisibility(View.VISIBLE);

                                                                    if (subcategorygobindgarhal.get(i).equalsIgnoreCase("-")) {

                                                                        subcategorytitletxt3.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt3.setText("");
                                                                    } else {
                                                                        subcategorytitletxt3.setText(subcategorygobindgarhal.get(i));
                                                                    }


                                                                    pricetxt3.setVisibility(View.VISIBLE);

                                                                    if (rategobindgarhal.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt3.setVisibility(View.INVISIBLE);
                                                                        pricetxt3.setText("");
                                                                    } else {
                                                                        pricetxt3.setText(rategobindgarhal.get(i));
                                                                    }


                                                                    //END OF GOBINDGARH ---------------------------


                                                                    //START MUMBAI ---------------------------

                                                                    categorytitletxt4.setVisibility(View.VISIBLE);

                                                                    if (categorymumbaial.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt4.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt4.setText("");
                                                                    } else {
                                                                        categorytitletxt4.setText(categorymumbaial.get(i));
                                                                    }

                                                                    subcategorytitletxt4.setVisibility(View.VISIBLE);

                                                                    if (subcategorymumbaial.get(i).equalsIgnoreCase("-")) {
                                                                        subcategorytitletxt4.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt4.setText("");
                                                                    } else {
                                                                        subcategorytitletxt4.setText(subcategorymumbaial.get(i));
                                                                    }

                                                                    pricetxt4.setVisibility(View.VISIBLE);

                                                                    if (ratemumbaial.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt4.setVisibility(View.INVISIBLE);
                                                                        pricetxt4.setText("");
                                                                    } else {
                                                                        pricetxt4.setText(ratemumbaial.get(i));
                                                                    }

                                                                    //END OF MUMBAI ---------------------------


                                                                    //START DURGAPUR ---------------------------


                                                                    categorytitletxt5.setVisibility(View.VISIBLE);

                                                                    if (categorydurgapural.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt5.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt5.setText("");
                                                                    } else {
                                                                        categorytitletxt5.setText(categorydurgapural.get(i));
                                                                    }

                                                                    subcategorytitletxt5.setVisibility(View.VISIBLE);

                                                                    if (subcategorydurgapural.get(i).equalsIgnoreCase("-")) {
                                                                        subcategorytitletxt5.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt5.setText("");
                                                                    } else {
                                                                        subcategorytitletxt5.setText(subcategorydurgapural.get(i));
                                                                    }

                                                                    pricetxt5.setVisibility(View.VISIBLE);

                                                                    if (ratedurgapural.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt5.setVisibility(View.INVISIBLE);
                                                                        pricetxt5.setText("");
                                                                    } else {
                                                                        pricetxt5.setText(ratedurgapural.get(i));
                                                                    }

                                                                    categorytitletxt6.setVisibility(View.VISIBLE);

                                                                    if (categoryhyderabadal.get(i).equalsIgnoreCase("-")) {
                                                                        categorytitletxt6.setVisibility(View.INVISIBLE);
                                                                        categorytitletxt6.setText("");
                                                                    } else {
                                                                        categorytitletxt6.setText(categoryhyderabadal.get(i));
                                                                    }


                                                                    subcategorytitletxt6.setVisibility(View.VISIBLE);

                                                                    if (subcategoryhyderabadal.get(i).equalsIgnoreCase("-")) {
                                                                        subcategorytitletxt6.setVisibility(View.INVISIBLE);
                                                                        subcategorytitletxt6.setText("");
                                                                    } else {
                                                                        subcategorytitletxt6.setText(subcategoryhyderabadal.get(i));
                                                                    }

                                                                    pricetxt6.setVisibility(View.VISIBLE);

                                                                    if (ratehyderabadal.get(i).equalsIgnoreCase("-")) {
                                                                        pricetxt6.setVisibility(View.INVISIBLE);
                                                                        pricetxt6.setText("");
                                                                    } else {
                                                                        pricetxt6.setText(ratehyderabadal.get(i));
                                                                    }


                                                                    //END OF HYDERABAD --------------------------i


                                                                    Animation topTobottom = AnimationUtils.loadAnimation(getActivity(), R.anim.top_bottom);


                                                                    categorytitletxt1.setDrawingCacheEnabled(true);
                                                                    categorytitletxt1.startAnimation(topTobottom);


                                                                    subcategorytitletxt1.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt1.startAnimation(topTobottom);

                                                                    pricetxt1.setDrawingCacheEnabled(true);
                                                                    pricetxt1.startAnimation(topTobottom);

                                                                    categorytitletxt2.setDrawingCacheEnabled(true);
                                                                    categorytitletxt2.startAnimation(topTobottom);

                                                                    subcategorytitletxt2.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt2.startAnimation(topTobottom);

                                                                    pricetxt2.setDrawingCacheEnabled(true);
                                                                    pricetxt2.startAnimation(topTobottom);

                                                                    categorytitletxt3.setDrawingCacheEnabled(true);
                                                                    categorytitletxt3.startAnimation(topTobottom);

                                                                    subcategorytitletxt3.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt3.startAnimation(topTobottom);

                                                                    pricetxt3.setDrawingCacheEnabled(true);
                                                                    pricetxt3.startAnimation(topTobottom);

                                                                    categorytitletxt4.setDrawingCacheEnabled(true);
                                                                    categorytitletxt4.startAnimation(topTobottom);

                                                                    subcategorytitletxt4.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt4.startAnimation(topTobottom);

                                                                    pricetxt4.setDrawingCacheEnabled(true);
                                                                    pricetxt4.startAnimation(topTobottom);

                                                                    categorytitletxt5.setDrawingCacheEnabled(true);
                                                                    categorytitletxt5.startAnimation(topTobottom);

                                                                    subcategorytitletxt5.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt5.startAnimation(topTobottom);

                                                                    pricetxt5.setDrawingCacheEnabled(true);
                                                                    pricetxt5.startAnimation(topTobottom);


                                                                    categorytitletxt6.setDrawingCacheEnabled(true);
                                                                    categorytitletxt6.startAnimation(topTobottom);

                                                                    subcategorytitletxt6.setDrawingCacheEnabled(true);
                                                                    subcategorytitletxt6.startAnimation(topTobottom);

                                                                    pricetxt6.setDrawingCacheEnabled(true);
                                                                    pricetxt6.startAnimation(topTobottom);

                                                                    i = i + 1;



                                                                        new Handler().postDelayed(new Runnable() {
                                                                            @Override
                                                                            public void run() {

                                                                                if(getActivity()!=null) {
                                                                                    Animation topTobottom1 = AnimationUtils.loadAnimation(getActivity(), R.anim.top_bottom1);

                                                                                    categorytitletxt1.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt1.startAnimation(topTobottom1);
                                                                                    pricetxt1.startAnimation(topTobottom1);


                                                                                    categorytitletxt2.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt2.startAnimation(topTobottom1);
                                                                                    pricetxt2.startAnimation(topTobottom1);


                                                                                    categorytitletxt3.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt3.startAnimation(topTobottom1);
                                                                                    pricetxt3.startAnimation(topTobottom1);


                                                                                    categorytitletxt4.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt4.startAnimation(topTobottom1);
                                                                                    pricetxt4.startAnimation(topTobottom1);


                                                                                    categorytitletxt5.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt5.startAnimation(topTobottom1);
                                                                                    pricetxt5.startAnimation(topTobottom1);


                                                                                    categorytitletxt6.startAnimation(topTobottom1);
                                                                                    subcategorytitletxt6.startAnimation(topTobottom1);
                                                                                    pricetxt6.startAnimation(topTobottom1);

                                                                                }
                                                                            }
                                                                        }, 3500);




                                                                    if (i == 9) {

                                                                        i = 0;

                                                                    }

                                                                }
                                                            });
                                                        }
                                                    }
                                                } catch (InterruptedException e) {
                                                }
                                            }
                                        };

                                        t.start();


                                        // }
                                    }
                                }

                                //-------END OF HYDERABAD DATA---------------------------------------------
                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block

                            e.printStackTrace();
                            pdia.dismiss();

                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        //Toast.makeText(getActivity(), messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               *//* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*//*


                System.out.println("get live news params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);
    }
*/



}
