package com.smtc.mxmart.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smtc.mxmart.APIName;
import com.smtc.mxmart.AddNewOfferLiveTradeEnhancementActivity;
import com.smtc.mxmart.AddNewOfferLiveTradeEnhancementActivity0712;
import com.smtc.mxmart.NetworkUtility;
import com.smtc.mxmart.R;
import com.smtc.mxmart.SingletonActivity;
import com.smtc.mxmart.UtilsDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by 10161 on 11/14/2017.
 */

public class SellTodaysOfferFragment1012 extends Fragment {

    public static final String MY_PREFS_NAME = "MyPrefsFile";
    Typeface source_sans_pro_normal;
    RelativeLayout  sellrelative;
    String userCodestr,mobilenumstr,todaysoffer;
    UtilsDialog util=new UtilsDialog();
    CustomAdap customadap;
    ListView selltodaysofferlistvw;
    AlertDialog b;
    int count = 0;
    FragmentTransaction ftt;
    TextView noofferfoundtxt;
    ProgressDialog pdia;
    private ProgressBar bar;
    Handler handler = new Handler();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View view = inflater.inflate(R.layout.fragment_sell_todays_offer, container, false);


        SharedPreferences prefs = getActivity().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        todaysoffer = prefs.getString("todaysoffer",null);

        noofferfoundtxt = (TextView)view.findViewById(R.id.noofferfoundtxt);
        bar = (ProgressBar)view.findViewById(R.id.progressBar);


        source_sans_pro_normal = Typeface.createFromAsset(getActivity().getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");
        noofferfoundtxt.setTypeface(source_sans_pro_normal);

        selltodaysofferlistvw = (ListView)view.findViewById(R.id.selltodaysofferlistvw);

        final com.smtc.mxmart.floatingactionbutton.FloatingActionsMenu floatingActionsMenu = (com.smtc.mxmart.floatingactionbutton.FloatingActionsMenu)view.findViewById(R.id.multiple_actions);
        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new com.smtc.mxmart.floatingactionbutton.FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                //  SellActivity.this.sellrelative.setBackgroundColor(Color.parseColor("#CC000000"));
               view.setBackgroundColor(Color.parseColor("#FFFFFF"));


            }

            @Override
            public void onMenuCollapsed() {
                view.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        });




            if (NetworkUtility.checkConnectivity(getActivity())) {


                String SellerTodaysOfferUrl = APIName.URL + "/seller/getOffer?user_code=" + userCodestr;
                System.out.println("SELLER TODAY'S OFFER URL IS---" + SellerTodaysOfferUrl);
                SellerTodaysOfferAPI(SellerTodaysOfferUrl);


            } else {
                util.dialog(getActivity(), "Please check your internet connection.");
            }



        final com.smtc.mxmart.floatingactionbutton.FloatingActionButton actionA = (com.smtc.mxmart.floatingactionbutton.FloatingActionButton)view.findViewById(R.id.action_offer);

        actionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (todaysoffer.equalsIgnoreCase("0")) {

                    SingletonActivity.fromselltodaysoffer = false;
                    SingletonActivity.isUpdateClicked = false;

                    Intent i = new Intent(getActivity(), AddNewOfferLiveTradeEnhancementActivity0712.class);
                    startActivity(i);
                }
                else
                {
                    Toast.makeText(getActivity(), "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        final  com.smtc.mxmart.floatingactionbutton.FloatingActionButton actionB = (com.smtc.mxmart.floatingactionbutton.FloatingActionButton)view.findViewById(R.id.action_promotions);
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (todaysoffer.equalsIgnoreCase("0")) {

                    //Toast.makeText(SellActivity.this,"click 2",Toast.LENGTH_SHORT).show();
                    AddNewPromoDialog(SingletonActivity.promocode);
                }
                else
                {
                    Toast.makeText(getActivity(), "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                }

            }
        });






        return view;

    }






    public void AddNewPromoDialog(String promocode) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.add_promotions_dialog, null);
        dialogBuilder.setView(dialogView);



        final EditText et11 = (EditText)dialogView.findViewById(R.id.et11);
        final EditText et12 = (EditText)dialogView.findViewById(R.id.et12);
        final EditText et13 = (EditText)dialogView.findViewById(R.id.et13);
        final EditText et21 = (EditText)dialogView.findViewById(R.id.et21);
        final EditText et22 = (EditText)dialogView.findViewById(R.id.et22);
        final EditText et23 = (EditText)dialogView.findViewById(R.id.et23);
        final  EditText et31 = (EditText)dialogView.findViewById(R.id.et31);
        final  EditText et32 = (EditText)dialogView.findViewById(R.id.et32);
        final EditText et33 = (EditText)dialogView.findViewById(R.id.et33);
        final   EditText et41 = (EditText)dialogView.findViewById(R.id.et41);
        final  EditText et42 = (EditText)dialogView.findViewById(R.id.et42);
        final EditText et43 = (EditText)dialogView.findViewById(R.id.et43);
        final EditText promocodeedt = (EditText)dialogView.findViewById(R.id.promocodeedt);

        final  RelativeLayout saverelative = (RelativeLayout)dialogView.findViewById(R.id.saverelative);
        final  RelativeLayout cancelbtn = (RelativeLayout)dialogView.findViewById(R.id.cancelrelative);



        promocodeedt.setText(promocode);

        et11.setEnabled(true);
        et12.setEnabled(false);
        et13.setEnabled(false);

        et21.setEnabled(false);
        et21.setBackgroundColor(Color.parseColor("#d3d3d3"));

        et22.setEnabled(false);
        et22.setBackgroundColor(Color.parseColor("#d3d3d3"));

        et23.setEnabled(false);
        et23.setBackgroundColor(Color.parseColor("#d3d3d3"));

        et31.setEnabled(false);
        et31.setBackgroundColor(Color.parseColor("#d3d3d3"));

        et32.setEnabled(false);
        et32.setBackgroundColor(Color.parseColor("#d3d3d3"));

        et33.setEnabled(false);
        et33.setBackgroundColor(Color.parseColor("#d3d3d3"));

        et41.setEnabled(false);
        et41.setBackgroundColor(Color.parseColor("#d3d3d3"));

        et42.setEnabled(false);
        et42.setBackgroundColor(Color.parseColor("#d3d3d3"));

        et43.setEnabled(false);
        et43.setBackgroundColor(Color.parseColor("#d3d3d3"));

        saverelative.setEnabled(false);
        saverelative.setBackgroundResource(R.drawable.grey_btn_rounded_rectangle);



        et11.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {


                et12.setEnabled(true);
               /* if(et11.getText().toString().length()>0) {
                    int et11int = Integer.parseInt(et11.getText().toString());
                    et12.setText(Integer.toString(et11int + 1));
                }
                else
                {
                    et12.setText("");
                }*/


            }
        });

        et12.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {


                et13.setEnabled(true);

            }
        });


        et12.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus)
                {

                    if(et11.getText().toString().length()>0) {
                        int et11int = Integer.parseInt(et11.getText().toString());
                        et12.setText(Integer.toString(et11int + 1));

                        saverelative.setEnabled(true);
                        saverelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                    }
                    else
                    {
                        et12.setText("");
                    }

                    int et11int = Integer.parseInt(et11.getText().toString());
                    int et12int = 0;

                    if(et12.getText().toString().length()>0) {
                        et12int = Integer.parseInt(et12.getText().toString());
                    }

                    if(et13.getText().toString().length()>0) {
                        et21.setEnabled(true);
                    }



                    if(et11int>et12int) {
                        if (et11.getText().toString().length() > 0) {

                            et12.setText(Integer.toString(et11int + 1));



                        } else {
                            et12.setText("");
                        }
                    }

                }
                else
                {



                }
            }
        });



        et13.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                int et11int = Integer.parseInt(et11.getText().toString());
                int et12int = 0;
                et21.setEnabled(true);

                et11.setEnabled(false);
                et12.setEnabled(false);
                // et13.setEnabled(false);

                et21.setText(Integer.toString(et11int + 2));

                if(et12.getText().toString().length()>0) {
                    et12int = Integer.parseInt(et12.getText().toString());
                }
                if(et11int>et12int) {
                    if (et11.getText().toString().length() > 0) {

                        et12.setText(Integer.toString(et11int + 1));

                        //  et21.setText(Integer.toString(et11int + 2));




                    } else {
                        et12.setText("");
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s) {



                int et11int = Integer.parseInt(et11.getText().toString());
                int et12int = 0;
                et21.setEnabled(true);

                et11.setEnabled(false);
                et12.setEnabled(false);
                //  et13.setEnabled(false);

                et21.setText(Integer.toString(et11int + 2));

                if(et12.getText().toString().length()>0) {
                    et12int = Integer.parseInt(et12.getText().toString());
                }

                if(et11int>et12int) {
                    if (et11.getText().toString().length() > 0) {

                        et12.setText(Integer.toString(et11int + 1));
                        //    et21.setText(Integer.toString(et11int + 2));



                    } else {
                        et12.setText("");
                    }
                }


            }
        });

        et13.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus)
                {

                    int et11int = Integer.parseInt(et11.getText().toString());
                    int et12int = 0;

                    if(et12.getText().toString().length()>0) {
                        et12int = Integer.parseInt(et12.getText().toString());
                    }

                    if(et13.getText().toString().length()>0) {
                        et21.setEnabled(true);

                        et11.setEnabled(false);
                        et12.setEnabled(false);
                        // et13.setEnabled(false);

                        // et21.setText(Integer.toString(et11int + 2));
                    }



                    if(et11int>et12int) {
                        if (et11.getText().toString().length() > 0) {

                            et12.setText(Integer.toString(et11int + 1));




                        } else {
                            et12.setText("");
                        }
                    }

                }
                else
                {



                }
            }
        });





        et21.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {




            }

            @Override
            public void afterTextChanged(Editable s) {

                //   et13.setEnabled(false);
                et22.setEnabled(true);

                int et21int = 0;

                if(et21.getText().toString().length()>0) {
                    et21int = Integer.parseInt(et21.getText().toString());
                }



                int et22int = 0;
                if(et22.getText().toString().length()>0)
                {
                    et22int = Integer.parseInt(et22.getText().toString());
                }


                if(et21int>et22int) {
                    if (et21.getText().toString().length() > 0) {

                        et22.setText(Integer.toString(et21int + 1));

                    } else {
                        et22.setText("");
                    }
                }



            }
        });

        et21.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus)
                {

                    int et11int = Integer.parseInt(et11.getText().toString());
                    int et12int = 0;

                    if(et12.getText().toString().length()>0) {
                        et12int = Integer.parseInt(et12.getText().toString());
                    }

                    if(et13.getText().toString().length()>0) {
                        et21.setEnabled(true);

                        et11.setEnabled(false);
                        et12.setEnabled(false);
                        et13.setEnabled(false);

                        et21.setText(Integer.toString(et11int + 2));
                    }



                    if(et11int>et12int) {
                        if (et11.getText().toString().length() > 0) {

                            et12.setText(Integer.toString(et11int + 1));




                        } else {
                            et12.setText("");
                        }
                    }

                }
                else
                {



                }
            }
        });



        et22.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {


                et23.setEnabled(true);

            }
        });


        et22.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus)
                {

                    int et21int = 0;

                    if(et21.getText().toString().length()>0)
                    {
                        et21int =   Integer.parseInt(et21.getText().toString());
                    }

                    int et22int = 0;

                    if(et22.getText().toString().length()>0) {
                        et22int = Integer.parseInt(et22.getText().toString());
                    }

                    if(et23.getText().toString().length()>0) {
                        et31.setEnabled(true);
                    }



                    if(et21int>et22int) {
                        if (et21.getText().toString().length() > 0) {

                            et22.setText(Integer.toString(et21int + 1));



                        } else {
                            et22.setText("");
                        }
                    }

                }
                else
                {



                }
            }
        });

        et23.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                int et21int = Integer.parseInt(et21.getText().toString());
                int et22int = 0;
                et31.setEnabled(true);

                et21.setEnabled(false);
                et22.setEnabled(false);
                et13.setEnabled(false);

                et31.setText(Integer.toString(et21int + 2));

                if(et22.getText().toString().length()>0) {
                    et22int = Integer.parseInt(et22.getText().toString());
                }
                if(et21int>et22int) {
                    if (et21.getText().toString().length() > 0) {

                        et22.setText(Integer.toString(et21int + 1));

                        //  et21.setText(Integer.toString(et11int + 2));




                    } else {
                        et22.setText("");
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s) {



                int et21int = Integer.parseInt(et21.getText().toString());
                int et22int = 0;
                et31.setEnabled(true);

                et21.setEnabled(false);
                et22.setEnabled(false);
                et13.setEnabled(false);

                et31.setText(Integer.toString(et21int + 2));

                if(et22.getText().toString().length()>0) {
                    et22int = Integer.parseInt(et22.getText().toString());
                }

                if(et21int>et22int) {
                    if (et21.getText().toString().length() > 0) {

                        et22.setText(Integer.toString(et21int + 1));
                        //    et21.setText(Integer.toString(et11int + 2));



                    } else {
                        et22.setText("");
                    }
                }


            }
        });

        et23.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus)
                {

                    int et21int = Integer.parseInt(et21.getText().toString());
                    int et22int = 0;

                    if(et22.getText().toString().length()>0) {
                        et22int = Integer.parseInt(et22.getText().toString());
                    }

                    if(et23.getText().toString().length()>0) {
                        et31.setEnabled(true);

                        et21.setEnabled(false);
                        et22.setEnabled(false);
                        et13.setEnabled(false);

                        et31.setText(Integer.toString(et21int + 2));
                    }



                    if(et21int>et22int) {
                        if (et21.getText().toString().length() > 0) {

                            et22.setText(Integer.toString(et21int + 1));




                        } else {
                            et22.setText("");
                        }
                    }

                }
                else
                {



                }
            }
        });




        et31.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {




            }

            @Override
            public void afterTextChanged(Editable s) {


                et32.setEnabled(true);

                int et31int = 0;

                if(et31.getText().toString().length()>0) {
                    et31int = Integer.parseInt(et31.getText().toString());
                }



                int et32int = 0;
                if(et32.getText().toString().length()>0)
                {
                    et32int = Integer.parseInt(et32.getText().toString());
                }


                if(et31int>et32int) {
                    if (et31.getText().toString().length() > 0) {

                        et32.setText(Integer.toString(et31int + 1));

                    } else {
                        et32.setText("");
                    }
                }



            }
        });

        et32.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {


                et33.setEnabled(true);

            }
        });


        et32.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus)
                {
                    int et31int = 0;
                    if(et31.getText().toString().length()>0){
                        et31int = Integer.parseInt(et31.getText().toString());
                    }

                    int et32int = 0;

                    if(et32.getText().toString().length()>0) {
                        et32int = Integer.parseInt(et32.getText().toString());
                    }

                    if(et33.getText().toString().length()>0) {
                        et41.setEnabled(true);
                    }



                    if(et31int>et32int) {
                        if (et31.getText().toString().length() > 0) {

                            et32.setText(Integer.toString(et31int + 1));



                        } else {
                            et32.setText("");
                        }
                    }

                }
                else
                {



                }
            }
        });

        et33.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                int et31int = Integer.parseInt(et31.getText().toString());
                int et32int = 0;
                et41.setEnabled(true);

                et31.setEnabled(false);
                et32.setEnabled(false);
                et23.setEnabled(false);

                et41.setText(Integer.toString(et31int + 2));

                if(et32.getText().toString().length()>0) {
                    et32int = Integer.parseInt(et32.getText().toString());
                }
                if(et31int>et32int) {
                    if (et31.getText().toString().length() > 0) {

                        et32.setText(Integer.toString(et31int + 1));

                        //  et21.setText(Integer.toString(et11int + 2));




                    } else {
                        et32.setText("");
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s) {



                int et31int = Integer.parseInt(et31.getText().toString());
                int et32int = 0;
                et41.setEnabled(true);

                et31.setEnabled(false);
                et32.setEnabled(false);
                et23.setEnabled(false);

                et41.setText(Integer.toString(et31int + 2));

                if(et32.getText().toString().length()>0) {
                    et32int = Integer.parseInt(et32.getText().toString());
                }

                if(et31int>et32int) {
                    if (et31.getText().toString().length() > 0) {

                        et32.setText(Integer.toString(et31int + 1));
                        //    et21.setText(Integer.toString(et11int + 2));



                    } else {
                        et32.setText("");
                    }
                }


            }
        });

        et33.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus)
                {



                    int et31int = Integer.parseInt(et31.getText().toString());
                    int et32int = 0;

                    if(et32.getText().toString().length()>0) {
                        et32int = Integer.parseInt(et32.getText().toString());
                    }

                    if(et33.getText().toString().length()>0) {
                        et41.setEnabled(true);

                        et31.setEnabled(false);
                        et32.setEnabled(false);
                        et33.setEnabled(false);

                        et41.setText(Integer.toString(et31int + 2));
                    }



                    if(et31int>et32int) {
                        if (et31.getText().toString().length() > 0) {

                            et32.setText(Integer.toString(et31int + 1));




                        } else {
                            et32.setText("");
                        }
                    }

                }
                else
                {



                }
            }
        });


        et41.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {




            }

            @Override
            public void afterTextChanged(Editable s) {


                et42.setEnabled(true);

                int et41int = 0;

                if(et41.getText().toString().length()>0) {
                    et41int = Integer.parseInt(et41.getText().toString());
                }



                int et42int = 0;
                if(et42.getText().toString().length()>0)
                {
                    et42int = Integer.parseInt(et42.getText().toString());
                }


                if(et41int>et42int) {
                    if (et41.getText().toString().length() > 0) {

                        et42.setText(Integer.toString(et41int + 1));

                    } else {
                        et42.setText("");
                    }
                }



            }
        });

        et42.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {


                et43.setEnabled(true);

            }
        });


        et42.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus)
                {
                    int et41int = 0 ;

                    if(et41.getText().toString().length()>0)
                    {
                        et41int = Integer.parseInt(et41.getText().toString());
                    }

                    int et42int = 0;

                    if(et42.getText().toString().length()>0) {
                        et42int = Integer.parseInt(et42.getText().toString());
                    }

                    if(et43.getText().toString().length()>0) {

                    }



                    if(et41int>et42int) {
                        if (et41.getText().toString().length() > 0) {

                            et42.setText(Integer.toString(et41int + 1));



                        } else {
                            et42.setText("");
                        }
                    }

                }
                else
                {



                }
            }
        });

        et43.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                int et41int = Integer.parseInt(et41.getText().toString());
                int et42int = 0;


                et41.setEnabled(false);
                et42.setEnabled(false);
                // et43.setEnabled(false);



                if(et42.getText().toString().length()>0) {
                    et42int = Integer.parseInt(et42.getText().toString());
                }
                if(et41int>et42int) {
                    if (et41.getText().toString().length() > 0) {

                        et42.setText(Integer.toString(et41int + 1));

                        //  et21.setText(Integer.toString(et11int + 2));




                    } else {
                        et42.setText("");
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s) {



                int et41int = Integer.parseInt(et41.getText().toString());
                int et42int = 0;


                et41.setEnabled(false);
                et42.setEnabled(false);
                // et43.setEnabled(false);



                if(et42.getText().toString().length()>0) {
                    et42int = Integer.parseInt(et42.getText().toString());
                }

                if(et41int>et42int) {
                    if (et41.getText().toString().length() > 0) {

                        et42.setText(Integer.toString(et41int + 1));
                        //    et21.setText(Integer.toString(et11int + 2));



                    } else {
                        et42.setText("");
                    }
                }


            }
        });

        et43.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus)
                {



                    int et41int = Integer.parseInt(et41.getText().toString());
                    int et42int = 0;

                    if(et42.getText().toString().length()>0) {
                        et42int = Integer.parseInt(et42.getText().toString());
                    }

                    if(et43.getText().toString().length()>0) {


                        et41.setEnabled(false);
                        et42.setEnabled(false);
                        // et43.setEnabled(false);


                    }



                    if(et41int>et42int) {
                        if (et41.getText().toString().length() > 0) {

                            et42.setText(Integer.toString(et41int + 1));

                        } else {
                            et42.setText("");
                        }
                    }

                }
                else
                {
                    et43.setEnabled(false);



                }
            }
        });


        saverelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String from_qty = "";
                if(et21.getText().toString().length()>0)
                {
                    from_qty = et11.getText().toString()+","+ et21.getText().toString();
                }

                if(et31.getText().toString().length()>0)
                {
                    from_qty = et11.getText().toString()+","+ et21.getText().toString() +","+ et31.getText().toString();
                }

                if(et41.getText().toString().length()>0)
                {
                    from_qty =et11.getText().toString()+","+ et21.getText().toString() +","+ et31.getText().toString() + "," + et41.getText().toString();
                }


                String to_qty = "";
                if(et22.getText().toString().length()>0)
                {
                    to_qty = et12.getText().toString()+","+ et22.getText().toString();
                }

                if(et32.getText().toString().length()>0)
                {
                    to_qty = et12.getText().toString()+","+ et22.getText().toString() +","+ et32.getText().toString();
                }

                if(et42.getText().toString().length()>0)
                {
                    to_qty =et12.getText().toString()+","+ et22.getText().toString() +","+ et32.getText().toString() + "," + et42.getText().toString();
                }

                String discount_per_ton = "";

                if(et13.getText().toString().length()>0)
                {
                    discount_per_ton = et13.getText().toString();
                }

                if(et23.getText().toString().length()>0)
                {
                    discount_per_ton = et13.getText().toString()+","+ et23.getText().toString();
                }

                if(et33.getText().toString().length()>0)
                {
                    discount_per_ton = et13.getText().toString()+","+ et23.getText().toString() +","+ et33.getText().toString();
                }

                if(et43.getText().toString().length()>0)
                {
                    discount_per_ton =et13.getText().toString()+","+ et23.getText().toString() +","+ et33.getText().toString() + "," + et43.getText().toString();
                }


                System.out.println("from qty in promo---"+ from_qty);
                System.out.println("to qty in promo---"+ to_qty);
                System.out.println("discount_per_ton qty in promo---"+ discount_per_ton);

                if(NetworkUtility.checkConnectivity(getActivity())){

                    saverelative.setEnabled(false);

                    String insertpromourl = APIName.URL+"/seller/insertPromo?user_code="+ userCodestr + "&mobile_number=" + mobilenumstr;
                    System.out.println("INSERT PROMO URL IS---"+ insertpromourl);
                    InsertPromoAPI(insertpromourl,from_qty,to_qty,discount_per_ton);

                }
                else{
                    util.dialog(getActivity(), "Please check your internet connection.");
                }


            }
        });
        b = dialogBuilder.create();
        b.show();


        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });



    }

    private void InsertPromoAPI(final String url,final String fromqty,final String toqty,final String discount) {


        System.out.println("INSERT PROMO URL in resp IS---"+ url);

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        System.out.println("RESPONSE OF INSERT PROMO API IS---" + response);



                        try {



                            JSONObject editinstalledcapacityjson = new JSONObject(response);
                            System.out.println("iNSERT PROMO JSON IS---" + editinstalledcapacityjson);

                            String statusstr = editinstalledcapacityjson.getString("status");
                            String msgstr = editinstalledcapacityjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Toast.makeText(getActivity(),msgstr,Toast.LENGTH_SHORT).show();
                                b.dismiss();

                            }
                            else
                            {
                                Toast.makeText(getActivity(),msgstr,Toast.LENGTH_SHORT).show();
                                b.dismiss();
                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block

                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(),error.toString(),Toast.LENGTH_SHORT).show();


                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("ERROR CODE--------" + networkResponse.statusCode);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(getActivity(), messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("from_qty",fromqty);
                params.put("to_qty",toqty);
                params.put("discount_per_ton",discount);
                params.put("promo_code",SingletonActivity.promocode);


                System.out.println("insert promo params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }

/*
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == 10001) && (resultCode == Activity.RESULT_OK))
            // recreate your fragment here
            ftt = getFragmentManager().beginTransaction();
        ftt.detach(SellTodaysOfferFragment.this).attach(SellTodaysOfferFragment.this).commit();
    }*/

    @Override
    public void onResume() {
        super.onResume();

        System.out.println("SellTodaysOfferFragment ONRESUME CALLED-------");


    }

    @Override
    public void onPause() {
        super.onPause();

        System.out.println("SellTodaysOfferFragment ONPAUSE CALLED-------");
    }



    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            //do when hidden
            System.out.println("onHiddenChanged IF CALLED-------");
        } else {
            //do when show
            System.out.println("onHiddenChanged ELSE CALLED-------");
        }
    }


    private class CustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        private JSONArray sellerTodaysOfferJSONArray;
        private JSONArray sub_categoryJsonArray;
        String promocodestr;



        public CustomAdap(Context mainActivity,JSONArray sub_categoryJsonArray,JSONArray sellerTodaysOfferJSONArray,String promocodestr)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.sellerTodaysOfferJSONArray = sellerTodaysOfferJSONArray;
            this.sub_categoryJsonArray = sub_categoryJsonArray;
            this.promocodestr = promocodestr;
            inflater = (LayoutInflater)c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub


            return sellerTodaysOfferJSONArray.length();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub



            Holder holder = new Holder();

            final View rowView;


            rowView = inflater.inflate(R.layout.sell_todays_offer_row, null);

            holder.proddesc = (TextView) rowView.findViewById(R.id.productdesctxtvw);
            holder.sub_cat_txt = (TextView) rowView.findViewById(R.id.subcatdesctxtvw);
            holder.qty = (TextView)rowView.findViewById(R.id.qtydesctxtvw);
            holder.remqty = (TextView)rowView.findViewById(R.id.remqtydesctxtvw);
            holder.minrateval = (TextView)rowView.findViewById(R.id.pricedesctxtvw);
            holder.viewoffertxtvw = (TextView) rowView.findViewById(R.id.viewtxtvw);
            holder.edittxtvw = (TextView)rowView.findViewById(R.id.edittxtvw);
            holder.deletetxtvw= (TextView)rowView.findViewById(R.id.deletetxtvw);

            holder.editiv=(ImageView) rowView.findViewById(R.id.editiv);
            holder.deleteiv=(ImageView) rowView.findViewById(R.id.deleteiv);
            SingletonActivity.promocode = promocodestr;


            System.out.println("promo code in custom adap==="+ SingletonActivity.promocode);
            SingletonActivity.fromselltodaysoffer = true;

            try {
                if(sellerTodaysOfferJSONArray.getJSONObject(position).getString("sell_status").equalsIgnoreCase("Trade Removed"))
                {
                  //  holder.edittxtvw.setVisibility(View.INVISIBLE);
                   // holder.deletetxtvw.setVisibility(View.INVISIBLE);
                    holder.edittxtvw.setEnabled(false);
                    holder.edittxtvw.setTextColor(Color.parseColor("#D3D3D3"));
                    holder.deletetxtvw.setEnabled(false);
                    holder.deletetxtvw.setTextColor(Color.parseColor("#D3D3D3"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }



            holder.deleteiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                    if (todaysoffer.equalsIgnoreCase("0")) {

                        new AlertDialog.Builder(getActivity())
                                .setTitle("Alert")
                                .setMessage("Do you really want to Delete Offer?")
                                // .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        if (NetworkUtility.checkConnectivity(getActivity())) {

                                            try {
                                                String sellerid = sellerTodaysOfferJSONArray.getJSONObject(position).getString("sell_id");

                                                String SellerDeleteOfferUrl = APIName.URL + "/seller/deleteOffer?sell_id=" + sellerid + "&user_code=" + userCodestr + "&mobile_num=" + mobilenumstr;
                                                System.out.println("SELLER DELETE OFFER URL IS---" + SellerDeleteOfferUrl);
                                                SellerDeleteOfferAPI(SellerDeleteOfferUrl);

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }


                                        } else {
                                            util.dialog(getActivity(), "Please check your internet connection.");
                                        }

                                    }
                                })
                                .setNegativeButton(android.R.string.no, null).show();

                    }
                    else
                    {
                        Toast.makeText(getActivity(), "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                    }

                }
            });

            holder.editiv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (todaysoffer.equalsIgnoreCase("0")) {


                        try {

                            //New Changes After Add New Offer Enhancements---

                            String length_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("length");

                            String sellerid = sellerTodaysOfferJSONArray.getJSONObject(position).getString("sell_id");
                            String user_code_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("user_code");
                            String mobile_num_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("mobile_num");
                            String sell_date_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("sell_date");
                            String sell_sequence_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("sell_sequence");
                            String categoryid = sellerTodaysOfferJSONArray.getJSONObject(position).getString("category");
                            String subcategoryid = sellerTodaysOfferJSONArray.getJSONObject(position).getString("sub_category");
                            String category_grade_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("category_grade");
                            String size_range_start_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("size_range_start");
                            String size_range_end_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("size_range_end");
                            String q1_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q1_title");
                            String q1_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q1_value");
                            String q2_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q2_title");
                            String q2_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q2_value");
                            String q3_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q3_title");
                            String q3_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q3_value");
                            String q4_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q4_title");
                            String q4_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q4_value");
                            String q5_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q5_title");
                            String q5_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q5_value");
                            String q6_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q6_title");
                            String q6_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q6_value");
                            String q7_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q7_title");
                            String q7_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q7_value");
                            String q8_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q8_title");
                            String q8_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q8_value");
                            String q1val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q1");
                            String q2val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q2");
                            String q3val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q3");
                            String q4val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q4");
                            String q5val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q5");
                            String q6val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q6");
                            String q7val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q7");
                            String q8val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q8");
                            String advanceratedesc = sellerTodaysOfferJSONArray.getJSONObject(position).getString("advance_rate");
                            String nextdayratedesc = sellerTodaysOfferJSONArray.getJSONObject(position).getString("next_day_rate");
                            String regularratedesc = sellerTodaysOfferJSONArray.getJSONObject(position).getString("regular_rate");
                            String qtyofferedstr = sellerTodaysOfferJSONArray.getJSONObject(position).getString("quantity");
                            String is_active_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("is_active");
                            String remaining_quantity_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("remaining_quantity");
                            String promocodeval = sellerTodaysOfferJSONArray.getJSONObject(position).getString("promo_code");
                            String tncval = sellerTodaysOfferJSONArray.getJSONObject(position).getString("sell_tandc");
                            String sell_status_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("sell_status");
                            String transport_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("transport");
                            String loading_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("loading");
                            String gst_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("gst");
                            String insurance_select_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("insurance_select");
                            String insurance_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("insurance");
                            String modified_field_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("modified_field");
                            String who_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("who");
                            String when_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("when");
                            String posteddatetime_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("posteddatetime");
                            String terms_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("terms");
                            String categorydesc = sellerTodaysOfferJSONArray.getJSONObject(position).getString("category_name");
                            String subcategorydesc = sellerTodaysOfferJSONArray.getJSONObject(position).getString("sub_category_name");
                            String category_grade_name_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("category_grade_name");

                            final String delivery_type_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("delivery_type");
                            final String delivery_in_days_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("delivery_in_days");
                            final String material_inspection_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("material_inspection_type");
                            final String weightment_type_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("weightment_type");
                            final String test_certificate_type_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("test_certificate_type");




                            SingletonActivity.fromaddnewoffer = false;
                            SingletonActivity.frombackofbuyerdispatch = false;
                            SingletonActivity.frombackofsellerdispatch = false;
                            SingletonActivity.frombackofbuyerpayment = false;
                            SingletonActivity.fromviewlivetrade = false;
                            SingletonActivity.isNewEnquiryClicked = false;
                            SingletonActivity.fromupdatenewoffer = false;
                            SingletonActivity.frombuyerhistorywebview = false;
                            SingletonActivity.fromsellerhistorywebview = false;
                            SingletonActivity.fromlivepricegraph = false;

                            SingletonActivity.isUpdateClicked = true;
                            SingletonActivity.fromselltodaysoffer = true;

                            Intent i = new Intent(getActivity(), AddNewOfferLiveTradeEnhancementActivity0712.class);
                            SingletonActivity.sub_categoryJsonArray = sub_categoryJsonArray;

                            if(!promocodeval.equalsIgnoreCase(" ")) {
                                JSONArray promoJSONArray = sellerTodaysOfferJSONArray.getJSONObject(position).getJSONArray("promo_details");
                                System.out.println("PROMO JSON ARRAY LENGTH IS===" + promoJSONArray.length());

                                SingletonActivity.PromoJSONArray = promoJSONArray;
                            }

                            i.putExtra("sellerid", sellerid);
                            i.putExtra("user_code_str", user_code_str);
                            i.putExtra("mobile_num_str", mobile_num_str);
                            i.putExtra("advanceratedesc", advanceratedesc);
                            i.putExtra("nextdayratedesc", nextdayratedesc);
                            i.putExtra("regularratedesc", regularratedesc);
                            i.putExtra("q1val", q1val);
                            i.putExtra("q2val", q2val);
                            i.putExtra("q3val", q3val);
                            i.putExtra("q4val", q4val);
                            i.putExtra("q5val", q5val);
                            i.putExtra("q6val", q6val);
                            i.putExtra("q7val", q7val);
                            i.putExtra("q8val", q8val);
                            i.putExtra("promocodeval", promocodeval);
                            i.putExtra("tncval", tncval);
                            i.putExtra("sellerid", sellerid);
                            i.putExtra("categoryid", categoryid);
                            i.putExtra("subcategoryid", subcategoryid);
                            i.putExtra("sell_date_str", sell_date_str);
                            i.putExtra("sell_sequence_str", sell_sequence_str);
                            i.putExtra("category_grade_str", category_grade_str);
                            i.putExtra("size_range_start_str", size_range_start_str);
                            i.putExtra("size_range_end_str", size_range_end_str);
                            i.putExtra("q1_title_str", q1_title_str);
                            i.putExtra("q1_value_str", q1_value_str);
                            i.putExtra("q2_title_str", q2_title_str);
                            i.putExtra("q2_value_str", q2_value_str);
                            i.putExtra("q3_title_str", q3_title_str);
                            i.putExtra("q3_value_str", q3_value_str);
                            i.putExtra("q4_title_str", q4_title_str);
                            i.putExtra("q4_value_str", q4_value_str);
                            i.putExtra("q5_title_str", q5_title_str);
                            i.putExtra("q5_value_str", q5_value_str);
                            i.putExtra("q6_title_str", q6_title_str);
                            i.putExtra("q6_value_str", q6_value_str);
                            i.putExtra("q7_title_str", q7_title_str);
                            i.putExtra("q7_value_str", q7_value_str);
                            i.putExtra("q8_title_str", q8_title_str);
                            i.putExtra("q8_value_str", q8_value_str);
                            i.putExtra("qtyofferedstr", qtyofferedstr);
                            i.putExtra("is_active_str", is_active_str);
                            i.putExtra("remaining_quantity_str", remaining_quantity_str);
                            i.putExtra("sell_status_str", sell_status_str);
                            i.putExtra("transport_str", transport_str);
                            i.putExtra("loading_str", loading_str);
                            i.putExtra("gst_str", gst_str);
                            i.putExtra("insurance_select_str", insurance_select_str);
                            i.putExtra("insurance_str", insurance_str);
                            i.putExtra("modified_field_str", modified_field_str);
                            i.putExtra("who_str", who_str);
                            i.putExtra("when_str", when_str);
                            i.putExtra("posteddatetime_str", posteddatetime_str);
                            i.putExtra("terms_str", terms_str);
                            i.putExtra("categorydesc", categorydesc);
                            i.putExtra("subcategorydesc", subcategorydesc);
                            i.putExtra("category_grade_name_str", category_grade_name_str);

                            i.putExtra("delivery_type",delivery_type_str);
                            i.putExtra("delivery_in_days",delivery_in_days_str);
                            i.putExtra("material_inspection",material_inspection_str);
                            i.putExtra("weighment_type",weightment_type_str);
                            i.putExtra("test_certificate_type",test_certificate_type_str);

                            i.putExtra("length",length_str);

                            startActivity(i);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    else
                    {
                        Toast.makeText(getActivity(), "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                    }

                }
            });




            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        String categoryid = sellerTodaysOfferJSONArray.getJSONObject(position).getString("category");
                        String qtyofferedstr = sellerTodaysOfferJSONArray.getJSONObject(position).getString("quantity");
                        // String companyname = sellerTodaysOfferJSONArray.getJSONObject(position).getString("company_name");
                        String categorydesc = sellerTodaysOfferJSONArray.getJSONObject(position).getString("category_name");
                        String subcategorydesc = sellerTodaysOfferJSONArray.getJSONObject(position).getString("sub_category_name");
                        //    String locationdesc = sellerTodaysOfferJSONArray.getJSONObject(position).getString("location");
                        String advanceratedesc = sellerTodaysOfferJSONArray.getJSONObject(position).getString("advance_rate");
                        String nextdayratedesc = sellerTodaysOfferJSONArray.getJSONObject(position).getString("next_day_rate");
                        String regularratedesc = sellerTodaysOfferJSONArray.getJSONObject(position).getString("regular_rate");
                        String q1val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q1");
                        String q2val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q2");
                        String q3val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q3");
                        String q4val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q4");
                        String q5val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q5");
                        String q6val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q6");
                        String q7val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q7");
                        String q8val = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q8");
                        String promocodeval = sellerTodaysOfferJSONArray.getJSONObject(position).getString("promo_code");
                        String tncval = sellerTodaysOfferJSONArray.getJSONObject(position).getString("sell_tandc");
                        String size_range_start_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("size_range_start");
                        String size_range_end_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("size_range_end");
                        String q1_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q1_title");
                        String q1_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q1_value");
                        String q2_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q2_title");
                        String q2_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q2_value");
                        String q3_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q3_title");
                        String q3_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q3_value");
                        String q4_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q4_title");
                        String q4_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q4_value");
                        String q5_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q5_title");
                        String q5_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q5_value");
                        String q6_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q6_title");
                        String q6_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q6_value");
                        String q7_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q7_title");
                        String q7_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q7_value");
                        String q8_title_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q8_title");
                        String q8_value_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("q8_value");
                        String transport_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("transport");
                        String loading_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("loading");
                        String gst_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("gst");
                        String insurance_select_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("insurance_select");
                        String insurance_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("insurance");
                        String category_grade_name_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("category_grade_name");
                        String length_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("length");
                        String modified_field_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("modified_field");

                        final String delivery_type_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("delivery_type");
                        final String delivery_in_days_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("delivery_in_days");
                        final String material_inspection_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("material_inspection_type");
                        final String weightment_type_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("weightment_type");
                        final String test_certificate_type_str = sellerTodaysOfferJSONArray.getJSONObject(position).getString("test_certificate_type");


                        ViewOfferDialog(modified_field_str,delivery_type_str,delivery_in_days_str,material_inspection_str,weightment_type_str,test_certificate_type_str,category_grade_name_str,length_str,transport_str,loading_str,gst_str,insurance_select_str,insurance_str,q1_title_str,q1_value_str,q2_title_str,q2_value_str,q3_title_str,q3_value_str,q4_title_str,q4_value_str,q5_title_str,q5_value_str,q6_title_str,q6_value_str,q7_title_str,q7_value_str,q8_title_str,q8_value_str,categoryid,qtyofferedstr,categorydesc,subcategorydesc,advanceratedesc,nextdayratedesc,regularratedesc,q1val,q2val,q3val,q4val,q5val,q6val,q7val,q8val,promocodeval,tncval,size_range_start_str,size_range_end_str);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });



            try {
                final String categorynamestr = sellerTodaysOfferJSONArray.getJSONObject(position).getString("category_name");
                final String subcategorynamestr = sellerTodaysOfferJSONArray.getJSONObject(position).getString("sub_category_name");
                System.out.println("CATEGORY NAME IN SELL TODAY'S OFFER---" + categorynamestr);
                holder.proddesc.setText(sellerTodaysOfferJSONArray.getJSONObject(position).getString("category_name"));

                if(sellerTodaysOfferJSONArray.getJSONObject(position).getString("category").equalsIgnoreCase("1")||sellerTodaysOfferJSONArray.getJSONObject(position).getString("category").equalsIgnoreCase("2")) {
                    holder.sub_cat_txt.setText(sellerTodaysOfferJSONArray.getJSONObject(position).getString("sub_category_name"));
                }
                else {
                    holder.sub_cat_txt.setText(sellerTodaysOfferJSONArray.getJSONObject(position).getString("size_range_start")+ "-" + sellerTodaysOfferJSONArray.getJSONObject(position).getString("size_range_end"));
                }

                holder.qty.setText(sellerTodaysOfferJSONArray.getJSONObject(position).getString("quantity"));

                holder.remqty.setText("Rem:" + sellerTodaysOfferJSONArray.getJSONObject(position).getString("remaining_quantity"));

                if(!sellerTodaysOfferJSONArray.getJSONObject(position).getString("remaining_quantity").equalsIgnoreCase("0.000")&&(sellerTodaysOfferJSONArray.getJSONObject(position).getString("is_active").equalsIgnoreCase("1")))
                {

                    holder.editiv.setVisibility(View.VISIBLE);
                    holder.deleteiv.setVisibility(View.VISIBLE);

                }
                else
                {
                    holder.editiv.setVisibility(View.GONE);
                    holder.deleteiv.setVisibility(View.GONE);
                }


                ArrayList<Float> priceAl = new ArrayList<Float>();

                priceAl.clear();

                float a = 0.0f;
                float n = 0.0f;
                float r = 0.0f;

                if(!sellerTodaysOfferJSONArray.getJSONObject(position).getString("advance_rate").equalsIgnoreCase("null"))
                {
                     a = Float.parseFloat(sellerTodaysOfferJSONArray.getJSONObject(position).getString("advance_rate"));
                }

                if(!sellerTodaysOfferJSONArray.getJSONObject(position).getString("next_day_rate").equalsIgnoreCase("null"))
                {
                     n = Float.parseFloat(sellerTodaysOfferJSONArray.getJSONObject(position).getString("next_day_rate"));
                }

                if(!sellerTodaysOfferJSONArray.getJSONObject(position).getString("regular_rate").equalsIgnoreCase("null"))
                {
                     r = Float.parseFloat(sellerTodaysOfferJSONArray.getJSONObject(position).getString("regular_rate"));
                }





                System.out.println("ADVANCE RATE---" + sellerTodaysOfferJSONArray.getJSONObject(position).getString("advance_rate"));
                System.out.println("NEXT RATE---" + sellerTodaysOfferJSONArray.getJSONObject(position).getString("next_day_rate"));
                System.out.println("REGULAR RATE---" + sellerTodaysOfferJSONArray.getJSONObject(position).getString("regular_rate"));


                HashMap<String, Float> hashMap = new HashMap<String, Float>();


                String ratestr = "";
                if (a > 0.00 || n > 0.00 || r > 0.00) {

                    if (a != 0.0) {

                        hashMap.put("A", a);


                    }

                    if (n != 0.0) {

                        hashMap.put("N", n);

                    }

                    if (r != 0.0) {

                        hashMap.put("R", r);

                    }


                    System.out.println("HASHMAP ---" + hashMap);


                    Float min = Collections.min(hashMap.values());
                    System.out.println("min value in hashmap---" + min); // 0.1

                    for (Map.Entry<String, Float> entry : hashMap.entrySet()) {
                        if (entry.getValue().equals(min)) {
                            System.out.println("key of min value---" + entry.getKey());
                            holder.minrateval.setText(min +"0"+"(" + entry.getKey() + ")");
                        }
                    }

                }



            } catch (JSONException e) {
                e.printStackTrace();
            }





            return rowView;

        }


    }


    public class Holder {
        TextView proddesc,sub_cat_txt,remqty,minrateval,qty;
        TextView viewoffertxtvw,edittxtvw,deletetxtvw;
        ImageView editiv,deleteiv;



    }

    public void ViewOfferDialog(String modified_field_str,String delivery_type_str,String delivery_in_days_str,String material_inspection_str,String weighment_type_str,String test_certificate_type_str,String category_grade_name_str,String length_str,String transport_str,String loading_str,String gst_str,String insurance_select_str,String insurance_str,String q1_title_str,String q1_value_str,String q2_title_str,String q2_value_str,String q3_title_str,String q3_value_str,String q4_title_str,String q4_value_str,String q5_title_str,String q5_value_str,String q6_title_str,String q6_value_str,String q7_title_str,String q7_value_str,String q8_title_str,String q8_value_str,String categoryid,String qtyoffered,String categorydescstr,String subcategorydescstr,String advancedpricedescstr,String nextpricedescstr,String regularpricestr,String q1val,String q2val,String q3val,String q4val,String q5val,String q6val,String q7val,String q8val,String promo,String terms,String size_range_start_str,String size_range_end_str) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.view_seller_todays_offer, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final TextView cmpnynametitletxt = (TextView)dialogView.findViewById(R.id.companytitletxtvw);
        final TextView cmpnynamedesctxt = (TextView) dialogView.findViewById(R.id.cmpnynamedesctxt);
        final TextView categorydesctxt = (TextView) dialogView.findViewById(R.id.categorydesctxt);
        final TextView subcategorydesctxt = (TextView) dialogView.findViewById(R.id.subcategorydesctxt);
        final TextView locationtitletxtvw = (TextView) dialogView.findViewById(R.id.locationtitletxtvw);
        final TextView locationdesctxt = (TextView) dialogView.findViewById(R.id.locationdesctxt);
        final TextView advancedpricedesctxtvw = (TextView) dialogView.findViewById(R.id.advancedpricedesctxtvw);
        final TextView nextpricedesctxtvw = (TextView) dialogView.findViewById(R.id.nextpricedesctxtvw);
        final TextView regularpricedesctxtvw = (TextView) dialogView.findViewById(R.id.regularpricedesctxtvw);
        final TextView qtyoffereddesctxtvw = (TextView)dialogView.findViewById(R.id.qtyoffereddesctxtvw);
        final TextView pricetxt1 = (TextView) dialogView.findViewById(R.id.pricetxt1);
        final TextView pricetxt2 = (TextView) dialogView.findViewById(R.id.pricetxt2);
        final TextView pricetxt3 = (TextView) dialogView.findViewById(R.id.pricetxt3);
        final TextView pricetxt4 = (TextView) dialogView.findViewById(R.id.pricetxt4);
        final TextView pricetxt5 = (TextView) dialogView.findViewById(R.id.pricetxt5);
        final TextView pricetxt6 = (TextView) dialogView.findViewById(R.id.pricetxt6);
        final TextView pricetxt7 = (TextView) dialogView.findViewById(R.id.pricetxt7);
        final TextView pricetxt8 = (TextView) dialogView.findViewById(R.id.pricetxt8);
        final TextView promodesctxt = (TextView) dialogView.findViewById(R.id.promodesctxt);
        final TextView termsdesctxt = (TextView) dialogView.findViewById(R.id.termsdesctxt);
        final View line3 = (View)dialogView.findViewById(R.id.line3);
        final TextView transportationdesctxt = (TextView) dialogView.findViewById(R.id.transportationdesctxt);
        final TextView loadingdesctxt = (TextView) dialogView.findViewById(R.id.loadingdesctxt);
        final TextView gstdesctxt = (TextView) dialogView.findViewById(R.id.gstdesctxt);
        final TextView insurancedesctxt = (TextView) dialogView.findViewById(R.id.insurancedesctxt);
        final TextView lengthdesctxt = (TextView) dialogView.findViewById(R.id.lengthdesctxt);


        final TextView q1title = (TextView) dialogView.findViewById(R.id.q1title);
        final TextView q2title = (TextView) dialogView.findViewById(R.id.q2title);
        final TextView q3title = (TextView) dialogView.findViewById(R.id.q3title);
        final TextView q4title = (TextView) dialogView.findViewById(R.id.q4title);
        final TextView q5title = (TextView) dialogView.findViewById(R.id.q5title);
        final TextView q6title = (TextView) dialogView.findViewById(R.id.q6title);
        final TextView q7title = (TextView) dialogView.findViewById(R.id.q7title);
        final TextView q8title = (TextView) dialogView.findViewById(R.id.q8title);
        final TextView gradedesctxt = (TextView) dialogView.findViewById(R.id.gradedesctxt);

        final TextView lengthtitletxtvw =  (TextView) dialogView.findViewById(R.id.lengthtitletxtvw);

        final TextView deliveryindaysdesctxt = (TextView)dialogView.findViewById(R.id.deliveryindaysdesctxt);
        final TextView materialinspectiondesctxt = (TextView)dialogView.findViewById(R.id.materialinspectiondesctxt);
        final TextView weighmentdesctxt = (TextView)dialogView.findViewById(R.id.weighmentdesctxt);
        final TextView testcertificatedesctxt = (TextView)dialogView.findViewById(R.id.testcertificatedesctxt);

        if(delivery_type_str.equalsIgnoreCase("1"))
        {
            deliveryindaysdesctxt.setText("As per Aachar Sanhita");
        }
        if(delivery_type_str.equalsIgnoreCase("2"))
        {
            deliveryindaysdesctxt.setText(delivery_in_days_str+ " days");
        }

        if(material_inspection_str.equalsIgnoreCase("1"))
        {
            materialinspectiondesctxt.setText("Buyer's Place");
        }
        if(material_inspection_str.equalsIgnoreCase("2"))
        {
            materialinspectiondesctxt.setText("Seller's Place");
        }

        if(weighment_type_str.equalsIgnoreCase("1"))
        {
            weighmentdesctxt.setText("Buyer's Place");
        }
        if(weighment_type_str.equalsIgnoreCase("2"))
        {
            weighmentdesctxt.setText("Seller's Place");
        }
        if(weighment_type_str.equalsIgnoreCase("3"))
        {
            weighmentdesctxt.setText("Buyer & Seller's Place");
        }


        if(test_certificate_type_str.equalsIgnoreCase("1"))
        {
           // testcertificatedesctxt.setText("Required");
            testcertificatedesctxt.setText("Yes");
        }
        if(test_certificate_type_str.equalsIgnoreCase("2"))
        {
          //  testcertificatedesctxt.setText("Not Required");
            testcertificatedesctxt.setText("No");
        }



        System.out.println("LENGTH IS==="+ length_str.length());

        if(length_str.length()!=4) {
           // lengthdesctxt.setText(length_str+" m");

            line3.setVisibility(View.VISIBLE);
            lengthtitletxtvw.setVisibility(View.VISIBLE);
            lengthdesctxt.setVisibility(View.VISIBLE);

            if(categoryid.equalsIgnoreCase("1")) {

                lengthtitletxtvw.setText("Length");
                lengthdesctxt.setText(length_str+" m");
            }

            if(categoryid.equalsIgnoreCase("2"))
            {
                lengthtitletxtvw.setText("Weight");
                lengthdesctxt.setText(length_str+" kg");
            }
        }
        else
        {
            line3.setVisibility(View.GONE);
            lengthtitletxtvw.setVisibility(View.GONE);
            lengthdesctxt.setVisibility(View.GONE);
        }

        gradedesctxt.setText(category_grade_name_str);

      /*  cmpnynametitletxt.setVisibility(View.GONE);
        line3.setVisibility(View.GONE);
        locationtitletxtvw.setVisibility(View.GONE);
        cmpnynamedesctxt.setVisibility(View.GONE);
        locationdesctxt.setVisibility(View.GONE);*/

        categorydesctxt.setText(categorydescstr);
      //  subcategorydesctxt.setText(subcategorydescstr);
        qtyoffereddesctxtvw.setText(qtyoffered);
        advancedpricedesctxtvw.setText(advancedpricedescstr+"(A)");
        nextpricedesctxtvw.setText(nextpricedescstr+"(N)");
        regularpricedesctxtvw.setText(regularpricestr+"(R)");
        pricetxt1.setText(q1_value_str);
        pricetxt2.setText(q2_value_str);
        pricetxt3.setText(q3_value_str);
        pricetxt4.setText(q4_value_str);
        pricetxt5.setText(q5_value_str);
        pricetxt6.setText(q6_value_str);




      /*  pricetxt7.setText(q7_value_str);
        pricetxt8.setText(q8_value_str);*/

        if(!q7_value_str.equalsIgnoreCase(""))
        {
            pricetxt7.setText(q7_value_str);
        }
        else {
            pricetxt7.setText("NA");
        }

        if(!q8_value_str.equalsIgnoreCase(""))
        {
            pricetxt8.setText(q8_value_str);
        }
        else
        {
            pricetxt8.setText("NA");
        }

        List<String> modified_field_List = Arrays.asList(modified_field_str.split(","));




        if (modified_field_List.contains("-3-"))
        {
            pricetxt1.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-4-"))
        {
            pricetxt2.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-5-"))
        {
            pricetxt3.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-6-"))
        {
            pricetxt4.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-7-"))
        {
            pricetxt5.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-8-"))
        {
            pricetxt6.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-9-"))
        {
            pricetxt7.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-10-"))
        {
            pricetxt8.setTextColor(Color.RED);
        }




        if(promo.equalsIgnoreCase("0")||promo.equalsIgnoreCase("(null)"))
        {
            promodesctxt.setText("--");
        }
        else {
            promodesctxt.setText(promo);
        }


        termsdesctxt.setText(terms);

        q1title.setText(q1_title_str);
        q2title.setText(q2_title_str);
        q3title.setText(q3_title_str);
        q4title.setText(q4_title_str);
        q5title.setText(q5_title_str);
        q6title.setText(q6_title_str);
        q7title.setText(q7_title_str);
        q8title.setText(q8_title_str);


        if(transport_str.equalsIgnoreCase("1"))
        {
            transportationdesctxt.setText("Seller's End");
        }
        if(transport_str.equalsIgnoreCase("2"))
        {
            transportationdesctxt.setText("Buyer's End");
        }
        if(transport_str.equalsIgnoreCase("3"))
        {
            transportationdesctxt.setText("Mxmart's End");
        }

        if(loading_str.equalsIgnoreCase("NA"))
        {
            loadingdesctxt.setText(loading_str);
        }
        else {
            loadingdesctxt.setText(loading_str + " \u20B9"+"/MT");
        }
        gstdesctxt.setText(gst_str);

        if(insurance_select_str.equalsIgnoreCase("NA"))
        {
            insurancedesctxt.setText("NA");
        }
        else {

            if (insurance_str.equalsIgnoreCase("1")) {
                insurancedesctxt.setText("Seller");
            } else {
                insurancedesctxt.setText("Buyer");
            }
        }


        if(categoryid.equalsIgnoreCase("1")||categoryid.equalsIgnoreCase("2")) {
            subcategorydesctxt.setText(subcategorydescstr);
        }
        else {
            subcategorydesctxt.setText(size_range_start_str + "-" + size_range_end_str);
        }


        final  AlertDialog b = dialogBuilder.create();
        b.show();


        ImageView ic_close = (ImageView)dialogView.findViewById(R.id.ic_close);
        ic_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }

    private void SellerDeleteOfferAPI(String url) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF SELLER DELETE OFFER API IS---" + response);



                        JSONObject SellerTodaysOfferJson = null;
                        try {
                            SellerTodaysOfferJson = new JSONObject(response);


                            String statusstr = SellerTodaysOfferJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                String messagestr = SellerTodaysOfferJson.getString("message");


                                Toast.makeText(getActivity(),messagestr,Toast.LENGTH_SHORT).show();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                       /* Intent i = new Intent(getActivity(), SellActivity.class);
                                        startActivity(i);*/

                                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        ft.detach(SellTodaysOfferFragment1012.this).attach(SellTodaysOfferFragment1012.this).commit();

                                    }
                                }, 1000);


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get live offer params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }


    private void SellerTodaysOfferAPI(String url) {

       /* pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();*/



     //   getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
          //      WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        bar.setVisibility(View.VISIBLE);

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(getActivity()));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {

                       // pdia.dismiss();


                       // getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {

                                bar.setVisibility(View.GONE);

                                System.out.println("RESPONSE OF SELLER TODAY'S OFFER API IS---" + response);


                                JSONObject SellerTodaysOfferJson = null;
                                try {
                                    SellerTodaysOfferJson = new JSONObject(response);


                                    String statusstr = SellerTodaysOfferJson.getString("status");
                                    String promocodestr = SellerTodaysOfferJson.getString("promo_code");
                                    SingletonActivity.promocode = promocodestr;

                                    if(statusstr.equalsIgnoreCase("true"))
                                    {

                                        noofferfoundtxt.setVisibility(View.GONE);
                                        selltodaysofferlistvw.setVisibility(View.VISIBLE);

                                        JSONArray SellerTodaysOfferJsonArray = SellerTodaysOfferJson.getJSONArray("offer");
                                        System.out.println("SELLER TODAY'S OFFER JSONARRAY IS---" + SellerTodaysOfferJsonArray);

                                        JSONArray sub_categoryJsonArray = SellerTodaysOfferJson.getJSONArray("sub_category");
                                        System.out.println("SELLER TODAY'S OFFER SUBCATEGORY JSONARRAY IS---" + sub_categoryJsonArray);


                                        if(getActivity()!=null) {

                                            customadap = new CustomAdap(getActivity(), sub_categoryJsonArray,SellerTodaysOfferJsonArray, promocodestr);
                                            selltodaysofferlistvw.setAdapter(customadap);
                                        }

                                    }
                                    else
                                    {
                                        noofferfoundtxt.setVisibility(View.VISIBLE);
                                        selltodaysofferlistvw.setVisibility(View.GONE);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                 //   pdia.dismiss();

                                 //   getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            //dialog.dismiss();
                                            bar.setVisibility(View.GONE);

                                        }
                                    }, 3000); // 3000 milliseconds delay

                                }
                            }
                        }, 3000); // 3000 milliseconds delay





                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                       //  pdia.dismiss();

                   //     getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        handler.postDelayed(new Runnable() {
                            public void run() {
                                //dialog.dismiss();
                                bar.setVisibility(View.GONE);

                            }
                        }, 3000); // 3000 milliseconds delay


                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(getActivity(),"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get live offer params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(),hurlStack);
        requestQueue.add(stringRequest);
    }




}

