package com.smtc.mxmart.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pushbots.push.Pushbots;
import com.pushbots.push.utils.PBConstants;
import com.smtc.mxmart.AddNewOfferLiveTradeEnhancementActivity0712;
import com.smtc.mxmart.HomeActivity;
import com.smtc.mxmart.LoginActivity;
import com.smtc.mxmart.SessionManager;
import com.smtc.mxmart.SingletonActivity;
import com.smtc.mxmart.util.Config;
import com.smtc.mxmart.util.NotificationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**

 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    static String notify;
    static String title;
    String mobile_num;
    boolean login;

   // List<ThemeModel> themeModels = new ArrayList<ThemeModel>();
   public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "Remote JSON: " + remoteMessage.toString());
       // Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData());

            try {
               // JSONObject json = new JSONObject(remoteMessage.getData().toString());
              //



                Map<String, String> params = remoteMessage.getData();
                JSONObject object = new JSONObject(params);
                Log.e("JSON_OBJECT", object.toString());
                handleDataMessage(object);

            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }else{
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());
        System.out.println("PUSH JSON ---"+ json.toString());

       // SingletonActivity.isLiveSalesClicked=true;

        try {
          //  JSONObject data = json.getJSONObject("data");

            String typestr = json.getString("type");
            String indexstr = json.getString("index");
            String mobilestr = json.getString("mobile");
            String message = json.getString("message");
            String title = json.getString("nTitle");
            String imageUrl = "http://mxmart.in/public/home/img/mxmart/logo.png";




            SingletonActivity.notificationmobileno = mobilestr;

            SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
            login = prefs.getBoolean("login", false);
            mobile_num = prefs.getString("mobile_num", null);

            System.out.println("login value==="+ login);
            SessionManager sessionManager = new SessionManager(getApplicationContext());

            SingletonActivity.isNewEnquiryClicked=false;
            SingletonActivity.isSpTradeSellerClicked = false;
            SingletonActivity.isSellerAcceptedClicked=false;
            SingletonActivity.isSpTradeBuyerClicked=false;
            SingletonActivity.fromselltodaysoffer = false;
            SingletonActivity.isLiveSalesClicked = false;
            SingletonActivity.fromviewlivetrade = false;
            SingletonActivity.frombackofbuyerdispatch = false;
            SingletonActivity.fromaddnewoffer = false;
            SingletonActivity.setdailylimitfromtrade = false;

            SingletonActivity.fromlivepricegraph = false;
            SingletonActivity.frombuycurrentenquiry = false;
            SingletonActivity.backfromeditpersonaldetails = false;
            SingletonActivity.backfromeditbusinessdetails = false;
            SingletonActivity.backfromeditcontactdetails = false;
            SingletonActivity.backfromeditinstalledcapacitydetails = false;
            SingletonActivity.backfromeditreferencedetails = false;
            SingletonActivity.updatedailylimitfromtrade = false;
            SingletonActivity.fromupdatenewoffer = false;
            SingletonActivity.frombackofsellerdispatch = false;
            SingletonActivity.frombackofsellerpayment = false;
            SingletonActivity.frombackofbuyerpayment = false;
            SingletonActivity.fromsellerhistorywebview = false;
            SingletonActivity.frombuyerhistorywebview = false;





            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {




                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);

                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);




                Log.e(TAG, "IF LOOPS");
               // showNotificationMessage(getApplicationContext(), title, message, timestamp, pushNotification);

                if(typestr.equalsIgnoreCase("live_sale")) {


                    Log.e(TAG, "FOREGROUND LIVE SALES");

                    if(sessionManager.isLoggedIn()==true)
                    {



                        if(SingletonActivity.notificationmobileno.equalsIgnoreCase(mobile_num)){
                            if(indexstr.equalsIgnoreCase("0")) {




                            /*    SingletonActivity.isNewEnquiryClicked=false;
                                SingletonActivity.isSpTradeSellerClicked = false;
                                SingletonActivity.isSellerAcceptedClicked=false;
                                SingletonActivity.isSpTradeBuyerClicked=false;
                                SingletonActivity.fromselltodaysoffer = false;
                                SingletonActivity.fromviewlivetrade = false;
                                SingletonActivity.frombackofbuyerdispatch = false;*/
                                SingletonActivity.isLiveSalesClicked=true;

                                pushNotification = new Intent(getApplicationContext(), HomeActivity.class);




                            }



                        }else{
                            pushNotification = new Intent(getApplicationContext(), HomeActivity.class);
                        }
                    }
                    else
                    {
                        pushNotification = new Intent(getApplicationContext(), LoginActivity.class);
                    }

                    // check for image attachment

                 /*   if (TextUtils.isEmpty(imageUrl)) {
                        showNotificationMessage(getApplicationContext(), title, message, imageUrl, pushNotification);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, imageUrl, pushNotification, "");
                    }
*/

                }



                if(typestr.equalsIgnoreCase("sell")) {

                    Log.e(TAG, "FOREGROUND SELL");

                   System.out.println("NOTIFICATION MOBILE NUMBER==="+ SingletonActivity.notificationmobileno);
                   System.out.println("SHARED PREFERENCE MOBILE NUMBER==="+ mobile_num);


                    if(sessionManager.isLoggedIn()==true)
                    {


                        Log.e(TAG, "LOGIN TRUE");



                        if(SingletonActivity.notificationmobileno.equalsIgnoreCase(mobile_num)){
                       //     Log.e("notification shoutout",SingletonActivity.notificationid);

                            System.out.println("SESSION MANAGER ISLOGGED IN SHOUTOUT==="+ sessionManager.isLoggedIn());



                            if(indexstr.equalsIgnoreCase("1")) {



                             /*   SingletonActivity.isSpTradeSellerClicked = false;
                                SingletonActivity.isSellerAcceptedClicked=false;
                                SingletonActivity.isSpTradeBuyerClicked=false;

                                SingletonActivity.fromselltodaysoffer = false;
                                SingletonActivity.fromviewlivetrade = false;
                                SingletonActivity.frombackofbuyerdispatch = false;
                                SingletonActivity.isLiveSalesClicked = false;*/
                                SingletonActivity.isNewEnquiryClicked=true;

                                pushNotification = new Intent(getApplicationContext(), HomeActivity.class);

                            }

                            if(indexstr.equalsIgnoreCase("2")) {


                              /*  SingletonActivity.isNewEnquiryClicked=false;
                                SingletonActivity.isSellerAcceptedClicked=false;
                                SingletonActivity.isSpTradeBuyerClicked=false;
                                SingletonActivity.isLiveSalesClicked = false;
                                SingletonActivity.fromselltodaysoffer = false;
                                SingletonActivity.fromviewlivetrade = false;
                                SingletonActivity.frombackofbuyerdispatch = false;*/
                                SingletonActivity.isSpTradeSellerClicked = true;

                                pushNotification = new Intent(getApplicationContext(), HomeActivity.class);

                            }

                        }
                        else{
                        pushNotification = new Intent(getApplicationContext(), HomeActivity.class);
                        }
                }
                else
                {
                    Log.e(TAG, "LOGIN FALSE");
                    pushNotification = new Intent(getApplicationContext(), LoginActivity.class);
                }


                    // check for image attachment
                   /* if (TextUtils.isEmpty(imageUrl)) {
                      //  Log.e("notification shoutout",SingletonActivity.notificationid);
                        showNotificationMessage(getApplicationContext(), title, message, "", pushNotification);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, "", pushNotification, imageUrl);
                    }*/
                }
                if(typestr.equalsIgnoreCase("buy")) {


                    Log.e(TAG, "FOREGROUND BUY");

                    if(sessionManager.isLoggedIn()==true)
                    {
                        if(SingletonActivity.notificationmobileno.equalsIgnoreCase(mobile_num)){
                            if(indexstr.equalsIgnoreCase("0")) {


                               /* SingletonActivity.isNewEnquiryClicked=false;
                                SingletonActivity.isSpTradeSellerClicked = false;
                                SingletonActivity.isSpTradeBuyerClicked=false;
                                SingletonActivity.isLiveSalesClicked = false;
                                SingletonActivity.fromselltodaysoffer = false;
                                SingletonActivity.fromviewlivetrade = false;
                                SingletonActivity.frombackofbuyerdispatch = false;*/
                                SingletonActivity.isSellerAcceptedClicked=true;

                                pushNotification = new Intent(getApplicationContext(), HomeActivity.class);


                            }

                            if(indexstr.equalsIgnoreCase("1")) {


                              /*  SingletonActivity.isNewEnquiryClicked=false;
                                SingletonActivity.isSpTradeSellerClicked = false;
                                SingletonActivity.isSellerAcceptedClicked=false;
                                SingletonActivity.isLiveSalesClicked = false;
                                SingletonActivity.fromselltodaysoffer = false;
                                SingletonActivity.fromviewlivetrade = false;
                                SingletonActivity.frombackofbuyerdispatch = false;*/
                                SingletonActivity.isSpTradeBuyerClicked=true;

                                pushNotification = new Intent(getApplicationContext(), HomeActivity.class);

                            }




                        }else{
                            pushNotification = new Intent(getApplicationContext(), HomeActivity.class);
                        }
                    }
                    else
                    {
                        pushNotification = new Intent(getApplicationContext(), LoginActivity.class);
                    }

                    // check for image attachment
                 /*   if (TextUtils.isEmpty(imageUrl)) {
                        showNotificationMessage(getApplicationContext(), title, message, "", pushNotification);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, "", pushNotification, imageUrl);
                    }
*/

                }



            }

           else
            {



                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);

                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);


               // showNotificationMessage(getApplicationContext(), title, message, timestamp, pushNotification);

                if(typestr.equalsIgnoreCase("live_sale")) {


                    Log.e(TAG, "FOREGROUND LIVE SALES");

                    if(sessionManager.isLoggedIn()==true)
                    {
                        if(SingletonActivity.notificationmobileno.equalsIgnoreCase(mobile_num)){
                            if(indexstr.equalsIgnoreCase("0")) {

                                SingletonActivity.isLiveSalesClicked=true;
                               /* SingletonActivity.isNewEnquiryClicked=false;
                                SingletonActivity.isSpTradeSellerClicked = false;
                                SingletonActivity.isSellerAcceptedClicked=false;
                                SingletonActivity.isSpTradeBuyerClicked=false;
                                SingletonActivity.fromselltodaysoffer = false;
                                SingletonActivity.fromviewlivetrade = false;
                                SingletonActivity.frombackofbuyerdispatch = false;*/


                                pushNotification = new Intent(getApplicationContext(), HomeActivity.class);

                            }
                        }else{
                            pushNotification = new Intent(getApplicationContext(), HomeActivity.class);
                        }
                    }
                    else
                    {
                        pushNotification = new Intent(getApplicationContext(), LoginActivity.class);
                    }

                    // check for image attachment
                   /* if (TextUtils.isEmpty(imageUrl)) {
                        showNotificationMessage(getApplicationContext(), title, message, "", pushNotification);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, "", pushNotification, imageUrl);
                    }*/


                }

                if(typestr.equalsIgnoreCase("sell")) {

                    Log.e(TAG, "FOREGROUND SHOUTOUT");



                    if(sessionManager.isLoggedIn()==true)
                    {

                        Log.e(TAG, "LOGIN TRUE");

                        System.out.println("NOTIFICATION MOBILE NUMBER==="+ SingletonActivity.notificationmobileno);
                        System.out.println("SHARED PREFERENCE MOBILE NUMBER==="+ mobile_num);

                        if(SingletonActivity.notificationmobileno.equalsIgnoreCase(mobile_num)){
                            //     Log.e("notification shoutout",SingletonActivity.notificationid);

                            if(indexstr.equalsIgnoreCase("1")) {

                              //  SingletonActivity.isLiveSalesClicked = false;
                                SingletonActivity.isNewEnquiryClicked=true;
                            /*    SingletonActivity.isSpTradeSellerClicked = false;
                                SingletonActivity.isSellerAcceptedClicked=false;
                                SingletonActivity.isSpTradeBuyerClicked=false;

                                SingletonActivity.fromselltodaysoffer = false;
                                SingletonActivity.fromviewlivetrade = false;
                                SingletonActivity.frombackofbuyerdispatch = false;*/


                                pushNotification = new Intent(getApplicationContext(), HomeActivity.class);

                            }

                            if(indexstr.equalsIgnoreCase("2")) {

                             //   SingletonActivity.isNewEnquiryClicked=false;
                                SingletonActivity.isSpTradeSellerClicked = true;

                             /*   SingletonActivity.isSpTradeSellerClicked = false;
                                SingletonActivity.isSellerAcceptedClicked=false;
                                SingletonActivity.isLiveSalesClicked = false;
                                SingletonActivity.fromselltodaysoffer = false;
                                SingletonActivity.fromviewlivetrade = false;
                                SingletonActivity.frombackofbuyerdispatch = false;*/


                                pushNotification = new Intent(getApplicationContext(), HomeActivity.class);

                            }

                        }else{
                            pushNotification = new Intent(getApplicationContext(), HomeActivity.class);
                        }
                    }
                    else
                    {
                        Log.e(TAG, "LOGIN FALSE");
                        pushNotification = new Intent(getApplicationContext(), LoginActivity.class);
                    }

                    // check for image attachment
                  /*  if (TextUtils.isEmpty(imageUrl)) {
                        //  Log.e("notification shoutout",SingletonActivity.notificationid);
                        showNotificationMessage(getApplicationContext(), title, message, "", pushNotification);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, "", pushNotification, imageUrl);
                    }*/
                }
                if(typestr.equalsIgnoreCase("buy")) {


                    Log.e(TAG, "FOREGROUND RESULT");

                    if(sessionManager.isLoggedIn()==true)
                    {
                        if(SingletonActivity.notificationmobileno.equalsIgnoreCase(mobile_num)){
                            if(indexstr.equalsIgnoreCase("0")) {

                              //  SingletonActivity.isSpTradeSellerClicked = false;
                                SingletonActivity.isSellerAcceptedClicked=true;
                             /*   SingletonActivity.isNewEnquiryClicked=false;

                                SingletonActivity.isSpTradeBuyerClicked=false;
                                SingletonActivity.isLiveSalesClicked = false;
                                SingletonActivity.fromselltodaysoffer = false;
                                SingletonActivity.fromviewlivetrade = false;
                                SingletonActivity.frombackofbuyerdispatch = false;*/


                                pushNotification = new Intent(getApplicationContext(), HomeActivity.class);


                            }

                            if(indexstr.equalsIgnoreCase("1")) {

                             //   SingletonActivity.isSellerAcceptedClicked=false;
                                SingletonActivity.isSpTradeBuyerClicked=true;
                            /*    SingletonActivity.isNewEnquiryClicked=false;
                                SingletonActivity.isSpTradeSellerClicked = false;

                                SingletonActivity.isLiveSalesClicked = false;


                                SingletonActivity.fromselltodaysoffer = false;
                                SingletonActivity.fromviewlivetrade = false;
                                SingletonActivity.frombackofbuyerdispatch = false;*/


                                pushNotification = new Intent(getApplicationContext(), HomeActivity.class);

                            }




                        }else{
                            pushNotification = new Intent(getApplicationContext(), HomeActivity.class);
                        }
                    }
                    else
                    {
                        pushNotification = new Intent(getApplicationContext(), LoginActivity.class);
                    }



                    // check for image attachment
                   /* if (TextUtils.isEmpty(imageUrl)) {
                        showNotificationMessage(getApplicationContext(), title, message, "", pushNotification);
                    } else {


                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, "", pushNotification, imageUrl);
                    }
*/

                }



            }


        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
           e.printStackTrace();
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
       intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      //  intent.setFlags(FLAG_ACTIVITY_MULTIPLE_TASK);
        intent.setAction(Long.toString(System.currentTimeMillis()));
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setAction(Long.toString(System.currentTimeMillis()));
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}
