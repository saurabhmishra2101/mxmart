package com.smtc.mxmart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;

import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class EditInstalledCapacityActivity extends AppCompatActivity {

    Typeface source_sans_pro_normal;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    UtilsDialog util = new UtilsDialog();
    String userCodestr,mobilenumstr,spongeironidstr,spongeironcatstr;
    ProgressDialog pdia;
    String pelletstr,spongeironstr,ingotsstr,billetsstr;
    EditText pelletedttxt,spongeironedttxt,ingotsedttxt,billetssedttxt;
    String billetcapacitystr,spongeironcapacitystr,ingotscapacitystr,pelletcapacitystr,billetidstr,billetcatstr,ingotsidstr,ingotscatstr,pelletidstr,pelletcatstr;
    ImageView backiv;
    SessionManager sessionManager;
    String fcm_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_installed_capacity);

        SharedPreferences prefs = EditInstalledCapacityActivity.this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num",null);
        fcm_id = prefs.getString("fcm_id",null);

      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.editinstalledcapacitytoolbar);
        setSupportActionBar(toolbar);*/

        sessionManager = new SessionManager(getApplicationContext());

        backiv = (ImageView)findViewById(R.id.backicon);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(EditInstalledCapacityActivity.this,HomeActivity.class);


                SingletonActivity.backfromeditcontactdetails = false;
                SingletonActivity.backfromeditbusinessdetails = false;
                SingletonActivity.backfromeditpersonaldetails = false;
                SingletonActivity.fromviewlivetrade = false;
                SingletonActivity.fromaddnewoffer = false;
                SingletonActivity.isNotificationClicked = false;
                // SingletonActivity.index = index;
                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                SingletonActivity.FromAddNewOfferTabOne=false;
                SingletonActivity.FromAddNewOfferTabZero=false;
                SingletonActivity.isNewEnquiryClicked = false;
                SingletonActivity.frombackofbuyerdispatch = false;
                SingletonActivity.frombackofsellerdispatch = false;
                SingletonActivity.frombackofbuyerpayment = false;
                SingletonActivity.frombackofsellerpayment = false;
                SingletonActivity.fromselltodaysoffer = false;
                SingletonActivity.backfromeditinstalledcapacitydetails = true;

                startActivity(i);

            }
        });



        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

        pelletedttxt = (EditText) findViewById(R.id.pelletedt);
        pelletedttxt.setTypeface(source_sans_pro_normal);

        spongeironedttxt = (EditText) findViewById(R.id.spongeironedt);
        spongeironedttxt.setTypeface(source_sans_pro_normal);

        ingotsedttxt = (EditText) findViewById(R.id.ingotsedt);
        ingotsedttxt.setTypeface(source_sans_pro_normal);

        billetssedttxt = (EditText) findViewById(R.id.billetssedt);
        billetssedttxt.setTypeface(source_sans_pro_normal);


        if(SingletonActivity.installedCapacityjsonArray!=null)
        {

            for (int i = 0; i < SingletonActivity.installedCapacityjsonArray.length(); i++) {
                try {

                    if (SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category").equalsIgnoreCase("Billets")) {
                        billetssedttxt.setText(SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity"));
                        billetcapacitystr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity");
                        billetidstr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("id");
                        billetcatstr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category");
                    }

                    if (SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category").equalsIgnoreCase("Sponge Iron")) {
                        spongeironedttxt.setText(SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity"));
                        spongeironcapacitystr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity");
                        spongeironidstr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("id");
                        spongeironcatstr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category");
                    }
                    if (SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category").equalsIgnoreCase("Ingots")) {
                        ingotsedttxt.setText(SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity"));
                        ingotscapacitystr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity");
                        ingotsidstr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("id");
                        ingotscatstr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category");
                    }

                    if (SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category").equalsIgnoreCase("Pellets")) {

                        pelletedttxt.setText(SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity"));
                        pelletcapacitystr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("capacity");
                        pelletidstr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("id");
                        pelletcatstr = SingletonActivity.installedCapacityjsonArray.getJSONObject(i).getString("category");
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //  Toast.makeText(LoginActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                }


            }

        }

        else
        {

            billetidstr = "";
            spongeironidstr = "";
            ingotsidstr = "";
            pelletidstr = "";

            billetcatstr = "Billets";
            spongeironcatstr = "Sponge Iron";
            ingotscatstr = "Ingots";
            pelletcatstr = "Pellets";



         /*   params.put("id",billetidstr+","+spongeironidstr+","+ingotsidstr+","+pelletidstr);
            params.put("capacity",billetsstr+","+spongeironstr+","+ingotsstr+","+pelletstr);
            params.put("category",billetcatstr+","+spongeironcatstr+","+ingotscatstr+","+pelletcatstr);*/

        }


        RelativeLayout submitrel = (RelativeLayout)findViewById(R.id.sbmtrelative);
        submitrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pelletstr = pelletedttxt.getText().toString();
                spongeironstr = spongeironedttxt.getText().toString();
                ingotsstr = ingotsedttxt.getText().toString();
                billetsstr = billetssedttxt.getText().toString();


                System.out.println("USER CODE IN EDIT INSTALLED CAPACITY DETAILS FRAGMENT---"+ userCodestr);

                boolean invalid = false;

                if((pelletstr.equalsIgnoreCase(""))&&(spongeironstr.equalsIgnoreCase("")&&(ingotsstr.equalsIgnoreCase("")&&(billetsstr.equalsIgnoreCase("")))))
                {
                    invalid = true;
                    Toast.makeText(EditInstalledCapacityActivity.this,
                            "Please provide Capacity for atleast 1 Material.", Toast.LENGTH_SHORT)
                            .show();
                }

                else if((pelletstr.equalsIgnoreCase("0"))&&(spongeironstr.equalsIgnoreCase("0")&&(ingotsstr.equalsIgnoreCase("0")&&(billetsstr.equalsIgnoreCase("0")))))
                {
                    invalid = true;
                    Toast.makeText(EditInstalledCapacityActivity.this,
                            "Please provide Capacity for atleast 1 Material.", Toast.LENGTH_SHORT)
                            .show();
                }
                else if((pelletstr.equalsIgnoreCase(""))&&(spongeironstr.equalsIgnoreCase("0")&&(ingotsstr.equalsIgnoreCase("0")&&(billetsstr.equalsIgnoreCase("0")))))
                {
                    invalid = true;
                    Toast.makeText(EditInstalledCapacityActivity.this,
                            "Please provide Capacity for atleast 1 Material.", Toast.LENGTH_SHORT)
                            .show();
                }

                else if((pelletstr.equalsIgnoreCase(""))&&(spongeironstr.equalsIgnoreCase("")&&(ingotsstr.equalsIgnoreCase("0")&&(billetsstr.equalsIgnoreCase("0")))))
                {
                    invalid = true;
                    Toast.makeText(EditInstalledCapacityActivity.this,
                            "Please provide Capacity for atleast 1 Material.", Toast.LENGTH_SHORT)
                            .show();
                }

                else if((pelletstr.equalsIgnoreCase(""))&&(spongeironstr.equalsIgnoreCase("")&&(ingotsstr.equalsIgnoreCase("")&&(billetsstr.equalsIgnoreCase("0")))))
                {
                    invalid = true;
                    Toast.makeText(EditInstalledCapacityActivity.this,
                            "Please provide Capacity for atleast 1 Material.", Toast.LENGTH_SHORT)
                            .show();
                }

                else if((pelletstr.equalsIgnoreCase("0"))&&(spongeironstr.equalsIgnoreCase("")&&(ingotsstr.equalsIgnoreCase("")&&(billetsstr.equalsIgnoreCase("")))))
                {
                    invalid = true;
                    Toast.makeText(EditInstalledCapacityActivity.this,
                            "Please provide Capacity for atleast 1 Material.", Toast.LENGTH_SHORT)
                            .show();
                }
                else if((pelletstr.equalsIgnoreCase("0"))&&(spongeironstr.equalsIgnoreCase("0")&&(ingotsstr.equalsIgnoreCase("")&&(billetsstr.equalsIgnoreCase("")))))
                {
                    invalid = true;
                    Toast.makeText(EditInstalledCapacityActivity.this,
                            "Please provide Capacity for atleast 1 Material.", Toast.LENGTH_SHORT)
                            .show();
                }

                else if((pelletstr.equalsIgnoreCase("0"))&&(spongeironstr.equalsIgnoreCase("0")&&(ingotsstr.equalsIgnoreCase("0")&&(billetsstr.equalsIgnoreCase("")))))
                {
                    invalid = true;
                    Toast.makeText(EditInstalledCapacityActivity.this,
                            "Please provide Capacity for atleast 1 Material.", Toast.LENGTH_SHORT)
                            .show();
                }

                else if (invalid == false) {

                    if (NetworkUtility.checkConnectivity(EditInstalledCapacityActivity.this)) {
                        String editinstalledcapacitydetailsurl = APIName.URL + "/user/insertUpdateCapDetails?user_code=" + userCodestr + "&mobile_num=" + mobilenumstr;
                        System.out.println("EDIT INSTALLED CAPACITY DETAILS URL IS---" + editinstalledcapacitydetailsurl);
                        EditInstalledCapacityProfileAPI(editinstalledcapacitydetailsurl, userCodestr);

                    } else {
                        util.dialog(EditInstalledCapacityActivity.this, "Please check your internet connection.");
                    }
                }



            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(EditInstalledCapacityActivity.this,HomeActivity.class);


        SingletonActivity.backfromeditcontactdetails = false;
        SingletonActivity.backfromeditbusinessdetails = false;
        SingletonActivity.backfromeditpersonaldetails = false;
        SingletonActivity.fromviewlivetrade = false;
        SingletonActivity.fromaddnewoffer = false;
        SingletonActivity.isNotificationClicked = false;
        // SingletonActivity.index = index;
        SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
        SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
        SingletonActivity.FromAddNewOfferTabOne=false;
        SingletonActivity.FromAddNewOfferTabZero=false;
        SingletonActivity.isNewEnquiryClicked = false;
        SingletonActivity.frombackofbuyerdispatch = false;
        SingletonActivity.frombackofsellerdispatch = false;
        SingletonActivity.frombackofbuyerpayment = false;
        SingletonActivity.frombackofsellerpayment = false;
        SingletonActivity.fromselltodaysoffer = false;
        SingletonActivity.backfromeditinstalledcapacitydetails = true;

        startActivity(i);
    }

    private void EditInstalledCapacityProfileAPI(final String url, final String usercode) {
        pdia = new ProgressDialog(EditInstalledCapacityActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(EditInstalledCapacityActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        System.out.println("EDIT INSTALLED CAPACITY DETAILS URL in resp IS---"+ url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();



                        System.out.println("RESPONSE OF EDIT INSTALLED CAPACITY API IS---" + response);



                        try {



                            JSONObject editinstalledcapacityjson = new JSONObject(response);
                            System.out.println("EDIT INSTALLED CAPACITY JSON IS---" + editinstalledcapacityjson);

                            String statusstr = editinstalledcapacityjson.getString("status");
                            String msgstr = editinstalledcapacityjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Toast.makeText(EditInstalledCapacityActivity.this,msgstr,Toast.LENGTH_SHORT).show();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(EditInstalledCapacityActivity.this,HomeActivity.class);

                                        SingletonActivity.backfromeditinstalledcapacitydetails = true;
                                        SingletonActivity.backfromeditcontactdetails = false;
                                        SingletonActivity.backfromeditbusinessdetails = false;
                                        SingletonActivity.backfromeditpersonaldetails = false;
                                        SingletonActivity.fromviewlivetrade = false;
                                        SingletonActivity.fromaddnewoffer = false;
                                        SingletonActivity.isNotificationClicked = false;
                                        // SingletonActivity.index = index;
                                        SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                        SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                        SingletonActivity.FromAddNewOfferTabOne=false;
                                        SingletonActivity.FromAddNewOfferTabZero=false;
                                        SingletonActivity.isNewEnquiryClicked = false;
                                        SingletonActivity.frombackofbuyerdispatch = false;
                                        SingletonActivity.frombackofsellerdispatch = false;
                                        SingletonActivity.frombackofbuyerpayment = false;
                                        SingletonActivity.frombackofsellerpayment = false;


                                        startActivity(i);


                                    }
                                }, 2000);
                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditInstalledCapacityActivity.this,error.toString(),Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("ERROR CODE--------" + networkResponse.statusCode);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(EditInstalledCapacityActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(EditInstalledCapacityActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(EditInstalledCapacityActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id",billetidstr+","+spongeironidstr+","+ingotsidstr+","+pelletidstr);
                params.put("capacity",billetsstr+","+spongeironstr+","+ingotsstr+","+pelletstr);
                params.put("category",billetcatstr+","+spongeironcatstr+","+ingotscatstr+","+pelletcatstr);




                System.out.println("edit installed capacity params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditInstalledCapacityActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
        //  return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                System.out.println("OVERFLOWMENU ITEM 1 CLICKED");

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("login",false);


                editor.clear();
                editor.commit();



                sessionManager.logoutUser(mobilenumstr,fcm_id);


                // sessionManager.logoutUser();

            /*    Intent i = new Intent(LeaveManagementActivity.this,LoginActivity.class);
                startActivity(i);
*/
                return true;
          /*  case R.id.overflowmenuitem2:
                System.out.println("OVERFLOWMENU ITEM 2 CLICKED");

                return true;
            case R.id.overflowmenuitem3:
                System.out.println("OVERFLOWMENU ITEM 3 CLICKED");

                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
