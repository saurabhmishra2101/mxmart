package com.smtc.mxmart;

import android.os.Bundle;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class AbbrevationActivity extends AppCompatActivity {

    ImageView backicon;
    TextView abbrevtitle6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abbrevation);

        backicon = (ImageView)findViewById(R.id.backicon);
        abbrevtitle6 = (TextView)findViewById(R.id.abbrevtitle6);

        abbrevtitle6.setText( " \u20B9");
        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

    }

}
