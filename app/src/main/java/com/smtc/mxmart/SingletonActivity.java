package com.smtc.mxmart;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.pushbots.push.Pushbots;
import com.pushbots.push.utils.PBPrefs;
import com.splunk.mint.Mint;

import org.acra.ACRA;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/*import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;*/

public class SingletonActivity extends Application {

    public static  SingletonActivity  _sInstane=null;
    Context context;

    public static JSONObject getUserDetailsJson,stuserjson,stuserprofjson;
    public static JSONArray HomePlatesListJSONArray,PromoJSONArray,sub_categoryJsonArray,contactjsonArray,installedCapacityjsonArray,referencejsonArray,liveOfferJsonArray,liveOfferPigIronJsonArray,FinishedEnqJSONArray,SellerTodaysOfferJsonArray,SellerGetPromoJsonArray,paymentDetailsJSONArray,SellerpaymentDetailsJSONArray,SellerHistoryJSONArray,BuyerHistoryJSONArray;
    public static int validatetab,count,index;
    public static JSONArray billetOfferJsonArray;
    public static JSONArray ingotOfferJsonArray;
    public static JSONArray spongeOfferJsonArray;
    public static JSONArray pilletOfferJsonArray;

    public static boolean enquiryfromhomepage = false;
    public static boolean enquiryfromplatespage = false;
    public static boolean isPassedFromLogin  = false;
    public static boolean isFABClicked = false;
    public static boolean isUpdateClicked = false;
    public static boolean isValidTradeTimeOpen = false;
    public static boolean isSpNotificationClicked = false;
    public static boolean isBuyerRejectedClicked = false;
    public static boolean isNotificationClicked = false;
    public static boolean isTodaysOfferClicked = false;
    public static boolean selleditoffer = false;
    public static ArrayList<String> SubCategoryArrayList = new ArrayList<String>();
    public static boolean FromAddNewOfferTabOne = false;
    public static boolean FromAddNewOfferTabZero = false;
    public static boolean FromAddNewOfferTabTwoBuyRej = false;
    public static boolean FromAddNewOfferTabTwoNewEnq = false;
    public static boolean loginstate = false;
    public static boolean isSplitSaudaClicked = false;
    public static boolean fromforgot = false;
    public static boolean backfromeditpersonaldetails = false;
    public static boolean backfromeditbusinessdetails = false;
    public static boolean backfromeditcontactdetails = false;
    public static boolean backfromeditinstalledcapacitydetails = false;
    public static boolean backfromeditreferencedetails = false;
    public static boolean updatedailylimitfromtrade = false;
    public static boolean setdailylimitfromtrade = false;
    public static boolean fromaddnewoffer = false;
    public static boolean fromupdatenewoffer = false;
    public static boolean fromviewlivetrade = false;
    public static boolean fromviewpigiron = false;
    public static boolean isNewEnquiryClicked = false;
    public static boolean isSellerAcceptedClicked = false;
    public static boolean isSellerRejectedClicked = false;
    public static boolean isSpTradeBuyerClicked = false;
    public static boolean isSpTradeSellerClicked = false;
    public static boolean isIsBuyerRejectedClicked = false;
    public static boolean isdailylimitAvailable = false;
    public static boolean frombuycurrentenquiry = false;
    public static boolean frombackofsellerdispatch = false;
    public static boolean frombackofsellerpayment = false;
    public static boolean frombackofbuyerdispatch = false;
    public static boolean frombackofbuyerpayment = false;
    public static boolean fromselltodaysoffer = false;
    public static boolean fromsellnotificationindexzero = false;
    public static boolean isLiveSalesClicked = false;
    public static boolean fromsellcurrentdeals = false;
    public static boolean frombuycurrentdeals = false;
    public static boolean isLivePriceClicked = false;
    public static boolean frombuyerhistorywebview = false;
    public static boolean fromsellerhistorywebview = false;
    public static boolean  fromhometopayment = false;
    public static boolean  fromlivepricegraph = false;
    public static boolean isPopUpClicked = false;
    public static String clickedaddicon = "";
    public static String splited = "";
    public static String gst_str = "";

    public static Menu menu;
    public static  int clickedcount = 0;
    public static  int checkboxclickedposition = 0;
    public static TabHost host;
    public static View tabView,tabView1,tabView2,tabView3;
    public static RelativeLayout tabhostrelative;
    public static String from = "non";
 // public static TabLayout tablayout,selltablayout;
    public static String promocode,addlimitval,tradeid,sellid,usercode,sellermobilenum,sellerusercode,buyerusercode,buyermobilenum,splitqty,trader,graphtitle;
    public static TextView selltxt,myprofiletxt;
    public static ImageView sellimgvw,myprofileimgvw;
    public static String ratetype,rate,categoryname,sellercompanyname,sellercity,updatestatus;
    public static int estdateyr,estdatemonth,estdateday;
    public static String regemail,regmobileno,stotp,sauda_patra_gen_date;
    public static String buyParent,invoice_gen_date;
    public  static String usermobile, useremail,mail_otp_str,notificationmobileno,fcmid,fcmidnew,doc,dispatchdocurl;
    public  static JSONObject notify;
    private RequestQueue mRequestQueue;

    public static Uri selectedFileURI;
    public static final String TAG = SingletonActivity.class.getSimpleName();

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();


        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/sourcesansproregular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/sourcesansproregular.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/sourcesansproregular.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/sourcesansproregular.ttf");
      //  FontsOverride.setDefaultFont(this, "DEFAULT_BOLD", "fonts/sourcesansprobold.ttf");
             _sInstane=this;

        // Initialize Pushbots Library
        Pushbots.sharedInstance().init(this);



    }


    public static SingletonActivity getInstance() {

        if(_sInstane==null)
        {
        }
        return _sInstane;
    }

    private static TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
        final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
        return new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return originalTrustManager.getAcceptedIssuers();
            }

            public void checkClientTrusted(X509Certificate[] certs,
                                           String authType) {
                try {
                    if (certs != null && certs.length > 0) {
                        certs[0].checkValidity();
                    } else {
                        originalTrustManager
                                .checkClientTrusted(certs, authType);
                    }
                } catch (CertificateException e) {
                    Log.w("checkClientTrusted", e.toString());
                }
            }

            public void checkServerTrusted(X509Certificate[] certs,
                                           String authType) {
                try {
                    if (certs != null && certs.length > 0) {
                        certs[0].checkValidity();
                    } else {
                        originalTrustManager
                                .checkServerTrusted(certs, authType);
                    }
                } catch (CertificateException e) {
                    Log.w("checkServerTrusted", e.toString());
                }
            }
        } };
    }

    public static SSLSocketFactory getSSLSocketFactory(Context context)
            throws CertificateException, KeyStoreException, IOException,
            NoSuchAlgorithmException, KeyManagementException {

// the certificate file will be stored in \app\src\main\res\raw folder path
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInput = context.getResources().openRawResource(
                R.raw.mxmart_in);

        Certificate ca = cf.generateCertificate(caInput);
        caInput.close();

        KeyStore keyStore = KeyStore.getInstance("BKS");

        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        TrustManager[] wrappedTrustManagers = getWrappedTrustManagers(tmf
                .getTrustManagers());

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, wrappedTrustManagers, null);

        return sslContext.getSocketFactory();
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {

            HurlStack hurlStack = new HurlStack() {
                @Override
                protected HttpURLConnection createConnection(java.net.URL url)
                        throws IOException {
                    HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                            .createConnection(url);
                    try {
                        httpsURLConnection
                                .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SingletonActivity.this));
                        // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return httpsURLConnection;
                }
            };

            mRequestQueue = Volley.newRequestQueue(getApplicationContext(),hurlStack);
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);


       ACRA.init(this);
       Mint.initAndStartSession(this, "256b2f8d");


    }

}
