package com.smtc.mxmart;



import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.smtc.mxmart.Fragment.BuyCurrentDealsFragment;
import com.smtc.mxmart.Fragment.BuyCurrentEnquiriesFragment;
import com.smtc.mxmart.Fragment.BuyHistoryFragment;
import com.smtc.mxmart.Fragment.SellCurrentDealsFragment;
import com.smtc.mxmart.Fragment.SellCurrentEnquiriesFragment;
import com.smtc.mxmart.Fragment.SellHistoryFragment;
import com.smtc.mxmart.Fragment.SellTodaysOfferFragment;

/**
 * Created by 10161 on 5/3/2017.
 */


public class BuyTabsAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public BuyTabsAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {


          case 0:

              BuyCurrentEnquiriesFragment tab1 = new BuyCurrentEnquiriesFragment();
                return tab1;


            case 1:
                BuyCurrentDealsFragment tab2 = new BuyCurrentDealsFragment();
                return tab2;



            case 2:
                BuyHistoryFragment tab3 = new BuyHistoryFragment();
                return tab3;



            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}