package com.smtc.mxmart;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DispatchWebviewActivity extends AppCompatActivity {

    String url;
    WebView webView;
    ImageView backicon;
    ProgressBar progressBar;
    TextView progresspercent;
    String saudapatraurl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispatch_webview);

        url = SingletonActivity.dispatchdocurl;

        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progresspercent = (TextView)findViewById(R.id.percent);



        backicon = (ImageView)findViewById(R.id.backicon);

        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();



            }
        });



        webView = (WebView) findViewById(R.id.webview);

        webView.setWebChromeClient(new WebChromeClient(){


            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                progresspercent.setText(progress + " %");
                if (progress == 100) {
                    progresspercent.setVisibility(View.GONE);

                } else {
                    progresspercent.setVisibility(View.VISIBLE);

                }
            }

        });
        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccess(true);

        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        webView.clearCache(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        if (url.endsWith(".pdf") || url.endsWith(".PDF") || url.endsWith(".docx") || url.endsWith(".doc") || url.endsWith(".jpg") || url.endsWith(".png") || url.endsWith(".JPG") || url.endsWith(".PNG")|| url.endsWith(".jpeg")|| url.endsWith(".JPEG")) {
            // progressBar.setVisibility(View.VISIBLE);
            System.out.println("IN IF LOOP----"+ url);

            if (url.endsWith(".pdf") || url.endsWith(".PDF") || url.endsWith(".docx") || url.endsWith(".doc")) {
                webView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + url);
            }
            else
            {
                webView.loadUrl(url);
            }



            webView.setDownloadListener(new DownloadListener() {
                public void onDownloadStart(String url, String userAgent,
                                            String contentDisposition, String mimetype,
                                            long contentLength) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            });
        } else {

            System.out.println("IN ELSE LOOP----");
            webView.loadUrl(url);
        }

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // progressBar.setVisibility(View.VISIBLE);
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {

            //    progressBar.setProgress(10);
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {


            super.onPageStarted(view, url, favicon);
            // progressBar.setVisibility(View.GONE);
        }
    }


}
