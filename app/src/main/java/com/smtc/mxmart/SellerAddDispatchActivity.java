package com.smtc.mxmart;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.OpenableColumns;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.smtc.mxmart.Fragment.SellTodaysOfferFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.WeakHashMap;

import javax.net.ssl.HttpsURLConnection;

public class SellerAddDispatchActivity extends AppCompatActivity {

    TextView selectfiletxtvw,upload_from_memory,click_image_from_camera;
    ImageView cancel_dialog;
    private static final int CAPTURE_IMAGE_CAPTURE_CODE = 1888;
    private static final int SELECT_PICTURE = 1;
    String attachmentstr;
    static  EditText dispatch_date_edt,invoice_due_date_edt;
    EditText invoice_amount_edt;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String userCodestr,mobilenumstr,dispatchremainingpaystr,dispatchremainingqtystr,rate_str,gst_str,israte,quoted_rate_str;
    String invoice_quantity_str,invoice_amount_str;
    UtilsDialog util = new UtilsDialog();
    JSONArray SellerCurrentDealsByTradeJsonArray;
    DatePickerFragment newFragment;
    DatePickerFragment1 newFragment1;
    ProgressDialog pdia;
    String dispatch_date_str,truck_num_str,transport_name_str,mobile_num_str,invoice_num_str,invoice_due_date_str,invoice_qty_str;
    TextView remaininginvoiceamounttxtvw,remainingqtytxtvw,sbmttext;
    EditText invoice_qty_edt,truck_num_edt,transport_name_edt,mobile_num_edt,invoice_num_edt;
    RelativeLayout sbmtrelative,cancelrelative;
    ArrayList<String> attachlist = new ArrayList<String>();
    ArrayList<String> imgattachlist = new ArrayList<String>();
    RelativeLayout viewdocrel1,viewdocrel2,viewdocrel3,viewdocrel4,viewdocrel5,viewdocrel6;
    ImageView backiv,remove_docs1_iv,remove_docs2_iv,remove_docs3_iv,remove_docs4_iv,remove_docs5_iv,remove_docs6_iv;
    TextView selectedfile1txtvw,selectedfile2txtvw,selectedfile3txtvw,selectedfile4txtvw,selectedfile5txtvw,selectedfile6txtvw;
    boolean adminAdded = false;
    JSONArray DispatchDetailsJsonArray;
    ImageView viewdociv1,viewdociv2,viewdociv3,viewdociv4,viewdociv5,viewdociv6,add_docs;
    JSONArray certificate_file_jsonarray;
    int p = 0;
    ScrollView scroll;
    private static final int PERMISSION_REQUEST_CODE = 200;
    String total_amount_sum_str,final_exceeded_qty_str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_add_dispatch);



        attachlist.clear();
        imgattachlist.clear();

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);


        backiv = (ImageView)findViewById(R.id.back_add_dispatch);
        scroll = (ScrollView) findViewById(R.id.scroll);

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SellerAddDispatchActivity.this,SellerDispatchActivity.class);
                SingletonActivity.clickedaddicon = "";
                startActivity(i);
            }
        });



        selectfiletxtvw = (TextView)findViewById(R.id.selectfiletxtvw);
        add_docs = (ImageView)findViewById(R.id.add_docs);

        selectedfile1txtvw = (TextView)findViewById(R.id.selectedfile1txtvw);
        selectedfile2txtvw = (TextView)findViewById(R.id.selectedfile2txtvw);
        selectedfile3txtvw = (TextView)findViewById(R.id.selectedfile3txtvw);
        selectedfile4txtvw = (TextView)findViewById(R.id.selectedfile4txtvw);
        selectedfile5txtvw = (TextView)findViewById(R.id.selectedfile5txtvw);
        selectedfile6txtvw = (TextView)findViewById(R.id.selectedfile6txtvw);

        viewdocrel1 = (RelativeLayout)findViewById(R.id.viewdocrel1);
        viewdocrel2= (RelativeLayout)findViewById(R.id.viewdocrel2);
        viewdocrel3= (RelativeLayout)findViewById(R.id.viewdocrel3);
        viewdocrel4= (RelativeLayout)findViewById(R.id.viewdocrel4);
        viewdocrel5= (RelativeLayout)findViewById(R.id.viewdocrel5);
        viewdocrel6= (RelativeLayout)findViewById(R.id.viewdocrel6);

        viewdociv1 = (ImageView)findViewById(R.id.viewdociv1);
        viewdociv2 = (ImageView)findViewById(R.id.viewdociv2);
        viewdociv3 = (ImageView)findViewById(R.id.viewdociv3);
        viewdociv4 = (ImageView)findViewById(R.id.viewdociv4);
        viewdociv5 = (ImageView)findViewById(R.id.viewdociv5);
        viewdociv6 = (ImageView)findViewById(R.id.viewdociv6);




        remove_docs1_iv = (ImageView)findViewById(R.id.remove_docs1_iv);
        remove_docs2_iv = (ImageView)findViewById(R.id.remove_docs2_iv);
        remove_docs3_iv = (ImageView)findViewById(R.id.remove_docs3_iv);
        remove_docs4_iv = (ImageView)findViewById(R.id.remove_docs4_iv);
        remove_docs5_iv = (ImageView)findViewById(R.id.remove_docs5_iv);
        remove_docs6_iv = (ImageView)findViewById(R.id.remove_docs6_iv);

        viewdocrel1.setVisibility(View.GONE);
        viewdocrel2.setVisibility(View.GONE);
        viewdocrel3.setVisibility(View.GONE);
        viewdocrel4.setVisibility(View.GONE);
        viewdocrel5.setVisibility(View.GONE);
        viewdocrel6.setVisibility(View.GONE);


        remove_docs1_iv.setVisibility(View.GONE);
        remove_docs2_iv.setVisibility(View.GONE);
        remove_docs3_iv.setVisibility(View.GONE);
        remove_docs4_iv.setVisibility(View.GONE);
        remove_docs5_iv.setVisibility(View.GONE);
        remove_docs6_iv.setVisibility(View.GONE);





        //=========


            dispatch_date_edt = (EditText)findViewById(R.id.dispatch_date_edt);
            truck_num_edt = (EditText)findViewById(R.id.truck_num_edt);
            transport_name_edt = (EditText)findViewById(R.id.transport_name_edt);
             mobile_num_edt = (EditText)findViewById(R.id.mobile_num_edt);
             invoice_num_edt = (EditText)findViewById(R.id.invoice_num_edt);
            invoice_due_date_edt = (EditText)findViewById(R.id.invoice_due_date_edt);
            invoice_qty_edt = (EditText)findViewById(R.id.invoice_qty_edt);
            invoice_amount_edt = (EditText)findViewById(R.id.invoice_amount_edt);
            remaininginvoiceamounttxtvw = (TextView)findViewById(R.id.remaininginvoiceamounttxtvw);
            remainingqtytxtvw = (TextView)findViewById(R.id.remainingqtytxtvw);
            //  final TextView addtxtvw = (TextView)dialogView.findViewById(R.id.adddispatchtxtvw);
            //  final TextView canceltxtvw = (TextView)dialogView.findViewById(R.id.canceldispatchtxtvw);
            sbmtrelative = (RelativeLayout)findViewById(R.id.sbmtrelative);
            cancelrelative = (RelativeLayout)findViewById(R.id.cancelrelative);
            sbmttext = (TextView)findViewById(R.id.sbmttext);



        scroll.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event){if (event != null && event.getAction() == MotionEvent.ACTION_MOVE)
            {
                InputMethodManager imm = ((InputMethodManager) SellerAddDispatchActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE));
                boolean isKeyboardUp = imm.isAcceptingText();

                if (isKeyboardUp)
                {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
                return false;}
        });

        if(NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)){
            String getCurrentDealsByTradeURL = APIName.URL+"/seller/getCurrentDealsByTrade?user_code="+userCodestr+"&tradeId="+SingletonActivity.tradeid;
            System.out.println("GET CURRENT DEALS BY TRADE URL IS---"+ getCurrentDealsByTradeURL);
            GetCurrentDealsByTradeAPI(getCurrentDealsByTradeURL);

        }
        else{
            util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
        }



        //==========
    }

    @Override
    public void onBackPressed() {


                Intent i = new Intent(SellerAddDispatchActivity.this,SellerDispatchActivity.class);
                SingletonActivity.clickedaddicon = "";
                startActivity(i);

    }

    public static class DatePickerFragment1 extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            System.out.println("SingletonActivity.sauda_patra_gen_date ======"+ SingletonActivity.sauda_patra_gen_date);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);


        //    String dateString = SingletonActivity.sauda_patra_gen_date;
            String dateString = dispatch_date_edt.getText().toString();
            DateFormat formatt = new SimpleDateFormat("dd/MM/yyyy");
            Date date = null;
            try {
                date = formatt.parse(dateString);


             //   System.out.println("Date in add payment=====:" +formatt.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            dialog.getDatePicker().setMinDate(date.getTime());



            //dialog.getDatePicker().setMaxDate(c.getTimeInMillis());

           /* if(dispatch_date_edt.getText().toString().length()==0) {

            //    Toast.makeText(getContext(), "IN IF OF DUE DATE===", Toast.LENGTH_LONG).show();
                String dateString = dispatch_date_edt.getText().toString();
                DateFormat formatt = new SimpleDateFormat("dd/MM/yyyy");
                Date date = null;
                try {
                    date = formatt.parse(dateString);



                  //  dialog.getDatePicker().setMinDate(date.getTime());
                   // dialog.getDatePicker().setMinDate(System.currentTimeMillis());

                    String dateStringsp = SingletonActivity.sauda_patra_gen_date;
                    Log.e("SPTRADE",dateStringsp);
                    DateFormat formattt = new SimpleDateFormat("dd-MM-yyyy");
                    Date datee = null;
                    try {
                        datee = formattt.parse(dateStringsp);



                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Date in add payment=====:" + datee);
                    dialog.getDatePicker().setMinDate(datee.getTime());
                //    dialog.getDatePicker().setMaxDate(c.getTimeInMillis());



                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            else
            {
               // Toast.makeText(getContext(), "IN ELSE OF DUE DATE===", Toast.LENGTH_LONG).show();
            }

*/
            return dialog;


        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user

            String daynew ="";
            String monthnew ="";

            if(day<10)
            {
                daynew = "0"+Integer.toString(day);
            }
            else
            {
                daynew = Integer.toString(day);
            }

            if(month+1<10)
            {
                monthnew = "0"+ Integer.toString(month+1);
            }
            else
            {
                monthnew = Integer.toString(month+1);
            }


            invoice_due_date_edt.setText(daynew + "/"
                    + monthnew + "/" + year);




        }
    }

    private void AddDispatchAPI(String url) {
        pdia = new ProgressDialog(SellerAddDispatchActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SellerAddDispatchActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        System.out.println("RESPONSE OF ADD DISPATCH API IS---" + response);
                        //Toast.makeText(SellerDispatchActivity.this,response,Toast.LENGTH_LONG).show();

                        JSONObject AddDispatchJson = null;

                        try {
                            AddDispatchJson = new JSONObject(response);
                            String statusstr = AddDispatchJson.getString("status");


                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Toast.makeText(SellerAddDispatchActivity.this,AddDispatchJson.getString("message"),Toast.LENGTH_LONG).show();


                                Intent i = new Intent(SellerAddDispatchActivity.this,SellerDispatchActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);



                            }
                            else
                            {
                                Toast.makeText(SellerAddDispatchActivity.this,AddDispatchJson.getString("message"),Toast.LENGTH_LONG).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_LONG).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(SellerAddDispatchActivity.this, messagestr, Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SellerAddDispatchActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                        else
                        {
                            util.dialog(SellerAddDispatchActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dispatch_date",dispatch_date_str);
                params.put("truck_number",truck_num_str);
                params.put("transport_name",transport_name_str);
                params.put("dispatch_mobile",mobile_num_str);
                params.put("invoice_number",invoice_num_str);
                params.put("invoice_due_date",invoice_due_date_str);
                params.put("invoice_qty",invoice_qty_str);
                params.put("invoice_amount",invoice_amount_edt.getText().toString());
                params.put("sell_id",SingletonActivity.sellid);
                params.put("mobile_num",mobilenumstr);

                if(attachmentstr==null)
                {
                    attachmentstr = "0";
                    params.put("dispatch_file",attachmentstr);
                    //  params.put("extension","");
                }
                else
                {
                    params.put("dispatch_file",attachmentstr);
                    //   params.put("extension",type);
                }





                System.out.println("add dispatch params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void deleteDispatchFileAPI(String url) {
        pdia = new ProgressDialog(SellerAddDispatchActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SellerAddDispatchActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();


                        System.out.println("RESPONSE OF SELLER DELETE DISPATCH FILE API IS---" + response);



                        JSONObject SellerDeleteDispatchJson = null;
                        try {
                            SellerDeleteDispatchJson = new JSONObject(response);


                            String statusstr = SellerDeleteDispatchJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                String messagestr = SellerDeleteDispatchJson.getString("message");


                                Toast.makeText(SellerAddDispatchActivity.this,messagestr,Toast.LENGTH_LONG).show();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(SellerAddDispatchActivity.this,SellerAddDispatchActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(i);


                                    }
                                }, 1000);


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia.dismiss();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_LONG).show();
                         pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SellerAddDispatchActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                        else
                        {
                            util.dialog(SellerAddDispatchActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/



                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(SellerAddDispatchActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void GetCurrentDealsByTradeAPI(String url) {

      /*  pdia = new ProgressDialog(SellerAddDispatchActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();
*/

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SellerAddDispatchActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                       // pdia.cancel();


                        System.out.println("RESPONSE OF GET CURRENT DEALS BY TRADE API IS---" + response);



                        JSONObject SellerGetCurrentDealsByTradeJson = null;
                        try {
                            SellerGetCurrentDealsByTradeJson = new JSONObject(response);


                            String statusstr = SellerGetCurrentDealsByTradeJson.getString("status");
                            System.out.println("STATUS OF GET CURRENT DEALS BY TRADE API IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {



                                SellerCurrentDealsByTradeJsonArray = SellerGetCurrentDealsByTradeJson.getJSONArray("cd_Offer");
                                SingletonActivity.sauda_patra_gen_date = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("sauda_patra_gen_date");

                                dispatchremainingpaystr = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("dispatch_remain_pay");
                                dispatchremainingqtystr = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("dispatch_remain_quan");
                                rate_str = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("rate");
                                quoted_rate_str = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("quoted_rate");
                                israte = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("israte");

                                final_exceeded_qty_str = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("final_exceed_quantity");

                                if (!israte.equalsIgnoreCase("1")) {
                                    if (!quoted_rate_str.equalsIgnoreCase("0.00")){


                                        double price = Double.parseDouble(rate_str) - Double.parseDouble(quoted_rate_str);

                                        rate_str = Double.toString(price);

                                    }else{



                                        rate_str = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("rate");
                                    }
                                }else{


                                    rate_str = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("rate");
                                }


                                gst_str = SingletonActivity.gst_str;

                                //Toast.makeText(SellerAddDispatchActivity.this,"Rate & GST "+ rate_str + "&" + gst_str,Toast.LENGTH_LONG).show();

                                System.out.println("DISPATCH REMAINING QTY=="+dispatchremainingqtystr);
                                System.out.println("DISPATCH REMAINING PAY=="+dispatchremainingpaystr);

                                int pos = SingletonActivity.checkboxclickedposition;


                              //
                               // Toast.makeText(SellerAddDispatchActivity.this, "Position Clicked==="+ DispatchDetailsJsonArray, Toast.LENGTH_LONG).show();
                                String dispatchdetails = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("dispatch_details");
                             //   Toast.makeText(SellerAddDispatchActivity.this,"dispatchdetails length: "+ dispatchdetails.length(),Toast.LENGTH_LONG).show();
                                if(dispatchdetails.length()>5)
                                {

                                    DispatchDetailsJsonArray = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("dispatch_details");

                                    if (DispatchDetailsJsonArray.getJSONObject(pos).getString("action").equalsIgnoreCase("1")) {
                                        adminAdded = true;

                                        dispatch_date_edt.setEnabled(false);
                                        truck_num_edt.setEnabled(false);
                                        transport_name_edt.setEnabled(false);
                                        mobile_num_edt.setEnabled(false);

                                        dispatch_date_edt.setBackgroundColor(Color.parseColor("#e4e4e4"));
                                        truck_num_edt.setBackgroundColor(Color.parseColor("#e4e4e4"));
                                        transport_name_edt.setBackgroundColor(Color.parseColor("#e4e4e4"));
                                        mobile_num_edt.setBackgroundColor(Color.parseColor("#e4e4e4"));

                                        dispatch_date_edt.getBackground().setColorFilter(getResources().getColor(R.color.radio_button_unselected_color),
                                                PorterDuff.Mode.SRC_ATOP);
                                        truck_num_edt.getBackground().setColorFilter(getResources().getColor(R.color.radio_button_unselected_color),
                                                PorterDuff.Mode.SRC_ATOP);
                                        transport_name_edt.getBackground().setColorFilter(getResources().getColor(R.color.radio_button_unselected_color),
                                                PorterDuff.Mode.SRC_ATOP);
                                        mobile_num_edt.getBackground().setColorFilter(getResources().getColor(R.color.radio_button_unselected_color),
                                                PorterDuff.Mode.SRC_ATOP);

                                        dispatch_date_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("dispatch_date"));
                                        truck_num_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("truck_number"));
                                        transport_name_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("transport_name"));
                                        mobile_num_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("dispatch_mobile"));


                                        if(DispatchDetailsJsonArray.getJSONObject(pos).getString("invoice_number").length()==4&&(DispatchDetailsJsonArray.getJSONObject(pos).getString("invoice_number").equalsIgnoreCase("null"))) {
                                            invoice_num_edt.setText("");
                                        }
                                        else
                                        {
                                            invoice_num_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("invoice_number"));
                                        }
                                        invoice_due_date_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("invoice_due_date"));


                                        certificate_file_jsonarray = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("certificate_file");

                                        System.out.println("certificate_file_jsonarray length==" + certificate_file_jsonarray.length());


                                        if (certificate_file_jsonarray.length() == 1) {
                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));

                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    //     Toast.makeText(SellerAddDispatchActivity.this,"IN T1b",Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }


                                            }

                                        }


                                        if (certificate_file_jsonarray.length() == 2) {

                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));


                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });

                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    //       Toast.makeText(SellerAddDispatchActivity.this,"IN T1b",Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }


                                            }

                                            if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                                    viewdocrel2.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));


                                                    remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });

                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile2txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv2.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv2.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }

                                            }

                                        }

                                        if (certificate_file_jsonarray.length() == 3) {
                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));

                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });

                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    //    Toast.makeText(SellerAddDispatchActivity.this,"IN T1b",Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }

                                            }

                                            if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel2.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });

                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile2txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv2.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv2.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }


                                            }

                                            if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel3.setVisibility(View.VISIBLE);
                                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs3_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });

                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile3txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv3.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv3.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }
                                            }

                                        }
                                        if (certificate_file_jsonarray.length() == 4) {

                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {
                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    //   Toast.makeText(SellerAddDispatchActivity.this,"IN T1b",Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }
                                            }

                                            if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                                    viewdocrel2.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile2txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv2.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv2.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }
                                            }

                                            if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel3.setVisibility(View.VISIBLE);
                                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs3_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile3txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv3.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv3.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }


                                            }

                                            if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel4.setVisibility(View.VISIBLE);
                                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs4_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile4txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv4.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv4.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }


                                            }
                                        }

                                        if (certificate_file_jsonarray.length() == 5) {


                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    // Toast.makeText(SellerAddDispatchActivity.this,"IN T1b",Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }
                                            }

                                            if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                                    viewdocrel2.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile2txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv2.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv2.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }


                                            }

                                            if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel3.setVisibility(View.VISIBLE);
                                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs3_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile3txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv3.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv3.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }
                                            }

                                            if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel4.setVisibility(View.VISIBLE);
                                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs4_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile4txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv4.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv4.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }
                                            }

                                            if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file").length() > 0) {
                                                if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                                    viewdocrel5.setVisibility(View.VISIBLE);
                                                    remove_docs5_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs5_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile5txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv5.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv5.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }
                                            }

                                        }
                                        if (certificate_file_jsonarray.length() == 6)

                                        {


                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    //   Toast.makeText(SellerAddDispatchActivity.this, "IN T1b", Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }


                                            }

                                            if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel2.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile2txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv2.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv2.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }

                                            }

                                            if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {
                                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                                    viewdocrel3.setVisibility(View.VISIBLE);
                                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs3_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile3txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv3.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv3.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }
                                            }

                                            if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel4.setVisibility(View.VISIBLE);
                                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs4_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile4txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv4.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv4.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }

                                            }

                                            if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel5.setVisibility(View.VISIBLE);
                                                    remove_docs5_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs5_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile5txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv5.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv5.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }


                                            }

                                            if (certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file").length() > 0) {
                                                if (certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                                    viewdocrel6.setVisibility(View.VISIBLE);
                                                    remove_docs6_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs6_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile6txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //     Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv6.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv6.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }

                                            }
                                        }

                                        if(selectedfile1txtvw.getText().toString().length()>0 && selectedfile2txtvw.getText().toString().length()>0 && selectedfile3txtvw.getText().toString().length()>0 && selectedfile4txtvw.getText().toString().length()>0 && selectedfile5txtvw.getText().toString().length()>0 && selectedfile6txtvw.getText().toString().length()>0)
                                        {
                                            selectfiletxtvw.setVisibility(View.GONE);
                                            add_docs.setVisibility(View.GONE);
                                        }
                                        else
                                        {
                                            selectfiletxtvw.setVisibility(View.VISIBLE);
                                            add_docs.setVisibility(View.VISIBLE);
                                        }


                                    }

                                    else
                                    {
                                        adminAdded = false;

                                        dispatch_date_edt.setEnabled(true);
                                        truck_num_edt.setEnabled(true);
                                        transport_name_edt.setEnabled(true);
                                        mobile_num_edt.setEnabled(true);

                                        /*dispatch_date_edt.setBackgroundColor(Color.parseColor("#e4e4e4"));
                                        truck_num_edt.setBackgroundColor(Color.parseColor("#e4e4e4"));
                                        transport_name_edt.setBackgroundColor(Color.parseColor("#e4e4e4"));
                                        mobile_num_edt.setBackgroundColor(Color.parseColor("#e4e4e4"));
*/
                                        /*dispatch_date_edt.getBackground().setColorFilter(getResources().getColor(R.color.radio_button_unselected_color),
                                                PorterDuff.Mode.SRC_ATOP);
                                        truck_num_edt.getBackground().setColorFilter(getResources().getColor(R.color.radio_button_unselected_color),
                                                PorterDuff.Mode.SRC_ATOP);
                                        transport_name_edt.getBackground().setColorFilter(getResources().getColor(R.color.radio_button_unselected_color),
                                                PorterDuff.Mode.SRC_ATOP);
                                        mobile_num_edt.getBackground().setColorFilter(getResources().getColor(R.color.radio_button_unselected_color),
                                                PorterDuff.Mode.SRC_ATOP);*/

                                        dispatch_date_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("dispatch_date"));
                                        truck_num_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("truck_number"));
                                        transport_name_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("transport_name"));
                                        mobile_num_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("dispatch_mobile"));


                                        if(DispatchDetailsJsonArray.getJSONObject(pos).getString("invoice_number").length()==4&&(DispatchDetailsJsonArray.getJSONObject(pos).getString("invoice_number").equalsIgnoreCase("null"))) {
                                            invoice_num_edt.setText("");
                                        }
                                        else
                                        {
                                            invoice_num_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("invoice_number"));
                                        }


                                        invoice_due_date_edt.setText(DispatchDetailsJsonArray.getJSONObject(pos).getString("invoice_due_date"));

                                        certificate_file_jsonarray = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("certificate_file");

                                        System.out.println("certificate_file_jsonarray length==" + certificate_file_jsonarray.length());


                                        if (certificate_file_jsonarray.length() == 1) {


                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));

                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here

                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);

                                                                //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    //     Toast.makeText(SellerAddDispatchActivity.this,"IN T1b",Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }




                                            }

                                        }


                                        if (certificate_file_jsonarray.length() == 2) {

                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));


                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });

                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here

                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);

                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    //       Toast.makeText(SellerAddDispatchActivity.this,"IN T1b",Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }


                                            }

                                            if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel2.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));


                                                    remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });

                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile2txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv2.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv2.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }



                                            }

                                        }

                                        if (certificate_file_jsonarray.length() == 3) {
                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));

                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });

                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    //    Toast.makeText(SellerAddDispatchActivity.this,"IN T1b",Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }





                                            }

                                            if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel2.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });

                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile2txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv2.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv2.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }



                                            }

                                            if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel3.setVisibility(View.VISIBLE);
                                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs3_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });

                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile3txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv3.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv3.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }


                                            }

                                        }
                                        if (certificate_file_jsonarray.length() == 4) {

                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    //   Toast.makeText(SellerAddDispatchActivity.this,"IN T1b",Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }




                                            }

                                            if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel2.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile2txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv2.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv2.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }




                                            }

                                            if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel3.setVisibility(View.VISIBLE);
                                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs3_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile3txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv3.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv3.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }




                                            }

                                            if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                                    viewdocrel4.setVisibility(View.VISIBLE);
                                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs4_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile4txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv4.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv4.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }



                                            }
                                        }

                                        if (certificate_file_jsonarray.length() == 5) {


                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    // Toast.makeText(SellerAddDispatchActivity.this,"IN T1b",Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }



                                            }

                                            if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel2.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile2txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv2.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv2.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }


                                            }



                                            if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel3.setVisibility(View.VISIBLE);
                                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs3_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile3txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv3.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv3.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }






                                            }

                                            if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel4.setVisibility(View.VISIBLE);
                                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs4_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile4txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv4.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv4.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }


                                            }

                                            if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel5.setVisibility(View.VISIBLE);
                                                    remove_docs5_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs5_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile5txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv5.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv5.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }



                                            }

                                        }
                                        if (certificate_file_jsonarray.length() == 6)

                                        {


                                            if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel1.setVisibility(View.VISIBLE);
                                                    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile1txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {
                                                                    viewdociv1.setBackgroundResource(R.mipmap.reciept);


                                                                } else {
                                                                    //   Toast.makeText(SellerAddDispatchActivity.this, "IN T1b", Toast.LENGTH_LONG).show();

                                                                    System.out.println("IN T1==b");
                                                                    viewdociv1.setImageBitmap(bmp);

                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }



                                            }

                                            if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                                    viewdocrel2.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile2txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv2.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv2.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }



                                            }

                                            if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel3.setVisibility(View.VISIBLE);
                                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs3_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile3txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv3.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv3.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }


                                            }

                                            if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel4.setVisibility(View.VISIBLE);
                                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs4_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile4txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv4.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv4.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }



                                            }

                                            if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel5.setVisibility(View.VISIBLE);
                                                    remove_docs5_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs5_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile5txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv5.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv5.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();

                                                }


                                            }

                                            if (certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file").length() > 0) {

                                                if (certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                                    viewdocrel6.setVisibility(View.VISIBLE);
                                                    remove_docs6_iv.setVisibility(View.VISIBLE);
                                                    //selectedfile1txtvw.setText(imgattachlist.get(0));
                                                    remove_docs6_iv.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {

                                                            if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                                String getCurrentDealsByTradeURL = null;
                                                                try {
                                                                    String deleteDispatchFileURL = APIName.URL + "/seller/deleteDispatchFile?file_id=" + certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_id");
                                                                    System.out.println("DELETE DISPATCH URL IS---" + deleteDispatchFileURL);
                                                                    deleteDispatchFileAPI(deleteDispatchFileURL);


                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                }

                                                            } else {
                                                                util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                            }
                                                        }
                                                    });


                                                    final String urlstr = certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file");
                                                    String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());
                                                    selectedfile6txtvw.setText(fileName);
                                                    final URL url = new URL(urlstr);

                                                    Thread thread = new Thread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            try {
                                                                //Your code goes here
                                                                BitmapFactory.Options options = new BitmapFactory.Options();
                                                                options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                System.out.println("EXTENSION IS===" + extension);
                                                                if (extension.equalsIgnoreCase(".pdf")) {

                                                                    viewdociv6.setBackgroundResource(R.mipmap.reciept);


                                                                } else {


                                                                    viewdociv6.setImageBitmap(bmp);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    });

                                                    thread.start();
                                                }


                                            }
                                        }


                                    }


                                }

                                if(SingletonActivity.clickedaddicon == "click")
                                {
                                    dispatch_date_edt.setText("");
                                    truck_num_edt.setText("");
                                    transport_name_edt.setText("");
                                    mobile_num_edt.setText("");
                                    invoice_num_edt.setText("");
                                    invoice_due_date_edt.setText("");
                                    //invoice_qty_edt.setText("");
                                    //  invoice_amount_edt.setText("");
                                    viewdocrel1.setVisibility(View.GONE);
                                    viewdocrel2.setVisibility(View.GONE);
                                    viewdocrel3.setVisibility(View.GONE);
                                    viewdocrel4.setVisibility(View.GONE);
                                    viewdocrel5.setVisibility(View.GONE);
                                    viewdocrel6.setVisibility(View.GONE);

                                    remove_docs1_iv.setVisibility(View.GONE);
                                    remove_docs2_iv.setVisibility(View.GONE);
                                    remove_docs3_iv.setVisibility(View.GONE);
                                    remove_docs4_iv.setVisibility(View.GONE);
                                    remove_docs5_iv.setVisibility(View.GONE);
                                    remove_docs6_iv.setVisibility(View.GONE);


                                }






                                //  invoice_qty_edt.setText(dispatchremainingqtystr+ "0");
                             //   invoice_amount_edt.setText(dispatchremainingpaystr);

                                if(SingletonActivity.clickedaddicon == "click")
                                {
                                  //  DecimalFormat df = new DecimalFormat("0.00");

                                  //  DecimalFormat df1 = new DecimalFormat("0.000");


                              //     invoice_qty_edt.setText(df.format(dispatchremainingqtystr));
                                //    invoice_amount_edt.setText(df1.format(dispatchremainingpaystr));

                                     invoice_qty_edt.setText(dispatchremainingqtystr+ "0");
                                     invoice_amount_edt.setText(dispatchremainingpaystr);

                                    total_amount_sum_str = dispatchremainingpaystr;

                                      remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: "+ dispatchremainingpaystr);
                                      remainingqtytxtvw.setText("Remaining Quantity: "+ dispatchremainingqtystr +"0");

                                    DecimalFormat dfq = new DecimalFormat("0.000");
                                    DecimalFormat dfa = new DecimalFormat("0.00");

                                    // Toast.makeText(SellerAddDispatchActivity.this,"EXCEEDED: "+ Double.parseDouble(final_exceeded_qty_str),Toast.LENGTH_SHORT).show();

                                    if(Double.parseDouble(final_exceeded_qty_str)>0)
                                    {
                                        remainingqtytxtvw.setText("Exceeded Quantity: "+dfq.format(Double.parseDouble(final_exceeded_qty_str)));
                                        double final_rate = Double.parseDouble(rate_str);
                                        double dispatchremainingpaystr_double_val = Double.parseDouble(dispatchremainingpaystr);
                                        double exceed_qty_val = Double.parseDouble(final_exceeded_qty_str);
                                        double gst_val = Double.parseDouble(gst_str);
                                        // double rate_pmt_val = Double.parseDouble(rate_str);


                                        double exceeded_gst_rate_val = (final_rate * exceed_qty_val * gst_val)/100;

                                        double final_amount_exceed = (final_rate* exceed_qty_val)+ exceeded_gst_rate_val ;

                                        DecimalFormat df2 = new DecimalFormat("0.00");

                                        remaininginvoiceamounttxtvw.setText("Exceeded Invoice Amount: " + dfa.format(final_amount_exceed));

                                       // Toast.makeText(SellerAddDispatchActivity.this,"Exceeded Invoice Amount: ss"+ df2.format(final_amount_exceed) ,Toast.LENGTH_SHORT).show();
                                        invoice_amount_edt.setText("0.00");
                                        invoice_qty_edt.setText("0.000");




                                    }



                                    if ((invoice_qty_edt.getText().toString().equalsIgnoreCase("0.000"))) {
                                        invoice_qty_edt.setEnabled(false);
                                        invoice_qty_edt.setTextColor(Color.parseColor("#696969"));

                                        sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                        sbmttext.setTextColor(Color.parseColor("#808080"));
                                        sbmtrelative.setEnabled(false);


                                    } else {
                                        invoice_qty_edt.setEnabled(true);
                                        invoice_qty_edt.setTextColor(Color.parseColor("#000000"));

                                        sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                        sbmttext.setTextColor(Color.WHITE);
                                        sbmtrelative.setEnabled(true);
                                    }

                                    if (invoice_amount_edt.getText().toString().equalsIgnoreCase("0.00")) {
                                        invoice_amount_edt.setEnabled(false);
                                        invoice_amount_edt.setTextColor(Color.parseColor("#696969"));

                                        sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                        sbmttext.setTextColor(Color.parseColor("#808080"));
                                        sbmtrelative.setEnabled(false);

                                    } else {
                                        invoice_amount_edt.setEnabled(true);
                                        invoice_amount_edt.setTextColor(Color.parseColor("#000000"));

                                        sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                        sbmttext.setTextColor(Color.WHITE);
                                        sbmtrelative.setEnabled(true);
                                    }




                                }

                                else {

                                    sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                    sbmttext.setTextColor(Color.WHITE);
                                    sbmtrelative.setEnabled(true);


                                                    // Toast.makeText(SellerDispatchActivity.this,"INDEX MATCHED=="+ i + "&" + j,Toast.LENGTH_SHORT).show();
                                                    if (certificate_file_jsonarray.length() == 1) {

                                                        for (int i = 0; i < certificate_file_jsonarray.length(); i++) {

                                                            for (int j = 0; j < DispatchDetailsJsonArray.length(); j++) {


                                                                if (certificate_file_jsonarray.getJSONObject(i).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(j).getString("auto_dispatch_id"))) {

                                                                    {

                                                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                                            viewdociv1.setVisibility(View.VISIBLE);


                                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                                            final URL url = new URL(urlstr);

                                                                            Thread thread = new Thread(new Runnable() {

                                                                                @Override
                                                                                public void run() {
                                                                                    try {
                                                                                        //Your code goes here
                                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                                        // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                                            viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                                            viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                                            viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                                                        } else {


                                                                                            viewdociv1.setImageBitmap(bmp);


                                                                                        }


                                                                                    } catch (Exception e) {
                                                                                        e.printStackTrace();
                                                                                    }
                                                                                }
                                                                            });

                                                                            thread.start();


                                                                        }

                                                                    }
                                                                }
                                                            }
                                                        }



                                                    if (certificate_file_jsonarray.length() == 2) {

                                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv1.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                                        } else {
                                                                            System.out.println("IN T1==b");
                                                                            viewdociv1.setImageBitmap(bmp);

                                                                        }


                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv2.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv2.setImageBitmap(bmp);
                                                                        }


                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                    }

                                                    if (certificate_file_jsonarray.length() == 3) {
                                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv1.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv1.setImageBitmap(bmp);

                                                                        }


                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv2.setVisibility(View.VISIBLE);

                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here

                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);

                                                                        // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.pdf);

                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv2.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv3.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.pdf);

                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv3.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                    }
                                                    if (certificate_file_jsonarray.length() == 4) {

                                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv1.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv1.setImageBitmap(bmp);

                                                                        }


                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv2.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv2.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv3.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv3.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv4.setVisibility(View.VISIBLE);

                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv4.setBackgroundResource(R.mipmap.pdf);

                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv4.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv4.setBackgroundResource(R.mipmap.docx);
                                                                        } else {
                                                                            viewdociv4.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }
                                                    }

                                                    if (certificate_file_jsonarray.length() == 5) {


                                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv1.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv1.setImageBitmap(bmp);

                                                                        }


                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv2.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv2.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv3.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv3.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv4.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv4.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv4.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv4.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv4.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv5.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv5.setBackgroundResource(R.mipmap.pdf);

                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv5.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv5.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv5.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                    }
                                                    if (certificate_file_jsonarray.length() == 6)

                                                    {


                                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv1.setVisibility(View.VISIBLE);

                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            System.out.println("IN T1==b");
                                                                            viewdociv1.setImageBitmap(bmp);

                                                                        }


                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv2.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv2.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv3.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv3.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv4.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv4.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv4.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv4.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv4.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv5.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv5.setBackgroundResource(R.mipmap.pdf);
                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv5.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv5.setBackgroundResource(R.mipmap.docx);
                                                                        } else {


                                                                            viewdociv5.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }

                                                        if (certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file").length() > 0) {

                                                            viewdociv6.setVisibility(View.VISIBLE);


                                                            final String urlstr = certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file");
                                                            String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                                            final URL url = new URL(urlstr);

                                                            Thread thread = new Thread(new Runnable() {

                                                                @Override
                                                                public void run() {
                                                                    try {
                                                                        //Your code goes here
                                                                        BitmapFactory.Options options = new BitmapFactory.Options();
                                                                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                                        Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                                        //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                                        String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                                        System.out.println("EXTENSION IS===" + extension);
                                                                        if (extension.equalsIgnoreCase(".pdf")) {
                                                                            viewdociv6.setBackgroundResource(R.mipmap.pdf);

                                                                        } else if (extension.equalsIgnoreCase(".doc")) {
                                                                            viewdociv6.setBackgroundResource(R.mipmap.doc2);
                                                                        } else if (extension.equalsIgnoreCase(".docx")) {
                                                                            viewdociv6.setBackgroundResource(R.mipmap.docx);
                                                                        } else {
                                                                            viewdociv6.setImageBitmap(bmp);
                                                                        }

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            });

                                                            thread.start();


                                                        }
                                                    }

                                                }





                                    invoice_qty_edt.setText(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("invoice_qty"));
                                    invoice_amount_edt.setText(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("invoice_amount"));

                             /*   float invoice_amount_float_val = Float.parseFloat(dispatchremainingpaystr);
                                float invoice_quantity_latest_val = Float.parseFloat(dispatchremainingqtystr);

                                float invoice_qty_float_latest_val = Float.parseFloat(invoice_qty_edt.getText().toString());
                                float invoice_amount_float_latest_val = Float.parseFloat(invoice_amount_edt.getText().toString());


                                float total_amount_sum = invoice_amount_float_val + invoice_amount_float_latest_val;
                                float total_quantity_sum = invoice_quantity_latest_val + invoice_qty_float_latest_val;

                              //  remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: "+ dispatchremainingpaystr);
                              //    remainingqtytxtvw.setText("Remaining Quantity: "+ dispatchremainingqtystr +"0");

                                remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: "+ Float.toString(total_amount_sum));
                                remainingqtytxtvw.setText("Remaining Quantity: "+ Float.toString(total_quantity_sum));
*/

                                    double invoice_amount_float_val = Double.parseDouble(dispatchremainingpaystr);
                                    double invoice_quantity_latest_val = Double.parseDouble(dispatchremainingqtystr);

                                    double invoice_qty_float_latest_val = Double.parseDouble(invoice_qty_edt.getText().toString());
                                    double invoice_amount_float_latest_val = Double.parseDouble(invoice_amount_edt.getText().toString());


                                    double total_amount_sum = invoice_amount_float_val + invoice_amount_float_latest_val;
                                    double total_quantity_sum = invoice_quantity_latest_val + invoice_qty_float_latest_val;

                                    //  remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: "+ dispatchremainingpaystr);
                                    //    remainingqtytxtvw.setText("Remaining Quantity: "+ dispatchremainingqtystr +"0");


                                    DecimalFormat df = new DecimalFormat("0.00");

                                    DecimalFormat df1 = new DecimalFormat("0.000");

                                    total_amount_sum_str = df.format(total_amount_sum);


                                 //   remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: " + df.format(total_amount_sum));
                                 //   remainingqtytxtvw.setText("Remaining Quantity: " + df1.format(total_quantity_sum));



                                    remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: " +dispatchremainingpaystr);
                                    remainingqtytxtvw.setText("Remaining Quantity: " + dispatchremainingqtystr);






                                    double sum = invoice_qty_float_latest_val + invoice_amount_float_latest_val;

                                    if (sum == 0) {
                                        // addtxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                                        // addtxtvw.setEnabled(false);

                                        sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                        sbmttext.setTextColor(Color.parseColor("#808080"));
                                        sbmtrelative.setEnabled(false);

                                        remove_docs1_iv.setVisibility(View.GONE);
                                        remove_docs2_iv.setVisibility(View.GONE);
                                        remove_docs3_iv.setVisibility(View.GONE);
                                        remove_docs4_iv.setVisibility(View.GONE);
                                        remove_docs5_iv.setVisibility(View.GONE);
                                        remove_docs6_iv.setVisibility(View.GONE);


                                    } else {
                                        //  addtxtvw.setTextColor(Color.parseColor("#000000"));
                                        //  addtxtvw.setEnabled(true);

                                        sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                        sbmttext.setTextColor(Color.WHITE);
                                        sbmtrelative.setEnabled(true);

                                    /*remove_docs1_iv.setVisibility(View.VISIBLE);
                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                    remove_docs5_iv.setVisibility(View.VISIBLE);
                                    remove_docs6_iv.setVisibility(View.VISIBLE);*/
                                    }

                                    DecimalFormat dfq = new DecimalFormat("0.000");
                                    DecimalFormat dfa = new DecimalFormat("0.00");

                                    // Toast.makeText(SellerAddDispatchActivity.this,"EXCEEDED: "+ Double.parseDouble(final_exceeded_qty_str),Toast.LENGTH_SHORT).show();

                                    if(Double.parseDouble(final_exceeded_qty_str)>0)
                                    {
                                        remainingqtytxtvw.setText("Exceeded Quantity: "+dfq.format(Double.parseDouble(final_exceeded_qty_str)));
                                        double final_rate = Double.parseDouble(rate_str);
                                        double dispatchremainingpaystr_double_val = Double.parseDouble(dispatchremainingpaystr);
                                        double exceed_qty_val = Double.parseDouble(final_exceeded_qty_str);
                                        double gst_val = Double.parseDouble(gst_str);
                                        // double rate_pmt_val = Double.parseDouble(rate_str);


                                        double exceeded_gst_rate_val = (final_rate * exceed_qty_val * gst_val)/100;

                                        double final_amount_exceed = (final_rate* exceed_qty_val)+ exceeded_gst_rate_val ;

                                        // DecimalFormat df2 = new DecimalFormat("0.00");


                                        remaininginvoiceamounttxtvw.setText("Exceeded Invoice Amount: " + dfa.format(final_amount_exceed));

                                        // Toast.makeText(SellerAddDispatchActivity.this,"Exceeded Invoice Amount: ss"+ df2.format(final_amount_exceed) ,Toast.LENGTH_SHORT).show();
                                        invoice_amount_edt.setText("0.00");
                                        invoice_qty_edt.setText("0.000");

                                        invoice_amount_edt.setEnabled(false);
                                        invoice_qty_edt.setEnabled(false);

                                        sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                        sbmttext.setTextColor(Color.parseColor("#808080"));
                                        sbmtrelative.setEnabled(false);


                                    }
                                }

                                invoice_qty_edt.addTextChangedListener(new TextWatcher() {

                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {



                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                                        if (s.length() > 0) {



                                                // if ((Float.parseFloat(invoice_qty_edt.getText().toString()) > dispatchremainingqty_float_val)) {

                                         //   if(SingletonActivity.clickedaddicon == "click") {

                                                float invoice_qty_float_val = Float.parseFloat(invoice_qty_edt.getText().toString());
                                                float dispatchremainingqty_float_val = Float.parseFloat(dispatchremainingqtystr);
                                                float exceededqty_float_val = 5.0f;
                                                float new_dispatchremainingqty_float_val = dispatchremainingqty_float_val + exceededqty_float_val;

                                                if(SingletonActivity.clickedaddicon == "")
                                                {
                                                    try {
                                                        if (Float.parseFloat(invoice_qty_edt.getText().toString()) > new_dispatchremainingqty_float_val + Float.parseFloat(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("invoice_qty"))) {
                                                            Toast.makeText(SellerAddDispatchActivity.this, "You have reached the maximum limit", Toast.LENGTH_LONG).show();
                                                            //  Toast.makeText(SellerAddDispatchActivity.this, "Quantity can't be greater than remaining quantity.", Toast.LENGTH_LONG).show();

                                                            //      addtxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                                                            //     addtxtvw.setEnabled(false);

                                                            remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: " + dispatchremainingpaystr);

                                                            remainingqtytxtvw.setText("Remaining Quantity: " + dispatchremainingqtystr + "0");

                                                            invoice_amount_edt.setText(dispatchremainingpaystr);

                                                            sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                                            sbmttext.setTextColor(Color.parseColor("#808080"));
                                                            sbmtrelative.setEnabled(false);

                                                            remove_docs1_iv.setVisibility(View.GONE);
                                                            remove_docs2_iv.setVisibility(View.GONE);
                                                            remove_docs3_iv.setVisibility(View.GONE);
                                                            remove_docs4_iv.setVisibility(View.GONE);
                                                            remove_docs5_iv.setVisibility(View.GONE);
                                                            remove_docs6_iv.setVisibility(View.GONE);
                                                        }
                                                        else
                                                        {

                                                            // Toast.makeText(getActivity(),"QUANTITY -",Toast.LENGTH_LONG).show();
                                                            //   addtxtvw.setTextColor(Color.parseColor("#000000"));
                                                            //    addtxtvw.setEnabled(true);

                                                            sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                                            sbmttext.setTextColor(Color.WHITE);
                                                            sbmtrelative.setEnabled(true);

                                                  /*  remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                                    remove_docs5_iv.setVisibility(View.VISIBLE);
                                                    remove_docs6_iv.setVisibility(View.VISIBLE);*/


                                                            if (invoice_qty_edt.getText().toString().length() > 0) {
                                                                float invoice_qty_float_latest_val = Float.parseFloat(invoice_qty_edt.getText().toString());

                                                                if (invoice_amount_edt.getText().toString().length() > 0) {


                                                                    //==========NEW ENHANCEMENT of quantity increase by 5============


                                                                    //invoice_qty_edt.setText(dispatchremainingqtystr+ "0");
                                                                    //  invoice_amount_edt.setText(dispatchremainingpaystr);


                                                          /*  DecimalFormat df = new DecimalFormat("0.00");

                                                            DecimalFormat df1 = new DecimalFormat("0.000");
                                                           */


                                                                    //   remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: "+ dispatchremainingpaystr);
                                                                    double invoice_qty_float_value = Double.parseDouble(invoice_qty_edt.getText().toString());
                                                                    double dispatchremainingqty_float_value = Double.parseDouble(dispatchremainingqtystr);

                                                                    double new_dispatchremainingqty_float_value = invoice_qty_float_value - (dispatchremainingqty_float_value + Float.parseFloat(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("invoice_qty")));


                                                                    //==========================================

                                                                    if (dispatchremainingqty_float_value + Float.parseFloat(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("invoice_qty")) < invoice_qty_float_value) {

                                                                        DecimalFormat df1 = new DecimalFormat("0.000");

                                                                        remainingqtytxtvw.setText("Exceeded Quantity: " + df1.format(new_dispatchremainingqty_float_value));

                                                                        double final_rate = Double.parseDouble(rate_str);
                                                                        double dispatchremainingpaystr_double_val = Double.parseDouble(dispatchremainingpaystr);
                                                                        double exceed_qty_val = new_dispatchremainingqty_float_value;
                                                                        double gst_val = Double.parseDouble(gst_str);
                                                                       // double rate_pmt_val = Double.parseDouble(rate_str);


                                                                        double exceeded_gst_rate_val = (final_rate * exceed_qty_val * gst_val)/100;

                                                                        double final_amount_exceed = (final_rate* exceed_qty_val)+ exceeded_gst_rate_val + dispatchremainingpaystr_double_val;

                                                                        DecimalFormat df2 = new DecimalFormat("0.00");


                                                                        remaininginvoiceamounttxtvw.setText("Exceeded Invoice Amount: " + df2.format(final_amount_exceed));
                                                                        invoice_amount_edt.setText(df2.format(final_amount_exceed));


                                                                    } else {
                                                                        remainingqtytxtvw.setText("Remaining Quantity: " + dispatchremainingqtystr + "0");
                                                                        remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: " + dispatchremainingpaystr);
                                                                        invoice_amount_edt.setText(dispatchremainingpaystr);



                                                                    }


                                                                    float invoice_amount_float_latest_val = Float.parseFloat(invoice_amount_edt.getText().toString());

                                                                    float sum = invoice_qty_float_latest_val + invoice_amount_float_latest_val;





                                                                    if (sum == 0) {
                                                                        //       addtxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                                                                        //       addtxtvw.setEnabled(false);

                                                                        sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                                                        sbmttext.setTextColor(Color.parseColor("#808080"));
                                                                        sbmtrelative.setEnabled(false);

                                                                        remove_docs1_iv.setVisibility(View.GONE);
                                                                        remove_docs2_iv.setVisibility(View.GONE);
                                                                        remove_docs3_iv.setVisibility(View.GONE);
                                                                        remove_docs4_iv.setVisibility(View.GONE);
                                                                        remove_docs5_iv.setVisibility(View.GONE);
                                                                        remove_docs6_iv.setVisibility(View.GONE);
                                                                    } else {
                                                                        //       addtxtvw.setTextColor(Color.parseColor("#000000"));
                                                                        //      addtxtvw.setEnabled(true);

                                                                        sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                                                        sbmttext.setTextColor(Color.WHITE);
                                                                        sbmtrelative.setEnabled(true);

                                                            /*    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                                                remove_docs4_iv.setVisibility(View.VISIBLE);
                                                                remove_docs5_iv.setVisibility(View.VISIBLE);
                                                                remove_docs6_iv.setVisibility(View.VISIBLE);
    */

                                                                    }


                                                                }


                                                            }


                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                                else {

                                                 //   Toast.makeText(SellerAddDispatchActivity.this,"EXCEEDED A: "+ Double.parseDouble(final_exceeded_qty_str),Toast.LENGTH_SHORT).show();


                                                        if (Float.parseFloat(invoice_qty_edt.getText().toString()) > new_dispatchremainingqty_float_val) {

                                                            //      Toast.makeText(getActivity(),"Quantity to be increased should not be less than the difference of Buyer Requested Quantity and Currently Remaining Quantity",Toast.LENGTH_LONG).show();
                                                            Toast.makeText(SellerAddDispatchActivity.this, "You have reached the maximum limit", Toast.LENGTH_LONG).show();
                                                            //  Toast.makeText(SellerAddDispatchActivity.this, "Quantity can't be greater than remaining quantity.", Toast.LENGTH_LONG).show();

                                                            //      addtxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                                                            //     addtxtvw.setEnabled(false);

                                                            remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: " + dispatchremainingpaystr);

                                                            remainingqtytxtvw.setText("Remaining Quantity: " + dispatchremainingqtystr + "0");

                                                            invoice_amount_edt.setText(dispatchremainingpaystr);

                                                            sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                                            sbmttext.setTextColor(Color.parseColor("#808080"));
                                                            sbmtrelative.setEnabled(false);

                                                            remove_docs1_iv.setVisibility(View.GONE);
                                                            remove_docs2_iv.setVisibility(View.GONE);
                                                            remove_docs3_iv.setVisibility(View.GONE);
                                                            remove_docs4_iv.setVisibility(View.GONE);
                                                            remove_docs5_iv.setVisibility(View.GONE);
                                                            remove_docs6_iv.setVisibility(View.GONE);


                                                        } else {

                                                            // Toast.makeText(getActivity(),"QUANTITY -",Toast.LENGTH_LONG).show();
                                                            //   addtxtvw.setTextColor(Color.parseColor("#000000"));
                                                            //    addtxtvw.setEnabled(true);

                                                            sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                                            sbmttext.setTextColor(Color.WHITE);
                                                            sbmtrelative.setEnabled(true);

                                                  /*  remove_docs1_iv.setVisibility(View.VISIBLE);
                                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                                    remove_docs5_iv.setVisibility(View.VISIBLE);
                                                    remove_docs6_iv.setVisibility(View.VISIBLE);*/


                                                            if (invoice_qty_edt.getText().toString().length() > 0) {
                                                                float invoice_qty_float_latest_val = Float.parseFloat(invoice_qty_edt.getText().toString());

                                                                if (invoice_amount_edt.getText().toString().length() > 0) {


                                                                    //==========NEW ENHANCEMENT of quantity increase by 5============


                                                                    //invoice_qty_edt.setText(dispatchremainingqtystr+ "0");
                                                                    //  invoice_amount_edt.setText(dispatchremainingpaystr);


                                                          /*  DecimalFormat df = new DecimalFormat("0.00");

                                                            DecimalFormat df1 = new DecimalFormat("0.000");
                                                           */


                                                                    //   remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: "+ dispatchremainingpaystr);
                                                                    double invoice_qty_float_value = Double.parseDouble(invoice_qty_edt.getText().toString());
                                                                    double dispatchremainingqty_float_value = Double.parseDouble(dispatchremainingqtystr);

                                                                    double new_dispatchremainingqty_float_value = invoice_qty_float_value - dispatchremainingqty_float_value;


                                                                    //==========================================

                                                                    if (dispatchremainingqty_float_value < invoice_qty_float_value) {

                                                                        DecimalFormat df1 = new DecimalFormat("0.000");

                                                                        remainingqtytxtvw.setText("Exceeded Quantity: " + df1.format(new_dispatchremainingqty_float_value));

                                                                        double final_rate = Double.parseDouble(rate_str) ;
                                                                        double dispatchremainingpaystr_double_val = Double.parseDouble(dispatchremainingpaystr);
                                                                        double exceed_qty_val = new_dispatchremainingqty_float_value;
                                                                        double gst_val = Double.parseDouble(gst_str);
                                                                      //  double rate_pmt_val = Double.parseDouble(rate_str);


                                                                        double exceeded_gst_rate_val = (final_rate * exceed_qty_val * gst_val)/100;

                                                                        double final_amount_exceed = (final_rate* exceed_qty_val)+ exceeded_gst_rate_val + dispatchremainingpaystr_double_val;

                                                                        DecimalFormat df2 = new DecimalFormat("0.00");


                                                                        remaininginvoiceamounttxtvw.setText("Exceeded Invoice Amount: " + df2.format(final_amount_exceed));
                                                                        invoice_amount_edt.setText(df2.format(final_amount_exceed));


                                                                    } else {
                                                                        remainingqtytxtvw.setText("Remaining Quantity: " + dispatchremainingqtystr + "0");
                                                                        remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: " + dispatchremainingpaystr);
                                                                        invoice_amount_edt.setText(dispatchremainingpaystr);


                                                                    }


                                                                    float invoice_amount_float_latest_val = Float.parseFloat(invoice_amount_edt.getText().toString());

                                                                    float sum = invoice_qty_float_latest_val + invoice_amount_float_latest_val;

                                                                    if (sum == 0) {
                                                                        //       addtxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                                                                        //       addtxtvw.setEnabled(false);

                                                                        sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                                                        sbmttext.setTextColor(Color.parseColor("#808080"));
                                                                        sbmtrelative.setEnabled(false);

                                                                        remove_docs1_iv.setVisibility(View.GONE);
                                                                        remove_docs2_iv.setVisibility(View.GONE);
                                                                        remove_docs3_iv.setVisibility(View.GONE);
                                                                        remove_docs4_iv.setVisibility(View.GONE);
                                                                        remove_docs5_iv.setVisibility(View.GONE);
                                                                        remove_docs6_iv.setVisibility(View.GONE);
                                                                    } else {
                                                                        //       addtxtvw.setTextColor(Color.parseColor("#000000"));
                                                                        //      addtxtvw.setEnabled(true);

                                                                        sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                                                        sbmttext.setTextColor(Color.WHITE);
                                                                        sbmtrelative.setEnabled(true);

                                                            /*    remove_docs1_iv.setVisibility(View.VISIBLE);
                                                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                                                remove_docs4_iv.setVisibility(View.VISIBLE);
                                                                remove_docs5_iv.setVisibility(View.VISIBLE);
                                                                remove_docs6_iv.setVisibility(View.VISIBLE);
    */

                                                                    }


                                                                }


                                                            }


                                                        }

                                                }


                                        }


                                       /* else
                                        {
                                            sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                            sbmttext.setTextColor(Color.parseColor("#808080"));
                                            sbmtrelative.setEnabled(false);

                                            remove_docs1_iv.setVisibility(View.GONE);
                                            remove_docs2_iv.setVisibility(View.GONE);
                                            remove_docs3_iv.setVisibility(View.GONE);
                                            remove_docs4_iv.setVisibility(View.GONE);
                                            remove_docs5_iv.setVisibility(View.GONE);
                                            remove_docs6_iv.setVisibility(View.GONE);
                                        }*/
                                    }
                                    @Override
                                    public void afterTextChanged(Editable edt) {

                                        String temp = edt.toString ();
                                        int posDot = temp.indexOf(".");

                                        if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 3) {
                                            edt.delete(posDot + 4, posDot + 5);
                                        }




                                    }
                                });

                                invoice_amount_edt.addTextChangedListener(new TextWatcher() {

                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                                        if (s.length() > 0) {
                                            if(invoice_qty_edt.getText().toString().length()>0)
                                            {
                                                float invoice_qty_float_latest_val = Float.parseFloat(invoice_qty_edt.getText().toString());

                                                if(invoice_amount_edt.getText().toString().length()>0) {



                                                    float invoice_amount_float_latest_val = Float.parseFloat(invoice_amount_edt.getText().toString());



                                                    float sum = invoice_qty_float_latest_val + invoice_amount_float_latest_val;

                                                    if(sum == 0)
                                                    {
                                                        //   addtxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                                                        //   addtxtvw.setEnabled(false);

                                                        sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                                        sbmttext.setTextColor(Color.parseColor("#808080"));
                                                        sbmtrelative.setEnabled(false);

                                                        remove_docs1_iv.setVisibility(View.GONE);
                                                        remove_docs2_iv.setVisibility(View.GONE);
                                                        remove_docs3_iv.setVisibility(View.GONE);
                                                        remove_docs4_iv.setVisibility(View.GONE);
                                                        remove_docs5_iv.setVisibility(View.GONE);
                                                        remove_docs6_iv.setVisibility(View.GONE);

                                                    }
                                                    else
                                                    {
                                                        //   addtxtvw.setTextColor(Color.parseColor("#000000"));
                                                        //   addtxtvw.setEnabled(true);

                                                        sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                                        sbmttext.setTextColor(Color.WHITE);
                                                        sbmtrelative.setEnabled(true);

                                                     /*   remove_docs1_iv.setVisibility(View.VISIBLE);
                                                        remove_docs2_iv.setVisibility(View.VISIBLE);
                                                        remove_docs3_iv.setVisibility(View.VISIBLE);
                                                        remove_docs4_iv.setVisibility(View.VISIBLE);
                                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                                        remove_docs6_iv.setVisibility(View.VISIBLE);*/

                                                    }
                                                }





                                            }

                                            double remaining_invoice_amt_float_latest_val =  Double.parseDouble(total_amount_sum_str);

                                            double invoice_amount_float_latest_val = Double.parseDouble((invoice_amount_edt.getText().toString()));


                                            if(invoice_amount_float_latest_val>remaining_invoice_amt_float_latest_val)
                                            {
                                               // Toast.makeText(SellerAddDispatchActivity.this,"Amount can't be greater than remaining amount",Toast.LENGTH_SHORT).show();

                                                sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                                sbmttext.setTextColor(Color.parseColor("#808080"));
                                                sbmtrelative.setEnabled(false);


                                            } else {

                                                sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                                sbmttext.setTextColor(Color.WHITE);
                                                sbmtrelative.setEnabled(true);
                                            }
                                        }

                                        else
                                        {
                                            sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                            sbmttext.setTextColor(Color.parseColor("#808080"));
                                            sbmtrelative.setEnabled(false);

                                            remove_docs1_iv.setVisibility(View.GONE);
                                            remove_docs2_iv.setVisibility(View.GONE);
                                            remove_docs3_iv.setVisibility(View.GONE);
                                            remove_docs4_iv.setVisibility(View.GONE);
                                            remove_docs5_iv.setVisibility(View.GONE);
                                            remove_docs6_iv.setVisibility(View.GONE);
                                        }
                                    }

                                    @Override
                                    public void afterTextChanged(Editable edt) {

                                        String temp = edt.toString ();
                                        int posDot = temp.indexOf(".");

                                        if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 2) {
                                            edt.delete(posDot + 3, posDot + 4);
                                        }
                                    }
                                });


                                dispatch_date_edt.setOnClickListener(new View.OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        // TODO Auto-generated method stub

                                        newFragment = new DatePickerFragment();
                                        newFragment.show(getFragmentManager(), "datePicker");


                                    }


                                });

                                dispatch_date_edt.setFocusable(true);
                                dispatch_date_edt.setClickable(true);


                                invoice_due_date_edt.setOnClickListener(new View.OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        // TODO Auto-generated method stub

                                        newFragment1 = new DatePickerFragment1();
                                        newFragment1.show(getFragmentManager(), "datePicker");



                                    }
                                });

                                invoice_due_date_edt.setFocusable(false);
                                invoice_due_date_edt.setClickable(true);

                                invoice_amount_edt.addTextChangedListener(new TextWatcher() {
                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                    }

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        invoice_amount_edt.removeTextChangedListener(this);

                                        try {
                                            String originalString = s.toString();

                                          /*  Long longval;
                                            if (originalString.contains(",")) {
                                                originalString = originalString.replaceAll(",", "");
                                            }*/
                                          //  longval = Long.parseLong(originalString);

                                           /* DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                                            formatter.applyPattern("#,###,###,###");
                                            String formattedString = formatter.format(longval);
*/
                                            //setting text after format to EditText
                                            invoice_amount_edt.setText(originalString);
                                            invoice_amount_edt.setSelection(invoice_amount_edt.getText().length());
                                        } catch (NumberFormatException nfe) {
                                            nfe.printStackTrace();
                                        }

                                        invoice_amount_edt.addTextChangedListener(this);
                                    }
                                });







                                sbmtrelative.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        dispatch_date_str = dispatch_date_edt.getText().toString();
                                        truck_num_str = truck_num_edt.getText().toString();
                                        transport_name_str = transport_name_edt.getText().toString();
                                        mobile_num_str = mobile_num_edt.getText().toString();
                                        invoice_num_str = invoice_num_edt.getText().toString();
                                        invoice_due_date_str = invoice_due_date_edt.getText().toString();
                                        invoice_qty_str = invoice_qty_edt.getText().toString();

                                        boolean invalid = false;

                                        if (dispatch_date_str.equals("")) {
                                            invalid = true;
                                            Toast.makeText(getApplicationContext(), "Please Select Dispatch Date",
                                                    Toast.LENGTH_LONG).show();
                                        }
                                        else  if (truck_num_str.equals("")) {
                                            invalid = true;
                                            Toast.makeText(getApplicationContext(),
                                                    "Please Enter Truck No.", Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                        else  if (transport_name_str.equals("")) {
                                            invalid = true;
                                            Toast.makeText(getApplicationContext(),
                                                    "Please Enter Transport Name", Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                        else  if (invoice_num_str.equals("")) {
                                            invalid = true;
                                            Toast.makeText(getApplicationContext(),
                                                    "Please Enter Invoice No.", Toast.LENGTH_LONG)
                                                    .show();
                                        }


                                        else  if (invoice_qty_str.equals("")) {
                                            invalid = true;
                                            Toast.makeText(getApplicationContext(),
                                                    "Please Enter Invoice Quantity", Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                        else  if (invoice_amount_edt.getText().toString().equals("")) {
                                            invalid = true;
                                            Toast.makeText(getApplicationContext(),
                                                    "Please Enter Invoice Amount", Toast.LENGTH_LONG)
                                                    .show();
                                        }

                                        else if (invalid == false)
                                        {


                                            if(adminAdded == true)
                                            {
                                                if(NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)){
                                                    sbmtrelative.setClickable(false);
                                                    String adddispatchurl = null;
                                                    try {
                                                        adddispatchurl = APIName.URL+"/seller/addDispatch?trade_id="+ SingletonActivity.tradeid+"&auto_dispatch_id="+DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id");
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    System.out.println("ADD DISPATCH URL IS 1---"+ adddispatchurl);
                                                    AddDispatchAPI(adddispatchurl);

                                                }
                                                else{
                                                    util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                }
                                            }
                                            else
                                            {
                                                if(SingletonActivity.clickedaddicon == "click") {
                                                    if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                        sbmtrelative.setClickable(false);
                                                        String adddispatchurl = null;
                                                        adddispatchurl = APIName.URL + "/seller/addDispatch?trade_id=" + SingletonActivity.tradeid + "&auto_dispatch_id=0";
                                                        ;
                                                        System.out.println("ADD DISPATCH URL IS 2 ADD---" + adddispatchurl);
                                                        AddDispatchAPI(adddispatchurl);

                                                    } else {
                                                        util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                    }
                                                }
                                                else
                                                {
                                                    if (NetworkUtility.checkConnectivity(SellerAddDispatchActivity.this)) {
                                                        sbmtrelative.setClickable(false);
                                                        String adddispatchurl = null;
                                                        try {
                                                            adddispatchurl = APIName.URL + "/seller/addDispatch?trade_id=" + SingletonActivity.tradeid + "&auto_dispatch_id="+ DispatchDetailsJsonArray.getJSONObject( SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id");
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                        ;
                                                        System.out.println("ADD DISPATCH URL IS 2 EDIT---" + adddispatchurl);
                                                        AddDispatchAPI(adddispatchurl);

                                                    } else {
                                                        util.dialog(SellerAddDispatchActivity.this, "Please check your internet connection.");
                                                    }
                                                }
                                            }


                                        }

                                    }
                                });

                                cancelrelative.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                      //  finish();
                                        Intent i = new Intent(SellerAddDispatchActivity.this,SellerDispatchActivity.class);
                                        startActivity(i);
                                    }
                                });

                                add_docs.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {




                                            final Dialog mDialog = new Dialog(SellerAddDispatchActivity.this, R.style.DialogSlideAnim);
                                            mDialog.setCancelable(false);
                                            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            mDialog.getWindow().setBackgroundDrawable(
                                                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                            mDialog.setContentView(R.layout.dialog_upload_image);
                                            cancel_dialog = (ImageView) mDialog
                                                    .findViewById(R.id.cancel_dialog);
                                            upload_from_memory = (TextView) mDialog
                                                    .findViewById(R.id.upload_from_memory);
                                            click_image_from_camera = (TextView)mDialog
                                                    .findViewById(R.id.click_image_from_camera);
                                            upload_from_memory.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {

                                                    getFileChooserIntent();

                                                    mDialog.dismiss();

                                                }
                                            });
                                            click_image_from_camera.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {



                                                    if (checkPermission()) {
                                                        //main logic or main code

                                                        // . write your main code to execute, It will execute if the permission is already given.
                                                        Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                                        startActivityForResult(i, CAPTURE_IMAGE_CAPTURE_CODE);
                                                        mDialog.dismiss();

                                                    } else {

                                                        requestPermission();
                                                       // mDialog.dismiss();
                                                    }
                                                }


                                            });
                                            cancel_dialog.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    mDialog.dismiss();
                                                }
                                            });
                                            mDialog.show();

                                    }
                                });



                                selectfiletxtvw.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {



                                        final Dialog mDialog = new Dialog(SellerAddDispatchActivity.this, R.style.DialogSlideAnim);
                                        mDialog.setCancelable(false);
                                        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        mDialog.getWindow().setBackgroundDrawable(
                                                new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                        mDialog.setContentView(R.layout.dialog_upload_image);
                                        cancel_dialog = (ImageView) mDialog
                                                .findViewById(R.id.cancel_dialog);
                                        upload_from_memory = (TextView) mDialog
                                                .findViewById(R.id.upload_from_memory);
                                        click_image_from_camera = (TextView)mDialog
                                                .findViewById(R.id.click_image_from_camera);
                                        upload_from_memory.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                getFileChooserIntent();

                                                mDialog.dismiss();

                                            }
                                        });
                                        click_image_from_camera.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                              /*  Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                                startActivityForResult(i, CAPTURE_IMAGE_CAPTURE_CODE);
                                                mDialog.dismiss();*/

                                                if (checkPermission()) {
                                                    //main logic or main code

                                                    // . write your main code to execute, It will execute if the permission is already given.
                                                    Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                                    startActivityForResult(i, CAPTURE_IMAGE_CAPTURE_CODE);
                                                    mDialog.dismiss();

                                                } else {
                                                    requestPermission();
                                                }


                                            }
                                        });
                                        cancel_dialog.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mDialog.dismiss();
                                            }
                                        });
                                        mDialog.show();
                                    }
                                });

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                         //   pdia.dismiss();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_LONG).show();
                      //  pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SellerAddDispatchActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                        else
                        {
                            util.dialog(SellerAddDispatchActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();



                System.out.println("get seller current enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(SellerAddDispatchActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            System.out.println("SingletonActivity.sauda_patra_gen_date IN ADD DISPATCH ======"+ SingletonActivity.sauda_patra_gen_date);



            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);


            String dateString = SingletonActivity.sauda_patra_gen_date;
            DateFormat formatt = new SimpleDateFormat("dd-MM-yyyy");
            Date date = null;
            try {
                date = formatt.parse(dateString);


                System.out.println("Date in add payment=====:" +formatt.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            dialog.getDatePicker().setMinDate(date.getTime());
            dialog.getDatePicker().setMaxDate(c.getTimeInMillis());




            return dialog;


        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user

            String daynew ="";
            String monthnew ="";

            if(day<10)
            {
                daynew = "0"+Integer.toString(day);
            }
            else
            {
                daynew = Integer.toString(day);
            }

            if(month+1<10)
            {
                monthnew = "0"+ Integer.toString(month+1);
            }
            else
            {
                monthnew = Integer.toString(month+1);
            }


            dispatch_date_edt.setText(daynew + "/"
                    + monthnew + "/" + year);




        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("" +
                ""+ requestCode);



        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedFileURI = data.getData();
                System.out.println("SELECTED FILE PATH IS--"+ selectedFileURI);
                if(selectedFileURI!=null)
                {
                    String filePath = selectedFileURI.toString();

                    File f = new File(filePath);
                    String imageName = f.getName();


                    String uriString = selectedFileURI.toString();
                    if (uriString.startsWith("content://")) {
                        Cursor cursor = null;
                        try {
                            cursor = SellerAddDispatchActivity.this.getContentResolver().query(selectedFileURI, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                String displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                System.out.println("displayName 1 is------"+ displayName);
                               // uploadresumetxt.setText(displayName);
                                imgattachlist.add(displayName);
                            }
                        } finally {
                            cursor.close();
                        }
                    }else if (uriString.startsWith("file://")) {
                        String string = imageName;
                        String[] parts = string.split("%3A");
                        String part1 = parts[0]; // 004
                        String part2 = parts[1]; // 034556

                        ContentResolver cR = this.getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        String type = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));

                        String imageNames = part1+" "+part2+"."+type;
                        imgattachlist.add(imageNames);
                    }


                    try{
                        InputStream iStream =   getContentResolver().openInputStream(selectedFileURI);
                        byte[] byteArray = getBytes(iStream);
                        attachmentstr = encodeByteArrayIntoBase64String(byteArray);

                        ContentResolver cR = this.getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        String type = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));

                        System.out.println("extension is------"+ type);
                       // String imageNames = part1+" "+part2+"."+type;
                      //  Toast.makeText(SellerAddDispatchActivity.this,"IMAGE SELECTED IS==="+ imageNames,Toast.LENGTH_LONG).show();



                        // attachmentstr = "data:application/"+type+"; base64,"+attachmentstr+"&user_code="+userCodestr;
                        attachmentstr = "data:application/"+type+"; base64,"+attachmentstr;

                        System.out.println("BASE 64 STRING------"+ attachmentstr);

                        attachlist.add(attachmentstr);
                        System.out.println("attachlist------"+ attachlist);

                    //    Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST====>>"+ certificate_file_jsonarray.toString().length(),Toast.LENGTH_LONG).show();



                        if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {

                            if(SingletonActivity.clickedaddicon == "") {


                                if (certificate_file_jsonarray.length() == 1) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            //  Toast.makeText(SellerAddDispatchActivity.this,"certificate_file_jsonarray====>>1"+ certificate_file_jsonarray.length(),Toast.LENGTH_LONG).show();

                                            viewdocrel1.setVisibility(View.VISIBLE);
                                            remove_docs1_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    if (imgattachlist.size() == 1) {

                                        //  Toast.makeText(SellerAddDispatchActivity.this,"certificate_file_jsonarray====>>2"+ certificate_file_jsonarray.length(),Toast.LENGTH_LONG).show();

                                        viewdocrel2.setVisibility(View.VISIBLE);
                                        remove_docs2_iv.setVisibility(View.VISIBLE);
                                        selectedfile2txtvw.setText(imgattachlist.get(0));
                                    }

                                    if (imgattachlist.size() == 2) {
                                        viewdocrel3.setVisibility(View.VISIBLE);
                                        remove_docs3_iv.setVisibility(View.VISIBLE);
                                        selectedfile3txtvw.setText(imgattachlist.get(1));
                                    }

                                    if (imgattachlist.size() == 3) {
                                        viewdocrel4.setVisibility(View.VISIBLE);
                                        remove_docs4_iv.setVisibility(View.VISIBLE);
                                        selectedfile4txtvw.setText(imgattachlist.get(2));
                                    }

                                    if (imgattachlist.size() == 4) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(3));
                                    }

                                    if (imgattachlist.size() == 5) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(4));
                                    }
                                }

                                if (certificate_file_jsonarray.length() == 2) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                            viewdocrel1.setVisibility(View.VISIBLE);
                                            remove_docs1_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel2.setVisibility(View.VISIBLE);
                                            remove_docs2_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    if (imgattachlist.size() == 1) {
                                        viewdocrel3.setVisibility(View.VISIBLE);
                                        remove_docs3_iv.setVisibility(View.VISIBLE);
                                        selectedfile3txtvw.setText(imgattachlist.get(0));
                                    }


                                    if (imgattachlist.size() == 2) {
                                        viewdocrel4.setVisibility(View.VISIBLE);
                                        remove_docs4_iv.setVisibility(View.VISIBLE);
                                        selectedfile4txtvw.setText(imgattachlist.get(1));
                                    }

                                    if (imgattachlist.size() == 3) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(2));
                                    }

                                    if (imgattachlist.size() == 4) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(3));
                                    }

                                }

                                if (certificate_file_jsonarray.length() == 3) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                            viewdocrel1.setVisibility(View.VISIBLE);
                                            remove_docs1_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel2.setVisibility(View.VISIBLE);
                                            remove_docs2_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel3.setVisibility(View.VISIBLE);
                                            remove_docs3_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    //  Toast.makeText(SellerAddDispatchActivity.this,"imgattachlist LOOP====>>"+ imgattachlist.size(),Toast.LENGTH_LONG).show();


                                    Log.e("TAG", imgattachlist.toString());
                                    if (imgattachlist.size() == 1) {
                                        viewdocrel4.setVisibility(View.VISIBLE);
                                        remove_docs4_iv.setVisibility(View.VISIBLE);
                                        selectedfile4txtvw.setText(imgattachlist.get(0));
                                    }


                                    if (imgattachlist.size() == 2) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(1));
                                    }

                                    if (imgattachlist.size() == 3) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(2));
                                    }


                                }

                                if (certificate_file_jsonarray.length() == 4) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel1.setVisibility(View.VISIBLE);
                                            remove_docs1_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel2.setVisibility(View.VISIBLE);
                                            remove_docs2_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel3.setVisibility(View.VISIBLE);
                                            remove_docs3_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel4.setVisibility(View.VISIBLE);
                                            remove_docs4_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (imgattachlist.size() == 1) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(0));
                                    }


                                    if (imgattachlist.size() == 2) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(1));
                                    }


                                }

                                if (certificate_file_jsonarray.length() == 5) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel1.setVisibility(View.VISIBLE);
                                            remove_docs1_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel2.setVisibility(View.VISIBLE);
                                            remove_docs2_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel3.setVisibility(View.VISIBLE);
                                            remove_docs3_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel4.setVisibility(View.VISIBLE);
                                            remove_docs4_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel5.setVisibility(View.VISIBLE);
                                            remove_docs5_iv.setVisibility(View.VISIBLE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (imgattachlist.size() == 1) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(0));
                                    }

                                }

                            }
                            else
                            {


                                if (certificate_file_jsonarray.length() == 1) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            //  Toast.makeText(SellerAddDispatchActivity.this,"certificate_file_jsonarray====>>1"+ certificate_file_jsonarray.length(),Toast.LENGTH_LONG).show();

                                            viewdocrel1.setVisibility(View.GONE);
                                            remove_docs1_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    if (imgattachlist.size() == 1) {

                                        //  Toast.makeText(SellerAddDispatchActivity.this,"certificate_file_jsonarray====>>2"+ certificate_file_jsonarray.length(),Toast.LENGTH_LONG).show();

                                        viewdocrel2.setVisibility(View.VISIBLE);
                                        remove_docs2_iv.setVisibility(View.VISIBLE);
                                        selectedfile2txtvw.setText(imgattachlist.get(0));
                                    }

                                    if (imgattachlist.size() == 2) {
                                        viewdocrel3.setVisibility(View.VISIBLE);
                                        remove_docs3_iv.setVisibility(View.VISIBLE);
                                        selectedfile3txtvw.setText(imgattachlist.get(1));
                                    }

                                    if (imgattachlist.size() == 3) {
                                        viewdocrel4.setVisibility(View.VISIBLE);
                                        remove_docs4_iv.setVisibility(View.VISIBLE);
                                        selectedfile4txtvw.setText(imgattachlist.get(2));
                                    }

                                    if (imgattachlist.size() == 4) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(3));
                                    }

                                    if (imgattachlist.size() == 5) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(4));
                                    }
                                }

                                if (certificate_file_jsonarray.length() == 2) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                            viewdocrel1.setVisibility(View.GONE);
                                            remove_docs1_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel2.setVisibility(View.GONE);
                                            remove_docs2_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    if (imgattachlist.size() == 1) {
                                        viewdocrel3.setVisibility(View.VISIBLE);
                                        remove_docs3_iv.setVisibility(View.VISIBLE);
                                        selectedfile3txtvw.setText(imgattachlist.get(0));
                                    }


                                    if (imgattachlist.size() == 2) {
                                        viewdocrel4.setVisibility(View.VISIBLE);
                                        remove_docs4_iv.setVisibility(View.VISIBLE);
                                        selectedfile4txtvw.setText(imgattachlist.get(1));
                                    }

                                    if (imgattachlist.size() == 3) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(2));
                                    }

                                    if (imgattachlist.size() == 4) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(3));
                                    }

                                }

                                if (certificate_file_jsonarray.length() == 3) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                            viewdocrel1.setVisibility(View.GONE);
                                            remove_docs1_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel2.setVisibility(View.GONE);
                                            remove_docs2_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel3.setVisibility(View.GONE);
                                            remove_docs3_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    //  Toast.makeText(SellerAddDispatchActivity.this,"imgattachlist LOOP====>>"+ imgattachlist.size(),Toast.LENGTH_LONG).show();


                                    Log.e("TAG", imgattachlist.toString());
                                    if (imgattachlist.size() == 1) {
                                        viewdocrel4.setVisibility(View.VISIBLE);
                                        remove_docs4_iv.setVisibility(View.VISIBLE);
                                        selectedfile4txtvw.setText(imgattachlist.get(0));
                                    }


                                    if (imgattachlist.size() == 2) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(1));
                                    }

                                    if (imgattachlist.size() == 3) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(2));
                                    }


                                }

                                if (certificate_file_jsonarray.length() == 4) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel1.setVisibility(View.GONE);
                                            remove_docs1_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel2.setVisibility(View.GONE);
                                            remove_docs2_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel3.setVisibility(View.GONE);
                                            remove_docs3_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel4.setVisibility(View.GONE);
                                            remove_docs4_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (imgattachlist.size() == 1) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(0));
                                    }


                                    if (imgattachlist.size() == 2) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(1));
                                    }


                                }

                                if (certificate_file_jsonarray.length() == 5) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel1.setVisibility(View.GONE);
                                            remove_docs1_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel2.setVisibility(View.GONE);
                                            remove_docs2_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel3.setVisibility(View.GONE);
                                            remove_docs3_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel4.setVisibility(View.GONE);
                                            remove_docs4_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel5.setVisibility(View.GONE);
                                            remove_docs5_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (imgattachlist.size() == 1) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(0));
                                    }

                                }

                            }


                        }
                        else {

                         //   Toast.makeText(SellerAddDispatchActivity.this,"CAPTURED IMAGE L4",Toast.LENGTH_SHORT).show();

                        //    Toast.makeText(SellerAddDispatchActivity.this,"ELSE LOOP====>>"+attachlist.size(),Toast.LENGTH_LONG).show();

                            viewdocrel1.setVisibility(View.GONE);
                            remove_docs1_iv.setVisibility(View.GONE);
                            viewdocrel2.setVisibility(View.GONE);
                            remove_docs2_iv.setVisibility(View.GONE);
                            viewdocrel3.setVisibility(View.GONE);
                            remove_docs3_iv.setVisibility(View.GONE);
                            viewdocrel4.setVisibility(View.GONE);
                            remove_docs4_iv.setVisibility(View.GONE);
                            viewdocrel5.setVisibility(View.GONE);
                            remove_docs5_iv.setVisibility(View.GONE);
                            viewdocrel6.setVisibility(View.GONE);
                            remove_docs6_iv.setVisibility(View.GONE);

                            if (attachlist.size() == 1) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                selectedfile1txtvw.setText(imgattachlist.get(0));


                            }

                            if (attachlist.size() == 2) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);


                                selectedfile1txtvw.setText(imgattachlist.get(0));
                                selectedfile2txtvw.setText(imgattachlist.get(1));



                            }

                            if (attachlist.size() == 3) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                viewdocrel3.setVisibility(View.VISIBLE);
                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                selectedfile1txtvw.setText(imgattachlist.get(0));
                                selectedfile2txtvw.setText(imgattachlist.get(1));
                                selectedfile3txtvw.setText(imgattachlist.get(2));





                            }

                            if (attachlist.size() == 4) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                viewdocrel3.setVisibility(View.VISIBLE);
                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                viewdocrel4.setVisibility(View.VISIBLE);
                                remove_docs4_iv.setVisibility(View.VISIBLE);

                                selectedfile1txtvw.setText(imgattachlist.get(0));
                                selectedfile2txtvw.setText(imgattachlist.get(1));
                                selectedfile3txtvw.setText(imgattachlist.get(2));
                                selectedfile4txtvw.setText(imgattachlist.get(3));


                            }

                            if (attachlist.size() == 5) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                viewdocrel3.setVisibility(View.VISIBLE);
                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                viewdocrel4.setVisibility(View.VISIBLE);
                                remove_docs4_iv.setVisibility(View.VISIBLE);
                                viewdocrel5.setVisibility(View.VISIBLE);
                                remove_docs5_iv.setVisibility(View.VISIBLE);
                                selectedfile1txtvw.setText(imgattachlist.get(0));
                                selectedfile2txtvw.setText(imgattachlist.get(1));
                                selectedfile3txtvw.setText(imgattachlist.get(2));
                                selectedfile4txtvw.setText(imgattachlist.get(3));
                                selectedfile5txtvw.setText(imgattachlist.get(4));




                            }
                            if (attachlist.size() == 6) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                viewdocrel3.setVisibility(View.VISIBLE);
                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                viewdocrel4.setVisibility(View.VISIBLE);
                                remove_docs4_iv.setVisibility(View.VISIBLE);
                                viewdocrel5.setVisibility(View.VISIBLE);
                                remove_docs5_iv.setVisibility(View.VISIBLE);
                                viewdocrel6.setVisibility(View.VISIBLE);
                                remove_docs6_iv.setVisibility(View.VISIBLE);
                                selectedfile1txtvw.setText(imgattachlist.get(0));
                                selectedfile2txtvw.setText(imgattachlist.get(1));
                                selectedfile3txtvw.setText(imgattachlist.get(2));
                                selectedfile4txtvw.setText(imgattachlist.get(3));
                                selectedfile5txtvw.setText(imgattachlist.get(4));
                                selectedfile6txtvw.setText(imgattachlist.get(5));


                            }
                        }
                        remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                p = p + 1;




                                viewdocrel1.setVisibility(View.GONE);
                                remove_docs1_iv.setVisibility(View.GONE);



                                if(attachlist.size() == 1) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);


                                 //   Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.1=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }
                                if(attachlist.size() == 2) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);

                                  //  Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.2=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }

                                if(attachlist.size() == 3) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);

                                 //   Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.3=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }

                                if(attachlist.size() == 4) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);

                                 //   Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.4=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }

                                if(attachlist.size() == 5) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);

                                  //  Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.5=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }
                                if(attachlist.size() == 6) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);

                                  //  Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.6=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }



                            }
                        });

                        remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                p = p + 1;



                                viewdocrel2.setVisibility(View.GONE);
                                remove_docs2_iv.setVisibility(View.GONE);

                             //   Toast.makeText(SellerAddDispatchActivity.this,"ATTACH LIST=="+ attachlist,Toast.LENGTH_SHORT).show();
                                if(attachlist.size()>0)
                                {
                                if(p == 1)
                                {
                                    if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {
                                        attachlist.remove(0);
                                        imgattachlist.remove(0);
                                    }
                                    else {
                                        attachlist.remove(1);
                                        imgattachlist.remove(1);
                                    }
                                }
                                else
                                    {

                                        attachlist.remove(0);
                                        imgattachlist.remove(0);
                                    }


                                }




                            }
                        });
                        remove_docs3_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {



                                p = p + 1;

                                viewdocrel3.setVisibility(View.GONE);
                                remove_docs3_iv.setVisibility(View.GONE);
                                if(attachlist.size()>0) {
                                    if (p == 1) {


                                        if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {
                                            attachlist.remove(attachlist.size() - 1);
                                            imgattachlist.remove(imgattachlist.size() - 1);
                                        }
                                        else {
                                            attachlist.remove(2);
                                            imgattachlist.remove(2);
                                        }

                                    } else {
                                        attachlist.remove(attachlist.size() - 1);
                                        imgattachlist.remove(imgattachlist.size() - 1);
                                    }
                                }


                            }
                        });
                        remove_docs4_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {




                                p = p + 1;
                                viewdocrel4.setVisibility(View.GONE);
                                remove_docs4_iv.setVisibility(View.GONE);


                                if(attachlist.size()>0) {
                                    if (p == 1) {


                                        if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {
                                            attachlist.remove(attachlist.size() - 1);
                                            imgattachlist.remove(imgattachlist.size() - 1);
                                        }
                                        else {
                                            attachlist.remove(3);
                                            imgattachlist.remove(3);

                                        }

                                    } else {
                                        attachlist.remove(attachlist.size() - 1);
                                        imgattachlist.remove(imgattachlist.size() - 1);
                                    }
                                }


                            }
                        });
                        remove_docs5_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {




                                p = p + 1;
                                viewdocrel5.setVisibility(View.GONE);
                                remove_docs5_iv.setVisibility(View.GONE);


                                if(attachlist.size()>0) {

                                    if (p == 1) {

                                        if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {
                                            attachlist.remove(attachlist.size() - 1);
                                            imgattachlist.remove(imgattachlist.size() - 1);
                                        }
                                        else {
                                            attachlist.remove(4);
                                            imgattachlist.remove(4);


                                        }


                                    } else {
                                        attachlist.remove(attachlist.size() - 1);
                                        imgattachlist.remove(imgattachlist.size() - 1);
                                    }
                                }


                            }
                        });
                        remove_docs6_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                p = p + 1;
                                viewdocrel6.setVisibility(View.GONE);
                                remove_docs6_iv.setVisibility(View.GONE);

                                if(attachlist.size()>0) {
                                    if (p == 1) {

                                        if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {
                                            attachlist.remove(attachlist.size() - 1);
                                            imgattachlist.remove(imgattachlist.size() - 1);
                                        }
                                        else {
                                            attachlist.remove(5);
                                            imgattachlist.remove(5);

                                        }


                                    } else {
                                        attachlist.remove(attachlist.size() - 1);
                                        imgattachlist.remove(imgattachlist.size() - 1);
                                    }
                                }


                            }
                        });


                        String joined = android.text.TextUtils.join("?", attachlist);

                        attachmentstr = joined;
                        scroll.post(new Runnable() {
                            @Override
                            public void run() {
                                scroll.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        });

                        if(selectedfile1txtvw.getText().toString().length()>0 && selectedfile2txtvw.getText().toString().length()>0 && selectedfile3txtvw.getText().toString().length()>0 && selectedfile4txtvw.getText().toString().length()>0 && selectedfile5txtvw.getText().toString().length()>0 && selectedfile6txtvw.getText().toString().length()>0)
                        {
                            selectfiletxtvw.setVisibility(View.GONE);
                            add_docs.setVisibility(View.GONE);
                        }
                        else
                        {
                            selectfiletxtvw.setVisibility(View.VISIBLE);
                            add_docs.setVisibility(View.VISIBLE);
                        }


                    }
                    catch (IOException e)
                    {
                        Toast.makeText(getBaseContext(),"An error occurred with file chosen",Toast.LENGTH_LONG).show();
                    }



                }



            }

        }

        if (requestCode == 1888) {
            if (resultCode == Activity.RESULT_OK) {

               // Toast.makeText(SellerAddDispatchActivity.this,"CAPTURED IMAGE L1",Toast.LENGTH_SHORT).show();
             // Uri selectedFileURI = data.getData();


                Bitmap photo = (Bitmap) data.getExtras().get("data");



                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                Uri tempUri = getImageUri(SellerAddDispatchActivity.this, photo);

                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));

                System.out.println("CAPTURED URI FROM IMAGE==="+ tempUri);

                Uri  selectedFileURI = tempUri;

                if(selectedFileURI!=null)
                {
                    String filePath = selectedFileURI.toString();

                    File f = new File(filePath);
                    String imageName = f.getName();


                    String uriString = selectedFileURI.toString();
                    if (uriString.startsWith("content://")) {
                        Cursor cursor = null;
                        try {
                            cursor = SellerAddDispatchActivity.this.getContentResolver().query(selectedFileURI, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                String displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                System.out.println("displayName 1 is------"+ displayName);
                                // uploadresumetxt.setText(displayName);
                                imgattachlist.add(displayName);
                            }
                        } finally {
                            cursor.close();
                        }
                    }else if (uriString.startsWith("file://")) {
                        String string = imageName;
                        String[] parts = string.split("%3A");
                        String part1 = parts[0]; // 004
                        String part2 = parts[1]; // 034556

                        ContentResolver cR = this.getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        String type = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));

                        String imageNames = part1+" "+part2+"."+type;
                        imgattachlist.add(imageNames);
                    }







                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);

                    byte[] imageInByte = stream.toByteArray();
                    long lengthbmp = imageInByte.length;

                  //  Toast.makeText(SellerAddDispatchActivity.this,"lengthbmp SELECTED IS==="+ lengthbmp,Toast.LENGTH_LONG).show();

                    String imgString = Base64.encodeToString(stream.toByteArray(),
                                Base64.NO_WRAP);

                       // Toast.makeText(SellerAddDispatchActivity.this,"IMAGE TAKEN IS==="+ imgString,Toast.LENGTH_LONG).show();





                       // InputStream iStream =   getContentResolver().openInputStream(selectedFileURI);
                       // byte[] byteArray = getBytes(iStream);

                       // attachmentstr = encodeByteArrayIntoBase64String(byteArray);


                        attachmentstr = imgString;
                        ContentResolver cR = this.getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        String type = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));

                        System.out.println("extension is------"+ type);
                        // String imageNames = part1+" "+part2+"."+type;
                        //  Toast.makeText(SellerAddDispatchActivity.this,"IMAGE SELECTED IS==="+ imageNames,Toast.LENGTH_LONG).show();



                        // attachmentstr = "data:application/"+type+"; base64,"+attachmentstr+"&user_code="+userCodestr;
                        attachmentstr = "data:application/"+type+"; base64,"+attachmentstr;

                        System.out.println("BASE 64 STRING------"+ attachmentstr);

                        attachlist.add(attachmentstr);
                        System.out.println("attachlist------"+ attachlist);

                        //    Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST====>>"+ certificate_file_jsonarray.toString().length(),Toast.LENGTH_LONG).show();



                        if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {

                           // Toast.makeText(SellerAddDispatchActivity.this,"CAPTURED IMAGE L2",Toast.LENGTH_SHORT).show();



                            {


                                if (certificate_file_jsonarray.length() == 1) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            //  Toast.makeText(SellerAddDispatchActivity.this,"certificate_file_jsonarray====>>1"+ certificate_file_jsonarray.length(),Toast.LENGTH_LONG).show();

                                            viewdocrel1.setVisibility(View.GONE);
                                            remove_docs1_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    if (imgattachlist.size() == 1) {

                                        //  Toast.makeText(SellerAddDispatchActivity.this,"certificate_file_jsonarray====>>2"+ certificate_file_jsonarray.length(),Toast.LENGTH_LONG).show();

                                        viewdocrel2.setVisibility(View.VISIBLE);
                                        remove_docs2_iv.setVisibility(View.VISIBLE);
                                        selectedfile2txtvw.setText(imgattachlist.get(0));
                                    }

                                    if (imgattachlist.size() == 2) {
                                        viewdocrel3.setVisibility(View.VISIBLE);
                                        remove_docs3_iv.setVisibility(View.VISIBLE);
                                        selectedfile3txtvw.setText(imgattachlist.get(1));
                                    }

                                    if (imgattachlist.size() == 3) {
                                        viewdocrel4.setVisibility(View.VISIBLE);
                                        remove_docs4_iv.setVisibility(View.VISIBLE);
                                        selectedfile4txtvw.setText(imgattachlist.get(2));
                                    }

                                    if (imgattachlist.size() == 4) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(3));
                                    }

                                    if (imgattachlist.size() == 5) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(4));
                                    }
                                }

                                if (certificate_file_jsonarray.length() == 2) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                            viewdocrel1.setVisibility(View.GONE);
                                            remove_docs1_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel2.setVisibility(View.GONE);
                                            remove_docs2_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                    if (imgattachlist.size() == 1) {
                                        viewdocrel3.setVisibility(View.VISIBLE);
                                        remove_docs3_iv.setVisibility(View.VISIBLE);
                                        selectedfile3txtvw.setText(imgattachlist.get(0));
                                    }


                                    if (imgattachlist.size() == 2) {
                                        viewdocrel4.setVisibility(View.VISIBLE);
                                        remove_docs4_iv.setVisibility(View.VISIBLE);
                                        selectedfile4txtvw.setText(imgattachlist.get(1));
                                    }

                                    if (imgattachlist.size() == 3) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(2));
                                    }

                                    if (imgattachlist.size() == 4) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(3));
                                    }

                                }

                                if (certificate_file_jsonarray.length() == 3) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {


                                            viewdocrel1.setVisibility(View.GONE);
                                            remove_docs1_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel2.setVisibility(View.GONE);
                                            remove_docs2_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel3.setVisibility(View.GONE);
                                            remove_docs3_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    //  Toast.makeText(SellerAddDispatchActivity.this,"imgattachlist LOOP====>>"+ imgattachlist.size(),Toast.LENGTH_LONG).show();


                                    Log.e("TAG", imgattachlist.toString());
                                    if (imgattachlist.size() == 1) {
                                        viewdocrel4.setVisibility(View.VISIBLE);
                                        remove_docs4_iv.setVisibility(View.VISIBLE);
                                        selectedfile4txtvw.setText(imgattachlist.get(0));
                                    }


                                    if (imgattachlist.size() == 2) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(1));
                                    }

                                    if (imgattachlist.size() == 3) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(2));
                                    }


                                }

                                if (certificate_file_jsonarray.length() == 4) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel1.setVisibility(View.GONE);
                                            remove_docs1_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel2.setVisibility(View.GONE);
                                            remove_docs2_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel3.setVisibility(View.GONE);
                                            remove_docs3_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel4.setVisibility(View.GONE);
                                            remove_docs4_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (imgattachlist.size() == 1) {
                                        viewdocrel5.setVisibility(View.VISIBLE);
                                        remove_docs5_iv.setVisibility(View.VISIBLE);
                                        selectedfile5txtvw.setText(imgattachlist.get(0));
                                    }


                                    if (imgattachlist.size() == 2) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(1));
                                    }


                                }

                                if (certificate_file_jsonarray.length() == 5) {

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel1.setVisibility(View.GONE);
                                            remove_docs1_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel2.setVisibility(View.GONE);
                                            remove_docs2_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel3.setVisibility(View.GONE);
                                            remove_docs3_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {

                                            viewdocrel4.setVisibility(View.GONE);
                                            remove_docs4_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(SingletonActivity.checkboxclickedposition).getString("auto_dispatch_id"))) {
                                            viewdocrel5.setVisibility(View.GONE);
                                            remove_docs5_iv.setVisibility(View.GONE);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (imgattachlist.size() == 1) {
                                        viewdocrel6.setVisibility(View.VISIBLE);
                                        remove_docs6_iv.setVisibility(View.VISIBLE);
                                        selectedfile6txtvw.setText(imgattachlist.get(0));
                                    }

                                }

                            }

                           /* if (certificate_file_jsonarray.length() == 1) {

                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);


                                if(imgattachlist.size()==1) {
                                    viewdocrel2.setVisibility(View.VISIBLE);
                                    remove_docs2_iv.setVisibility(View.VISIBLE);
                                    selectedfile2txtvw.setText(imgattachlist.get(0));
                                }

                                if(imgattachlist.size()==2) {
                                    viewdocrel3.setVisibility(View.VISIBLE);
                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                    selectedfile3txtvw.setText(imgattachlist.get(1));
                                }

                                if(imgattachlist.size()==3) {
                                    viewdocrel4.setVisibility(View.VISIBLE);
                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                    selectedfile4txtvw.setText(imgattachlist.get(2));
                                }

                                if(imgattachlist.size()==4) {
                                    viewdocrel5.setVisibility(View.VISIBLE);
                                    remove_docs5_iv.setVisibility(View.VISIBLE);
                                    selectedfile5txtvw.setText(imgattachlist.get(3));
                                }

                                if(imgattachlist.size()==5) {
                                    viewdocrel6.setVisibility(View.VISIBLE);
                                    remove_docs6_iv.setVisibility(View.VISIBLE);
                                    selectedfile6txtvw.setText(imgattachlist.get(4));
                                }
                            }

                            if (certificate_file_jsonarray.length() == 2) {

                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);

                                if(imgattachlist.size()==1) {
                                    viewdocrel3.setVisibility(View.VISIBLE);
                                    remove_docs3_iv.setVisibility(View.VISIBLE);
                                    selectedfile3txtvw.setText(imgattachlist.get(0));
                                }


                                if(imgattachlist.size()==2) {
                                    viewdocrel4.setVisibility(View.VISIBLE);
                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                    selectedfile4txtvw.setText(imgattachlist.get(1));
                                }

                                if(imgattachlist.size()==3) {
                                    viewdocrel5.setVisibility(View.VISIBLE);
                                    remove_docs5_iv.setVisibility(View.VISIBLE);
                                    selectedfile5txtvw.setText(imgattachlist.get(2));
                                }

                                if(imgattachlist.size()==4) {
                                    viewdocrel6.setVisibility(View.VISIBLE);
                                    remove_docs6_iv.setVisibility(View.VISIBLE);
                                    selectedfile6txtvw.setText(imgattachlist.get(3));
                                }

                            }

                            if (certificate_file_jsonarray.length() == 3) {

                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                viewdocrel3.setVisibility(View.VISIBLE);
                                remove_docs3_iv.setVisibility(View.VISIBLE);

                                Log.e("TAG",imgattachlist.toString());
                                if(imgattachlist.size()==1) {
                                    viewdocrel4.setVisibility(View.VISIBLE);
                                    remove_docs4_iv.setVisibility(View.VISIBLE);
                                    selectedfile4txtvw.setText(imgattachlist.get(0));
                                }



                                if(imgattachlist.size()==2) {
                                    viewdocrel5.setVisibility(View.VISIBLE);
                                    remove_docs5_iv.setVisibility(View.VISIBLE);
                                    selectedfile5txtvw.setText(imgattachlist.get(1));
                                }

                                if(imgattachlist.size()==3) {
                                    viewdocrel6.setVisibility(View.VISIBLE);
                                    remove_docs6_iv.setVisibility(View.VISIBLE);
                                    selectedfile6txtvw.setText(imgattachlist.get(2));
                                }


                            }

                            if (certificate_file_jsonarray.length() == 4) {

                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                viewdocrel3.setVisibility(View.VISIBLE);
                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                viewdocrel4.setVisibility(View.VISIBLE);
                                remove_docs4_iv.setVisibility(View.VISIBLE);
                                if(imgattachlist.size()==1) {
                                    viewdocrel5.setVisibility(View.VISIBLE);
                                    remove_docs5_iv.setVisibility(View.VISIBLE);
                                    selectedfile5txtvw.setText(imgattachlist.get(0));
                                }




                                if(imgattachlist.size()==2) {
                                    viewdocrel6.setVisibility(View.VISIBLE);
                                    remove_docs6_iv.setVisibility(View.VISIBLE);
                                    selectedfile6txtvw.setText(imgattachlist.get(1));
                                }


                            }

                            if (certificate_file_jsonarray.length() == 5) {

                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                viewdocrel3.setVisibility(View.VISIBLE);
                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                viewdocrel4.setVisibility(View.VISIBLE);
                                remove_docs4_iv.setVisibility(View.VISIBLE);
                                viewdocrel5.setVisibility(View.VISIBLE);
                                remove_docs5_iv.setVisibility(View.VISIBLE);

                                if(imgattachlist.size()==1) {
                                    viewdocrel6.setVisibility(View.VISIBLE);
                                    remove_docs6_iv.setVisibility(View.VISIBLE);
                                    selectedfile6txtvw.setText(imgattachlist.get(0));
                                }

                            }
*/

                        }
                        else {



                          //  Toast.makeText(SellerAddDispatchActivity.this,"CAPTURED IMAGE L3",Toast.LENGTH_SHORT).show();

                            viewdocrel1.setVisibility(View.GONE);
                            remove_docs1_iv.setVisibility(View.GONE);
                            viewdocrel2.setVisibility(View.GONE);
                            remove_docs2_iv.setVisibility(View.GONE);
                            viewdocrel3.setVisibility(View.GONE);
                            remove_docs3_iv.setVisibility(View.GONE);
                            viewdocrel4.setVisibility(View.GONE);
                            remove_docs4_iv.setVisibility(View.GONE);
                            viewdocrel5.setVisibility(View.GONE);
                            remove_docs5_iv.setVisibility(View.GONE);
                            viewdocrel6.setVisibility(View.GONE);
                            remove_docs6_iv.setVisibility(View.GONE);

                            if (attachlist.size() == 1) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                selectedfile1txtvw.setText(imgattachlist.get(0));


                            }

                            if (attachlist.size() == 2) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);


                                selectedfile1txtvw.setText(imgattachlist.get(0));
                                selectedfile2txtvw.setText(imgattachlist.get(1));



                            }

                            if (attachlist.size() == 3) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                viewdocrel3.setVisibility(View.VISIBLE);
                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                selectedfile1txtvw.setText(imgattachlist.get(0));
                                selectedfile2txtvw.setText(imgattachlist.get(1));
                                selectedfile3txtvw.setText(imgattachlist.get(2));





                            }

                            if (attachlist.size() == 4) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                viewdocrel3.setVisibility(View.VISIBLE);
                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                viewdocrel4.setVisibility(View.VISIBLE);
                                remove_docs4_iv.setVisibility(View.VISIBLE);

                                selectedfile1txtvw.setText(imgattachlist.get(0));
                                selectedfile2txtvw.setText(imgattachlist.get(1));
                                selectedfile3txtvw.setText(imgattachlist.get(2));
                                selectedfile4txtvw.setText(imgattachlist.get(3));


                            }

                            if (attachlist.size() == 5) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                viewdocrel3.setVisibility(View.VISIBLE);
                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                viewdocrel4.setVisibility(View.VISIBLE);
                                remove_docs4_iv.setVisibility(View.VISIBLE);
                                viewdocrel5.setVisibility(View.VISIBLE);
                                remove_docs5_iv.setVisibility(View.VISIBLE);
                                selectedfile1txtvw.setText(imgattachlist.get(0));
                                selectedfile2txtvw.setText(imgattachlist.get(1));
                                selectedfile3txtvw.setText(imgattachlist.get(2));
                                selectedfile4txtvw.setText(imgattachlist.get(3));
                                selectedfile5txtvw.setText(imgattachlist.get(4));




                            }
                            if (attachlist.size() == 6) {
                                viewdocrel1.setVisibility(View.VISIBLE);
                                remove_docs1_iv.setVisibility(View.VISIBLE);
                                viewdocrel2.setVisibility(View.VISIBLE);
                                remove_docs2_iv.setVisibility(View.VISIBLE);
                                viewdocrel3.setVisibility(View.VISIBLE);
                                remove_docs3_iv.setVisibility(View.VISIBLE);
                                viewdocrel4.setVisibility(View.VISIBLE);
                                remove_docs4_iv.setVisibility(View.VISIBLE);
                                viewdocrel5.setVisibility(View.VISIBLE);
                                remove_docs5_iv.setVisibility(View.VISIBLE);
                                viewdocrel6.setVisibility(View.VISIBLE);
                                remove_docs6_iv.setVisibility(View.VISIBLE);
                                selectedfile1txtvw.setText(imgattachlist.get(0));
                                selectedfile2txtvw.setText(imgattachlist.get(1));
                                selectedfile3txtvw.setText(imgattachlist.get(2));
                                selectedfile4txtvw.setText(imgattachlist.get(3));
                                selectedfile5txtvw.setText(imgattachlist.get(4));
                                selectedfile6txtvw.setText(imgattachlist.get(5));


                            }
                        }
                        remove_docs1_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                p = p + 1;




                                viewdocrel1.setVisibility(View.GONE);
                                remove_docs1_iv.setVisibility(View.GONE);



                                if(attachlist.size() == 1) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);


                                    //   Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.1=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }
                                if(attachlist.size() == 2) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);

                                    //  Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.2=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }

                                if(attachlist.size() == 3) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);

                                    //   Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.3=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }

                                if(attachlist.size() == 4) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);

                                    //   Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.4=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }

                                if(attachlist.size() == 5) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);

                                    //  Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.5=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }
                                if(attachlist.size() == 6) {
                                    attachlist.remove(0);
                                    imgattachlist.remove(0);

                                    //  Toast.makeText(SellerAddDispatchActivity.this,"IMG ATTCH LIST 1.6=="+ imgattachlist,Toast.LENGTH_LONG).show();
                                }



                            }
                        });

                        remove_docs2_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                p = p + 1;



                                viewdocrel2.setVisibility(View.GONE);
                                remove_docs2_iv.setVisibility(View.GONE);

                                //   Toast.makeText(SellerAddDispatchActivity.this,"ATTACH LIST=="+ attachlist,Toast.LENGTH_SHORT).show();
                                if(attachlist.size()>0)
                                {
                                    if(p == 1)
                                    {
                                        if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {
                                            attachlist.remove(0);
                                            imgattachlist.remove(0);
                                        }
                                        else {
                                            attachlist.remove(1);
                                            imgattachlist.remove(1);
                                        }
                                    }
                                    else
                                    {

                                        attachlist.remove(0);
                                        imgattachlist.remove(0);
                                    }


                                }




                            }
                        });
                        remove_docs3_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {



                                p = p + 1;

                                viewdocrel3.setVisibility(View.GONE);
                                remove_docs3_iv.setVisibility(View.GONE);
                                if(attachlist.size()>0) {
                                    if (p == 1) {


                                        if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {
                                            attachlist.remove(attachlist.size() - 1);
                                            imgattachlist.remove(imgattachlist.size() - 1);
                                        }
                                        else {
                                            attachlist.remove(2);
                                            imgattachlist.remove(2);
                                        }

                                    } else {
                                        attachlist.remove(attachlist.size() - 1);
                                        imgattachlist.remove(imgattachlist.size() - 1);
                                    }
                                }


                            }
                        });
                        remove_docs4_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {




                                p = p + 1;
                                viewdocrel4.setVisibility(View.GONE);
                                remove_docs4_iv.setVisibility(View.GONE);


                                if(attachlist.size()>0) {
                                    if (p == 1) {


                                        if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {
                                            attachlist.remove(attachlist.size() - 1);
                                            imgattachlist.remove(imgattachlist.size() - 1);
                                        }
                                        else {
                                            attachlist.remove(3);
                                            imgattachlist.remove(3);

                                        }

                                    } else {
                                        attachlist.remove(attachlist.size() - 1);
                                        imgattachlist.remove(imgattachlist.size() - 1);
                                    }
                                }


                            }
                        });
                        remove_docs5_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {




                                p = p + 1;
                                viewdocrel5.setVisibility(View.GONE);
                                remove_docs5_iv.setVisibility(View.GONE);


                                if(attachlist.size()>0) {

                                    if (p == 1) {

                                        if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {
                                            attachlist.remove(attachlist.size() - 1);
                                            imgattachlist.remove(imgattachlist.size() - 1);
                                        }
                                        else {
                                            attachlist.remove(4);
                                            imgattachlist.remove(4);


                                        }


                                    } else {
                                        attachlist.remove(attachlist.size() - 1);
                                        imgattachlist.remove(imgattachlist.size() - 1);
                                    }
                                }


                            }
                        });
                        remove_docs6_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                p = p + 1;
                                viewdocrel6.setVisibility(View.GONE);
                                remove_docs6_iv.setVisibility(View.GONE);

                                if(attachlist.size()>0) {
                                    if (p == 1) {

                                        if(certificate_file_jsonarray != null && certificate_file_jsonarray.length() > 0 ) {
                                            attachlist.remove(attachlist.size() - 1);
                                            imgattachlist.remove(imgattachlist.size() - 1);
                                        }
                                        else {
                                            attachlist.remove(5);
                                            imgattachlist.remove(5);

                                        }


                                    } else {
                                        attachlist.remove(attachlist.size() - 1);
                                        imgattachlist.remove(imgattachlist.size() - 1);
                                    }
                                }


                            }
                        });


                        String joined = android.text.TextUtils.join("?", attachlist);

                        attachmentstr = joined;
                        scroll.post(new Runnable() {
                            @Override
                            public void run() {
                                scroll.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        });

                        if(selectedfile1txtvw.getText().toString().length()>0 && selectedfile2txtvw.getText().toString().length()>0 && selectedfile3txtvw.getText().toString().length()>0 && selectedfile4txtvw.getText().toString().length()>0 && selectedfile5txtvw.getText().toString().length()>0 && selectedfile6txtvw.getText().toString().length()>0)
                        {
                            selectfiletxtvw.setVisibility(View.GONE);
                            add_docs.setVisibility(View.GONE);
                        }
                        else
                        {
                            selectfiletxtvw.setVisibility(View.VISIBLE);
                            add_docs.setVisibility(View.VISIBLE);
                        }






                }



            }

        }



    }

    private Intent getFileChooserIntent() {
        String[] mimeTypes = {"image/*", "application/pdf"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
       // intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";

            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }

            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
        return intent;
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static String encodeByteArrayIntoBase64String(byte[] bytes)
    {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
      //  String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);

        ContentValues values=new ContentValues();
        values.put(MediaStore.Images.Media.TITLE,"Title");
        values.put(MediaStore.Images.Media.DESCRIPTION,"From Camera");
        Uri path=getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,values);

        return path;
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

   /* private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            // Permission is not granted
            return false;
        }
        return true;
    }*/

    private boolean checkPermission() {

        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);


        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED;

    }


    private void requestPermission() {

       /* ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);

        //new
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this,  new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSION_REQUEST_CODE);
            return;
        }*/


        ActivityCompat.requestPermissions(this, new String[]
                {
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,

                }, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
        /*    case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                    Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(i, CAPTURE_IMAGE_CAPTURE_CODE);
                  //  mDialog.dismiss();



                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;*/

            case PERMISSION_REQUEST_CODE:

                if (grantResults.length > 0) {

                    boolean CameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean WriteExternalStoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (CameraPermission && WriteExternalStoragePermission) {

                        Toast.makeText(SellerAddDispatchActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(SellerAddDispatchActivity.this,"Permission Denied",Toast.LENGTH_LONG).show();

                    }
                }

                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(SellerAddDispatchActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private String encodeImage(Bitmap bm)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

}

