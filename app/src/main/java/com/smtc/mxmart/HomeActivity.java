package com.smtc.mxmart;

import android.app.Activity;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.OpenableColumns;

import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.smtc.mxmart.Fragment.TradeFragment1012;
import com.smtc.mxmart.Fragment.TradeFragment111119;
import com.smtc.mxmart.Fragment.TransactionFragment;
import com.smtc.mxmart.Fragment.HomeFragment;
import com.smtc.mxmart.Fragment.MoreFragment;
import com.smtc.mxmart.Fragment.ProfileFragment;
import com.smtc.mxmart.Fragment.TradeFragment;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by 10161 on 11/6/2017.
 */

public class HomeActivity extends AppCompatActivity {

    BottomNavigationView navigation;
    String attachmentstr,userCodestr;
    UtilsDialog util = new UtilsDialog();
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    int clickedcount = 0;
    SessionManager sessionManager;
    boolean login;
    boolean isMandatoryFilled;
    JSONArray referencejsonArray;
    Menu menu;
    String commentStr, tradeStr,mobile_num,fcm_id,useridstr;
    TextView notification_total_count;
    FrameLayout rootLayout;
    ImageView logout_icon;
    TextView todays_offer_count, new_enquiry_count, seller_accepted_count, seller_rejected_count, sp_generated_count,dispatch_count,payment_count,buyer_rejected_count;
    Handler handler = new Handler();
    Timer timer;
    TimerTask doAsynchronousTask;
    public boolean isRefresh = false;
    int backButtonCount =0;
    JSONObject notify;
    String types;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        System.out.println("IN HOME ACTIVITY isNewEnquiryClicked==================="+ SingletonActivity.isNewEnquiryClicked);
        System.out.println("IN HOME ACTIVITY isSpTradeSellerClicked==================="+ SingletonActivity.isSpTradeSellerClicked);
        System.out.println("IN HOME ACTIVITY isSellerAcceptedClicked==================="+ SingletonActivity.isSellerAcceptedClicked);
        System.out.println("IN HOME ACTIVITY isSpTradeBuyerClicked==================="+ SingletonActivity.isSpTradeBuyerClicked);
        System.out.println("IN HOME ACTIVITY isLiveSalesClicked==================="+ SingletonActivity.isLiveSalesClicked);




        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        login = prefs.getBoolean("login", false);
        mobile_num = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id",null);
        useridstr = prefs.getString("userid",null);




        notification_total_count = (TextView) findViewById(R.id.notification_total_count);
        rootLayout =  (FrameLayout) findViewById(R.id.rootLayout);
        logout_icon = (ImageView)findViewById(R.id.logout_icon);

        sessionManager = new SessionManager(getApplicationContext());





    logout_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder builder1 = new AlertDialog.Builder(HomeActivity.this);
                builder1.setMessage("Are you sure you want to Logout?");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putBoolean("login",false);


                                editor.clear();
                                editor.commit();

                                sessionManager.logoutUser(mobile_num,fcm_id);
                            }
                        });


                builder1.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

                AlertDialog alert11 = builder1.create();
                alert11.show();



            }
        });





        if(login==true) {

          /*  Toolbar toolbar = (Toolbar) findViewById(R.id.hometoolbar);
            setSupportActionBar(toolbar);

            getSupportActionBar().setDisplayShowTitleEnabled(false);

            toolbar.setTitle("");
            toolbar.setSubtitle("");*/


            navigation = (BottomNavigationView) findViewById(R.id.navigation);
            disableShiftMode(navigation);

            navigation.getMenu().getItem(0).setChecked(true);


            if (navigation != null) {

                // Select first menu item by default and show Fragment accordingly.
                menu = navigation.getMenu();

                SingletonActivity.menu = menu;

                //=====================================================

//==========================================================================

/*
                if(SingletonActivity.isLivePriceClicked == true) {
                    selectFragment(menu.getItem(0));
                    System.out.println("IN HOME ACTIVITY==============================================22");
                }*/

                if(SingletonActivity.fromlivepricegraph == true) {
                    selectFragment(menu.getItem(0));

                }


                if(SingletonActivity.frombuycurrentenquiry == true)
                {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================1");
                }

                if (SingletonActivity.isPassedFromLogin == true) {
                    selectFragment(menu.getItem(3));
                    System.out.println("IN HOME ACTIVITY==============================================2");
                }

                if (SingletonActivity.backfromeditpersonaldetails == true) {
                    selectFragment(menu.getItem(3));
                    System.out.println("IN HOME ACTIVITY==============================================3");
                }

                if (SingletonActivity.backfromeditbusinessdetails == true) {
                    selectFragment(menu.getItem(3));
                    System.out.println("IN HOME ACTIVITY==============================================4");
                }

                if (SingletonActivity.backfromeditcontactdetails == true) {
                    selectFragment(menu.getItem(3));
                    System.out.println("IN HOME ACTIVITY==============================================5");
                }

                if (SingletonActivity.backfromeditinstalledcapacitydetails == true) {
                    selectFragment(menu.getItem(3));
                    System.out.println("IN HOME ACTIVITY==============================================6");
                }

                if (SingletonActivity.backfromeditreferencedetails == true) {
                    selectFragment(menu.getItem(3));
                    System.out.println("IN HOME ACTIVITY==============================================7");
                }

                if (SingletonActivity.updatedailylimitfromtrade == true) {
                    selectFragment(menu.getItem(1));
                    System.out.println("IN HOME ACTIVITY==============================================8");
                }

                if (SingletonActivity.setdailylimitfromtrade == true) {
                    selectFragment(menu.getItem(1));
                    System.out.println("IN HOME ACTIVITY==============================================9");
                }

                if (SingletonActivity.fromaddnewoffer == true) {
                    selectFragment(menu.getItem(1));
                    System.out.println("IN HOME ACTIVITY==============================================10");
                }

                if (SingletonActivity.fromupdatenewoffer == true) {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================11");
                }

                if (SingletonActivity.fromviewlivetrade == true) {
                    selectFragment(menu.getItem(1));
                    System.out.println("IN HOME ACTIVITY==============================================12");
                }

                if(SingletonActivity.frombackofsellerdispatch == true)
                {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================13");
                }


                if(SingletonActivity.frombackofsellerpayment == true)
                {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================14");
                }

                if(SingletonActivity.frombackofbuyerdispatch == true)
                {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================15");
                }


                if(SingletonActivity.frombackofbuyerpayment == true)
                {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================16");
                }

                if(SingletonActivity.isNewEnquiryClicked == true)
                {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================17");
                }

                if(SingletonActivity.isSellerAcceptedClicked == true) {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================18");
                }

                if(SingletonActivity.isSpTradeSellerClicked == true) {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================19");
                }

                if(SingletonActivity.isSpTradeBuyerClicked == true) {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================20");
                }

                if(SingletonActivity.isLiveSalesClicked == true) {
                    selectFragment(menu.getItem(1));
                    System.out.println("IN HOME ACTIVITY==============================================21");
                }

                if(SingletonActivity.fromsellerhistorywebview == true)
                {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================22");
                }

                if(SingletonActivity.frombuyerhistorywebview == true)
                {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================23");
                }

                if(SingletonActivity.fromselltodaysoffer == true)
                {
                    selectFragment(menu.getItem(2));
                    System.out.println("IN HOME ACTIVITY==============================================24");
                }

                if(SingletonActivity.enquiryfromhomepage == true)
                {
                    selectFragment(menu.getItem(0));
                    System.out.println("IN HOME ACTIVITY==============================================25");
                }
                if(SingletonActivity.enquiryfromplatespage == true)
                {
                    selectFragment(menu.getItem(1));
                    System.out.println("IN HOME ACTIVITY==============================================26");
                }

                if(SingletonActivity.fromviewpigiron == true)
                {
                    selectFragment(menu.getItem(1));
                    System.out.println("IN HOME ACTIVITY==============================================27");
                }



                Intent intent = getIntent();
                String action = intent.getAction();
                Uri datas = intent.getData();

               // Toast.makeText(HomeActivity.this,"DATAS ARE==="+ datas , Toast.LENGTH_SHORT).show();

                if(datas!=null)
                {
                    selectFragment(menu.getItem(0));
                }


                // Set action to perform when any menu-item is selected.
                navigation.setOnNavigationItemSelectedListener(
                        new BottomNavigationView.OnNavigationItemSelectedListener() {
                            @Override
                            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                                selectFragment(item);
                                return false;
                            }
                        });
                }


            if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                String NotificationCountUrl = APIName.URL + "/home/getNotificationNew?user_code=" + userCodestr;
                System.out.println("NOTIFICATION COUNT URL IS---" + NotificationCountUrl);
                NotificationCountAPI(NotificationCountUrl);

            } else {
                util.dialog(HomeActivity.this, "Please check your internet connection.");
            }



            ImageView notification_icon = (ImageView) findViewById(R.id.notification_icon);
            notification_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                  //  SingletonActivity.isNotificationClicked = true;
                    NotificationDialog();


                }
            });


        }
      //  Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(HomeActivity.this));


    }


               @Override
               public void onBackPressed()
               {
                   if(backButtonCount >= 1)
                   {


                       Intent intent = new Intent(Intent.ACTION_MAIN);
                       intent.addCategory(Intent.CATEGORY_HOME);
                       intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                       startActivity(intent);
                   }
                   else
                   {
                       Toast.makeText(this, "Press back button once again to exit from application", Toast.LENGTH_SHORT).show();
                       backButtonCount++;
                   }
               }


    public void RefreshPage() {
        handler = new Handler();
        timer = new Timer();

        doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {


                handler.post(new Runnable() {
                    public void run() {

                        if(!isRefresh)




                            try {

                                if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                                    String NotificationCountUrl = APIName.URL + "/home/getNotificationNew?user_code=" + userCodestr;
                                    System.out.println("NOTIFICATION COUNT REFRESH URL IS---" + NotificationCountUrl);
                                    NotificationCountAPI(NotificationCountUrl);

                                } else {
                                    util.dialog(HomeActivity.this, "Please check your internet connection.");
                                }






                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                            }
                    }
                });
            }
        };

        timer.schedule(doAsynchronousTask, 0, 5000); //execute in every 50000 ms*/

       /* Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // Actions to do after 10 seconds



                    if(!isRefresh)




                        try {

                            if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                                String NotificationCountUrl = APIName.URL + "/home/getNotificationNew?user_code=" + userCodestr;
                                System.out.println("NOTIFICATION COUNT REFRESH URL IS---" + NotificationCountUrl);
                                NotificationCountAPI(NotificationCountUrl);

                            } else {
                                util.dialog(HomeActivity.this, "Please check your internet connection.");
                            }






                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }

            }
        }, 5000);*/
    }

    public void NotificationDialog() {

        final Dialog mDialog = new Dialog(HomeActivity.this, R.style.AppBaseTheme);

        // Setting dialogview

        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(true);

        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.notification_dialog);
        Window window = mDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);


        mDialog.show();

        RelativeLayout notification = (RelativeLayout) mDialog.findViewById(R.id.notification);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

       // todays_offer_count = (TextView) mDialog.findViewById(R.id.todays_offer_count);
        new_enquiry_count = (TextView) mDialog.findViewById(R.id.new_enquiry_count);
        seller_accepted_count = (TextView) mDialog.findViewById(R.id.seller_accepted_count);
        seller_rejected_count = (TextView) mDialog.findViewById(R.id.seller_rejected_count);
        sp_generated_count = (TextView) mDialog.findViewById(R.id.sp_generated_count);
        dispatch_count = (TextView) mDialog.findViewById(R.id.dispatch_count);
        payment_count =(TextView) mDialog.findViewById(R.id.payment_count);

        buyer_rejected_count = (TextView) mDialog.findViewById(R.id.buyer_rejected_count);

      //  RelativeLayout todays_offer_relative = (RelativeLayout) mDialog.findViewById(R.id.todays_offer_relative);
        RelativeLayout new_enquiry_relative = (RelativeLayout) mDialog.findViewById(R.id.new_enquiry_relative);
        RelativeLayout seller_accepted_relative = (RelativeLayout) mDialog.findViewById(R.id.seller_accepted_relative);
        RelativeLayout seller_rejected_relative = (RelativeLayout) mDialog.findViewById(R.id.seller_rejected_relative);
        RelativeLayout sp_generated_relative = (RelativeLayout) mDialog.findViewById(R.id.sp_generated_relative);
        RelativeLayout buyer_rejected_relative = (RelativeLayout) mDialog.findViewById(R.id.buyer_rejected_relative);
        RelativeLayout dispatch_relative = (RelativeLayout) mDialog.findViewById(R.id.dispatch_relative);
        RelativeLayout payment_relative = (RelativeLayout) mDialog.findViewById(R.id.payment_relative);


        if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
            String NotificationCountUrl = APIName.URL + "/home/getNotificationNew?user_code=" + userCodestr;
            System.out.println("NOTIFICATION COUNT URL IS---" + NotificationCountUrl);
            NotificationCountAPI(NotificationCountUrl);

        } else {
            util.dialog(HomeActivity.this, "Please check your internet connection.");
        }


        try {
            new_enquiry_count.setText(notify.getString("new_enquiry"));
            seller_accepted_count.setText(notify.getString("seller_accepted"));
            seller_rejected_count.setText(notify.getString("seller_rejected"));
            sp_generated_count.setText(notify.getString("buyer_accepted"));
            buyer_rejected_count.setText(notify.getString("buyer_rejected"));
            dispatch_count.setText(notify.getString("dispatch"));
            payment_count.setText(notify.getString("payment"));

        } catch (JSONException e) {
            e.printStackTrace();
        }



/*

            todays_offer_relative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if(!todays_offer_count.getText().toString().equalsIgnoreCase("0")) {

                        mDialog.dismiss();

                        commentStr = "new offer";
                        tradeStr = "live_trading";


                        if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                            String NotificationResetUrl = APIName.URL + "/home/changeAlertStatus";
                            System.out.println("NOTIFICATION RESET URL IS---" + NotificationResetUrl);
                            NotificationResetAPI(NotificationResetUrl, commentStr, tradeStr);

                        } else {
                            util.dialog(HomeActivity.this, "Please check your internet connection.");
                        }


                        //Go to live sales-----
                        SingletonActivity.fromselltodaysoffer = false;
                        selectFragment(menu.getItem(1));
                    }
                    else
                    {
                        mDialog.dismiss();
                    }


                }
            });
*/


        new_enquiry_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!new_enquiry_count.getText().toString().equalsIgnoreCase("0")) {

                    mDialog.dismiss();

                    commentStr = "new purchase enquiry";
                    tradeStr = "seller";


                    if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                        String NotificationResetUrl = APIName.URL + "/home/changeAlertStatus";
                        System.out.println("NOTIFICATION RESET URL IS---" + NotificationResetUrl);
                        NotificationResetAPI(NotificationResetUrl, commentStr, tradeStr);

                    } else {
                        util.dialog(HomeActivity.this, "Please check your internet connection.");
                    }



                    try {
                        if(SingletonActivity.notify.getString("ne_url").equalsIgnoreCase("seller-enquiry")) {

                            SingletonActivity.fromselltodaysoffer = false;
                            SingletonActivity.isSpTradeBuyerClicked = false;
                            SingletonActivity.isSpTradeSellerClicked = false;

                            SingletonActivity.isNewEnquiryClicked = true;
                            selectFragment(menu.getItem(2));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    mDialog.dismiss();
                }


            }
        });

        seller_accepted_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!seller_accepted_count.getText().toString().equalsIgnoreCase("0")) {

                    mDialog.dismiss();

                    commentStr = "has Accepted Trade Id";
                    tradeStr = "buyer";

                    if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                        String NotificationResetUrl = APIName.URL + "/home/changeAlertStatus";
                        System.out.println("NOTIFICATION RESET URL IS---" + NotificationResetUrl);
                        NotificationResetAPI(NotificationResetUrl, commentStr, tradeStr);

                    } else {
                        util.dialog(HomeActivity.this, "Please check your internet connection.");
                    }




                    try {
                        if(SingletonActivity.notify.getString("sa_url").equalsIgnoreCase("buyer-enquiry")) {


                            SingletonActivity.fromselltodaysoffer = false;
                            SingletonActivity.isSpTradeBuyerClicked = false;
                            SingletonActivity.isSpTradeSellerClicked = false;
                            SingletonActivity.isSellerAcceptedClicked = true;
                            selectFragment(menu.getItem(2));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    mDialog.dismiss();
                }



            }
        });

        seller_rejected_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!seller_rejected_count.getText().toString().equalsIgnoreCase("0")) {

                    mDialog.dismiss();

                    commentStr = "rejected";
                    tradeStr = "buyer";

                    if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                        String NotificationResetUrl = APIName.URL + "/home/changeAlertStatus";
                        System.out.println("NOTIFICATION RESET URL IS---" + NotificationResetUrl);
                        NotificationResetAPI(NotificationResetUrl, commentStr, tradeStr);

                    } else {
                        util.dialog(HomeActivity.this, "Please check your internet connection.");
                    }




                    try {
                        if(SingletonActivity.notify.getString("sr_url").equalsIgnoreCase("buyer-enquiry")) {


                            SingletonActivity.fromselltodaysoffer = false;
                            SingletonActivity.isSpTradeBuyerClicked = false;
                            SingletonActivity.isSpTradeSellerClicked = false;
                            SingletonActivity.isSellerRejectedClicked = true;
                            selectFragment(menu.getItem(2));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    mDialog.dismiss();
                }



            }
        });

        sp_generated_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!sp_generated_count.getText().toString().equalsIgnoreCase("0")) {
                    mDialog.dismiss();



                    //=========== if trade == buyer==========


                    try {
                        if(SingletonActivity.notify.getString("ba_url").equalsIgnoreCase("buyer-deals")) {


                            SingletonActivity.isSpTradeBuyerClicked = true;
                            SingletonActivity.fromselltodaysoffer = false;


                            commentStr = "sauda patrak";
                            tradeStr = "buyer";

                            if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                                String NotificationResetUrl = APIName.URL + "/home/changeAlertStatus";
                                System.out.println("NOTIFICATION RESET URL IS---" + NotificationResetUrl);
                                NotificationResetAPI(NotificationResetUrl, commentStr, tradeStr);

                            } else {
                                util.dialog(HomeActivity.this, "Please check your internet connection.");
                            }
                        }

                    //=========== if trade == seller==========

                        if(SingletonActivity.notify.getString("ba_url").equalsIgnoreCase("seller-deals")) {

                        SingletonActivity.isSpTradeSellerClicked = true;
                        SingletonActivity.fromselltodaysoffer = false;

                        commentStr = "sauda patrak";
                        tradeStr = "seller";

                        if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                            String NotificationResetUrl = APIName.URL + "/home/changeAlertStatus";
                            System.out.println("NOTIFICATION RESET URL IS---" + NotificationResetUrl);
                            NotificationResetAPI(NotificationResetUrl, commentStr, tradeStr);

                        } else {
                            util.dialog(HomeActivity.this, "Please check your internet connection.");
                        }
                    }
                  //  SingletonActivity.fromselltodaysoffer = false;
                    selectFragment(menu.getItem(2));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                else
                {
                    mDialog.dismiss();
                }


            }
        });


        dispatch_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(!dispatch_count.getText().toString().equalsIgnoreCase("0")) {

                    mDialog.dismiss();

                    try {
                        commentStr = SingletonActivity.notify.getString("dispatch_comment");

                        String string = SingletonActivity.notify.getString("dis_url");
                        String[] parts = string.split("-");
                        String part1 = parts[0]; // 004

                        tradeStr = part1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                        String NotificationResetUrl = APIName.URL + "/home/changeAlertStatus";
                        System.out.println("NOTIFICATION RESET URL IS---" + NotificationResetUrl);
                        NotificationResetAPI(NotificationResetUrl, commentStr, tradeStr);

                    } else {
                        util.dialog(HomeActivity.this, "Please check your internet connection.");
                    }



                    try {

                        if(SingletonActivity.notify.getString("dis_url").equalsIgnoreCase("seller-deals")) {


                              Intent i = new Intent(HomeActivity.this,SellerDispatchActivity.class);
                              SingletonActivity.tradeid = SingletonActivity.notify.getString("trade_id");
                               i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                               startActivity(i);




                        }

                        if(SingletonActivity.notify.getString("dis_url").equalsIgnoreCase("buyer-deals")) {

                            Intent i = new Intent(HomeActivity.this,BuyerDispatchActivity.class);
                            SingletonActivity.tradeid = SingletonActivity.notify.getString("trade_id");
                            SingletonActivity.splited = "not split";
                            SingletonActivity.buyParent = "parent";
                            i.putExtra("splitted",SingletonActivity.splited);
                            i.putExtra("buyParent",SingletonActivity.buyParent);
                            SingletonActivity.usercode = userCodestr;
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);

                        }

                        if(SingletonActivity.notify.getString("transport").equalsIgnoreCase("3")) {

                            Intent i = new Intent(HomeActivity.this,SellerDispatchActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    mDialog.dismiss();
                }


            }
        });

        payment_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(!payment_count.getText().toString().equalsIgnoreCase("0")) {

                    mDialog.dismiss();

                    try {
                        commentStr = SingletonActivity.notify.getString("payment_comment");

                        String string = SingletonActivity.notify.getString("pay_url");
                        String[] parts = string.split("-");
                        String part1 = parts[0]; // 004

                        tradeStr = part1;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                        String NotificationResetUrl = APIName.URL + "/home/changeAlertStatus";
                        System.out.println("NOTIFICATION RESET URL IS---" + NotificationResetUrl);
                        NotificationResetAPI(NotificationResetUrl, commentStr, tradeStr);

                    } else {
                        util.dialog(HomeActivity.this, "Please check your internet connection.");
                    }



                    try {

                        if(SingletonActivity.notify.getString("pay_url").equalsIgnoreCase("seller-deals")) {

                            Intent i = new Intent(HomeActivity.this,SellerPaymentDetailsActivity.class);
                            SingletonActivity.tradeid = SingletonActivity.notify.getString("trade_id");
                            SingletonActivity.fromhometopayment = true;


                           JSONArray seller_trade_jsonArray = SingletonActivity.notify.getJSONArray("seller_trade");
                            SingletonActivity.sellid = seller_trade_jsonArray.getJSONObject(0).getString("sell_id");
                            SingletonActivity.buyerusercode = seller_trade_jsonArray.getJSONObject(0).getString("buyer_user_code");
                            SingletonActivity.buyermobilenum = seller_trade_jsonArray.getJSONObject(0).getString("buyer_mobile_num");
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);

                        }

                        if(SingletonActivity.notify.getString("pay_url").equalsIgnoreCase("buyer-deals")) {

                            Intent i = new Intent(HomeActivity.this,BuyerPaymentDetailsActivity.class);
                            SingletonActivity.tradeid = SingletonActivity.notify.getString("trade_id");
                            SingletonActivity.splited = "not split";
                            SingletonActivity.buyParent = "parent";
                            i.putExtra("splitted",SingletonActivity.splited);
                            i.putExtra("buyParent",SingletonActivity.buyParent);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);



                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    mDialog.dismiss();
                }



            }
        });




        buyer_rejected_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(!buyer_rejected_count.getText().toString().equalsIgnoreCase("0")) {

                    mDialog.dismiss();

                    commentStr = "rejected";
                    tradeStr = "seller";


                    if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                        String NotificationResetUrl = APIName.URL + "/home/changeAlertStatus";
                        System.out.println("NOTIFICATION RESET URL IS---" + NotificationResetUrl);
                        NotificationResetAPI(NotificationResetUrl, commentStr, tradeStr);

                    } else {
                        util.dialog(HomeActivity.this, "Please check your internet connection.");
                    }




                    try {
                        if(SingletonActivity.notify.getString("br_url").equalsIgnoreCase("seller-enquiry")) {


                            //Go to Seller Enquiry-----
                            SingletonActivity.fromselltodaysoffer = false;
                            SingletonActivity.isSpTradeBuyerClicked = false;
                            SingletonActivity.isSpTradeSellerClicked = false;
                            SingletonActivity.isIsBuyerRejectedClicked = true;

                            selectFragment(menu.getItem(2));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    mDialog.dismiss();
                }


            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        isRefresh = false;
        //      freeMemory();
        RefreshPage();

        if(SingletonActivity.fromlivepricegraph == true) {
            selectFragment(menu.getItem(0));

        }


        if (SingletonActivity.fromaddnewoffer == true) {
            selectFragment(menu.getItem(1));

        }

        if(SingletonActivity.fromsellerhistorywebview == true)
        {
            selectFragment(menu.getItem(2));

        }

        if(SingletonActivity.frombuyerhistorywebview == true)
        {
            selectFragment(menu.getItem(2));

        }


        if (SingletonActivity.fromupdatenewoffer == true) {
            selectFragment(menu.getItem(2));

        }

        if(SingletonActivity.isNewEnquiryClicked == true)
        {
            selectFragment(menu.getItem(2));

        }



        if (SingletonActivity.fromviewlivetrade == true) {
            selectFragment(menu.getItem(1));

        }

        if(SingletonActivity.frombackofbuyerdispatch == true || SingletonActivity.frombackofsellerdispatch == true || SingletonActivity.frombackofbuyerpayment == true)
        {
            selectFragment(menu.getItem(2));

        }

        if(SingletonActivity.fromselltodaysoffer == true)
        {
            selectFragment(menu.getItem(2));

        }

        if (SingletonActivity.isPassedFromLogin == true) {
            selectFragment(menu.getItem(3));

        }




//======================



    }

    @Override
    protected void onPause() {
        super.onPause();

        isRefresh = true;
        // freeMemory();
        //  handler.sendEmptyMessageDelayed(0,0000);
        //            System.out.println("APP ON PAUSE" + isRefresh);

    }


    @Override
    protected void onStop() {
        super.onStop();

        isRefresh = true;
        //    freeMemory();
        //            System.out.println("APP ON STOP" + isRefresh);
        // handler.sendEmptyMessageDelayed(0,0000);

    }




    private void NotificationCountAPI(String url) {

        String tag_json_obj = "json_obj_req";

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(HomeActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF NOTIFICATION COUNT API IS---" + response);

                        JSONObject NotificationCountJson = null;

                        try {
                            NotificationCountJson = new JSONObject(response);

                            String statusstr = NotificationCountJson.getString("status");
                            System.out.println("STATUS OF NOTIFICATION COUNT API IS---" + statusstr);

                            if (statusstr.equalsIgnoreCase("true")) {

                                 notify = NotificationCountJson.getJSONObject("notify");
                                 SingletonActivity.notify = notify;

                                String totalcountstr = notify.getString("total");
                              //  String todayofferstr = notify.getString("today_offer");
                                String newenquirystr = notify.getString("new_enquiry");
                                String selleracceptedstr = notify.getString("seller_accepted");
                                String sellerrejectedstr = notify.getString("seller_rejected");
                                String buyeracceptedstr = notify.getString("buyer_accepted");
                                String buyerrejectedstr = notify.getString("buyer_rejected");



//                                SingletonActivity.trader = notify.getString("trader");
                               // Toast.makeText(HomeActivity.this,"TRADER IS=="+ SingletonActivity.trader,Toast.LENGTH_SHORT).show();

                                if (totalcountstr.equalsIgnoreCase("0")) {
                                    System.out.println("TOTAL COUNT IF---" + totalcountstr);
                                    notification_total_count.setVisibility(View.GONE);
                                } else {
                                    System.out.println("TOTAL COUNT ELSE---" + totalcountstr);
                                    notification_total_count.setVisibility(View.VISIBLE);
                                }

                                System.out.println("TODAY'S OFFER COUNT 1---" + SingletonActivity.isNotificationClicked);

                                notification_total_count.setText(totalcountstr);

                            //    if (SingletonActivity.isNotificationClicked == true) {

                                    /*if (!todayofferstr.equalsIgnoreCase("")) {
                                        System.out.println("TODAY'S OFFER COUNT 2---" + todayofferstr);
                                        todays_offer_count.setText(todayofferstr);
                                    }*/
                                //   if (newenquirystr.equalsIgnoreCase("")) {
                                       // Log.e("TAG","NEW ENQUIRY STR IS--"+ newenquirystr);
                                      //  new_enquiry_count.setText(newenquirystr);
                                //  }
                                  //  if (!selleracceptedstr.equalsIgnoreCase("")) {
                                      //  seller_accepted_count.setText(notify.getString("seller_accepted"));
                                 //   }
                                 //   if (!sellerrejectedstr.equalsIgnoreCase("")) {
                                     //   seller_rejected_count.setText(notify.getString("seller_rejected"));
                                //    }

                                 //   if (!buyeracceptedstr.equalsIgnoreCase("")) {
                                     //   sp_generated_count.setText(notify.getString("buyer_accepted"));
                                //    }

                                 //   if (!buyerrejectedstr.equalsIgnoreCase("")) {
                                        //buyer_rejected_count.setText(notify.getString("buyer_rejected"));
                                 //   }
                              //  }


                                //  NotificationDialog(totalcountstr,todayofferstr,newenquirystr,selleracceptedstr,sellerrejectedstr,buyeracceptedstr,buyerrejectedstr);


                            } else {
                                Toast.makeText(HomeActivity.this, NotificationCountJson.getString("message"), Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(HomeActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            util.dialog(HomeActivity.this, "Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                System.out.println("notification count params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this,hurlStack);
        requestQueue.add(stringRequest);

      //  SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);

    }



    private void NotificationResetAPI(String url, final String commentStr, final String tradeStr) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(HomeActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF NOTIFICATION RESET API IS---" + response);

                        JSONObject NotificationResetJson = null;

                        try {
                            NotificationResetJson = new JSONObject(response);

                            String statusstr = NotificationResetJson.getString("status");
                            System.out.println("STATUS OF NOTIFICATION RESET API IS---" + statusstr);

                            if (statusstr.equalsIgnoreCase("true")) {

                                notification_total_count.setVisibility(View.GONE);

                                // Toast.makeText(HomeActivity.this,NotificationResetJson.getString("message"),Toast.LENGTH_SHORT).show();

                            } else {
                                // Toast.makeText(HomeActivity.this,NotificationResetJson.getString("message"),Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(HomeActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            util.dialog(HomeActivity.this, "Some Error Occured, please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_code", userCodestr);
                params.put("comment", commentStr);
                params.put("trader", tradeStr);
                System.out.println("notification reset params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }



    /**
     * Perform action when any item is selected.
     *
     * @param item Item that is selected.
     */
    public void selectFragment(MenuItem item) {

        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.menu_home:
                // Action to perform when Home Menu item is selected.
                pushFragment(new HomeFragment());

                break;

            case R.id.menu_trade:

                clickedcount = clickedcount + 1;
                SingletonActivity.clickedcount = clickedcount;
                // Action to perform when Account Menu item is selected.
               // pushFragment(new TradeFragment());
                pushFragment(new TradeFragment111119());

                break;

            case R.id.menu_transaction:
                // Action to perform when Bag Menu item is selected.
                pushFragment(new TransactionFragment());



                break;

            case R.id.menu_profile:

                // Action to perform when Account Menu item is selected.
                pushFragment(new ProfileFragment());


                break;
         /*   case R.id.menu_more:
                // Action to perform when Account Menu item is selected.
                pushFragment(new MoreFragment());
                break;*/
        }
    }


    /**
     * Method to push any fragment into given id.
     *
     * @param fragment An instance of Fragment to show into the given id.
     */
    public void pushFragment(androidx.fragment.app.Fragment fragment) {
        if (fragment == null)
            return;

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            if (ft != null) {

                ft.replace(R.id.rootLayout, fragment);
                ft.addToBackStack(null); //this will add it to back stack
             //   ft.commit();
                ft.commitAllowingStateLoss();
            }

        }
    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
        // return true;
    }
*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                System.out.println("OVERFLOWMENU ITEM 1 CLICKED");

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("login",false);


            /*    editor.clear();
                editor.commit();
*/
                sessionManager.logoutUser(mobile_num,fcm_id);


                // sessionManager.logoutUser();

            /*    Intent i = new Intent(LeaveManagementActivity.this,LoginActivity.class);
                startActivity(i);
*/
                return true;
          /*  case R.id.overflowmenuitem2:
                System.out.println("OVERFLOWMENU ITEM 2 CLICKED");

                return true;
            case R.id.overflowmenuitem3:
                System.out.println("OVERFLOWMENU ITEM 3 CLICKED");

                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
              //  item.setShiftingMode(false);
                view.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
                // set once again checked value, so view will be updated

                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static String encodeByteArrayIntoBase64String(byte[] bytes)
    {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("" +
                ""+ requestCode);

        //Toast.makeText(HomeActivity.this,"requestCode TAKEN IS==="+ requestCode,Toast.LENGTH_LONG).show();

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedFileURI = data.getData();
                System.out.println("SELECTED FILE PATH IS--"+ selectedFileURI);
                if(selectedFileURI!=null)
                {
                    String filePath = selectedFileURI.toString();


                    try{
                        InputStream iStream =   getContentResolver().openInputStream(selectedFileURI);
                        byte[] byteArray = getBytes(iStream);
                        attachmentstr = encodeByteArrayIntoBase64String(byteArray);

                        ContentResolver cR = this.getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        String type = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));

                        System.out.println("extension is------"+ type);

                        // attachmentstr = "data:application/"+type+"; base64,"+attachmentstr+"&user_code="+userCodestr;
                        attachmentstr = "data:application/"+type+"; base64,"+attachmentstr;

                        System.out.println("BASE 64 STRING------"+ attachmentstr);

                       SingletonActivity.selectedFileURI = selectedFileURI;



                        if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                            String insertimageurl = APIName.URL + "/user/insertUpdateImage";
                            System.out.println("INSERT IMAGE URL IS---" + insertimageurl);
                            InsertImageAPI(insertimageurl,attachmentstr);

                        } else {
                            util.dialog(HomeActivity.this, "Please check your internet connection.");
                        }

                    }
                    catch (IOException e)
                    {
                        Toast.makeText(getBaseContext(),"An error occurred with file chosen",Toast.LENGTH_SHORT).show();
                    }



                }



            }

        }



        if (requestCode == 67424) {
            if (resultCode == Activity.RESULT_OK) {


                Bitmap photo = (Bitmap) data.getExtras().get("data");



                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                Uri tempUri = getImageUri(HomeActivity.this, photo);

                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));

                System.out.println("CAPTURED URI FROM IMAGE==="+ tempUri);

                Uri  selectedFileURI = tempUri;

                if(selectedFileURI!=null) {
                    String filePath = selectedFileURI.toString();

                    File f = new File(filePath);
                    String imageName = f.getName();


                    String uriString = selectedFileURI.toString();
                    if (uriString.startsWith("content://")) {
                        Cursor cursor = null;
                        try {
                            cursor = HomeActivity.this.getContentResolver().query(selectedFileURI, null, null, null, null);
                            if (cursor != null && cursor.moveToFirst()) {
                                String displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                System.out.println("displayName 1 is------" + displayName);
                                // uploadresumetxt.setText(displayName);
                                ContentResolver cR = this.getContentResolver();
                                MimeTypeMap mime = MimeTypeMap.getSingleton();
                                types = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));
                               // Toast.makeText(HomeActivity.this,"In content:// loop TYPE=="+ type,Toast.LENGTH_LONG).show();
                            }
                        } finally {
                            cursor.close();
                        }
                    } /*else if (uriString.startsWith("file://")) {
                        String string = imageName;
                        String[] parts = string.split("%3A");
                        String part1 = parts[0]; // 004
                        String part2 = parts[1]; // 034556

                        ContentResolver cR = this.getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        String type = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));

                        String imageNames = part1 + " " + part2 + "." + type;
                        Toast.makeText(HomeActivity.this,"In file:// loop+",Toast.LENGTH_LONG).show();

                    }*/


                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 70, stream);

                    String imgString = Base64.encodeToString(stream.toByteArray(),
                            Base64.NO_WRAP);

                  // Toast.makeText(HomeActivity.this,"IMAGE TAKEN IS==="+ imgString,Toast.LENGTH_LONG).show();


                    attachmentstr = imgString;

                   attachmentstr = "data:application/"+types+"; base64,"+attachmentstr;

                    System.out.println("BASE 64 STRING------"+ attachmentstr);

                    SingletonActivity.selectedFileURI = selectedFileURI;

                    ProfileFragment profileFragment = new ProfileFragment();
                    profileFragment.profileiv = (ImageView)findViewById(R.id.profileiv);

                    Bitmap circleBitmap = Bitmap.createBitmap(photo.getWidth(), photo.getHeight(), Bitmap.Config.ARGB_8888);

                    BitmapShader shader = new BitmapShader (photo,  Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                    Paint paint = new Paint();
                    paint.setShader(shader);
                    paint.setAntiAlias(true);
                    Canvas c = new Canvas(circleBitmap);
                    c.drawCircle(photo.getWidth()/2, photo.getHeight()/2, photo.getWidth()/2, paint);


                    profileFragment.profileiv.setImageBitmap(photo);



                    if (NetworkUtility.checkConnectivity(HomeActivity.this)) {
                        String insertimageurl = APIName.URL + "/user/insertUpdateImage";
                        System.out.println("INSERT IMAGE URL IS---" + insertimageurl);
                        InsertImageAPI(insertimageurl,attachmentstr);

                    } else {
                        util.dialog(HomeActivity.this, "Please check your internet connection.");
                    }
                }



            }

        }



    }

        public Uri getImageUri(Context inContext, Bitmap inImage) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            //  String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);

            ContentValues values=new ContentValues();
            values.put(MediaStore.Images.Media.TITLE,"Title");
            values.put(MediaStore.Images.Media.DESCRIPTION,"From Camera");
            Uri path=getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,values);

            return path;
        }

        public String getRealPathFromURI(Uri uri) {
            String path = "";
            if (getContentResolver() != null) {
                Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    path = cursor.getString(idx);
                    cursor.close();
                }
            }
            return path;
        }

    private void InsertImageAPI(String url, final String attachment) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(HomeActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF INSERT IMAGE API IS---" + response);

                     //   {"status":"UPDATE","message":"Profile Details Updated Successfully."}

                        JSONObject InsertImageJson = null;

                        try {
                            InsertImageJson = new JSONObject(response);


                            String statusstr = InsertImageJson.getString("status");

                            if (statusstr.equalsIgnoreCase("UPDATE")) {
                                //String messagestr = InsertImageJson.getString("message");
                                selectFragment(menu.getItem(3));
                              Toast.makeText(HomeActivity.this,"Image uploaded successfully",Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(HomeActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(HomeActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_code",userCodestr);
                params.put("attachment",attachment);
               // params.put("user_id",useridstr);

                System.out.println("insert update image params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }



}
