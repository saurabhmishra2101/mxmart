package com.smtc.mxmart;

/**
 * Created by 10161 on 11/2/2017.
 */

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.pushbots.push.Pushbots;
import com.smtc.mxmart.util.Config;


public class SplashActivity extends Activity {

    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 5000;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    boolean login;
    String regId;
    BroadcastReceiver mRegistrationBroadcastReceiver;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        login = prefs.getBoolean("login", false);

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {


                if(login == true)
                {
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    SingletonActivity.isPassedFromLogin = true;
                    SplashActivity.this.startActivity(i);
                    SplashActivity.this.finish();
                }
                else
                {

                    mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {

                            // checking for type intent filter
                            if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                                // gcm successfully registered
                                // now subscribe to `global` topic to receive app wide notifications
                                FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);


                                displayFirebaseRegId();

                            } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                                // new push notification is received

                                String message = intent.getStringExtra("message");

                                Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                                //txtMessage.setText(message);
                            }
                        }
                    };

                    displayFirebaseRegId();




                    Pushbots.sharedInstance().registerForRemoteNotifications();

                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                }

            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);

        if (!TextUtils.isEmpty(regId)) {
            // txtRegId.setText("Firebase Reg Id: " + regId);
      //      SingletonActivity.fcmid = regId;
            System.out.println("Firebase Reg Id in Login--:" + regId);
        }
        else
            // txtRegId.setText("Firebase Reg Id is not received yet!");
            System.out.println("Firebase Reg Id is not received yet!");
    }

}
