package com.smtc.mxmart;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;


import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class EditPersonalDetailsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    ImageView backiv;
    String[] SPINNERLIST = {"Sole Proprietorship", "Partnership", "Public/Private Ltd"};
    MaterialBetterSpinner materialDesignSpinner;
    String etDesignationstr,etCompanyNamestr,selectedspinnerstr,dobstr,lastnamepersonalstr,dobpersonaldetstr,addresspersonaldetstr,landmarkpersonaldetstr,citypersonaldetstr;
    int mYear,mMonth,mDay;
    UtilsDialog util = new UtilsDialog();
    ProgressDialog pdia;
    Typeface  source_sans_pro_normal;
    String changepwdurl;
    String userCodestr,selectedcity;
    String etLandlinestr,etStatestr,etDistrictstr,etZipPostalCodestr,firstnamepersonalstr,emailidpersonalstr,mobilenumpersonaldetstr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    EditText etCompanyName,etDesignation,etLandline,etZipPostalCode,etDistrict,firstnamepersonaldet,lastnamepersonaldet,emailidpersonaldet,mobilenumpersonaldet,landmarkpersonaldet;

    AutoCompleteTextView addresspersonaldet,citypersonaldet,etState;
    static  EditText dobpersonaldet;
    DatePickerFragment newFragment;
    SessionManager sessionManager;
    private static final String LOG_TAG = "ExampleApp";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String API_KEY = "AIzaSyCPffetd-dKlj51pNBPrDphhcnipwwS2Zo";

    ArrayList<String> cityal = new ArrayList<String>();
    ArrayList<String> stateal = new ArrayList<String>();
    String pwdstr,mobilenumstr;
    String mobile_num,fcm_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_personal_details);



        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        pwdstr = prefs.getString("password", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        mobile_num = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id",null);


      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.editpersonaldetailstoolbar);
        setSupportActionBar(toolbar);*/

        sessionManager = new SessionManager(getApplicationContext());

        System.out.println("ST USER JSON IN EDIT PERSONAL DETAILS FRAGMENT---"+ SingletonActivity.stuserjson);
        System.out.println("ST USER PROF JSON IN EDIT PERSONAL DETAILS FRAGMENT---"+ SingletonActivity.stuserprofjson);

        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

     
        firstnamepersonaldet = (EditText)findViewById(R.id.firstnameedt);
        firstnamepersonaldet.setTypeface(source_sans_pro_normal);


        lastnamepersonaldet = (EditText)findViewById(R.id.lastnameedt);
        lastnamepersonaldet.setTypeface(source_sans_pro_normal);




        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(EditPersonalDetailsActivity.this,
                android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        materialDesignSpinner = (MaterialBetterSpinner)
                findViewById(R.id.android_material_design_spinner);
        materialDesignSpinner.setAdapter(arrayAdapter);


        materialDesignSpinner.setTypeface(source_sans_pro_normal);

        materialDesignSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedspinnerstr = parent.getItemAtPosition(position).toString();


            }
        });

        emailidpersonaldet = (EditText)findViewById(R.id.etEmailIdPersonal);
        emailidpersonaldet.setTypeface(source_sans_pro_normal);

        mobilenumpersonaldet = (EditText)findViewById(R.id.etMobileNoPersonal);
        mobilenumpersonaldet.setTypeface(source_sans_pro_normal);

        dobpersonaldet = (EditText)findViewById(R.id.etDobPersonal);
        dobpersonaldet.setTypeface(source_sans_pro_normal);

        dobpersonaldet.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");

             /*   Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                System.out.println("the selected " + mDay);
                DatePickerDialog dialog = new DatePickerDialog(getActivity(),
                        new mDateSetListener(), mYear, mMonth, mDay);
                dialog.show();*/

            }
        });

        dobpersonaldet.setFocusable(false);
        dobpersonaldet.setClickable(true);

        addresspersonaldet = (AutoCompleteTextView)findViewById(R.id.etAddressPersonal);
        addresspersonaldet.setTypeface(source_sans_pro_normal);

        addresspersonaldet.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.google_places_list));
        addresspersonaldet.setOnItemClickListener(EditPersonalDetailsActivity.this);

        landmarkpersonaldet = (EditText)findViewById(R.id.landmarkedt);
        landmarkpersonaldet.setTypeface(source_sans_pro_normal);

        citypersonaldet = (AutoCompleteTextView)findViewById(R.id.cityedt);
        citypersonaldet.setTypeface(source_sans_pro_normal);



        etZipPostalCode = (EditText)findViewById(R.id.etZipPostalCode);
        etZipPostalCode.setTypeface(source_sans_pro_normal);

        etDistrict = (EditText)findViewById(R.id.etDistrict);
        etDistrict.setTypeface(source_sans_pro_normal);

        etState = (AutoCompleteTextView)findViewById(R.id.etState);
        etState.setTypeface(source_sans_pro_normal);

        etLandline = (EditText)findViewById(R.id.etLandline);
        etLandline.setTypeface(source_sans_pro_normal);

        etCompanyName = (EditText)findViewById(R.id.etCompanyName);
        etCompanyName.setTypeface(source_sans_pro_normal);

        etDesignation = (EditText)findViewById(R.id.etDesignation);
        etDesignation.setTypeface(source_sans_pro_normal);

        if(NetworkUtility.checkConnectivity(EditPersonalDetailsActivity.this)){
            String getlocationurl = APIName.URL+"/home/getLocation";
            System.out.println("GET LOCATION API IS---"+ getlocationurl);
            GetLocationAPI(getlocationurl);

        }
        else{
            util.dialog(EditPersonalDetailsActivity.this, "Please check your internet connection.");
        }




        if (SingletonActivity.stuserprofjson != null) {

            try {
                firstnamepersonaldet.setText(SingletonActivity.stuserjson.getString("first_name"));
                lastnamepersonaldet.setText(SingletonActivity.stuserjson.getString("last_name"));
                selectedspinnerstr = SingletonActivity.stuserprofjson.getString("company_type");
                if(!selectedspinnerstr.equalsIgnoreCase("null")) {
                    materialDesignSpinner.setText(selectedspinnerstr);
                }
                else
                {
                    materialDesignSpinner.setText("");
                }
                etCompanyName.setText(SingletonActivity.stuserprofjson.getString("company_name"));
                if(!SingletonActivity.stuserprofjson.getString("designation").equalsIgnoreCase("null")) {
                    etDesignation.setText(SingletonActivity.stuserprofjson.getString("designation"));
                }
                else
                {
                    etDesignation.setText("");
                }

                emailidpersonaldet.setText(SingletonActivity.stuserjson.getString("email"));
                mobilenumpersonaldet.setText(SingletonActivity.stuserjson.getString("mobile_num"));
                if(!SingletonActivity.stuserjson.getString("dob").equalsIgnoreCase("null")) {
                    dobpersonaldet.setText(SingletonActivity.stuserjson.getString("dob"));
                }
                addresspersonaldet.setText(SingletonActivity.stuserjson.getString("address"));
                landmarkpersonaldet.setText(SingletonActivity.stuserjson.getString("landmark"));
                citypersonaldet.setText(SingletonActivity.stuserjson.getString("city"));
                etZipPostalCode.setText(SingletonActivity.stuserjson.getString("pincode"));
                etDistrict.setText(SingletonActivity.stuserjson.getString("district"));
                etState.setText(SingletonActivity.stuserjson.getString("state"));
                etLandline.setText(SingletonActivity.stuserjson.getString("phone"));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }




        RelativeLayout sbmtrelative = (RelativeLayout)findViewById(R.id.sbmtrelative);
        sbmtrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstnamepersonalstr = firstnamepersonaldet.getText().toString();
                lastnamepersonalstr = lastnamepersonaldet.getText().toString();
                emailidpersonalstr = emailidpersonaldet.getText().toString();
                mobilenumpersonaldetstr = mobilenumpersonaldet.getText().toString();
                dobpersonaldetstr = dobpersonaldet.getText().toString();
                addresspersonaldetstr = addresspersonaldet.getText().toString();
                landmarkpersonaldetstr = landmarkpersonaldet.getText().toString();



                if(cityal.contains(citypersonaldet.getText().toString()))
                {
                    citypersonaldetstr = citypersonaldet.getText().toString();
                }
                else
                {
                    //citypersonaldetstr = "";
                    citypersonaldet.setText(""); // clear your TextView
                }

                etZipPostalCodestr = etZipPostalCode.getText().toString();
                etDistrictstr = etDistrict.getText().toString();

                if(stateal.contains(etState.getText().toString()))
                {
                    etStatestr = etState.getText().toString();
                }
                else
                {
                    etState.setText("");
                }

                etLandlinestr = etLandline.getText().toString();
                etCompanyNamestr = etCompanyName.getText().toString();
                etDesignationstr = etDesignation.getText().toString();


                System.out.println("Personal Details:--- "+ firstnamepersonalstr + "," + lastnamepersonalstr + "," + selectedspinnerstr + "," + emailidpersonalstr + "," + mobilenumpersonaldetstr + "," + dobpersonaldetstr + "," + addresspersonaldetstr + "," + landmarkpersonaldetstr + "," + citypersonaldetstr);

                SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                userCodestr = prefs.getString("user_code", null);
                System.out.println("USER CODE IN EDIT PERSONAL DETAILS FRAGMENT---"+ userCodestr);


                boolean invalid = false;

                if (firstnamepersonalstr.trim().equals("")) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this, "Please Enter First Name",
                            Toast.LENGTH_SHORT).show();
                }
                else  if (lastnamepersonalstr.trim().equals("")) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Enter Last Name", Toast.LENGTH_SHORT)
                            .show();
                }
                else  if (materialDesignSpinner.getText().toString().trim().equals("")) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Select Type of Organization", Toast.LENGTH_SHORT)
                            .show();
                }


                else  if (etCompanyName.getText().toString().trim().equals("")) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Enter Company Name", Toast.LENGTH_SHORT)
                            .show();
                }


                else  if (etDesignation.getText().toString().trim().equals("")) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Enter Designation", Toast.LENGTH_SHORT)
                            .show();
                }

                else  if (!isValidEmail(emailidpersonaldet.getText().toString().trim())) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Enter Valid Email", Toast.LENGTH_SHORT)
                            .show();


                }


                else  if (!isValidMobileNumber(mobilenumpersonaldet.getText().toString().trim())) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Enter Valid Mobile No.", Toast.LENGTH_SHORT)
                            .show();


                }

              /*  else  if (dobpersonaldet.getText().toString().equals("")) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Enter Date of Birth", Toast.LENGTH_SHORT)
                            .show();


                }*/

                else  if (addresspersonaldet.getText().toString().trim().equals("")) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Enter Address", Toast.LENGTH_SHORT)
                            .show();


                }

                else  if (citypersonaldet.getText().toString().trim().equals("")) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Enter City", Toast.LENGTH_SHORT)
                            .show();


                }

                else  if (!isValidPinNumber(etZipPostalCode.getText().toString().trim())) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Enter Valid Pin Code", Toast.LENGTH_SHORT)
                            .show();


                }

               /* else  if (etDistrict.getText().toString().equals("")) {
                    invalid = true;
                    Toast.makeText(getActivity(),
                            "Please Enter District", Toast.LENGTH_SHORT)
                            .show();


                }*/

                else  if (etState.getText().toString().trim().equals("")) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Enter State", Toast.LENGTH_SHORT)
                            .show();


                }

                else  if (!isValidLandlineNumber(etLandline.getText().toString().trim())) {
                    invalid = true;
                    Toast.makeText(EditPersonalDetailsActivity.this,
                            "Please Enter Valid Landline No.", Toast.LENGTH_SHORT)
                            .show();
                }

                else if (invalid == false) {


                    if (NetworkUtility.checkConnectivity(EditPersonalDetailsActivity.this)) {
                        String editpersonaldetailsurl = APIName.URL + "/user/insertUpdatePersonalDetails?user_code=" + userCodestr;
                        System.out.println("EDIT PERSONAL DETAILS URL IS---" + editpersonaldetailsurl);
                        EditPersonalProfileAPI(editpersonaldetailsurl, userCodestr);

                    } else {
                        util.dialog(EditPersonalDetailsActivity.this, "Please check your internet connection.");
                    }
                }

            }
        });

        RelativeLayout chngpwdrelative = (RelativeLayout)findViewById(R.id.chngpwdrelative);

        chngpwdrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("wantToCloseDialog", false);
                editor.commit();

                showForgetPwdDialog();
            }
        });


        backiv = (ImageView)findViewById(R.id.backicon);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(EditPersonalDetailsActivity.this,HomeActivity.class);

                SingletonActivity.fromviewlivetrade = false;
                SingletonActivity.fromaddnewoffer = false;
                SingletonActivity.isNotificationClicked = false;
                // SingletonActivity.index = index;
                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                SingletonActivity.FromAddNewOfferTabOne=false;
                SingletonActivity.FromAddNewOfferTabZero=false;
                SingletonActivity.isNewEnquiryClicked = false;
                SingletonActivity.frombackofbuyerdispatch = false;
                SingletonActivity.frombackofsellerdispatch = false;
                SingletonActivity.frombackofbuyerpayment = false;
                SingletonActivity.frombackofsellerpayment = false;
                SingletonActivity.fromselltodaysoffer = false;

                SingletonActivity.backfromeditpersonaldetails = true;
                startActivity(i);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();


        Intent i = new Intent(EditPersonalDetailsActivity.this,HomeActivity.class);

        SingletonActivity.fromviewlivetrade = false;
        SingletonActivity.fromaddnewoffer = false;
        SingletonActivity.isNotificationClicked = false;
        // SingletonActivity.index = index;
        SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
        SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
        SingletonActivity.FromAddNewOfferTabOne=false;
        SingletonActivity.FromAddNewOfferTabZero=false;
        SingletonActivity.isNewEnquiryClicked = false;
        SingletonActivity.frombackofbuyerdispatch = false;
        SingletonActivity.frombackofsellerdispatch = false;
        SingletonActivity.frombackofbuyerpayment = false;
        SingletonActivity.frombackofsellerpayment = false;
        SingletonActivity.fromselltodaysoffer = false;

        SingletonActivity.backfromeditpersonaldetails = true;
        startActivity(i);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        String str = (String) adapterView.getItemAtPosition(position);
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            //sb.append("&components=country:in");
            sb.append("&components=");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: "+url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());
            return dialog;


        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user

            String daynew ="";
            String monthnew ="";

            if(day<10)
            {
                daynew = "0"+Integer.toString(day);
            }
            else
            {
                daynew = Integer.toString(day);
            }

            if(month+1<10)
            {
                monthnew = "0"+ Integer.toString(month+1);
            }
            else
            {
                monthnew = Integer.toString(month+1);
            }


            dobpersonaldet.setText(daynew + "/"
                    + monthnew + "/" + year);




        }
    }


    public  final boolean isValidLandlineNumber(String landline) {
        if (landline.length() < 10 || landline.length() > 12) {

            Toast.makeText(EditPersonalDetailsActivity.this,
                    "Please Enter Valid Landline No.", Toast.LENGTH_SHORT)
                    .show();

            return false;
        } else {
            return true;
        }
    }


    public  final boolean isValidPinNumber(String pin) {
        if (pin.length()!=6) {

            Toast.makeText(EditPersonalDetailsActivity.this,
                    "Please Enter Valid Pin Code", Toast.LENGTH_SHORT)
                    .show();

            return false;
        } else {
            return true;
        }
    }

    private void GetLocationAPI(String url) {
        pdia = new ProgressDialog(EditPersonalDetailsActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(EditPersonalDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        cityal.clear();
                        stateal.clear();

                        System.out.println("RESPONSE OF GET LOCATION API IS---" + response);
                        final JSONArray jsonArray;
                        try {
                            //jsonArray = new JSONArray(response);


                            JSONObject getlocationjson = new JSONObject(response);
                            System.out.println("GET LOCATION JSON IS---" + getlocationjson);

                            String statusstr = getlocationjson.getString("status");
                            String msgstr = getlocationjson.getString("message");

                            JSONArray locationJSONArray = getlocationjson.getJSONArray("location");
                            System.out.println("GET LOCATION JSON ARRAY IS---" + locationJSONArray);

                            for(int i = 0;i < locationJSONArray.length();i++)
                            {
                                String citystr = locationJSONArray.getJSONObject(i).getString("city");
                                String statestr = locationJSONArray.getJSONObject(i).getString("state");

                                cityal.add(citystr);
                                stateal.add(statestr);

                                HashSet hs = new HashSet();

                                hs.addAll(stateal); // demoArrayList= name of arrayList from which u want to remove duplicates

                                stateal.clear();
                                stateal.addAll(hs);



                            }

                            System.out.println("CITIES FROM LOCATION API ARE-----"+ cityal);
                            System.out.println("STATES FROM LOCATION API ARE-----"+ stateal);



                            ArrayAdapter<String> cityadapter = new ArrayAdapter<String>(EditPersonalDetailsActivity.this,android.R.layout.simple_list_item_1,cityal);
                            citypersonaldet.setThreshold(3);
                            citypersonaldet.setAdapter(cityadapter);

                            citypersonaldet.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                    selectedcity = adapterView.getItemAtPosition(i).toString();
                                    citypersonaldet.setText(selectedcity);
                                }
                            });





                            ArrayAdapter<String> stateadapter = new ArrayAdapter<String>(EditPersonalDetailsActivity.this,android.R.layout.simple_list_item_1,stateal);
                            etState.setAdapter(stateadapter);



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(EditPersonalDetailsActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(EditPersonalDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(EditPersonalDetailsActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                System.out.println("getlocation params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }

    public  final boolean isValidMobileNumber(String mobile) {
        if (mobile.length()!=10) {

            Toast.makeText(EditPersonalDetailsActivity.this,
                    "Please Enter Valid Mobile No.", Toast.LENGTH_SHORT)
                    .show();

            return false;
        } else {
            return true;
        }
    }

    public final  boolean isValidEmail(String email) {
        if (email == null) {
            Toast.makeText(EditPersonalDetailsActivity.this,
                    "Please Enter Valid Email", Toast.LENGTH_SHORT)
                    .show();

            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public void showForgetPwdDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EditPersonalDetailsActivity.this);
        LayoutInflater inflater = EditPersonalDetailsActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.change_pwd_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);


        final EditText currentpwdedttxt = (EditText) dialogView.findViewById(R.id.currentpwdedt);
        final EditText newpwdedttxt = (EditText) dialogView.findViewById(R.id.newpwdedt);
        final EditText reenterpwdedttxt = (EditText) dialogView.findViewById(R.id.reenterpwdedt);


        currentpwdedttxt.setTransformationMethod(new PasswordTransformationMethod());
        newpwdedttxt.setTransformationMethod(new PasswordTransformationMethod());
        reenterpwdedttxt.setTransformationMethod(new PasswordTransformationMethod());

        currentpwdedttxt.setTypeface(source_sans_pro_normal);
        newpwdedttxt.setTypeface(source_sans_pro_normal);
        reenterpwdedttxt.setTypeface(source_sans_pro_normal);

        currentpwdedttxt.setEnabled(true);
        newpwdedttxt.setEnabled(false);
        newpwdedttxt.setBackgroundColor(Color.parseColor("#E4E4E4"));
        reenterpwdedttxt.setEnabled(false);
        reenterpwdedttxt.setBackgroundColor(Color.parseColor("#E4E4E4"));

       dialogBuilder.setTitle("  Change Password");

        dialogBuilder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {



            }
        });



       final AlertDialog b = dialogBuilder.create();
        b.show();


        Button pbutton = b.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(Color.parseColor("#FF1BA46B"));

        b.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                String currentpwdstr = currentpwdedttxt.getText().toString();
                String newpwdstr = newpwdedttxt.getText().toString();
                String reenterpwdstr = reenterpwdedttxt.getText().toString();

                Boolean wantToCloseDialog = false;

                SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                wantToCloseDialog = prefs.getBoolean("wantToCloseDialog", false);


                //Do stuff, possibly set wantToCloseDialog to true then...
                if (wantToCloseDialog == false) {

                    //b.dismiss();


                    if (currentpwdstr.trim().length() > 0) {

                        System.out.println("CURRENT PASSWORD ENTERED===" + currentpwdstr);
                        System.out.println("CURRENT PASSWORD FROM SHAREDPREFERENCES===" + pwdstr);

                        if (currentpwdstr.trim().equals(pwdstr)) {
                            currentpwdedttxt.setEnabled(false);
                            newpwdedttxt.setEnabled(true);
                            newpwdedttxt.setBackgroundResource(R.drawable.rounded_edittext);
                            reenterpwdedttxt.setEnabled(true);
                            reenterpwdedttxt.setBackgroundResource(R.drawable.rounded_edittext);

                            wantToCloseDialog = true;

                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            editor.putBoolean("wantToCloseDialog", true);
                            editor.commit();

                        } else {
                            Toast.makeText(EditPersonalDetailsActivity.this, "Please Enter Correct Password", Toast.LENGTH_SHORT).show();

                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                            editor.putBoolean("wantToCloseDialog", false);
                            editor.commit();
                        }
                    } else {
                        Toast.makeText(EditPersonalDetailsActivity.this, "Please Enter Password", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    System.out.println("wantToCloseDialog in else===" + wantToCloseDialog);

                    if (newpwdstr.length() > 0) {

                        if (reenterpwdstr.length() > 0) {


                            if (newpwdstr.equals(reenterpwdstr)) {

                                if(NetworkUtility.checkConnectivity(EditPersonalDetailsActivity.this)){
                                    String updatePasswordurl = APIName.URL+"/user/updatePassword";
                                    System.out.println("UPDATE PASSWORD URL IS---"+ updatePasswordurl);
                                    updatePasswordAPI(updatePasswordurl,newpwdedttxt.getText().toString());

                                }
                                else{
                                    util.dialog(EditPersonalDetailsActivity.this, "Please check your internet connection.");
                                }



                            } else {
                                Toast.makeText(EditPersonalDetailsActivity.this, "Password not matching", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(EditPersonalDetailsActivity.this, "Please Re-Enter New Password", Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        Toast.makeText(EditPersonalDetailsActivity.this, "Please Enter New Password", Toast.LENGTH_SHORT).show();
                    }


                    //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
                }
            }
        });



    }

    private void updatePasswordAPI(String url,final String newpwd) {
        pdia = new ProgressDialog(EditPersonalDetailsActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(EditPersonalDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        System.out.println("RESPONSE OF UPDATE PASSWORD API IS---" + response);

                        //{"status":true,"message":"Password is successfully updated."}


                        try {



                            JSONObject updatepwdjson = new JSONObject(response);
                            System.out.println("UPDATE PASSWORD JSON IS---" + updatepwdjson);

                            String statusstr = updatepwdjson.getString("status");
                            String msgstr = updatepwdjson.getString("message");


                            if (statusstr.equalsIgnoreCase("true")) {

                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("password",newpwd);


                                editor.commit();


                                Toast.makeText(EditPersonalDetailsActivity.this,msgstr,Toast.LENGTH_SHORT).show();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {

                                        Intent i = new Intent(EditPersonalDetailsActivity.this, HomeActivity.class);
                                        SingletonActivity.backfromeditpersonaldetails = true;
                                        SingletonActivity.fromviewlivetrade = false;
                                        SingletonActivity.fromaddnewoffer = false;
                                        SingletonActivity.isNotificationClicked = false;
                                        // SingletonActivity.index = index;
                                        SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                        SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                        SingletonActivity.FromAddNewOfferTabOne=false;
                                        SingletonActivity.FromAddNewOfferTabZero=false;
                                        SingletonActivity.isNewEnquiryClicked = false;
                                        SingletonActivity.frombackofbuyerdispatch = false;
                                        SingletonActivity.frombackofsellerdispatch = false;
                                        SingletonActivity.frombackofbuyerpayment = false;
                                        SingletonActivity.frombackofsellerpayment = false;
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                            else
                            {

                                Toast.makeText(EditPersonalDetailsActivity.this,msgstr,Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(EditPersonalDetailsActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(EditPersonalDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(EditPersonalDetailsActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_num",mobilenumstr);
                params.put("password",newpwd);
                params.put("type","2");


                System.out.println("update password params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }



    private void EditPersonalProfileAPI(final String url,final String usercode) {
     /*   pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();*/

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(EditPersonalDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // pdia.dismiss();


                        System.out.println("EDIT PERSONAL DETAILS URL in resp IS---"+ url);
                        System.out.println("RESPONSE OF EDIT PERSONAL PROFILE API IS---" + response);

                        try {

                            JSONObject editpersonalprofilejson = new JSONObject(response);
                            System.out.println("EDIT PERSONAL JSON IS---" + editpersonalprofilejson);

                            String statusstr = editpersonalprofilejson.getString("status");
                            String msgstr = editpersonalprofilejson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Toast.makeText(EditPersonalDetailsActivity.this,msgstr,Toast.LENGTH_SHORT).show();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        SingletonActivity.backfromeditpersonaldetails = true;
                                        Intent i = new Intent(EditPersonalDetailsActivity.this, HomeActivity.class);
                                        startActivity(i);


                                    }
                                }, 2000);
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            //  pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditPersonalDetailsActivity.this,error.toString(),Toast.LENGTH_SHORT).show();
                        //     pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(EditPersonalDetailsActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(EditPersonalDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(EditPersonalDetailsActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("first_name",firstnamepersonalstr);
                params.put("last_name",lastnamepersonalstr);
                params.put("dob",dobpersonaldetstr);
                params.put("address",addresspersonaldetstr);
                params.put("landmark",landmarkpersonaldetstr);
                params.put("city",citypersonaldetstr);
                params.put("pincode",etZipPostalCodestr);
                params.put("mobile_num",mobilenumpersonaldetstr);
                params.put("district",etDistrictstr);
                params.put("state",etStatestr);
                params.put("phone",etLandlinestr);
                params.put("email",emailidpersonalstr);
                params.put("company_type",selectedspinnerstr);
                params.put("company_name",etCompanyNamestr);
                params.put("designation",etDesignationstr);




                System.out.println("edit personal details params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditPersonalDetailsActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
        //  return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                System.out.println("OVERFLOWMENU ITEM 1 CLICKED");

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("login",false);


                editor.clear();
                editor.commit();

                sessionManager.logoutUser(mobile_num,fcm_id);


                // sessionManager.logoutUser();

            /*    Intent i = new Intent(LeaveManagementActivity.this,LoginActivity.class);
                startActivity(i);
*/
                return true;
          /*  case R.id.overflowmenuitem2:
                System.out.println("OVERFLOWMENU ITEM 2 CLICKED");

                return true;
            case R.id.overflowmenuitem3:
                System.out.println("OVERFLOWMENU ITEM 3 CLICKED");

                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
