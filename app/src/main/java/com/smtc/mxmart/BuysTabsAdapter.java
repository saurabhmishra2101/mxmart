package com.smtc.mxmart;




import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.smtc.mxmart.Fragment.SellCurrentDealsFragment;
import com.smtc.mxmart.Fragment.SellCurrentEnquiriesFragment;
import com.smtc.mxmart.Fragment.SellHistoryFragment;
import com.smtc.mxmart.Fragment.SellTodaysOfferFragment;



public class BuysTabsAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public BuysTabsAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {


          case 0:

              SellCurrentEnquiriesFragment tab1 = new SellCurrentEnquiriesFragment();
                return tab1;


            case 2:
                SellCurrentDealsFragment tab2 = new SellCurrentDealsFragment();
                return tab2;



            case 3:
                SellHistoryFragment tab3 = new SellHistoryFragment();
                return tab3;



            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}