package com.smtc.mxmart;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;

import android.os.Build;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.WeakHashMap;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by 10161 on 8/21/2017.
 */

public class SellerDispatchActivity extends AppCompatActivity {

    ImageView backiv;
    SessionManager sessionManager;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    UtilsDialog util = new UtilsDialog();
    ImageView empty_dispatch_iv;
    TextView nodispatchdetailstxtvw,clickonplusbtntxtvw;
    RelativeLayout seller_dispatch_layout;
    ImageView adddispatchiv;
    String tradeid,sellid,invoice_amount_str,dispatch_date_str,truck_num_str,transport_name_str,mobile_num_str,invoice_num_str,invoice_due_date_str,invoice_qty_str;
    ProgressDialog pdia;
    EditText invoice_amount_edt;
    String userCodestr,mobilenumstr,dispatchdate,invoiceduedate;
    int mYear,mMonth,mDay;
    int mYear1,mMonth1,mDay1;
    ExpandableListView sellerdispatchexpandablelistvw;
    CustomExpandableListAdapter expandableListAdapter;
    static List<String> expandableListTitle;
    static WeakHashMap<String, List<String>> expandableListDetail;
    AlertDialog b;
    ImageView deletedispatchiv;
    int selected = 0;
    ArrayList<String> dispatchIdArray = new ArrayList<String>();
    ArrayList<Integer> positionArray = new ArrayList<Integer>();
    JSONArray SellerCurrentDealsByTradeJsonArray,dispatch_resolved_jsonarray;
    TextView notification_total_count;
    String commentStr,tradeStr;
    TextView todays_offer_count,new_enquiry_count,seller_accepted_count,seller_rejected_count,sp_generated_count,buyer_rejected_count;
    int index;
    DatePickerFragment newFragment;
    DatePickerFragment1 newFragment1;
    static EditText dispatch_date_edt,invoice_due_date_edt;
    String selldeals,mobile_num,fcm_id;
    String dispatchremainingpaystr,dispatchremainingqtystr;
    int selectedPosition =   -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seller_dispatch);

        notification_total_count = (TextView)findViewById(R.id.notification_total_count);

        nodispatchdetailstxtvw = (TextView)findViewById(R.id.nodispatchdetailstxtvw);
        clickonplusbtntxtvw = (TextView)findViewById(R.id.clickonplusbtntxtvw);
        empty_dispatch_iv = (ImageView) findViewById(R.id.empty_dispatch_iv);
        seller_dispatch_layout = (RelativeLayout)findViewById(R.id.seller_dispatch_layout);
        adddispatchiv = (ImageView)findViewById(R.id.adddispatchiv);
        deletedispatchiv = (ImageView)findViewById(R.id.deletedispatchiv);
        sellerdispatchexpandablelistvw = (ExpandableListView)findViewById(R.id.sellerdispatchexpandablelistvw);

/*

        Bundle b = new Bundle();
        b = getIntent().getExtras();

        if(b!=null) {
            tradeid = b.getString("tradeid");
            sellid = b.getString("sellid");
        }
*/



        sessionManager = new SessionManager(getApplicationContext());

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        mobile_num = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id",null);
        selldeals = prefs.getString("selldeals",null);

        System.out.println("USER CODE IN MAIN ACTIVITY---"+ userCodestr);

      /*  Toolbar toolbar = (Toolbar)findViewById(R.id.sellerdispatchtoolbar);
        setSupportActionBar(toolbar);*/

        backiv = (ImageView)findViewById(R.id.back_dispatch);

        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent i = new Intent(SellerDispatchActivity.this,HomeActivity.class);
                SingletonActivity.frombackofbuyerdispatch = false;
                SingletonActivity.fromsellcurrentdeals = false;
                SingletonActivity.frombuycurrentdeals = false;
                SingletonActivity.frombackofsellerdispatch = true;
                startActivity(i);





            }
        });

        adddispatchiv.setEnabled(false);

        if(NetworkUtility.checkConnectivity(SellerDispatchActivity.this)){
            String getCurrentDealsByTradeURL = APIName.URL+"/seller/getCurrentDealsByTrade?user_code="+userCodestr+"&tradeId="+SingletonActivity.tradeid;
            System.out.println("GET CURRENT DEALS BY TRADE URL IS---"+ getCurrentDealsByTradeURL);
            GetCurrentDealsByTradeAPI(getCurrentDealsByTradeURL);

        }
        else{
            util.dialog(SellerDispatchActivity.this, "Please check your internet connection.");
        }



        if(selected == 0)
        {
            deletedispatchiv.setBackgroundResource(R.mipmap.ic_delete_disabled);
        }



        adddispatchiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(selldeals.equalsIgnoreCase("0")) {
                   // AddDispatchDialog();
                    Intent i = new Intent(SellerDispatchActivity.this,SellerAddDispatchActivity.class);
                    SingletonActivity.clickedaddicon = "click";
                    startActivity(i);
                }
                else
                {
                    Toast.makeText(SellerDispatchActivity.this, "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        deletedispatchiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(selldeals.equalsIgnoreCase("0")) {
                    deleteDispatch();
                }
                else
                {
                    Toast.makeText(SellerDispatchActivity.this, "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



    public static class DatePickerFragment1 extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            System.out.println("SingletonActivity.sauda_patra_gen_date ======"+ SingletonActivity.sauda_patra_gen_date);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);

               if(dispatch_date_edt.getText().toString().length()>0) {
                   String dateString = dispatch_date_edt.getText().toString();
                   DateFormat formatt = new SimpleDateFormat("dd/MM/yyyy");
                   Date date = null;
                   try {
                       date = formatt.parse(dateString);


                       System.out.println("Date in add payment=====:" + formatt.format(date));
                       dialog.getDatePicker().setMinDate(date.getTime());
                   } catch (ParseException e) {
                       e.printStackTrace();
                   }
               }






            return dialog;


        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user

            String daynew ="";
            String monthnew ="";

            if(day<10)
            {
                daynew = "0"+Integer.toString(day);
            }
            else
            {
                daynew = Integer.toString(day);
            }

            if(month+1<10)
            {
                monthnew = "0"+ Integer.toString(month+1);
            }
            else
            {
                monthnew = Integer.toString(month+1);
            }


            invoice_due_date_edt.setText(daynew + "/"
                    + monthnew + "/" + year);




        }
    }



    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

           System.out.println("SingletonActivity.sauda_patra_gen_date IN ADD DISPATCH ======"+ SingletonActivity.sauda_patra_gen_date);



            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);


                String dateString = SingletonActivity.sauda_patra_gen_date;
              DateFormat formatt = new SimpleDateFormat("dd-MM-yyyy");
              Date date = null;
              try {
                  date = formatt.parse(dateString);


                  System.out.println("Date in add payment=====:" +formatt.format(date));
              } catch (ParseException e) {
                  e.printStackTrace();
              }


              dialog.getDatePicker().setMinDate(date.getTime());
              dialog.getDatePicker().setMaxDate(c.getTimeInMillis());




            return dialog;


        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user

            String daynew ="";
            String monthnew ="";

            if(day<10)
            {
                daynew = "0"+Integer.toString(day);
            }
            else
            {
                daynew = Integer.toString(day);
            }

            if(month+1<10)
            {
                monthnew = "0"+ Integer.toString(month+1);
            }
            else
            {
                monthnew = Integer.toString(month+1);
            }


            dispatch_date_edt.setText(daynew + "/"
                    + monthnew + "/" + year);




        }
    }





    @Override
    public void onBackPressed() {


            Intent i = new Intent(SellerDispatchActivity.this,HomeActivity.class);
        SingletonActivity.frombackofbuyerdispatch = false;
        SingletonActivity.fromsellcurrentdeals = false;
        SingletonActivity.frombuycurrentdeals = false;
        SingletonActivity.frombackofsellerdispatch = true;
            startActivity(i);



    }

    @Override
    protected void onResume() {
        super.onResume();



    }

    public void deleteDispatch()
    {
        String dispatchIdStr = "";
        for (int str = 0; str< dispatchIdArray.size(); str++) {
            if (dispatchIdStr.length()>0) {
                dispatchIdStr = dispatchIdStr+","+dispatchIdArray.get(str);
            }else{
                dispatchIdStr =dispatchIdArray.get(str);
            }
        }
     System.out.println("DISPATCH ID------"+ dispatchIdStr);

       String dispatchcsv = new LinkedHashSet<String>(Arrays.asList(dispatchIdStr.split(","))).toString().replaceAll("(^\\[|\\]$)", "").replace(", ", ",");
        System.out.println("DISPATCH CSV ID------"+ dispatchcsv);

        String tradeid = null;
        String buyerusercode = null;
        try {
            tradeid = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("trade_id");
            buyerusercode = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("user_code");

            if (!dispatchcsv.equalsIgnoreCase(""))

                if (NetworkUtility.checkConnectivity(SellerDispatchActivity.this)) {
                    String DeleteDispatchURL = APIName.URL + "/seller/deleteDispatch?trade_id=" + tradeid + "&auto_dispatch_id=" + dispatchcsv + "&buyer_user_code=" + buyerusercode;
                    System.out.println("DELETE DISPATCH URL IS---" + DeleteDispatchURL);
                    DeleteDispatchAPI(DeleteDispatchURL);

                } else {
                    util.dialog(SellerDispatchActivity.this, "Please check your internet connection.");
                }

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    public void AddDispatchDialog(){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SellerDispatchActivity.this);
        LayoutInflater inflater = SellerDispatchActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.add_dispatch_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

         dispatch_date_edt = (EditText)dialogView.findViewById(R.id.dispatch_date_edt);
        final EditText truck_num_edt = (EditText)dialogView.findViewById(R.id.truck_num_edt);
        final EditText transport_name_edt = (EditText)dialogView.findViewById(R.id.transport_name_edt);
        final EditText mobile_num_edt = (EditText)dialogView.findViewById(R.id.mobile_num_edt);
        final EditText invoice_num_edt = (EditText)dialogView.findViewById(R.id.invoice_num_edt);
        invoice_due_date_edt = (EditText)dialogView.findViewById(R.id.invoice_due_date_edt);
        final EditText invoice_qty_edt = (EditText)dialogView.findViewById(R.id.invoice_qty_edt);
        invoice_amount_edt = (EditText)dialogView.findViewById(R.id.invoice_amount_edt);
        final TextView remaininginvoiceamounttxtvw = (TextView)dialogView.findViewById(R.id.remaininginvoiceamounttxtvw);
        final TextView remainingqtytxtvw = (TextView)dialogView.findViewById(R.id.remainingqtytxtvw);
      //  final TextView addtxtvw = (TextView)dialogView.findViewById(R.id.adddispatchtxtvw);
      //  final TextView canceltxtvw = (TextView)dialogView.findViewById(R.id.canceldispatchtxtvw);
        final RelativeLayout sbmtrelative = (RelativeLayout)dialogView.findViewById(R.id.sbmtrelative);
        final RelativeLayout cancelrelative = (RelativeLayout)dialogView.findViewById(R.id.cancelrelative);
        final TextView sbmttext = (TextView)dialogView.findViewById(R.id.sbmttext);



        System.out.println("DISPATCH REMAINING QTY=="+dispatchremainingqtystr);
        System.out.println("DISPATCH REMAINING PAY=="+dispatchremainingpaystr);

        remaininginvoiceamounttxtvw.setText("Remaining Invoice Amount: "+ dispatchremainingpaystr);
        remainingqtytxtvw.setText("Remaining Quantity: "+ dispatchremainingqtystr +"0");

        invoice_qty_edt.setText(dispatchremainingqtystr+ "0");
        invoice_amount_edt.setText(dispatchremainingpaystr);

        if(dispatchremainingqtystr.equalsIgnoreCase("0.00"))
        {
            invoice_qty_edt.setEnabled(false);
            invoice_qty_edt.setTextColor(Color.parseColor("#696969"));

        }
        else
        {
            invoice_qty_edt.setEnabled(true);
            invoice_qty_edt.setTextColor(Color.parseColor("#000000"));
        }

        if(dispatchremainingpaystr.equalsIgnoreCase("0.00"))
        {
            invoice_amount_edt.setEnabled(false);
            invoice_amount_edt.setTextColor(Color.parseColor("#696969"));
        }
        else
        {
            invoice_amount_edt.setEnabled(true);
            invoice_amount_edt.setTextColor(Color.parseColor("#000000"));
        }

        float invoice_qty_float_latest_val = Float.parseFloat(invoice_qty_edt.getText().toString());
        float invoice_amount_float_latest_val = Float.parseFloat(invoice_amount_edt.getText().toString());

        float sum = invoice_qty_float_latest_val + invoice_amount_float_latest_val;

        if(sum == 0)
        {
           // addtxtvw.setTextColor(Color.parseColor("#d3d3d3"));
           // addtxtvw.setEnabled(false);

            sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
            sbmttext.setTextColor(Color.parseColor("#808080"));
            sbmtrelative.setEnabled(false);
        }
        else
        {
          //  addtxtvw.setTextColor(Color.parseColor("#000000"));
          //  addtxtvw.setEnabled(true);

            sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
            sbmttext.setTextColor(Color.WHITE);
            sbmtrelative.setEnabled(true);
        }

        invoice_qty_edt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {



            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {

                    float invoice_qty_float_val = Float.parseFloat(invoice_qty_edt.getText().toString());
                    float dispatchremainingqty_float_val = Float.parseFloat(dispatchremainingqtystr);


                    if ((Float.parseFloat(invoice_qty_edt.getText().toString()) > dispatchremainingqty_float_val)) {

                        //      Toast.makeText(getActivity(),"Quantity to be increased should not be less than the difference of Buyer Requested Quantity and Currently Remaining Quantity",Toast.LENGTH_SHORT).show();

                        Toast.makeText(SellerDispatchActivity.this,"Quantity can't be greater than remaining quantity.",Toast.LENGTH_SHORT).show();

                  //      addtxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                   //     addtxtvw.setEnabled(false);
                        sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                        sbmttext.setTextColor(Color.parseColor("#808080"));
                        sbmtrelative.setEnabled(false);


                    } else {

                        // Toast.makeText(getActivity(),"QUANTITY -",Toast.LENGTH_SHORT).show();
                     //   addtxtvw.setTextColor(Color.parseColor("#000000"));
                    //    addtxtvw.setEnabled(true);

                        sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                        sbmttext.setTextColor(Color.WHITE);
                        sbmtrelative.setEnabled(true);


                        if(invoice_qty_edt.getText().toString().length()>0)
                        {
                            float invoice_qty_float_latest_val = Float.parseFloat(invoice_qty_edt.getText().toString());

                            if(invoice_amount_edt.getText().toString().length()>0) {

                               float invoice_amount_float_latest_val = Float.parseFloat(invoice_amount_edt.getText().toString());

                                float sum = invoice_qty_float_latest_val + invoice_amount_float_latest_val;

                                if(sum == 0)
                                {
                             //       addtxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                             //       addtxtvw.setEnabled(false);

                                    sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                    sbmttext.setTextColor(Color.parseColor("#808080"));
                                    sbmtrelative.setEnabled(false);
                                }
                                else
                                {
                             //       addtxtvw.setTextColor(Color.parseColor("#000000"));
                              //      addtxtvw.setEnabled(true);

                                    sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                    sbmttext.setTextColor(Color.WHITE);
                                    sbmtrelative.setEnabled(true);
                                }
                            }



                        }



                    }

                }
            }

            @Override
            public void afterTextChanged(Editable edt) {

                String temp = edt.toString ();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 3) {
                    edt.delete(posDot + 4, posDot + 5);
                }


            }
        });

        invoice_amount_edt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    if(invoice_qty_edt.getText().toString().length()>0)
                    {
                        float invoice_qty_float_latest_val = Float.parseFloat(invoice_qty_edt.getText().toString());

                        if(invoice_amount_edt.getText().toString().length()>0) {

                            float invoice_amount_float_latest_val = Float.parseFloat(invoice_amount_edt.getText().toString());

                            float sum = invoice_qty_float_latest_val + invoice_amount_float_latest_val;

                            if(sum == 0)
                            {
                             //   addtxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                             //   addtxtvw.setEnabled(false);

                                sbmtrelative.setBackgroundResource(R.drawable.disable_rounded_edittext);
                                sbmttext.setTextColor(Color.parseColor("#808080"));
                                sbmtrelative.setEnabled(false);
                            }
                            else
                            {
                             //   addtxtvw.setTextColor(Color.parseColor("#000000"));
                             //   addtxtvw.setEnabled(true);

                                sbmtrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);
                                sbmttext.setTextColor(Color.WHITE);
                                sbmtrelative.setEnabled(true);
                            }
                        }



                    }
                }
            }

            @Override
            public void afterTextChanged(Editable edt) {

                String temp = edt.toString ();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 2) {
                    edt.delete(posDot + 3, posDot + 4);
                }
            }
        });


        dispatch_date_edt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");


                }


        });

        dispatch_date_edt.setFocusable(true);
        dispatch_date_edt.setClickable(true);


        invoice_due_date_edt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                newFragment1 = new DatePickerFragment1();
                newFragment1.show(getFragmentManager(), "datePicker");



            }
        });

        invoice_due_date_edt.setFocusable(false);
        invoice_due_date_edt.setClickable(true);

        invoice_amount_edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                invoice_amount_edt.removeTextChangedListener(this);

                try {
                    String originalString = s.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    invoice_amount_edt.setText(formattedString);
                    invoice_amount_edt.setSelection(invoice_amount_edt.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                invoice_amount_edt.addTextChangedListener(this);
            }
        });







        sbmtrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                dispatch_date_str = dispatch_date_edt.getText().toString();
                truck_num_str = truck_num_edt.getText().toString();
                transport_name_str = transport_name_edt.getText().toString();
                mobile_num_str = mobile_num_edt.getText().toString();
                invoice_num_str = invoice_num_edt.getText().toString();
                invoice_due_date_str = invoice_due_date_edt.getText().toString();
                invoice_qty_str = invoice_qty_edt.getText().toString();

                boolean invalid = false;

                if (dispatch_date_str.equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please Select Dispatch Date",
                            Toast.LENGTH_SHORT).show();
                }
                else  if (truck_num_str.equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(),
                            "Please Enter Truck No.", Toast.LENGTH_SHORT)
                            .show();
                }

                else  if (transport_name_str.equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(),
                            "Please Enter Transport Name", Toast.LENGTH_SHORT)
                            .show();
                }




                else  if (invoice_num_str.equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(),
                            "Please Enter Invoice No.", Toast.LENGTH_SHORT)
                            .show();
                }


                else  if (invoice_qty_str.equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(),
                            "Please Enter Invoice Quantity", Toast.LENGTH_SHORT)
                            .show();
                }

                else  if (invoice_amount_edt.getText().toString().equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(),
                            "Please Enter Invoice Amount", Toast.LENGTH_SHORT)
                            .show();
                }

                else if (invalid == false)
                {
                    if(NetworkUtility.checkConnectivity(SellerDispatchActivity.this)){
                        sbmtrelative.setEnabled(false);
                        String adddispatchurl = APIName.URL+"/seller/addDispatch?trade_id="+SingletonActivity.tradeid;
                        System.out.println("ADD DISPATCH URL IS---"+ adddispatchurl);
                        AddDispatchAPI(adddispatchurl);

                    }
                    else{
                        util.dialog(SellerDispatchActivity.this, "Please check your internet connection.");
                    }


                }

            }
        });



        b = dialogBuilder.create();
        b.show();


        cancelrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               b.dismiss();
            }
        });


    }



    private void AddDispatchAPI(String url) {
        pdia = new ProgressDialog(SellerDispatchActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SellerDispatchActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        System.out.println("RESPONSE OF ADD DISPATCH API IS---" + response);
                        //Toast.makeText(SellerDispatchActivity.this,response,Toast.LENGTH_SHORT).show();

                        JSONObject AddDispatchJson = null;

                        try {
                            AddDispatchJson = new JSONObject(response);
                            String statusstr = AddDispatchJson.getString("status");


                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Toast.makeText(SellerDispatchActivity.this,AddDispatchJson.getString("message"),Toast.LENGTH_SHORT).show();
                                b.dismiss();

                               Intent i = new Intent(SellerDispatchActivity.this,SellerDispatchActivity.class);
                               i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);

                             /*   if(NetworkUtility.checkConnectivity(SellerDispatchActivity.this)){
                                    String getCurrentDealsByTradeURL = APIName.URL+"/seller/getCurrentDealsByTrade?user_code="+userCodestr+"&tradeId="+SingletonActivity.tradeid;
                                    System.out.println("GET CURRENT DEALS BY TRADE URL IS---"+ getCurrentDealsByTradeURL);
                                    GetCurrentDealsByTradeAPI(getCurrentDealsByTradeURL);

                                }
                                else{
                                    util.dialog(SellerDispatchActivity.this, "Please check your internet connection.");
                                }*/

                            }
                            else
                            {
                                Toast.makeText(SellerDispatchActivity.this,AddDispatchJson.getString("message"),Toast.LENGTH_SHORT).show();
                                b.dismiss();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(SellerDispatchActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SellerDispatchActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(SellerDispatchActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("dispatch_date",dispatch_date_str);
                params.put("truck_number",truck_num_str);
                params.put("transport_name",transport_name_str);
                params.put("dispatch_mobile",mobile_num_str);
                params.put("invoice_number",invoice_num_str);
                params.put("invoice_due_date",invoice_due_date_str);
                params.put("invoice_qty",invoice_qty_str);
                params.put("invoice_amount",invoice_amount_edt.getText().toString());
                params.put("sell_id",SingletonActivity.sellid);
                params.put("mobile_num",mobilenumstr);





                System.out.println("add dispatch params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }


    public  final boolean isValidMobileNumber(String mobile) {
        if (mobile.length()!=10) {

            Toast.makeText(this,
                    "Please Enter Valid Mobile No.", Toast.LENGTH_SHORT)
                    .show();

            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {


            sessionManager.logoutUser(mobile_num,fcm_id);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void DeleteDispatchAPI(String url) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SellerDispatchActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF DELETE DISPATCH API IS---" + response);



                        JSONObject DeleteDispatchJson = null;
                        try {
                            DeleteDispatchJson = new JSONObject(response);


                            String statusstr = DeleteDispatchJson.getString("status");
                            System.out.println("STATUS OF DELETE DISPATCH API IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                Toast.makeText(SellerDispatchActivity.this,DeleteDispatchJson.getString("message"),Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(SellerDispatchActivity.this,SellerDispatchActivity.class);
                                startActivity(i);
                                }
                                else
                                {
                                    Toast.makeText(SellerDispatchActivity.this,DeleteDispatchJson.getString("message"),Toast.LENGTH_SHORT).show();
                                }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SellerDispatchActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(SellerDispatchActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();





                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(SellerDispatchActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }


    private void GetCurrentDealsByTradeAPI(String url) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SellerDispatchActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF GET CURRENT DEALS BY TRADE API IS---" + response);



                        JSONObject SellerGetCurrentDealsByTradeJson = null;
                        try {
                            SellerGetCurrentDealsByTradeJson = new JSONObject(response);


                            String statusstr = SellerGetCurrentDealsByTradeJson.getString("status");
                            System.out.println("STATUS OF GET CURRENT DEALS BY TRADE API IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {



                                 SellerCurrentDealsByTradeJsonArray = SellerGetCurrentDealsByTradeJson.getJSONArray("cd_Offer");
                                 SingletonActivity.sauda_patra_gen_date = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("sauda_patra_gen_date");

                                 if(SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("transport").equalsIgnoreCase("3"))
                                 {
                                     adddispatchiv.setBackgroundResource(R.mipmap.edit);
                                     clickonplusbtntxtvw.setText( "Transportation details will be added by Mxmart admin.");
                                 }
                                 else
                                 {
                                     adddispatchiv.setBackgroundResource(R.mipmap.ic_add_dispatch);
                                     adddispatchiv.setEnabled(true);
                                 }

                                 dispatchremainingpaystr = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("dispatch_remain_pay");
                                 dispatchremainingqtystr = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("dispatch_remain_quan");

                                    String dispatchdetails = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("dispatch_details");
                                    String paymentdetails =  SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("payment_details");

                                System.out.println("DISPATCH DETAILS LENGTH IS---" + dispatchdetails.length());

                                    if(dispatchdetails.length()>5)
                                    {
                                        expandableListTitle = new ArrayList<String>();
                                        expandableListDetail = new WeakHashMap<String, List<String>>();

                                        JSONArray DispatchDetailsJsonArray = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("dispatch_details");
                                        JSONArray CertificateFileJsonArray = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("certificate_file");

                                        String dispatch_resolved_jsonobj = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("dispatch_resolved");
                                     //   Toast.makeText(SellerDispatchActivity.this,"DISPATCH RESOLVED LENGTH==="+ dispatch_resolved_jsonobj.length(),Toast.LENGTH_SHORT).show();

                                        expandableListAdapter = new CustomExpandableListAdapter(SellerDispatchActivity.this,SellerCurrentDealsByTradeJsonArray,DispatchDetailsJsonArray,CertificateFileJsonArray);
                                        sellerdispatchexpandablelistvw.setAdapter(expandableListAdapter);

                                        sellerdispatchexpandablelistvw.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                                            @Override
                                            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                                                System.out.println("ON GROUP CLICKED--------"+ groupPosition);
                                               // parent.expandGroup(groupPosition);

                                                if(parent.isGroupExpanded(groupPosition))
                                                {

                                                    // Do your Staff

                                                    deletedispatchiv.setBackgroundResource(R.mipmap.ic_delete_disabled);
                                                    adddispatchiv.setBackgroundResource(R.mipmap.ic_add_dispatch);

                                                    try {
                                                        if(SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("transport").equalsIgnoreCase("3"))
                                                        {
                                                            adddispatchiv.setBackgroundResource(R.mipmap.edit);
                                                          //  clickonplusbtntxtvw.setText( "Transportation details will be added by Mxmart admin.");
                                                        }
                                                        else
                                                        {
                                                            adddispatchiv.setBackgroundResource(R.mipmap.ic_add_dispatch);
                                                            adddispatchiv.setEnabled(true);
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                                else{

                                                    // Expanded ,Do your Staff
                                                    deletedispatchiv.setBackgroundResource(R.mipmap.ic_delete_disabled);
                                                    adddispatchiv.setBackgroundResource(R.mipmap.ic_add_dispatch);

                                                    try {
                                                        if(SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("transport").equalsIgnoreCase("3"))
                                                        {
                                                            adddispatchiv.setBackgroundResource(R.mipmap.edit);
                                                            clickonplusbtntxtvw.setText( "Transportation details will be added by Mxmart admin.");
                                                        }
                                                        else
                                                        {
                                                            adddispatchiv.setBackgroundResource(R.mipmap.ic_add_dispatch);
                                                            adddispatchiv.setEnabled(true);
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }

                                                }

                                                return false;
                                            }
                                        });



                                        sellerdispatchexpandablelistvw.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                                            @Override
                                            public boolean onChildClick(ExpandableListView parent, View v,
                                                                        int groupPosition, int childPosition, long id) {


                                                return false;
                                            }
                                        });

                                        sellerdispatchexpandablelistvw.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                                            int previousGroup = -1;

                                            @Override
                                            public void onGroupExpand(int groupPosition) {
                                                if(groupPosition != previousGroup)
                                                    sellerdispatchexpandablelistvw.collapseGroup(previousGroup);
                                                previousGroup = groupPosition;
                                            }
                                        });


                                    }
                                    else
                                    {
                                        empty_dispatch_iv.setVisibility(View.VISIBLE);
                                        nodispatchdetailstxtvw.setVisibility(View.VISIBLE);
                                        clickonplusbtntxtvw.setVisibility(View.VISIBLE);
                                        sellerdispatchexpandablelistvw.setVisibility(View.GONE);

                                        if(SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("transport").equalsIgnoreCase("3"))
                                        {
                                            adddispatchiv.setVisibility(View.GONE);

                                        }
                                        else
                                        {
                                            adddispatchiv.setVisibility(View.VISIBLE);

                                        }



                                    }







                            }

                            else
                            {
                                empty_dispatch_iv.setVisibility(View.VISIBLE);
                                nodispatchdetailstxtvw.setVisibility(View.VISIBLE);
                                clickonplusbtntxtvw.setVisibility(View.VISIBLE);
                                sellerdispatchexpandablelistvw.setVisibility(View.GONE);
                            }





                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SellerDispatchActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(SellerDispatchActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();



                System.out.println("get seller current enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(SellerDispatchActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }



    public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

        private Context context;
        JSONArray DispatchDetailsJsonArray;
        JSONArray CertificateFileJsonArray;
        JSONArray SellerCurrentDealsByTradeJsonArray;
      //  JSONObject dispatch_resolved_jsonobj;

        public CustomExpandableListAdapter(Context context,JSONArray SellerCurrentDealsByTradeJsonArray, JSONArray DispatchDetailsJsonArray,JSONArray CertificateFileJsonArray) {
            this.context = context;
            this.DispatchDetailsJsonArray = DispatchDetailsJsonArray;
            this.CertificateFileJsonArray = CertificateFileJsonArray;
            this.SellerCurrentDealsByTradeJsonArray = SellerCurrentDealsByTradeJsonArray;
          //  this.dispatch_resolved_jsonobj = dispatch_resolved_jsonobj;

        }

        @Override
        public Object getChild(int listPosition, int expandedListPosition) {

            System.out.println("IN GETCHILD listPosition---"+ listPosition);

            System.out.println("IN GETCHILD expandedListPosition---"+ expandedListPosition);

            return listPosition;




        }

        @Override
        public long getChildId(int listPosition, int expandedListPosition) {

            System.out.println("IN GETCHILDID listPosition---"+ listPosition);

            return listPosition;
        }

        @Override
        public View getChildView(int listPosition, final int expandedListPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
         //   final String expandedListText = (String) getChild(listPosition, expandedListPosition);
            System.out.println("IN GETCHILDVIEW---");

            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) this.context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.seller_dispatch_details_child_row, null);
            }




            TextView trucknodesctxtvw = (TextView) convertView
                .findViewById(R.id.trucknodesctxtvw);

            TextView transportnamedesctxtvw = (TextView) convertView
                    .findViewById(R.id.transportnamedesctxtvw);

             TextView qtydesctxtvw = (TextView) convertView
                    .findViewById(R.id.qtydesctxtvw);

            TextView mobilenodesctxtvw = (TextView) convertView
                    .findViewById(R.id.mobilenodesctxtvw);

            TextView duedatedesctxtvw = (TextView) convertView
                    .findViewById(R.id.duedatedesctxtvw);

            TextView invoiceamountdesctxtvw = (TextView) convertView
                    .findViewById(R.id.invoiceamountdesctxtvw);

            final ImageView viewdociv1 = (ImageView) convertView
                    .findViewById(R.id.viewdociv1);
            final ImageView viewdociv2 = (ImageView) convertView
                    .findViewById(R.id.viewdociv2);
            final ImageView viewdociv3 = (ImageView) convertView
                    .findViewById(R.id.viewdociv3);
            final ImageView viewdociv4 = (ImageView) convertView
                    .findViewById(R.id.viewdociv4);
            final ImageView viewdociv5 = (ImageView) convertView
                    .findViewById(R.id.viewdociv5);
            final ImageView viewdociv6 = (ImageView) convertView
                    .findViewById(R.id.viewdociv6);

            viewdociv1.setVisibility(View.GONE);
            viewdociv2.setVisibility(View.GONE);
            viewdociv3.setVisibility(View.GONE);
            viewdociv4.setVisibility(View.GONE);
            viewdociv5.setVisibility(View.GONE);
            viewdociv6.setVisibility(View.GONE);


            try {
                trucknodesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("truck_number"));
                transportnamedesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("transport_name"));
                qtydesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_qty"));
                mobilenodesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("dispatch_mobile"));
                duedatedesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_due_date"));
                invoiceamountdesctxtvw.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_amount"));


                final JSONArray certificate_file_jsonarray = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("certificate_file");


               // for (int i = 0; i < CertificateFileJsonArray.length(); i++) {

                //    for (int j = 0; j < DispatchDetailsJsonArray.length(); j++) {




                       // if (CertificateFileJsonArray.getJSONObject(i).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {

                             // Toast.makeText(SellerDispatchActivity.this,"INDEX MATCHED=="+ i + "&" + j,Toast.LENGTH_SHORT).show();
                            if (certificate_file_jsonarray.length() == 1) {
                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {


                                        viewdociv1.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv1.setImageBitmap(bmp);


                                                    }


                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                            }


                            if (certificate_file_jsonarray.length() == 2) {

                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv1.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                    } else {
                                                        System.out.println("IN T1==b");
                                                        viewdociv1.setImageBitmap(bmp);

                                                    }


                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {
                                    if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {

                                        viewdociv2.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv2.setImageBitmap(bmp);
                                                    }


                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }

                                }

                            }

                            if (certificate_file_jsonarray.length() == 3) {
                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv1.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv1.setImageBitmap(bmp);

                                                    }


                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv2.setVisibility(View.VISIBLE);

                                        final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here

                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);

                                                    // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.pdf);

                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv2.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv3.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.pdf);

                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv3.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                            }
                            if (certificate_file_jsonarray.length() == 4) {

                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv1.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv1.setImageBitmap(bmp);

                                                    }


                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }

                                }

                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv2.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv2.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv3.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv3.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv4.setVisibility(View.VISIBLE);

                                        final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv4.setBackgroundResource(R.mipmap.pdf);

                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv4.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv4.setBackgroundResource(R.mipmap.docx);
                                                    } else {
                                                        viewdociv4.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }
                            }

                            if (certificate_file_jsonarray.length() == 5) {


                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv1.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv1.setImageBitmap(bmp);

                                                    }


                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv2.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv2.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv3.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv3.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv4.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv4.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv4.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv4.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv4.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv5.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv5.setBackgroundResource(R.mipmap.pdf);

                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv5.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv5.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv5.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                            }
                            if (certificate_file_jsonarray.length() == 6)

                            {


                                if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv1.setVisibility(View.VISIBLE);

                                        final String urlstr = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv1.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        System.out.println("IN T1==b");
                                                        viewdociv1.setImageBitmap(bmp);

                                                    }


                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv2.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //   Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv2.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv2.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }

                                }

                                if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv3.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv3.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv3.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv4.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    // Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv4.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv4.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv4.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv4.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv5.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv5.setBackgroundResource(R.mipmap.pdf);
                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv5.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv5.setBackgroundResource(R.mipmap.docx);
                                                    } else {


                                                        viewdociv5.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }

                                if (certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file").length() > 0) {

                                    if (certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_dispatch_id").equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                        viewdociv6.setVisibility(View.VISIBLE);


                                        final String urlstr = certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file");
                                        String fileName = urlstr.substring(urlstr.lastIndexOf('/') + 1, urlstr.length());

                                        final URL url = new URL(urlstr);

                                        Thread thread = new Thread(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    //Your code goes here
                                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                                                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream(), null, options);
                                                    //  Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                                                    String extension = urlstr.substring(urlstr.lastIndexOf("."));
                                                    System.out.println("EXTENSION IS===" + extension);
                                                    if (extension.equalsIgnoreCase(".pdf")) {
                                                        viewdociv6.setBackgroundResource(R.mipmap.pdf);

                                                    } else if (extension.equalsIgnoreCase(".doc")) {
                                                        viewdociv6.setBackgroundResource(R.mipmap.doc2);
                                                    } else if (extension.equalsIgnoreCase(".docx")) {
                                                        viewdociv6.setBackgroundResource(R.mipmap.docx);
                                                    } else {
                                                        viewdociv6.setImageBitmap(bmp);
                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                        thread.start();
                                    }


                                }
                            }

                     //   }
                   // }
              //  }


            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (JSONException e1) {
                e1.printStackTrace();
            }

            final JSONArray certificate_file_jsonarray;
            try {
                certificate_file_jsonarray = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("certificate_file");

                viewdociv1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(SellerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(0).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });

                viewdociv2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(SellerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(1).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });

                viewdociv3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(SellerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(2).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });

                viewdociv4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(SellerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(3).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });

                viewdociv5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(SellerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(4).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });

                viewdociv6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i= new Intent(SellerDispatchActivity.this,DispatchWebviewActivity.class);
                        try {
                            SingletonActivity.dispatchdocurl = certificate_file_jsonarray.getJSONObject(5).getString("st_test_cert_file");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);

                    }
                });




            } catch (JSONException e) {
                e.printStackTrace();
            }





            return convertView;
        }

        @Override
        public int getChildrenCount(int listPosition) {

            System.out.println("IN GETCHILDRENCOUNT---"+ listPosition);
                return 1;

        }

        @Override
        public Object getGroup(int listPosition) {

            System.out.println("IN GETGROUP---");
                return 1;

        }

        @Override
        public int getGroupCount() {

            System.out.println("IN GETGROUPCOUNT---");
            return DispatchDetailsJsonArray.length();
        }

        @Override
        public long getGroupId(int listPosition) {
            return listPosition;
        }

        @Override
        public View getGroupView(final int listPosition,final boolean isExpanded,
                                 View convertView,final ViewGroup parent) {
//            String listTitle = (String) getGroup(listPosition);

            System.out.println("IN GETGROUPVIEW---");

            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) this.context.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = layoutInflater.inflate(R.layout.seller_dispatch_details_title_row, null);
            }

            Typeface source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

            TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.date);
            TextView invoicenumtxt = (TextView) convertView
                    .findViewById(R.id.invoicenumtxt);
            ImageView disputeiv = (ImageView) convertView
                    .findViewById(R.id.disputeiv);


           final  CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.checkbox);
           // Toast.makeText(SellerDispatchActivity.this,"isExpanded is="+ isExpanded,Toast.LENGTH_SHORT).show();


            try {
                String dispatch_resolved_str = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("dispatch_resolved_new");


               // if((DispatchDetailsJsonArray.getJSONObject(listPosition).getString("is_active").equalsIgnoreCase("9"))) {

                    dispatch_resolved_jsonarray = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getJSONArray("dispatch_resolved_new");

                    if (dispatch_resolved_jsonarray.length() > 0) {

                        disputeiv.setVisibility(View.VISIBLE);



                        //Toast.makeText(SellerDispatchActivity.this,"DISPUTE RESOLUTION ==" + dispatch_resolved_jsonarray,Toast.LENGTH_SHORT).show();

                      //if(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))

                        for(int i = 0 ; i < dispatch_resolved_jsonarray.length();i++) {
                            if(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id").equalsIgnoreCase(dispatch_resolved_jsonarray.getJSONObject(i).getString("auto_dispatch_id"))) {

                                if (dispatch_resolved_jsonarray.getJSONObject(i).getString("flag").equalsIgnoreCase("1")) {
                                    disputeiv.setBackgroundResource(R.mipmap.dispute_resolved_orange_info);
                                  //  Toast.makeText(SellerDispatchActivity.this,"if is="+ dispatch_resolved_jsonarray.getJSONObject(i).getString("flag"),Toast.LENGTH_SHORT).show();
                                } else {
                                    disputeiv.setBackgroundResource(R.mipmap.dispute_raised_exclamatory);
                                 //   Toast.makeText(SellerDispatchActivity.this,"else is="+ dispatch_resolved_jsonarray.getJSONObject(i).getString("flag"),Toast.LENGTH_SHORT).show();
                                }

                                disputeiv.setVisibility(View.VISIBLE);

                                final int disputePos = i ;

                                disputeiv.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        try {
                                            String dispute_raised_comment = dispatch_resolved_jsonarray.getJSONObject(disputePos).getString("dispute_comment");
                                            String dispute_resolution_comment = dispatch_resolved_jsonarray.getJSONObject(disputePos).getString("dispute_resolution_comment");
                                            String pos = Integer.toString(listPosition);

                                            DisputeDetailsDispatchDialog(dispute_raised_comment, dispute_resolution_comment, pos);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }
                                });
                            }
                           /* else
                            {
                                disputeiv.setVisibility(View.GONE);
                                Toast.makeText(SellerDispatchActivity.this,"GONE 1",Toast.LENGTH_SHORT).show();
                            }*/
                        }


                    } else {
                        disputeiv.setVisibility(View.GONE);
                        //Toast.makeText(SellerDispatchActivity.this,"GONE 2",Toast.LENGTH_SHORT).show();
                    }

            /*} else {
                disputeiv.setVisibility(View.GONE);
            }*/

            } catch (JSONException e) {
                e.printStackTrace();
            }



            try {
                listTitleTextView.setText(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("dispatch_date"));

               // Toast.makeText(SellerDispatchActivity.this,"INV NUM IS=="+ DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_number").length(),Toast.LENGTH_LONG).show();
                if(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_number").length()==4&&(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_number").equalsIgnoreCase("null"))) {
                    invoicenumtxt.setText("");

                }
                else
                {
                    invoicenumtxt.setText("Invoice No. "+DispatchDetailsJsonArray.getJSONObject(listPosition).getString("invoice_number"));

                }

                listTitleTextView.setTypeface(source_sans_pro_normal);
                invoicenumtxt.setTypeface(source_sans_pro_normal);

                if((DispatchDetailsJsonArray.getJSONObject(listPosition).getString("ack_status").equalsIgnoreCase("1"))||(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("is_active").equalsIgnoreCase("9")))
                {
                    checkBox.setEnabled(false);
                    checkBox.setBackgroundResource(R.mipmap.ic_checkbox_disabled);

                }
                else
                {
                    checkBox.setEnabled(true);
                    checkBox.setBackgroundResource(R.mipmap.ic_unchecked);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }







            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkBox.setEnabled(true);
                    checkBox.setBackgroundResource(R.mipmap.ic_checkbox);

                    //adddispatchiv.setEnabled(true);
                    //adddispatchiv.setBackgroundResource(R.mipmap.ic_add_dispatch);




                    try {
                    if(checkBox.isChecked()==false)
                    {


                        checkBox.setBackgroundResource(R.mipmap.ic_unchecked);

                        checkBox.setChecked(false);
                        selected = selected - 1;

                        for (int j = 0 ; j < dispatchIdArray.size() ; j++) {

                            if (dispatchIdArray.get(j).equalsIgnoreCase(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"))) {
                                System.out.println("DISPATCH ID BEFORE REMOVING----"+ dispatchIdArray);
                                dispatchIdArray.remove(j);

                                System.out.println("DISPATCH ID AFTER REMOVING----"+ dispatchIdArray);
                            }

                        }

                     /*   if(selected == 0)
                        {
                            deletedispatchiv.setBackgroundResource(R.mipmap.ic_delete_disabled);
                            adddispatchiv.setBackgroundResource(R.mipmap.ic_add_dispatch);

                        }

*/

                    }

                    else
                    {

                        selected = selected+1;

                        dispatchIdArray.add(DispatchDetailsJsonArray.getJSONObject(listPosition).getString("auto_dispatch_id"));

                        checkBox.setChecked(true);
                      //  positionArray.add(listPosition);



                    }



                        System.out.println("selected----"+ checkBox.getId());





                    } catch (JSONException e) {
                        e.printStackTrace();
                    }




                    if(selected == 0)
                    {
                        deletedispatchiv.setBackgroundResource(R.mipmap.ic_delete_disabled);
                        adddispatchiv.setBackgroundResource(R.mipmap.ic_add_dispatch);

                        try {
                            if(SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("transport").equalsIgnoreCase("3"))
                            {
                                adddispatchiv.setBackgroundResource(R.mipmap.edit);
                                clickonplusbtntxtvw.setText( "Transportation details will be added by Mxmart admin.");
                            }
                            else
                            {
                                adddispatchiv.setBackgroundResource(R.mipmap.ic_add_dispatch);
                                adddispatchiv.setEnabled(true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                    else
                    {
                        deletedispatchiv.setBackgroundResource(R.mipmap.ic_delete_dispatch);
                        adddispatchiv.setBackgroundResource(R.mipmap.edit);


                    }

                    if(selected > 1)
                    {

                        adddispatchiv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Toast.makeText(SellerDispatchActivity.this,"Please click one checkbox at a time",Toast.LENGTH_SHORT).show();
                               // checkBox.setChecked(false);
                               /* Intent i = new Intent(SellerDispatchActivity.this,SellerDispatchActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);*/
                            }
                        });

                       // adddispatchiv.setEnabled(false);


                    }

                    if(selected == 1)
                    {
                        adddispatchiv.setEnabled(true);

                        adddispatchiv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(selldeals.equalsIgnoreCase("0")) {
                                    // AddDispatchDialog();
                                    Intent i = new Intent(SellerDispatchActivity.this,SellerAddDispatchActivity.class);



                                        SingletonActivity.checkboxclickedposition =  listPosition;


                                    SingletonActivity.clickedaddicon = "";
                                    try {
                                        SingletonActivity.sellid = SellerCurrentDealsByTradeJsonArray.getJSONObject(0).getString("sell_id");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    startActivity(i);
                                }
                                else
                                {
                                    Toast.makeText(SellerDispatchActivity.this, "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });



                    }



                   /* else
                    {
                        adddispatchiv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Toast.makeText(SellerDispatchActivity.this,"Please click one checkbox at a time",Toast.LENGTH_SHORT).show();
                                checkBox.setChecked(false);

                            }
                        });

                        //adddispatchiv.setEnabled(false);
                        //Toast.makeText(SellerDispatchActivity.this,"SELECTED NOT POSSIBLE",Toast.LENGTH_SHORT).show();

                    }*/





                }
            });


            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            System.out.println("IN HASSTABLEIDS---");
            return false;
        }

        @Override
        public boolean isChildSelectable(int listPosition, int expandedListPosition) {
            System.out.println("IN ISCHILDSELECTABLE---");
            return true;
        }
    }

    public void DisputeDetailsDispatchDialog(String  dispute_raised_comment,String dispute_resolution_comment,String pos){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SellerDispatchActivity.this);
        LayoutInflater inflater = SellerDispatchActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.seller_dispatch_dispute_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);


        TextView disputeraiseddesctxtvw = (TextView)dialogView.findViewById(R.id.disputeraiseddesctxtvw);
        TextView resolvedcommenttxtvw = (TextView)dialogView.findViewById(R.id.resolvedcommenttxtvw);
        TextView resolvedcommentdesctxtvw = (TextView)dialogView.findViewById(R.id.resolvedcommentdesctxtvw);

        TextView closedisputetv = (TextView)dialogView.findViewById(R.id.closetxtvw);
        RelativeLayout closerelative = (RelativeLayout)dialogView.findViewById(R.id.closerelative);

        disputeraiseddesctxtvw.setText(dispute_raised_comment);

        try {

            int position = Integer.parseInt(pos);
            if(SellerCurrentDealsByTradeJsonArray.getJSONObject(position).getJSONObject("dispatch_resolved").getString("flag").equalsIgnoreCase("1"))
            {
                resolvedcommenttxtvw.setVisibility(View.VISIBLE);
                resolvedcommentdesctxtvw.setVisibility(View.VISIBLE);
                resolvedcommentdesctxtvw.setText(dispute_resolution_comment);
            }
            else
            {
                resolvedcommenttxtvw.setVisibility(View.GONE);
                resolvedcommentdesctxtvw.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }




        b = dialogBuilder.create();
        b.show();


        closerelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }

}
