package com.smtc.mxmart;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by 10161 on 11/5/2017.
 */

public class VerifyOtpActivity extends Activity {

    Typeface source_sans_pro_normal;
    EditText etMobileOTP,etNwPassword,etConfrmPassword;
    TextView verifyotptxt,genotptxt;
    RelativeLayout sbmtrelative,cancelrelative;
    UtilsDialog util = new UtilsDialog();
    ProgressDialog pdia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_verify_otp);

        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

        etMobileOTP = (EditText)findViewById(R.id.etMobileOTP);

        etNwPassword = (EditText)findViewById(R.id.etNwPassword);
        etConfrmPassword = (EditText)findViewById(R.id.etConfrmPassword);

        verifyotptxt = (TextView) findViewById(R.id.verifyotptxt);
        genotptxt = (TextView) findViewById(R.id.genotptxt);

        sbmtrelative = (RelativeLayout) findViewById(R.id.sbmtrelative);
        cancelrelative = (RelativeLayout) findViewById(R.id.cancelrelative);

        etMobileOTP.setTypeface(source_sans_pro_normal);

        etNwPassword.setTypeface(source_sans_pro_normal);
        etConfrmPassword.setTypeface(source_sans_pro_normal);
        verifyotptxt.setTypeface(source_sans_pro_normal);
        genotptxt.setTypeface(source_sans_pro_normal);

        genotptxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String emailstr = SingletonActivity.useremail;
                String mobilenumstr =  SingletonActivity.usermobile;

                    if(NetworkUtility.checkConnectivity(VerifyOtpActivity.this)){
                        String verifyusermobileurl = APIName.URL+"/user/verifyUserMobile";
                        System.out.println("VERIFY USER MOBILE URL IS---"+ verifyusermobileurl);
                        VerifyUserMobileAPI(verifyusermobileurl,emailstr,mobilenumstr);

                    }
                    else{
                        util.dialog(VerifyOtpActivity.this, "Please check your internet connection.");
                    }



            }
        });

        verifyotptxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean invalid = false;

                if (!isValidOTP(etMobileOTP.getText().toString())) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile OTP",
                            Toast.LENGTH_SHORT).show();
                }/* else if (!isValidOTP(etEmailOtp.getText().toString())) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please Enter Valid Email OTP",
                            Toast.LENGTH_SHORT).show();
                } */else if (invalid == false) {

                    if(etMobileOTP.getText().toString().equalsIgnoreCase(SingletonActivity.stotp))
                    {
                       // if(etEmailOtp.getText().toString().equalsIgnoreCase(SingletonActivity.mail_otp_str))
                       // {
                            etMobileOTP.setEnabled(false);
                          //  etEmailOtp.setEnabled(false);
                            verifyotptxt.setText("Verified");
                            genotptxt.setEnabled(false);
                            verifyotptxt.setTextColor(Color.parseColor("#e4e4e4"));
                            genotptxt.setTextColor(Color.parseColor("#e4e4e4"));
                            etNwPassword.setEnabled(true);
                            etConfrmPassword.setEnabled(true);
                            etNwPassword.setVisibility(View.VISIBLE);
                            etConfrmPassword.setVisibility(View.VISIBLE);
                            sbmtrelative.setVisibility(View.VISIBLE);
                            cancelrelative.setVisibility(View.VISIBLE);

                       /* }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Wrong Email OTP",
                                    Toast.LENGTH_SHORT).show();
                        }
*/
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Wrong Mobile OTP",
                                Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        sbmtrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(((etNwPassword.length()>5)&&(!etNwPassword.getText().toString().equalsIgnoreCase("")))&&((etConfrmPassword.length()>5)&&(!etConfrmPassword.getText().toString().equalsIgnoreCase("")))) {


                    if (etConfrmPassword.getText().toString().equalsIgnoreCase(etNwPassword.getText().toString())) {
                        if(SingletonActivity.fromforgot==false) {
                            sbmtrelative.setEnabled(false);
                            if (NetworkUtility.checkConnectivity(VerifyOtpActivity.this)) {
                                String registerusermobileurl = APIName.URL + "/user/registerUser";
                                System.out.println("VERIFY USER MOBILE URL IS---" + registerusermobileurl);
                                RegisterUserAPI(registerusermobileurl);

                            } else {
                                util.dialog(VerifyOtpActivity.this, "Please check your internet connection.");
                            }
                        }else
                        {
                          /*  if (NetworkUtility.checkConnectivity(VerifyOtpActivity.this)) {
                                String updatepwdurl = APIName.URL + "/user/updatePassword";
                                System.out.println("UPDATE PASSWORD URL IS---" + updatepwdurl);
                                UpdatePasswordAPI(updatepwdurl, etMobileNum.getText().toString(), etNewPassword.getText().toString(),SingletonActivity.stotp);


                            } else {
                                util.dialog(VerifyOtpActivity.this, "Please check your internet connection.");
                            }*/

                        }

                    } else {
                        Toast.makeText(VerifyOtpActivity.this, "Password Not Matching", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {


                        if(etConfrmPassword.length()>5&&etNwPassword.length()>5)
                        {
                            if (etConfrmPassword.getText().toString().equalsIgnoreCase(etNwPassword.getText().toString())) {

                                System.out.println("FROM FORGOT VALUE==="+ SingletonActivity.fromforgot);

                                if(SingletonActivity.fromforgot==false) {
                                    sbmtrelative.setEnabled(false);
                                    if (NetworkUtility.checkConnectivity(VerifyOtpActivity.this)) {
                                        String registerusermobileurl = APIName.URL + "/user/registerUser";
                                        System.out.println("VERIFY USER MOBILE URL IS---" + registerusermobileurl);
                                        RegisterUserAPI(registerusermobileurl);

                                    } else {
                                        util.dialog(VerifyOtpActivity.this, "Please check your internet connection.");
                                    }
                                }
                                else
                                {
                                   /* if (NetworkUtility.checkConnectivity(VerifyOtpActivity.this)) {
                                        String updatepwdurl = APIName.URL + "/user/updatePassword";
                                        System.out.println("UPDATE PASSWORD URL IS---" + updatepwdurl);
                                        UpdatePasswordAPI(updatepwdurl, etMobileNum.getText().toString(), etNewPassword.getText().toString(),SingletonActivity.stotp);


                                    } else {
                                        util.dialog(VerifyOtpActivity.this, "Please check your internet connection.");
                                    }*/
                                }

                            } else {
                                Toast.makeText(VerifyOtpActivity.this, "Password Not Matching", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else{
                            Toast.makeText(VerifyOtpActivity.this, "Please Enter Atleast 6 Digit Password", Toast.LENGTH_SHORT).show();
                        }

                    }

                }


        });

       /*



        System.out.println("FROM FORGOT 1===="+ SingletonActivity.fromforgot);

        if (SingletonActivity.fromforgot == true)
        {
            resetpasswordtxt.setVisibility(View.VISIBLE);
            etMobileNum.setVisibility(View.VISIBLE);
            etMobileNum.setEnabled(true);
            genotptxt.setVisibility(View.VISIBLE);
            etOtp.setVisibility(View.GONE);
            verifytxt.setVisibility(View.GONE);
        }
        else
        {
            resetpasswordtxt.setVisibility(View.INVISIBLE);
            etMobileNum.setVisibility(View.GONE);
            genotptxt.setVisibility(View.GONE);
        }




       */

       cancelrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  finish();
            }
        });

    }public  final boolean isValidOTP(String mobile) {
        if (mobile.length()!=6) {



          /*  Toast.makeText(this,
                    "Please Enter Valid Mobile No.", Toast.LENGTH_SHORT)
                    .show();
*/
            return false;
        } else {
            return true;
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        SingletonActivity.fromforgot = false;
    }

    private void UpdatePasswordAPI(String url, final String mobilenum, final String pwd, final String otp) {
        pdia = new ProgressDialog(VerifyOtpActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(VerifyOtpActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        System.out.println("RESPONSE OF UPDATE PASSWORD OTP API IS---" + response);


                        try {
                            //jsonArray = new JSONArray(response);


                            JSONObject updatepwdjson = new JSONObject(response);
                            System.out.println("UPDATE PASSWORD OTP RESPONSE JSON IS---" + updatepwdjson);

                            String statusstr = updatepwdjson.getString("status");





                            if (statusstr.equalsIgnoreCase("true")) {

                                AlertDialog.Builder builder1 = new AlertDialog.Builder(VerifyOtpActivity.this);
                                builder1.setMessage(updatepwdjson.getString("message"));
                                builder1.setCancelable(true);

                                builder1.setPositiveButton(
                                        "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent i = new Intent(VerifyOtpActivity.this,LoginActivity.class);
                                                startActivity(i);
                                            }
                                        });



                                AlertDialog alert11 = builder1.create();
                                alert11.show();






                            }

                            else
                            {
                                String msgstr = updatepwdjson.getString("message");
                                Toast.makeText(VerifyOtpActivity.this,msgstr,Toast.LENGTH_SHORT).show();
                            }




                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(VerifyOtpActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(VerifyOtpActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(VerifyOtpActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_num",mobilenum);
                params.put("password",pwd);
                params.put("otp",otp);
                params.put("type","1");




                System.out.println("update pwd params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void VerifyUserMobileAPI(String url,final String email,final String mobilestr) {
        pdia = new ProgressDialog(VerifyOtpActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(VerifyOtpActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        System.out.println("RESPONSE OF VERIFY USER MOBILE API IS---" + response);
                        final JSONArray jsonArray;
                        try {
                            //jsonArray = new JSONArray(response);


                            JSONObject verifyusermobilejson = new JSONObject(response);
                            System.out.println("VERIFY USER MOBILE JSON IS---" + verifyusermobilejson);

                            String statusstr = verifyusermobilejson.getString("status");

                            if (statusstr.equalsIgnoreCase("true")) {

                                String msgstr = verifyusermobilejson.getString("message");
                                String stotpstr = verifyusermobilejson.getString("st_otp");
                                String usercodestr = verifyusermobilejson.getString("user_code");
                                String mail_otp_str = verifyusermobilejson.getString("mail_otp");




                                Intent i = new Intent(VerifyOtpActivity.this,VerifyOtpActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                SingletonActivity.regmobileno = mobilestr;
                                SingletonActivity.regemail = email;
                                SingletonActivity.stotp = stotpstr;
                                SingletonActivity.mail_otp_str = mail_otp_str;
                                startActivity(i);


                            }
                            else
                            {
                                String msgstr = verifyusermobilejson.getString("message");
                                Toast.makeText(VerifyOtpActivity.this,msgstr,Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(VerifyOtpActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(VerifyOtpActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(VerifyOtpActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_num",mobilestr);
                params.put("email",email);

               /* params.put("company_name",cmpnyname);
                params.put("first_name",firstnamestr);*/


                System.out.println("verifymobileuser params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }


    private void GenerateOTPAPI(String url,final String mobilenum) {
        pdia = new ProgressDialog(VerifyOtpActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(VerifyOtpActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        System.out.println("RESPONSE OF GENERATE OTP API IS---" + response);



                        try {


                            final JSONObject generateotpjson = new JSONObject(response);
                            System.out.println("GENERATE OTP RESPONSE JSON IS---" + generateotpjson);

                            String statusstr = generateotpjson.getString("status");


                            if (statusstr.equalsIgnoreCase("true")) {

                                etMobileOTP.setEnabled(true);
                            //    etEmailOtp.setEnabled(true);
                                verifyotptxt.setText("Verify");
                                etNwPassword.setEnabled(false);
                                etConfrmPassword.setEnabled(false);

                              /*  otpstr = generateotpjson.getString("otp");
                                SingletonActivity.stotp = otpstr;
                                genotptxt.setText("Re-generate OTP");
                                etOtp.setText(otpstr);
                                etOtp.setVisibility(View.VISIBLE);
                                verifytxt.setVisibility(View.VISIBLE);
                                etMobileNum.setEnabled(false);
                             */

                            }

                            else
                            {
                                Toast.makeText(VerifyOtpActivity.this,generateotpjson.getString("message"),Toast.LENGTH_SHORT).show();
                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(VerifyOtpActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(VerifyOtpActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(VerifyOtpActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_num",mobilenum);




                System.out.println("generate otp params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void RegisterUserAPI(String url) {
        pdia = new ProgressDialog(VerifyOtpActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(VerifyOtpActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };



        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        sbmtrelative.setEnabled(false);

                        System.out.println("RESPONSE OF REGISTER USER API IS---" + response);
                        //  Toast.makeText(VerifyOtpActivity.this, response, Toast.LENGTH_SHORT).show();


                        final JSONArray jsonArray;
                        try {
                            //jsonArray = new JSONArray(response);


                            JSONObject registeruserjson = new JSONObject(response);
                            System.out.println("REGISTER USER JSON IS---" + registeruserjson);

                            String statusstr = registeruserjson.getString("status");



                            if (statusstr.equalsIgnoreCase("true")) {



                                AlertDialog.Builder builder1 = new AlertDialog.Builder(VerifyOtpActivity.this);
                                builder1.setMessage("You Have Successfully Registered.Please Login!!!");
                                builder1.setCancelable(false);

                                builder1.setPositiveButton(
                                        "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Intent i = new Intent(VerifyOtpActivity.this,LoginActivity.class);
                                                startActivity(i);
                                            }
                                        });



                                AlertDialog alert11 = builder1.create();
                                alert11.show();







                            }


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            sbmtrelative.setEnabled(true);
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();
                        sbmtrelative.setEnabled(true);

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(VerifyOtpActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(VerifyOtpActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(VerifyOtpActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_num",SingletonActivity.regmobileno);
                params.put("user_type","2");
                params.put("password",etNwPassword.getText().toString());
                params.put("is_active","1");
                params.put("email",SingletonActivity.regemail);
                params.put("st_otp",etMobileOTP.getText().toString());
                params.put("mail_otp",SingletonActivity.mail_otp_str);



                System.out.println("register user params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }


}
