package com.smtc.mxmart;

import android.app.Activity;
import android.os.PersistableBundle;

import android.os.Bundle;
import android.widget.EditText;

public class Main2Activity extends Activity {

    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        editText = (EditText)findViewById(R.id.editText);

        System.out.println("IN ONCREATE");

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        System.out.println("IN onSaveInstanceState");
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
        System.out.println("IN onRestoreInstanceState");
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("IN onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("IN onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("IN onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("IN onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("IN onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("IN onDestroy");
    }

    @Override
    public void recreate() {
        super.recreate();
        System.out.println("IN recreate");
    }
}
