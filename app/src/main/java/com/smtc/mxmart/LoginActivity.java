package com.smtc.mxmart;

import android.app.Activity;
import android.app.AppComponentFactory;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.content.DialogInterface;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.pushbots.push.Pushbots;
import com.smtc.mxmart.service.MyFirebaseInstanceIDService;
import com.smtc.mxmart.util.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import android.graphics.Paint;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import static com.smtc.mxmart.SingletonActivity.TAG;

/**
 * Created by 10161 on 11/2/2017.
 */

public class LoginActivity extends AppCompatActivity {

    Typeface source_sans_pro_normal;
    EditText etMobileNum,etPassword;
    TextView forgotpwdtxtvw,newuser,logintext;
    RelativeLayout loginrelative;
    UtilsDialog util=new UtilsDialog();
    ProgressDialog pdia;
    SessionManager session;
    boolean login;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
     BroadcastReceiver mRegistrationBroadcastReceiver;
    String regId;
    TextView tnctxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);







      /*  mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);


                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                    //txtMessage.setText(message);
                }
            }
        };

        displayFirebaseRegId();*/



        session = new SessionManager(getApplicationContext());

        etMobileNum = (EditText)findViewById(R.id.etMobileNum);
        etPassword = (EditText)findViewById(R.id.etPassword);
        tnctxt = (TextView)findViewById(R.id.tnctxts);

        forgotpwdtxtvw = (TextView)findViewById(R.id.forgotpwdtxtvw);
        newuser = (TextView)findViewById(R.id.newuser);
        logintext = (TextView)findViewById(R.id.logintext);

        loginrelative = (RelativeLayout)findViewById(R.id.loginrelative);

        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

        etMobileNum.setTypeface(source_sans_pro_normal);
        etPassword.setTypeface(source_sans_pro_normal);
        // etPassword.setText("tmxmart@2018");

    /*  etMobileNum.setText("8282828282");
      etPassword.setText("tmxmart@2018");*/

       /* etMobileNum.setText("9609609609");
        etPassword.setText("tmxmart@2018");*/

       /* etMobileNum.setText("8971131199");
        etPassword.setText("tmxmart@2018");*/

     /*  etMobileNum.setText("8095279625");
        etPassword.setText("mine12");
     */

       /* etMobileNum.setText("8050952463");
        etPassword.setText("sam123");
*/


        etPassword.setTransformationMethod(new PasswordTransformationMethod());

        forgotpwdtxtvw.setTypeface(source_sans_pro_normal);
        newuser.setTypeface(source_sans_pro_normal);
        logintext.setTypeface(source_sans_pro_normal);

        Spannable wordtoSpan = new SpannableString("New User? Sign Up");
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#FFF15922")), 9, 17, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        newuser.setText(wordtoSpan);

         tnctxt.setPaintFlags(tnctxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        tnctxt.setOnClickListener(new View.OnClickListener() {
         @Override
        public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Terms & Conditions");
        builder.setView(R.layout.activity_terms_and_conditions);
        builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // dismiss dialog
                dialogInterface.dismiss();
            }
        });
        builder.show();

            }
        });

        forgotpwdtxtvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this,ForgetPasswordActivity.class);
                SingletonActivity.fromforgot = true;
                startActivity(i);
            }
        });

        newuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(i);
            }
        });

        loginrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobilenumstr = etMobileNum.getText().toString();
                String pwdstr = etPassword.getText().toString();
                boolean invalid = false;
                if (!isValidMobileNumber(mobilenumstr)) {
                    invalid = true;
                        /*   Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number",
                                   Toast.LENGTH_SHORT).show();*/
                }
                else if (pwdstr.length()<6) {
                    invalid = true;
                  /*  Toast.makeText(getApplicationContext(),
                            "Please Enter Atleast 6 Digit Password", Toast.LENGTH_SHORT)
                            .show();
                  */
                    Toast.makeText(getApplicationContext(),
                            "Please Enter Valid Password", Toast.LENGTH_SHORT)
                            .show();
                }
                else if (invalid == false)
                {

                    SingletonActivity.fcmid = Pushbots.sharedInstance().getGCMRegistrationId();

                    regId = SingletonActivity.fcmid;

                    if(NetworkUtility.checkConnectivity(LoginActivity.this)){
                        String loginurl = APIName.URL+"/user/login";
                        System.out.println("LOGIN URL IS---"+ loginurl);
                   //   Toast.makeText(LoginActivity.this,"URL=="+ loginurl,Toast.LENGTH_SHORT).show();
                        LoginAPI(loginurl,mobilenumstr,pwdstr,regId);
                    }
                    else{
                        util.dialog(LoginActivity.this, "Please check your internet connection.");
                    }
                }
            }
        });



    }

    private void subscribeUserToParse() {
        String deviceToken = FirebaseInstanceId.getInstance().getToken();
        if (TextUtils.isEmpty(deviceToken)) {
            Intent intent = new Intent(this, MyFirebaseInstanceIDService.class);
            startService(intent);
            return;
        }


    }


    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
         regId = pref.getString("regId", null);



        if (!TextUtils.isEmpty(regId)) {
            // txtRegId.setText("Firebase Reg Id: " + regId);
         //   SingletonActivity.fcmid = regId;
            System.out.println("Firebase Reg Id in Login--:" + regId);
            regId = SingletonActivity.fcmid;
        }
        else
            // txtRegId.setText("Firebase Reg Id is not received yet!");
            System.out.println("Firebase Reg Id is not received yet!");
    }


    public  final boolean isValidMobileNumber(String mobile) {
        if (mobile.length()!=10) {

            Toast.makeText(this,
                    "Please Enter Valid Mobile No.", Toast.LENGTH_SHORT)
                    .show();

            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(LoginActivity.this,LoginActivity.class);
        startActivity(i);

    }



    private void LoginAPI(String url, final String mobilenum, final String pwd,final String regId) {
        pdia = new ProgressDialog(LoginActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(LoginActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();

                        System.out.println("RESPONSE OF LOGIN API IS---" + response);

                      // Toast.makeText(getApplicationContext(), response,
                         //       Toast.LENGTH_SHORT).show();
                        final JSONArray jsonArray;
                        try {
                            //jsonArray = new JSONArray(response);


                            JSONObject loginuserjson = new JSONObject(response);
                            System.out.println("LOGIN USER JSON IS---" + loginuserjson);

                            String statusstr = loginuserjson.getString("status");

                            if (statusstr.equalsIgnoreCase("true")) {

                                String lastPostedCountStr = loginuserjson.getString("lastPostedCount");
                                System.out.println("lastPostedCountStr IS---" + lastPostedCountStr);
                                JSONObject userdatajson = loginuserjson.getJSONObject("userData");
                                String usercodestr = userdatajson.getString("user_code");
                                String mobilestr = userdatajson.getString("mobile_num");
                                String useridstr =  userdatajson.getString("id");

                                JSONObject module_permission_json = userdatajson.getJSONObject("module_permission");

                                System.out.println("USER CODE IS----"+ usercodestr);

                                String livesales = module_permission_json.getString("livesales");
                                String todaysoffer = module_permission_json.getString("todaysoffer");
                                String sellenquiry = module_permission_json.getString("sellenquiry");
                                String selldeals = module_permission_json.getString("selldeals");
                                String buyenquiry = module_permission_json.getString("buyenquiry");
                                String buydeals = module_permission_json.getString("buydeals");


                                session.createLoginSession();

                                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString("user_code", usercodestr);
                                editor.putString("mobile_num",mobilestr);
                                editor.putBoolean("login",true);
                                editor.putString("password",pwd);
                                editor.putString("fcm_id",regId);
                                editor.putString("livesales",livesales);
                                editor.putString("todaysoffer",todaysoffer);
                                editor.putString("sellenquiry",sellenquiry);
                                editor.putString("selldeals",selldeals);
                                editor.putString("buyenquiry",buyenquiry);
                                editor.putString("buydeals",buydeals);
                                editor.putString("userid",useridstr);
                                editor.putString("lastPostedCount",lastPostedCountStr);





                                editor.commit();


                               Intent i = new Intent(LoginActivity.this,HomeActivity.class);
                                SingletonActivity.isPassedFromLogin  = true;
                                startActivity(i);









                            }

                            else {
                                String msgstr = loginuserjson.getString("message");
                                Toast.makeText(LoginActivity.this,msgstr,Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(LoginActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(LoginActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_num",mobilenum);
                params.put("password",pwd);
                params.put("registration_Id",regId);
                params.put("deviceType","1");

                System.out.println("login params---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }


}
