package com.smtc.mxmart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
//import android.icu.text.SimpleDateFormat;

import android.os.Build;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class ViewLiveTradeActivity1012 extends AppCompatActivity {

    public static final String MY_PREFS_NAME = "MyPrefsFile";
    private ImageView backicon;
    Spinner breakdownprice_spinner;
    RadioButton advancepriceradiobtn,nextpriceradiobtn,regularpriceradiobtn;
    EditText buying_quantity_edittext,enquiry_price;
    TextView companynametxtvw,categorydesctxtvw,subcategorydesctxtvw,gradedesctxtvw,qtyoffereddesctxtvw,termsandconditionsdesctxtvw,promodesctxtvw;
    TextView buytext,pricetxt1,pricetxt2,pricetxt3,pricetxt4,pricetxt5,pricetxt6,pricetxt7,pricetxt8;
    String q1val,q2val,q3val,q4val,q5val,q6val,q7val,q8val;
    ArrayList<Integer> qtyal = new ArrayList<>();
    RelativeLayout buyrelative;
    RadioGroup priceradiogroup;
    UtilsDialog util = new UtilsDialog();
    ProgressDialog pdia;
    AlertDialog b;
    View line4,line5;
    ImageView profileiv;
    SessionManager sessionManager;
    TextView pricetitletxtvw,locationdesctxt,lengthtitletxtvw,lengthdesctxt,q1title,q2title,q3title,q4title,q5title,q6title,q7title,q8title,transportationdesctxt,loadingdesctxt,gstdesctxt,insurancedesctxt;
    String q1_title_str,q1_value_str,q2_title_str,q2_value_str,q3_title_str,q3_value_str,q4_title_str,q4_value_str,q5_title_str,q5_value_str,q6_title_str,q6_value_str,q7_title_str,q7_value_str,q8_title_str,q8_value_str,transport_str,loading_str,gst_str,insurance_select_str,insurance_str;
    String user_code_str,livesales,category_grade_name_str,size_range_start_str,size_range_end_str,fcm_id,modified_field,ratetypestr,user_code,userCodestr,mobilenumstr,addlimitstr,quantity_val,quoted_rate_val,promo_code,location,lengthstr,companyname,categorydesc,subcategorydesc,advanceratedesc,nextdayratedesc,regularratedesc,remaining_quantity,sellidstr,categoryidstr,subcategoryidstr,tncstr;
    String whenstr,delivery_type_str,delivery_in_days_str,material_inspection_str,weighment_type_str,test_certificate_type_str;
    TextView deliveryindaystitletxtvw,deliveryindaysdesctxt,materialinspectiondesctxt,weighmentdesctxt,testcertificatedesctxt;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_live_trade_dec);

       /* Toolbar toolbar = (Toolbar) findViewById(R.id.viewtradetoolbar);
        setSupportActionBar(toolbar);*/

        sessionManager = new SessionManager(getApplicationContext());

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id",null);
        livesales = prefs.getString("livesales",null);


        pricetitletxtvw = (TextView)findViewById(R.id.pricetitletxtvw);
        deliveryindaysdesctxt = (TextView)findViewById(R.id.deliveryindaysdesctxt);
        materialinspectiondesctxt = (TextView)findViewById(R.id.materialinspectiondesctxt);
        weighmentdesctxt = (TextView)findViewById(R.id.weighmentdesctxt);
        testcertificatedesctxt = (TextView)findViewById(R.id.testcertificatedesctxt);
        buying_quantity_edittext = (EditText)findViewById(R.id.buying_quantity_edittext);
        enquiry_price = (EditText)findViewById(R.id.enquiry_price);


        profileiv = (ImageView)findViewById(R.id.profileiv); 
        line4 = (View)findViewById(R.id.line4);
        lengthtitletxtvw = (TextView)findViewById(R.id.lengthtitletxtvw);
        lengthdesctxt = (TextView)findViewById(R.id.lengthdesctxt);
        priceradiogroup = (RadioGroup)findViewById(R.id.priceradiogroup);
        companynametxtvw = (TextView) findViewById(R.id.companynametxtvw);
        categorydesctxtvw = (TextView) findViewById(R.id.categorydesctxtvw);
        subcategorydesctxtvw = (TextView) findViewById(R.id.subcategorydesctxtvw);
        gradedesctxtvw = (TextView) findViewById(R.id.gradedesctxtvw);
        advancepriceradiobtn = (RadioButton) findViewById(R.id.advancepriceradiobtn);
        nextpriceradiobtn = (RadioButton) findViewById(R.id.nextpriceradiobtn);
        regularpriceradiobtn = (RadioButton) findViewById(R.id.regularpriceradiobtn);
        qtyoffereddesctxtvw = (TextView) findViewById(R.id.qtyoffereddesctxtvw);
        pricetxt1 = (TextView) findViewById(R.id.pricetxt1);
        pricetxt2 = (TextView) findViewById(R.id.pricetxt2);
        pricetxt3 = (TextView) findViewById(R.id.pricetxt3);
        pricetxt4 = (TextView) findViewById(R.id.pricetxt4);
        pricetxt5 = (TextView) findViewById(R.id.pricetxt5);
        pricetxt6 = (TextView) findViewById(R.id.pricetxt6);
        pricetxt7 = (TextView) findViewById(R.id.pricetxt7);
        pricetxt8 = (TextView) findViewById(R.id.pricetxt8);
        termsandconditionsdesctxtvw = (TextView) findViewById(R.id.termsandconditionsdesctxtvw);
        promodesctxtvw = (TextView) findViewById(R.id.promodesctxtvw);
        buying_quantity_edittext = (EditText) findViewById(R.id.buying_quantity_edittext);
        breakdownprice_spinner = (Spinner) findViewById(R.id.markdown_price_spinner);
        buyrelative = (RelativeLayout) findViewById(R.id.buyrelative);
        buytext = (TextView) findViewById(R.id.buytext);
        q1title = (TextView) findViewById(R.id.q1title);
        q2title = (TextView) findViewById(R.id.q2title);
        q3title = (TextView) findViewById(R.id.q3title);
        q4title = (TextView) findViewById(R.id.q4title);
        q5title = (TextView) findViewById(R.id.q5title);
        q6title = (TextView) findViewById(R.id.q6title);
        q7title = (TextView) findViewById(R.id.q7title);
        q8title = (TextView) findViewById(R.id.q8title);
        transportationdesctxt = (TextView) findViewById(R.id.transportationdesctxt);
        loadingdesctxt = (TextView) findViewById(R.id.loadingdesctxt);
        gstdesctxt = (TextView) findViewById(R.id.gstdesctxt);
        insurancedesctxt = (TextView) findViewById(R.id.insurancedesctxt);
        locationdesctxt = (TextView) findViewById(R.id.locationdesctxt);

        Bundle b = new Bundle();
        b = getIntent().getExtras();

        if (b != null) {

            lengthstr = b.getString("length");
            companyname = b.getString("companyname");
            categorydesc = b.getString("categorydesc");
            subcategorydesc = b.getString("subcategorydesc");
            advanceratedesc = b.getString("advanceratedesc");
            nextdayratedesc = b.getString("nextdayratedesc");
            regularratedesc = b.getString("regularratedesc");
            remaining_quantity = b.getString("remaining_quantity");
            sellidstr = b.getString("sellidstr");
            categoryidstr = b.getString("categoryidstr");
            subcategoryidstr = b.getString("subcategoryidstr");
            tncstr = b.getString("tncstr");
            q1val = b.getString("q1val");
            q2val = b.getString("q2val");
            q3val = b.getString("q3val");
            q4val = b.getString("q4val");
            q5val = b.getString("q5val");
            q6val = b.getString("q6val");
            q7val = b.getString("q7val");
            q8val = b.getString("q8val");
            location = b.getString("location");
            promo_code = b.getString("promo_code");
            addlimitstr = b.getString("addlimitstr");
            user_code = b.getString("user_code");
            modified_field = b.getString("modified_field");
            size_range_start_str = b.getString("size_range_start_str");
            size_range_end_str = b.getString("size_range_end_str");
            q1_title_str =  b.getString("q1_title");
            q1_value_str =  b.getString("q1_value");
            q2_title_str = b.getString("q2_title");
            q2_value_str = b.getString("q2_value");
            q3_title_str = b.getString("q3_title");
            q3_value_str = b.getString("q3_value");
            q4_title_str = b.getString("q4_title");
            q4_value_str = b.getString("q4_value");
            q5_title_str = b.getString("q5_title");
            q5_value_str = b.getString("q5_value");
            q6_title_str = b.getString("q6_title");
            q6_value_str = b.getString("q6_value");
            q7_title_str = b.getString("q7_title");
            q7_value_str = b.getString("q7_value");
            q8_title_str = b.getString("q8_title");
            q8_value_str = b.getString("q8_value");
            transport_str = b.getString("transport");
            loading_str = b.getString("loading");
            gst_str = b.getString("gst");
            insurance_select_str = b.getString("insurance_select");
            insurance_str = b.getString("insurance");
            category_grade_name_str = b.getString("category_grade_name");
            user_code_str = b.getString("user_code_str");

            delivery_type_str = b.getString("delivery_type");
            delivery_in_days_str = b.getString("delivery_in_days");
            material_inspection_str = b.getString("material_inspection");
            weighment_type_str = b.getString("weighment_type");
            test_certificate_type_str = b.getString("test_certificate_type");

            whenstr = b.getString("when");


        }
        System.out.println("addlimitstr---" + addlimitstr);



        if (NetworkUtility.checkConnectivity(ViewLiveTradeActivity1012.this)) {
            String getuserdetailsurl = APIName.URL + "/user/getUserDetails?user_code=" + user_code_str;
            System.out.println("GET USER DETAILS URL IN VIEW OFFER IS---" + getuserdetailsurl);
            GetUserDetailsAPI(getuserdetailsurl);

        } else {
            util.dialog(ViewLiveTradeActivity1012.this, "Please check your internet connection.");
        }

        locationdesctxt.setText(location);

       /* line4 = (View)findViewById(R.id.line4);
        lengthtitletxtvw = (TextView)findViewById(R.id.lengthtitletxtvw);
        lengthdesctxt = (TextView)findViewById(R.id.lengthdesctxt);*/
       // Toast.makeText(ViewLiveTradeActivity.this,"LENGTH IS"+ lengthstr.length(),Toast.LENGTH_SHORT).show();

        if(lengthstr.length()==4){


            line4.setVisibility(View.GONE);
            lengthtitletxtvw.setVisibility(View.GONE);
            lengthdesctxt.setVisibility(View.GONE);


        }
        else
        {
           // lengthdesctxt.setText(lengthstr+ " m");
            line4.setVisibility(View.VISIBLE);
            lengthtitletxtvw.setVisibility(View.VISIBLE);
            lengthdesctxt.setVisibility(View.VISIBLE);

            if(categoryidstr.equalsIgnoreCase("1"))
            {
                lengthtitletxtvw.setText("Length");
                lengthdesctxt.setText(lengthstr+ " m");
            }

            if(categoryidstr.equalsIgnoreCase("2"))
            {
                lengthtitletxtvw.setText("Weight");
                lengthdesctxt.setText(lengthstr+ " kg");
            }
        }


        if(SingletonActivity.isdailylimitAvailable == false)
        {
            advancepriceradiobtn.setEnabled(false);
            nextpriceradiobtn.setEnabled(false);
            regularpriceradiobtn.setEnabled(false);
            buying_quantity_edittext.setEnabled(false);
            breakdownprice_spinner.setEnabled(false);
            buyrelative.setEnabled(false);


        }

        q1title.setText(q1_title_str);
        q2title.setText(q2_title_str);
        q3title.setText(q3_title_str);
        q4title.setText(q4_title_str);
        q5title.setText(q5_title_str);
        q6title.setText(q6_title_str);
        q7title.setText(q7_title_str);
        q8title.setText(q8_title_str);


        pricetxt1.setText(q1_value_str);
        pricetxt2.setText(q2_value_str);
        pricetxt3.setText(q3_value_str);
        pricetxt4.setText(q4_value_str);
        pricetxt5.setText(q5_value_str);
        pricetxt6.setText(q6_value_str);

        enquiry_price.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable edt) {

                String temp = edt.toString ();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 2) {
                    edt.delete(posDot + 3, posDot + 4);
                }
            }
        });

        if(delivery_type_str.equalsIgnoreCase("1"))
        {
            deliveryindaysdesctxt.setText("As per Aachar Sanhita");
        }
        if(delivery_type_str.equalsIgnoreCase("2"))
        {
            deliveryindaysdesctxt.setText(delivery_in_days_str+ " days");
        }

        if(material_inspection_str.equalsIgnoreCase("1"))
        {
            materialinspectiondesctxt.setText("Buyer's Place");
        }
        if(material_inspection_str.equalsIgnoreCase("2"))
        {
            materialinspectiondesctxt.setText("Seller's Place");
        }

        if(weighment_type_str.equalsIgnoreCase("1"))
        {
            weighmentdesctxt.setText("Buyer's Place");
        }
        if(weighment_type_str.equalsIgnoreCase("2"))
        {
            weighmentdesctxt.setText("Seller's Place");
        }

        if(weighment_type_str.equalsIgnoreCase("3"))
        {
            weighmentdesctxt.setText("Buyer & Seller's Place");
        }

        if(test_certificate_type_str.equalsIgnoreCase("1"))
        {
           // testcertificatedesctxt.setText("Required");
            testcertificatedesctxt.setText("Yes");
        }
        if(test_certificate_type_str.equalsIgnoreCase("2"))
        {
          //  testcertificatedesctxt.setText("Not Required");
            testcertificatedesctxt.setText("No");
        }


        if(!q7_value_str.equalsIgnoreCase(""))
        {
            pricetxt7.setText(q7_value_str);
        }
        else {
            pricetxt7.setText("NA");
        }

        if(!q8_value_str.equalsIgnoreCase(""))
        {
            pricetxt8.setText(q8_value_str);
        }
        else
        {
            pricetxt8.setText("NA");
        }


        if(transport_str.equalsIgnoreCase("1"))
        {
            transportationdesctxt.setText("Seller's End");
        }
        if(transport_str.equalsIgnoreCase("2"))
        {
            transportationdesctxt.setText("Buyer's End");
        }
        if(transport_str.equalsIgnoreCase("3"))
        {
            transportationdesctxt.setText("Mxmart's End");
        }

        if(loading_str.equalsIgnoreCase("NA"))
        {
            loadingdesctxt.setText(loading_str);
        }
        else {
          //  loadingdesctxt.setText(loading_str + " Rs/MT");
            loadingdesctxt.setText(loading_str + " \u20B9"+"/MT");
        }
        gstdesctxt.setText(gst_str);

        if(insurance_select_str.equalsIgnoreCase("NA"))
        {
            insurancedesctxt.setText("NA");
        }
        else {

            if (insurance_str.equalsIgnoreCase("0")) {
                insurancedesctxt.setText("NA");
            }

            if (insurance_str.equalsIgnoreCase("1")) {
                insurancedesctxt.setText("Seller");
            }

            if (insurance_str.equalsIgnoreCase("2")) {
                {
                    insurancedesctxt.setText("Buyer");
                }
            }
        }


        if(categoryidstr.equalsIgnoreCase("1")||categoryidstr.equalsIgnoreCase("2")) {
            subcategorydesctxtvw.setText(subcategorydesc);
        }
        else {
            subcategorydesctxtvw.setText(size_range_start_str + "-" + size_range_end_str);
        }


        if(size_range_start_str.equalsIgnoreCase("null")&&(size_range_end_str.equalsIgnoreCase("null")))
        {
            subcategorydesctxtvw.setText("--");

        }


        companynametxtvw.setText(companyname);
        categorydesctxtvw.setText(categorydesc);
      //  subcategorydesctxtvw.setText(subcategorydesc);
        if (category_grade_name_str.equalsIgnoreCase("")) {
            gradedesctxtvw.setText("--");
        } else {
            gradedesctxtvw.setText(category_grade_name_str);
        }


        advancepriceradiobtn.setText(advanceratedesc + "(A)");
        nextpriceradiobtn.setText(nextdayratedesc + "(N)");
        regularpriceradiobtn.setText(regularratedesc + "(R)");


        System.out.println("advanceratedesc radio button---" + advanceratedesc);
        System.out.println("nextdayratedesc radio button---" + nextdayratedesc);
        System.out.println("regularratedesc radio button---" + regularratedesc);


        if (advanceratedesc.equalsIgnoreCase("0.00")) {


            advancepriceradiobtn.setEnabled(false);
            advancepriceradiobtn.setClickable(false);
            advancepriceradiobtn.setBackgroundColor(Color.parseColor("#e4e4e4"));
        }


        if (nextdayratedesc.equalsIgnoreCase("0.00")) {

            nextpriceradiobtn.setEnabled(false);
            nextpriceradiobtn.setClickable(false);
            nextpriceradiobtn.setBackgroundColor(Color.parseColor("#e4e4e4"));
        }

        if (regularratedesc.equalsIgnoreCase("0.00")) {

            regularpriceradiobtn.setEnabled(false);
            regularpriceradiobtn.setClickable(false);
            regularpriceradiobtn.setBackgroundColor(Color.parseColor("#e4e4e4"));
        }

        String datestr = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
      //  Toast.makeText(ViewLiveTradeActivity1012.this,"CURRENT SYSTEM DATE : ="+ datestr,Toast.LENGTH_SHORT).show();

        String str = whenstr;
        String[] splitStr = str.split("\\s+");

        if(splitStr[0].equalsIgnoreCase(datestr))
        {

            buytext.setText("BUY");
            breakdownprice_spinner.setBackgroundResource(R.drawable.trade_rounded_rectangle);
            priceradiogroup.setVisibility(View.VISIBLE);
            enquiry_price.setVisibility(View.INVISIBLE);
            pricetitletxtvw.setText("Price (P MT)");


        }
        else
        {
            buytext.setText("ENQUIRY");
            breakdownprice_spinner.setEnabled(false);
            breakdownprice_spinner.setBackgroundResource(R.drawable.grey_btn_rounded_rectangle);
            priceradiogroup.setVisibility(View.INVISIBLE);
            enquiry_price.setVisibility(View.VISIBLE);
            pricetitletxtvw.setText("Next Day Price (P MT)");

            advanceratedesc = "0.00";
            nextdayratedesc = "0.00";
            regularratedesc = "0.00";
        }

        priceradiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                View radioButton = radioGroup.findViewById(i);
                int index = radioGroup.indexOfChild(radioButton);
                System.out.println("index of radio button---" + index);

                if (index == 0) {
                    ratetypestr = "1";
                }

                if (index == 1) {
                    ratetypestr = "2";
                }
                if (index == 2) {
                    ratetypestr = "3";
                }
            }
        });




        qtyoffereddesctxtvw.setText(remaining_quantity);


        if(tncstr.equalsIgnoreCase(""))
        {
            termsandconditionsdesctxtvw.setText("--");
        }
        else
        {
            termsandconditionsdesctxtvw.setText(tncstr);
        }

     // Toast.makeText(ViewLiveTradeActivity.this,"promo IS"+ promo_code,Toast.LENGTH_SHORT).show();
        if(promo_code.equalsIgnoreCase("0")||promo_code.equalsIgnoreCase("(null)"))
        {
            promodesctxtvw.setText("--");
        }
        else {
            promodesctxtvw.setText(promo_code);
        }



        backicon = (ImageView)findViewById(R.id.backicon);




        backicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // finish();


                Intent i = new Intent(ViewLiveTradeActivity1012.this,HomeActivity.class);
                SingletonActivity.fromviewlivetrade = true;
                SingletonActivity.enquiryfromplatespage = false;
                SingletonActivity.fromviewpigiron = false;
                SingletonActivity.fromaddnewoffer = false;
                SingletonActivity.isNotificationClicked = false;
                // SingletonActivity.index = index;
                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                SingletonActivity.FromAddNewOfferTabOne=false;
                SingletonActivity.FromAddNewOfferTabZero=false;
                SingletonActivity.isNewEnquiryClicked = false;
                SingletonActivity.frombackofbuyerdispatch = false;
                SingletonActivity.frombackofsellerdispatch = false;
                SingletonActivity.frombackofbuyerpayment = false;
                SingletonActivity.frombackofsellerpayment = false;


                startActivity(i);
            }
        });



        List<String> breakdownpricelist = new ArrayList<String>();
        breakdownpricelist.add("0");
        breakdownpricelist.add("50");
        breakdownpricelist.add("100");
        breakdownpricelist.add("150");
        breakdownpricelist.add("200");
        breakdownpricelist.add("250");
        breakdownpricelist.add("300");
        breakdownpricelist.add("350");
        breakdownpricelist.add("400");
        breakdownpricelist.add("450");
        breakdownpricelist.add("500");


        ArrayAdapter<String> breakdownpriceAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, breakdownpricelist);
        breakdownpriceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        breakdownprice_spinner.setSelection(0);
        breakdownprice_spinner.setAdapter(breakdownpriceAdapter);
        breakdownpriceAdapter.notifyDataSetChanged();


        breakdownprice_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                quoted_rate_val = parent.getItemAtPosition(position).toString();
                System.out.println("QUOTED RATE SELECTED VAL---" + quoted_rate_val);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });


        if (addlimitstr!=null) {

            int addlimitint = Integer.parseInt(addlimitstr);

            quantity_val = buying_quantity_edittext.getText().toString();
            buying_quantity_edittext.setEnabled(true);
        /*    int remaining_quantity_int = Integer.parseInt(remaining_quantity);

            System.out.println("MINIMUM VALUE IN BUY DIALOG---" + Math.min(addlimitint, remaining_quantity_int));

            int minimumValue = Math.min(addlimitint, remaining_quantity_int);
*/

           // qtyal.clear();

          //  qtyal.add(0);
            /*for (int i = 50; i <= addlimitint; i = i + 50) {

                qtyal.add(i);
            }*/
            System.out.println("QTY VALUE ARRAYLIST IN BUY DIALOG---" + qtyal);




          /*  ArrayAdapter<Integer> quantityAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_dropdown_item, qtyal);
            quantityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            quantity_spinner.setSelection(0);
            quantity_spinner.setAdapter(quantityAdapter);
            quantityAdapter.notifyDataSetChanged();
*/

          /*  quantity_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    quantity_val = parent.getItemAtPosition(position).toString();
                    System.out.println("QUANTITY SELECTED VAL---" + quantity_val);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }


            });*/
        }
        else
        {
            buying_quantity_edittext.setEnabled(false);
            buying_quantity_edittext.setText("0");
           // quantity_spinner.setEnabled(false);
           // qtyal.add(0);
           /* ArrayAdapter<Integer> quantityAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_dropdown_item, qtyal);
            quantityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            quantity_spinner.setSelection(0);
            quantity_spinner.setAdapter(quantityAdapter);
            quantityAdapter.notifyDataSetChanged();*/

            breakdownprice_spinner.setEnabled(false);
        }

        if(userCodestr.equalsIgnoreCase(user_code))
        {
            advancepriceradiobtn.setEnabled(false);
            nextpriceradiobtn.setEnabled(false);
            regularpriceradiobtn.setEnabled(false);

            buyrelative.setEnabled(false);
            breakdownprice_spinner.setEnabled(false);
            buying_quantity_edittext.setText("0");
            buying_quantity_edittext.setEnabled(false);
            buyrelative.setBackgroundResource(R.drawable.grey_btn_rounded_rectangle);

        }
        else
        {
            advancepriceradiobtn.setEnabled(true);
            nextpriceradiobtn.setEnabled(true);
            regularpriceradiobtn.setEnabled(true);

            buyrelative.setEnabled(true);
            buyrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);

            if (addlimitstr!=null) {

                buyrelative.setEnabled(true);
                buyrelative.setBackgroundResource(R.drawable.red_btn_rounded_rectangle);

            }
            else
            {
                buyrelative.setEnabled(false);
                breakdownprice_spinner.setEnabled(false);
                buying_quantity_edittext.setEnabled(false);
                buyrelative.setBackgroundResource(R.drawable.grey_btn_rounded_rectangle);

            }

        }

        buyrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (livesales.equalsIgnoreCase("0")) {

                    if (advanceratedesc.equalsIgnoreCase("0.00") && (nextdayratedesc.equalsIgnoreCase("0.00") && (regularratedesc.equalsIgnoreCase("0.00")))) {

                        boolean invalid = false;

                        if(enquiry_price.getText().toString().equalsIgnoreCase("")||(Integer.parseInt(enquiry_price.getText().toString())==0))
                        {
                            invalid = true;
                            Toast.makeText(getApplicationContext(), "Please Enter Valid Price",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else if (buying_quantity_edittext.getText().toString().equals("0") || buying_quantity_edittext.getText().toString().equals("")) {
                            invalid = true;
                            Toast.makeText(getApplicationContext(), "Please Enter Valid Buying Quantity",
                                    Toast.LENGTH_SHORT).show();
                        } else if (Integer.parseInt(buying_quantity_edittext.getText().toString()) < 20) {
                            invalid = true;
                            Toast.makeText(getApplicationContext(), "Buying quantity should not be less than 20",
                                    Toast.LENGTH_SHORT).show();
                        } else if (Integer.parseInt(buying_quantity_edittext.getText().toString()) > Integer.parseInt(addlimitstr)) {
                            invalid = true;
                            Toast.makeText(getApplicationContext(), "Quantity should be less than buying limit",
                                    Toast.LENGTH_SHORT).show();
                        } else if (invalid == false) {
                            TermsandConditionsDialog(quoted_rate_val, buying_quantity_edittext.getText().toString(), tncstr);


                        }



                    } else {

                        String quotedrate = quoted_rate_val;
                        String qtystr = quantity_val;

                        boolean invalid = false;


                        if ((advancepriceradiobtn.isChecked() == false) && (nextpriceradiobtn.isChecked() == false) && (regularpriceradiobtn.isChecked() == false)) {
                            invalid = true;
                            Toast.makeText(getApplicationContext(), "Please Select Price",
                                    Toast.LENGTH_SHORT).show();
                        }

             /*   else if(quoted_rate_val.equals(""))
                {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please Select Mark Down Price",
                            Toast.LENGTH_SHORT).show();
                }*/
                        else if (buying_quantity_edittext.getText().toString().equals("0") || buying_quantity_edittext.getText().toString().equals("")) {
                            invalid = true;
                            Toast.makeText(getApplicationContext(), "Please Enter Valid Buying Quantity",
                                    Toast.LENGTH_SHORT).show();
                        } else if (Integer.parseInt(buying_quantity_edittext.getText().toString()) < 20) {
                            invalid = true;
                            Toast.makeText(getApplicationContext(), "Buying quantity should not be less than 20",
                                    Toast.LENGTH_SHORT).show();
                        } else if (Integer.parseInt(buying_quantity_edittext.getText().toString()) > Integer.parseInt(addlimitstr)) {
                            invalid = true;
                            Toast.makeText(getApplicationContext(), "Quantity should be less than buying limit",
                                    Toast.LENGTH_SHORT).show();
                        } else if (invalid == false) {
                            TermsandConditionsDialog(quoted_rate_val, buying_quantity_edittext.getText().toString(), tncstr);

                 /*   if (NetworkUtility.checkConnectivity(ViewLiveTradeActivity.this)) {
                        String InsertTradeUrl = APIName.URL + "/liveTrading/insertTrade";
                        System.out.println("INSERT TRADE URL IS---" + InsertTradeUrl);


                        InsertTradeAPI(InsertTradeUrl, sellidstr, categoryidstr, subcategoryidstr, tncstr, ratetypestr, quotedrate, qtystr);

                    } else {
                        util.dialog(ViewLiveTradeActivity.this, "Please check your internet connection.");
                    }*/
                        }
                    }
                }
                else
                {
                    Toast.makeText(ViewLiveTradeActivity1012.this, "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        List<String> modified_field_List = Arrays.asList(modified_field.split(","));




        if (modified_field_List.contains("-12-")) {
            advancepriceradiobtn.setTextColor(Color.RED);
        }

        if (modified_field_List.contains("-14-"))
        {
            regularpriceradiobtn.setTextColor(Color.RED);
        }

        if (modified_field_List.contains("-13-")) {
            nextpriceradiobtn.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-2-")) {
            subcategorydesctxtvw.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-3-"))
        {
            pricetxt1.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-4-"))
        {
            pricetxt2.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-5-"))
        {
            pricetxt3.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-6-"))
        {
            pricetxt4.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-7-"))
        {
            pricetxt5.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-8-"))
        {
            pricetxt6.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-9-"))
        {
            pricetxt7.setTextColor(Color.RED);
        }
        if (modified_field_List.contains("-10-"))
        {
            pricetxt8.setTextColor(Color.RED);
        }

        if (modified_field_List.contains("-16-"))
        {
            termsandconditionsdesctxtvw.setTextColor(Color.RED);
        }

        if (modified_field_List.contains("-17-"))
        {
            lengthdesctxt.setTextColor(Color.RED);
        }





    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


        Intent i = new Intent(ViewLiveTradeActivity1012.this,HomeActivity.class);
        SingletonActivity.fromviewlivetrade = true;
        SingletonActivity.enquiryfromplatespage = false;
        SingletonActivity.fromviewpigiron = false;
        SingletonActivity.fromaddnewoffer = false;
        SingletonActivity.isNotificationClicked = false;
        // SingletonActivity.index = index;
        SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
        SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
        SingletonActivity.FromAddNewOfferTabOne=false;
        SingletonActivity.FromAddNewOfferTabZero=false;
        SingletonActivity.isNewEnquiryClicked = false;
        SingletonActivity.frombackofbuyerdispatch = false;
        SingletonActivity.frombackofsellerdispatch = false;
        SingletonActivity.frombackofbuyerpayment = false;
        SingletonActivity.frombackofsellerpayment = false;


        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
       //  return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                System.out.println("OVERFLOWMENU ITEM 1 CLICKED");

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("login",false);


                editor.clear();
                editor.commit();


                sessionManager.logoutUser(mobilenumstr,fcm_id);


                // sessionManager.logoutUser();

            /*    Intent i = new Intent(LeaveManagementActivity.this,LoginActivity.class);
                startActivity(i);
*/
                return true;
          /*  case R.id.overflowmenuitem2:
                System.out.println("OVERFLOWMENU ITEM 2 CLICKED");

                return true;
            case R.id.overflowmenuitem3:
                System.out.println("OVERFLOWMENU ITEM 3 CLICKED");

                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void TermsandConditionsDialog(final String quotedrate,final String qtystr,final String tncstr){

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ViewLiveTradeActivity1012.this);
        LayoutInflater inflater = ViewLiveTradeActivity1012.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.trade_termsnconditions_popup, null);
        dialogBuilder.setView(dialogView);



            //final EditText dispatch_date_edt = (EditText)dialogView.findViewById(R.id.dispatch_date_edt);



        final EditText commentdesctxt = (EditText) dialogView.findViewById(R.id.commentdesctxt);
        if(tncstr.equalsIgnoreCase(""))
        {

        }
        else {
            commentdesctxt.setText(tncstr);
        }
        RelativeLayout buybtnrelative = (RelativeLayout)dialogView.findViewById(R.id.buybtnrelative);
        RelativeLayout cancelbtnrelative = (RelativeLayout)dialogView.findViewById(R.id.cancelbtnrelative);
        TextView sbmttext = (TextView)dialogView.findViewById(R.id.sbmttext);




        buybtnrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                if (advanceratedesc.equalsIgnoreCase("0.00") && (nextdayratedesc.equalsIgnoreCase("0.00") && (regularratedesc.equalsIgnoreCase("0.00")))) {

                    if (NetworkUtility.checkConnectivity(ViewLiveTradeActivity1012.this)) {
                        buyrelative.setEnabled(false);
                        String InsertEnquiryUrl = APIName.URL + "/liveTrading/insertEnquiry";
                        System.out.println("INSERT ENQUIRY URL IS---" + InsertEnquiryUrl);


                        InsertEnquiryAPI(InsertEnquiryUrl, sellidstr, categoryidstr, subcategoryidstr, tncstr, ratetypestr, quotedrate, qtystr, commentdesctxt.getText().toString());

                    } else {
                        util.dialog(ViewLiveTradeActivity1012.this, "Please check your internet connection.");
                    }

                } else {

                    if (NetworkUtility.checkConnectivity(ViewLiveTradeActivity1012.this)) {
                        buyrelative.setEnabled(false);
                        String InsertTradeUrl = APIName.URL + "/liveTrading/insertTrade";
                        System.out.println("INSERT TRADE URL IS---" + InsertTradeUrl);


                        InsertTradeAPI(InsertTradeUrl, sellidstr, categoryidstr, subcategoryidstr, tncstr, ratetypestr, quotedrate, qtystr, commentdesctxt.getText().toString());

                    } else {
                        util.dialog(ViewLiveTradeActivity1012.this, "Please check your internet connection.");
                    }


                }
            }
        });



        b = dialogBuilder.create();
        b.show();


        cancelbtnrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }

    private void GetUserDetailsAPI(String url) {


        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(ViewLiveTradeActivity1012.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //pdia.dismiss();

                        System.out.println("RESPONSE OF GET USER DETAILS API IS---" + response);


                        try {

                            JSONObject userdetailsjson = new JSONObject(response);


                        

                            final JSONObject userdetjson = userdetailsjson.getJSONObject("userDetails");
                            System.out.println("USER DET JSON IS---" + userdetjson);

                          
                         


                      
                            if(userdetjson.getString("picture").length()>0) {


                                String picture = userdetjson.getString("picture");
                                System.out.println("PICTURE IS---" + picture);

                                if (!picture.equalsIgnoreCase("")) {
                                    Picasso.with(ViewLiveTradeActivity1012.this).load(picture).transform(new CircleTransform()).into((profileiv));
                                    // Picasso.with(ViewLiveTradeActivity.this()).load(picture).resize(314,512).transform(new CircleTransform()).into((profileiv));
                                } else {

                                    profileiv.setImageResource(R.mipmap.ic_cam);
                                    profileiv.setBackgroundResource(R.drawable.circle);
                                    profileiv.setPadding(30, 30, 30, 30);

                                }
                            }

                           
                          
                            
                            
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                          //  pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                      //  pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        if(networkResponse!=null) {

                            int respCode = networkResponse.statusCode;
                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(ViewLiveTradeActivity1012.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(ViewLiveTradeActivity1012.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(ViewLiveTradeActivity1012.this,"Some Error Occured,Please try after some time");
                        }

                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                System.out.println("get user details params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(ViewLiveTradeActivity1012.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void InsertTradeAPI(String url,final String sellerid,final String categoryidstr,final  String subcategoryidstr,final String tncstr,final String ratetypestr,final String quotedrate,final  String qtystr,final  String tncmodified) {

        pdia = new ProgressDialog(ViewLiveTradeActivity1012.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();


        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(ViewLiveTradeActivity1012.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            if ((pdia != null) && pdia.isShowing()) {
                                pdia.dismiss();
                            }
                        } catch (final IllegalArgumentException e) {
                            // Handle or log or ignore
                        } catch (final Exception e) {
                            // Handle or log or ignore
                        } finally {
                            pdia = null;
                        }




                        System.out.println("RESPONSE OF INSERT TRADE LIMIT API IS---" + response);


                        JSONObject InsertTradeLimitJson;
                        try {
                            InsertTradeLimitJson = new JSONObject(response);


                            String statusstr = InsertTradeLimitJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                String messagestr = InsertTradeLimitJson.getString("message");
                                Toast.makeText(ViewLiveTradeActivity1012.this,messagestr,Toast.LENGTH_SHORT).show();

                                Intent i = new Intent(ViewLiveTradeActivity1012.this,HomeActivity.class);
                                SingletonActivity.fromviewlivetrade = true;
                                SingletonActivity.fromaddnewoffer = false;
                                SingletonActivity.isNotificationClicked = false;
                                // SingletonActivity.index = index;
                                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                SingletonActivity.FromAddNewOfferTabOne=false;
                                SingletonActivity.FromAddNewOfferTabZero=false;
                                SingletonActivity.isNewEnquiryClicked = false;
                                SingletonActivity.frombackofbuyerdispatch = false;
                                SingletonActivity.frombackofsellerdispatch = false;
                                SingletonActivity.frombackofbuyerpayment = false;
                                SingletonActivity.frombackofsellerpayment = false;


                                startActivity(i);
                            }
                            else
                            {

                                String messagestr = InsertTradeLimitJson.getString("message");
                                Toast.makeText(ViewLiveTradeActivity1012.this,messagestr,Toast.LENGTH_SHORT).show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }









                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pdia.dismiss();

                        Log.e("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(ViewLiveTradeActivity1012.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(ViewLiveTradeActivity1012.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(ViewLiveTradeActivity1012.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("rate_type",ratetypestr);
                params.put("quoted_rate",quotedrate);
                params.put("sell_id",sellerid);
                params.put("user_code",userCodestr);
                params.put("mobile_num",mobilenumstr);
                params.put("quantity",qtystr);
                params.put("category",categoryidstr);
                params.put("sub_category",subcategoryidstr);
                params.put("terms",tncstr);

                if(tncstr.equalsIgnoreCase(tncmodified))
                {
                    params.put("terms_modified","");
                }
                else {
                    params.put("terms_modified", tncmodified);
                }


                System.out.println("Insert Trade params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void InsertEnquiryAPI(String url,final String sellerid,final String categoryidstr,final  String subcategoryidstr,final String tncstr,final String ratetypestr,final String quotedrate,final  String qtystr,final  String tncmodified) {

        pdia = new ProgressDialog(ViewLiveTradeActivity1012.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();


        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(ViewLiveTradeActivity1012.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            if ((pdia != null) && pdia.isShowing()) {
                                pdia.dismiss();
                            }
                        } catch (final IllegalArgumentException e) {
                            // Handle or log or ignore
                        } catch (final Exception e) {
                            // Handle or log or ignore
                        } finally {
                            pdia = null;
                        }




                        System.out.println("RESPONSE OF INSERT ENQUIRY API IS---" + response);


                        JSONObject InsertEnquiryJson;
                        try {
                            InsertEnquiryJson = new JSONObject(response);


                            String statusstr = InsertEnquiryJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                String messagestr = InsertEnquiryJson.getString("message");
                                Toast.makeText(ViewLiveTradeActivity1012.this,messagestr,Toast.LENGTH_SHORT).show();

                                Intent i = new Intent(ViewLiveTradeActivity1012.this,HomeActivity.class);
                                SingletonActivity.fromviewlivetrade = true;
                                SingletonActivity.enquiryfromplatespage = false;
                                SingletonActivity.fromviewpigiron = false;
                                SingletonActivity.fromaddnewoffer = false;
                                SingletonActivity.isNotificationClicked = false;
                                // SingletonActivity.index = index;
                                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                SingletonActivity.FromAddNewOfferTabOne=false;
                                SingletonActivity.FromAddNewOfferTabZero=false;
                                SingletonActivity.isNewEnquiryClicked = false;
                                SingletonActivity.frombackofbuyerdispatch = false;
                                SingletonActivity.frombackofsellerdispatch = false;
                                SingletonActivity.frombackofbuyerpayment = false;
                                SingletonActivity.frombackofsellerpayment = false;


                                startActivity(i);
                            }
                            else
                            {

                                String messagestr = InsertEnquiryJson.getString("message");
                                Toast.makeText(ViewLiveTradeActivity1012.this,messagestr,Toast.LENGTH_SHORT).show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }









                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pdia.dismiss();

                        Log.e("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(ViewLiveTradeActivity1012.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(ViewLiveTradeActivity1012.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(ViewLiveTradeActivity1012.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("rate_type","2");
                params.put("quoted_rate","0");
                params.put("sell_id",sellerid);
                params.put("user_code",userCodestr);
                params.put("mobile_num",mobilenumstr);
                params.put("quantity",qtystr);
                params.put("category",categoryidstr);
                params.put("sub_category",subcategoryidstr);
                params.put("terms",tncstr);
                params.put("rate",enquiry_price.getText().toString());

                if(tncstr.equalsIgnoreCase(tncmodified))
                {
                    params.put("terms_modified","");
                }
                else {
                    params.put("terms_modified", tncmodified);
                }


                System.out.println("Insert Enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }

}
