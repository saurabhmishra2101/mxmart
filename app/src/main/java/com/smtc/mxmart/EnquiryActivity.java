package com.smtc.mxmart;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.smtc.mxmart.Fragment.TradeFragment111119;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.OpenableColumns;
import android.util.Base64;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class EnquiryActivity extends AppCompatActivity {

    ImageView backiv;
    private ProgressBar progressBarFinishedEnquiry;
    Handler handler = new Handler();
    UtilsDialog util = new UtilsDialog();
    String tag_json_obj = "json_obj_req";
    ImageView materialimgvw;
    RelativeLayout downloadexcelrelative;
    TextView size_desc_txt,choosefileseltext,steel_type,material_desc_txt,brand_desc_txt,standard_desc_txt,grade_desc_txt,thickness_desc_txt,specification_desc_txt;
    // Progress Dialog
    private ProgressDialog pDialog;
    ImageView my_image;
    // Progress dialog type (0 - for Horizontal progress bar)
    public static final int progress_bar_type = 0;
    String fileName = "list";
    ArrayList<String> thicknessPredefinedlist = new ArrayList<String>();
    Spinner thicknessspinner;
    RadioButton userdefineradiobtn,predefineradiobtn;
    RadioGroup defineradiogrp;
    EditText thickness_edittext,width_edittext,height_edittext,qty_edittext,descedttxt;
    RelativeLayout thicknessspinnerrelative,sendenqrelative,choosefilerelative;
    String typeid,userCodestr,post_type,material_term_cond,material_id,material_type_id,material_thickness_user,material_width_user,material_length_user,material_quantity_user,material_width,material_length,material_quantity,material_thickness;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String materialid,attachmentstr;
    String type = "";
    JSONObject material_type_details_json;
    ProgressDialog pdia;
    ImageView exceliconiv;
    TextView thickness_title_txt,size_title_txt,specification_title_txt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enquiry);

        specification_title_txt = (TextView)findViewById(R.id.specification_title_txt);
        size_title_txt = (TextView)findViewById(R.id.size_title_txt);
        thickness_title_txt = (TextView)findViewById(R.id.thickness_title_txt);
        choosefileseltext = (TextView)findViewById(R.id.choosefileseltext);
        descedttxt = (EditText) findViewById(R.id.descedttxt);
        thicknessspinner = (Spinner)findViewById(R.id.thicknessspinner);
        sendenqrelative = (RelativeLayout)findViewById(R.id.sendenqrelative);
        backiv = (ImageView)findViewById(R.id.backicon);
        progressBarFinishedEnquiry = (ProgressBar)findViewById(R.id.progressBarFinishedEnquiry);
        steel_type = (TextView)findViewById(R.id.steel_type);
        material_desc_txt = (TextView)findViewById(R.id.material_desc_txt);
        brand_desc_txt = (TextView)findViewById(R.id.brand_desc_txt);
        standard_desc_txt = (TextView)findViewById(R.id.standard_desc_txt);
        grade_desc_txt = (TextView)findViewById(R.id.grade_desc_txt);
        thickness_desc_txt = (TextView)findViewById(R.id.thickness_desc_txt);
        specification_desc_txt = (TextView)findViewById(R.id.specification_desc_txt);
        materialimgvw = (ImageView)findViewById(R.id.materialimgvw);
   //     downloadexcelrelative = (RelativeLayout)findViewById(R.id.downloadexcelrelative);
        defineradiogrp = (RadioGroup)findViewById(R.id.defineradiogrp);
        userdefineradiobtn = (RadioButton) findViewById(R.id.userdefineradiobtn);
        predefineradiobtn = (RadioButton) findViewById(R.id.predefineradiobtn);
        thickness_edittext = (EditText)findViewById(R.id.thickness_edittext);
        thicknessspinnerrelative = (RelativeLayout)findViewById(R.id.thicknessspinnerrelative);
        width_edittext = (EditText)findViewById(R.id.width_edittext);
        height_edittext = (EditText)findViewById(R.id.height_edittext);
        qty_edittext = (EditText)findViewById(R.id.qty_edittext);
        choosefilerelative = (RelativeLayout)findViewById(R.id.choosefilerelative);
        exceliconiv = (ImageView)findViewById(R.id.exceliconiv);
        size_desc_txt = (TextView)findViewById(R.id.size_desc_txt);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);




        post_type = "pd";

        defineradiogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.userdefineradiobtn) {
                    //do work when radioButton1 is active
                    thicknessspinnerrelative.setVisibility(View.INVISIBLE);
                    thickness_edittext.setVisibility(View.VISIBLE);
                    width_edittext.setEnabled(true);
                    height_edittext.setEnabled(true);
                    qty_edittext.setEnabled(true);
                    thickness_edittext.requestFocus();
                    width_edittext.setText("");
                    height_edittext.setText("");
                    qty_edittext.setText("");
                    post_type = "ud";

                } else  if (checkedId == R.id.predefineradiobtn) {
                    //do work when radioButton2 is active
                    thicknessspinnerrelative.setVisibility(View.VISIBLE);
                    thickness_edittext.setVisibility(View.INVISIBLE);
                    width_edittext.setEnabled(false);
                    height_edittext.setEnabled(false);
                    qty_edittext.setEnabled(false);
                    post_type = "pd";
                    try {
                        width_edittext.setText(material_type_details_json.getString("width"));
                        height_edittext.setText(material_type_details_json.getString("length"));
                        qty_edittext.setText(material_type_details_json.getString("quantity"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });




        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if(SingletonActivity.enquiryfromhomepage == true)
                {
                    Intent i = new Intent(EnquiryActivity.this, HomeActivity.class);
                    SingletonActivity.enquiryfromhomepage = true;
                    SingletonActivity.enquiryfromplatespage = false;

                    startActivity(i);
                }
                if(SingletonActivity.enquiryfromplatespage == true)
                {
                    Intent i = new Intent(EnquiryActivity.this,HomeActivity.class);
                    SingletonActivity.enquiryfromhomepage = false;
                    SingletonActivity.enquiryfromplatespage = true;

                    startActivity(i);
                }
            }
        });



        Bundle b = getIntent().getExtras();
        materialid = b.getString("materialId");
        String materialname = b.getString("materialname");
        String materialthickness = b.getString("materialthickness");
        String materialbrand = b.getString("materialbrand");
        String materialstandard = b.getString("materialstandard");
        String materialgrade = b.getString("materialgrade");
        String materialimage = b.getString("materialimage");
        String materialsize = b.getString("materialsize");
        final String materialspec = b.getString("materialspec");
        String materialloc = b.getString("materialloc");
        String material_size_excel = b.getString("material_size_excel");

        steel_type.setText(materialname+"("+materialthickness+")");
        material_desc_txt.setText(materialname);
        brand_desc_txt.setText(materialbrand);
        standard_desc_txt.setText(materialstandard);
        grade_desc_txt.setText(materialgrade);
        thickness_desc_txt.setText(materialthickness);
        specification_desc_txt.setText(materialspec);

        if(materialsize.equalsIgnoreCase("null"))
        {
            size_desc_txt.setVisibility(View.GONE);
            size_desc_txt.setText("");
        }
        else
        {
            size_desc_txt.setVisibility(View.VISIBLE);
            size_desc_txt.setText(materialsize);
        }


        if(materialthickness.equalsIgnoreCase(""))
        {
            thickness_title_txt.setVisibility(View.GONE);
            thickness_desc_txt.setVisibility(View.GONE);
        }
        else
        {
            thickness_title_txt.setVisibility(View.VISIBLE);
            thickness_desc_txt.setVisibility(View.VISIBLE);
        }

        if(materialsize.equalsIgnoreCase("")&&(material_size_excel.equalsIgnoreCase("null")))
        {
            size_title_txt.setVisibility(View.GONE);
            size_desc_txt.setVisibility(View.GONE);
            exceliconiv.setVisibility(View.GONE);
        }
        else
        {
            size_title_txt.setVisibility(View.VISIBLE);
            size_desc_txt.setVisibility(View.VISIBLE);
            exceliconiv.setVisibility(View.VISIBLE);
        }




        if(materialspec.equalsIgnoreCase(""))
        {
            specification_title_txt.setVisibility(View.GONE);
            specification_desc_txt.setVisibility(View.GONE);
        }
        else
        {
            specification_title_txt.setVisibility(View.VISIBLE);
            specification_desc_txt.setVisibility(View.VISIBLE);
        }




        String imageURL = APIName.IMAGE_URL + "/" + materialimage;
        System.out.println("imageURL IS---" + imageURL);

        final String excelURL = APIName.IMAGE_URL + "/" + material_size_excel;
        System.out.println("excelURL IS---" + excelURL);

        PicassoTrustAll.getInstance(EnquiryActivity.this)
                .load(imageURL)
                .into(materialimgvw);

        specification_desc_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                final Dialog dialog = new Dialog(EnquiryActivity.this);
                dialog.setContentView(R.layout.activity_enquiry_desc_dialog);

                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {

                        dialog.dismiss();
                    }
                });

                dialog.show();

                TextView dialogText = (TextView)dialog.getWindow().findViewById(R.id.dialogtxt);
                dialogText.setText(materialspec);


            }
        });


       exceliconiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(EnquiryActivity.this, SizelistWebViewActivity.class);
                i.putExtra("sizeurl",excelURL);
                startActivity(i);




            }
        });

        choosefilerelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showFileChooser();
            }
        });

        if (NetworkUtility.checkConnectivity(EnquiryActivity.this)) {
            String materialDetailsUrl = APIName.URL + "/livetrading/materialDetails?material_id="+materialid;
            System.out.println("MATERIAL DETAILS URL IS---" + materialDetailsUrl);
            MaterialDetailsAPI(materialDetailsUrl);

        } else {
            util.dialog(EnquiryActivity.this, "Please check your internet connection.");
        }

        sendenqrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                material_term_cond = descedttxt.getText().toString();




                    boolean invalid = false;
                    if (thickness_edittext.getText().toString().equals(""))  {
                        invalid = true;
                        Toast.makeText(getApplicationContext(), "Thickness can't be empty",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if (width_edittext.getText().toString().equals("")) {
                        invalid = true;

                        Toast.makeText(getApplicationContext(), "Width can't be empty",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if (height_edittext.getText().toString().equals("")) {
                        invalid = true;

                        Toast.makeText(getApplicationContext(), "Length can't be empty",
                                Toast.LENGTH_SHORT).show();
                    }
                    else if (qty_edittext.getText().toString().equals("")) {
                        invalid = true;

                        Toast.makeText(getApplicationContext(), "Quantity can't be empty",
                                Toast.LENGTH_SHORT).show();
                    }

                    else if (invalid == false)
                    {

                        sendenqrelative.setEnabled(false);
                        if(NetworkUtility.checkConnectivity(EnquiryActivity.this)){

                            if (NetworkUtility.checkConnectivity(EnquiryActivity.this)) {
                                String AddMaterialEnquiryUrl = APIName.URL + "/livetrading/add_material_enquiry";
                                System.out.println("ADD MATERIAL ENQUIRY URL IS---" + AddMaterialEnquiryUrl);
                                AddMaterialEnquiryAPI(AddMaterialEnquiryUrl);

                            } else {
                                util.dialog(EnquiryActivity.this, "Please check your internet connection.");
                            }
                        }
                        else{
                            util.dialog(EnquiryActivity.this, "Please check your internet connection.");
                        }
                    }








            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(SingletonActivity.enquiryfromhomepage == true)
        {
            Intent i = new Intent(EnquiryActivity.this, HomeActivity.class);
            SingletonActivity.enquiryfromhomepage = true;
            SingletonActivity.enquiryfromplatespage = false;

            startActivity(i);
        }
        if(SingletonActivity.enquiryfromplatespage == true)
        {
            Intent i = new Intent(EnquiryActivity.this,HomeActivity.class);
            SingletonActivity.enquiryfromhomepage = false;
            SingletonActivity.enquiryfromplatespage = true;

            startActivity(i);
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");



        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    1);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Uri selectedFileURI = data.getData();

                if(selectedFileURI!=null)
                {
                    String filePath = selectedFileURI.toString();
                    System.out.println("SELECTED FILE PATH IS--"+ filePath);

                    Uri uri = data.getData();
                    String uriString = uri.toString();
                    File myFile = new File(uriString);
                    String path = myFile.getAbsolutePath();
                    String displayName = null;



                    try{
                        InputStream iStream =   getContentResolver().openInputStream(selectedFileURI);
                        byte[] byteArray = getBytes(iStream);
                        attachmentstr = encodeByteArrayIntoBase64String(byteArray);

                        ContentResolver cR = this.getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        type = mime.getExtensionFromMimeType(cR.getType(selectedFileURI));

                        System.out.println("extension is------"+ type);

                        if(type.equals("jpg")||type.equals("jpeg")||type.equals("png")||type.equals("pdf")||type.equals("xlsx")||type.equals("xls"))
                        {
                            attachmentstr = "data:application/"+type+"; base64,"+attachmentstr;

                            System.out.println("BASE 64 STRING------"+ attachmentstr);

                            if (uriString.startsWith("content://")) {
                                Cursor cursor = null;
                                try {
                                    cursor = EnquiryActivity.this.getContentResolver().query(uri, null, null, null, null);
                                    if (cursor != null && cursor.moveToFirst()) {
                                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                        System.out.println("displayName 1 is------"+ displayName);
                                        choosefileseltext.setText(displayName);
                                    }
                                } finally {
                                    cursor.close();
                                }
                            } else if (uriString.startsWith("file://")) {
                                displayName = myFile.getName();
                                System.out.println("displayName 2 is------"+ displayName);
                                choosefileseltext.setText(displayName);
                            }
                        }
                       else {

                            Toast.makeText(EnquiryActivity.this,"Please upload the document in following formats .jpg,.jpeg,.png,.pdf,.xlsx,.xls only",Toast.LENGTH_SHORT).show();


                        }





                    }
                    catch (IOException e)
                    {
                        Toast.makeText(getBaseContext(),"An error occurred with file chosen",Toast.LENGTH_SHORT).show();
                    }



                }



            }
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public static String encodeByteArrayIntoBase64String(byte[] bytes)
    {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }




    private void AddMaterialEnquiryAPI(String url) {

        pdia = new ProgressDialog(EnquiryActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

    //   progressBarFinishedEnquiry.setVisibility(View.VISIBLE);
        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(EnquiryActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {

                        pdia.dismiss();
                       // progressBarFinishedEnquiry.setVisibility(View.INVISIBLE);

                      //  System.out.println("RESPONSE of upload IS==="+ response);

                    //    Toast.makeText(EnquiryActivity.this,"Response is: "+ response,Toast.LENGTH_SHORT).show();


                        JSONObject AddMaterialEnquiryJson = null;

                        try {
                            AddMaterialEnquiryJson = new JSONObject(response);
                            String statusstr = AddMaterialEnquiryJson.getString("status");

                            if (statusstr.equalsIgnoreCase("true")) {

                                String msgstr = AddMaterialEnquiryJson.getString("message");
                                Toast.makeText(EnquiryActivity.this,msgstr,Toast.LENGTH_SHORT).show();

                                finish();


                            }
                            else
                            {
                                String msgstr = AddMaterialEnquiryJson.getString("message");
                                Toast.makeText(EnquiryActivity.this,msgstr,Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia.dismiss();
                        }





                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noenquirytxt.setVisibility(View.VISIBLE);
                                        // enquirylistvw.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(EnquiryActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            util.dialog(EnquiryActivity.this, "Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("material_term_cond",material_term_cond);
                params.put("post_type",post_type);
                params.put("material_id",materialid);
               // params.put("material_type_id",typeid);


                    material_thickness_user = thickness_edittext.getText().toString();
                    material_width_user = width_edittext.getText().toString();
                    material_length_user = height_edittext.getText().toString();
                    material_quantity_user = qty_edittext.getText().toString();
                    material_width = "";
                    material_length = "";
                    material_quantity = "";
                    material_thickness = "";




                params.put("material_thickness_user",material_thickness_user);
                params.put("material_width_user",material_width_user);
                params.put("material_length_user",material_length_user);
                params.put("material_quantity_user",material_quantity_user);
                params.put("material_width",material_width);
                params.put("material_length",material_length);
                params.put("material_quantity",material_quantity);
                params.put("material_thickness",material_thickness);
                params.put("user_code",userCodestr);
                if(attachmentstr==null)
                {
                    params.put("trader_file_Upload","");
                }
                else
                {
                    params.put("trader_file_Upload",attachmentstr);
                }



                 System.out.println("Add Material enquiry params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));



        if (SingletonActivity.clickedcount == 0) {
            RequestQueue requestQueue = Volley.newRequestQueue(EnquiryActivity.this, hurlStack);
            requestQueue.add(stringRequest);
        } else {
            SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }

    }


    private void MaterialTypeDetailsAPI(String url) {

        pdia = new ProgressDialog(EnquiryActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        //  progressBarFinishedEnquiry.setVisibility(View.VISIBLE);
        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(EnquiryActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                      //  progressBarFinishedEnquiry.setVisibility(View.GONE);

                        pdia.dismiss();

                        JSONObject MaterialTypeDetailsJson = null;

                        try {
                            MaterialTypeDetailsJson = new JSONObject(response);
                            String statusstr = MaterialTypeDetailsJson.getString("status");

                            if (statusstr.equalsIgnoreCase("true")) {

                                material_type_details_json = MaterialTypeDetailsJson.getJSONObject("material_type_details");

                                width_edittext.setText(material_type_details_json.getString("width"));
                                height_edittext.setText(material_type_details_json.getString("length"));
                                qty_edittext.setText(material_type_details_json.getString("quantity"));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                           // progressBarFinishedEnquiry.setVisibility(View.GONE);
                            pdia.dismiss();
                        }





                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();
                      //  progressBarFinishedEnquiry.setVisibility(View.GONE);

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noenquirytxt.setVisibility(View.VISIBLE);
                                        // enquirylistvw.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(EnquiryActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            util.dialog(EnquiryActivity.this, "Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


              //  System.out.println("finished enquiry list params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));



        if (SingletonActivity.clickedcount == 0) {
            RequestQueue requestQueue = Volley.newRequestQueue(EnquiryActivity.this, hurlStack);
            requestQueue.add(stringRequest);
        } else {
            SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }

    }





    private void MaterialDetailsAPI(String url) {

        pdia = new ProgressDialog(EnquiryActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

      //  progressBarFinishedEnquiry.setVisibility(View.VISIBLE);
        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(EnquiryActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {

                         pdia.dismiss();
                    //    progressBarFinishedEnquiry.setVisibility(View.GONE);

                        thicknessPredefinedlist.clear();

                        handler.postDelayed(new Runnable() {
                            public void run() {

                              //  progressBarFinishedEnquiry.setVisibility(View.GONE);
                                System.out.println("RESPONSE OF MATERIAL DETAILS API IS---" + response);


                                JSONObject MaterialDetailsJson = null;
                                try {
                                    MaterialDetailsJson = new JSONObject(response);


                                    String statusstr = MaterialDetailsJson.getString("status");

                                    if (statusstr.equalsIgnoreCase("true")) {


                                        final JSONArray MaterialDetailsJSONArray = MaterialDetailsJson.getJSONArray("material_details");


                                        for(int i = 0; i < MaterialDetailsJSONArray.length(); i++)
                                        {
                                            thicknessPredefinedlist.add(MaterialDetailsJSONArray.getJSONObject(i).getString("thickness"));
                                        }



                                        ArrayAdapter<String> thicknessPreDefAdap = new ArrayAdapter<String>(EnquiryActivity.this, android.R.layout.simple_spinner_dropdown_item, thicknessPredefinedlist);
                                        thicknessPreDefAdap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        thicknessspinner.setSelection(0);
                                        thicknessspinner.setAdapter(thicknessPreDefAdap);
                                        thicknessPreDefAdap.notifyDataSetChanged();


                                        thicknessspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                            @Override
                                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                                try {
                                                    typeid = MaterialDetailsJSONArray.getJSONObject(position).getString("type_id");
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }


                                                if (NetworkUtility.checkConnectivity(EnquiryActivity.this)) {
                                                    String materialTypeDetailsUrl = APIName.URL + "/livetrading/materialTypeDetails?type_id="+typeid;
                                                    System.out.println("MATERIAL TYPE DETAILS URL IS---" + materialTypeDetailsUrl);
                                                    MaterialTypeDetailsAPI(materialTypeDetailsUrl);

                                                } else {
                                                    util.dialog(EnquiryActivity.this, "Please check your internet connection.");
                                                }
                                            }

                                            @Override
                                            public void onNothingSelected(AdapterView<?> parent) {

                                                try {
                                                    typeid = MaterialDetailsJSONArray.getJSONObject(0).getString("type_id");
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }


                                                if (NetworkUtility.checkConnectivity(EnquiryActivity.this)) {
                                                    String materialTypeDetailsUrl = APIName.URL + "/livetrading/materialTypeDetails?type_id="+typeid;
                                                    System.out.println("MATERIAL TYPE DETAILS URL IS---" + materialTypeDetailsUrl);
                                                    MaterialTypeDetailsAPI(materialTypeDetailsUrl);

                                                } else {
                                                    util.dialog(EnquiryActivity.this, "Please check your internet connection.");
                                                }
                                            }


                                        });



                                    } else {


                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                     pdia.dismiss();
                                 //   progressBarFinishedEnquiry.setVisibility(View.GONE);
                                }
                            }
                        }, 3000); // 3000 milliseconds delay






                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();
                       // progressBarFinishedEnquiry.setVisibility(View.GONE);

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if (networkResponse != null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                       // noenquirytxt.setVisibility(View.VISIBLE);
                                       // enquirylistvw.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(EnquiryActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            util.dialog(EnquiryActivity.this, "Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


             //   System.out.println("finished enquiry list params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

      //  System.out.println("SingletonActivity.clickedcount no.=====" + SingletonActivity.clickedcount);

        if (SingletonActivity.clickedcount == 0) {
            RequestQueue requestQueue = Volley.newRequestQueue(EnquiryActivity.this, hurlStack);
            requestQueue.add(stringRequest);
        } else {
            SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
        }

    }


}
