package com.smtc.mxmart;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Menu;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


public class SessionManager {
	// Shared Preferences
	SharedPreferences pref;
	
	// Editor for Shared preferences
	Editor editor;

	UtilsDialog util = new UtilsDialog();
	
	// Context
	Context _context;
	
	// Shared pref mode
	int PRIVATE_MODE = 0;
	
	// Sharedpref file name
	private static final String PREF_NAME ="IceportPref";
	
	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";
	
	// User name (make variable public to access from outside)
	public static final String KEY_USERNAME = "username";
	
	// Password (make variable public to access from outside)
	public static final String KEY_PASSWORD = "password";
	
	// Constructor
	public SessionManager(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	
	/**
	 * Create login session
	 * */
	public void createLoginSession(){
		// Storing login value as TRUE
		editor.putBoolean(IS_LOGIN, true);
		
		/*// Storing schoolname in pref
		editor.putString(KEY_USERNAME, schoolname);
		
		// Storing mobile number in pref
		editor.putString(KEY_PASSWORD, mobilenum);

		// Storing password in pref
		editor.putString(KEY_PASSWORD, password);*/
		// commit changes
		editor.commit();
	}	
	
	/**
	 * Check login method wil check user login status
	 * If false it will redirect user to login page
	 * Else won't do anything
	 * */
	public void checkLogin(){
		// Check login status
		if(!this.isLoggedIn()){
			// user is not logged in redirect him to Login Activity
			Intent i = new Intent(_context, LoginActivity.class);
			// Closing all the Activities
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			
			// Add new Flag to start new Activity
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			
			// Staring Login Activity
			_context.startActivity(i);
		}

		/*else
		{
			Intent i = new Intent(_context, LiveMenuActivity.class);
			_context.startActivity(i);

		}*/
		
	}
	
	
	
	/**
	 * Get stored session data
	 * */
	public HashMap<String, String> getUserDetails(){
		HashMap<String, String> user = new HashMap<String, String>();
		// user name
		user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
		
		// user email id
		user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));
		
		// return user
		return user;
	}
	
	/**
	 * Clear session details
	 * */
	public void logoutUser(String mobno,String regid){
		// Clearing all data from Shared Preferences
		editor.clear();
		editor.commit();

		SingletonActivity.contactjsonArray =  null;
		SingletonActivity.installedCapacityjsonArray  =  null;
		SingletonActivity.referencejsonArray =  null;

		SingletonActivity.isNotificationClicked = false;

		SingletonActivity.isPassedFromLogin  = false;
		SingletonActivity.isFABClicked = false;
		SingletonActivity.isUpdateClicked = false;
		SingletonActivity.isValidTradeTimeOpen = false;
		SingletonActivity.isSpNotificationClicked = false;
		SingletonActivity.isBuyerRejectedClicked = false;
		SingletonActivity.isNotificationClicked = false;
		SingletonActivity.isTodaysOfferClicked = false;
		SingletonActivity.selleditoffer = false;

		SingletonActivity.FromAddNewOfferTabOne = false;
		SingletonActivity.FromAddNewOfferTabZero = false;
		SingletonActivity.FromAddNewOfferTabTwoBuyRej = false;
		SingletonActivity.FromAddNewOfferTabTwoNewEnq = false;

		SingletonActivity.frombuycurrentenquiry = false;

		SingletonActivity.frombackofsellerdispatch = false;
		SingletonActivity.frombackofsellerpayment = false;

		SingletonActivity.frombackofbuyerdispatch = false;
		SingletonActivity.frombackofbuyerpayment = false;


		SingletonActivity.fromselltodaysoffer = false;
		SingletonActivity.fromsellnotificationindexzero = false;
		SingletonActivity.isLiveSalesClicked = false;
		SingletonActivity.splited = "";
		SingletonActivity.loginstate = false;
		SingletonActivity.isSplitSaudaClicked = false;
		SingletonActivity.fromforgot = false;
		SingletonActivity.backfromeditpersonaldetails = false;
		SingletonActivity.backfromeditbusinessdetails = false;
		SingletonActivity.backfromeditcontactdetails = false;
		SingletonActivity.backfromeditinstalledcapacitydetails = false;
		SingletonActivity.backfromeditreferencedetails = false;
		SingletonActivity.updatedailylimitfromtrade = false;
		SingletonActivity.setdailylimitfromtrade = false;
		SingletonActivity.fromaddnewoffer = false;
		SingletonActivity.fromupdatenewoffer = false;
		SingletonActivity.fromviewlivetrade = false;
		SingletonActivity.isNewEnquiryClicked = false;
		SingletonActivity.isSellerAcceptedClicked = false;
		SingletonActivity.isSellerRejectedClicked = false;
		SingletonActivity.isSpTradeBuyerClicked = false;
		SingletonActivity.isSpTradeSellerClicked = false;
		SingletonActivity.isIsBuyerRejectedClicked = false;
		SingletonActivity.isdailylimitAvailable = false;
		SingletonActivity.frombuycurrentenquiry = false;
		SingletonActivity.frombackofsellerdispatch = false;
		SingletonActivity.frombackofsellerpayment = false;
		SingletonActivity.frombackofbuyerdispatch = false;
		SingletonActivity.frombackofbuyerpayment = false;
		SingletonActivity.fromselltodaysoffer = false;
		SingletonActivity.fromsellnotificationindexzero = false;
		SingletonActivity.isLiveSalesClicked = false;
		SingletonActivity.fromsellcurrentdeals = false;
		SingletonActivity.frombuycurrentdeals = false;
		SingletonActivity.isLivePriceClicked = false;
		SingletonActivity.fromsellerhistorywebview = false;
		SingletonActivity.frombuyerhistorywebview = false;
		SingletonActivity.fromhometopayment = false;

		SingletonActivity.clickedcount = 0;


	
		






		if(NetworkUtility.checkConnectivity(_context)){
			String logouturl = APIName.URL + "/user/logout";
			LogOutApi(logouturl,mobno,regid);

		}
		else{
			Toast.makeText(_context,"Please check your internet connection.",Toast.LENGTH_SHORT).show();
		}

/*
		// After logout redirect user to Loing Activity
		Intent i = new Intent(_context, LoginActivity.class);
		// Closing all the Activities---
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// Add new Flag to start new Activity
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// Staring Login Activity
		_context.startActivity(i);*/
	}
	
	/**
	 * Quick check for login
	 * **/
	// Get Login State
	public boolean isLoggedIn(){
		return pref.getBoolean(IS_LOGIN, false);
	}


	public void LogOutApi(final String url,final String mobilenum,final  String fcmid){

		/*final ProgressDialog pdia = new ProgressDialog(_context);
		pdia.setMessage("Please Wait...");
		pdia.setCanceledOnTouchOutside(false);
		pdia.show();*/


		//          System.out.println("LOGOUT API URL IS---"+ url);

		HurlStack hurlStack = new HurlStack() {
			@Override
			protected HttpURLConnection createConnection(java.net.URL url)
					throws IOException {
				HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
						.createConnection(url);
				try {
					httpsURLConnection
							.setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(_context));
					// httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
				} catch (Exception e) {
					e.printStackTrace();
				}
				return httpsURLConnection;
			}
		};

		String tag_json_obj = "json_obj_req";

		StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {

						//	Toast.makeText(_context,response,Toast.LENGTH_SHORT).show();
						System.out.println("URL OF LOGOUT API IS---"+ url);
						System.out.println("RESPONSE OF LOGOUT API IS---"+ response);
						//pdia.dismiss();


						//                     System.out.println("RESPONSE OF LOGOUT API IS---"+ response);
						final JSONArray jsonArray;
						try {
							//jsonArray = new JSONArray(response);



							JSONObject logoutresponsejson = new JSONObject(response);
							//                   System.out.println("LOGOUT API JSON  IS---"+ logoutresponsejson);

							String logoutstatusstr = logoutresponsejson.getString("status");
							String logoutmsgstr = logoutresponsejson.getString("message");


							if(logoutstatusstr.equalsIgnoreCase("true")) {

								/*editor.clear();
								editor.commit();*/

								// After logout redirect user to Loing Activity
								Intent i = new Intent(_context, LoginActivity.class);

								// Closing all the Activities---
								i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
								// Add new Flag to start new Activity
								i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

								SingletonActivity.fcmid = fcmid;

								// Staring Login Activity
								_context.startActivity(i);





							}




						}
						catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							//pdia.dismiss();
						}




					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						//pdia.dismiss();

						VolleyLog.d("TAG", "Error: " + error.getMessage());
						NetworkResponse networkResponse = error.networkResponse;

						if(networkResponse!=null) {
							int respCode = networkResponse.statusCode;
							System.out.println("RESPONSE CODE IN LOGOUT--------" + respCode);
							//  String respcodestr = Integer.toString(respCode);
							if (respCode == 404) {
								String respData = new String(networkResponse.data);
								System.out.println("RESPONSE DATA--------" + respData);
								try {
									JSONObject jsonObject = new JSONObject(respData);
									String statusstr = jsonObject.getString("status");
									String messagestr = jsonObject.getString("message");
									if (statusstr.equalsIgnoreCase("false")) {


                                      /*  noattendancetxtvw.setVisibility(View.VISIBLE);
                                        daysheader.setVisibility(View.GONE);
                                        monthheader.setVisibility(View.GONE);*/
										Toast.makeText(_context, messagestr, Toast.LENGTH_SHORT).show();

									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
							} else {
								Toast.makeText(_context, error.getMessage(), Toast.LENGTH_SHORT).show();
							}
						}
						else
						{
							util.dialog(_context,"Some Error Occured,Please try after some time");
						}


					}
				}){
			@Override
			protected Map<String,String> getParams(){
				Map<String,String> params = new HashMap<String, String>();

				params.put("mobile_num",mobilenum);
				params.put("registration_Id",fcmid);
				params.put("device_type","1");

				System.out.println("logout params--------" + params);

				return params;
			}
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> params = new HashMap<String, String>();
				params.put("Content-Type", "application/x-www-form-urlencoded");
				return params;
			}

		};

		stringRequest.setRetryPolicy(new DefaultRetryPolicy(
				10000,
				DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
				DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


                   RequestQueue requestQueue = Volley.newRequestQueue(_context,hurlStack);
                   requestQueue.add(stringRequest);

		//SingletonActivity.getInstance().addToRequestQueue(stringRequest, tag_json_obj);

	}
}
