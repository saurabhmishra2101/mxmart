package com.smtc.mxmart;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by 10161 on 9/7/2017.
 */

public class SplitSaudaPatraActivity extends AppCompatActivity {

    TextView notification_total_count;
    String commentStr,tradeStr;
    TextView todays_offer_count,new_enquiry_count,seller_accepted_count,seller_rejected_count,sp_generated_count,buyer_rejected_count;
    int index;
    SessionManager sessionManager;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    UtilsDialog util = new UtilsDialog();
    String userCodestr,mobilenumstr;
    ImageView back_split_saudapatra;
    TextView splittxtvw,entersplitqtytxt,tradeidtxt,materialdesctxt,sellerdesctxtvw,qtydesctxtvw,paymenttypedesctxt,locationdesctxtvw,pricetontxtvw;
    RelativeLayout split_block_1,split_block_2,split_block_3,split_block_4,addmorerelative,submitrelative;
    String paymenttypestr;
    int addmoreclickcount = 0;
    ImageView deleteiv1,deleteiv2,deleteiv3,deleteiv4;
    EditText etLocation1,etLocation2,etLocation3,etLocation4,etQtyTon1,etQtyTon2,etQtyTon3,etQtyTon4;
    String requote_spinner1_val,requote_spinner2_val,requote_spinner3_val,requote_spinner4_val;
    int qty1 = 0;
    int qty2 = 0;
    int qty3 = 0;
    int qty4 = 0;
    int quantityint;
    String quantity,user_code,mobile_num;
    String user_code_str,mobile_num_str;
    String mob_num,fcm_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.split_sauda_patra);

        etLocation1 = (EditText)findViewById(R.id.etLocation1);
        etLocation2 = (EditText)findViewById(R.id.etLocation2);
        etLocation3 = (EditText)findViewById(R.id.etLocation3);
        etLocation4 = (EditText)findViewById(R.id.etLocation4);

        etQtyTon1 = (EditText)findViewById(R.id.etQtyTon1);
        etQtyTon2 = (EditText)findViewById(R.id.etQtyTon2);
        etQtyTon3 = (EditText)findViewById(R.id.etQtyTon3);
        etQtyTon4 = (EditText)findViewById(R.id.etQtyTon4);

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mob_num = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id",null);

        if(NetworkUtility.checkConnectivity(SplitSaudaPatraActivity.this)){


            String GetChildCompanyUrl = APIName.URL+"/buyer/getChildCompany?user_code="+userCodestr;
            System.out.println("GET CHILD COMPANY API URL IS---"+ GetChildCompanyUrl);
            GetChildCompanyAPI(GetChildCompanyUrl);


        }
        else{
            util.dialog(SplitSaudaPatraActivity.this, "Please check your internet connection.");
        }



        tradeidtxt = (TextView)findViewById(R.id.tradeidtxt);
        tradeidtxt.setText("Trade ID: "+SingletonActivity.tradeid);

        materialdesctxt = (TextView)findViewById(R.id.materialdesctxt);
        materialdesctxt.setText(SingletonActivity.categoryname);

        sellerdesctxtvw = (TextView)findViewById(R.id.sellerdesctxtvw);
        sellerdesctxtvw.setText(SingletonActivity.sellercompanyname);

        qtydesctxtvw = (TextView)findViewById(R.id.qtydesctxtvw);
        qtydesctxtvw.setText(SingletonActivity.splitqty);

        paymenttypedesctxt = (TextView)findViewById(R.id.paymenttypedesctxt);

        if(SingletonActivity.ratetype.equalsIgnoreCase("1"))
        {
            paymenttypestr = "Advance";
        }
        if(SingletonActivity.ratetype.equalsIgnoreCase("2"))
        {
            paymenttypestr = "Next Day";
        }

        if(SingletonActivity.ratetype.equalsIgnoreCase("3"))
        {
            paymenttypestr = "Regular";
        }

        paymenttypedesctxt.setText(paymenttypestr);

        locationdesctxtvw =  (TextView)findViewById(R.id.locationdesctxtvw);
        locationdesctxtvw.setText(SingletonActivity.sellercity);

        pricetontxtvw = (TextView)findViewById(R.id.pricetontxtvw);
        pricetontxtvw.setText("Rs."+SingletonActivity.rate);

        splittxtvw = (TextView)findViewById(R.id.splittxtvw);
        if(qtydesctxtvw.getText().toString().equalsIgnoreCase("0"))
        {
            splittxtvw.setEnabled(false);
            splittxtvw.setTextColor(Color.parseColor("#d3d3d3"));
        }

        else
        {
            splittxtvw.setEnabled(true);
            splittxtvw.setTextColor(Color.parseColor("#00ff00"));
        }


        notification_total_count = (TextView)findViewById(R.id.notification_total_count);
        back_split_saudapatra = (ImageView)findViewById(R.id.back_split_saudapatra);


        entersplitqtytxt = (TextView)findViewById(R.id.entersplitqtytxt);
        split_block_1 = (RelativeLayout)findViewById(R.id.split_block_1);
        split_block_2 = (RelativeLayout)findViewById(R.id.split_block_2);
        split_block_3 = (RelativeLayout)findViewById(R.id.split_block_3);
        split_block_4 = (RelativeLayout)findViewById(R.id.split_block_4);
        addmorerelative = (RelativeLayout)findViewById(R.id.addmorerelative);
        submitrelative =(RelativeLayout)findViewById(R.id.submitrelative);

        deleteiv1 = (ImageView)findViewById(R.id.deleteiv1);
        deleteiv2 = (ImageView)findViewById(R.id.deleteiv2);
        deleteiv3 = (ImageView)findViewById(R.id.deleteiv3);
        deleteiv4 = (ImageView)findViewById(R.id.deleteiv4);


        splittxtvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entersplitqtytxt.setVisibility(View.VISIBLE);
                split_block_1.setVisibility(View.VISIBLE);
                addmorerelative.setVisibility(View.VISIBLE);
                submitrelative.setVisibility(View.VISIBLE);


            }
        });

        back_split_saudapatra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SingletonActivity.isSplitSaudaClicked = true;
                  finish();
              /*  SingletonActivity.isSplitSaudaClicked = true;
                Intent i = new Intent(SplitSaudaPatraActivity.this,HomeActivity.class);
                startActivity(i);*/
            }
        });


        addmorerelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addmoreclickcount = addmoreclickcount + 1;
                if(addmoreclickcount==1)
                {
                    split_block_2.setVisibility(View.VISIBLE);
                    deleteiv2.setVisibility(View.VISIBLE);
                    deleteiv3.setVisibility(View.INVISIBLE);
                    deleteiv4.setVisibility(View.INVISIBLE);


                }

                if(addmoreclickcount==2)
                {
                    split_block_3.setVisibility(View.VISIBLE);
                    deleteiv2.setVisibility(View.INVISIBLE);
                    deleteiv3.setVisibility(View.VISIBLE);
                    deleteiv4.setVisibility(View.INVISIBLE);


                }

                if(addmoreclickcount==3)
                {
                    split_block_4.setVisibility(View.VISIBLE);
                    deleteiv2.setVisibility(View.INVISIBLE);
                    deleteiv3.setVisibility(View.INVISIBLE);
                    deleteiv4.setVisibility(View.VISIBLE);


                }

            }
        });

        deleteiv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                split_block_2.setVisibility(View.GONE);
                addmoreclickcount=0;

                etQtyTon2.setText("0");

                if(qty1>0) {
                    quantity = Integer.toString(qty1);
                    user_code_str = user_code;
                    mobile_num_str = mobile_num;
                }


            }
        });

        deleteiv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                split_block_3.setVisibility(View.GONE);
                deleteiv2.setVisibility(View.VISIBLE);
                addmoreclickcount=1;


                etQtyTon3.setText("0");

                if(qty1>0&&qty2>0) {
                    quantity = Integer.toString(qty1)+","+Integer.toString(qty2);
                    user_code_str = user_code+","+user_code;
                    mobile_num_str = mobile_num+","+mobile_num;
                }


            }
        });

        deleteiv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                split_block_4.setVisibility(View.GONE);
                deleteiv3.setVisibility(View.VISIBLE);
                addmoreclickcount=2;

                etQtyTon4.setText("0");

                if(qty1>0&&qty2>0&&qty3>0) {
                    quantity = Integer.toString(qty1)+","+Integer.toString(qty2)+","+Integer.toString(qty3);
                    user_code_str = user_code+","+user_code+","+user_code;
                    mobile_num_str = mobile_num+","+mobile_num+","+mobile_num;
                }


            }
        });

        sessionManager = new SessionManager(getApplicationContext());







        submitrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(etQtyTon1.getText().toString().length()>0) {
                    qty1 = Integer.parseInt(etQtyTon1.getText().toString());
                }
                else
                {
                    qty1 = 0;
                }
                if(etQtyTon2.getText().toString().length()>0) {
                    qty2 = Integer.parseInt(etQtyTon2.getText().toString());
                }
                else
                {
                    qty2 = 0;
                }
                if(etQtyTon3.getText().toString().length()>0) {
                    qty3 = Integer.parseInt(etQtyTon3.getText().toString());
                }
                else
                {
                    qty3 = 0;
                }
                if(etQtyTon4.getText().toString().length()>0) {
                    qty4 = Integer.parseInt(etQtyTon4.getText().toString());
                }
                else
                {
                    qty4 =0;
                }


                int totalqty = qty1 + qty2 + qty3 + qty4;

                int splitqty = Integer.parseInt(SingletonActivity.splitqty);
                if(splitqty == totalqty)
                {
                    if(qty1>0) {
                        quantity = Integer.toString(qty1);
                        user_code_str = user_code;
                        mobile_num_str = mobile_num;
                    }

                    if(qty1>0&&qty2>0) {
                        quantity = Integer.toString(qty1)+","+Integer.toString(qty2);
                        user_code_str = user_code+","+user_code;
                        mobile_num_str = mobile_num+","+mobile_num;
                    }

                    if(qty1>0&&qty2>0&&qty3>0) {
                        quantity = Integer.toString(qty1)+","+Integer.toString(qty2)+","+Integer.toString(qty3);
                        user_code_str = user_code+","+user_code+","+user_code;
                        mobile_num_str = mobile_num+","+mobile_num+","+mobile_num;
                    }

                    if(qty1>0&&qty2>0&&qty3>0&&qty4>0) {
                        quantity = Integer.toString(qty1)+","+Integer.toString(qty2)+","+Integer.toString(qty3)+","+Integer.toString(qty4);
                        user_code_str = user_code+","+user_code+","+user_code+","+user_code;
                        mobile_num_str = mobile_num+","+mobile_num+","+mobile_num+","+mobile_num;
                    }

                    System.out.println("QTY CSV IS-----"+ quantity);
                    System.out.println("USER CODE CSV IS-----"+ user_code_str);
                    System.out.println("MOBILE NUM CSV IS-----"+ mobile_num_str);

                    if(deleteiv1.getVisibility()==View.VISIBLE)
                    {



                        if(requote_spinner1_val!=null)
                        {
                            if(NetworkUtility.checkConnectivity(SplitSaudaPatraActivity.this)){


                                String SplitSaudaUrl = APIName.URL+"/buyer/splitSauda";
                                System.out.println("SPLIT SAUDA API URL IS---"+ SplitSaudaUrl);
                                SplitSaudaPatraAPI(SplitSaudaUrl,quantity,user_code_str,mobile_num_str);


                            }
                            else{
                                util.dialog(SplitSaudaPatraActivity.this, "Please check your internet connection.");
                            }


                        }
                        else
                        {
                            Toast.makeText(SplitSaudaPatraActivity.this,"Please Select Buyer",Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(deleteiv2.getVisibility()==View.VISIBLE)
                    {

                        if((requote_spinner2_val!=null) && (requote_spinner1_val!=null))
                        {
                            if(NetworkUtility.checkConnectivity(SplitSaudaPatraActivity.this)){


                                String SplitSaudaUrl = APIName.URL+"/buyer/splitSauda";
                                System.out.println("SPLIT SAUDA API URL IS---"+ SplitSaudaUrl);
                                SplitSaudaPatraAPI(SplitSaudaUrl,quantity,user_code_str,mobile_num_str);


                            }
                            else{
                                util.dialog(SplitSaudaPatraActivity.this, "Please check your internet connection.");
                            }


                        }
                        else
                        {
                            Toast.makeText(SplitSaudaPatraActivity.this,"Please Select Buyer",Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(deleteiv3.getVisibility()==View.VISIBLE)
                    {

                        if((requote_spinner3_val!=null) && (requote_spinner2_val!=null) && (requote_spinner1_val!=null))
                        {
                            if(NetworkUtility.checkConnectivity(SplitSaudaPatraActivity.this)){


                                String SplitSaudaUrl = APIName.URL+"/buyer/splitSauda";
                                System.out.println("SPLIT SAUDA API URL IS---"+ SplitSaudaUrl);
                                SplitSaudaPatraAPI(SplitSaudaUrl,quantity,user_code_str,mobile_num_str);


                            }
                            else{
                                util.dialog(SplitSaudaPatraActivity.this, "Please check your internet connection.");
                            }


                        }
                        else
                        {
                            Toast.makeText(SplitSaudaPatraActivity.this,"Please Select Buyer",Toast.LENGTH_SHORT).show();
                        }
                    }

                    if(deleteiv4.getVisibility()==View.VISIBLE)
                    {

                        if((requote_spinner4_val!= null) && (requote_spinner3_val!=null) && (requote_spinner2_val!=null) && (requote_spinner1_val!=null))
                        {
                            if(NetworkUtility.checkConnectivity(SplitSaudaPatraActivity.this)){


                                String SplitSaudaUrl = APIName.URL+"/buyer/splitSauda";
                                System.out.println("SPLIT SAUDA API URL IS---"+ SplitSaudaUrl);
                                SplitSaudaPatraAPI(SplitSaudaUrl,quantity,user_code_str,mobile_num_str);


                            }
                            else{
                                util.dialog(SplitSaudaPatraActivity.this, "Please check your internet connection.");
                            }


                        }
                        else
                        {
                            Toast.makeText(SplitSaudaPatraActivity.this,"Please Select Buyer",Toast.LENGTH_SHORT).show();
                        }
                    }



                }

                else
                {


                    Toast.makeText(SplitSaudaPatraActivity.this,"Enter Split Quantity, whose sum should be equal to Quantity purchased",Toast.LENGTH_SHORT).show();

                }

            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();

      /*  SingletonActivity.isNotificationClicked = false;

        if(NetworkUtility.checkConnectivity(SplitSaudaPatraActivity.this)){
            String NotificationCountUrl = APIName.URL+"/home/getNotification?user_code="+userCodestr;
            System.out.println("NOTIFICATION COUNT URL IS---"+ NotificationCountUrl);
            NotificationCountAPI(NotificationCountUrl);

        }
        else{
            util.dialog(SplitSaudaPatraActivity.this, "Please check your internet connection.");
        }


        System.out.println("IS NOTIFICATION CLICKED------"+ SingletonActivity.isNotificationClicked);
        ImageView notification_icon = (ImageView)findViewById(R.id.notification_icon);
        notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SingletonActivity.isNotificationClicked = true;
                NotificationDialog();



            }
        });
*/
    }

    private void SplitSaudaPatraAPI(String url,final String quantity,final String user_code_str,final String mobile_num_str) {


        final ProgressDialog pdia = new ProgressDialog(SplitSaudaPatraActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SplitSaudaPatraActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        JSONObject SplitSaudaJson = null;
                        try {
                            SplitSaudaJson = new JSONObject(response);


                            String statusstr = SplitSaudaJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true")) {

                               Toast.makeText(SplitSaudaPatraActivity.this,SplitSaudaJson.getString("message"),Toast.LENGTH_SHORT).show();
                               finish();

                            }
                            else
                            {
                                Toast.makeText(SplitSaudaPatraActivity.this,SplitSaudaJson.getString("message"),Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SplitSaudaPatraActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(SplitSaudaPatraActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                params.put("sell_id",SingletonActivity.sellid);
                params.put("old_trade_id",SingletonActivity.tradeid);
                params.put("quantity",quantity);
                params.put("user_code",user_code_str);
                params.put("mobile_num",mobile_num_str);

                System.out.println("SPLIT SAUDA PARAMS IS---"+ params);


                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(SplitSaudaPatraActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void GetChildCompanyAPI(String url) {


       final ProgressDialog pdia = new ProgressDialog(SplitSaudaPatraActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SplitSaudaPatraActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();



                        JSONObject GetChildCompanyJson = null;
                        try {
                            GetChildCompanyJson = new JSONObject(response);


                            String statusstr = GetChildCompanyJson.getString("status");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                JSONArray buyer_group_mem_jsonarray = GetChildCompanyJson.getJSONArray("buyer_group_mem");
                                System.out.println("buyer_group_mem_jsonarray IS---" + buyer_group_mem_jsonarray);




                                String first_name_str = buyer_group_mem_jsonarray.getJSONObject(0).getString("first_name");

                                final String city_str = buyer_group_mem_jsonarray.getJSONObject(0).getString("city");
                                user_code = buyer_group_mem_jsonarray.getJSONObject(0).getString("user_code");
                                mobile_num = buyer_group_mem_jsonarray.getJSONObject(0).getString("mobile_num");

                                ArrayList<String> buyeral1 = new ArrayList<String>();
                                ArrayList<String> buyeral2 = new ArrayList<String>();
                                ArrayList<String> buyeral3 = new ArrayList<String>();
                                ArrayList<String> buyeral4 = new ArrayList<String>();

                                buyeral1.clear();
                                buyeral2.clear();
                                buyeral3.clear();
                                buyeral4.clear();

                                for(int i = 0; i < buyer_group_mem_jsonarray.length(); i++)
                                {
                                    buyeral1.add(buyer_group_mem_jsonarray.getJSONObject(i).getString("company_name"));
                                    buyeral2.add(buyer_group_mem_jsonarray.getJSONObject(i).getString("company_name"));
                                    buyeral3.add(buyer_group_mem_jsonarray.getJSONObject(i).getString("company_name"));
                                    buyeral4.add(buyer_group_mem_jsonarray.getJSONObject(i).getString("company_name"));
                                }

/*
                                buyeral1.add(first_name_str);
                                buyeral2.add(first_name_str);
                                buyeral3.add(first_name_str);
                                buyeral4.add(first_name_str);*/


                                final MaterialBetterSpinner requote_spinner1 = (MaterialBetterSpinner)findViewById(R.id.buyer_spinner1);
                                ArrayAdapter<String> requoteAdapter1 = new ArrayAdapter<String>(SplitSaudaPatraActivity.this, android.R.layout.simple_spinner_dropdown_item, buyeral1);
                                requoteAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                requote_spinner1.setSelection(0);
                                requote_spinner1.setAdapter(requoteAdapter1);
                                requoteAdapter1.notifyDataSetChanged();


                                final MaterialBetterSpinner requote_spinner2 = (MaterialBetterSpinner)findViewById(R.id.buyer_spinner2);
                                ArrayAdapter<String> requoteAdapter2 = new ArrayAdapter<String>(SplitSaudaPatraActivity.this, android.R.layout.simple_spinner_dropdown_item, buyeral2);
                                requoteAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                requote_spinner2.setSelection(0);
                                requote_spinner2.setAdapter(requoteAdapter2);
                                requoteAdapter2.notifyDataSetChanged();

                                final MaterialBetterSpinner requote_spinner3 = (MaterialBetterSpinner)findViewById(R.id.buyer_spinner3);
                                ArrayAdapter<String> requoteAdapter3 = new ArrayAdapter<String>(SplitSaudaPatraActivity.this, android.R.layout.simple_spinner_dropdown_item, buyeral3);
                                requoteAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                requote_spinner3.setSelection(0);
                                requote_spinner3.setAdapter(requoteAdapter3);
                                requoteAdapter3.notifyDataSetChanged();

                                final MaterialBetterSpinner requote_spinner4 = (MaterialBetterSpinner)findViewById(R.id.buyer_spinner4);
                                ArrayAdapter<String> requoteAdapter4 = new ArrayAdapter<String>(SplitSaudaPatraActivity.this, android.R.layout.simple_spinner_dropdown_item, buyeral4);
                                requoteAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                requote_spinner4.setSelection(0);
                                requote_spinner4.setAdapter(requoteAdapter4);
                                requoteAdapter4.notifyDataSetChanged();



                                requote_spinner1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                                        if(etQtyTon1.getText().toString().length()>0) {
                                            requote_spinner1_val = parent.getItemAtPosition(position).toString();
                                            etLocation1.setText(city_str);
                                            etLocation1.setEnabled(false);

                                        }

                                        else
                                        {
                                            Toast.makeText(SplitSaudaPatraActivity.this,"Please Enter Quantity",Toast.LENGTH_SHORT).show();
                                        }


                                    }
                                });

                                requote_spinner2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                        if(etQtyTon1.getText().toString().length()>0&&etQtyTon2.getText().toString().length()>0) {
                                        requote_spinner2_val = parent.getItemAtPosition(position).toString();
                                        etLocation2.setText(city_str);
                                        etLocation2.setEnabled(false);
                                        }

                                        else
                                        {
                                            Toast.makeText(SplitSaudaPatraActivity.this,"Please Enter Quantity",Toast.LENGTH_SHORT).show();
                                        }



                                    }
                                });

                                requote_spinner3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                        if(etQtyTon1.getText().toString().length()>0&&etQtyTon2.getText().toString().length()>0&&etQtyTon3.getText().toString().length()>0) {
                                            requote_spinner3_val = parent.getItemAtPosition(position).toString();
                                            etLocation3.setText(city_str);
                                            etLocation3.setEnabled(false);
                                        }
                                        else
                                        {
                                            Toast.makeText(SplitSaudaPatraActivity.this,"Please Enter Quantity",Toast.LENGTH_SHORT).show();
                                        }






                                    }
                                });

                                requote_spinner4.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                        if(etQtyTon1.getText().toString().length()>0&&etQtyTon2.getText().toString().length()>0&&etQtyTon3.getText().toString().length()>0&&etQtyTon4.getText().toString().length()>0) {
                                            requote_spinner4_val = parent.getItemAtPosition(position).toString();
                                            etLocation4.setText(city_str);
                                            etLocation4.setEnabled(false);
                                        }
                                        else
                                        {
                                            Toast.makeText(SplitSaudaPatraActivity.this,"Please Enter Quantity",Toast.LENGTH_SHORT).show();
                                        }




                                    }
                                });




                            }
                            else
                            {
                                Toast.makeText(SplitSaudaPatraActivity.this,GetChildCompanyJson.getString("message"),Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SplitSaudaPatraActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(SplitSaudaPatraActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();





                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(SplitSaudaPatraActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {

            sessionManager.logoutUser(mob_num,fcm_id);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
