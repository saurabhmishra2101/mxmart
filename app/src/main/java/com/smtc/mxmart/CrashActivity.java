package com.smtc.mxmart;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by user on 20-04-2016.
 */
public class CrashActivity extends AppCompatActivity {
    Button tryagain;
    TextView no_inter;
    Typeface face;
    TextView app_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.crash);

      /*  Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

            @Override
            public void uncaughtException(Thread t, Throwable e) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
            }
        });
*/
       /* LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.no_internet, null, false);
        drawer.addView(contentView, 0);*/
   /*     Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_no_internet);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");*/
      /*  app_title= (TextView) findViewById(R.id.app_title);
        app_title.setTypeface(face, Typeface.BOLD);
*/
        tryagain= (Button) findViewById(R.id.tryagain_btn);
        no_inter=(TextView)findViewById(R.id.no_internet);
       // face = Typeface.createFromAsset(getApplication().getAssets(),"fonts/OpenSans-Regular.ttf");


        tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mStartActivity = new Intent(CrashActivity.this, HomeActivity.class);
                SingletonActivity.isPassedFromLogin = true;
                startActivity(mStartActivity);
            }
        });

      //  onBackPressed();

    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
    }



}
