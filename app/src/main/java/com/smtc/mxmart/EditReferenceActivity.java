package com.smtc.mxmart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;

import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static com.smtc.mxmart.R.id.cmpnynamerefdesctxtvw;
import static com.smtc.mxmart.R.id.mobilenorefdesctxtvw;
import static com.smtc.mxmart.R.id.namerefdesctxtvw;

public class EditReferenceActivity extends AppCompatActivity {

    Typeface source_sans_pro_normal;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String userCodestr,mobilenumstr;
    UtilsDialog util = new UtilsDialog();
    ProgressDialog pdia;
    String ref1cmpnynameeditrefedttxtstr,ref1nameeditrefedttxtstr,ref1mobnoeditrefedttxtstr,ref1emailideditrefedttxtstr;
    String ref2cmpnynameeditrefedttxtstr,ref2nameeditrefedttxtstr,ref2mobnoeditrefedttxtstr,ref2emailideditrefedttxtstr;
    String ref3cmpnynameeditrefedttxtstr,ref3nameeditrefedttxtstr,ref3mobnoeditrefedttxtstr,ref3emailideditrefedttxtstr;
    String ref1id,ref2id,ref3id;
    ImageView backiv;
    SessionManager sessionManager;
    String fcm_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_reference);

        SharedPreferences prefs = EditReferenceActivity.this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num",null);
        fcm_id = prefs.getString("fcm_id",null);

      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.editreferencetoolbar);
        setSupportActionBar(toolbar);*/

        sessionManager = new SessionManager(getApplicationContext());

        backiv = (ImageView)findViewById(R.id.backicon);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(EditReferenceActivity.this,HomeActivity.class);


                SingletonActivity.backfromeditinstalledcapacitydetails = false;
                SingletonActivity.backfromeditcontactdetails = false;
                SingletonActivity.backfromeditbusinessdetails = false;
                SingletonActivity.backfromeditpersonaldetails = false;
                SingletonActivity.fromviewlivetrade = false;
                SingletonActivity.fromaddnewoffer = false;
                SingletonActivity.isNotificationClicked = false;
                // SingletonActivity.index = index;
                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                SingletonActivity.FromAddNewOfferTabOne=false;
                SingletonActivity.FromAddNewOfferTabZero=false;
                SingletonActivity.isNewEnquiryClicked = false;
                SingletonActivity.frombackofbuyerdispatch = false;
                SingletonActivity.frombackofsellerdispatch = false;
                SingletonActivity.frombackofbuyerpayment = false;
                SingletonActivity.frombackofsellerpayment = false;
                SingletonActivity.fromselltodaysoffer = false;
                SingletonActivity.backfromeditreferencedetails = true;

                startActivity(i);
            }
        });


        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

        //Reference 1---
        final EditText ref1cmpnynameeditrefedttxt = (EditText)findViewById(R.id.etCmpnyNameRef1EditRefDet);
        ref1cmpnynameeditrefedttxt.setTypeface(source_sans_pro_normal);

        final EditText ref1nameeditrefedttxt = (EditText)findViewById(R.id.etNameRef1EditRefDet);
        ref1nameeditrefedttxt.setTypeface(source_sans_pro_normal);

        final EditText ref1mobnoeditrefedttxt = (EditText)findViewById(R.id.etMobileNoRef1EditRefDet);
        ref1mobnoeditrefedttxt.setTypeface(source_sans_pro_normal);

        final EditText ref1emailideditrefedttxt = (EditText)findViewById(R.id.etEmailIdRef1EditRefDet);
        ref1emailideditrefedttxt.setTypeface(source_sans_pro_normal);

        //Reference 2---
        final EditText ref2cmpnynameeditrefedttxt = (EditText)findViewById(R.id.etCmpnyNameRef2EditRefDet);
        ref2cmpnynameeditrefedttxt.setTypeface(source_sans_pro_normal);

        final EditText ref2nameeditrefedttxt = (EditText)findViewById(R.id.etNameRef2EditRefDet);
        ref2nameeditrefedttxt.setTypeface(source_sans_pro_normal);

        final EditText ref2mobnoeditrefedttxt = (EditText)findViewById(R.id.etMobileNoRef2EditRefDet);
        ref2mobnoeditrefedttxt.setTypeface(source_sans_pro_normal);

        final EditText ref2emailideditrefedttxt = (EditText)findViewById(R.id.etEmailIdRef2EditRefDet);
        ref2emailideditrefedttxt.setTypeface(source_sans_pro_normal);

        //Reference 3---
        final EditText ref3cmpnynameeditrefedttxt = (EditText)findViewById(R.id.etCmpnyNameRef3EditRefDet);
        ref3cmpnynameeditrefedttxt.setTypeface(source_sans_pro_normal);

        final EditText ref3nameeditrefedttxt = (EditText)findViewById(R.id.etNameRef3EditRefDet);
        ref3nameeditrefedttxt.setTypeface(source_sans_pro_normal);

        final EditText ref3mobnoeditrefedttxt = (EditText)findViewById(R.id.etMobileNoRef3EditRefDet);
        ref3mobnoeditrefedttxt.setTypeface(source_sans_pro_normal);

        final EditText ref3emailideditrefedttxt = (EditText)findViewById(R.id.etEmailIdRef3EditRefDet);
        ref3emailideditrefedttxt.setTypeface(source_sans_pro_normal);

        System.out.println("REFERENCE JSON ARRAY ----"+ SingletonActivity.referencejsonArray);

        if(SingletonActivity.referencejsonArray!=null) {

            try {
                ref1cmpnynameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_company_name"));
                ref1nameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_name"));
                ref1mobnoeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_mobile"));
                ref1emailideditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_email"));


                ref2cmpnynameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_company_name"));
                ref2nameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_name"));
                ref2mobnoeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_mobile"));
                ref2emailideditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_email"));

                ref3cmpnynameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_company_name"));
                ref3nameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_name"));
                ref3mobnoeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_mobile"));
                ref3emailideditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_email"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        RelativeLayout submitrel = (RelativeLayout)findViewById(R.id.sbmteditrefrelative);

        submitrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("ref2id value1---"+ ref2id);

                if(ref2id == null)
                {
                    ref2id = "";
                }

                System.out.println("ref2id value2---"+ ref2id);


                System.out.println("ref3id value1---"+ ref3id);

                if(ref3id == null)
                {
                    ref3id = "";
                }

                System.out.println("ref3id value2---"+ ref3id);


                ref1cmpnynameeditrefedttxtstr = ref1cmpnynameeditrefedttxt.getText().toString();
                ref1nameeditrefedttxtstr = ref1nameeditrefedttxt.getText().toString();
                ref1mobnoeditrefedttxtstr = ref1mobnoeditrefedttxt.getText().toString();
                ref1emailideditrefedttxtstr = ref1emailideditrefedttxt.getText().toString();

                ref2cmpnynameeditrefedttxtstr = ref2cmpnynameeditrefedttxt.getText().toString();
                ref2nameeditrefedttxtstr = ref2nameeditrefedttxt.getText().toString();
                ref2mobnoeditrefedttxtstr = ref2mobnoeditrefedttxt.getText().toString();
                ref2emailideditrefedttxtstr = ref2emailideditrefedttxt.getText().toString();

                ref3cmpnynameeditrefedttxtstr = ref3cmpnynameeditrefedttxt.getText().toString();
                ref3nameeditrefedttxtstr = ref3nameeditrefedttxt.getText().toString();
                ref3mobnoeditrefedttxtstr = ref3mobnoeditrefedttxt.getText().toString();
                ref3emailideditrefedttxtstr = ref3emailideditrefedttxt.getText().toString();


                System.out.println("USER CODE IN EDIT INSTALLED CAPACITY DETAILS FRAGMENT---"+ userCodestr);

                if(NetworkUtility.checkConnectivity(EditReferenceActivity.this)){
                    String editrefsurl = APIName.URL+"/user/insertUpdateRefDetails?user_code="+ userCodestr;
                    System.out.println("EDIT REFERENCE URL IS---"+ editrefsurl);
                    EditReferencesProfileAPI(editrefsurl,userCodestr);

                }
                else{
                    util.dialog(EditReferenceActivity.this, "Please check your internet connection.");
                }

            }
        });

        try {

            if(SingletonActivity.referencejsonArray!=null) {
                ref1id = SingletonActivity.referencejsonArray.getJSONObject(0).getString("id");
                ref2id = SingletonActivity.referencejsonArray.getJSONObject(1).getString("id");
                ref3id = SingletonActivity.referencejsonArray.getJSONObject(2).getString("id");


                if(SingletonActivity.referencejsonArray.length()==1) {
                    ref1cmpnynameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_company_name"));
                    ref1nameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_name"));
                    ref1mobnoeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_mobile"));
                    ref1emailideditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_email"));
                }

                if(SingletonActivity.referencejsonArray.length()==2) {
                    ref1cmpnynameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_company_name"));
                    ref1nameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_name"));
                    ref1mobnoeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_mobile"));
                    ref1emailideditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_email"));

                    ref2cmpnynameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_company_name"));
                    ref2nameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_name"));
                    ref2mobnoeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_mobile"));
                    ref2emailideditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_email"));
                }

                if(SingletonActivity.referencejsonArray.length()==3) {

                    ref1cmpnynameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_company_name"));
                    ref1nameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_name"));
                    ref1mobnoeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_mobile"));
                    ref1emailideditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(0).getString("ref_email"));

                    ref2cmpnynameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_company_name"));
                    ref2nameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_name"));
                    ref2mobnoeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_mobile"));
                    ref2emailideditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(1).getString("ref_email"));

                    ref3cmpnynameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_company_name"));
                    ref3nameeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_name"));
                    ref3mobnoeditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_mobile"));
                    ref3emailideditrefedttxt.setText(SingletonActivity.referencejsonArray.getJSONObject(2).getString("ref_email"));
                }
                }




        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(EditReferenceActivity.this,HomeActivity.class);


        SingletonActivity.backfromeditinstalledcapacitydetails = false;
        SingletonActivity.backfromeditcontactdetails = false;
        SingletonActivity.backfromeditbusinessdetails = false;
        SingletonActivity.backfromeditpersonaldetails = false;
        SingletonActivity.fromviewlivetrade = false;
        SingletonActivity.fromaddnewoffer = false;
        SingletonActivity.isNotificationClicked = false;
        // SingletonActivity.index = index;
        SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
        SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
        SingletonActivity.FromAddNewOfferTabOne=false;
        SingletonActivity.FromAddNewOfferTabZero=false;
        SingletonActivity.isNewEnquiryClicked = false;
        SingletonActivity.frombackofbuyerdispatch = false;
        SingletonActivity.frombackofsellerdispatch = false;
        SingletonActivity.frombackofbuyerpayment = false;
        SingletonActivity.frombackofsellerpayment = false;
        SingletonActivity.fromselltodaysoffer = false;
        SingletonActivity.backfromeditreferencedetails = true;

        startActivity(i);
    }

    private void EditReferencesProfileAPI(final String url, final String usercode) {
        pdia = new ProgressDialog(EditReferenceActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(EditReferenceActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        System.out.println("EDIT REFERENCES URL in resp IS---"+ url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();



                        System.out.println("RESPONSE OF EDIT REFERENCE API IS---" + response);



                        try {



                            JSONObject editinstalledcapacityjson = new JSONObject(response);
                            System.out.println("EDIT REFERENCE JSON IS---" + editinstalledcapacityjson);

                            String statusstr = editinstalledcapacityjson.getString("status");
                            String msgstr = editinstalledcapacityjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Toast.makeText(EditReferenceActivity.this,msgstr,Toast.LENGTH_SHORT).show();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent(EditReferenceActivity.this,HomeActivity.class);

                                        SingletonActivity.backfromeditreferencedetails = true;
                                        SingletonActivity.backfromeditinstalledcapacitydetails = false;
                                        SingletonActivity.backfromeditcontactdetails = false;
                                        SingletonActivity.backfromeditbusinessdetails = false;
                                        SingletonActivity.backfromeditpersonaldetails = false;
                                        SingletonActivity.fromviewlivetrade = false;
                                        SingletonActivity.fromaddnewoffer = false;
                                        SingletonActivity.isNotificationClicked = false;
                                        // SingletonActivity.index = index;
                                        SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                        SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                        SingletonActivity.FromAddNewOfferTabOne=false;
                                        SingletonActivity.FromAddNewOfferTabZero=false;
                                        SingletonActivity.isNewEnquiryClicked = false;
                                        SingletonActivity.frombackofbuyerdispatch = false;
                                        SingletonActivity.frombackofsellerdispatch = false;
                                        SingletonActivity.frombackofbuyerpayment = false;
                                        SingletonActivity.frombackofsellerpayment = false;


                                        startActivity(i);

                                    }
                                }, 2000);
                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditReferenceActivity.this,error.toString(),Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("ERROR CODE--------" + networkResponse.statusCode);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(EditReferenceActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(EditReferenceActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(EditReferenceActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id",ref1id+","+ref2id+","+ref3id);
                params.put("ref_company_name",ref1cmpnynameeditrefedttxtstr+","+ref2cmpnynameeditrefedttxtstr+","+ref3cmpnynameeditrefedttxtstr);
                params.put("ref_name",ref1nameeditrefedttxtstr+","+ref2nameeditrefedttxtstr+","+ref3nameeditrefedttxtstr);
                params.put("ref_mobile",ref1mobnoeditrefedttxtstr+","+ref2mobnoeditrefedttxtstr+","+ref3mobnoeditrefedttxtstr);
                params.put("ref_email",ref1emailideditrefedttxtstr+","+ref2emailideditrefedttxtstr+","+ref3emailideditrefedttxtstr);
                params.put("mobile_num",mobilenumstr);



                System.out.println("edit references params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditReferenceActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
        //  return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                System.out.println("OVERFLOWMENU ITEM 1 CLICKED");

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("login",false);


                editor.clear();
                editor.commit();

                sessionManager.logoutUser(mobilenumstr,fcm_id);


                // sessionManager.logoutUser();

            /*    Intent i = new Intent(LeaveManagementActivity.this,LoginActivity.class);
                startActivity(i);
*/
                return true;
          /*  case R.id.overflowmenuitem2:
                System.out.println("OVERFLOWMENU ITEM 2 CLICKED");

                return true;
            case R.id.overflowmenuitem3:
                System.out.println("OVERFLOWMENU ITEM 3 CLICKED");

                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
