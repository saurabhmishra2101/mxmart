package com.smtc.mxmart;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class AddNewOfferLiveTradeActivity extends AppCompatActivity {


    String[] SubCategoryArray = {"Lumps DR-CLO", "Fines DR-CLO", "Mix 0-20 DR-CLO"};
    MaterialBetterSpinner materialDesignSpinner,materialDesignSpinner1,materialDesignSpinner2;
    String selectedmatcatspinnerstr,selectedsubcatspinnerstr,selectedpromocodestr;
    Typeface  source_sans_pro_normal;
    ImageView addnewofferbackiv;
    UtilsDialog util=new UtilsDialog();
    ArrayList<String> materialcatarraylist = new ArrayList<String>();
    ArrayList<String> materialcatidarraylist = new ArrayList<String>();
    ArrayList<String> materialsubcatarraylist = new ArrayList<String>();
    String category_id;
    TextView quality1,quality2,quality3,quality4,quality5,quality6,quality7,quality8;
    EditText etqty1,etqty2,etqty3,etqty4,etqty5,etqty6,etqty7,etqty8,termsandtitledesctxt,regularratedesctxt,nextdayratedesctxt,advanceratedesctxt,quantitytondesctxt;
    RelativeLayout submitrel;
    String etqty1str,etqty2str,etqty3str,etqty4str,etqty5str,etqty6str,etqty7str,etqty8str,termsandtitledesctxtstr,regularratedesctxtstr,nextdayratedesctxtstr,advanceratedesctxtstr,quantitytondesctxtstr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String userCodestr,mobilenumstr,sub_cat_id;
    HashMap<String, String> subCatHashMap = new HashMap<String, String>();
    String subcatnamefromsubcatjsonarrstr;
    TextView subcatidtxt;
    String subcatidfromsubcatjsonarrstr;
    ArrayList<String> promocodelist = new ArrayList<String>();
    ArrayList<String> promocodefinallist = new ArrayList<String>();
    MaterialBetterSpinner promotion_spinner;
    //Spinner promotion_spinner;
    JSONArray  PromotionsJSONArray;
    String promocodevalwithoutspaceonitemclick;
    TableLayout promo_table;
    TableRow tr_header,tr_item_1,tr_item_2,tr_item_3,tr_item_4;
    View line;
    TextView tv_qtyfromdesc1,tv_qtyfromdesc2,tv_qtyfromdesc3,tv_qtyfromdesc4,no_promotion_txt;
    TextView tv_qtytodesc1,tv_qtytodesc2,tv_qtytodesc3,tv_qtytodesc4;
    TextView tv_discdesc1,tv_discdesc2,tv_discdesc3,tv_discdesc4;
    String sellerid,categoryid,subcategoryid,qtyofferedstr,categorydesc,subcategorydesc,advanceratedesc,nextdayratedesc,regularratedesc,q1val,q2val,q3val,q4val,q5val,q6val,q7val,q8val,promocodeval,tncval;
    RelativeLayout cancelrelative;
    ProgressDialog pdia;
    SessionManager sessionManager;
    TextView todays_offer_count,new_enquiry_count,seller_accepted_count,seller_rejected_count,sp_generated_count,buyer_rejected_count;
    String commentStr,tradeStr;
    View tabView,tabView1,tabView2,tabView3;
    TextView livetradetxt,buytxt,selltxt,myprofiletxt;
    ImageView livetradeimgvw,buyimgvw,sellimgvw,myprofileimgvw;
    TextView notification_total_count;
    int index;
    ImageView notification_icon;
    String mobile_num,fcm_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_offer_live_trade1);
      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.addnewoffertoolbar1);
        setSupportActionBar(toolbar);
      */
        System.out.println("SubCategoryArrayList in Add New Offer Live Trade Activity-----"+ SingletonActivity.SubCategoryArrayList);
        sessionManager = new SessionManager(getApplicationContext());
        notification_total_count = (TextView)findViewById(R.id.notification_total_count);

        materialDesignSpinner1 = (MaterialBetterSpinner)
                findViewById(R.id.material_category_spinner);
        materialDesignSpinner2 = (MaterialBetterSpinner)
                findViewById(R.id.sub_category_spinner);

        materialcatarraylist.add("Categories");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeActivity.this,
                android.R.layout.simple_dropdown_item_1line, materialcatarraylist);

        materialDesignSpinner1.setAdapter(arrayAdapter);
        materialDesignSpinner1.setClickable(false);

        materialDesignSpinner1.setTypeface(source_sans_pro_normal);

        materialsubcatarraylist.add("Sub Categories");
        ArrayAdapter<String> SubCatArrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeActivity.this,
                android.R.layout.simple_dropdown_item_1line, materialsubcatarraylist);

        materialDesignSpinner2.setAdapter(SubCatArrayAdapter);

        materialDesignSpinner2.setClickable(false);

        System.out.println("TAB NUMBER IN ADD NEW OFFER--------"+SingletonActivity.validatetab);

        promocodelist.clear();
        promocodefinallist.clear();

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        mobile_num = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id",null);

        System.out.println("USER CODE IN MAIN ACTIVITY---"+ userCodestr);

        cancelrelative = (RelativeLayout)findViewById(R.id.cancelrelative);
        cancelrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_qtyfromdesc1 = (TextView)findViewById(R.id.tv_qtyfromdesc1);
        tv_qtyfromdesc2 = (TextView)findViewById(R.id.tv_qtyfromdesc2);
        tv_qtyfromdesc3 = (TextView)findViewById(R.id.tv_qtyfromdesc3);
        tv_qtyfromdesc4= (TextView)findViewById(R.id.tv_qtyfromdesc4);
        tv_qtytodesc1 = (TextView)findViewById(R.id.tv_qtytodesc1);
        tv_qtytodesc2 = (TextView)findViewById(R.id.tv_qtytodesc2);
        tv_qtytodesc3 = (TextView)findViewById(R.id.tv_qtytodesc3);
        tv_qtytodesc4 = (TextView)findViewById(R.id.tv_qtytodesc4);
        tv_discdesc1 = (TextView)findViewById(R.id.tv_discdesc1);
        tv_discdesc2 = (TextView)findViewById(R.id.tv_discdesc2);
        tv_discdesc3 = (TextView)findViewById(R.id.tv_discdesc3);
        tv_discdesc4 = (TextView)findViewById(R.id.tv_discdesc4);

        no_promotion_txt = (TextView)findViewById(R.id.no_promotion_txt);

        submitrel = (RelativeLayout)findViewById(R.id.sbmtrelative);
        promo_table = (TableLayout)findViewById(R.id.promo_table);
        tr_header = (TableRow)findViewById(R.id.tr_header);
        tr_item_1 = (TableRow)findViewById(R.id.tr_item_1);
        tr_item_2= (TableRow)findViewById(R.id.tr_item_2);
        tr_item_3 = (TableRow)findViewById(R.id.tr_item_3);
        tr_item_4 = (TableRow)findViewById(R.id.tr_item_4);
        line = (View)findViewById(R.id.line);

        quality1 = (TextView)findViewById(R.id.quality1);
        quality2 = (TextView)findViewById(R.id.quality2);
        quality3 = (TextView)findViewById(R.id.quality3);
        quality4 = (TextView)findViewById(R.id.quality4);
        quality5 = (TextView)findViewById(R.id.quality5);
        quality6 = (TextView)findViewById(R.id.quality6);
        quality7 = (TextView)findViewById(R.id.quality7);
        quality8 = (TextView)findViewById(R.id.quality8);
        etqty1 = (EditText)findViewById(R.id.etqty1);
        etqty2 = (EditText)findViewById(R.id.etqty2);
        etqty3 = (EditText)findViewById(R.id.etqty3);
        etqty4 = (EditText)findViewById(R.id.etqty4);
        etqty5 = (EditText)findViewById(R.id.etqty5);
        etqty6 = (EditText)findViewById(R.id.etqty6);
        etqty7 = (EditText)findViewById(R.id.etqty7);
        etqty8 = (EditText)findViewById(R.id.etqty8);

        termsandtitledesctxt = (EditText)findViewById(R.id.termsandtitledesctxt);
        regularratedesctxt = (EditText)findViewById(R.id.regularratedesctxt);
        nextdayratedesctxt = (EditText)findViewById(R.id.nextdayratedesctxt);
        advanceratedesctxt = (EditText)findViewById(R.id.advanceratedesctxt);
        quantitytondesctxt = (EditText)findViewById(R.id.quantitytondesctxt);

       // promocodefinallist.add("Promo Code");

        promotion_spinner = (MaterialBetterSpinner) findViewById(R.id.promotion_spinner);


        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

        promotion_spinner.setTypeface(source_sans_pro_normal);

        if(NetworkUtility.checkConnectivity(AddNewOfferLiveTradeActivity.this)){
            String GetPromoUrl = APIName.URL+"/seller/getPromo?user_code="+userCodestr;
            System.out.println("GET PROMO URL IS---"+ GetPromoUrl);
            GetPromoAPI(GetPromoUrl);

        }
        else{
            util.dialog(AddNewOfferLiveTradeActivity.this, "Please check your internet connection.");
        }


      /*  promotion_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // promocodefinallist.remove(0);
                if(NetworkUtility.checkConnectivity(AddNewOfferLiveTradeActivity.this)){
                    String GetPromoUrl = APIName.URL+"/seller/getPromo?user_code="+userCodestr;
                    System.out.println("GET PROMO URL IS---"+ GetPromoUrl);
                    GetPromoAPI(GetPromoUrl);

                }
                else{
                    util.dialog(AddNewOfferLiveTradeActivity.this, "Please check your internet connection.");
                }

            }
        });
*/

        Bundle b = new Bundle();
        b = getIntent().getExtras();

        if(b!=null)
        {

            qtyofferedstr = b.getString("qtyofferedstr");
             categorydesc =b.getString("categorydesc");
             subcategorydesc =b.getString("subcategorydesc");
             advanceratedesc =b.getString("advanceratedesc");
             nextdayratedesc =b.getString("nextdayratedesc");
             regularratedesc =b.getString("regularratedesc");
             q1val =b.getString("q1val");
             q2val =b.getString("q2val");
             q3val =b.getString("q3val");
             q4val =b.getString("q4val");
             q5val =b.getString("q5val");
             q6val =b.getString("q6val");
             q7val =b.getString("q7val");
             q8val =b.getString("q8val");
             promocodeval =b.getString("promocodeval");
             tncval =b.getString("tncval");
             sellerid = b.getString("sellerid");
            categoryid = b.getString("categoryid");
            subcategoryid = b.getString("subcategoryid");

            materialDesignSpinner1.setText(categorydesc);
            materialDesignSpinner2.setText(subcategorydesc);
            etqty1.setText(q1val);
            etqty2.setText(q2val);
            etqty3.setText(q3val);
            etqty4.setText(q4val);
            etqty5.setText(q5val);
            etqty6.setText(q6val);
            etqty7.setText(q7val);
            etqty8.setText(q8val);
            quantitytondesctxt.setText(qtyofferedstr);
            advanceratedesctxt.setText(advanceratedesc);
            nextdayratedesctxt.setText(nextdayratedesc);
            regularratedesctxt.setText(regularratedesc);
            termsandtitledesctxt.setText(tncval);
            System.out.println("PROMO CODE IN EDIT---"+ promocodeval);
            if(!promocodeval.equalsIgnoreCase("0"))
            {
                promotion_spinner.setText("Promo Code :"+promocodeval);
            }
            else
            {
                promotion_spinner.setText("");
            }

        }







        if(NetworkUtility.checkConnectivity(AddNewOfferLiveTradeActivity.this)){
            String GetProductMasterUrl = APIName.URL+"/home/getProductMaster";
            System.out.println("GET PRODUCT MASTER URL IS---"+ GetProductMasterUrl);
            GetProductMasterAPI(GetProductMasterUrl);

        }
        else{
            util.dialog(AddNewOfferLiveTradeActivity.this, "Please check your internet connection.");
        }


        submitrel.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {


                                             boolean invalid = false;

                                             if (materialDesignSpinner1.getText().toString().equals("")) {
                                                 invalid = true;
                                                 Toast.makeText(getApplicationContext(), "Please Select Category",
                                                         Toast.LENGTH_SHORT).show();
                                             } else if (materialDesignSpinner2.getText().toString().equals("")) {
                                                 invalid = true;
                                                 Toast.makeText(getApplicationContext(),
                                                         "Please Select Sub Category", Toast.LENGTH_SHORT)
                                                         .show();
                                             } else if (etqty1.getText().toString().equals("")) {
                                                 invalid = true;
                                                 Toast.makeText(getApplicationContext(),
                                                         "Please Fill the all Mandatory Fields", Toast.LENGTH_SHORT)
                                                         .show();
                                             } else if (etqty2.getText().toString().equals("")) {
                                                 invalid = true;
                                                 Toast.makeText(getApplicationContext(),
                                                         "Please Fill the all Mandatory Fields", Toast.LENGTH_SHORT)
                                                         .show();
                                             } else if (etqty3.getText().toString().equals("")) {
                                                 invalid = true;
                                                 Toast.makeText(getApplicationContext(),
                                                         "Please Fill the all Mandatory Fields", Toast.LENGTH_SHORT)
                                                         .show();
                                             } else if (etqty4.getText().toString().equals("")) {
                                                 invalid = true;
                                                 Toast.makeText(getApplicationContext(),
                                                         "Please Fill the all Mandatory Fields", Toast.LENGTH_SHORT)
                                                         .show();


                                             } else if (etqty5.getText().toString().equals("")) {
                                                 invalid = true;
                                                 Toast.makeText(getApplicationContext(),
                                                         "Please Fill the all Mandatory Fields", Toast.LENGTH_SHORT)
                                                         .show();


                                             } else if (etqty6.getText().toString().equals("")) {
                                                 invalid = true;
                                                 Toast.makeText(getApplicationContext(),
                                                         "Please Fill the all Mandatory Fields", Toast.LENGTH_SHORT)
                                                         .show();


                                             } else if (etqty7.getText().toString().equals("")) {
                                                 invalid = true;
                                                 Toast.makeText(getApplicationContext(),
                                                         "Please Fill the all Mandatory Fields", Toast.LENGTH_SHORT)
                                                         .show();
                                             } else if (etqty8.getText().toString().equals("")) {
                                                 invalid = true;
                                                 Toast.makeText(getApplicationContext(),
                                                         "Please Fill the all Mandatory Fields", Toast.LENGTH_SHORT)
                                                         .show();
                                             } else if (!isValidQuantity(quantitytondesctxt.getText().toString())) {
                                                 invalid = true;

                                            }


                                             float advrate = 0.0f;

                                             System.out.println("ADVANCE RATE IS==="+ advanceratedesctxt.getText().toString());

                                             if(!advanceratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                 advrate = Float.parseFloat(advanceratedesctxt.getText().toString());
                                             }

                                             float nextdayrate = 0.0f;

                                             if(!nextdayratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                 nextdayrate = Float.parseFloat(nextdayratedesctxt.getText().toString());
                                             }

                                             float regularrate = 0.0f;

                                             if(!regularratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                 regularrate = Float.parseFloat(regularratedesctxt.getText().toString());
                                             }

                                             float total = advrate + nextdayrate + regularrate;

                                             if (total==0&&(!quantitytondesctxt.getText().toString().equalsIgnoreCase(""))){

                                                 Toast.makeText(getApplicationContext(),
                                                         "Please Enter Valid Amount", Toast.LENGTH_SHORT)
                                                         .show();

                                             }

                                            /* else if ((!isValidRate(advanceratedesctxt.getText().toString())&&(!isValidRate(nextdayratedesctxt.getText().toString())&&(!isValidRate(regularratedesctxt.getText().toString()))))) {
                                                invalid = true;

                                             }*/








                                                 else if (invalid == false) {

                                                 String txt;

                                                 if (SingletonActivity.isUpdateClicked == true) {
                                                      txt = "Update";
                                                 }
                                                 else
                                                 {
                                                     txt = "Add New";
                                                 }

                                         //        submitrel.setEnabled(false);

                                                 new AlertDialog.Builder(AddNewOfferLiveTradeActivity.this)
                                                         .setTitle("Confirmation")
                                                         .setMessage("Confirm to "+ txt +" Offer for " + quantitytondesctxt.getText().toString() + "mt of " + materialDesignSpinner1.getText().toString() + "?")


                                                         .setIcon(android.R.drawable.ic_dialog_alert)
                                                         .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                                             public void onClick(DialogInterface dialog, int whichButton) {
                                                                 System.out.println("IN TAB NUMBER---" + SingletonActivity.validatetab);

                                                                 if (SingletonActivity.isUpdateClicked == true) {



                                                                     etqty1str = etqty1.getText().toString();
                                                                     etqty2str = etqty2.getText().toString();
                                                                     etqty3str = etqty3.getText().toString();
                                                                     etqty4str = etqty4.getText().toString();
                                                                     etqty5str = etqty5.getText().toString();
                                                                     etqty6str = etqty6.getText().toString();
                                                                     etqty7str = etqty7.getText().toString();
                                                                     etqty8str = etqty8.getText().toString();

                                                                     termsandtitledesctxtstr = termsandtitledesctxt.getText().toString();
                                                                     regularratedesctxtstr = regularratedesctxt.getText().toString();
                                                                     nextdayratedesctxtstr = nextdayratedesctxt.getText().toString();
                                                                     advanceratedesctxtstr = advanceratedesctxt.getText().toString();
                                                                     quantitytondesctxtstr = quantitytondesctxt.getText().toString();

                                                                     if (NetworkUtility.checkConnectivity(AddNewOfferLiveTradeActivity.this)) {
                                                                         String UpdateOfferUrl = APIName.URL + "/seller/updateOffer?sell_id=" + sellerid;
                                                                         System.out.println("UPDATE OFFER URL IS---" + UpdateOfferUrl);
                                                                         UpdateOfferAPI(UpdateOfferUrl);

                                                                     } else {
                                                                         util.dialog(AddNewOfferLiveTradeActivity.this, "Please check your internet connection.");
                                                                     }

                                                                 }



                                                                 else {



                                                                     etqty1str = etqty1.getText().toString();
                                                                     etqty2str = etqty2.getText().toString();
                                                                     etqty3str = etqty3.getText().toString();
                                                                     etqty4str = etqty4.getText().toString();
                                                                     etqty5str = etqty5.getText().toString();
                                                                     etqty6str = etqty6.getText().toString();
                                                                     etqty7str = etqty7.getText().toString();
                                                                     etqty8str = etqty8.getText().toString();

                                                                     termsandtitledesctxtstr = termsandtitledesctxt.getText().toString();
                                                                     regularratedesctxtstr = regularratedesctxt.getText().toString();
                                                                     nextdayratedesctxtstr = nextdayratedesctxt.getText().toString();
                                                                     advanceratedesctxtstr = advanceratedesctxt.getText().toString();
                                                                     quantitytondesctxtstr = quantitytondesctxt.getText().toString();




                                                                          if (NetworkUtility.checkConnectivity(AddNewOfferLiveTradeActivity.this)) {
                                                                              String InsertOfferUrl = APIName.URL + "/seller/insertOffer";
                                                                              System.out.println("ADD OFFER URL IS---" + InsertOfferUrl);
                                                                              InsertOfferAPI(InsertOfferUrl);

                                                                          } else {
                                                                              util.dialog(AddNewOfferLiveTradeActivity.this, "Please check your internet connection.");
                                                                          }

                                                                 }
                                                             }})
                                                         .setNegativeButton(android.R.string.no, null).show();



                                             }


                                         }
                                     }
        );










        addnewofferbackiv = (ImageView)findViewById(R.id.backicon);
        addnewofferbackiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SingletonActivity.fromselltodaysoffer = false;



                finish();



            }
        });




        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");



    }


    @Override
    protected void onResume() {
        super.onResume();



    }





    private void NotificationResetAPI(String url,final String commentStr,final String tradeStr,final int index) {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF NOTIFICATION RESET API IS---" + response);

                        JSONObject NotificationResetJson = null;

                        try {
                            NotificationResetJson = new JSONObject(response);

                            String statusstr = NotificationResetJson.getString("status");
                            System.out.println("STATUS OF NOTIFICATION RESET API IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {


                              Intent i = new Intent(AddNewOfferLiveTradeActivity.this,HomeActivity.class);

                                SingletonActivity.isNotificationClicked = false;
                                SingletonActivity.index = index;
                                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                SingletonActivity.FromAddNewOfferTabOne=false;
                                SingletonActivity.FromAddNewOfferTabZero=false;
                                SingletonActivity.isNewEnquiryClicked = false;

                                SingletonActivity.from = "Add New Offer";
                              //  i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                startActivity(i);
                                // Toast.makeText(AddNewOfferLiveTradeActivity.this,NotificationResetJson.getString("message"),Toast.LENGTH_SHORT).show();

                            }
                            else
                            {
                                // Toast.makeText(AddNewOfferLiveTradeActivity.this,NotificationResetJson.getString("message"),Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeActivity.this,"Some Error Occured, please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_code",userCodestr);
                params.put("comment",commentStr);
                params.put("trader",tradeStr);
                System.out.println("notification reset params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(AddNewOfferLiveTradeActivity.this);
        requestQueue.add(stringRequest);
    }




    public  final boolean isValidQuantity(String qty) {

        if(qty.length()>0) {

            int qty_val = Integer.parseInt(qty);
            if (qty_val < 50) {

                Toast.makeText(getApplicationContext(),
                        "Enter Quantity Greater Or Equal To 50.", Toast.LENGTH_SHORT)
                        .show();
                return false;
            } else {
                return true;
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(),
                    "Enter Quantity Greater Or Equal To 50.", Toast.LENGTH_SHORT)
                    .show();
            return false;
        }
    }







    private void UpdateOfferAPI(final String url) {
     /*   pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();*/


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //Toast.makeText(AddNewOfferLiveTradeActivity.this,response,Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        try {

                            JSONObject insertofferjson = new JSONObject(response);
                            System.out.println("UPDATE OFFER JSON IS---" + insertofferjson);

                            String statusstr = insertofferjson.getString("status");
                            String msgstr = insertofferjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                    Intent i = new Intent(AddNewOfferLiveTradeActivity.this,HomeActivity.class);
                                SingletonActivity.fromupdatenewoffer = true;
                                SingletonActivity.fromaddnewoffer = true;
                                SingletonActivity.isNotificationClicked = false;
                                SingletonActivity.index = index;
                                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                SingletonActivity.FromAddNewOfferTabOne=false;
                                SingletonActivity.FromAddNewOfferTabZero=false;
                                SingletonActivity.isNewEnquiryClicked = false;
                                SingletonActivity.fromselltodaysoffer = true;
                                startActivity(i);





                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            //  pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }















                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AddNewOfferLiveTradeActivity.this,error.toString(),Toast.LENGTH_SHORT).show();
                        //     pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(AddNewOfferLiveTradeActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {


                Map<String, String> params = new HashMap<String, String>();
                params.put("user_code",userCodestr);
                params.put("mobile_num",mobilenumstr);
                params.put("category",categoryid);
                params.put("sub_category", subcategoryid);
                params.put("q1",etqty1str);
                params.put("q2",etqty2str);
                params.put("q3",etqty3str);
                params.put("q4",etqty4str);
                params.put("q5",etqty5str);
                params.put("q6",etqty6str);
                params.put("q7",etqty7str);
                params.put("q8",etqty8str);



                params.put("advance_rate",advanceratedesctxtstr);
                params.put("next_day_rate",nextdayratedesctxtstr);
                params.put("regular_rate",regularratedesctxtstr);
                params.put("quantity",quantitytondesctxtstr);

                String promocodevalwithoutspace;
                System.out.println("SELECTED PROMO CODE STRING IS---"+ selectedpromocodestr);
                if(selectedpromocodestr!=null) {
                    String[] selectedpromocodestring = selectedpromocodestr.split(":");
                    String promocodeval = selectedpromocodestring[1];


                    promocodevalwithoutspace = promocodeval.replaceAll("\\s+", "");


                    System.out.println("PROMO CODE VALUE without space IN IF---" + promocodevalwithoutspace);
                }
                else
                {
                    promocodevalwithoutspace = "0";
                    System.out.println("PROMO CODE VALUE without space IN ELSE---" + promocodevalwithoutspace);
                }
                params.put("promo_code",promocodevalwithoutspace);
                params.put("sell_tandc",termsandtitledesctxtstr);







                System.out.println("update offer params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(AddNewOfferLiveTradeActivity.this);
        requestQueue.add(stringRequest);
    }




    private void GetPromoAPI(String url) {


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF GET PROMO API IS---" + response);



                         JSONObject PromotionsJson = null;
                        try {
                            PromotionsJson = new JSONObject(response);


                            String statusstr = PromotionsJson.getString("status");


                            if (statusstr.equalsIgnoreCase("true"))
                            {
                                   PromotionsJSONArray = PromotionsJson.getJSONArray("promotions");
                                  System.out.println("PROMO JSON ARRAY  LENGTH IS 1---" + PromotionsJSONArray.length());

                                for(int i = 0; i < PromotionsJSONArray.length();i++)
                                {
                                    JSONArray promo_inner_array = PromotionsJSONArray.getJSONArray(i);
                                    System.out.println("PROMO INNER JSON ARRAY IS---" + promo_inner_array);

                                    for(int j = 0 ; j < promo_inner_array.length();j++)
                                    {
                                        String promocode = promo_inner_array.getJSONObject(j).getString("promo_code");
                                        System.out.println("PROMO CODE IS---" + promocode);


                                        promocodelist.add(promocode);



                                    }
                                }



                                Set<String> hs = new HashSet<>();
                                hs.addAll(promocodelist);
                                promocodelist.clear();
                                promocodelist.addAll(hs);

                                Collections.sort(promocodelist);

                                for(int k = 0; k< promocodelist.size();k++)
                                {
                                    String promoccodenewstr = "Promo Code : "+promocodelist.get(k);
                                    promocodefinallist.add(promoccodenewstr);


                                }
                                System.out.println("PROMO CODE FINAL LIST IS---" + promocodefinallist);




                                ArrayAdapter<String> PromotionAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeActivity.this,
                                        android.R.layout.simple_dropdown_item_1line, promocodefinallist);

                                promotion_spinner.setAdapter(PromotionAdapter);

                                source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

                                promotion_spinner.setTypeface(source_sans_pro_normal);

                                promotion_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                        selectedpromocodestr = parent.getItemAtPosition(position).toString();
                                        System.out.println("SELECTED PROMO CODE ON ITEM CLICKED---"+ selectedpromocodestr);

                                        String[] selectedpromocodestring = selectedpromocodestr.split(":");
                                        String promocodeval = selectedpromocodestring[1];


                                        promocodevalwithoutspaceonitemclick = promocodeval.replaceAll("\\s+", "");


                                        System.out.println("PROMO CODE VALUE without space in on item click---" + promocodevalwithoutspaceonitemclick);

                                        try {


                                            System.out.println("PROMO JSON ARRAY in on item click IS---" + PromotionsJSONArray.length());

                                            for(int i = 0; i < PromotionsJSONArray.length();i++)
                                            {

                                                System.out.println("PromotionsJSONArray length---" + i);

                                               // JSONArray promo_inner_array = PromotionsJSONArray.getJSONArray(i);

                                                JSONArray promo_inner_array = PromotionsJSONArray.getJSONArray(i);
                                                System.out.println("PROMO INNER JSON ARRAY IS---" + promo_inner_array);

                                                for(int j = 0 ; j < promo_inner_array.length();j++)
                                                {
                                                    String promocode = promo_inner_array.getJSONObject(j).getString("promo_code");
                                                    System.out.println("PROMO CODE IS---" + promocode);


                                                    if(promocodevalwithoutspaceonitemclick.equalsIgnoreCase(promocode))
                                                    {
                                                        System.out.println("PROMO INNER JSON ARRAY IN ON ITEM CLICK IF EQUAL IS---" + promo_inner_array);

                                                        if(promo_inner_array.length()==0)
                                                        {
                                                            tr_header.setVisibility(View.GONE);
                                                            tr_item_1.setVisibility(View.GONE);
                                                            tr_item_2.setVisibility(View.GONE);
                                                            tr_item_3.setVisibility(View.GONE);
                                                            tr_item_4.setVisibility(View.GONE);
                                                            line.setVisibility(View.GONE);
                                                        }

                                                        if(promo_inner_array.length()==1)
                                                        {
                                                            tr_header.setVisibility(View.VISIBLE);
                                                            tr_item_1.setVisibility(View.VISIBLE);
                                                            tr_item_2.setVisibility(View.GONE);
                                                            tr_item_3.setVisibility(View.GONE);
                                                            tr_item_4.setVisibility(View.GONE);
                                                            line.setVisibility(View.VISIBLE);

                                                            tv_qtyfromdesc1.setText(promo_inner_array.getJSONObject(0).getString("from_qty"));
                                                            tv_qtytodesc1.setText(promo_inner_array.getJSONObject(0).getString("to_qty"));
                                                            tv_discdesc1.setText("Rs. "+promo_inner_array.getJSONObject(0).getString("discount_per_ton"));


                                                        }

                                                        if(promo_inner_array.length()==2)
                                                        {
                                                            tr_header.setVisibility(View.VISIBLE);
                                                            tr_item_1.setVisibility(View.VISIBLE);
                                                            tr_item_2.setVisibility(View.VISIBLE);
                                                            tr_item_3.setVisibility(View.GONE);
                                                            tr_item_4.setVisibility(View.GONE);
                                                            line.setVisibility(View.VISIBLE);

                                                            tv_qtyfromdesc1.setText(promo_inner_array.getJSONObject(0).getString("from_qty"));
                                                            tv_qtytodesc1.setText(promo_inner_array.getJSONObject(0).getString("to_qty"));
                                                            tv_discdesc1.setText("Rs. "+promo_inner_array.getJSONObject(0).getString("discount_per_ton"));

                                                            tv_qtyfromdesc2.setText(promo_inner_array.getJSONObject(1).getString("from_qty"));
                                                            tv_qtytodesc2.setText(promo_inner_array.getJSONObject(1).getString("to_qty"));
                                                            tv_discdesc2.setText("Rs. "+promo_inner_array.getJSONObject(1).getString("discount_per_ton"));
                                                        }

                                                        if(promo_inner_array.length()==3)
                                                        {
                                                            tr_header.setVisibility(View.VISIBLE);
                                                            tr_item_1.setVisibility(View.VISIBLE);
                                                            tr_item_2.setVisibility(View.VISIBLE);
                                                            tr_item_3.setVisibility(View.VISIBLE);
                                                            tr_item_4.setVisibility(View.GONE);
                                                            line.setVisibility(View.VISIBLE);

                                                            tv_qtyfromdesc1.setText(promo_inner_array.getJSONObject(0).getString("from_qty"));
                                                            tv_qtytodesc1.setText(promo_inner_array.getJSONObject(0).getString("to_qty"));
                                                            tv_discdesc1.setText("Rs. "+promo_inner_array.getJSONObject(0).getString("discount_per_ton"));

                                                            tv_qtyfromdesc2.setText(promo_inner_array.getJSONObject(1).getString("from_qty"));
                                                            tv_qtytodesc2.setText(promo_inner_array.getJSONObject(1).getString("to_qty"));
                                                            tv_discdesc2.setText("Rs. "+promo_inner_array.getJSONObject(1).getString("discount_per_ton"));

                                                            tv_qtyfromdesc3.setText(promo_inner_array.getJSONObject(2).getString("from_qty"));
                                                            tv_qtytodesc3.setText(promo_inner_array.getJSONObject(2).getString("to_qty"));
                                                            tv_discdesc3.setText("Rs. "+promo_inner_array.getJSONObject(2).getString("discount_per_ton"));
                                                        }

                                                        if(promo_inner_array.length()==4)
                                                        {
                                                            tr_header.setVisibility(View.VISIBLE);
                                                            tr_item_1.setVisibility(View.VISIBLE);
                                                            tr_item_2.setVisibility(View.VISIBLE);
                                                            tr_item_3.setVisibility(View.VISIBLE);
                                                            tr_item_4.setVisibility(View.VISIBLE);
                                                            line.setVisibility(View.VISIBLE);

                                                            tv_qtyfromdesc1.setText(promo_inner_array.getJSONObject(0).getString("from_qty"));
                                                            tv_qtytodesc1.setText(promo_inner_array.getJSONObject(0).getString("to_qty"));
                                                            tv_discdesc1.setText("Rs. "+promo_inner_array.getJSONObject(0).getString("discount_per_ton"));

                                                            tv_qtyfromdesc2.setText(promo_inner_array.getJSONObject(1).getString("from_qty"));
                                                            tv_qtytodesc2.setText(promo_inner_array.getJSONObject(1).getString("to_qty"));
                                                            tv_discdesc2.setText("Rs. "+promo_inner_array.getJSONObject(1).getString("discount_per_ton"));

                                                            tv_qtyfromdesc3.setText(promo_inner_array.getJSONObject(2).getString("from_qty"));
                                                            tv_qtytodesc3.setText(promo_inner_array.getJSONObject(2).getString("to_qty"));
                                                            tv_discdesc3.setText("Rs. "+promo_inner_array.getJSONObject(2).getString("discount_per_ton"));

                                                            tv_qtyfromdesc4.setText(promo_inner_array.getJSONObject(3).getString("from_qty"));
                                                            tv_qtytodesc4.setText(promo_inner_array.getJSONObject(3).getString("to_qty"));
                                                            tv_discdesc4.setText("Rs. "+promo_inner_array.getJSONObject(3).getString("discount_per_ton"));
                                                        }

                                                    }



                                                }



                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }






                                    }
                                });
                            }
                          else
                            {

                                no_promotion_txt.setVisibility(View.VISIBLE);
                                promotion_spinner.setVisibility(View.GONE);

                                no_promotion_txt.setEnabled(false);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                    /*    noliveoffertxt.setVisibility(View.VISIBLE);
                                        lv.setVisibility(View.GONE);*/

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get promo master params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void GetProductMasterAPI(String url) {


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        materialcatarraylist.clear();
                        materialcatidarraylist.clear();
                        materialsubcatarraylist.clear();

                        System.out.println("RESPONSE OF GET PRODUCT MASTER API IS---" + response);


                        JSONObject GetProductMasterJson = null;
                        try {
                            GetProductMasterJson = new JSONObject(response);


                            String statusstr = GetProductMasterJson.getString("status");


                            if (statusstr.equalsIgnoreCase("true")) {

                               final JSONArray CategoryJsonArray = GetProductMasterJson.getJSONArray("category");
                                System.out.println("CATEGORY JSONARRAY IS---" + CategoryJsonArray);

                                final JSONArray SubCategoryJsonArray = GetProductMasterJson.getJSONArray("sub_category");
                                System.out.println("SUB-CATEGORY JSONARRAY IS---" + SubCategoryJsonArray);

                                final JSONArray QualityJsonArray = GetProductMasterJson.getJSONArray("quality");
                                System.out.println("QUALITY JSONARRAY IS---" + QualityJsonArray);

                                if(SingletonActivity.fromselltodaysoffer == true) {

                                    System.out.println("STF ---11");

                                    materialDesignSpinner1.setText(categorydesc);

                                    materialDesignSpinner1.setEnabled(false);
                                    materialDesignSpinner1.setClickable(false);
                                    materialDesignSpinner1.setBackgroundColor(Color.parseColor("#e4e4e4"));
                                }
                                else {
                                    System.out.println("STF ---111");

                                    materialDesignSpinner1.setEnabled(true);
                                    materialDesignSpinner1.setClickable(true);

                                    for (int i = 0; i < CategoryJsonArray.length(); i++) {
                                        String categoryidfromcatjsonarrstr = CategoryJsonArray.getJSONObject(i).getString("category_id");
                                        String categorynamestr = CategoryJsonArray.getJSONObject(i).getString("category_name");


                                        materialcatarraylist.add(categorynamestr);
                                        materialcatidarraylist.add(categoryidfromcatjsonarrstr);
                                    }

                                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeActivity.this,
                                            android.R.layout.simple_dropdown_item_1line, materialcatarraylist);

                                    materialDesignSpinner1.setAdapter(arrayAdapter);
                                    materialDesignSpinner1.setClickable(true);

                                    materialDesignSpinner1.setTypeface(source_sans_pro_normal);
                                }



                                System.out.println("STF ---1");



                                //  pdia.dismiss();

                                ArrayAdapter<String> SubCatArrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeActivity.this,
                                        android.R.layout.simple_dropdown_item_1line, materialsubcatarraylist);

                                materialDesignSpinner2.setAdapter(SubCatArrayAdapter);

                                materialDesignSpinner2.setClickable(true);

                                    materialDesignSpinner1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                            materialsubcatarraylist.clear();
                                            selectedmatcatspinnerstr = "";

                                            selectedmatcatspinnerstr = parent.getItemAtPosition(position).toString();

                                            try
                                            {

                                                category_id = CategoryJsonArray.getJSONObject(position).getString("category_id");





                                            if(position==0)
                                            {
                                                category_id = CategoryJsonArray.getJSONObject(0).getString("category_id");
                                                selectedsubcatspinnerstr = "";

                                              //  materialDesignSpinner2.setText(materialsubcatarraylist.get(0));


                                            }

                                                if(position==1)
                                                {
                                                    category_id = CategoryJsonArray.getJSONObject(1).getString("category_id");
                                                    selectedsubcatspinnerstr = "";
                                                //    materialDesignSpinner2.setText(materialsubcatarraylist.get(0));
                                                }


                                                if(position==2)
                                                {
                                                    category_id = CategoryJsonArray.getJSONObject(2).getString("category_id");
                                                    selectedsubcatspinnerstr = "";
                                                 //   materialDesignSpinner2.setText(materialsubcatarraylist.get(0));
                                                }

                                                if(position==3)
                                                {
                                                    category_id = CategoryJsonArray.getJSONObject(3).getString("category_id");
                                                    selectedsubcatspinnerstr = "";
                                                 //   materialDesignSpinner2.setText(materialsubcatarraylist.get(0));
                                                }



                                                for (int j = 0; j < SubCategoryJsonArray.length(); j++) {

                                                    String categoryidfromsubcatjsonarrstr = SubCategoryJsonArray.getJSONObject(j).getString("category_id");
                                                    subcatnamefromsubcatjsonarrstr  = SubCategoryJsonArray.getJSONObject(j).getString("sub_category_name");


                                                    if (category_id.equalsIgnoreCase(SubCategoryJsonArray.getJSONObject(j).getString("category_id"))) {
                                                           materialsubcatarraylist.add(subcatnamefromsubcatjsonarrstr);
                                                    }


                                                }

                                                for(int p = 0;  p < materialsubcatarraylist.size();p++)
                                                {
                                                    for(int q = 0; q < SingletonActivity.SubCategoryArrayList.size();q++)
                                                    {
                                                        if (SingletonActivity.SubCategoryArrayList.get(q).equalsIgnoreCase(materialsubcatarraylist.get(p))) {
                                                            materialsubcatarraylist.remove(p);
                                                        }

                                                    }
                                                }

                                                System.out.println("SUB-CATEGORY ARRAYLIST IS---" + materialsubcatarraylist);
                                                System.out.println("SINGLETON SUB-CATEGORY ARRAYLIST IS---" + SingletonActivity.SubCategoryArrayList);

                                                ArrayAdapter<String> SubCatArrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeActivity.this,
                                                        android.R.layout.simple_dropdown_item_1line, materialsubcatarraylist);
                                                materialDesignSpinner2 = (MaterialBetterSpinner)
                                                        findViewById(R.id.sub_category_spinner);
                                                materialDesignSpinner2.setAdapter(SubCatArrayAdapter);

                                                materialDesignSpinner2.setText(materialsubcatarraylist.get(0));


                                                source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");
                                                materialDesignSpinner2.setTypeface(source_sans_pro_normal);

                                                materialDesignSpinner2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                    @Override
                                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                                        selectedsubcatspinnerstr = parent.getItemAtPosition(position).toString();

                                                        for (int k = 0; k < QualityJsonArray.length(); k++) {
                                                            String categorynamefromqualityarray = null;
                                                            try {
                                                                categorynamefromqualityarray = QualityJsonArray.getJSONObject(k).getString("category_name");

                                                                String subcategorynamefromqualityarray = QualityJsonArray.getJSONObject(k).getString("sub_category_name");

                                                                if (materialDesignSpinner1.getText().toString().equalsIgnoreCase(categorynamefromqualityarray) && materialDesignSpinner2.getText().toString().equalsIgnoreCase(subcategorynamefromqualityarray)) {

                                                                    sub_cat_id = QualityJsonArray.getJSONObject(k).getString("sub_category");
                                                                    System.out.println("sub cat id in q arr==="+ QualityJsonArray.getJSONObject(k).getString("sub_category"));

                                                                String q1str = QualityJsonArray.getJSONObject(k).getString("q1");
                                                                System.out.println("Q1 VALUE IS---->> " + q1str);
                                                                String quality1str = q1str;
                                                                String[] separated1 = quality1str.split(":");
                                                                String quality1string = separated1[0];
                                                                quality1.setText(quality1string);
                                                                etqty1.setText(q1str);

                                                                String q2str = QualityJsonArray.getJSONObject(k).getString("q2");
                                                                System.out.println("Q2 VALUE IS---->> " + q2str);
                                                                String quality2str = q2str;
                                                                String[] separated2 = quality2str.split(":");
                                                                String quality2string = separated2[0];
                                                                quality2.setText(quality2string);
                                                                etqty2.setText(q2str);

                                                                String q3str = QualityJsonArray.getJSONObject(k).getString("q3");
                                                                System.out.println("Q3 VALUE IS---->> " + q3str);
                                                                String quality3str = q3str;
                                                                String[] separated3 = quality3str.split(":");
                                                                String quality3string = separated3[0];
                                                                quality3.setText(quality3string);
                                                                etqty3.setText(q3str);

                                                                String q4str = QualityJsonArray.getJSONObject(k).getString("q4");
                                                                System.out.println("Q4 VALUE IS---->> " + q4str);
                                                                String quality4str = q4str;
                                                                String[] separated4 = quality4str.split(":");
                                                                String quality4string = separated4[0];
                                                                quality4.setText(quality4string);
                                                                etqty4.setText(q4str);

                                                                String q5str = QualityJsonArray.getJSONObject(k).getString("q5");
                                                                System.out.println("Q5 VALUE IS---->> " + q5str);
                                                                String quality5str = q5str;
                                                                String[] separated5 = quality5str.split(":");
                                                                String quality5string = separated5[0];
                                                                quality5.setText(quality5string);
                                                                etqty5.setText(q5str);

                                                                String q6str = QualityJsonArray.getJSONObject(k).getString("q6");
                                                                System.out.println("Q6 VALUE IS---->> " + q6str);
                                                                String quality6str = q6str;
                                                                String[] separated6 = quality6str.split(":");
                                                                String quality6string = separated6[0];
                                                                quality6.setText(quality6string);
                                                                etqty6.setText(q6str);

                                                                String q7str = QualityJsonArray.getJSONObject(k).getString("q7");
                                                                System.out.println("Q7 VALUE IS---->> " + q7str);
                                                                String quality7str = q7str;
                                                                String[] separated7 = quality7str.split(":");
                                                                String quality7string = separated7[0];
                                                                quality7.setText(quality7string);
                                                                etqty7.setText(q7str);

                                                                String q8str = QualityJsonArray.getJSONObject(k).getString("q8");
                                                                System.out.println("Q8 VALUE IS---->> " + q8str);
                                                                String quality8str = q8str;
                                                                String[] separated8 = quality8str.split(":");
                                                                String quality8string = separated8[0];
                                                                quality8.setText(quality8string);
                                                                etqty8.setText(q8str);

                                                            }
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }

                                                    }
                                                });

                                                for (int k = 0; k < QualityJsonArray.length(); k++) {
                                                    String categorynamefromqualityarray = null;
                                                    try {
                                                        categorynamefromqualityarray = QualityJsonArray.getJSONObject(k).getString("category_name");

                                                        String subcategorynamefromqualityarray = QualityJsonArray.getJSONObject(k).getString("sub_category_name");




                                                        if (materialDesignSpinner1.getText().toString().equalsIgnoreCase(categorynamefromqualityarray) && materialDesignSpinner2.getText().toString().equalsIgnoreCase(subcategorynamefromqualityarray)) {

                                                            sub_cat_id = QualityJsonArray.getJSONObject(k).getString("sub_category");
                                                            System.out.println("sub cat id in q arr==="+ QualityJsonArray.getJSONObject(k).getString("sub_category"));

                                                            String q1str = QualityJsonArray.getJSONObject(k).getString("q1");
                                                            System.out.println("Q1 VALUE IS---->> " + q1str);
                                                            String quality1str = q1str;
                                                            String[] separated1 = quality1str.split(":");
                                                            String quality1string = separated1[0];
                                                            quality1.setText(quality1string);
                                                            etqty1.setText(q1str);

                                                            String q2str = QualityJsonArray.getJSONObject(k).getString("q2");
                                                            System.out.println("Q2 VALUE IS---->> " + q2str);
                                                            String quality2str = q2str;
                                                            String[] separated2 = quality2str.split(":");
                                                            String quality2string = separated2[0];
                                                            quality2.setText(quality2string);
                                                            etqty2.setText(q2str);

                                                            String q3str = QualityJsonArray.getJSONObject(k).getString("q3");
                                                            System.out.println("Q3 VALUE IS---->> " + q3str);
                                                            String quality3str = q3str;
                                                            String[] separated3 = quality3str.split(":");
                                                            String quality3string = separated3[0];
                                                            quality3.setText(quality3string);
                                                            etqty3.setText(q3str);

                                                            String q4str = QualityJsonArray.getJSONObject(k).getString("q4");
                                                            System.out.println("Q4 VALUE IS---->> " + q4str);
                                                            String quality4str = q4str;
                                                            String[] separated4 = quality4str.split(":");
                                                            String quality4string = separated4[0];
                                                            quality4.setText(quality4string);
                                                            etqty4.setText(q4str);

                                                            String q5str = QualityJsonArray.getJSONObject(k).getString("q5");
                                                            System.out.println("Q5 VALUE IS---->> " + q5str);
                                                            String quality5str = q5str;
                                                            String[] separated5 = quality5str.split(":");
                                                            String quality5string = separated5[0];
                                                            quality5.setText(quality5string);
                                                            etqty5.setText(q5str);

                                                            String q6str = QualityJsonArray.getJSONObject(k).getString("q6");
                                                            System.out.println("Q6 VALUE IS---->> " + q6str);
                                                            String quality6str = q6str;
                                                            String[] separated6 = quality6str.split(":");
                                                            String quality6string = separated6[0];
                                                            quality6.setText(quality6string);
                                                            etqty6.setText(q6str);

                                                            String q7str = QualityJsonArray.getJSONObject(k).getString("q7");
                                                            System.out.println("Q7 VALUE IS---->> " + q7str);
                                                            String quality7str = q7str;
                                                            String[] separated7 = quality7str.split(":");
                                                            String quality7string = separated7[0];
                                                            quality7.setText(quality7string);
                                                            etqty7.setText(q7str);

                                                            String q8str = QualityJsonArray.getJSONObject(k).getString("q8");
                                                            System.out.println("Q8 VALUE IS---->> " + q8str);
                                                            String quality8str = q8str;
                                                            String[] separated8 = quality8str.split(":");
                                                            String quality8string = separated8[0];
                                                            quality8.setText(quality8string);
                                                            etqty8.setText(q8str);

                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }



                                            } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                        }
                                    });


                                }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                    /*    noliveoffertxt.setVisibility(View.VISIBLE);
                                        lv.setVisibility(View.GONE);*/

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get product master params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void InsertOfferAPI(final String url) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        System.out.println("RESPONSE OF ADD NEW OFFER==="+ response);



                        try {



                            JSONObject insertofferjson = new JSONObject(response);
                            System.out.println("INSERT OFFER JSON IS---" + insertofferjson);

                            String statusstr = insertofferjson.getString("status");
                            String msgstr = insertofferjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                   Intent i = new Intent(AddNewOfferLiveTradeActivity.this,HomeActivity.class);
                                   SingletonActivity.fromaddnewoffer = true;
                                 SingletonActivity.isNotificationClicked = false;
                                SingletonActivity.index = index;
                                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                SingletonActivity.FromAddNewOfferTabOne=false;
                                SingletonActivity.FromAddNewOfferTabZero=false;
                                SingletonActivity.isNewEnquiryClicked = false;

                                   startActivity(i);




                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            //  pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }















                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AddNewOfferLiveTradeActivity.this,error.toString(),Toast.LENGTH_SHORT).show();
                        //     pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(AddNewOfferLiveTradeActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_code",userCodestr);
                params.put("mobile_num",mobilenumstr);
                params.put("category",category_id);
                params.put("sub_category", sub_cat_id);
                params.put("q1",etqty1str);
                params.put("q2",etqty2str);
                params.put("q3",etqty3str);
                params.put("q4",etqty4str);
                params.put("q5",etqty5str);
                params.put("q6",etqty6str);
                params.put("q7",etqty7str);
                params.put("q8",etqty8str);
                params.put("advance_rate",advanceratedesctxtstr);
                params.put("next_day_rate",nextdayratedesctxtstr);
                params.put("regular_rate",regularratedesctxtstr);
                params.put("quantity",quantitytondesctxtstr);

                String promocodevalwithoutspace;
                System.out.println("SELECTED PROMO CODE STRING IS---"+ selectedpromocodestr);
                if(selectedpromocodestr!=null) {
                    String[] selectedpromocodestring = selectedpromocodestr.split(":");
                    String promocodeval = selectedpromocodestring[1];


                     promocodevalwithoutspace = promocodeval.replaceAll("\\s+", "");


                    System.out.println("PROMO CODE VALUE without space IN IF---" + promocodevalwithoutspace);
                }
                else
                {
                    promocodevalwithoutspace = "0";
                    System.out.println("PROMO CODE VALUE without space IN ELSE---" + promocodevalwithoutspace);
                }
                params.put("promo_code",promocodevalwithoutspace);
                params.put("sell_tandc",termsandtitledesctxtstr);







                System.out.println("insert offer params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(AddNewOfferLiveTradeActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
        //  return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                System.out.println("OVERFLOWMENU ITEM 1 CLICKED");

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("login",false);


                editor.clear();
                editor.commit();


                sessionManager.logoutUser(mobile_num,fcm_id);


                // sessionManager.logoutUser();

            /*    Intent i = new Intent(LeaveManagementActivity.this,LoginActivity.class);
                startActivity(i);
*/
                return true;
          /*  case R.id.overflowmenuitem2:
                System.out.println("OVERFLOWMENU ITEM 2 CLICKED");

                return true;
            case R.id.overflowmenuitem3:
                System.out.println("OVERFLOWMENU ITEM 3 CLICKED");

                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
