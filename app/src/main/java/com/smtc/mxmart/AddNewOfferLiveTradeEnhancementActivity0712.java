
package com.smtc.mxmart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

public class AddNewOfferLiveTradeEnhancementActivity0712 extends AppCompatActivity {


    String[] SubCategoryArray = {"Lumps DR-CLO", "Fines DR-CLO", "Mix 0-20 DR-CLO"};
    RelativeLayout qualityparamsrel1,qualityparamsrel2;
    MaterialBetterSpinner materialDesignSpinner,materialDesignSpinner1,materialDesignCategoryGradeSpinner,materialDesignSpinner2,extra_insurance_spinner;
    String selectedmatcatspinnerstr,selectedsubcatspinnerstr,selectedpromocodestr;
    Typeface  source_sans_pro_normal;
    ImageView addnewofferbackiv;
    UtilsDialog util=new UtilsDialog();
    ArrayList<String> materialcatarraylist = new ArrayList<String>();
    ArrayList<String> materialcatidarraylist = new ArrayList<String>();
    ArrayList<String> materialsubcatarraylist = new ArrayList<String>();
    ArrayList<String> categorygradearraylist = new ArrayList<String>();
    ArrayList<String> categorygradeidarraylist = new ArrayList<String>();
    ArrayList<String> transportationspinnerlist = new ArrayList<String>();
    ArrayList<String> insurancespinnerlist = new ArrayList<String>();
    ArrayList<String> extrainsurancespinnerlist = new ArrayList<String>();
    ArrayList<String> subcategoryidlist = new ArrayList<String>();
    ArrayList<String> deliveryindayslist = new ArrayList<String>();
    ArrayList<String> materialinspectionlist = new ArrayList<String>();
    ArrayList<String> weighmentlist = new ArrayList<String>();
    ArrayList<String> testcertificatelist = new ArrayList<String>();
    JSONArray CategoryGradeJsonArray, CategoryJsonArray,SubCategoryJsonArray;
    String category_id,promo_code_edit;
    TextView sizerangetxt,quality1,quality2,quality3,quality4,quality5,quality6,quality7,quality8;
    EditText etqty1,etqty2,etqty3,etqty4,etqty5,etqty6,etqty7,etqty8,termsandtitledesctxt,regularratedesctxt,nextdayratedesctxt,advanceratedesctxt,quantitytondesctxt;
    RelativeLayout submitrel;
    String etqty1str,etqty2str,etqty3str,etqty4str,etqty5str,etqty6str,etqty7str,etqty8str,termsandtitledesctxtstr,regularratedesctxtstr,nextdayratedesctxtstr,advanceratedesctxtstr,quantitytondesctxtstr;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    String sub_category_id,userCodestr,mobilenumstr,sub_cat_id,lastPostedCountStr;
    HashMap<String, String> subCatHashMap = new HashMap<String, String>();
    String subcategoryidsub_category_id,subcatnamefromsubcatjsonarrstr,categorygradenamejsonarrstr,category_grade_id;
    TextView subcatidtxt;
    String promocodevalwithoutspace;
    String subcatidfromsubcatjsonarrstr;
    ArrayList<String> promocodelist = new ArrayList<String>();
    ArrayList<String> promocodefinallist = new ArrayList<String>();
    MaterialBetterSpinner test_certificate_spinner,weighment_spinner,promotion_spinner,transportation_spinner,insurance_spinner,deliveryindays_spinner,material_inspection_spinner;
    //Spinner promotion_spinner;
    JSONArray  PromotionsJSONArray;
    String promo_code;
    String promocodevalwithoutspaceonitemclick,deliveryindays_spinner_str,weighment_spinner_str,test_certificate_spinner_str,material_inspection_spinner_str;
    TableLayout promo_table;
    TableRow tr_header,tr_item_1,tr_item_2,tr_item_3,tr_item_4;
    View line;
    TextView tv_qtyfromdesc1,tv_qtyfromdesc2,tv_qtyfromdesc3,tv_qtyfromdesc4,no_promotion_txt,promotion_txt;
    TextView tv_qtytodesc1,tv_qtytodesc2,tv_qtytodesc3,tv_qtytodesc4;
    TextView tv_discdesc1,tv_discdesc2,tv_discdesc3,tv_discdesc4;
    String length_str,sellerid,categoryid,subcategoryid,qtyofferedstr,categorydesc,subcategorydesc,advanceratedesc,nextdayratedesc,regularratedesc,q1val,q2val,q3val,q4val,q5val,q6val,q7val,q8val,promocodeval,tncval;
    RelativeLayout cancelrelative;
    ProgressDialog pdia,pdia1;
    SessionManager sessionManager;
    TextView todays_offer_count,new_enquiry_count,seller_accepted_count,seller_rejected_count,sp_generated_count,buyer_rejected_count;
    String commentStr,tradeStr;
    View tabView,tabView1,tabView2,tabView3;
    TextView livetradetxt,buytxt,selltxt,myprofiletxt;
    ImageView livetradeimgvw,buyimgvw,sellimgvw,myprofileimgvw;
    TextView notification_total_count,hypentxt,basicpricingtitle2txt,extrainsurancetxt,termsandtitletitletxt;
    int index;
    ImageView notification_icon;
    String mobile_num,fcm_id;
    LinearLayout sizerangeedtlinear;
    EditText sizerange1edt,sizerange2edt,loadingedttxt;
    View line6,line7;
    String catstr,transport_spinner_str,insurance_spinner_str,extra_insurance_spinner_str;
    EditText withindaysedt,carbonc1edt,carbonc2edt,manganese1edt,manganese2edt,silicon1edt,silicon2edt,etqty4a,etqty5a,etqty6a,etqty7a,etqty8a,gstedttxt;
    TextView withindaystitletxt,titletxt,carbonctitletxt,quality2a,quality3a,quality4a,quality5a,quality6a,quality7a,quality8a;
    EditText carbonc1bedt,carbonc2bedt,etqty2b,etqty3b,feintotal1edt,feintotal2edt,feinmetallic1edt,feinmetallic2edt,metallization1edt,metallization2edt,etqty7b,etqty8b;
    TextView loadingtxt,quality1b,quality2b,quality3b,quality4b,quality5b,quality6b,quality7b,quality8b,basicpricingtitle1txt;
    String user_code_str,mobile_num_str,sell_date_str,sell_sequence_str,category_grade_str,size_range_start_str,size_range_end_str,is_active_str,remaining_quantity_str,sell_status_str,transport_str,loading_str,gst_str,insurance_select_str,insurance_str,modified_field_str,who_str,when_str,posteddatetime_str,terms_str;
    String category_grade_name_str,q1_title_str,q1_value_str,q2_title_str,q2_value_str,q3_title_str,q3_value_str,q4_title_str,q4_value_str,q5_title_str,q5_value_str,q6_title_str,q6_value_str,q7_title_str,q7_value_str,q8_title_str,q8_value_str;
    ScrollView activity_add_new_offer_live_trade;
    TextView lengthtxt;
    EditText etlength;
    String livesales;
    String delivery_type_str,delivery_in_days_str,material_inspection_str,weighment_type_str,test_certificate_type_str;
    boolean isSpinnerTouched = false;
    JSONArray promo_details_jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnewoffer_dec_enhancements);



        notification_icon = (ImageView)findViewById(R.id.notification_icon);


        withindaystitletxt = (TextView)findViewById(R.id.withindaystitletxt);
        withindaysedt = (EditText) findViewById(R.id.withindaysedt);
        titletxt = (TextView) findViewById(R.id.titletxt);
        loadingtxt = (TextView)findViewById(R.id.loadingtxt);
        basicpricingtitle1txt = (TextView)findViewById(R.id.basicpricingtitle1txt);
        activity_add_new_offer_live_trade = (ScrollView)findViewById(R.id.activity_add_new_offer_live_trade);
        lengthtxt = (TextView)findViewById(R.id.lengthtxt);
        etlength = (EditText)findViewById(R.id.etlength);
       carbonc1edt = (EditText)findViewById(R.id.carbonc1edt);
       carbonc2edt = (EditText)findViewById(R.id.carbonc2edt);
       manganese1edt = (EditText)findViewById(R.id.manganese1edt);
       manganese2edt = (EditText)findViewById(R.id.manganese2edt);
       silicon1edt =  (EditText)findViewById(R.id.silicon1edt);
       silicon2edt =  (EditText)findViewById(R.id.silicon2edt);
       etqty4a =  (EditText)findViewById(R.id.etqty4a);
       etqty5a =  (EditText)findViewById(R.id.etqty5a);
       etqty6a =  (EditText)findViewById(R.id.etqty6a);
       etqty7a =  (EditText)findViewById(R.id.etqty7a);
       etqty8a =  (EditText)findViewById(R.id.etqty8a);
       carbonctitletxt = (TextView)findViewById(R.id.carbonctitletxt);
       quality2a = (TextView)findViewById(R.id.quality2a);
       quality3a = (TextView)findViewById(R.id.quality3a);
       quality4a = (TextView)findViewById(R.id.quality4a);
       quality5a = (TextView)findViewById(R.id.quality5a);
       quality6a = (TextView)findViewById(R.id.quality6a);
       quality7a = (TextView)findViewById(R.id.quality7a);
       quality8a = (TextView)findViewById(R.id.quality8a);
       loadingedttxt = (EditText)findViewById(R.id.loadingedttxt);
       gstedttxt = (EditText)findViewById(R.id.gstedttxt);
       sizerange1edt = (EditText)findViewById(R.id.sizerange1edt);
       sizerange2edt = (EditText)findViewById(R.id.sizerange2edt);
      carbonc1bedt = (EditText)findViewById(R.id.carbonc1bedt);
      carbonc2bedt = (EditText)findViewById(R.id.carbonc2bedt);
       etqty2b = (EditText)findViewById(R.id.etqty2b);
       etqty3b = (EditText)findViewById(R.id.etqty3b);
      feintotal1edt = (EditText)findViewById(R.id.feintotal1edt);
      feintotal2edt = (EditText)findViewById(R.id.feintotal2edt);
      feinmetallic1edt = (EditText)findViewById(R.id.feinmetallic1edt);
      feinmetallic2edt = (EditText)findViewById(R.id.feinmetallic2edt);
      metallization1edt = (EditText)findViewById(R.id.metallization1edt);
      metallization2edt = (EditText)findViewById(R.id.metallization2edt);
      etqty7b =(EditText)findViewById(R.id.etqty7b);
      etqty8b =(EditText)findViewById(R.id.etqty8b);
     quality1b =(TextView)findViewById(R.id.quality1b);
     quality2b =(TextView)findViewById(R.id.quality2b);
     quality3b =(TextView)findViewById(R.id.quality3b);
     quality4b =(TextView)findViewById(R.id.quality4b);
     quality5b =(TextView)findViewById(R.id.quality5b);
     quality6b =(TextView)findViewById(R.id.quality6b);
     quality7b =(TextView)findViewById(R.id.quality7b);
     quality8b =(TextView)findViewById(R.id.quality8b);

        System.out.println("SubCategoryArrayList in Add New Offer Live Trade Activity-----"+ SingletonActivity.SubCategoryArrayList);

        sessionManager = new SessionManager(getApplicationContext());

        notification_total_count = (TextView)findViewById(R.id.notification_total_count);
        sizerangeedtlinear = (LinearLayout)findViewById(R.id.sizerangeedtlinear);
        basicpricingtitle2txt = (TextView)findViewById(R.id.basicpricingtitle2txt);
        extrainsurancetxt = (TextView)findViewById(R.id.extrainsurancetxt);
        promotion_txt = (TextView)findViewById(R.id.promotion_txt);
        termsandtitletitletxt = (TextView)findViewById(R.id.termsandtitletitletxt);
        termsandtitletitletxt.setText("Terms & Conditions");
        basicpricingtitle1txt.setText("BASIC PRICING (IN "+ " \u20B9" + ")");
        loadingtxt.setText("LOADING "+ " \u20B9" + "/MT *");

        line6 = (View)findViewById(R.id.line6);
        line7 = (View)findViewById(R.id.line7);

        materialDesignSpinner1 = (MaterialBetterSpinner)
                findViewById(R.id.material_category_spinner);
        materialDesignSpinner2 = (MaterialBetterSpinner)
                findViewById(R.id.sub_category_spinner);
        materialDesignCategoryGradeSpinner = (MaterialBetterSpinner)
                findViewById(R.id.grade_spinner);
        transportation_spinner = (MaterialBetterSpinner)
                findViewById(R.id.transportation_spinner);
        insurance_spinner = (MaterialBetterSpinner)
                findViewById(R.id.insurance_spinner);
        extra_insurance_spinner = (MaterialBetterSpinner)
                findViewById(R.id.extra_insurance_spinner);
        loadingedttxt = (EditText)findViewById(R.id.loadingedttxt);

        deliveryindays_spinner = (MaterialBetterSpinner)
                findViewById(R.id.deliveryindays_spinner);
        material_inspection_spinner = (MaterialBetterSpinner)findViewById(R.id.material_inspection_spinner);
        weighment_spinner = (MaterialBetterSpinner)findViewById(R.id.weighment_spinner);
        test_certificate_spinner = (MaterialBetterSpinner)findViewById(R.id.test_certificate_spinner);

        promocodelist.clear();
        promocodefinallist.clear();

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        mobile_num = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id",null);
        livesales = prefs.getString("livesales",null);

        lastPostedCountStr = prefs.getString("lastPostedCount", null);




        transportationspinnerlist.clear();
        insurancespinnerlist.clear();
        extrainsurancespinnerlist.clear();
        subcategoryidlist.clear();
        deliveryindayslist.clear();
        materialinspectionlist.clear();
        weighmentlist.clear();
        testcertificatelist.clear();

        notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(AddNewOfferLiveTradeEnhancementActivity0712.this,AbbrevationActivity.class);
                startActivity(i);
            }
        });

        testcertificatelist.add("Yes");
        testcertificatelist.add("No");

        ArrayAdapter<String> TestCertificateSpinnerAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                android.R.layout.simple_dropdown_item_1line, testcertificatelist);

        test_certificate_spinner.setAdapter(TestCertificateSpinnerAdapter);

        test_certificate_spinner.setText(testcertificatelist.get(0));
        test_certificate_spinner_str = "1";

        test_certificate_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position == 0)
                {
                    test_certificate_spinner_str = "1";

                }

                if(position == 1)
                {
                    test_certificate_spinner_str = "2";

                }


            }
        });



        if(NetworkUtility.checkConnectivity(AddNewOfferLiveTradeEnhancementActivity0712.this)){
            String GetProductMasterUrl = APIName.URL+"/home/getProductMaster";
            System.out.println("GET PRODUCT MASTER URL IS---"+ GetProductMasterUrl);
            GetProductMasterAPI(GetProductMasterUrl);

        }
        else{
            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "Please check your internet connection.");
        }



    //    testcertificatelist.add("Required");
   //     testcertificatelist.add("Not Required");


        weighmentlist.add("Buyer's Place");
        weighmentlist.add("Seller's Place");
        weighmentlist.add("Both");

        ArrayAdapter<String> WeighmentSpinnerAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                android.R.layout.simple_dropdown_item_1line, weighmentlist);

        weighment_spinner.setAdapter(WeighmentSpinnerAdapter);

        weighment_spinner.setText(weighmentlist.get(0));
        weighment_spinner_str = "1";

        weighment_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position == 0)
                {
                    weighment_spinner_str = "1";

                }

                if(position == 1)
                {
                    weighment_spinner_str = "2";

                }
                if(position == 2)
                {
                    weighment_spinner_str = "3";
                }


            }
        });

        materialinspectionlist.add("Buyer's Place");
        materialinspectionlist.add("Seller's Place");

        ArrayAdapter<String> MaterialInspectionSpinnerAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                android.R.layout.simple_dropdown_item_1line, materialinspectionlist);

        material_inspection_spinner.setAdapter(MaterialInspectionSpinnerAdapter);

        material_inspection_spinner.setText(materialinspectionlist.get(0));
        material_inspection_spinner_str = "1";

        material_inspection_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position == 0)
                {
                    material_inspection_spinner_str = "1";

                }

                if(position == 1)
                {
                    material_inspection_spinner_str = "2";

                }


            }
        });




        deliveryindayslist.add("With in(Days)");
        deliveryindayslist.add("As per Aachar Sanhita");

        ArrayAdapter<String> DeliveryinDaysSpinnerAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                android.R.layout.simple_dropdown_item_1line, deliveryindayslist);

        deliveryindays_spinner.setAdapter(DeliveryinDaysSpinnerAdapter);

        deliveryindays_spinner.setText(deliveryindayslist.get(0));

        withindaystitletxt.setVisibility(View.VISIBLE);
        withindaysedt.setVisibility(View.VISIBLE);

        deliveryindays_spinner_str = "2";

        deliveryindays_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position == 0)
                {
                    deliveryindays_spinner_str = "2";
                   // withindaystitletxt.setVisibility(View.GONE);
                  //  withindaysedt.setVisibility(View.GONE);

                    withindaystitletxt.setVisibility(View.VISIBLE);
                    withindaysedt.setVisibility(View.VISIBLE);

                }

                if(position == 1)
                {
                    deliveryindays_spinner_str = "1";
                   /* withindaystitletxt.setVisibility(View.VISIBLE);
                    withindaysedt.setVisibility(View.VISIBLE);*/

                     withindaystitletxt.setVisibility(View.GONE);
                     withindaysedt.setVisibility(View.GONE);

                }


            }
        });



        transportationspinnerlist.add("MXMART'S END");
        transportationspinnerlist.add("SELLER'S END");
        transportationspinnerlist.add("BUYER'S END");


        insurancespinnerlist.add("SELECT");
        insurancespinnerlist.add("NA");

       // extrainsurancespinnerlist.add("MXMART'S END");
        extrainsurancespinnerlist.add("SELLER'S END");
        extrainsurancespinnerlist.add("BUYER'S END");

        ArrayAdapter<String> ExtraInsuranceSpinnerAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                android.R.layout.simple_dropdown_item_1line, extrainsurancespinnerlist);

        extra_insurance_spinner.setAdapter(ExtraInsuranceSpinnerAdapter);
      //  extra_insurance_spinner.setText(extrainsurancespinnerlist.get(0));


        extra_insurance_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
          public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            if(position == 0)
            {
             extra_insurance_spinner_str = "1";
            }

             if(position == 1)
            {
             extra_insurance_spinner_str = "2";
            }


          }
        });





        ArrayAdapter<String> InsuranceSpinnerAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                android.R.layout.simple_dropdown_item_1line, insurancespinnerlist);

        insurance_spinner.setAdapter(InsuranceSpinnerAdapter);
        insurance_spinner.setText(insurancespinnerlist.get(0));



        insurance_spinner_str = "select";

          insurance_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
          public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            if(position == 0)
            {
             extra_insurance_spinner.setVisibility(View.VISIBLE);
             extrainsurancetxt.setVisibility(View.VISIBLE);
             insurance_spinner_str = "select";
             extra_insurance_spinner_str = "1";
            }

             if(position == 1)
            {
             extra_insurance_spinner.setVisibility(View.GONE);
             extrainsurancetxt.setVisibility(View.GONE);
              insurance_spinner_str = "NA";

            }


          }
        });



        ArrayAdapter<String> TransportationSpinnerAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                android.R.layout.simple_dropdown_item_1line, transportationspinnerlist);

        transportation_spinner.setAdapter(TransportationSpinnerAdapter);
        transportation_spinner.setText(transportationspinnerlist.get(0));
        transport_spinner_str = "3";




        transportation_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                        if(position == 0)
                                        {
                                            // loadingedttxt.setText("NA");
                                            //  loadingedttxt.setEnabled(false);
                                            transport_spinner_str = "3";
                                        }

                                        if(position == 1)
                                        {
                                          //  loadingedttxt.setText("");
                                            loadingedttxt.setEnabled(true);
                                            transport_spinner_str = "1";
                                        }

                                        if(position == 2)
                                        {
                                           // loadingedttxt.setText("NA");
                                          //  loadingedttxt.setEnabled(false);
                                            transport_spinner_str = "2";
                                        }


                                    }
                                }
        );


        sizerangetxt = (TextView)findViewById(R.id.sizerangetxt);
        qualityparamsrel1 = (RelativeLayout) findViewById(R.id.qualityparamsrel1);
        qualityparamsrel2 = (RelativeLayout) findViewById(R.id.qualityparamsrel2);
        sizerange1edt = (EditText) findViewById(R.id.sizerange1edt);
        hypentxt = (TextView) findViewById(R.id.hypentxt);
        sizerange2edt = (EditText) findViewById(R.id.sizerange2edt);



        materialcatarraylist.add("Categories");
     //   materialcatarraylist.add("");
       //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity.this,
           //     android.R.layout.simple_dropdown_item_1line, materialcatarraylist);

        final int listsize = materialcatarraylist.size() - 1;

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, materialcatarraylist) {
            @Override
            public int getCount() {
                return(listsize); // Truncate the list
            }
        };

        materialDesignSpinner1.setAdapter(arrayAdapter);
        materialDesignSpinner1.setClickable(false);
        materialDesignSpinner1.setEnabled(false);

        materialDesignSpinner1.setSelection(listsize);


        materialDesignSpinner1.setTypeface(source_sans_pro_normal);


        categorygradearraylist.add("Grade");

       /* materialsubcatarraylist.add("");
        categorygradearraylist.add("");*/

        final int listsizes = categorygradearraylist.size() - 1;

        ArrayAdapter<String> arrayAdapters = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, categorygradearraylist) {
            @Override
            public int getCount() {
                return(listsizes); // Truncate the list
            }
        };

       // ArrayAdapter<String> CategoryGradeAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity.this,
         //       android.R.layout.simple_dropdown_item_1line, categorygradearraylist);

        materialDesignCategoryGradeSpinner.setAdapter(arrayAdapters);

       // materialDesignCategoryGradeSpinner.setClickable(false);
       // materialDesignCategoryGradeSpinner.setEnabled(false);


        materialsubcatarraylist.add("Sub Categories");

        final int listsizess = materialsubcatarraylist.size() - 1;

        ArrayAdapter<String> arrayAdapterss = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, materialsubcatarraylist) {
            @Override
            public int getCount() {
                return(listsizess); // Truncate the list
            }
        };

      //  ArrayAdapter<String> SubCatArrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity.this,
            //    android.R.layout.simple_dropdown_item_1line, materialsubcatarraylist);

        materialDesignSpinner2.setAdapter(arrayAdapterss);

      //  materialDesignSpinner2.setClickable(false);
      //  materialDesignSpinner2.setEnabled(false);


        System.out.println("TAB NUMBER IN ADD NEW OFFER--------"+SingletonActivity.validatetab);



      /*  editor.putString("todaysoffer",todaysoffer);
        editor.putString("sellenquiry",sellenquiry);
        editor.putString("selldeals",selldeals);
        editor.putString("buyenquiry",buyenquiry);
        editor.putString("buydeals",buydeals);*/

        System.out.println("USER CODE IN MAIN ACTIVITY---"+ userCodestr);

        cancelrelative = (RelativeLayout)findViewById(R.id.cancelrelative);
        cancelrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_qtyfromdesc1 = (TextView)findViewById(R.id.tv_qtyfromdesc1);
        tv_qtyfromdesc2 = (TextView)findViewById(R.id.tv_qtyfromdesc2);
        tv_qtyfromdesc3 = (TextView)findViewById(R.id.tv_qtyfromdesc3);
        tv_qtyfromdesc4= (TextView)findViewById(R.id.tv_qtyfromdesc4);

        tv_qtytodesc1 = (TextView)findViewById(R.id.tv_qtytodesc1);
        tv_qtytodesc2 = (TextView)findViewById(R.id.tv_qtytodesc2);
        tv_qtytodesc3 = (TextView)findViewById(R.id.tv_qtytodesc3);
        tv_qtytodesc4 = (TextView)findViewById(R.id.tv_qtytodesc4);

        tv_discdesc1 = (TextView)findViewById(R.id.tv_discdesc1);
        tv_discdesc2 = (TextView)findViewById(R.id.tv_discdesc2);
        tv_discdesc3 = (TextView)findViewById(R.id.tv_discdesc3);
        tv_discdesc4 = (TextView)findViewById(R.id.tv_discdesc4);

        no_promotion_txt = (TextView)findViewById(R.id.no_promotion_txt);

        submitrel = (RelativeLayout)findViewById(R.id.sbmtrelative);

        promo_table = (TableLayout)findViewById(R.id.promo_table);
        tr_header = (TableRow)findViewById(R.id.tr_header);
        tr_item_1 = (TableRow)findViewById(R.id.tr_item_1);
        tr_item_2= (TableRow)findViewById(R.id.tr_item_2);
        tr_item_3 = (TableRow)findViewById(R.id.tr_item_3);
        tr_item_4 = (TableRow)findViewById(R.id.tr_item_4);
        line = (View)findViewById(R.id.line);


        quality1 = (TextView)findViewById(R.id.quality1);
        quality2 = (TextView)findViewById(R.id.quality2);
        quality3 = (TextView)findViewById(R.id.quality3);
        quality4 = (TextView)findViewById(R.id.quality4);
        quality5 = (TextView)findViewById(R.id.quality5);
        quality6 = (TextView)findViewById(R.id.quality6);
        quality7 = (TextView)findViewById(R.id.quality7);
        quality8 = (TextView)findViewById(R.id.quality8);

        etqty1 = (EditText)findViewById(R.id.etqty1);
        etqty2 = (EditText)findViewById(R.id.etqty2);
        etqty3 = (EditText)findViewById(R.id.etqty3);
        etqty4 = (EditText)findViewById(R.id.etqty4);
        etqty5 = (EditText)findViewById(R.id.etqty5);
        etqty6 = (EditText)findViewById(R.id.etqty6);
        etqty7 = (EditText)findViewById(R.id.etqty7);
        etqty8 = (EditText)findViewById(R.id.etqty8);

        termsandtitledesctxt = (EditText)findViewById(R.id.termsandtitledesctxt);
        regularratedesctxt = (EditText)findViewById(R.id.regularratedesctxt);
        nextdayratedesctxt = (EditText)findViewById(R.id.nextdayratedesctxt);
        advanceratedesctxt = (EditText)findViewById(R.id.advanceratedesctxt);
        quantitytondesctxt = (EditText)findViewById(R.id.quantitytondesctxt);

       /* etlength.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable edt) {

                String temp = edt.toString ();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 2) {
                    edt.delete(posDot + 3, posDot + 4);
                }
            }
        });
*/

        loadingedttxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable edt) {

                String temp = edt.toString ();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 2) {
                    edt.delete(posDot + 3, posDot + 4);
                }
            }
        });

        nextdayratedesctxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable edt) {

                String temp = edt.toString ();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 2) {
                    edt.delete(posDot + 3, posDot + 4);
                }
            }
        });

        advanceratedesctxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable edt) {

                String temp = edt.toString ();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 2) {
                    edt.delete(posDot + 3, posDot + 4);
                }
            }
        });

        regularratedesctxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable edt) {

                String temp = edt.toString ();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 2) {
                    edt.delete(posDot + 3, posDot + 4);
                }
            }
        });

        quantitytondesctxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable edt) {

                String temp = edt.toString ();
                int posDot = temp.indexOf(".");

                if (posDot <= 0) { return ; } if (temp.length() - posDot - 1 > 3) {
                    edt.delete(posDot + 4, posDot + 5);
                }
            }
        });

      //  basicpricingtitle2txt.setText("#GST, HANDLING & TRANSPORATION ADDITIONAL");

       // promocodefinallist.add("Promo Code");

        promotion_spinner = (MaterialBetterSpinner) findViewById(R.id.promotion_spinner);


        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

        promotion_spinner.setTypeface(source_sans_pro_normal);


        Bundle b = new Bundle();
        b = getIntent().getExtras();

        if(b!=null)
        {

            length_str = b.getString("length");

            sellerid = b.getString("sellerid");
            user_code_str = b.getString("user_code_str");
            mobile_num_str = b.getString("mobile_num_str");
            advanceratedesc =b.getString("advanceratedesc");
            nextdayratedesc =b.getString("nextdayratedesc");
            regularratedesc =b.getString("regularratedesc");
            q1val =b.getString("q1val");
            q2val =b.getString("q2val");
            q3val =b.getString("q3val");
            q4val =b.getString("q4val");
            q5val =b.getString("q5val");
            q6val =b.getString("q6val");
            q7val =b.getString("q7val");
            q8val =b.getString("q8val");
            promocodeval =b.getString("promocodeval");
            tncval =b.getString("tncval");
            categoryid = b.getString("categoryid");
            subcategoryid = b.getString("subcategoryid");
            sell_date_str = b.getString("sell_date_str");
            sell_sequence_str = b.getString("sell_sequence_str");
            category_grade_str = b.getString("category_grade_str");
            size_range_start_str = b.getString("size_range_start_str");
            size_range_end_str = b.getString("size_range_end_str");
            q1_title_str =  b.getString("q1_title_str");
            q1_value_str = b.getString("q1_value_str");
            q2_title_str =  b.getString("q2_title_str");
            q2_value_str = b.getString("q2_value_str");
            q3_title_str =  b.getString("q3_title_str");
            q3_value_str = b.getString("q3_value_str");
            q4_title_str =  b.getString("q4_title_str");
            q4_value_str = b.getString("q4_value_str");
            q5_title_str =  b.getString("q5_title_str");
            q5_value_str = b.getString("q5_value_str");
            q6_title_str =  b.getString("q6_title_str");
            q6_value_str = b.getString("q6_value_str");
            q7_title_str =  b.getString("q7_title_str");
            q7_value_str = b.getString("q7_value_str");
            q8_title_str =  b.getString("q8_title_str");
            q8_value_str = b.getString("q8_value_str");
            qtyofferedstr = b.getString("qtyofferedstr");
            is_active_str =  b.getString("is_active_str");
            remaining_quantity_str =  b.getString("remaining_quantity_str");
            sell_status_str = b.getString("sell_status_str");
            transport_str = b.getString("transport_str");
            loading_str =  b.getString("loading_str");
            gst_str = b.getString("gst_str");
            insurance_select_str = b.getString("insurance_select_str");
            insurance_str =  b.getString("insurance_str");
            modified_field_str =  b.getString("modified_field_str");
            who_str =  b.getString("who_str");
            when_str =  b.getString("when_str");
            posteddatetime_str =  b.getString("posteddatetime_str");
            terms_str =  b.getString("terms_str");
            categorydesc = b.getString("categorydesc");
            subcategorydesc = b.getString("subcategorydesc");
            category_grade_name_str = b.getString("category_grade_name_str");

            delivery_type_str = b.getString("delivery_type");
            delivery_in_days_str = b.getString("delivery_in_days");
            material_inspection_str = b.getString("material_inspection");
            weighment_type_str = b.getString("weighment_type");
            test_certificate_type_str = b.getString("test_certificate_type");


            materialDesignSpinner1.setBackgroundColor(Color.parseColor("#e4e4e4"));
            materialDesignSpinner1.getText().clearSpans();
            materialDesignSpinner1.setText(categorydesc);
          //  materialDesignSpinner1.setEnabled(false);
         //   materialDesignSpinner1.setClickable(false);





            materialDesignCategoryGradeSpinner.setBackgroundColor(Color.parseColor("#e4e4e4"));
            materialDesignCategoryGradeSpinner.getText().clearSpans();
            materialDesignCategoryGradeSpinner.setText(category_grade_name_str);
         //   materialDesignCategoryGradeSpinner.setEnabled(false);
          //  materialDesignCategoryGradeSpinner.setClickable(false);


            materialsubcatarraylist.clear();
            //FOR SUB CATEGORY=====
            for (int j = 0; j < SingletonActivity.sub_categoryJsonArray.length(); j++) {


                try {
                    sub_category_id = SingletonActivity.sub_categoryJsonArray.getJSONObject(j).getString("sub_category_id");

                    String categoryidfromsubcatjsonarrstr = SingletonActivity.sub_categoryJsonArray.getJSONObject(j).getString("category_id");
                    subcatnamefromsubcatjsonarrstr  = SingletonActivity.sub_categoryJsonArray.getJSONObject(j).getString("sub_category_name");

                if (categoryid.equalsIgnoreCase(SingletonActivity.sub_categoryJsonArray.getJSONObject(j).getString("category_id"))) {
                 //   materialsubcatarraylist.remove(0);
                    materialsubcatarraylist.add(subcatnamefromsubcatjsonarrstr);
                    subcategoryidlist.add(SingletonActivity.sub_categoryJsonArray.getJSONObject(j).getString("sub_category_id"));
                }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            for(int p = 0;  p < materialsubcatarraylist.size();p++)
            {
                for(int q = 0; q < SingletonActivity.SubCategoryArrayList.size();q++)
                {
                    if (SingletonActivity.SubCategoryArrayList.get(q).equalsIgnoreCase(materialsubcatarraylist.get(p))) {
                        materialsubcatarraylist.remove(p);
                    }

                }
            }




            System.out.println("SUB-CATEGORY ARRAYLIST IS---" + materialsubcatarraylist);
            System.out.println("SINGLETON SUB-CATEGORY ARRAYLIST IS---" + SingletonActivity.SubCategoryArrayList);



            ArrayAdapter<String> SubCateArrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                    android.R.layout.simple_dropdown_item_1line, materialsubcatarraylist);
            materialDesignSpinner2 = (MaterialBetterSpinner)
                    findViewById(R.id.sub_category_spinner);
            materialDesignSpinner2.setAdapter(SubCateArrayAdapter);
            if(categoryid.equalsIgnoreCase("1")||categoryid.equalsIgnoreCase("2")) {
                materialDesignSpinner2.setText(materialsubcatarraylist.get(0));
               // subcategoryid = subcategoryidlist.get(0);
            }


          //FROM SELL TODAYS OFFER-EDIT
            materialDesignSpinner2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    subcategoryid = subcategoryidlist.get(i);

                  //  Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this,"SELECTED SUB CATGEGORY ID B IS==="+ subcategoryid,Toast.LENGTH_SHORT).show();
                }
            });


          //  subcategoryid = subcategoryidlist.get(0);


            quantitytondesctxt.setText(qtyofferedstr);
            advanceratedesctxt.setText(advanceratedesc);
            nextdayratedesctxt.setText(nextdayratedesc);
            regularratedesctxt.setText(regularratedesc);

            materialDesignSpinner2.setText(subcategorydesc);


            if(delivery_type_str.equalsIgnoreCase("1"))
            {

                deliveryindays_spinner.setText("As per Aachar Sanhita");
                withindaystitletxt.setVisibility(View.GONE);
                withindaysedt.setVisibility(View.GONE);
                withindaysedt.setText("");
                deliveryindays_spinner_str = "1";
            }
            if(delivery_type_str.equalsIgnoreCase("2"))
            {
                deliveryindays_spinner.setText("With in(Days)");
                withindaystitletxt.setVisibility(View.VISIBLE);
                withindaysedt.setVisibility(View.VISIBLE);
                withindaysedt.setText(delivery_in_days_str);

                deliveryindays_spinner_str = "2";
            }

            if(material_inspection_str.equalsIgnoreCase("1"))
            {
                material_inspection_spinner_str = "1";
                material_inspection_spinner.setText("Buyer's Place");
            }
            if(material_inspection_str.equalsIgnoreCase("2"))
            {
                material_inspection_spinner_str = "2";
                material_inspection_spinner.setText("Seller's Place");
            }

            if(weighment_type_str.equalsIgnoreCase("1"))
            {
                weighment_spinner_str = "1";
                weighment_spinner.setText("Buyer's Place");
            }
            if(weighment_type_str.equalsIgnoreCase("2"))
            {
                weighment_spinner_str = "2";
                weighment_spinner.setText("Seller's Place");
            }
            if(weighment_type_str.equalsIgnoreCase("3"))
            {
                weighment_spinner_str = "3";
                weighment_spinner.setText("Both");
            }


            if(test_certificate_type_str.equalsIgnoreCase("1"))
            {
                test_certificate_spinner_str = "1";
                test_certificate_spinner.setText("Yes");
               // test_certificate_spinner.setText("Required");
            }
            if(test_certificate_type_str.equalsIgnoreCase("2"))
            {
                test_certificate_spinner_str = "2";
                test_certificate_spinner.setText("No");
              //  test_certificate_spinner.setText("Not Required");
            }


            if(transport_str.equalsIgnoreCase("1"))
            {
                transportation_spinner.setText("SELLER'S END");
                transport_spinner_str = "1";
            }
            if(transport_str.equalsIgnoreCase("2"))
            {
                transportation_spinner.setText("BUYER'S END");
                transport_spinner_str = "2";
            }
            if(transport_str.equalsIgnoreCase("3"))
            {
                transportation_spinner.setText("MXMART'S END");
                transport_spinner_str = "3";
            }


            if(loading_str.equalsIgnoreCase("NA"))
            {
                loadingedttxt.setText(loading_str);
            }
            else {
                loadingedttxt.setText(loading_str);
            }
            gstedttxt.setText(gst_str);

            if(insurance_select_str.equalsIgnoreCase("NA"))
            {
                insurance_spinner.setText("NA");
                insurance_spinner_str = "NA";
                extra_insurance_spinner.setVisibility(View.GONE);
                extrainsurancetxt.setVisibility(View.GONE);
            }
            else
            {
                extrainsurancetxt.setVisibility(View.VISIBLE);
                extra_insurance_spinner.setVisibility(View.VISIBLE);

                if (insurance_str.equalsIgnoreCase("1")) {
                    extra_insurance_spinner.setText("SELLER'S END");
                    extra_insurance_spinner_str = "1";
                }  if (insurance_str.equalsIgnoreCase("2")) {
                    extra_insurance_spinner.setText("BUYER'S END");
                    extra_insurance_spinner_str = "2";
                }

            }

            termsandtitledesctxt.setText(tncval);
            promo_code_edit = promocodeval;
            if(!promocodeval.equalsIgnoreCase(" "))
            {
                promotion_spinner.setText("Promo Code :"+promocodeval);

                System.out.println("promotion_spinner IN EDIT if--"+ promocodeval);


            }
            else
            {
                System.out.println("promotion_spinner IN EDIT else--"+ promocodeval);
                promotion_spinner.setText("Select Promo");

            }


            if(!promocodeval.equalsIgnoreCase(" ")) {
                promo_details_jsonArray = SingletonActivity.PromoJSONArray;
                System.out.println("promo_details_jsonArray IS---" + promo_details_jsonArray.length());


                if (promo_details_jsonArray.length() > 0) {

                    promo_table.setVisibility(View.VISIBLE);

                    promotion_txt.setVisibility(View.GONE);
                    line6.setVisibility(View.GONE);
                    line7.setVisibility(View.VISIBLE);

                    {
                        System.out.println("PROMO INNER JSON ARRAY IN ON ITEM CLICK IF EQUAL IS---" + promo_details_jsonArray);

                        if (promo_details_jsonArray.length() == 0) {
                            tr_header.setVisibility(View.GONE);
                            tr_item_1.setVisibility(View.GONE);
                            tr_item_2.setVisibility(View.GONE);
                            tr_item_3.setVisibility(View.GONE);
                            tr_item_4.setVisibility(View.GONE);
                            line.setVisibility(View.GONE);
                        }

                        if (promo_details_jsonArray.length() == 1) {
                            tr_header.setVisibility(View.VISIBLE);
                            tr_item_1.setVisibility(View.VISIBLE);
                            tr_item_2.setVisibility(View.GONE);
                            tr_item_3.setVisibility(View.GONE);
                            tr_item_4.setVisibility(View.GONE);
                            line.setVisibility(View.VISIBLE);

                            try {
                                tv_qtyfromdesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("from_qty"));
                                tv_qtytodesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("to_qty"));
                                tv_discdesc1.setText("Rs. " + promo_details_jsonArray.getJSONObject(0).getString("discount_per_ton"));



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        if (promo_details_jsonArray.length() == 2) {
                            tr_header.setVisibility(View.VISIBLE);
                            tr_item_1.setVisibility(View.VISIBLE);
                            tr_item_2.setVisibility(View.VISIBLE);
                            tr_item_3.setVisibility(View.GONE);
                            tr_item_4.setVisibility(View.GONE);
                            line.setVisibility(View.VISIBLE);

                            try {
                                tv_qtyfromdesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("from_qty"));
                                tv_qtytodesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("to_qty"));
                                tv_discdesc1.setText("Rs. " + promo_details_jsonArray.getJSONObject(0).getString("discount_per_ton"));

                                tv_qtyfromdesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("from_qty"));
                                tv_qtytodesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("to_qty"));
                                tv_discdesc2.setText("Rs. " + promo_details_jsonArray.getJSONObject(1).getString("discount_per_ton"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        if (promo_details_jsonArray.length() == 3) {
                            tr_header.setVisibility(View.VISIBLE);
                            tr_item_1.setVisibility(View.VISIBLE);
                            tr_item_2.setVisibility(View.VISIBLE);
                            tr_item_3.setVisibility(View.VISIBLE);
                            tr_item_4.setVisibility(View.GONE);
                            line.setVisibility(View.VISIBLE);

                            try {
                                tv_qtyfromdesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("from_qty"));
                                tv_qtytodesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("to_qty"));
                                tv_discdesc1.setText("Rs. " + promo_details_jsonArray.getJSONObject(0).getString("discount_per_ton"));

                                tv_qtyfromdesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("from_qty"));
                                tv_qtytodesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("to_qty"));
                                tv_discdesc2.setText("Rs. " + promo_details_jsonArray.getJSONObject(1).getString("discount_per_ton"));

                                tv_qtyfromdesc3.setText(promo_details_jsonArray.getJSONObject(2).getString("from_qty"));
                                tv_qtytodesc3.setText(promo_details_jsonArray.getJSONObject(2).getString("to_qty"));
                                tv_discdesc3.setText("Rs. " + promo_details_jsonArray.getJSONObject(2).getString("discount_per_ton"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        if (promo_details_jsonArray.length() == 4) {
                            tr_header.setVisibility(View.VISIBLE);
                            tr_item_1.setVisibility(View.VISIBLE);
                            tr_item_2.setVisibility(View.VISIBLE);
                            tr_item_3.setVisibility(View.VISIBLE);
                            tr_item_4.setVisibility(View.VISIBLE);
                            line.setVisibility(View.VISIBLE);

                            try {
                                tv_qtyfromdesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("from_qty"));
                                tv_qtytodesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("to_qty"));
                                tv_discdesc1.setText("Rs. " + promo_details_jsonArray.getJSONObject(0).getString("discount_per_ton"));

                                tv_qtyfromdesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("from_qty"));
                                tv_qtytodesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("to_qty"));
                                tv_discdesc2.setText("Rs. " + promo_details_jsonArray.getJSONObject(1).getString("discount_per_ton"));

                                tv_qtyfromdesc3.setText(promo_details_jsonArray.getJSONObject(2).getString("from_qty"));
                                tv_qtytodesc3.setText(promo_details_jsonArray.getJSONObject(2).getString("to_qty"));
                                tv_discdesc3.setText("Rs. " + promo_details_jsonArray.getJSONObject(2).getString("discount_per_ton"));

                                tv_qtyfromdesc4.setText(promo_details_jsonArray.getJSONObject(3).getString("from_qty"));
                                tv_qtytodesc4.setText(promo_details_jsonArray.getJSONObject(3).getString("to_qty"));
                                tv_discdesc4.setText("Rs. " + promo_details_jsonArray.getJSONObject(3).getString("discount_per_ton"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }

                }
            }



            if(SingletonActivity.isUpdateClicked == true) {
                titletxt.setText("Seller- Edit Offer");

                if(categoryid.equalsIgnoreCase("1"))
                {
                    lengthtxt.setText("LENGTH(M) *");
                    etlength.setText("6");

                }

                if(categoryid.equalsIgnoreCase("2"))
                {
                    lengthtxt.setText("WEIGHT(kg) *");
                    etlength.setText("110");

                }
            }
            else
            {
                titletxt.setText("Add New Offer");
            }


                if(SingletonActivity.fromselltodaysoffer == true) {


                if (categoryid.equalsIgnoreCase("1")||categoryid.equalsIgnoreCase("2")) {


                    sizerangeedtlinear.setVisibility(View.INVISIBLE);
                    materialDesignSpinner2.setVisibility(View.VISIBLE);
                    lengthtxt.setVisibility(View.VISIBLE);
                    etlength.setVisibility(View.VISIBLE);
                    sizerangetxt.setVisibility(View.GONE);
                    qualityparamsrel1.setVisibility(View.VISIBLE);
                    qualityparamsrel2.setVisibility(View.INVISIBLE);

                    String[] q1_value_str_parts = q1_value_str.split("-");
                    carbonc1edt.setText(q1_value_str_parts[0]); // 004
                    carbonc2edt.setText(q1_value_str_parts[1]); // 034556


                    String[] q2_value_str_parts = q2_value_str.split("-");
                    manganese1edt.setText(q2_value_str_parts[0]);
                    manganese2edt.setText(q2_value_str_parts[1]);


                    String[] q3_value_str_parts = q3_value_str.split("-");
                    silicon1edt.setText(q3_value_str_parts[0]);
                    silicon2edt.setText(q3_value_str_parts[1]);

                    etqty4a.setText(q4_value_str);
                    etqty5a.setText(q5_value_str);
                    etqty6a.setText(q6_value_str);
                    etqty7a.setText(q7_value_str);
                    etqty8a.setText(q8_value_str);

                    etlength.setText(length_str);


                }

                else
                {

                    sizerangeedtlinear.setVisibility(View.VISIBLE);
                    sizerange1edt.setVisibility(View.VISIBLE);
                    hypentxt.setVisibility(View.VISIBLE);
                    sizerange2edt.setVisibility(View.VISIBLE);
                    materialDesignSpinner2.setVisibility(View.INVISIBLE);
                    sizerangetxt.setVisibility(View.VISIBLE);
                    qualityparamsrel1.setVisibility(View.INVISIBLE);
                    qualityparamsrel2.setVisibility(View.VISIBLE);

                    sizerange1edt.setText(size_range_start_str);
                    sizerange2edt.setText(size_range_end_str);

                    String[] q1_value_str_parts = q1_value_str.split("-");
                    carbonc1bedt.setText(q1_value_str_parts[0]); // 004
                    carbonc2bedt.setText(q1_value_str_parts[1]); // 034556

                    etqty2b.setText(q2_value_str);
                    etqty3b.setText(q3_value_str);


                    String[] q4_value_str_parts = q4_value_str.split("-");
                    feintotal1edt.setText(q4_value_str_parts[0]); // 004
                    feintotal2edt.setText(q4_value_str_parts[1]); // 034556


                    String[] q5_value_str_parts = q5_value_str.split("-");
                    feinmetallic1edt.setText(q5_value_str_parts[0]); // 004
                    feinmetallic2edt.setText(q5_value_str_parts[1]); // 034556



                    String[] q6_value_str_parts = q6_value_str.split("-");
                    metallization1edt.setText(q6_value_str_parts[0]); // 004
                    metallization2edt.setText(q6_value_str_parts[1]); // 034556

                    etqty7b.setText(q7_value_str);
                    etqty8b.setText(q8_value_str);
                }
            }

        }


        if(NetworkUtility.checkConnectivity(AddNewOfferLiveTradeEnhancementActivity0712.this)){
            String GetPromoUrl = APIName.URL+"/seller/getPromo?user_code="+userCodestr;
            System.out.println("GET PROMO URL IS---"+ GetPromoUrl);
            GetPromoAPI(GetPromoUrl);

        }
        else{
            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "Please check your internet connection.");
        }

      //  util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "SingletonActivity.isValidTradeTimeOpen"+SingletonActivity.isValidTradeTimeOpen);





        submitrel.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {


                                             if (livesales.equalsIgnoreCase("0")) {

                                                 if (SingletonActivity.isUpdateClicked == true) {


                                                     advanceratedesctxtstr = advanceratedesctxt.getText().toString();
                                                     nextdayratedesctxtstr = nextdayratedesctxt.getText().toString();
                                                     regularratedesctxtstr = regularratedesctxt.getText().toString();
                                                     quantitytondesctxtstr = quantitytondesctxt.getText().toString();
                                                     termsandtitledesctxtstr = termsandtitledesctxt.getText().toString();


                                                     boolean invalid = false;

                                                     if (materialDesignSpinner1.getText().toString().equals("")) {
                                                         invalid = true;
                                                         Toast.makeText(getApplicationContext(), "MATERIAL SPECIFICATIONS* can't be empty",
                                                                 Toast.LENGTH_SHORT).show();
                                                     } else if (categoryid.equalsIgnoreCase("1") || categoryid.equalsIgnoreCase("2")) {

                                                         if (!isValidLength(etlength.getText().toString())) {
                                                             invalid = true;
                                                             // Toast.makeText(getApplicationContext(), "LENGTH* can't be empty",
                                                             //      Toast.LENGTH_SHORT).show();
                                                         } else if (carbonc1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "CARBON(C)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (carbonc2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "CARBON(C)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (manganese1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "MANGANESE(Mn)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (manganese2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "MANGANESE(Mn)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (silicon1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SILICON(Si)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (silicon2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SILICON(Si)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty4a.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SULPHUR(S)(Max)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty5a.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "PHOSPHORUS(P)(Max)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty6a.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SULPHUR + PHOSPHORUS(Max)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (!isValidQuantity(quantitytondesctxt.getText().toString())) {
                                                             invalid = true;

                                                         } else if (loadingedttxt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "LOADING* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (extra_insurance_spinner.getText().toString().equalsIgnoreCase("") && (insurance_spinner_str.equalsIgnoreCase("select"))) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Please select insurance from extra insurance dropdown", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }
                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (withindaysedt.getText().toString().equalsIgnoreCase(""))) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "With in(Days) can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }
                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (Integer.parseInt(withindaysedt.getText().toString())>60))
                                                         {
                                                         invalid = true;
                                                         Toast.makeText(getApplicationContext(),
                                                                 "With in(Days) can't be greater than 60 days", Toast.LENGTH_SHORT)
                                                                 .show();
                                                     }

                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (Integer.parseInt(withindaysedt.getText().toString())== 0))
                                                         {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "With in(Days) can't be less than 0 days", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }

                                                         else if (invalid == false) {

                                                             float advrate = 0.0f;

                                                             System.out.println("ADVANCE RATE IS===" + advanceratedesctxt.getText().toString());

                                                             if (!advanceratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 advrate = Float.parseFloat(advanceratedesctxt.getText().toString());
                                                             }

                                                             float nextdayrate = 0.0f;

                                                             if (!nextdayratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 nextdayrate = Float.parseFloat(nextdayratedesctxt.getText().toString());
                                                             }

                                                             float regularrate = 0.0f;

                                                             if (!regularratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 regularrate = Float.parseFloat(regularratedesctxt.getText().toString());
                                                             }

                                                             float total = advrate + nextdayrate + regularrate;

                                                            if (total == 0) {

                                                                 Toast.makeText(getApplicationContext(),
                                                                         "Please enter a value among 3 pricing fields", Toast.LENGTH_SHORT)
                                                                         .show();

                                                             } else {




                                                                 if (NetworkUtility.checkConnectivity(AddNewOfferLiveTradeEnhancementActivity0712.this)) {
                                                                     submitrel.setEnabled(false);
                                                                     String UpdateOfferUrl = APIName.URL + "/seller/updateOffer?sell_id=" + sellerid;
                                                                     System.out.println("UPDATE OFFER URL IS---" + UpdateOfferUrl);
                                                                     UpdateOfferAPI(UpdateOfferUrl);

                                                                 } else {
                                                                     util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "Please check your internet connection.");
                                                                 }




                                                             }
                                                         }


                                                     } else {
                                                         if (sizerange1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SIZE RANGE* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (sizerange2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SIZE RANGE* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (carbonc1bedt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "CARBON(C)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (carbonc2bedt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "CARBON(C)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty2b.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SULPHUR(S)(Max)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty3b.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "PHOSPHORUS(P)(Max)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (feintotal1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Fe In Total (Fe(T))* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (feintotal2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Fe In Total (Fe(T))* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (feinmetallic1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Fe In Metallic (Fe(M))* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (feinmetallic2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Fe In Metallic (Fe(M))* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (metallization1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "METALLIZATION (MTZ)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (metallization2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "METALLIZATION (MTZ)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty7b.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "NON MAGNETIC MATERIAL (Non Mag.)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty8b.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "TOTAL SILICA AND ALUMINA (SiO2 + Al2O3)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (!isValidQuantity(quantitytondesctxt.getText().toString())) {
                                                             invalid = true;

                                                         } else if (loadingedttxt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "LOADING* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (extra_insurance_spinner.getText().toString().equalsIgnoreCase("") && (insurance_spinner_str.equalsIgnoreCase("select"))) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Please select insurance from extra insurance dropdown", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }
                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (withindaysedt.getText().toString().equalsIgnoreCase(""))) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "With in(Days) can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }

                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (Integer.parseInt(withindaysedt.getText().toString())>60))
                                                         {
                                                             invalid = true;
                                                         Toast.makeText(getApplicationContext(),
                                                                 "With in(Days) can't be greater than 60 days", Toast.LENGTH_SHORT)
                                                                 .show();
                                                     }

                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (Integer.parseInt(withindaysedt.getText().toString())== 0))
                                                         {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "With in(Days) can't be less than 0 days", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }



                                                         else if (invalid == false) {

                                                             float advrate = 0.0f;

                                                             System.out.println("ADVANCE RATE IS===" + advanceratedesctxt.getText().toString());

                                                             if (!advanceratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 advrate = Float.parseFloat(advanceratedesctxt.getText().toString());
                                                             }

                                                             float nextdayrate = 0.0f;

                                                             if (!nextdayratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 nextdayrate = Float.parseFloat(nextdayratedesctxt.getText().toString());
                                                             }

                                                             float regularrate = 0.0f;

                                                             if (!regularratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 regularrate = Float.parseFloat(regularratedesctxt.getText().toString());
                                                             }

                                                             float total = advrate + nextdayrate + regularrate;

                                                             if (total == 0) {

                                                                 Toast.makeText(getApplicationContext(),
                                                                         "Please enter a value among 3 pricing fields", Toast.LENGTH_SHORT)
                                                                         .show();

                                                             } else {




                                                                 if (NetworkUtility.checkConnectivity(AddNewOfferLiveTradeEnhancementActivity0712.this)) {
                                                                     submitrel.setEnabled(false);

                                                                     String UpdateOfferUrl = APIName.URL + "/seller/updateOffer?sell_id=" + sellerid;
                                                                     System.out.println("UPDATE OFFER URL IS---" + UpdateOfferUrl);
                                                                     UpdateOfferAPI(UpdateOfferUrl);

                                                                 } else {
                                                                     util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "Please check your internet connection.");
                                                                 }




                                                            }
                                                         }


                                                     }
                                                 } else {
                                                     advanceratedesctxtstr = advanceratedesctxt.getText().toString();
                                                     nextdayratedesctxtstr = nextdayratedesctxt.getText().toString();
                                                     regularratedesctxtstr = regularratedesctxt.getText().toString();
                                                     quantitytondesctxtstr = quantitytondesctxt.getText().toString();
                                                     termsandtitledesctxtstr = termsandtitledesctxt.getText().toString();



                                                     System.out.println("SELECTED PROMO CODE STRING IS---" + selectedpromocodestr);
                                                     if (selectedpromocodestr != null) {

                                                         String[] selectedpromocodestring = selectedpromocodestr.split(":");
                                                         String promocodeval = selectedpromocodestring[1];
                                                         promocodevalwithoutspace = promocodeval.replaceAll("\\s+", "");

                                                         System.out.println("PROMO CODE VALUE without space IN IF---" + promocodevalwithoutspace);
                                                     } else {
                                                         promocodevalwithoutspace = " ";
                                                         System.out.println("PROMO CODE VALUE without space IN ELSE---" + promocodevalwithoutspace);
                                                     }

                                                     boolean invalid = false;

                                                     if (materialDesignSpinner1.getText().toString().equals("")) {
                                                         invalid = true;
                                                         Toast.makeText(getApplicationContext(), "MATERIAL SPECIFICATIONS* can't be empty",
                                                                 Toast.LENGTH_SHORT).show();
                                                     } else if (catstr.equalsIgnoreCase("billetsingots")) {

                                                         if (!isValidLength(etlength.getText().toString())) {
                                                             invalid = true;
                                                             // Toast.makeText(getApplicationContext(), "LENGTH* can't be empty",
                                                             //      Toast.LENGTH_SHORT).show();
                                                         }
                                                         if (carbonc1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "CARBON(C)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (carbonc2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "CARBON(C)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (manganese1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "MANGANESE(Mn)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (manganese2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "MANGANESE(Mn)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (silicon1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SILICON(Si)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (silicon2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SILICON(Si)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty4a.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SULPHUR(S)(Max)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty5a.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "PHOSPHORUS(P)(Max)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty6a.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SULPHUR + PHOSPHORUS(Max)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (!isValidQuantity(quantitytondesctxt.getText().toString())) {
                                                             invalid = true;

                                                         } else if (loadingedttxt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "LOADING* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (extra_insurance_spinner.getText().toString().equalsIgnoreCase("") && (insurance_spinner_str.equalsIgnoreCase("select"))) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Please select insurance from extra insurance dropdown", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }
                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (withindaysedt.getText().toString().equalsIgnoreCase(""))) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "With in(Days) can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }

                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (Integer.parseInt(withindaysedt.getText().toString())>60))
                                                         {
                                                             invalid = true;
                                                         Toast.makeText(getApplicationContext(),
                                                                 "With in(Days) can't be greater than 60 days", Toast.LENGTH_SHORT)
                                                                 .show();
                                                     }

                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (Integer.parseInt(withindaysedt.getText().toString())== 0))
                                                         {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "With in(Days) can't be less than 0 days", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }


                                                     else if (invalid == false) {

                                                             float advrate = 0.0f;

                                                             System.out.println("ADVANCE RATE IS===" + advanceratedesctxt.getText().toString());

                                                             if (!advanceratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 advrate = Float.parseFloat(advanceratedesctxt.getText().toString());
                                                             }

                                                             float nextdayrate = 0.0f;

                                                             if (!nextdayratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 nextdayrate = Float.parseFloat(nextdayratedesctxt.getText().toString());
                                                             }

                                                             float regularrate = 0.0f;

                                                             if (!regularratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 regularrate = Float.parseFloat(regularratedesctxt.getText().toString());
                                                             }

                                                             float total = advrate + nextdayrate + regularrate;

                                                            if (total == 0) {

                                                                 Toast.makeText(getApplicationContext(),
                                                                         "Please enter a value among 3 pricing fields", Toast.LENGTH_SHORT)
                                                                         .show();

                                                             } else {



                                                                 if (NetworkUtility.checkConnectivity(AddNewOfferLiveTradeEnhancementActivity0712.this)) {
                                                                     submitrel.setEnabled(false);
                                                                     String InsertOfferUrl = APIName.URL + "/seller/insertOffer";
                                                                     System.out.println("ADD OFFER URL IS---" + InsertOfferUrl);
                                                                     InsertOfferAPI(InsertOfferUrl);

                                                                 } else {
                                                                     util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "Please check your internet connection.");
                                                                 }


                                                            }
                                                         }


                                                     } else {
                                                         if (sizerange1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SIZE RANGE* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (sizerange2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SIZE RANGE* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (carbonc1bedt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "CARBON(C)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (carbonc2bedt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "CARBON(C)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty2b.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "SULPHUR(S)(Max)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty3b.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "PHOSPHORUS(P)(Max)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (feintotal1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Fe In Total (Fe(T))* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (feintotal2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Fe In Total (Fe(T))* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (feinmetallic1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Fe In Metallic (Fe(M))* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (feinmetallic2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Fe In Metallic (Fe(M))* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (metallization1edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "METALLIZATION (MTZ)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (metallization2edt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "METALLIZATION (MTZ)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty7b.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "NON MAGNETIC MATERIAL (Non Mag.)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (etqty8b.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "TOTAL SILICA AND ALUMINA (SiO2 + Al2O3)* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (!isValidQuantity(quantitytondesctxt.getText().toString())) {
                                                             invalid = true;

                                                         } else if (loadingedttxt.getText().toString().equals("")) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "LOADING* can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         } else if (extra_insurance_spinner.getText().toString().equalsIgnoreCase("") && (insurance_spinner_str.equalsIgnoreCase("select"))) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "Please select insurance from extra insurance dropdown", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }
                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (withindaysedt.getText().toString().equalsIgnoreCase(""))) {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "With in(Days) can't be empty", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }

                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (Integer.parseInt(withindaysedt.getText().toString())>60))
                                                         {
                                                             invalid = true;
                                                         Toast.makeText(getApplicationContext(),
                                                                 "With in(Days) can't be greater than 60 days", Toast.LENGTH_SHORT)
                                                                 .show();
                                                     }

                                                         else if (deliveryindays_spinner.getText().toString().equalsIgnoreCase("With in(Days)") && (Integer.parseInt(withindaysedt.getText().toString())== 0))
                                                         {
                                                             invalid = true;
                                                             Toast.makeText(getApplicationContext(),
                                                                     "With in(Days) can't be less than 0 days", Toast.LENGTH_SHORT)
                                                                     .show();
                                                         }


                                                         else if (invalid == false) {

                                                             float advrate = 0.0f;

                                                             System.out.println("ADVANCE RATE IS===" + advanceratedesctxt.getText().toString());

                                                             if (!advanceratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 advrate = Float.parseFloat(advanceratedesctxt.getText().toString());
                                                             }

                                                             float nextdayrate = 0.0f;

                                                             if (!nextdayratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 nextdayrate = Float.parseFloat(nextdayratedesctxt.getText().toString());
                                                             }

                                                             float regularrate = 0.0f;

                                                             if (!regularratedesctxt.getText().toString().equalsIgnoreCase("")) {
                                                                 regularrate = Float.parseFloat(regularratedesctxt.getText().toString());
                                                             }

                                                             float total = advrate + nextdayrate + regularrate;

                                                             if (total == 0) {

                                                                 Toast.makeText(getApplicationContext(),
                                                                         "Please enter a value among 3 pricing fields", Toast.LENGTH_SHORT)
                                                                         .show();

                                                             } else {


                                                                 if (NetworkUtility.checkConnectivity(AddNewOfferLiveTradeEnhancementActivity0712.this)) {
                                                                     submitrel.setEnabled(false);
                                                                     String InsertOfferUrl = APIName.URL + "/seller/insertOffer";
                                                                     System.out.println("ADD OFFER URL IS---" + InsertOfferUrl);
                                                                     InsertOfferAPI(InsertOfferUrl);

                                                                 } else {
                                                                     util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "Please check your internet connection.");
                                                                 }


                                                             }
                                                         }


                                                     }

                                                 }

                                             }
                                             else
                                             {
                                                 Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                                             }
                                         }


                                         });






        addnewofferbackiv = (ImageView)findViewById(R.id.backicon);
        addnewofferbackiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (SingletonActivity.isUpdateClicked == true) {
                    //finish();

                    Intent i = new Intent(AddNewOfferLiveTradeEnhancementActivity0712.this,HomeActivity.class);

                    SingletonActivity.fromaddnewoffer = false;
                    SingletonActivity.fromselltodaysoffer = true;

                    startActivity(i);

                }
                else {
                    Intent i = new Intent(AddNewOfferLiveTradeEnhancementActivity0712.this,HomeActivity.class);

                    SingletonActivity.fromselltodaysoffer = false;
                    SingletonActivity.fromaddnewoffer = true;
                    startActivity(i);
                }

            }
        });




        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (SingletonActivity.isUpdateClicked == true) {
          //  finish();

            Intent i = new Intent(AddNewOfferLiveTradeEnhancementActivity0712.this,HomeActivity.class);

            SingletonActivity.fromaddnewoffer = false;
            SingletonActivity.fromselltodaysoffer = true;

            startActivity(i);

        }
        else {
            Intent i = new Intent(AddNewOfferLiveTradeEnhancementActivity0712.this,HomeActivity.class);

            SingletonActivity.fromselltodaysoffer = false;
            SingletonActivity.fromaddnewoffer = true;
            startActivity(i);
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        submitrel.setEnabled(true);


    }

     private void GetProductMasterAutoFillAPI(String url) {

         HurlStack hurlStack = new HurlStack() {
             @Override
             protected HttpURLConnection createConnection(java.net.URL url)
                     throws IOException {
                 HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                         .createConnection(url);
                 try {
                     httpsURLConnection
                             .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(AddNewOfferLiveTradeEnhancementActivity0712.this));
                     // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
                 return httpsURLConnection;
             }
         };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF GetProductMasterAutoFillUrl API IS---" + response);



                         JSONObject GetProductMasterAutoFillUrlJson = null;
                        try {
                            GetProductMasterAutoFillUrlJson = new JSONObject(response);


                            String statusstr = GetProductMasterAutoFillUrlJson.getString("status");


                            if (statusstr.equalsIgnoreCase("true"))
                            {

                                JSONArray product_detail_jsonarray =  GetProductMasterAutoFillUrlJson.getJSONArray("product_detail");
                                JSONArray sub_category_jsonarray =  GetProductMasterAutoFillUrlJson.getJSONArray("sub_category");
                                subcategoryid = sub_category_jsonarray.getJSONObject(0).getString("sub_category_id");

                                if(catstr.equalsIgnoreCase("billetsingots")) {

                                    String q1_value_str = product_detail_jsonarray.getJSONObject(0).getString("q1_value");
                                    String[] q1_value_str_parts = q1_value_str.split("-");
                                    carbonc1edt.setText(q1_value_str_parts[0]); // 004
                                    carbonc2edt.setText(q1_value_str_parts[1]); // 034556

                                    String q2_value_str = product_detail_jsonarray.getJSONObject(0).getString("q2_value");
                                    String[] q2_value_str_parts = q2_value_str.split("-");
                                    manganese1edt.setText(q2_value_str_parts[0]);
                                    manganese2edt.setText(q2_value_str_parts[1]);

                                    String q3_value_str = product_detail_jsonarray.getJSONObject(0).getString("q3_value");
                                    String[] q3_value_str_parts = q3_value_str.split("-");
                                    silicon1edt.setText(q3_value_str_parts[0]);
                                    silicon2edt.setText(q3_value_str_parts[1]);

                                    etqty4a.setText(product_detail_jsonarray.getJSONObject(0).getString("q4_value"));
                                    etqty5a.setText(product_detail_jsonarray.getJSONObject(0).getString("q5_value"));
                                    etqty6a.setText(product_detail_jsonarray.getJSONObject(0).getString("q6_value"));
                                }
                                if(catstr.equalsIgnoreCase("spongeironpellets"))
                                {

                                    sizerange1edt.setText(product_detail_jsonarray.getJSONObject(0).getString("size_range_start"));
                                    sizerange2edt.setText(product_detail_jsonarray.getJSONObject(0).getString("size_range_end"));

                                    String q1_value_str = product_detail_jsonarray.getJSONObject(0).getString("q1_value");
                                    String[] q1_value_str_parts = q1_value_str.split("-");
                                    carbonc1bedt.setText(q1_value_str_parts[0]); // 004
                                    carbonc2bedt.setText(q1_value_str_parts[1]); // 034556

                                    etqty2b.setText(product_detail_jsonarray.getJSONObject(0).getString("q2_value"));
                                    etqty3b.setText(product_detail_jsonarray.getJSONObject(0).getString("q3_value"));

                                    String q4_value_str = product_detail_jsonarray.getJSONObject(0).getString("q4_value");
                                    String[] q4_value_str_parts = q4_value_str.split("-");
                                    feintotal1edt.setText(q4_value_str_parts[0]); // 004
                                    feintotal2edt.setText(q4_value_str_parts[1]); // 034556

                                    String q5_value_str = product_detail_jsonarray.getJSONObject(0).getString("q5_value");
                                    String[] q5_value_str_parts = q5_value_str.split("-");
                                    feinmetallic1edt.setText(q5_value_str_parts[0]); // 004
                                    feinmetallic2edt.setText(q5_value_str_parts[1]); // 034556


                                    String q6_value_str = product_detail_jsonarray.getJSONObject(0).getString("q6_value");
                                    String[] q6_value_str_parts = q6_value_str.split("-");
                                    metallization1edt.setText(q6_value_str_parts[0]); // 004
                                    metallization2edt.setText(q6_value_str_parts[1]); // 034556

                                    etqty7b.setText(product_detail_jsonarray.getJSONObject(0).getString("q7_value"));
                                    etqty8b.setText(product_detail_jsonarray.getJSONObject(0).getString("q8_value"));
                                }

                                quantitytondesctxt.setText("");
                                withindaysedt.setText("");
                                advanceratedesctxt.setText("");
                                nextdayratedesctxt.setText("");
                                regularratedesctxt.setText("");
                                loadingedttxt.setText("");
                                extra_insurance_spinner.setText("");
                                // extra_insurance_spinner.setHint("Select");
                                promotion_spinner.setText("");
                                // promotion_spinner.setHint("Promotion");
                                termsandtitledesctxt.setText("");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                    /*    noliveoffertxt.setVisibility(View.VISIBLE);
                                        lv.setVisibility(View.GONE);*/

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get product autofill params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }





    private void NotificationResetAPI(String url,final String commentStr,final String tradeStr,final int index) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(AddNewOfferLiveTradeEnhancementActivity0712.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF NOTIFICATION RESET API IS---" + response);

                        JSONObject NotificationResetJson = null;

                        try {
                            NotificationResetJson = new JSONObject(response);

                            String statusstr = NotificationResetJson.getString("status");
                            System.out.println("STATUS OF NOTIFICATION RESET API IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {


                              Intent i = new Intent(AddNewOfferLiveTradeEnhancementActivity0712.this,HomeActivity.class);

                                SingletonActivity.isNotificationClicked = false;
                                SingletonActivity.index = index;
                                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                SingletonActivity.FromAddNewOfferTabOne=false;
                                SingletonActivity.FromAddNewOfferTabZero=false;
                                SingletonActivity.isNewEnquiryClicked = false;
                                SingletonActivity.fromviewlivetrade = false;

                                SingletonActivity.from = "Add New Offer";
                              //  i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                startActivity(i);
                                // Toast.makeText(AddNewOfferLiveTradeActivity.this,NotificationResetJson.getString("message"),Toast.LENGTH_SHORT).show();

                            }
                            else
                            {
                                // Toast.makeText(AddNewOfferLiveTradeActivity.this,NotificationResetJson.getString("message"),Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this,"Some Error Occured, please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_code",userCodestr);
                params.put("comment",commentStr);
                params.put("trader",tradeStr);
                System.out.println("notification reset params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(AddNewOfferLiveTradeEnhancementActivity0712.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    public  final boolean isValidLength(String len) {

        if(len.length()>0) {



            char first_char = len.charAt(0);
            String first_char_str = Character.toString(first_char);
            if (first_char_str.equalsIgnoreCase("-")||first_char_str.equalsIgnoreCase(".")) {

                Toast.makeText(getApplicationContext(),
                        "Enter Valid Length", Toast.LENGTH_SHORT)
                        .show();
                return false;
            } else {

                int counter = 0;
                for (int i = 0; i < len.length(); i++) {
                    if (len.charAt(i) == '-') {
                        counter++;
                    }
                }

                if (counter > 1) ///contains more more than two dashed
                {


                    Toast.makeText(getApplicationContext(),
                            "Enter Valid Length", Toast.LENGTH_SHORT)
                            .show();
                    return false;
                } else {

                    //ZERO NOT ALLOW
                    int hyphencounter = 0;
                    for (int i = 0; i < len.length(); i++) {
                        if (len.charAt(i) == '-') {
                            hyphencounter++;
                        }
                    }

                    float first_float;
                    float second_float;
                    float sum;

                    if (hyphencounter == 1) {

                       /* Toast.makeText(getApplicationContext(),
                                "Hyphen present", Toast.LENGTH_SHORT)
                                .show();*/

                        String[] len_arr = len.split("-");
                        String before_dec_str = len_arr[0];
                        String after_dec_str = len_arr[1];

                        int beforedotcounter = 0;
                        int afterdotcounter = 0;

                        for (int i = 0; i < before_dec_str.length(); i++) {
                            if (before_dec_str.charAt(i) == '.') {
                                beforedotcounter++;
                            }
                        }

                        for (int j = 0; j < after_dec_str.length(); j++) {
                            if (after_dec_str.charAt(j) == '.') {
                                afterdotcounter++;
                            }
                        }


                        if(beforedotcounter > 1 || afterdotcounter > 1)
                        {
                            Toast.makeText(getApplicationContext(),
                                    "Enter Valid Length", Toast.LENGTH_SHORT)
                                    .show();
                            return false;
                        }
                        else
                        {

                            first_float = Float.parseFloat(before_dec_str);
                            second_float = Float.parseFloat(after_dec_str);

                        }






                    }
                    else
                    {
                       /* Toast.makeText(getApplicationContext(),
                                "Hyphen not present", Toast.LENGTH_SHORT)
                                .show();*/

                        int dotcounter = 0;
                        for (int i = 0; i < len.length(); i++) {
                            if (len.charAt(i) == '.') {
                                dotcounter++;
                            }
                        }

                        if(dotcounter == 0)
                        {
                            first_float = Float.parseFloat(etlength.getText().toString());
                            second_float = 0;
                        }
                        else {




                            if (len.contains(".."))
                             {

                                Toast.makeText(getApplicationContext(),
                                        "Enter Valid Length", Toast.LENGTH_SHORT)
                                        .show();
                                return false;
                            }
                            else {

                                if(len.equalsIgnoreCase("."))
                                {
                                    Toast.makeText(getApplicationContext(),
                                            "Enter Valid Length", Toast.LENGTH_SHORT)
                                            .show();
                                    return false;
                                }
                                else {
                                    String[] len_arr = len.split("\\.");
                                    String before_dec_str = len_arr[0];
                                    String after_dec_str = len_arr[1];


                                    first_float = Float.parseFloat(before_dec_str);
                                    second_float = Float.parseFloat(after_dec_str);
                                }
                            }
                        }
                    }

                     sum = first_float +  second_float;


                    if(sum == 0)
                    {
                        Toast.makeText(getApplicationContext(),
                                "Enter Valid Length", Toast.LENGTH_SHORT)
                                .show();
                        return false;
                    }
                    else {

                        int hyphen_counter = 0;
                        for (int i = 0; i < len.length(); i++) {
                            if (len.charAt(i) == '-') {
                                hyphen_counter++;
                            }
                        }


                        if (hyphen_counter == 1) {

                            String[] len_arr = len.split("-");
                            String before_dec_str_1 = len_arr[0];
                            String after_dec_str_1 = len_arr[1];


                            int beforedotcounter = 0;
                            int afterdotcounter = 0;

                            for (int i = 0; i < before_dec_str_1.length(); i++) {
                                if (before_dec_str_1.charAt(i) == '.') {
                                    beforedotcounter++;
                                }
                            }

                            for (int j = 0; j < after_dec_str_1.length(); j++) {
                                if (after_dec_str_1.charAt(j) == '.') {
                                    afterdotcounter++;
                                }
                            }


                            if(beforedotcounter > 1 || afterdotcounter > 1)
                            {
                                Toast.makeText(getApplicationContext(),
                                        "Enter Valid Length", Toast.LENGTH_SHORT)
                                        .show();
                                return false;
                            }
                            else {
                                int sumdotcounter = beforedotcounter + afterdotcounter;

                                if (sumdotcounter > 2) {
                                    Toast.makeText(getApplicationContext(),
                                            "Enter Valid Length", Toast.LENGTH_SHORT)
                                            .show();
                                    return false;
                                } else {
                                    return true;
                                }

                            }

                        }
                        else
                        {
                            String string = etlength.getText().toString();



                            int counters = 0;


                            for (int i = 0; i < string.length(); i++) {
                                if (string.charAt(i) == '.') {
                                    counters++;
                                }
                            }

                            if(counters > 1)
                            {
                                Toast.makeText(getApplicationContext(),
                                        "Enter Valid Length", Toast.LENGTH_SHORT)
                                        .show();
                                return false;
                            }
                            else
                            {
                                return true;
                            }

                        }



                    }
                }


            }



        }
        else
        {
            Toast.makeText(getApplicationContext(),
                    "Enter Valid Length", Toast.LENGTH_SHORT)
                    .show();
            return false;
        }
    }



    /*public  final boolean isValidLength(String len) {

        float first_float = 0.0f;
        float second_float = 0.0f;


        if(len.length()>0) {

           // float len_val = Float.parseFloat(len);

            char first_char = len.charAt(0);
            String first_char_str = Character.toString(first_char);
            if (first_char_str.equalsIgnoreCase("-")) {

                Toast.makeText(getApplicationContext(),
                        "Enter Valid Length", Toast.LENGTH_SHORT)
                        .show();
                return false;
            } else {


                String[] split_str = len.split("-");


                if (split_str.length == 1) {
                    second_float = 0.0f;
                } else {

                    int dotcounter = 0;
                    for (int i = 0; i < len.length(); i++) {
                        if (len.charAt(i) == '.') {
                            dotcounter++;
                        }
                    }

                    if (dotcounter > 2) {
                        Toast.makeText(getApplicationContext(),
                                "Enter Valid Length", Toast.LENGTH_SHORT)
                                .show();
                        return false;
                    } else {

                        String string = etlength.getText().toString();

                        String[] str = string.split("\\.");

                        first_float = Float.parseFloat(str[0]);

                        int hyphencounterss = 0;
                        for (int i = 0; i < len.length(); i++) {
                            if (len.charAt(i) == '-') {
                                hyphencounterss++;
                            }
                        }

                        if (hyphencounterss == 1) {


                            String[] len_arr = len.split("-");
                            String before_dec_str_1 = len_arr[0];
                            String after_dec_str_1 = len_arr[1];

                            String[] after_dot_arr_1 = before_dec_str_1.split("\\.");

                            String[] after_dot_arr_2 = after_dec_str_1.split("\\.");

                            if ((after_dot_arr_1[1].length() > 3 || after_dot_arr_2[1].length() > 3)) {
                                Toast.makeText(getApplicationContext(),
                                        "Enter Valid Length", Toast.LENGTH_SHORT)
                                        .show();
                                return false;
                            }


                          //  second_float = Float.parseFloat(str[1]);

                        } else {
                            second_float = 0.0f;

                        }


                        int counter = 0;
                        for (int i = 0; i < len.length(); i++) {
                            if (len.charAt(i) == '-') {
                                counter++;
                            }
                        }

                        if (counter > 1) ///contains more more than two dashed
                        {


                            Toast.makeText(getApplicationContext(),
                                    "Enter Valid Length", Toast.LENGTH_SHORT)
                                    .show();
                            return false;
                        } else {
                       *//* Toast.makeText(getApplicationContext(),
                                "Pass", Toast.LENGTH_SHORT)
                                .show();*//*

                            int dotcounters = 0;
                            for (int i = 0; i < len.length(); i++) {
                                if (len.charAt(i) == '-') {
                                    dotcounters++;
                                }
                            }

                            if (dotcounters > 2) {
                                Toast.makeText(getApplicationContext(),
                                        "Enter Valid Length", Toast.LENGTH_SHORT)
                                        .show();
                                return false;
                            } else {

                                int decimalcounter = 0;
                                for (int i = 0; i < len.length(); i++) {
                                    if (len.charAt(i) == '-') {
                                        decimalcounter++;
                                    }
                                }

                                if (decimalcounter == 1) {
                                    String[] len_arr = len.split("-");
                                    String before_dec_str_1 = len_arr[0];
                                    String after_dec_str_1 = len_arr[1];

                                    String[] after_dot_arr_1 = before_dec_str_1.split("\\.");

                                    String[] after_dot_arr_2 = after_dec_str_1.split("\\.");

                                    if ((after_dot_arr_1[1].length() > 3 || after_dot_arr_2[1].length() > 3)) {
                                        Toast.makeText(getApplicationContext(),
                                                "Enter Valid Length", Toast.LENGTH_SHORT)
                                                .show();
                                        return false;
                                    }

                                }


                            }

                        }


                    }

                }


                if ((first_float == 0.0f && second_float == 0.0f) || (first_float == 0 && second_float == 0)) {
                    Toast.makeText(getApplicationContext(),
                            "Enter Valid Length", Toast.LENGTH_SHORT)
                            .show();
                    return false;
                } else {
                    return true;
                }

            }
        }
        else
        {
            Toast.makeText(getApplicationContext(),
                    "Enter Valid Length", Toast.LENGTH_SHORT)
                    .show();
            return false;
        }

    }
*/

    public  final boolean isValidQuantity(String qty) {

        if(qty.length()>0) {

            float qty_val = Float.parseFloat(qty);
            if (qty_val < 50) {

                Toast.makeText(getApplicationContext(),
                        "Enter Quantity Greater Or Equal To 50.", Toast.LENGTH_SHORT)
                        .show();
                return false;
            } else {
                return true;
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(),
                    "Enter Quantity Greater Or Equal To 50.", Toast.LENGTH_SHORT)
                    .show();
            return false;
        }
    }







    private void UpdateOfferAPI(final String url) {
     /*   pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();*/

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(AddNewOfferLiveTradeEnhancementActivity0712.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        //Toast.makeText(AddNewOfferLiveTradeActivity.this,response,Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();



                        try {

                            JSONObject insertofferjson = new JSONObject(response);
                            System.out.println("UPDATE OFFER JSON IS---" + insertofferjson);

                            String statusstr = insertofferjson.getString("status");
                            String msgstr = insertofferjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this,msgstr,Toast.LENGTH_SHORT).show();

                                    Intent i = new Intent(AddNewOfferLiveTradeEnhancementActivity0712.this,HomeActivity.class);

                             /*   SingletonActivity.isNotificationClicked = false;
                                SingletonActivity.index = index;
                                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                SingletonActivity.FromAddNewOfferTabOne=false;
                                SingletonActivity.FromAddNewOfferTabZero=false;
                                SingletonActivity.isNewEnquiryClicked = false;
                                SingletonActivity.fromselltodaysoffer = true;*/
                                SingletonActivity.fromselltodaysoffer = true;
                                //SingletonActivity.fromaddnewoffer = true;
                                startActivity(i);





                            }
                            else
                            {
                                Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, msgstr, Toast.LENGTH_SHORT).show();
                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            //  pdia.dismiss();
                            e.printStackTrace();

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this,error.toString(),Toast.LENGTH_SHORT).show();
                        //     pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {


                Map<String, String> params = new HashMap<String, String>();

                String promocodevalwithoutspace;
                System.out.println("SELECTED PROMO CODE STRING IS---"+ selectedpromocodestr);


                if(selectedpromocodestr!=null) {
                    String[] selectedpromocodestring = selectedpromocodestr.split(":");
                    String promocodeval = selectedpromocodestring[1];


                    promocodevalwithoutspace = promocodeval.replaceAll("\\s+", "");


                    System.out.println("PROMO CODE VALUE without space IN IF---" + promocodevalwithoutspace);
                }
                else
                {
                    promocodevalwithoutspace = " ";
                    System.out.println("PROMO CODE VALUE without space IN ELSE---" + promocodevalwithoutspace);


                    if (!promo_code_edit.equalsIgnoreCase(" ")) {

                        promocodevalwithoutspace = promo_code_edit;
                    }
                    else
                    {
                        promocodevalwithoutspace = " ";
                    }
                }
                params.put("promo_code",promocodevalwithoutspace);


              //  String promocodevalwithoutspace = promocodeval;
          //      System.out.println("SELECTED PROMO CODE STRING in update offer IS---"+ promocodevalwithoutspace);

              //  params.put("promo_code",promocodevalwithoutspace);

                if(categoryid.equalsIgnoreCase("1")||categoryid.equalsIgnoreCase("2"))
                {

                    params.put("user_code",userCodestr);
                    params.put("mobile_num", mobilenumstr);
                    params.put("category",categoryid);
                    params.put("sub_category",subcategoryid);
                    params.put("category_grade",category_grade_str);
                    params.put("advance_rate",advanceratedesctxtstr);
                    params.put("next_day_rate",nextdayratedesctxtstr);
                    params.put("regular_rate",regularratedesctxtstr);
                    params.put("quantity",quantitytondesctxtstr);
                    params.put("promo_code",promocodevalwithoutspace);
                    params.put("sell_tandc", termsandtitledesctxtstr);
                    params.put("q1_value1",carbonc1edt.getText().toString());
                    params.put("q1_value2",carbonc2edt.getText().toString());
                    params.put("q2_value1",manganese1edt.getText().toString());
                    params.put("q2_value2",manganese2edt.getText().toString());
                    params.put("q3_value1",silicon1edt.getText().toString());
                    params.put("q3_value2",silicon2edt.getText().toString());
                    params.put("q4_value",etqty4a.getText().toString());
                    params.put("q5_value",etqty5a.getText().toString());
                    params.put("q6_value",etqty6a.getText().toString());
                    params.put("q7_value",etqty7a.getText().toString());
                    params.put("q8_value",etqty8a.getText().toString());
                    params.put("q1_title", "CARBON (C)*");
                    params.put("q2_title", "MANGANESE (Mn)*");
                    params.put("q3_title", "SILICON (Si)*");
                    params.put("q4_title", "SULPHUR (S) (Max)*");
                    params.put("q5_title", "PHOSPHORUS (P) (Max)*");
                    params.put("q6_title", "S + P(Max)*");
                    params.put("q7_title", "ALUMINIUM (Al)");
                    params.put("q8_title", "NICKEL (Max)");
                    params.put("transport", transport_spinner_str);
                    params.put("loading", loadingedttxt.getText().toString());
                    params.put("gst", gstedttxt.getText().toString());
                    params.put("insurance_select",insurance_spinner_str);
                    params.put("length",etlength.getText().toString());

                    if(insurance_spinner_str.equalsIgnoreCase("select")) {

                        if (extra_insurance_spinner_str.equalsIgnoreCase("1")) {
                            params.put("insurance", extra_insurance_spinner_str);
                        }

                        if (extra_insurance_spinner_str.equalsIgnoreCase("2")) {
                            params.put("insurance", extra_insurance_spinner_str);
                        }
                    }
                    if(insurance_spinner_str.equalsIgnoreCase("NA"))
                    {
                        extra_insurance_spinner_str = "";
                        params.put("insurance",extra_insurance_spinner_str);
                    }

                    if(deliveryindays_spinner_str.equalsIgnoreCase("1"))
                    {
                        params.put("delivery_type","1");
                        // params.put("delivery_in_days","0");
                    }
                    if(deliveryindays_spinner_str.equalsIgnoreCase("2"))
                    {
                        params.put("delivery_type","2");
                        params.put("delivery_in_days",withindaysedt.getText().toString());
                    }

                    if(material_inspection_spinner_str.equalsIgnoreCase("1"))
                    {
                        params.put("material_inspection_type","1");
                    }
                    if(material_inspection_spinner_str.equalsIgnoreCase("2"))
                    {
                        params.put("material_inspection_type","2");
                    }

                    if(weighment_spinner_str.equalsIgnoreCase("1"))
                    {
                        params.put("weightment_type","1");
                    }
                    if(weighment_spinner_str.equalsIgnoreCase("2"))
                    {
                        params.put("weightment_type","2");
                    }
                    if(weighment_spinner_str.equalsIgnoreCase("3"))
                    {
                        params.put("weightment_type","3");
                    }

                    if(test_certificate_spinner_str.equalsIgnoreCase("1"))
                    {
                        params.put("test_certificate_type","1");
                    }
                    if(test_certificate_spinner_str.equalsIgnoreCase("2"))
                    {
                        params.put("test_certificate_type","2");
                    }


                }

                if(categoryid.equalsIgnoreCase("3")||categoryid.equalsIgnoreCase("4"))
                {

                    params.put("user_code",userCodestr);
                    params.put("mobile_num", mobilenumstr);
                    params.put("category", categoryid);
                    params.put("size_range_start",sizerange1edt.getText().toString());
                    params.put("size_range_end", sizerange2edt.getText().toString());
                    params.put("category_grade",category_grade_str);
                    params.put("advance_rate",advanceratedesctxtstr);
                    params.put("next_day_rate",nextdayratedesctxtstr);
                    params.put("regular_rate",regularratedesctxtstr);
                    params.put("quantity",quantitytondesctxtstr);
                    params.put("promo_code",promocodevalwithoutspace);
                    params.put("sell_tandc",termsandtitledesctxtstr);
                    params.put("q1_value1",carbonc1bedt.getText().toString());
                    params.put("q1_value2",carbonc2bedt.getText().toString());
                    params.put("q2_value",etqty2b.getText().toString());
                    params.put("q3_value",etqty3b.getText().toString());
                    params.put("q4_value1",feintotal1edt.getText().toString());
                    params.put("q4_value2",feintotal2edt.getText().toString());
                    params.put("q5_value1",feinmetallic1edt.getText().toString());
                    params.put("q5_value2",feinmetallic2edt.getText().toString());
                    params.put("q6_value1",metallization1edt.getText().toString());
                    params.put("q6_value2",metallization2edt.getText().toString());
                    params.put("q7_value",etqty7b.getText().toString());
                    params.put("q8_value",etqty8b.getText().toString());
                    params.put("q1_title","CARBON (C)");
                    params.put("q2_title","SULPHUR (S) (Max)");
                    params.put("q3_title","PHOSPHORUS (P) (Max)");
                    params.put("q4_title","Fe in Total (Fe(T))");
                    params.put("q5_title","Fe in Metallic (Fe(M))");
                    params.put("q6_title","METALLIZATION (MTZ)");
                    params.put("q7_title","NON MAGNETIC MATERIAL (Non Mag.)");
                    params.put("q8_title","TOTAL SILICA AND ALUMINA (SiO2 + Al2O3)");
                    params.put("transport",transport_spinner_str);
                    params.put("loading",loadingedttxt.getText().toString());
                    params.put("gst",gstedttxt.getText().toString());
                    params.put("insurance_select", insurance_spinner_str);
                    if(insurance_spinner_str.equalsIgnoreCase("select")) {

                        if (extra_insurance_spinner_str.equalsIgnoreCase("1")) {
                            params.put("insurance", extra_insurance_spinner_str);
                        }

                        if (extra_insurance_spinner_str.equalsIgnoreCase("2")) {
                            params.put("insurance", extra_insurance_spinner_str);
                        }
                    }
                    if(insurance_spinner_str.equalsIgnoreCase("NA"))
                    {
                        extra_insurance_spinner_str = "";
                        params.put("insurance",extra_insurance_spinner_str);
                    }

                    if(deliveryindays_spinner_str.equalsIgnoreCase("1"))
                    {
                        params.put("delivery_type","1");
                        // params.put("delivery_in_days","0");
                    }
                    if(deliveryindays_spinner_str.equalsIgnoreCase("2"))
                    {
                        params.put("delivery_type","2");
                        params.put("delivery_in_days",withindaysedt.getText().toString());
                    }

                    if(material_inspection_spinner_str.equalsIgnoreCase("1"))
                    {
                        params.put("material_inspection_type","1");
                    }
                    if(material_inspection_spinner_str.equalsIgnoreCase("2"))
                    {
                        params.put("material_inspection_type","2");
                    }

                    if(weighment_spinner_str.equalsIgnoreCase("1"))
                    {
                        params.put("weightment_type","1");
                    }
                    if(weighment_spinner_str.equalsIgnoreCase("2"))
                    {
                        params.put("weightment_type","2");
                    }
                    if(weighment_spinner_str.equalsIgnoreCase("3"))
                    {
                        params.put("weightment_type","3");
                    }

                    if(test_certificate_spinner_str.equalsIgnoreCase("1"))
                    {
                        params.put("test_certificate_type","1");
                    }
                    if(test_certificate_spinner_str.equalsIgnoreCase("2"))
                    {
                        params.put("test_certificate_type","2");
                    }

                }




                System.out.println("update offer params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(AddNewOfferLiveTradeEnhancementActivity0712.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void GetLastPostedOffer1API(String url) {

        pdia = new ProgressDialog(AddNewOfferLiveTradeEnhancementActivity0712.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(AddNewOfferLiveTradeEnhancementActivity0712.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();


                        System.out.println("RESPONSE OF GET LAST POSTED OFFER API IS---" + response);



                        JSONObject GetLastPostedOfferJson = null;
                        try {
                            GetLastPostedOfferJson = new JSONObject(response);


                            String statusstr = GetLastPostedOfferJson.getString("status");


                            if (statusstr.equalsIgnoreCase("true"))
                            {

                                JSONObject lastOfferJSON = GetLastPostedOfferJson.getJSONObject("lastOffer");

                                String category = lastOfferJSON.getString("category");
                                String category_name = lastOfferJSON.getString("category_name");
                                String sub_category = lastOfferJSON.getString("sub_category");
                                String category_grade_name = lastOfferJSON.getString("category_grade_name");
                                String sub_category_name = lastOfferJSON.getString("sub_category_name");
                                String category_grade = lastOfferJSON.getString("category_grade");
                                String length = lastOfferJSON.getString("length");
                                String quantity = lastOfferJSON.getString("quantity");
                                String advance_rate = lastOfferJSON.getString("advance_rate");
                                String next_day_rate = lastOfferJSON.getString("next_day_rate");
                                String regular_rate = lastOfferJSON.getString("regular_rate");
                                String delivery_type = lastOfferJSON.getString("delivery_type");
                                String delivery_in_days = lastOfferJSON.getString("delivery_in_days");
                                String material_inspection_type = lastOfferJSON.getString("material_inspection_type");
                                String weighment_type = lastOfferJSON.getString("weightment_type");
                                String  test_certificate_type = lastOfferJSON.getString("test_certificate_type");
                                String transport = lastOfferJSON.getString("transport");
                                String loading = lastOfferJSON.getString("loading");
                                String gst = lastOfferJSON.getString("gst");
                                String insurance_select = lastOfferJSON.getString("insurance_select");
                                String insurance = lastOfferJSON.getString("insurance");
                                String sell_tandc = lastOfferJSON.getString("sell_tandc");
                                promo_code = lastOfferJSON.getString("promo_code");

                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                                        android.R.layout.simple_dropdown_item_1line, materialcatarraylist);

                                materialDesignSpinner1.setAdapter(arrayAdapter);

                                materialDesignSpinner1.setSelection(Integer.parseInt(category)-1);


                                category_id = category;
                                subcategoryid = sub_category;
                                category_grade_id = category_grade;

                                materialDesignCategoryGradeSpinner.setText(category_grade_name);
                            //    materialDesignSpinner2.setText(sub_category_name);
                                quantitytondesctxt.setText(quantity);

                                advanceratedesctxt.setText(advance_rate);
                                nextdayratedesctxt.setText(next_day_rate);
                                regularratedesctxt.setText(regular_rate);




                                if(delivery_type.equalsIgnoreCase("1"))
                                {

                                    deliveryindays_spinner.setText("As per Aachar Sanhita");
                                    withindaystitletxt.setVisibility(View.GONE);
                                    withindaysedt.setVisibility(View.GONE);
                                    withindaysedt.setText("");
                                    deliveryindays_spinner_str = "1";
                                }
                                if(delivery_type.equalsIgnoreCase("2"))
                                {
                                    deliveryindays_spinner.setText("With in(Days)");
                                    withindaystitletxt.setVisibility(View.VISIBLE);
                                    withindaysedt.setVisibility(View.VISIBLE);
                                    withindaysedt.setText(delivery_in_days);

                                    deliveryindays_spinner_str = "2";
                                }

                                if(material_inspection_type.equalsIgnoreCase("1"))
                                {
                                    material_inspection_spinner_str = "1";
                                    material_inspection_spinner.setText("Buyer's Place");
                                }
                                if(material_inspection_type.equalsIgnoreCase("2"))
                                {
                                    material_inspection_spinner_str = "2";
                                    material_inspection_spinner.setText("Seller's Place");
                                }

                                if(weighment_type.equalsIgnoreCase("1"))
                                {
                                    weighment_spinner_str = "1";
                                    weighment_spinner.setText("Buyer's Place");
                                }
                                if(weighment_type.equalsIgnoreCase("2"))
                                {
                                    weighment_spinner_str = "2";
                                    weighment_spinner.setText("Seller's Place");
                                }
                                if(weighment_type.equalsIgnoreCase("3"))
                                {
                                    weighment_spinner_str = "3";
                                    weighment_spinner.setText("Both");
                                }


                                if(test_certificate_type.equalsIgnoreCase("1"))
                                {
                                    test_certificate_spinner_str = "1";
                                    test_certificate_spinner.setText("Yes");
                                    // test_certificate_spinner.setText("Required");
                                }
                                if(test_certificate_type.equalsIgnoreCase("2"))
                                {
                                    test_certificate_spinner_str = "2";
                                    test_certificate_spinner.setText("No");
                                    //  test_certificate_spinner.setText("Not Required");
                                }


                                if(transport.equalsIgnoreCase("1"))
                                {
                                    transportation_spinner.setText("SELLER'S END");
                                    transport_spinner_str = "1";
                                }
                                if(transport.equalsIgnoreCase("2"))
                                {
                                    transportation_spinner.setText("BUYER'S END");
                                    transport_spinner_str = "2";
                                }
                                if(transport.equalsIgnoreCase("3"))
                                {
                                    transportation_spinner.setText("MXMART'S END");
                                    transport_spinner_str = "3";
                                }


                                if(loading.equalsIgnoreCase("NA"))
                                {
                                    loadingedttxt.setText(loading);
                                }
                                else {
                                    loadingedttxt.setText(loading);
                                }
                                gstedttxt.setText(gst);

                                if(insurance_select.equalsIgnoreCase("NA"))
                                {
                                    insurance_spinner.setText("NA");
                                    insurance_spinner_str = "NA";
                                    extra_insurance_spinner.setVisibility(View.GONE);
                                    extrainsurancetxt.setVisibility(View.GONE);
                                }
                                else
                                {
                                    extrainsurancetxt.setVisibility(View.VISIBLE);
                                    extra_insurance_spinner.setVisibility(View.VISIBLE);

                                    if (insurance.equalsIgnoreCase("1")) {
                                        extra_insurance_spinner.setText("SELLER'S END");
                                        extra_insurance_spinner_str = "1";
                                    }  if (insurance.equalsIgnoreCase("2")) {
                                    extra_insurance_spinner.setText("BUYER'S END");
                                    extra_insurance_spinner_str = "2";
                                }

                                }

                                termsandtitledesctxt.setText(sell_tandc);

                                if(!promo_code.equalsIgnoreCase(" "))
                                {
                                    promotion_spinner.setText("Promo Code :"+promo_code);

                                    System.out.println("promotion_spinner IN EDIT if--"+ promo_code);



                                }
                                else
                                {
                                    System.out.println("promotion_spinner IN EDIT else--"+ promo_code);
                                    promotion_spinner.setText("Select Promo");



                                }





                                if(category.equalsIgnoreCase("1")||category.equalsIgnoreCase("2")) {

                                    sizerangeedtlinear.setVisibility(View.INVISIBLE);
                                    materialDesignSpinner2.setVisibility(View.VISIBLE);
                                    lengthtxt.setVisibility(View.VISIBLE);
                                    etlength.setVisibility(View.VISIBLE);
                                    sizerangetxt.setVisibility(View.GONE);
                                    qualityparamsrel1.setVisibility(View.VISIBLE);
                                    qualityparamsrel2.setVisibility(View.INVISIBLE);

                                    materialDesignSpinner2.setText(sub_category_name);
                                    etlength.setText(length);


                                    String q1_value_str = lastOfferJSON.getString("q1_value");
                                    String[] q1_value_str_parts = q1_value_str.split("-");
                                    carbonc1edt.setText(q1_value_str_parts[0]); // 004
                                    carbonc2edt.setText(q1_value_str_parts[1]); // 034556

                                    String q2_value_str = lastOfferJSON.getString("q2_value");
                                    String[] q2_value_str_parts = q2_value_str.split("-");
                                    manganese1edt.setText(q2_value_str_parts[0]);
                                    manganese2edt.setText(q2_value_str_parts[1]);

                                    String q3_value_str = lastOfferJSON.getString("q3_value");
                                    String[] q3_value_str_parts = q3_value_str.split("-");
                                    silicon1edt.setText(q3_value_str_parts[0]);
                                    silicon2edt.setText(q3_value_str_parts[1]);

                                    etqty4a.setText(lastOfferJSON.getString("q4_value"));
                                    etqty5a.setText(lastOfferJSON.getString("q5_value"));
                                    etqty6a.setText(lastOfferJSON.getString("q6_value"));
                                    etqty7a.setText(lastOfferJSON.getString("q7_value"));
                                    etqty8a.setText(lastOfferJSON.getString("q8_value"));
                                }
                                if(category.equalsIgnoreCase("3")||category.equalsIgnoreCase("4")) {

                                    sizerangeedtlinear.setVisibility(View.VISIBLE);
                                    sizerange1edt.setVisibility(View.VISIBLE);
                                    hypentxt.setVisibility(View.VISIBLE);
                                    sizerange2edt.setVisibility(View.VISIBLE);
                                    materialDesignSpinner2.setVisibility(View.INVISIBLE);
                                    sizerangetxt.setVisibility(View.VISIBLE);
                                    qualityparamsrel1.setVisibility(View.INVISIBLE);
                                    qualityparamsrel2.setVisibility(View.VISIBLE);

                                    sizerange1edt.setText(lastOfferJSON.getString("size_range_start"));
                                    sizerange2edt.setText(lastOfferJSON.getString("size_range_end"));

                                    String q1_value_str = lastOfferJSON.getString("q1_value");
                                    String[] q1_value_str_parts = q1_value_str.split("-");
                                    carbonc1bedt.setText(q1_value_str_parts[0]); // 004
                                    carbonc2bedt.setText(q1_value_str_parts[1]); // 034556

                                    etqty2b.setText(lastOfferJSON.getString("q2_value"));
                                    etqty3b.setText(lastOfferJSON.getString("q3_value"));

                                    String q4_value_str = lastOfferJSON.getString("q4_value");
                                    String[] q4_value_str_parts = q4_value_str.split("-");
                                    feintotal1edt.setText(q4_value_str_parts[0]); // 004
                                    feintotal2edt.setText(q4_value_str_parts[1]); // 034556

                                    String q5_value_str = lastOfferJSON.getString("q5_value");
                                    String[] q5_value_str_parts = q5_value_str.split("-");
                                    feinmetallic1edt.setText(q5_value_str_parts[0]); // 004
                                    feinmetallic2edt.setText(q5_value_str_parts[1]); // 034556


                                    String q6_value_str = lastOfferJSON.getString("q6_value");
                                    String[] q6_value_str_parts = q6_value_str.split("-");
                                    metallization1edt.setText(q6_value_str_parts[0]); // 004
                                    metallization2edt.setText(q6_value_str_parts[1]); // 034556

                                    etqty7b.setText(lastOfferJSON.getString("q7_value"));
                                    etqty8b.setText(lastOfferJSON.getString("q8_value"));
                                }
                            }
                            else
                            {
                                if (NetworkUtility.checkConnectivity(AddNewOfferLiveTradeEnhancementActivity0712.this)) {
                                    String GetProductMasterAutoFillUrl = APIName.URL + "/home/getProductMaster?category_id=" + category_id + "&category_grade_id=" + category_grade_id;
                                    System.out.println("GET PRODUCT MASTER AUTOFILL URL IS---" + GetProductMasterAutoFillUrl);
                                    GetProductMasterAutoFillAPI(GetProductMasterAutoFillUrl);

                                } else {
                                    util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "Please check your internet connection.");
                                }

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia.dismiss();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                    /*    noliveoffertxt.setVisibility(View.VISIBLE);
                                        lv.setVisibility(View.GONE);*/

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();



                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }


    private void GetLastPostedOfferAPI(String url) {

        pdia1 = new ProgressDialog(AddNewOfferLiveTradeEnhancementActivity0712.this);
        pdia1.setMessage("Please Wait...");
        pdia1.setCanceledOnTouchOutside(false);
        pdia1.setCancelable(false);
        pdia1.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(AddNewOfferLiveTradeEnhancementActivity0712.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia1.dismiss();


                        System.out.println("RESPONSE OF GET LAST POSTED OFFER API IS---" + response);



                        JSONObject GetLastPostedOfferJson = null;
                        try {
                            GetLastPostedOfferJson = new JSONObject(response);


                            String statusstr = GetLastPostedOfferJson.getString("status");


                            if (statusstr.equalsIgnoreCase("true")) {

                                JSONObject lastOfferJSON = GetLastPostedOfferJson.getJSONObject("lastOffer");

                                final String category = lastOfferJSON.getString("category");
                                String category_name = lastOfferJSON.getString("category_name");
                                String sub_category = lastOfferJSON.getString("sub_category");
                                String category_grade_name = lastOfferJSON.getString("category_grade_name");
                                String sub_category_name = lastOfferJSON.getString("sub_category_name");
                                String category_grade = lastOfferJSON.getString("category_grade");
                                String length = lastOfferJSON.getString("length");
                                String quantity = lastOfferJSON.getString("quantity");
                                String advance_rate = lastOfferJSON.getString("advance_rate");
                                String next_day_rate = lastOfferJSON.getString("next_day_rate");
                                String regular_rate = lastOfferJSON.getString("regular_rate");
                                String delivery_type = lastOfferJSON.getString("delivery_type");
                                String delivery_in_days = lastOfferJSON.getString("delivery_in_days");
                                String material_inspection_type = lastOfferJSON.getString("material_inspection_type");
                                String weighment_type = lastOfferJSON.getString("weightment_type");
                                String test_certificate_type = lastOfferJSON.getString("test_certificate_type");
                                String transport = lastOfferJSON.getString("transport");
                                String loading = lastOfferJSON.getString("loading");
                                String gst = lastOfferJSON.getString("gst");
                                String insurance_select = lastOfferJSON.getString("insurance_select");
                                String insurance = lastOfferJSON.getString("insurance");
                                String sell_tandc = lastOfferJSON.getString("sell_tandc");
                                promo_code = lastOfferJSON.getString("promo_code");

                                if(!promo_code.equalsIgnoreCase(" ")) {
                                    promo_details_jsonArray = lastOfferJSON.getJSONArray("promo_details");
                                    System.out.println("promo_details_jsonArray IS---" + promo_details_jsonArray.length());


                                    if (promo_details_jsonArray.length() > 0) {

                                        promo_table.setVisibility(View.VISIBLE);

                                        promotion_txt.setVisibility(View.GONE);
                                        line6.setVisibility(View.GONE);
                                        line7.setVisibility(View.VISIBLE);

                                        {
                                            System.out.println("PROMO INNER JSON ARRAY IN ON ITEM CLICK IF EQUAL IS---" + promo_details_jsonArray);

                                            if (promo_details_jsonArray.length() == 0) {
                                                tr_header.setVisibility(View.GONE);
                                                tr_item_1.setVisibility(View.GONE);
                                                tr_item_2.setVisibility(View.GONE);
                                                tr_item_3.setVisibility(View.GONE);
                                                tr_item_4.setVisibility(View.GONE);
                                                line.setVisibility(View.GONE);
                                            }

                                            if (promo_details_jsonArray.length() == 1) {
                                                tr_header.setVisibility(View.VISIBLE);
                                                tr_item_1.setVisibility(View.VISIBLE);
                                                tr_item_2.setVisibility(View.GONE);
                                                tr_item_3.setVisibility(View.GONE);
                                                tr_item_4.setVisibility(View.GONE);
                                                line.setVisibility(View.VISIBLE);

                                                tv_qtyfromdesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("from_qty"));
                                                tv_qtytodesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("to_qty"));
                                                tv_discdesc1.setText("Rs. " + promo_details_jsonArray.getJSONObject(0).getString("discount_per_ton"));


                                            }

                                            if (promo_details_jsonArray.length() == 2) {
                                                tr_header.setVisibility(View.VISIBLE);
                                                tr_item_1.setVisibility(View.VISIBLE);
                                                tr_item_2.setVisibility(View.VISIBLE);
                                                tr_item_3.setVisibility(View.GONE);
                                                tr_item_4.setVisibility(View.GONE);
                                                line.setVisibility(View.VISIBLE);

                                                tv_qtyfromdesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("from_qty"));
                                                tv_qtytodesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("to_qty"));
                                                tv_discdesc1.setText("Rs. " + promo_details_jsonArray.getJSONObject(0).getString("discount_per_ton"));

                                                tv_qtyfromdesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("from_qty"));
                                                tv_qtytodesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("to_qty"));
                                                tv_discdesc2.setText("Rs. " + promo_details_jsonArray.getJSONObject(1).getString("discount_per_ton"));
                                            }

                                            if (promo_details_jsonArray.length() == 3) {
                                                tr_header.setVisibility(View.VISIBLE);
                                                tr_item_1.setVisibility(View.VISIBLE);
                                                tr_item_2.setVisibility(View.VISIBLE);
                                                tr_item_3.setVisibility(View.VISIBLE);
                                                tr_item_4.setVisibility(View.GONE);
                                                line.setVisibility(View.VISIBLE);

                                                tv_qtyfromdesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("from_qty"));
                                                tv_qtytodesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("to_qty"));
                                                tv_discdesc1.setText("Rs. " + promo_details_jsonArray.getJSONObject(0).getString("discount_per_ton"));

                                                tv_qtyfromdesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("from_qty"));
                                                tv_qtytodesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("to_qty"));
                                                tv_discdesc2.setText("Rs. " + promo_details_jsonArray.getJSONObject(1).getString("discount_per_ton"));

                                                tv_qtyfromdesc3.setText(promo_details_jsonArray.getJSONObject(2).getString("from_qty"));
                                                tv_qtytodesc3.setText(promo_details_jsonArray.getJSONObject(2).getString("to_qty"));
                                                tv_discdesc3.setText("Rs. " + promo_details_jsonArray.getJSONObject(2).getString("discount_per_ton"));
                                            }

                                            if (promo_details_jsonArray.length() == 4) {
                                                tr_header.setVisibility(View.VISIBLE);
                                                tr_item_1.setVisibility(View.VISIBLE);
                                                tr_item_2.setVisibility(View.VISIBLE);
                                                tr_item_3.setVisibility(View.VISIBLE);
                                                tr_item_4.setVisibility(View.VISIBLE);
                                                line.setVisibility(View.VISIBLE);

                                                tv_qtyfromdesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("from_qty"));
                                                tv_qtytodesc1.setText(promo_details_jsonArray.getJSONObject(0).getString("to_qty"));
                                                tv_discdesc1.setText("Rs. " + promo_details_jsonArray.getJSONObject(0).getString("discount_per_ton"));

                                                tv_qtyfromdesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("from_qty"));
                                                tv_qtytodesc2.setText(promo_details_jsonArray.getJSONObject(1).getString("to_qty"));
                                                tv_discdesc2.setText("Rs. " + promo_details_jsonArray.getJSONObject(1).getString("discount_per_ton"));

                                                tv_qtyfromdesc3.setText(promo_details_jsonArray.getJSONObject(2).getString("from_qty"));
                                                tv_qtytodesc3.setText(promo_details_jsonArray.getJSONObject(2).getString("to_qty"));
                                                tv_discdesc3.setText("Rs. " + promo_details_jsonArray.getJSONObject(2).getString("discount_per_ton"));

                                                tv_qtyfromdesc4.setText(promo_details_jsonArray.getJSONObject(3).getString("from_qty"));
                                                tv_qtytodesc4.setText(promo_details_jsonArray.getJSONObject(3).getString("to_qty"));
                                                tv_discdesc4.setText("Rs. " + promo_details_jsonArray.getJSONObject(3).getString("discount_per_ton"));
                                            }

                                        }

                                    }
                                }


                                category_id = category;
                                subcategoryid = sub_category;
                                category_grade_id = category_grade;



                                materialDesignSpinner1.getText().clearSpans();
                                materialDesignSpinner1.setText(category_name);


                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                                        android.R.layout.simple_dropdown_item_1line, materialcatarraylist);

                                materialDesignSpinner1.setAdapter(arrayAdapter);


                                // materialDesignSpinner1.setSelected(false);


                                //   materialDesignSpinner1.setSelection(-1);


                                final AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        //set the text of TextView
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                };

                                materialDesignSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        materialDesignSpinner1.setOnItemSelectedListener(listener);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });

                                //materialDesignSpinner1.setSelection(0);

                                materialDesignSpinner1.setSelection(Integer.parseInt(category)-1);
                                //================================================================================

                                for (int j = 0; j < CategoryGradeJsonArray.length(); j++) {


                                    categorygradenamejsonarrstr = CategoryGradeJsonArray.getJSONObject(j).getString("category_grade_name");


                                    if (category_id.equalsIgnoreCase(CategoryGradeJsonArray.getJSONObject(j).getString("category_id"))) {
                                        categorygradearraylist.add(categorygradenamejsonarrstr);
                                        categorygradeidarraylist.add(CategoryGradeJsonArray.getJSONObject(j).getString("category_grade_id"));

                                    }


                                }
                                materialsubcatarraylist.clear();
                                //FOR SUB CATEGORY=====
                                for (int j = 0; j < SubCategoryJsonArray.length(); j++) {


                                    sub_category_id = SubCategoryJsonArray.getJSONObject(j).getString("sub_category_id");
                                    String categoryidfromsubcatjsonarrstr = SubCategoryJsonArray.getJSONObject(j).getString("category_id");
                                    subcatnamefromsubcatjsonarrstr = SubCategoryJsonArray.getJSONObject(j).getString("sub_category_name");


                                    if (category_id.equalsIgnoreCase(SubCategoryJsonArray.getJSONObject(j).getString("category_id"))) {
                                        materialsubcatarraylist.add(subcatnamefromsubcatjsonarrstr);
                                        subcategoryidlist.add(SubCategoryJsonArray.getJSONObject(j).getString("sub_category_id"));
                                    }


                                }

                                for (int p = 0; p < materialsubcatarraylist.size(); p++) {
                                    for (int q = 0; q < SingletonActivity.SubCategoryArrayList.size(); q++) {
                                        if (SingletonActivity.SubCategoryArrayList.get(q).equalsIgnoreCase(materialsubcatarraylist.get(p))) {
                                            materialsubcatarraylist.remove(p);
                                        }

                                    }
                                }


                                System.out.println("SUB-CATEGORY ARRAYLIST IS---" + materialsubcatarraylist);
                                System.out.println("SINGLETON SUB-CATEGORY ARRAYLIST IS---" + SingletonActivity.SubCategoryArrayList);


                                ArrayAdapter<String> CategoryGradeAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                                        android.R.layout.simple_dropdown_item_1line, categorygradearraylist);
                                materialDesignCategoryGradeSpinner = (MaterialBetterSpinner)
                                        findViewById(R.id.grade_spinner);
                                materialDesignCategoryGradeSpinner.setAdapter(CategoryGradeAdapter);
                                materialDesignCategoryGradeSpinner.setText(categorygradearraylist.get(0));


                                ArrayAdapter<String> SubCatArrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                                        android.R.layout.simple_dropdown_item_1line, materialsubcatarraylist);
                                materialDesignSpinner2 = (MaterialBetterSpinner)
                                        findViewById(R.id.sub_category_spinner);
                                materialDesignSpinner2.setAdapter(SubCatArrayAdapter);
                                /*if (category.equalsIgnoreCase("1")||category.equalsIgnoreCase("2")) {
                                    materialDesignSpinner2.setText(materialsubcatarraylist.get(0));
                                    subcategoryid = subcategoryidlist.get(0);
                                }
*/

                                category_grade_id = categorygradeidarraylist.get(0);


                               //GET LAST POSTED OFFER=======
                                materialDesignSpinner2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                        subcategoryid = subcategoryidlist.get(i);

                                        // Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, "SELECTED SUB CATGEGORY ID in lastposted IS===" + subcategoryid, Toast.LENGTH_SHORT).show();
                                    }
                                });
                                //================================================
                                //===============================

                                materialDesignCategoryGradeSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        category_grade_id = categorygradeidarraylist.get(position);

                                        //  Toast.makeText(AddNewOfferLiveTradeEnhancementActivity.this,"category_grade_id is=="+ category_grade_id,Toast.LENGTH_SHORT).show();
                                        //for autofill=========>

                                    /*    ArrayAdapter<String> SubCatArrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                                                android.R.layout.simple_dropdown_item_1line, materialsubcatarraylist);
                                        materialDesignSpinner2 = (MaterialBetterSpinner)
                                                findViewById(R.id.sub_category_spinner);
                                        materialDesignSpinner2.setAdapter(SubCatArrayAdapter);

                                        materialDesignSpinner2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                                sub_category_id = subcategoryidlist.get(position);
                                            }
                                        });*/


                                     /*   if (catstr.equalsIgnoreCase("billetsingots")) {
                                            materialDesignSpinner2.setText(materialsubcatarraylist.get(0));
                                        }*/
                                    }




                                });









                                materialDesignCategoryGradeSpinner.setText(category_grade_name);






                                //===================================================================================

                                //    materialDesignSpinner2.setText(sub_category_name);
                                quantitytondesctxt.setText(quantity);

                                advanceratedesctxt.setText(advance_rate);
                                nextdayratedesctxt.setText(next_day_rate);
                                regularratedesctxt.setText(regular_rate);




                                if(delivery_type.equalsIgnoreCase("1"))
                                {

                                    deliveryindays_spinner.setText("As per Aachar Sanhita");
                                    withindaystitletxt.setVisibility(View.GONE);
                                    withindaysedt.setVisibility(View.GONE);
                                    withindaysedt.setText("");
                                    deliveryindays_spinner_str = "1";
                                }
                                if(delivery_type.equalsIgnoreCase("2"))
                                {
                                    deliveryindays_spinner.setText("With in(Days)");
                                    withindaystitletxt.setVisibility(View.VISIBLE);
                                    withindaysedt.setVisibility(View.VISIBLE);
                                    withindaysedt.setText(delivery_in_days);

                                    deliveryindays_spinner_str = "2";
                                }

                                if(material_inspection_type.equalsIgnoreCase("1"))
                                {
                                    material_inspection_spinner_str = "1";
                                    material_inspection_spinner.setText("Buyer's Place");
                                }
                                if(material_inspection_type.equalsIgnoreCase("2"))
                                {
                                    material_inspection_spinner_str = "2";
                                    material_inspection_spinner.setText("Seller's Place");
                                }

                                if(weighment_type.equalsIgnoreCase("1"))
                                {
                                    weighment_spinner_str = "1";
                                    weighment_spinner.setText("Buyer's Place");
                                }
                                if(weighment_type.equalsIgnoreCase("2"))
                                {
                                    weighment_spinner_str = "2";
                                    weighment_spinner.setText("Seller's Place");
                                }
                                if(weighment_type.equalsIgnoreCase("3"))
                                {
                                    weighment_spinner_str = "3";
                                    weighment_spinner.setText("Both");
                                }


                                if(test_certificate_type.equalsIgnoreCase("1"))
                                {
                                    test_certificate_spinner_str = "1";
                                    test_certificate_spinner.setText("Yes");
                                    // test_certificate_spinner.setText("Required");
                                }
                                if(test_certificate_type.equalsIgnoreCase("2"))
                                {
                                    test_certificate_spinner_str = "2";
                                    test_certificate_spinner.setText("No");
                                    //  test_certificate_spinner.setText("Not Required");
                                }


                                if(transport.equalsIgnoreCase("1"))
                                {
                                    transportation_spinner.setText("SELLER'S END");
                                    transport_spinner_str = "1";
                                }
                                if(transport.equalsIgnoreCase("2"))
                                {
                                    transportation_spinner.setText("BUYER'S END");
                                    transport_spinner_str = "2";
                                }
                                if(transport.equalsIgnoreCase("3"))
                                {
                                    transportation_spinner.setText("MXMART'S END");
                                    transport_spinner_str = "3";
                                }


                                if(loading.equalsIgnoreCase("NA"))
                                {
                                    loadingedttxt.setText(loading);
                                }
                                else {
                                    loadingedttxt.setText(loading);
                                }
                                gstedttxt.setText(gst);

                                if(insurance_select.equalsIgnoreCase("NA"))
                                {
                                    insurance_spinner.setText("NA");
                                    insurance_spinner_str = "NA";
                                    extra_insurance_spinner.setVisibility(View.GONE);
                                    extrainsurancetxt.setVisibility(View.GONE);
                                }
                                else
                                {
                                    extrainsurancetxt.setVisibility(View.VISIBLE);
                                    extra_insurance_spinner.setVisibility(View.VISIBLE);

                                    if (insurance.equalsIgnoreCase("1")) {
                                        extra_insurance_spinner.setText("SELLER'S END");
                                        extra_insurance_spinner_str = "1";
                                    }  if (insurance.equalsIgnoreCase("2")) {
                                    extra_insurance_spinner.setText("BUYER'S END");
                                    extra_insurance_spinner_str = "2";
                                }

                                }

                                termsandtitledesctxt.setText(sell_tandc);

                                if(!promo_code.equalsIgnoreCase(" "))
                                {
                                    promotion_spinner.setText("Promo Code :"+promo_code);

                                    System.out.println("promotion_spinner IN EDIT if--"+ promo_code);



                                }
                                else
                                {
                                    System.out.println("promotion_spinner IN EDIT else--"+ promo_code);
                                    promotion_spinner.setText("Select Promo");


                                }




                                if(category.equalsIgnoreCase("1")||category.equalsIgnoreCase("2")) {

                                    catstr = "billetsingots";
                                    sizerangeedtlinear.setVisibility(View.INVISIBLE);
                                    materialDesignSpinner2.setVisibility(View.VISIBLE);
                                    lengthtxt.setVisibility(View.VISIBLE);
                                    etlength.setVisibility(View.VISIBLE);
                                    sizerangetxt.setVisibility(View.GONE);
                                    qualityparamsrel1.setVisibility(View.VISIBLE);
                                    qualityparamsrel2.setVisibility(View.INVISIBLE);

                                 //   Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this,"T 1",Toast.LENGTH_SHORT).show();
                                    materialDesignSpinner2.setText(sub_category_name);
                                    etlength.setText(length);


                                    String q1_value_str = lastOfferJSON.getString("q1_value");
                                    String[] q1_value_str_parts = q1_value_str.split("-");
                                    carbonc1edt.setText(q1_value_str_parts[0]); // 004
                                    carbonc2edt.setText(q1_value_str_parts[1]); // 034556

                                    String q2_value_str = lastOfferJSON.getString("q2_value");
                                    String[] q2_value_str_parts = q2_value_str.split("-");
                                    manganese1edt.setText(q2_value_str_parts[0]);
                                    manganese2edt.setText(q2_value_str_parts[1]);

                                    String q3_value_str = lastOfferJSON.getString("q3_value");
                                    String[] q3_value_str_parts = q3_value_str.split("-");
                                    silicon1edt.setText(q3_value_str_parts[0]);
                                    silicon2edt.setText(q3_value_str_parts[1]);

                                    etqty4a.setText(lastOfferJSON.getString("q4_value"));
                                    etqty5a.setText(lastOfferJSON.getString("q5_value"));
                                    etqty6a.setText(lastOfferJSON.getString("q6_value"));
                                    etqty7a.setText(lastOfferJSON.getString("q7_value"));
                                    etqty8a.setText(lastOfferJSON.getString("q8_value"));
                                }
                                if(category.equalsIgnoreCase("3")||category.equalsIgnoreCase("4")) {

                                    catstr = "spongeironpellets";

                                    sizerangeedtlinear.setVisibility(View.VISIBLE);
                                    sizerange1edt.setVisibility(View.VISIBLE);
                                    hypentxt.setVisibility(View.VISIBLE);
                                    sizerange2edt.setVisibility(View.VISIBLE);
                                    materialDesignSpinner2.setVisibility(View.INVISIBLE);
                                    sizerangetxt.setVisibility(View.VISIBLE);
                                    qualityparamsrel1.setVisibility(View.INVISIBLE);
                                    qualityparamsrel2.setVisibility(View.VISIBLE);


                                 //   Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this,"T 2",Toast.LENGTH_SHORT).show();
                                    sizerange1edt.setText(lastOfferJSON.getString("size_range_start"));
                                    sizerange2edt.setText(lastOfferJSON.getString("size_range_end"));

                                    String q1_value_str = lastOfferJSON.getString("q1_value");
                                    String[] q1_value_str_parts = q1_value_str.split("-");
                                    carbonc1bedt.setText(q1_value_str_parts[0]); // 004
                                    carbonc2bedt.setText(q1_value_str_parts[1]); // 034556

                                    etqty2b.setText(lastOfferJSON.getString("q2_value"));
                                    etqty3b.setText(lastOfferJSON.getString("q3_value"));

                                    String q4_value_str = lastOfferJSON.getString("q4_value");
                                    String[] q4_value_str_parts = q4_value_str.split("-");
                                    feintotal1edt.setText(q4_value_str_parts[0]); // 004
                                    feintotal2edt.setText(q4_value_str_parts[1]); // 034556

                                    String q5_value_str = lastOfferJSON.getString("q5_value");
                                    String[] q5_value_str_parts = q5_value_str.split("-");
                                    feinmetallic1edt.setText(q5_value_str_parts[0]); // 004
                                    feinmetallic2edt.setText(q5_value_str_parts[1]); // 034556


                                    String q6_value_str = lastOfferJSON.getString("q6_value");
                                    String[] q6_value_str_parts = q6_value_str.split("-");
                                    metallization1edt.setText(q6_value_str_parts[0]); // 004
                                    metallization2edt.setText(q6_value_str_parts[1]); // 034556

                                    etqty7b.setText(lastOfferJSON.getString("q7_value"));
                                    etqty8b.setText(lastOfferJSON.getString("q8_value"));
                                }
                            }
                            else
                            {
                                if (NetworkUtility.checkConnectivity(AddNewOfferLiveTradeEnhancementActivity0712.this)) {
                                    String GetProductMasterAutoFillUrl = APIName.URL + "/home/getProductMaster?category_id=" + category_id + "&category_grade_id=" + category_grade_id;
                                    System.out.println("GET PRODUCT MASTER AUTOFILL URL IS---" + GetProductMasterAutoFillUrl);
                                    GetProductMasterAutoFillAPI(GetProductMasterAutoFillUrl);

                                } else {
                                    util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "Please check your internet connection.");
                                }

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia1.dismiss();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia1.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                    /*    noliveoffertxt.setVisibility(View.VISIBLE);
                                        lv.setVisibility(View.GONE);*/

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();



                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void GetPromoAPI(String url) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(AddNewOfferLiveTradeEnhancementActivity0712.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF GET PROMO API IS---" + response);



                         JSONObject PromotionsJson = null;
                        try {
                            PromotionsJson = new JSONObject(response);


                            String statusstr = PromotionsJson.getString("status");


                            if (statusstr.equalsIgnoreCase("true"))
                            {
                                   PromotionsJSONArray = PromotionsJson.getJSONArray("promotions");
                                  System.out.println("PROMO JSON ARRAY  LENGTH IS 1---" + PromotionsJSONArray.length());

                                for(int i = 0; i < PromotionsJSONArray.length();i++)
                                {
                                    JSONArray promo_inner_array = PromotionsJSONArray.getJSONArray(i);
                                    System.out.println("PROMO INNER JSON ARRAY IS---" + promo_inner_array);

                                    for(int j = 0 ; j < promo_inner_array.length();j++)
                                    {
                                        String promocode = promo_inner_array.getJSONObject(j).getString("promo_code");
                                        System.out.println("PROMO CODE IS---" + promocode);


                                        promocodelist.add(promocode);



                                    }
                                }



                                Set<String> hs = new HashSet<>();
                                hs.addAll(promocodelist);
                                promocodelist.clear();
                                promocodelist.addAll(hs);

                                Collections.sort(promocodelist);
                                promocodefinallist.add("Select Promo");

                                for(int k = 0; k< promocodelist.size();k++)
                                {
                                    String promoccodenewstr = "Promo Code : "+promocodelist.get(k);


                                    promocodefinallist.add(promoccodenewstr);


                                }
                                System.out.println("PROMO CODE FINAL LIST IS---" + promocodefinallist);




                                ArrayAdapter<String> PromotionAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                                        android.R.layout.simple_dropdown_item_1line, promocodefinallist);

                                promotion_spinner.setAdapter(PromotionAdapter);
                                promo_code = " ";
                              //  promotion_spinner.setText(promocodefinallist.get(0));
                               // selectedpromocodestr = null;

                                source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");

                                promotion_spinner.setTypeface(source_sans_pro_normal);

                                promotion_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                        if(position == 0)
                                        {
                                            selectedpromocodestr = null;

                                            promo_code = " ";
                                            promo_code_edit = " ";

                                            promo_table.setVisibility(View.GONE);

                                            promotion_txt.setVisibility(View.VISIBLE);
                                            line6.setVisibility(View.VISIBLE);
                                            line7.setVisibility(View.GONE);


                                        }
                                       else {

                                            promo_table.setVisibility(View.VISIBLE);

                                            promotion_txt.setVisibility(View.GONE);
                                            line6.setVisibility(View.GONE);
                                            line7.setVisibility(View.VISIBLE);


                                            selectedpromocodestr = parent.getItemAtPosition(position).toString();
                                            System.out.println("SELECTED PROMO CODE ON ITEM CLICKED---" + selectedpromocodestr);

                                            String[] selectedpromocodestring = selectedpromocodestr.split(":");
                                            String promocodeval = selectedpromocodestring[1];


                                            promocodevalwithoutspaceonitemclick = promocodeval.replaceAll("\\s+", "");


                                            System.out.println("PROMO CODE VALUE without space in on item click---" + promocodevalwithoutspaceonitemclick);

                                            try {


                                                System.out.println("PROMO JSON ARRAY in on item click IS---" + PromotionsJSONArray.length());

                                                for (int i = 0; i < PromotionsJSONArray.length(); i++) {

                                                    System.out.println("PromotionsJSONArray length---" + i);

                                                    // JSONArray promo_inner_array = PromotionsJSONArray.getJSONArray(i);

                                                    JSONArray promo_inner_array = PromotionsJSONArray.getJSONArray(i);
                                                    System.out.println("PROMO INNER JSON ARRAY IS---" + promo_inner_array);

                                                    for (int j = 0; j < promo_inner_array.length(); j++) {
                                                        String promocode = promo_inner_array.getJSONObject(j).getString("promo_code");
                                                        System.out.println("PROMO CODE IS---" + promocode);


                                                        if (promocodevalwithoutspaceonitemclick.equalsIgnoreCase(promocode)) {
                                                            System.out.println("PROMO INNER JSON ARRAY IN ON ITEM CLICK IF EQUAL IS---" + promo_inner_array);

                                                            if (promo_inner_array.length() == 0) {
                                                                tr_header.setVisibility(View.GONE);
                                                                tr_item_1.setVisibility(View.GONE);
                                                                tr_item_2.setVisibility(View.GONE);
                                                                tr_item_3.setVisibility(View.GONE);
                                                                tr_item_4.setVisibility(View.GONE);
                                                                line.setVisibility(View.GONE);
                                                            }

                                                            if (promo_inner_array.length() == 1) {
                                                                tr_header.setVisibility(View.VISIBLE);
                                                                tr_item_1.setVisibility(View.VISIBLE);
                                                                tr_item_2.setVisibility(View.GONE);
                                                                tr_item_3.setVisibility(View.GONE);
                                                                tr_item_4.setVisibility(View.GONE);
                                                                line.setVisibility(View.VISIBLE);

                                                                tv_qtyfromdesc1.setText(promo_inner_array.getJSONObject(0).getString("from_qty"));
                                                                tv_qtytodesc1.setText(promo_inner_array.getJSONObject(0).getString("to_qty"));
                                                                tv_discdesc1.setText("Rs. " + promo_inner_array.getJSONObject(0).getString("discount_per_ton"));


                                                            }

                                                            if (promo_inner_array.length() == 2) {
                                                                tr_header.setVisibility(View.VISIBLE);
                                                                tr_item_1.setVisibility(View.VISIBLE);
                                                                tr_item_2.setVisibility(View.VISIBLE);
                                                                tr_item_3.setVisibility(View.GONE);
                                                                tr_item_4.setVisibility(View.GONE);
                                                                line.setVisibility(View.VISIBLE);

                                                                tv_qtyfromdesc1.setText(promo_inner_array.getJSONObject(0).getString("from_qty"));
                                                                tv_qtytodesc1.setText(promo_inner_array.getJSONObject(0).getString("to_qty"));
                                                                tv_discdesc1.setText("Rs. " + promo_inner_array.getJSONObject(0).getString("discount_per_ton"));

                                                                tv_qtyfromdesc2.setText(promo_inner_array.getJSONObject(1).getString("from_qty"));
                                                                tv_qtytodesc2.setText(promo_inner_array.getJSONObject(1).getString("to_qty"));
                                                                tv_discdesc2.setText("Rs. " + promo_inner_array.getJSONObject(1).getString("discount_per_ton"));
                                                            }

                                                            if (promo_inner_array.length() == 3) {
                                                                tr_header.setVisibility(View.VISIBLE);
                                                                tr_item_1.setVisibility(View.VISIBLE);
                                                                tr_item_2.setVisibility(View.VISIBLE);
                                                                tr_item_3.setVisibility(View.VISIBLE);
                                                                tr_item_4.setVisibility(View.GONE);
                                                                line.setVisibility(View.VISIBLE);

                                                                tv_qtyfromdesc1.setText(promo_inner_array.getJSONObject(0).getString("from_qty"));
                                                                tv_qtytodesc1.setText(promo_inner_array.getJSONObject(0).getString("to_qty"));
                                                                tv_discdesc1.setText("Rs. " + promo_inner_array.getJSONObject(0).getString("discount_per_ton"));

                                                                tv_qtyfromdesc2.setText(promo_inner_array.getJSONObject(1).getString("from_qty"));
                                                                tv_qtytodesc2.setText(promo_inner_array.getJSONObject(1).getString("to_qty"));
                                                                tv_discdesc2.setText("Rs. " + promo_inner_array.getJSONObject(1).getString("discount_per_ton"));

                                                                tv_qtyfromdesc3.setText(promo_inner_array.getJSONObject(2).getString("from_qty"));
                                                                tv_qtytodesc3.setText(promo_inner_array.getJSONObject(2).getString("to_qty"));
                                                                tv_discdesc3.setText("Rs. " + promo_inner_array.getJSONObject(2).getString("discount_per_ton"));
                                                            }

                                                            if (promo_inner_array.length() == 4) {
                                                                tr_header.setVisibility(View.VISIBLE);
                                                                tr_item_1.setVisibility(View.VISIBLE);
                                                                tr_item_2.setVisibility(View.VISIBLE);
                                                                tr_item_3.setVisibility(View.VISIBLE);
                                                                tr_item_4.setVisibility(View.VISIBLE);
                                                                line.setVisibility(View.VISIBLE);

                                                                tv_qtyfromdesc1.setText(promo_inner_array.getJSONObject(0).getString("from_qty"));
                                                                tv_qtytodesc1.setText(promo_inner_array.getJSONObject(0).getString("to_qty"));
                                                                tv_discdesc1.setText("Rs. " + promo_inner_array.getJSONObject(0).getString("discount_per_ton"));

                                                                tv_qtyfromdesc2.setText(promo_inner_array.getJSONObject(1).getString("from_qty"));
                                                                tv_qtytodesc2.setText(promo_inner_array.getJSONObject(1).getString("to_qty"));
                                                                tv_discdesc2.setText("Rs. " + promo_inner_array.getJSONObject(1).getString("discount_per_ton"));

                                                                tv_qtyfromdesc3.setText(promo_inner_array.getJSONObject(2).getString("from_qty"));
                                                                tv_qtytodesc3.setText(promo_inner_array.getJSONObject(2).getString("to_qty"));
                                                                tv_discdesc3.setText("Rs. " + promo_inner_array.getJSONObject(2).getString("discount_per_ton"));

                                                                tv_qtyfromdesc4.setText(promo_inner_array.getJSONObject(3).getString("from_qty"));
                                                                tv_qtytodesc4.setText(promo_inner_array.getJSONObject(3).getString("to_qty"));
                                                                tv_discdesc4.setText("Rs. " + promo_inner_array.getJSONObject(3).getString("discount_per_ton"));
                                                            }

                                                        }


                                                    }


                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }






                                    }
                                });
                            }
                          else
                            {

                                no_promotion_txt.setVisibility(View.VISIBLE);
                                promotion_spinner.setVisibility(View.INVISIBLE);
                                selectedpromocodestr = null;
                                promo_code = " ";
                                promo_code_edit = " ";
                                no_promotion_txt.setEnabled(false);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                    /*    noliveoffertxt.setVisibility(View.VISIBLE);
                                        lv.setVisibility(View.GONE);*/

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get promo master params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void GetProductMasterAPI(String url) {

        pdia = new ProgressDialog(AddNewOfferLiveTradeEnhancementActivity0712.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.setCancelable(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(AddNewOfferLiveTradeEnhancementActivity0712.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        pdia.dismiss();

                        materialcatarraylist.clear();
                        materialcatidarraylist.clear();
                        materialsubcatarraylist.clear();
                        categorygradearraylist.clear();
                        categorygradeidarraylist.clear();

                        System.out.println("RESPONSE OF GET PRODUCT MASTER API IS---" + response);


                        JSONObject GetProductMasterJson = null;
                        try {
                            GetProductMasterJson = new JSONObject(response);


                            String statusstr = GetProductMasterJson.getString("status");


                            if (statusstr.equalsIgnoreCase("true")) {

                                CategoryJsonArray = GetProductMasterJson.getJSONArray("category");
                                System.out.println("CATEGORY JSONARRAY IS---" + CategoryJsonArray);

                                SubCategoryJsonArray = GetProductMasterJson.getJSONArray("sub_category");
                                System.out.println("SUB-CATEGORY JSONARRAY IS---" + SubCategoryJsonArray);

                                CategoryGradeJsonArray = GetProductMasterJson.getJSONArray("category_grade");
                                System.out.println("CATEGORY GRADE JSONARRAY IS---" + CategoryGradeJsonArray);

                                final JSONArray ProductDetailJsonArray = GetProductMasterJson.getJSONArray("product_detail");
                                System.out.println("CATEGORY GRADE JSONARRAY IS---" + CategoryGradeJsonArray);

                                if (SingletonActivity.fromselltodaysoffer == true) {

                                    System.out.println("STF ---11");

                                    materialDesignSpinner1.setBackgroundColor(Color.parseColor("#e4e4e4"));
                                    materialDesignSpinner1.getText().clearSpans();
                                    materialDesignSpinner1.setText(categorydesc);
                                    materialDesignSpinner1.setEnabled(false);
                                    materialDesignSpinner1.setClickable(false);


                                } else {
                                    System.out.println("STF ---111");

                                    materialDesignSpinner1.setEnabled(true);
                                    materialDesignSpinner1.setClickable(true);

                                    for (int i = 0; i < CategoryJsonArray.length(); i++) {
                                        String categoryidfromcatjsonarrstr = CategoryJsonArray.getJSONObject(i).getString("category_id");
                                        String categorynamestr = CategoryJsonArray.getJSONObject(i).getString("category_name");


                                        materialcatarraylist.add(categorynamestr);
                                        materialcatidarraylist.add(categoryidfromcatjsonarrstr);
                                    }

                                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                                            android.R.layout.simple_dropdown_item_1line, materialcatarraylist);

                                    materialDesignSpinner1.setAdapter(arrayAdapter);
                                    materialDesignSpinner1.setClickable(true);

                                    materialDesignSpinner1.setTypeface(source_sans_pro_normal);


                                    System.out.println("STF ---1");


                                    ArrayAdapter<String> CategoryGradeAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                                            android.R.layout.simple_dropdown_item_1line, categorygradearraylist);

                                    materialDesignCategoryGradeSpinner.setAdapter(CategoryGradeAdapter);
                                    materialDesignCategoryGradeSpinner.setClickable(true);
                                    materialDesignCategoryGradeSpinner.setEnabled(true);


                                    //  pdia.dismiss();


                                    materialDesignSpinner1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                            materialsubcatarraylist.clear();
                                            categorygradearraylist.clear();
                                            categorygradeidarraylist.clear();

                                            selectedmatcatspinnerstr = "";

                                            selectedmatcatspinnerstr = parent.getItemAtPosition(position).toString();

                                            try {

                                                category_id = CategoryJsonArray.getJSONObject(position).getString("category_id");


                                                if (position == 0) {
                                                    category_id = CategoryJsonArray.getJSONObject(0).getString("category_id");
                                                    selectedsubcatspinnerstr = "";
                                                    sizerangeedtlinear.setVisibility(View.INVISIBLE);
                                                    materialDesignSpinner2.setVisibility(View.VISIBLE);
                                                    sizerangetxt.setVisibility(View.GONE);
                                                    lengthtxt.setVisibility(View.VISIBLE);
                                                    lengthtxt.setText("LENGTH(M) *");
                                                    etlength.setText("6");
                                                    etlength.setVisibility(View.VISIBLE);
                                                    qualityparamsrel1.setVisibility(View.VISIBLE);
                                                    qualityparamsrel2.setVisibility(View.INVISIBLE);

                                                    catstr = "billetsingots";


                                                }

                                                if (position == 1) {
                                                    category_id = CategoryJsonArray.getJSONObject(1).getString("category_id");
                                                    selectedsubcatspinnerstr = "";
                                                    sizerangeedtlinear.setVisibility(View.INVISIBLE);
                                                    materialDesignSpinner2.setVisibility(View.VISIBLE);
                                                    sizerangetxt.setVisibility(View.GONE);
                                                    lengthtxt.setVisibility(View.VISIBLE);
                                                    lengthtxt.setText("WEIGHT(kg) *");
                                                    etlength.setText("110");
                                                    etlength.setVisibility(View.VISIBLE);
                                                    qualityparamsrel1.setVisibility(View.VISIBLE);
                                                    qualityparamsrel2.setVisibility(View.INVISIBLE);

                                                    catstr = "billetsingots";


                                                }


                                                if (position == 2) {
                                                    category_id = CategoryJsonArray.getJSONObject(2).getString("category_id");
                                                    selectedsubcatspinnerstr = "";
                                                    sizerangeedtlinear.setVisibility(View.VISIBLE);
                                                    sizerange1edt.setVisibility(View.VISIBLE);
                                                    hypentxt.setVisibility(View.VISIBLE);
                                                    sizerange2edt.setVisibility(View.VISIBLE);
                                                    materialDesignSpinner2.setVisibility(View.INVISIBLE);
                                                    sizerangetxt.setVisibility(View.VISIBLE);
                                                    lengthtxt.setVisibility(View.GONE);
                                                    etlength.setVisibility(View.GONE);
                                                    etlength.setText("6");
                                                    qualityparamsrel1.setVisibility(View.INVISIBLE);
                                                    qualityparamsrel2.setVisibility(View.VISIBLE);

                                                    catstr = "spongeironpellets";


                                                }

                                                if (position == 3) {
                                                    category_id = CategoryJsonArray.getJSONObject(3).getString("category_id");
                                                    selectedsubcatspinnerstr = "";
                                                    sizerangeedtlinear.setVisibility(View.VISIBLE);
                                                    sizerange1edt.setVisibility(View.VISIBLE);
                                                    hypentxt.setVisibility(View.VISIBLE);
                                                    sizerange2edt.setVisibility(View.VISIBLE);
                                                    materialDesignSpinner2.setVisibility(View.INVISIBLE);
                                                    sizerangetxt.setVisibility(View.VISIBLE);
                                                    lengthtxt.setVisibility(View.GONE);
                                                    etlength.setVisibility(View.GONE);
                                                    etlength.setText("6");
                                                    qualityparamsrel1.setVisibility(View.INVISIBLE);
                                                    qualityparamsrel2.setVisibility(View.VISIBLE);

                                                    catstr = "spongeironpellets";


                                                }


                                                //For grade array---

                                                for (int j = 0; j < CategoryGradeJsonArray.length(); j++) {


                                                    categorygradenamejsonarrstr = CategoryGradeJsonArray.getJSONObject(j).getString("category_grade_name");


                                                    if (category_id.equalsIgnoreCase(CategoryGradeJsonArray.getJSONObject(j).getString("category_id"))) {
                                                        categorygradearraylist.add(categorygradenamejsonarrstr);
                                                        categorygradeidarraylist.add(CategoryGradeJsonArray.getJSONObject(j).getString("category_grade_id"));

                                                    }


                                                }

                                                materialsubcatarraylist.clear();
                                                //FOR SUB CATEGORY=====
                                                for (int j = 0; j < SubCategoryJsonArray.length(); j++) {


                                                    sub_category_id = SubCategoryJsonArray.getJSONObject(j).getString("sub_category_id");
                                                    String categoryidfromsubcatjsonarrstr = SubCategoryJsonArray.getJSONObject(j).getString("category_id");
                                                    subcatnamefromsubcatjsonarrstr = SubCategoryJsonArray.getJSONObject(j).getString("sub_category_name");


                                                    if (category_id.equalsIgnoreCase(SubCategoryJsonArray.getJSONObject(j).getString("category_id"))) {

                                                        materialsubcatarraylist.add(subcatnamefromsubcatjsonarrstr);
                                                        subcategoryidlist.add(SubCategoryJsonArray.getJSONObject(j).getString("sub_category_id"));
                                                    }


                                                }

                                                for (int p = 0; p < materialsubcatarraylist.size(); p++) {
                                                    for (int q = 0; q < SingletonActivity.SubCategoryArrayList.size(); q++) {
                                                        if (SingletonActivity.SubCategoryArrayList.get(q).equalsIgnoreCase(materialsubcatarraylist.get(p))) {
                                                            materialsubcatarraylist.remove(p);
                                                        }

                                                    }
                                                }


                                                System.out.println("SUB-CATEGORY id ARRAYLIST IS---" + subcategoryidlist);
                                                System.out.println("SINGLETON SUB-CATEGORY ARRAYLIST IS---" + SingletonActivity.SubCategoryArrayList);


                                                ArrayAdapter<String> CategoryGradeAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                                                        android.R.layout.simple_dropdown_item_1line, categorygradearraylist);
                                                materialDesignCategoryGradeSpinner = (MaterialBetterSpinner)
                                                        findViewById(R.id.grade_spinner);
                                                materialDesignCategoryGradeSpinner.setAdapter(CategoryGradeAdapter);
                                                materialDesignCategoryGradeSpinner.setText(categorygradearraylist.get(0));


                                                ArrayAdapter<String> SubCatArrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                                                        android.R.layout.simple_dropdown_item_1line, materialsubcatarraylist);
                                                materialDesignSpinner2 = (MaterialBetterSpinner)
                                                        findViewById(R.id.sub_category_spinner);
                                                materialDesignSpinner2.setAdapter(SubCatArrayAdapter);
                                                if (catstr.equalsIgnoreCase("billetsingots")) {
                                                    materialDesignSpinner2.setText(materialsubcatarraylist.get(0));
                                                    //subcategoryid = subcategoryidlist.get(0);
                                                }


                                                category_grade_id = categorygradeidarraylist.get(0);

                                                // GET PRODUCT MASTER---------------
                                                materialDesignSpinner2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                    @Override
                                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                                        subcategoryidlist.clear();

                                                        //FOR SUB CATEGORY=====
                                                        for (int j = 0; j < SubCategoryJsonArray.length(); j++) {


                                                            try {
                                                                sub_category_id = SubCategoryJsonArray.getJSONObject(j).getString("sub_category_id");

                                                                String categoryidfromsubcatjsonarrstr = SubCategoryJsonArray.getJSONObject(j).getString("category_id");
                                                                subcatnamefromsubcatjsonarrstr = SubCategoryJsonArray.getJSONObject(j).getString("sub_category_name");


                                                                if (category_id.equalsIgnoreCase(SubCategoryJsonArray.getJSONObject(j).getString("category_id"))) {

                                                                    materialsubcatarraylist.add(subcatnamefromsubcatjsonarrstr);
                                                                    subcategoryidlist.add(SubCategoryJsonArray.getJSONObject(j).getString("sub_category_id"));
                                                                }



                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }

                                                        subcategoryid = subcategoryidlist.get(i);
                                                      //  Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, "sub_category_id_is==" + subcategoryid, Toast.LENGTH_SHORT).show();

                                                    }
                                                });
                                                //================================================
                                                //===============================

                                                materialDesignCategoryGradeSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                    @Override
                                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                        category_grade_id = categorygradeidarraylist.get(position);

                                                        //  Toast.makeText(AddNewOfferLiveTradeEnhancementActivity.this,"category_grade_id is=="+ category_grade_id,Toast.LENGTH_SHORT).show();
                                                        //for autofill=========>
                                                        if (NetworkUtility.checkConnectivity(AddNewOfferLiveTradeEnhancementActivity0712.this)) {
                                                            String GetProductMasterAutoFillUrl = APIName.URL + "/home/getProductMaster?category_id=" + category_id + "&category_grade_id=" + category_grade_id;
                                                            System.out.println("GET PRODUCT MASTER AUTOFILL URL IS---" + GetProductMasterAutoFillUrl);
                                                            GetProductMasterAutoFillAPI(GetProductMasterAutoFillUrl);

                                                        } else {
                                                            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "Please check your internet connection.");
                                                        }

                                                        ArrayAdapter<String> SubCatArrayAdapter = new ArrayAdapter<String>(AddNewOfferLiveTradeEnhancementActivity0712.this,
                                                                android.R.layout.simple_dropdown_item_1line, materialsubcatarraylist);
                                                        materialDesignSpinner2 = (MaterialBetterSpinner)
                                                                findViewById(R.id.sub_category_spinner);
                                                        materialDesignSpinner2.setAdapter(SubCatArrayAdapter);

                                                        materialDesignSpinner2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                            @Override
                                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                                                subcategoryidlist.clear();

                                                                //FOR SUB CATEGORY=====
                                                                for (int j = 0; j < SubCategoryJsonArray.length(); j++) {


                                                                    try {
                                                                        sub_category_id = SubCategoryJsonArray.getJSONObject(j).getString("sub_category_id");

                                                                        String categoryidfromsubcatjsonarrstr = SubCategoryJsonArray.getJSONObject(j).getString("category_id");
                                                                        subcatnamefromsubcatjsonarrstr = SubCategoryJsonArray.getJSONObject(j).getString("sub_category_name");


                                                                        if (category_id.equalsIgnoreCase(SubCategoryJsonArray.getJSONObject(j).getString("category_id"))) {

                                                                            materialsubcatarraylist.add(subcatnamefromsubcatjsonarrstr);
                                                                            subcategoryidlist.add(SubCategoryJsonArray.getJSONObject(j).getString("sub_category_id"));
                                                                        }



                                                                    } catch (JSONException e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }

                                                                subcategoryid = subcategoryidlist.get(position);
                                                            }
                                                        });


                                                        if (catstr.equalsIgnoreCase("billetsingots")) {
                                                            materialDesignSpinner2.setText(materialsubcatarraylist.get(0));
                                                        }
                                                    }


                                                });

                                                if (SingletonActivity.fromselltodaysoffer == false) {

                                                    if (Integer.parseInt(lastPostedCountStr) > 0) {
                                                        if (NetworkUtility.checkConnectivity(AddNewOfferLiveTradeEnhancementActivity0712.this)) {
                                                            String GetLastPostedOfferUrl = APIName.URL + "/livetrading/getLastPostedOffer?user_code=" + userCodestr + "&category_Id=" + category_id;
                                                            System.out.println("GET LAST POSTED OFFER 1 URL IS---" + GetLastPostedOfferUrl);
                                                            GetLastPostedOffer1API(GetLastPostedOfferUrl);

                                                        } else {
                                                            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "Please check your internet connection.");
                                                        }
                                                    }
                                                }


                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }


                                        }
                                    });


                                }
                                if (SingletonActivity.fromselltodaysoffer == false) {
                                    if (Integer.parseInt(lastPostedCountStr) > 0) {
                                        if (NetworkUtility.checkConnectivity(AddNewOfferLiveTradeEnhancementActivity0712.this)) {
                                            String GetLastPostedOfferUrl = APIName.URL + "/livetrading/getLastPostedOffer?user_code=" + userCodestr;
                                            System.out.println("GET LAST POSTED OFFER URL IS---" + GetLastPostedOfferUrl);
                                            GetLastPostedOfferAPI(GetLastPostedOfferUrl);

                                        } else {
                                            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this, "Please check your internet connection.");
                                        }
                                    }
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            pdia.dismiss();
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                         pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                    /*    noliveoffertxt.setVisibility(View.VISIBLE);
                                        lv.setVisibility(View.GONE);*/

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
               /* params.put("mobile_num",mobilenum);
                params.put("password",pwd);
*/


                System.out.println("get product master params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(this,hurlStack);
        requestQueue.add(stringRequest);
    }

    private void InsertOfferAPI(final String url) {
     /*   pdia = new ProgressDialog(getActivity());
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();*/

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(AddNewOfferLiveTradeEnhancementActivity0712.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        System.out.println("RESPONSE OF ADD NEW OFFER==="+ response);

                       // Toast.makeText(AddNewOfferLiveTradeActivity.this,response,Toast.LENGTH_SHORT).show();


                        // pdia.dismiss();





                        try {



                            JSONObject insertofferjson = new JSONObject(response);
                            System.out.println("INSERT OFFER JSON IS---" + insertofferjson);

                            String statusstr = insertofferjson.getString("status");
                            String msgstr = insertofferjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                   Intent i = new Intent(AddNewOfferLiveTradeEnhancementActivity0712.this,HomeActivity.class);

                                SingletonActivity.fromaddnewoffer = true;
                                SingletonActivity.fromupdatenewoffer = false;
                                SingletonActivity.isNotificationClicked = false;
                                SingletonActivity.index = index;
                                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                SingletonActivity.FromAddNewOfferTabOne=false;
                                SingletonActivity.FromAddNewOfferTabZero=false;

                                SingletonActivity.fromviewlivetrade = false;
                                SingletonActivity.frombuycurrentenquiry = false;
                                SingletonActivity.frombackofsellerdispatch = false;
                                SingletonActivity.frombackofsellerpayment = false;
                                SingletonActivity.frombackofbuyerpayment = false;
                                SingletonActivity.frombackofbuyerdispatch = false;
                                SingletonActivity.isNewEnquiryClicked = false;

                                SingletonActivity.isSellerAcceptedClicked = false;
                                SingletonActivity.isSpTradeSellerClicked = false;
                                SingletonActivity.isSpTradeBuyerClicked = false;
                                SingletonActivity.fromsellerhistorywebview = false;
                                SingletonActivity.frombuyerhistorywebview = false;
                                SingletonActivity.fromselltodaysoffer = false;


                                   startActivity(i);


                             /*   if(SingletonActivity.validatetab==2)
                                {
                                    Intent i = new Intent(AddNewOfferLiveTradeActivity.this,SellActivity.class);
                                    startActivity(i);
                                }*/


                            }

                            else
                            {
                                Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, msgstr, Toast.LENGTH_SHORT).show();
                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            //  pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }





                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this,error.toString(),Toast.LENGTH_SHORT).show();
                        //     pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(AddNewOfferLiveTradeEnhancementActivity0712.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(AddNewOfferLiveTradeEnhancementActivity0712.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                 String promocodevalwithoutspace;
                System.out.println("SELECTED PROMO CODE STRING IS---"+ selectedpromocodestr);
                if(selectedpromocodestr!=null) {
                    String[] selectedpromocodestring = selectedpromocodestr.split(":");
                    String promocodeval = selectedpromocodestring[1];


                     promocodevalwithoutspace = promocodeval.replaceAll("\\s+", "");


                    System.out.println("PROMO CODE VALUE without space IN IF---" + promocodevalwithoutspace);
                }
                else
                {
                    promocodevalwithoutspace = " ";
                    System.out.println("PROMO CODE VALUE without space IN ELSEA---" + promo_code);

                    if (!promo_code.equalsIgnoreCase(" ")) {

                        promocodevalwithoutspace = promo_code;
                    }
                    else
                    {
                        promocodevalwithoutspace = " ";
                    }

                }




                params.put("promo_code",promocodevalwithoutspace);

                                         if(catstr.equalsIgnoreCase("billetsingots"))
                                          {

                                              params.put("user_code",userCodestr);
                                              params.put("mobile_num", mobilenumstr);
                                              params.put("category",category_id);
                                              params.put("sub_category",subcategoryid);
                                              params.put("category_grade",category_grade_id);
                                              params.put("advance_rate",advanceratedesctxtstr);
                                              params.put("next_day_rate",nextdayratedesctxtstr);
                                              params.put("regular_rate",regularratedesctxtstr);
                                              params.put("quantity",quantitytondesctxtstr);
                                              params.put("promo_code",promocodevalwithoutspace);
                                              params.put("sell_tandc", termsandtitledesctxtstr);
                                              params.put("q1_value1",carbonc1edt.getText().toString());
                                              params.put("q1_value2",carbonc2edt.getText().toString());
                                              params.put("q2_value1",manganese1edt.getText().toString());
                                              params.put("q2_value2",manganese2edt.getText().toString());
                                              params.put("q3_value1",silicon1edt.getText().toString());
                                              params.put("q3_value2",silicon2edt.getText().toString());
                                              params.put("q4_value",etqty4a.getText().toString());
                                              params.put("q5_value",etqty5a.getText().toString());
                                              params.put("q6_value",etqty6a.getText().toString());
                                              params.put("q7_value",etqty7a.getText().toString());
                                              params.put("q8_value",etqty8a.getText().toString());
                                              params.put("q1_title", "CARBON (C)*");
                                              params.put("q2_title", "MANGANESE (Mn)*");
                                              params.put("q3_title", "SILICON (Si)*");
                                              params.put("q4_title", "SULPHUR (S) (Max)*");
                                              params.put("q5_title", "PHOSPHORUS (P) (Max)*");
                                              params.put("q6_title", "S + P(Max)*");
                                              params.put("q7_title", "ALUMINIUM (Al)");
                                              params.put("q8_title", "NICKEL (Max)");
                                              params.put("transport", transport_spinner_str);
                                              params.put("loading", loadingedttxt.getText().toString());
                                              params.put("gst", gstedttxt.getText().toString());
                                              params.put("insurance_select",insurance_spinner_str);
                                              params.put("length",etlength.getText().toString());


                                              if(insurance_spinner_str.equalsIgnoreCase("select")) {

                                                  if (extra_insurance_spinner_str.equalsIgnoreCase("1")) {
                                                      params.put("insurance", extra_insurance_spinner_str);
                                                  }

                                                  if (extra_insurance_spinner_str.equalsIgnoreCase("2")) {
                                                      params.put("insurance", extra_insurance_spinner_str);
                                                  }
                                              }
                                            if(insurance_spinner_str.equalsIgnoreCase("NA"))
                                              {
                                                  extra_insurance_spinner_str = "";
                                              params.put("insurance",extra_insurance_spinner_str);
                                              }

                                              if(deliveryindays_spinner_str.equalsIgnoreCase("1"))
                                              {
                                                  params.put("delivery_type","1");
                                                 // params.put("delivery_in_days","0");
                                              }
                                              if(deliveryindays_spinner_str.equalsIgnoreCase("2"))
                                              {
                                                  params.put("delivery_type","2");
                                                  params.put("delivery_in_days",withindaysedt.getText().toString());
                                              }

                                              if(material_inspection_spinner_str.equalsIgnoreCase("1"))
                                              {
                                                  params.put("material_inspection_type","1");
                                              }
                                              if(material_inspection_spinner_str.equalsIgnoreCase("2"))
                                              {
                                                  params.put("material_inspection_type","2");
                                              }

                                              if(weighment_spinner_str.equalsIgnoreCase("1"))
                                              {
                                                  params.put("weightment_type","1");
                                              }
                                              if(weighment_spinner_str.equalsIgnoreCase("2"))
                                              {
                                                  params.put("weightment_type","2");
                                              }
                                              if(weighment_spinner_str.equalsIgnoreCase("3"))
                                              {
                                                  params.put("weightment_type","3");
                                              }
                                              if(test_certificate_spinner_str.equalsIgnoreCase("1"))
                                              {
                                                  params.put("test_certificate_type","1");
                                              }
                                              if(test_certificate_spinner_str.equalsIgnoreCase("2"))
                                              {
                                                  params.put("test_certificate_type","2");
                                              }




                                          }

                                          if(catstr.equalsIgnoreCase("spongeironpellets"))
                                          {

                                             params.put("user_code",userCodestr);
                                             params.put("mobile_num", mobilenumstr);
                                             params.put("category", category_id);
                                             params.put("size_range_start",sizerange1edt.getText().toString());
                                             params.put("size_range_end", sizerange2edt.getText().toString());
                                             params.put("category_grade",category_grade_id);
                                             params.put("advance_rate",advanceratedesctxtstr);
                                             params.put("next_day_rate",nextdayratedesctxtstr);
                                             params.put("regular_rate",regularratedesctxtstr);
                                             params.put("quantity",quantitytondesctxtstr);
                                             params.put("promo_code",promocodevalwithoutspace);
                                             params.put("sell_tandc",termsandtitledesctxtstr);
                                             params.put("q1_value1",carbonc1bedt.getText().toString());
                                             params.put("q1_value2",carbonc2bedt.getText().toString());
                                             params.put("q2_value",etqty2b.getText().toString());
                                             params.put("q3_value",etqty3b.getText().toString());
                                             params.put("q4_value1",feintotal1edt.getText().toString());
                                             params.put("q4_value2",feintotal2edt.getText().toString());
                                             params.put("q5_value1",feinmetallic1edt.getText().toString());
                                             params.put("q5_value2",feinmetallic2edt.getText().toString());
                                             params.put("q6_value1",metallization1edt.getText().toString());
                                             params.put("q6_value2",metallization2edt.getText().toString());
                                             params.put("q7_value",etqty7b.getText().toString());
                                             params.put("q8_value",etqty8b.getText().toString());
                                             params.put("q1_title","CARBON (C)");
                                             params.put("q2_title","SULPHUR (S) (Max)");
                                             params.put("q3_title","PHOSPHORUS (P) (Max)");
                                             params.put("q4_title","Fe in Total (Fe(T))");
                                             params.put("q5_title","Fe in Metallic (Fe(M))");
                                             params.put("q6_title","METALLIZATION (MTZ)");
                                             params.put("q7_title","NON MAGNETIC MATERIAL (Non Mag.)");
                                             params.put("q8_title","TOTAL SILICA AND ALUMINA (SiO2 + Al2O3)");
                                             params.put("transport",transport_spinner_str);
                                             params.put("loading",loadingedttxt.getText().toString());
                                             params.put("gst",gstedttxt.getText().toString());
                                             params.put("insurance_select", insurance_spinner_str);

                                              if(insurance_spinner_str.equalsIgnoreCase("select")) {

                                                  if (extra_insurance_spinner_str.equalsIgnoreCase("1")) {
                                                      params.put("insurance", extra_insurance_spinner_str);
                                                  }

                                                  if (extra_insurance_spinner_str.equalsIgnoreCase("2")) {
                                                      params.put("insurance", extra_insurance_spinner_str);
                                                  }
                                              }
                                              if(insurance_spinner_str.equalsIgnoreCase("NA"))
                                              {
                                                  extra_insurance_spinner_str = "";
                                                  params.put("insurance",extra_insurance_spinner_str);
                                              }

                                              if(deliveryindays_spinner_str.equalsIgnoreCase("1"))
                                              {
                                                  params.put("delivery_type","1");
                                                //  params.put("delivery_in_days","0");
                                              }
                                              if(deliveryindays_spinner_str.equalsIgnoreCase("2"))
                                              {
                                                  params.put("delivery_type","2");
                                                  params.put("delivery_in_days",withindaysedt.getText().toString());
                                              }

                                              if(material_inspection_spinner_str.equalsIgnoreCase("1"))
                                              {
                                                  params.put("material_inspection_type","1");
                                              }
                                              if(material_inspection_spinner_str.equalsIgnoreCase("2"))
                                              {
                                                  params.put("material_inspection_type","2");
                                              }

                                              if(weighment_spinner_str.equalsIgnoreCase("1"))
                                              {
                                                  params.put("weightment_type","1");
                                              }
                                              if(weighment_spinner_str.equalsIgnoreCase("2"))
                                              {
                                                  params.put("weightment_type","2");
                                              }
                                              if(weighment_spinner_str.equalsIgnoreCase("3"))
                                              {
                                                  params.put("weightment_type","3");
                                              }

                                              if(test_certificate_spinner_str.equalsIgnoreCase("1"))
                                              {
                                                  params.put("test_certificate_type","1");
                                              }
                                              if(test_certificate_spinner_str.equalsIgnoreCase("2"))
                                              {
                                                  params.put("test_certificate_type","2");
                                              }

                                          }




                System.out.println("insert offer params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(AddNewOfferLiveTradeEnhancementActivity0712.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
        //  return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                System.out.println("OVERFLOWMENU ITEM 1 CLICKED");

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("login",false);


                editor.clear();
                editor.commit();


                sessionManager.logoutUser(mobile_num,fcm_id);


                // sessionManager.logoutUser();

            /*    Intent i = new Intent(LeaveManagementActivity.this,LoginActivity.class);
                startActivity(i);
*/
                return true;
          /*  case R.id.overflowmenuitem2:
                System.out.println("OVERFLOWMENU ITEM 2 CLICKED");

                return true;
            case R.id.overflowmenuitem3:
                System.out.println("OVERFLOWMENU ITEM 3 CLICKED");

                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
