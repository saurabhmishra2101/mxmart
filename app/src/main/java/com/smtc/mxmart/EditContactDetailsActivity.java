package com.smtc.mxmart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Handler;

import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class EditContactDetailsActivity extends AppCompatActivity {

    Typeface source_sans_pro_normal;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    UtilsDialog util = new UtilsDialog();
    ProgressDialog pdia;
    String userCodestr,accuserid,salesuserid,dispatchuserid;
    String accnamedttxtstr,accphnnoedttxtstr,accmobnodttxtstr,accemaildttxtstr,salmktnamedttxtstr,salmktphnnoedttxtstr,salmktmobnodttxtstr,salmktemaildttxtstr;
    String  dispatchnamedttxtstr,dispatchphnnoedttxtstr,dispatchmobnodttxtstr,dispatchemaildttxtstr,mobilenumstr;
    ImageView backiv;
    SessionManager sessionManager;
    String mobile_num,fcm_id;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact_details);

        SharedPreferences prefs = EditContactDetailsActivity.this.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        mobile_num = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id",null);

       /* Toolbar toolbar = (Toolbar) findViewById(R.id.editcontactstoolbar);
        setSupportActionBar(toolbar);*/

        sessionManager = new SessionManager(getApplicationContext());


        backiv = (ImageView)findViewById(R.id.backicon);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(EditContactDetailsActivity.this,HomeActivity.class);

                SingletonActivity.backfromeditbusinessdetails = false;
                SingletonActivity.backfromeditpersonaldetails = false;
                SingletonActivity.fromviewlivetrade = false;
                SingletonActivity.fromaddnewoffer = false;
                SingletonActivity.isNotificationClicked = false;
                // SingletonActivity.index = index;
                SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                SingletonActivity.FromAddNewOfferTabOne=false;
                SingletonActivity.FromAddNewOfferTabZero=false;
                SingletonActivity.isNewEnquiryClicked = false;
                SingletonActivity.frombackofbuyerdispatch = false;
                SingletonActivity.frombackofsellerdispatch = false;
                SingletonActivity.frombackofbuyerpayment = false;
                SingletonActivity.frombackofsellerpayment = false;
                SingletonActivity.fromselltodaysoffer = false;
                SingletonActivity.backfromeditcontactdetails = true;
                startActivity(i);
            }
        });

        source_sans_pro_normal = Typeface.createFromAsset(getAssets(), "SourceSansPro/SourceSansPro-Regular.ttf");


        //Accounts----
        final EditText accnamedttxt = (EditText)findViewById(R.id.etNameAccountsEditContactDet);
        accnamedttxt.setTypeface(source_sans_pro_normal);

        final EditText accphnnoedttxt = (EditText)findViewById(R.id.etPhoneNoAccountsEditContactDet);
        accphnnoedttxt.setTypeface(source_sans_pro_normal);

        final EditText accmobnodttxt = (EditText)findViewById(R.id.etMobileNoAccountsEditContactDet);
        accmobnodttxt.setTypeface(source_sans_pro_normal);

        final EditText accemaildttxt = (EditText)findViewById(R.id.etEmailIdAccountsEditContactDet);
        accemaildttxt.setTypeface(source_sans_pro_normal);

        //Sales marketing----

        final EditText salmktnamedttxt = (EditText)findViewById(R.id.etNameAccountsEditContactDet1);
        salmktnamedttxt.setTypeface(source_sans_pro_normal);

        final EditText salmktphnnoedttxt = (EditText)findViewById(R.id.etPhoneNoAccountsEditContactDet1);
        salmktphnnoedttxt.setTypeface(source_sans_pro_normal);

        final EditText salmktmobnodttxt = (EditText)findViewById(R.id.etMobileNoAccountsEditContactDet1);
        salmktmobnodttxt.setTypeface(source_sans_pro_normal);

        final EditText salmktemaildttxt = (EditText)findViewById(R.id.etEmailIdAccountsEditContactDet1);
        salmktemaildttxt.setTypeface(source_sans_pro_normal);

        //Dispatch----

        final EditText dispatchnamedttxt = (EditText)findViewById(R.id.etNameAccountsEditContactDet2);
        dispatchnamedttxt.setTypeface(source_sans_pro_normal);

        final EditText dispatchphnnoedttxt = (EditText)findViewById(R.id.etPhoneNoAccountsEditContactDet2);
        dispatchphnnoedttxt.setTypeface(source_sans_pro_normal);

        final EditText dispatchmobnodttxt = (EditText)findViewById(R.id.etMobileNoAccountsEditContactDet2);
        dispatchmobnodttxt.setTypeface(source_sans_pro_normal);

        final EditText dispatchemaildttxt = (EditText)findViewById(R.id.etEmailIdAccountsEditContactDet2);
        dispatchemaildttxt.setTypeface(source_sans_pro_normal);

        RelativeLayout sbmteditcontactrelative = (RelativeLayout)findViewById(R.id.sbmteditcontactrelative);
        sbmteditcontactrelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                accnamedttxtstr = accnamedttxt.getText().toString();
                accphnnoedttxtstr = accphnnoedttxt.getText().toString();
                accmobnodttxtstr = accmobnodttxt.getText().toString();
                accemaildttxtstr = accemaildttxt.getText().toString();

                salmktnamedttxtstr = salmktnamedttxt.getText().toString();
                salmktphnnoedttxtstr = salmktphnnoedttxt.getText().toString();
                salmktmobnodttxtstr = salmktmobnodttxt.getText().toString();
                salmktemaildttxtstr = salmktemaildttxt.getText().toString();

                dispatchnamedttxtstr = dispatchnamedttxt.getText().toString();
                dispatchphnnoedttxtstr = dispatchphnnoedttxt.getText().toString();
                dispatchmobnodttxtstr = dispatchmobnodttxt.getText().toString();
                dispatchemaildttxtstr = dispatchemaildttxt.getText().toString();





                System.out.println("USER CODE IN EDIT CONTACT DETAILS FRAGMENT---"+ userCodestr);
                boolean invalid = false;

                if (accnamedttxt.getText().toString().trim().equals("")) {
                    invalid = true;
                    Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();
                }

              /*  else  if (!isValidLandlineNumber(accphnnoedttxt.getText().toString().trim())) {

                    invalid = true;
                   Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();


                }*/



                else  if (!isValidMobileNumber(accmobnodttxt.getText().toString().trim())) {

                    invalid = true;
                    Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();

                }

                else  if (!isValidEmail(accemaildttxt.getText().toString().trim())) {


                    invalid = true;
                    Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();

                }

                else  if (salmktnamedttxt.getText().toString().trim().equals("")) {
                    invalid = true;
                    Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();

                }

               /* else  if (!isValidLandlineNumber(salmktphnnoedttxt.getText().toString().trim())) {


                    invalid = true;
                    Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();

                }*/

                else  if (!isValidMobileNumber(salmktmobnodttxt.getText().toString().trim())) {

                    invalid = true;
                    Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();

                }

                else  if (!isValidEmail(salmktemaildttxt.getText().toString().trim())) {


                    invalid = true;
                    Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();

                }

                else  if (dispatchnamedttxt.getText().toString().trim().equals("")) {
                    invalid = true;
                    Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();


                }

             /*   else  if (!isValidLandlineNumber(dispatchphnnoedttxt.getText().toString().trim())) {


                    invalid = true;
                    Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();
                }*/

                else  if (!isValidMobileNumber(dispatchmobnodttxt.getText().toString().trim())) {

                    invalid = true;
                    Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();
                }

                else  if (!isValidEmail(dispatchemaildttxt.getText().toString().trim())) {


                    invalid = true;
                    Toast.makeText(EditContactDetailsActivity.this, "Please fill all the fields",
                            Toast.LENGTH_SHORT).show();

                }

                else if (invalid == false) {
                    if (NetworkUtility.checkConnectivity(EditContactDetailsActivity.this)) {
                        String editcontactdetailsurl = APIName.URL + "/user/insertUpdateConDetails?user_code=" + userCodestr + "&mobile_num=" + mobilenumstr;
                        System.out.println("EDIT CONTACT DETAILS URL IS---" + editcontactdetailsurl);
                        EditContactDetailsAPI(editcontactdetailsurl, userCodestr);

                    } else {
                        util.dialog(EditContactDetailsActivity.this, "Please check your internet connection.");
                    }
                }

            }
        });


        try {

            if (SingletonActivity.contactjsonArray!=null) {
                for (int i = 0; i < SingletonActivity.contactjsonArray.length(); i++) {

                    if (SingletonActivity.contactjsonArray.getJSONObject(i).getString("user_type").equalsIgnoreCase("22")) {

                        accuserid = SingletonActivity.contactjsonArray.getJSONObject(i).getString("id");

                        accnamedttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("first_name"));
                        accphnnoedttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("phone"));
                        accmobnodttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("mobile_num"));
                        accemaildttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("email"));
                    }

                    //Sales marketing----

                    if (SingletonActivity.contactjsonArray.getJSONObject(i).getString("user_type").equalsIgnoreCase("23")) {
                        salesuserid = SingletonActivity.contactjsonArray.getJSONObject(i).getString("id");

                        salmktnamedttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("first_name"));
                        salmktphnnoedttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("phone"));
                        salmktmobnodttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("mobile_num"));
                        salmktemaildttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("email"));


                    }


                    //Dispatch----
                    if (SingletonActivity.contactjsonArray.getJSONObject(i).getString("user_type").equalsIgnoreCase("21")) {

                        dispatchuserid = SingletonActivity.contactjsonArray.getJSONObject(i).getString("id");

                        dispatchnamedttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("first_name"));
                        dispatchphnnoedttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("phone"));
                        dispatchmobnodttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("mobile_num"));
                        dispatchemaildttxt.setText(SingletonActivity.contactjsonArray.getJSONObject(i).getString("email"));
                    }

                }
            }
            else
            {
                accuserid = "";
                salesuserid = "";
                dispatchuserid = "";

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent i = new Intent(EditContactDetailsActivity.this,HomeActivity.class);

        SingletonActivity.backfromeditbusinessdetails = false;
        SingletonActivity.backfromeditpersonaldetails = false;
        SingletonActivity.fromviewlivetrade = false;
        SingletonActivity.fromaddnewoffer = false;
        SingletonActivity.isNotificationClicked = false;
        // SingletonActivity.index = index;
        SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
        SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
        SingletonActivity.FromAddNewOfferTabOne=false;
        SingletonActivity.FromAddNewOfferTabZero=false;
        SingletonActivity.isNewEnquiryClicked = false;
        SingletonActivity.frombackofbuyerdispatch = false;
        SingletonActivity.frombackofsellerdispatch = false;
        SingletonActivity.frombackofbuyerpayment = false;
        SingletonActivity.frombackofsellerpayment = false;
        SingletonActivity.fromselltodaysoffer = false;
        SingletonActivity.backfromeditcontactdetails = true;
        startActivity(i);
    }

    public final  boolean isValidEmail(String email) {
        if (email == null) {

            Toast.makeText(EditContactDetailsActivity.this,
                    "Please Enter Valid Email", Toast.LENGTH_SHORT)
                    .show();


            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }


    public  final boolean isValidMobileNumber(String mobile) {
        if (mobile.length()!=10) {

            Toast.makeText(EditContactDetailsActivity.this,
                    "Please Enter Valid Mobile No.", Toast.LENGTH_SHORT)
                    .show();

            return false;
        } else {
            return true;
        }
    }

    public  final boolean isValidLandlineNumber(String landline) {
        //if (landline.length() != 10 || landline.length() != 11 || landline.length() != 12) {
            if(landline.length() < 10 || landline.length() > 12){

            Toast.makeText(EditContactDetailsActivity.this,
                    "Please Enter Valid Phone No.", Toast.LENGTH_SHORT)
                    .show();

            return false;
        } else {
            return true;
        }
    }

    private void EditContactDetailsAPI(final String url,final String usercode) {
        pdia = new ProgressDialog(EditContactDetailsActivity.this);
        pdia.setMessage("Please Wait...");
        pdia.setCanceledOnTouchOutside(false);
        pdia.show();

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(EditContactDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };


        System.out.println("EDIT CONTACT URL in resp IS---"+ url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pdia.dismiss();



                        System.out.println("RESPONSE OF EDIT CONTACT API IS---" + response);



                        try {



                            JSONObject editcontactjson = new JSONObject(response);
                            System.out.println("EDIT CONTACT JSON IS---" + editcontactjson);

                            String statusstr = editcontactjson.getString("status");
                            String msgstr = editcontactjson.getString("message");

                            if(statusstr.equalsIgnoreCase("true"))
                            {
                                Toast.makeText(EditContactDetailsActivity.this,msgstr,Toast.LENGTH_SHORT).show();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {


                                        Intent i = new Intent(EditContactDetailsActivity.this, HomeActivity.class);
                                        SingletonActivity.backfromeditcontactdetails = true;
                                        SingletonActivity.backfromeditbusinessdetails = false;
                                        SingletonActivity.backfromeditpersonaldetails = false;
                                        SingletonActivity.fromviewlivetrade = false;
                                        SingletonActivity.fromaddnewoffer = false;
                                        SingletonActivity.isNotificationClicked = false;
                                        // SingletonActivity.index = index;
                                        SingletonActivity.FromAddNewOfferTabTwoBuyRej=false;
                                        SingletonActivity.FromAddNewOfferTabTwoNewEnq=false;
                                        SingletonActivity.FromAddNewOfferTabOne=false;
                                        SingletonActivity.FromAddNewOfferTabZero=false;
                                        SingletonActivity.isNewEnquiryClicked = false;
                                        SingletonActivity.frombackofbuyerdispatch = false;
                                        SingletonActivity.frombackofsellerdispatch = false;
                                        SingletonActivity.frombackofbuyerpayment = false;
                                        SingletonActivity.frombackofsellerpayment = false;


                                        startActivity(i);


                                    }
                                }, 2000);
                            }



                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            pdia.dismiss();
                            e.printStackTrace();
                            //  Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditContactDetailsActivity.this,error.toString(),Toast.LENGTH_SHORT).show();
                        pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("ERROR CODE--------" + networkResponse.statusCode);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {
                                        Toast.makeText(EditContactDetailsActivity.this, messagestr, Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(EditContactDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(EditContactDetailsActivity.this,"Some Error Occured,Please try after some time");
                        }
                        //Toast.makeText(SetPasswordActivity.this, "Server not responding,Please try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("act_user_id",accuserid);
                params.put("act_user_name",accnamedttxtstr);
                params.put("act_user_phone",accphnnoedttxtstr);
                params.put("act_user_mobile",accmobnodttxtstr);
                params.put("act_user_email",accemaildttxtstr);

                params.put("sales_user_id",salesuserid);
                params.put("sales_user_name",salmktnamedttxtstr);
                params.put("sales_user_phone",salmktphnnoedttxtstr);
                params.put("sales_user_mobile",salmktmobnodttxtstr);
                params.put("sales_user_email",salmktemaildttxtstr);

                params.put("dispatch_user_id",dispatchuserid);
                params.put("dispatch_user_name",dispatchnamedttxtstr);
                params.put("dispatch_user_phone",dispatchphnnoedttxtstr);
                params.put("dispatch_user_mobile",dispatchmobnodttxtstr);
                params.put("dispatch_user_email",dispatchemaildttxtstr);




                System.out.println("edit contact details params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(EditContactDetailsActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
        //  return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                System.out.println("OVERFLOWMENU ITEM 1 CLICKED");

                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putBoolean("login",false);


                editor.clear();
                editor.commit();

                sessionManager.logoutUser(mobile_num,fcm_id);


                // sessionManager.logoutUser();

            /*    Intent i = new Intent(LeaveManagementActivity.this,LoginActivity.class);
                startActivity(i);
*/
                return true;
          /*  case R.id.overflowmenuitem2:
                System.out.println("OVERFLOWMENU ITEM 2 CLICKED");

                return true;
            case R.id.overflowmenuitem3:
                System.out.println("OVERFLOWMENU ITEM 3 CLICKED");

                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
