package com.smtc.mxmart;




import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.smtc.mxmart.Fragment.SellCurrentDealsFragment;
import com.smtc.mxmart.Fragment.SellCurrentEnquiriesFragment;
import com.smtc.mxmart.Fragment.SellHistoryFragment;
import com.smtc.mxmart.Fragment.SellTodaysOfferFragment;
import com.smtc.mxmart.Fragment.SellTodaysOfferFragment1012;

/**
 * Created by 10161 on 5/3/2017.
 */


public class SellTabsAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public SellTabsAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
              //  SellTodaysOfferFragment tab1 = new SellTodaysOfferFragment();
                SellTodaysOfferFragment1012 tab1 = new SellTodaysOfferFragment1012();
                return tab1;

          case 1:

              SellCurrentEnquiriesFragment tab2 = new SellCurrentEnquiriesFragment();
                return tab2;


            case 2:
                SellCurrentDealsFragment tab3 = new SellCurrentDealsFragment();
                return tab3;



            case 3:
                SellHistoryFragment tab4 = new SellHistoryFragment();
                return tab4;



            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}