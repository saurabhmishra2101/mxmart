package com.smtc.mxmart;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by 10161 on 8/21/2017.
 */

public class SellerPaymentDetailsActivity extends AppCompatActivity {

    ImageView backiv;
    SessionManager sessionManager;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    UtilsDialog util = new UtilsDialog();
    ImageView empty_payment_iv;
    TextView nopaymentdetailstxtvw,clickonplusbtntxtvw;
    ImageView addpaymentiv;
    ProgressDialog pdia;
    String userCodestr,mobilenumstr,paymentdate;
    AlertDialog b;
    ImageView deletepaymentiv;
    int selected = 0;
    ArrayList<String> paymentIdArray = new ArrayList<String>();
    CustomAdap customadap;
    ListView sellerpaymentlistvw;
    int mYear,mMonth,mDay;
    String paymentchqnum,datestr,banknamestr,amtstr;
    TextView notification_total_count;
    String commentStr,tradeStr;
    TextView todays_offer_count,new_enquiry_count,seller_accepted_count,seller_rejected_count,sp_generated_count,buyer_rejected_count;
    int index;
    String fcm_id,selldeals;
    JSONArray payment_resolved_jsonarray;
    JSONArray GetPaymentDetailsJsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seller_payment_details);
        notification_total_count = (TextView)findViewById(R.id.notification_total_count);


        System.out.println("SELLER PAYMENT DETAILS JSONARRAY---"+ SingletonActivity.SellerpaymentDetailsJSONArray);
        System.out.println("SELLER PAYMENT TRADE ID---"+ SingletonActivity.tradeid);

       nopaymentdetailstxtvw = (TextView)findViewById(R.id.nopaymentdetailstxtvw);
        clickonplusbtntxtvw = (TextView)findViewById(R.id.clickonplusbtntxtvw);
        empty_payment_iv = (ImageView) findViewById(R.id.empty_payment_iv);


        sellerpaymentlistvw = (ListView)findViewById(R.id.sellerpaymentlistvw);


        sessionManager = new SessionManager(getApplicationContext());

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        userCodestr = prefs.getString("user_code", null);
        mobilenumstr = prefs.getString("mobile_num", null);
        fcm_id = prefs.getString("fcm_id",null);
        selldeals = prefs.getString("selldeals",null);


      /*  Toolbar toolbar = (Toolbar)findViewById(R.id.sellerpaymentdetailstoolbar);
        setSupportActionBar(toolbar);*/

        backiv = (ImageView)findViewById(R.id.back_payment);
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // finish();
                Intent i = new Intent(SellerPaymentDetailsActivity.this,HomeActivity.class);
                SingletonActivity.frombackofbuyerdispatch = false;
                SingletonActivity.fromsellcurrentdeals = false;
                SingletonActivity.frombuycurrentdeals = false;
                SingletonActivity.frombackofsellerdispatch = true;
                startActivity(i);
            }
        });


//        System.out.println("BUYER PAYMENT DETAILS JSONARRAY LENGTH---"+ SingletonActivity.SellerpaymentDetailsJSONArray.length());

        if(SingletonActivity.fromhometopayment == false) {
            if (SingletonActivity.SellerpaymentDetailsJSONArray.length() == 0) {
                empty_payment_iv.setVisibility(View.VISIBLE);
                nopaymentdetailstxtvw.setVisibility(View.VISIBLE);
                sellerpaymentlistvw.setVisibility(View.GONE);

            } else {
                empty_payment_iv.setVisibility(View.GONE);
                nopaymentdetailstxtvw.setVisibility(View.GONE);
                clickonplusbtntxtvw.setVisibility(View.GONE);
                sellerpaymentlistvw.setVisibility(View.VISIBLE);

            /*customadap = new CustomAdap(SellerPaymentDetailsActivity.this,SingletonActivity.SellerpaymentDetailsJSONArray);
            sellerpaymentlistvw.setAdapter(customadap);
*/

                if (NetworkUtility.checkConnectivity(SellerPaymentDetailsActivity.this)) {
                    String GetSellerPaymentDetailsURL = APIName.URL + "/seller/getCurrentDealsByTrade?user_code=" + userCodestr + "&tradeId=" + SingletonActivity.tradeid;
                    System.out.println("GET SELLER PAYMENT DETAILS URL IS---" + GetSellerPaymentDetailsURL);
                    GetSellerPaymentDetailsAPI(GetSellerPaymentDetailsURL);

                } else {
                    util.dialog(SellerPaymentDetailsActivity.this, "Please check your internet connection.");
                }

            }
        }
        else
        {
            empty_payment_iv.setVisibility(View.GONE);
            nopaymentdetailstxtvw.setVisibility(View.GONE);
            clickonplusbtntxtvw.setVisibility(View.GONE);
            sellerpaymentlistvw.setVisibility(View.VISIBLE);

            /*customadap = new CustomAdap(SellerPaymentDetailsActivity.this,SingletonActivity.SellerpaymentDetailsJSONArray);
            sellerpaymentlistvw.setAdapter(customadap);
*/

            if (NetworkUtility.checkConnectivity(SellerPaymentDetailsActivity.this)) {
                String GetSellerPaymentDetailsURL = APIName.URL + "/seller/getCurrentDealsByTrade?user_code=" + userCodestr + "&tradeId=" + SingletonActivity.tradeid;
                System.out.println("GET SELLER PAYMENT DETAILS URL IS---" + GetSellerPaymentDetailsURL);
                GetSellerPaymentDetailsAPI(GetSellerPaymentDetailsURL);

            } else {
                util.dialog(SellerPaymentDetailsActivity.this, "Please check your internet connection.");
            }
        }


    }


    @Override
    public void onBackPressed() {

        Intent i = new Intent(SellerPaymentDetailsActivity.this,HomeActivity.class);

        SingletonActivity.frombackofbuyerdispatch = false;
        SingletonActivity.fromsellcurrentdeals = false;
        SingletonActivity.frombuycurrentdeals = false;
        SingletonActivity.frombackofsellerdispatch = true;

        startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();


    }


    private void GetSellerPaymentDetailsAPI(String url) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SellerPaymentDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF GET SELLER PAYMENT DETAILS API IS---" + response);



                        JSONObject GetPaymentDetailsJson = null;
                        try {
                            GetPaymentDetailsJson = new JSONObject(response);


                            String statusstr = GetPaymentDetailsJson.getString("status");
                            System.out.println("STATUS OF GET SELLER PAYMENT DETAILS JSON IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {

                                GetPaymentDetailsJsonArray = GetPaymentDetailsJson.getJSONArray("cd_Offer");
                                SingletonActivity.SellerpaymentDetailsJSONArray = GetPaymentDetailsJsonArray.getJSONObject(0).getJSONArray("payment_details");




                                customadap = new CustomAdap(SellerPaymentDetailsActivity.this,SingletonActivity.SellerpaymentDetailsJSONArray);
                                sellerpaymentlistvw.setAdapter(customadap);

                            }
                            else
                            {
                                Toast.makeText(SellerPaymentDetailsActivity.this,GetPaymentDetailsJson.getString("message"),Toast.LENGTH_SHORT).show();
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SellerPaymentDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(SellerPaymentDetailsActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();





                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(SellerPaymentDetailsActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }




    private class CustomAdap extends BaseAdapter {
        private ArrayList<String> data;
        private Context c;
        private LayoutInflater inflater = null;
        private JSONArray SellerPaymentDetailsJSONArray;



        // public CustomAdap(Context mainActivity)
        public CustomAdap(Context mainActivity,JSONArray SellerPaymentDetailsJSONArray)
        {
            // TODO Auto-generated constructor stub
            //  this.data = leavetypelist;
            this.c = mainActivity;
            this.SellerPaymentDetailsJSONArray = SellerPaymentDetailsJSONArray;

            inflater = (LayoutInflater) c
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            // return 7;
            return SellerPaymentDetailsJSONArray.length();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            // TODO Auto-generated method stub



             final Holder holder = new Holder();

            final View rowView;


            rowView = inflater.inflate(R.layout.seller_payment_row, null);


            holder.date = (TextView)rowView.findViewById(R.id.date);
            holder.paymenttypetxt = (TextView)rowView.findViewById(R.id.paymenttypetxt);
            holder.utrchqnumdesctxtvw = (TextView)rowView.findViewById(R.id.utrchqnumdesctxtvw);
            holder.banknamedesctxtvw = (TextView)rowView.findViewById(R.id.banknamedesctxtvw);
            holder.amtdesctxtvw = (TextView)rowView.findViewById(R.id.amtdesctxtvw);
            holder.acktxtvw = (TextView)rowView.findViewById(R.id.acktxtvw);
            holder.disputetxtvw = (TextView)rowView.findViewById(R.id.disputetxtvw);
            holder.disputeiv = (ImageView)rowView.findViewById(R.id.disputeiv);

            try {
                if((SellerPaymentDetailsJSONArray.getJSONObject(position).getString("ack_status").equalsIgnoreCase("1"))||(SellerPaymentDetailsJSONArray.getJSONObject(position).getString("is_active").equalsIgnoreCase("9")))
                {
                    holder.acktxtvw.setEnabled(false);
                    holder.disputetxtvw.setEnabled(false);
                    holder.acktxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                    holder.disputetxtvw.setTextColor(Color.parseColor("#d3d3d3"));
                }
                else
                {
                    holder.acktxtvw.setEnabled(true);
                    holder.disputetxtvw.setEnabled(true);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                holder.date.setText(SellerPaymentDetailsJSONArray.getJSONObject(position).getString("payment_ref_date"));
                holder.paymenttypetxt.setText("Payment Type: "+ SellerPaymentDetailsJSONArray.getJSONObject(position).getString("invoice_type"));

                if(!SellerPaymentDetailsJSONArray.getJSONObject(position).getString("payment_ref_number").equalsIgnoreCase("null"))
                {
                    holder.utrchqnumdesctxtvw.setText(SellerPaymentDetailsJSONArray.getJSONObject(position).getString("payment_ref_number"));
                }
                else
                {
                    holder.utrchqnumdesctxtvw.setText("--");
                }

                if(!SellerPaymentDetailsJSONArray.getJSONObject(position).getString("bank_name").equalsIgnoreCase("null")) {
                    holder.banknamedesctxtvw.setText(SellerPaymentDetailsJSONArray.getJSONObject(position).getString("bank_name"));
                }
                else
                {
                    holder.banknamedesctxtvw.setText("--");
                }

                holder.amtdesctxtvw.setText(SellerPaymentDetailsJSONArray.getJSONObject(position).getString("amount"));

                final String tradeid = SellerPaymentDetailsJSONArray.getJSONObject(position).getString("trade_id");
                final String sellid = SingletonActivity.sellid;
                final String autopaymentid = SellerPaymentDetailsJSONArray.getJSONObject(position).getString("auto_payment_id");
                final  String buyerusercode = SingletonActivity.buyerusercode;
                final String buyermobilenum = SingletonActivity.buyermobilenum;
                final String sellerusercode =  userCodestr ;
                final String sellermobilenum = mobilenumstr;



                holder.acktxtvw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(selldeals.equalsIgnoreCase("0")) {



                            if (NetworkUtility.checkConnectivity(SellerPaymentDetailsActivity.this)) {
                                holder.acktxtvw.setEnabled(false);
                                String AcknowledgeSellerPaymentURL = APIName.URL + "/seller/ackPayment";
                                System.out.println("ACKNOWLEDGE SELLER PAYMENT URL IS---" + AcknowledgeSellerPaymentURL);
                                AcknowledgeSellerPaymentAPI(AcknowledgeSellerPaymentURL, tradeid, sellid, autopaymentid, buyerusercode, buyermobilenum, sellerusercode, sellermobilenum);

                            } else {
                                util.dialog(SellerPaymentDetailsActivity.this, "Please check your internet connection.");
                            }
                        }
                        else
                        {
                            Toast.makeText(SellerPaymentDetailsActivity.this, "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                holder.disputetxtvw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(selldeals.equalsIgnoreCase("0")) {


                            DisputeDialog(tradeid, autopaymentid, buyerusercode, buyermobilenum, sellerusercode, sellermobilenum);
                        }
                        else
                        {
                            Toast.makeText(SellerPaymentDetailsActivity.this, "User don't have permission to perform action. Please contact Administrator for more details.", Toast.LENGTH_SHORT).show();
                        }


                    }
                });


                try {

                    // Toast.makeText(BuyerPaymentDetailsActivity.this,"payment_resolved_jsonarray RESOLUTION LENGTH ==" + payment_resolved_jsonarray.length(),Toast.LENGTH_SHORT).show();

                    // String payment_resolved_str = payment_resolved_jsonarray.getJSONObject(position).getString("payment_resolved");
                    payment_resolved_jsonarray = GetPaymentDetailsJsonArray.getJSONObject(0).getJSONArray("payment_resolved");



                    if (payment_resolved_jsonarray.length() > 0) {

                        holder.disputeiv.setVisibility(View.VISIBLE);

                        //Toast.makeText(BuyerPaymentDetailsActivity.this,"DISPUTE RESOLUTION LENGTH ==" +payment_resolved_jsonarray.getJSONObject(0).getString("dispute_resolution_comment").length(),Toast.LENGTH_SHORT).show();
                        for(int i = 0 ; i < payment_resolved_jsonarray.length();i++) {
                            if(SellerPaymentDetailsJSONArray.getJSONObject(position).getString("auto_payment_id").equalsIgnoreCase(payment_resolved_jsonarray.getJSONObject(i).getString("auto_payment_id"))) {
                                holder.disputeiv.setVisibility(View.VISIBLE);
                                if (payment_resolved_jsonarray.getJSONObject(i).getString("flag").equalsIgnoreCase("1")) {
                                    holder.disputeiv.setBackgroundResource(R.mipmap.dispute_resolved_orange_info);
                                } else {
                                    holder.disputeiv.setBackgroundResource(R.mipmap.dispute_raised_exclamatory);
                                }

                                final int disputePos = i ;

                                holder.disputeiv.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        try {
                                            String dispute_raised_comment = payment_resolved_jsonarray.getJSONObject(disputePos).getString("dispute_comment");
                                            String dispute_resolution_comment = payment_resolved_jsonarray.getJSONObject(disputePos).getString("dispute_resolution_comment");
                                            String pos = Integer.toString(position);

                                            DisputeDetailsPaymentDialog(dispute_raised_comment, dispute_resolution_comment, pos);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }
                                });
                            }
                            /*else
                            {
                                holder.disputeiv.setVisibility(View.GONE);
                            }*/
                        }


                    } else {
                        holder.disputeiv.setVisibility(View.GONE);
                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }





            } catch (JSONException e) {
                e.printStackTrace();
            }






            return rowView;

        }


    }

    public void DisputeDetailsPaymentDialog(String dispute_raised_comment, String dispute_resolution_comment, String pos) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SellerPaymentDetailsActivity.this);
        LayoutInflater inflater = SellerPaymentDetailsActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.seller_dispatch_dispute_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);


        TextView disputeraiseddesctxtvw = (TextView) dialogView.findViewById(R.id.disputeraiseddesctxtvw);
        TextView resolvedcommenttxtvw = (TextView) dialogView.findViewById(R.id.resolvedcommenttxtvw);
        TextView resolvedcommentdesctxtvw = (TextView) dialogView.findViewById(R.id.resolvedcommentdesctxtvw);

        TextView closedisputetv = (TextView) dialogView.findViewById(R.id.closetxtvw);
        RelativeLayout closerelative = (RelativeLayout) dialogView.findViewById(R.id.closerelative);

        disputeraiseddesctxtvw.setText(dispute_raised_comment);


        int position = Integer.parseInt(pos);
        try {
            if (payment_resolved_jsonarray.getJSONObject(position).getString("flag").equalsIgnoreCase("1"))  {
                resolvedcommenttxtvw.setVisibility(View.VISIBLE);
                resolvedcommentdesctxtvw.setVisibility(View.VISIBLE);
                resolvedcommentdesctxtvw.setText(dispute_resolution_comment);
            } else {
                resolvedcommenttxtvw.setVisibility(View.GONE);
                resolvedcommentdesctxtvw.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        b = dialogBuilder.create();
        b.show();


        closerelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }


    public void DisputeDialog(final String tradeid,final String autopaymentid,final String buyerusercode,final String buyermobilenum,final String sellerusercode,final String sellermobilenum){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SellerPaymentDetailsActivity.this);
        LayoutInflater inflater = SellerPaymentDetailsActivity.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.payment_dispute_dialog, null);
        dialogBuilder.setView(dialogView);


        //final EditText dispatch_date_edt = (EditText)dialogView.findViewById(R.id.dispatch_date_edt);



        final EditText commentdesctxt = (EditText) dialogView.findViewById(R.id.commentdesctxt);
        final TextView addtxtvw = (TextView)dialogView.findViewById(R.id.addtxtvw);
        TextView canceltxtvw = (TextView)dialogView.findViewById(R.id.canceltxtvw);


        addtxtvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String comment = commentdesctxt.getText().toString();



                boolean invalid = false;

                if (comment.equals("")) {
                    invalid = true;
                    Toast.makeText(getApplicationContext(), "Please fill the comment",
                            Toast.LENGTH_SHORT).show();
                }


                else if (invalid == false)
                {


                    if(NetworkUtility.checkConnectivity(SellerPaymentDetailsActivity.this)){
                        addtxtvw.setEnabled(false);
                        String raisedisputepaymenturl = APIName.URL+"/seller/addPaymentDispute";
                        System.out.println("RAISE DISPUTE PAYMENT URL IS---"+ raisedisputepaymenturl);
                        RaiseDisputePaymentAPI(raisedisputepaymenturl,tradeid,autopaymentid,buyerusercode,buyermobilenum,sellerusercode,sellermobilenum,comment);

                    }
                    else{
                        util.dialog(SellerPaymentDetailsActivity.this, "Please check your internet connection.");
                    }


                }

            }
        });



        b = dialogBuilder.create();
        b.show();


        canceltxtvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });


    }

    private void RaiseDisputePaymentAPI(String url,final String tradeid,final String autopaymentid,final String buyerusercode,final String buyermobilenum,final String sellerusercode,final String sellermobilenum,final String comment) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SellerPaymentDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF RAISE DISPUTE PAYMENT API IS---" + response);



                        JSONObject PaymentDisputeJson = null;
                        try {
                            PaymentDisputeJson = new JSONObject(response);


                            String statusstr = PaymentDisputeJson.getString("status");
                            System.out.println("STATUS OF RAISE DISPUTE PAYMENT API IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {


                                Toast.makeText(SellerPaymentDetailsActivity.this,PaymentDisputeJson.getString("message"),Toast.LENGTH_SHORT).show();

                                b.dismiss();

                                if (NetworkUtility.checkConnectivity(SellerPaymentDetailsActivity.this)) {
                                    String GetSellerPaymentDetailsURL = APIName.URL + "/seller/getCurrentDealsByTrade?user_code=" + userCodestr + "&tradeId=" + SingletonActivity.tradeid;
                                    System.out.println("GET SELLER PAYMENT DETAILS URL IS---" + GetSellerPaymentDetailsURL);
                                    GetSellerPaymentDetailsAPI(GetSellerPaymentDetailsURL);

                                } else {
                                    util.dialog(SellerPaymentDetailsActivity.this, "Please check your internet connection.");
                                }



                            }
                            else
                            {
                                Toast.makeText(SellerPaymentDetailsActivity.this,PaymentDisputeJson.getString("message"),Toast.LENGTH_SHORT).show();

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SellerPaymentDetailsActivity.this,"Network Code:"+ error.getMessage(),Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SellerPaymentDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(SellerPaymentDetailsActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("trade_id",tradeid);
                params.put("auto_payment_id",autopaymentid);
                params.put("seller_user_code",sellerusercode);
                params.put("seller_mobile_num",sellermobilenum);
                params.put("buyer_user_code",buyerusercode);
                params.put("buyer_mobile_num",buyermobilenum);
                params.put("dispute_comment",comment);


                System.out.println("raise payment dispute params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(SellerPaymentDetailsActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }


    private void AcknowledgeSellerPaymentAPI(String url,final String tradeid,final String sellid,final String autopaymentid,final String buyerusercode,final String buyermobilenum,final String sellerusercode,final String sellermobilenum) {

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(java.net.URL url)
                    throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super
                        .createConnection(url);
                try {
                    httpsURLConnection
                            .setSSLSocketFactory(SingletonActivity.getSSLSocketFactory(SellerPaymentDetailsActivity.this));
                    // httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        System.out.println("RESPONSE OF ACKNOWLEDGE PAYMENT API IS---" + response);



                        JSONObject AcknowledgePaymentJson = null;
                        try {
                            AcknowledgePaymentJson = new JSONObject(response);


                            String statusstr = AcknowledgePaymentJson.getString("status");
                            System.out.println("STATUS OF ACKNOWLEDGE PAYMENT API IS---" + statusstr);

                            if(statusstr.equalsIgnoreCase("true"))
                            {


                                Toast.makeText(SellerPaymentDetailsActivity.this,AcknowledgePaymentJson.getString("message"),Toast.LENGTH_SHORT).show();

                              /*  Intent i = new Intent(SellerPaymentDetailsActivity.this,SellerPaymentDetailsActivity.class);
                                startActivity(i);
*/
                                if (NetworkUtility.checkConnectivity(SellerPaymentDetailsActivity.this)) {
                                    String GetSellerPaymentDetailsURL = APIName.URL + "/seller/getCurrentDealsByTrade?user_code=" + userCodestr + "&tradeId=" + SingletonActivity.tradeid;
                                    System.out.println("GET SELLER PAYMENT DETAILS URL IS---" + GetSellerPaymentDetailsURL);
                                    GetSellerPaymentDetailsAPI(GetSellerPaymentDetailsURL);

                                } else {
                                    util.dialog(SellerPaymentDetailsActivity.this, "Please check your internet connection.");
                                }


                            }
                            else
                            {
                                Toast.makeText(SellerPaymentDetailsActivity.this,AcknowledgePaymentJson.getString("message"),Toast.LENGTH_SHORT).show();

                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  Toast.makeText(SetPasswordActivity.this,"Wrong Details",Toast.LENGTH_SHORT).show();
                        // pdia.dismiss();

                        VolleyLog.d("TAG", "Error: " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        System.out.println("network RESPONSE CODE--------" + networkResponse);

                        if(networkResponse!=null) {
                            int respCode = networkResponse.statusCode;


                            //  String respcodestr = Integer.toString(respCode);
                            if (respCode == 404) {
                                String respData = new String(networkResponse.data);
                                System.out.println("RESPONSE DATA--------" + respData);
                                try {
                                    JSONObject jsonObject = new JSONObject(respData);
                                    String statusstr = jsonObject.getString("status");
                                    String messagestr = jsonObject.getString("message");
                                    if (statusstr.equalsIgnoreCase("false")) {

                                        // noliveoffertxt.setVisibility(View.VISIBLE);
                                        //lv.setVisibility(View.GONE);

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(SellerPaymentDetailsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
                            util.dialog(SellerPaymentDetailsActivity.this,"Some Error Occured,Please try after some time");
                        }

                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("trade_id",tradeid);
                params.put("sell_id",sellid);
                params.put("auto_payment_id",autopaymentid);
                params.put("seller_user_code",sellerusercode);
                params.put("seller_mobile_num",sellermobilenum);
                params.put("buyer_user_code",buyerusercode);
                params.put("buyer_mobile_num",buyermobilenum);



                System.out.println("acknowledge dispute params---" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(SellerPaymentDetailsActivity.this,hurlStack);
        requestQueue.add(stringRequest);
    }





    public class Holder {
        TextView date,paymenttypetxt,utrchqnumdesctxtvw,banknamedesctxtvw,amtdesctxtvw,acktxtvw,disputetxtvw;
        ImageView disputeiv;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {



            sessionManager.logoutUser(mobilenumstr,fcm_id);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }







}
